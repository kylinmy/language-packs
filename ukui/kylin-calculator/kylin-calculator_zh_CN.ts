<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>Calc</name>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="85"/>
        <source>The expression is empty!</source>
        <translation>表达式为空!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="106"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="121"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="126"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="153"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="180"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="247"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="272"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="298"/>
        <source>Expression error!</source>
        <translation>表达式错误!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="116"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="140"/>
        <source>Missing left parenthesis!</source>
        <translation>缺少左括号!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="207"/>
        <source>The value is too large!</source>
        <translation>数值过大!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="219"/>
        <source>Miss operand!</source>
        <translation>缺少操作数!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="328"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="386"/>
        <source>Operator undefined!</source>
        <translation>未定义操作符!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="350"/>
        <source>Divisor cannot be 0!</source>
        <translation>除数不能为0!</translation>
    </message>
    <message>
        <location filename="../calc_programmer/calc/calc.cpp" line="370"/>
        <location filename="../calc_programmer/calc/calc.cpp" line="378"/>
        <source>Right operand error!</source>
        <translation>右操作数错误!</translation>
    </message>
    <message>
        <source>The shifted right operand is negative!</source>
        <translation type="vanished">移位操作右值不能为负数!</translation>
    </message>
</context>
<context>
    <name>FuncList</name>
    <message>
        <location filename="../src/funclist.cpp" line="37"/>
        <source>Calculator</source>
        <translation>计算器</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="42"/>
        <source>standard</source>
        <translation>标准</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="42"/>
        <source>scientific</source>
        <translation>科学</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="58"/>
        <source>Unit converter</source>
        <translation>换算器</translation>
    </message>
    <message>
        <location filename="../src/funclist.cpp" line="64"/>
        <source>exchange rate</source>
        <translation>汇率</translation>
    </message>
</context>
<context>
    <name>IntelModeList</name>
    <message>
        <location filename="../src/basicbutton.cpp" line="129"/>
        <source>standard</source>
        <translation>标准</translation>
    </message>
    <message>
        <location filename="../src/basicbutton.cpp" line="132"/>
        <source>scientific</source>
        <translation>科学</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="275"/>
        <source>Calculator</source>
        <translation>计算器</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1195"/>
        <location filename="../src/mainwindow.cpp" line="1377"/>
        <location filename="../src/mainwindow.cpp" line="1393"/>
        <source>standard</source>
        <translation>标准</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1375"/>
        <source>calculator</source>
        <translation>计算器</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="302"/>
        <source>Copy</source>
        <translation>复制</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="303"/>
        <source>Paste</source>
        <translation>粘贴</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1052"/>
        <source>input too long</source>
        <translation>输入过长</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="55"/>
        <location filename="../src/mainwindow.cpp" line="1377"/>
        <location filename="../src/mainwindow.cpp" line="1397"/>
        <source>scientific</source>
        <translation>科学</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1410"/>
        <source>exchange rate</source>
        <translation>汇率</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="947"/>
        <location filename="../src/mainwindow.cpp" line="951"/>
        <source>Error!</source>
        <translation>错误!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="955"/>
        <source>Input error!</source>
        <translation>输入错误!</translation>
    </message>
</context>
<context>
    <name>ProgramDisplay</name>
    <message>
        <location filename="../src/programmer/programdisplay.cpp" line="56"/>
        <location filename="../src/programmer/programdisplay.cpp" line="79"/>
        <source>input too long!</source>
        <translation>输入过长!</translation>
    </message>
</context>
<context>
    <name>ProgramModel</name>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="234"/>
        <location filename="../src/programmer/programmodel.cpp" line="318"/>
        <source>Input error!</source>
        <translation>输入错误!</translation>
    </message>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="400"/>
        <source>ShowBinary</source>
        <translation>显示二进制</translation>
    </message>
    <message>
        <source>DisplayBinary</source>
        <translation type="vanished">显示二进制</translation>
    </message>
    <message>
        <location filename="../src/programmer/programmodel.cpp" line="406"/>
        <source>HideBinary</source>
        <translation>隐藏二进制</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>FuncList</source>
        <translation type="vanished">功能列表</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="264"/>
        <source>Standard</source>
        <translation>计算器—标准</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="265"/>
        <source>Scientific</source>
        <translation>计算器—科学</translation>
    </message>
    <message>
        <source>standard </source>
        <translation type="vanished">标准 </translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="56"/>
        <location filename="../src/titlebar.cpp" line="67"/>
        <location filename="../src/titlebar.cpp" line="232"/>
        <source>standard</source>
        <translation>标准</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="57"/>
        <location filename="../src/titlebar.cpp" line="244"/>
        <source>scientific</source>
        <translation>科学</translation>
    </message>
    <message>
        <source>scientific </source>
        <translation type="vanished">科学 </translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="266"/>
        <source>Exchange Rate</source>
        <translation>计算器—汇率</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="267"/>
        <source>Programmer</source>
        <translation>计算器—程序员</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="292"/>
        <source>StayTop</source>
        <translation>置顶</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="110"/>
        <location filename="../src/titlebar.cpp" line="293"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="111"/>
        <location filename="../src/titlebar.cpp" line="294"/>
        <source>Maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../src/titlebar.cpp" line="112"/>
        <location filename="../src/titlebar.cpp" line="295"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <source>DisplayBinary</source>
        <translation type="vanished">显示二进制</translation>
    </message>
    <message>
        <location filename="../src/programmer/toolbar.cpp" line="63"/>
        <location filename="../src/programmer/toolbar.cpp" line="219"/>
        <location filename="../src/programmer/toolbar.cpp" line="222"/>
        <source>ShowBinary</source>
        <translation>显示二进制</translation>
    </message>
    <message>
        <location filename="../src/programmer/toolbar.cpp" line="220"/>
        <location filename="../src/programmer/toolbar.cpp" line="221"/>
        <source>HideBinary</source>
        <translation>隐藏二进制</translation>
    </message>
</context>
<context>
    <name>ToolModelOutput</name>
    <message>
        <location filename="../src/toolmodel.cpp" line="80"/>
        <source>Rate update</source>
        <translation>汇率更新</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="95"/>
        <source>Chinese Yuan</source>
        <translation>人民币</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="103"/>
        <source>US Dollar</source>
        <translation>美元</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="515"/>
        <source>Error!</source>
        <translation>错误!</translation>
    </message>
</context>
<context>
    <name>UnitListWidget</name>
    <message>
        <location filename="../src/toolmodel.cpp" line="1163"/>
        <source>currency</source>
        <translation>货币</translation>
    </message>
    <message>
        <location filename="../src/toolmodel.cpp" line="1169"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>search</source>
        <translation type="vanished">搜索</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="44"/>
        <source>Menu</source>
        <translation>菜单</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="56"/>
        <location filename="../src/menumodule/menumodule.cpp" line="155"/>
        <source>Standard</source>
        <translation>标准</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="58"/>
        <location filename="../src/menumodule/menumodule.cpp" line="157"/>
        <source>Scientific</source>
        <translation>科学</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="60"/>
        <location filename="../src/menumodule/menumodule.cpp" line="159"/>
        <source>Exchange Rate</source>
        <translation>汇率</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="62"/>
        <location filename="../src/menumodule/menumodule.cpp" line="161"/>
        <source>Programmer</source>
        <translation>程序员</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="68"/>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="70"/>
        <location filename="../src/menumodule/menumodule.cpp" line="153"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="72"/>
        <location filename="../src/menumodule/menumodule.cpp" line="151"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="74"/>
        <location filename="../src/menumodule/menumodule.cpp" line="149"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="85"/>
        <source>Auto</source>
        <translation>自动</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="89"/>
        <source>Light</source>
        <translation>浅色</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="93"/>
        <source>Dark</source>
        <translation>深色</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="302"/>
        <source>Version: </source>
        <translation>版本号: </translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="306"/>
        <source>Calculator is a lightweight calculator based on Qt5, which provides standard calculation, scientific calculation and exchange rate conversion.</source>
        <translation>计算器是一款基于qt5开发的轻量级计算器，提供标准计算，科学计算和汇率换算。</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.cpp" line="371"/>
        <location filename="../src/menumodule/menumodule.cpp" line="379"/>
        <source>Service &amp; Support: </source>
        <translation>服务与支持团队：</translation>
    </message>
    <message>
        <source>Support: support@kylinos.cn</source>
        <translation type="vanished">支持：support@kylinos.cn</translation>
    </message>
    <message>
        <location filename="../src/menumodule/menumodule.h" line="66"/>
        <source>Calculator</source>
        <translation>计算器</translation>
    </message>
</context>
</TS>
