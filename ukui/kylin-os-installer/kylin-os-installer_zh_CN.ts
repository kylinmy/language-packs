<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="zh_CN" sourcelanguage="en_AS">
<context>
    <name>DiskInfoView</name>
    <message>
        <location filename="../src/plugins/KPartition/frames/diskinfoview.cpp" line="184"/>
        <source>as data disk</source>
        <translation>设置为数据盘</translation>
    </message>
</context>
<context>
    <name>KInstaller::CreatePartitionFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="417"/>
        <source>OK</source>
        <translatorcomment>确定</translatorcomment>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="419"/>
        <source>Used to:</source>
        <translation>用于：</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="423"/>
        <source>Mount point</source>
        <translatorcomment>挂载点：</translatorcomment>
        <translation>挂载点：</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="425"/>
        <source>Location for the new partition</source>
        <translatorcomment>新分区的位置：</translatorcomment>
        <translation>新分区的位置</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="420"/>
        <source>Create Partition</source>
        <translation>新建分区</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="361"/>
        <source>Mount point starts with &apos;/&apos;</source>
        <translation>挂载点要以“/”开头</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="302"/>
        <source>The disk can only create one root partition!</source>
        <translation>根分区只能创建一个，后续再创建的根分区无效!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="319"/>
        <source>The filesystem type of boot partition should be ext4 or ext3!</source>
        <translation>boot分区的格式只能为ext4或ext3!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="343"/>
        <source>The disk can only create one boot partition!</source>
        <translation>boot分区只能创建一个，后续再创建的boot分区无效!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="426"/>
        <source>End of this space</source>
        <translation>剩余空间尾部</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="427"/>
        <source>Beginning of this space</source>
        <translatorcomment>剩余空间头部</translatorcomment>
        <translation>剩余空间头部</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="429"/>
        <source>Type for the new partition:</source>
        <translatorcomment>新分区的类型：</translatorcomment>
        <translation>新分区的类型：</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="493"/>
        <source>Recommended efi partition size is between 256MiB and 2GiB.</source>
        <translation>EFI分区大小256MiB到2GiB之间。</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="517"/>
        <source>Recommended boot partition size is between 1GiB and 2GiB.
Note that the size must be greater than 500MiB.</source>
        <translation>boot分区大小推荐1GiB到2GiB之间，不可小于500MiB。</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="520"/>
        <source>Root partition size is greater than 15GiB, 
but Huawei machines require greater than 25GiB.</source>
        <translation>根分区大小应大于15GiB，华为机器要求大于25GiB。</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="538"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="435"/>
        <source>Size(MiB)</source>
        <translation>大小(MiB)</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="430"/>
        <source>Logical</source>
        <translatorcomment>逻辑分区</translatorcomment>
        <translation>逻辑分区</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="431"/>
        <source>Primary</source>
        <translatorcomment>主分区</translatorcomment>
        <translation>主分区</translation>
    </message>
</context>
<context>
    <name>KInstaller::CustomPartitionFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/custompartitionframe.cpp" line="228"/>
        <source>remove this partition?</source>
        <translation>确定删除此分区?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/custompartitionframe.cpp" line="303"/>
        <source>Device for boot loader path:</source>
        <translation>引导加载程序路径：</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/custompartitionframe.cpp" line="304"/>
        <source>Revert</source>
        <translatorcomment>还原</translatorcomment>
        <translation>还原</translation>
    </message>
</context>
<context>
    <name>KInstaller::CustomPartitiondelegate</name>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="81"/>
        <source>#%1 partition on the device %2 will be created.
</source>
        <translation>#%1 分区在设备上 %2 将被创建。
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="85"/>
        <source>#%1 partition on the device %2 will be mounted %3.
</source>
        <translation>#%1 分区在 %2 上将被挂载为 %3。
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="90"/>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="99"/>
        <source>#%1 partition on the device %2 will be formated %3.
</source>
        <translation>#%1 分区在 %2 上将被格式化为 %3。
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="95"/>
        <source>#%1 partition on the device %2 will be deleted.
</source>
        <translation>#%1 分区在设备 %2 中将被删除。
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="103"/>
        <source>#%1 partition  on the device %2 will be mounted %3.
</source>
        <translation>#%1 分区在 %2 上将被挂载为 %3。
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="118"/>
        <source>%1 GPT new partition table will be created.
</source>
        <translation>%1 GPT 分区表将被创建。
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="120"/>
        <source>%1 MsDos new partition table will be created.
</source>
        <translation>%1 MsDos 分区表将被创建。
</translation>
    </message>
</context>
<context>
    <name>KInstaller::FinishedFrame</name>
    <message>
        <location filename="../src/Installer_main/finishedInstall.cpp" line="80"/>
        <source>Installation Finished</source>
        <translation>安装完成</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/finishedInstall.cpp" line="81"/>
        <source>Restart</source>
        <translation>现在重启</translation>
    </message>
</context>
<context>
    <name>KInstaller::FullPartitionFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="150"/>
        <source>This action will  use the original old account as the default account. Please make sure you remember the password of the original account!</source>
        <translation>勾选该选项将保留原来系统的用户及其目录下的数据，同时也会保留data分区中的数据。在版本匹配的情况下，系统会默认保存您的系统激活信息与激活状态。
选择该选项将不会再提示您新建用户，请确保你记这些账户的密码。</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="249"/>
        <source>Please choose custom way to install, disk size less than 50G!</source>
        <translation>请选择自定义方式进行安装，磁盘大小小于50G！</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="250"/>
        <source>Full disk encryption</source>
        <translation>全盘加密</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="251"/>
        <source>lvm</source>
        <translation>逻辑卷(lvm)</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="252"/>
        <source>Factory backup</source>
        <translation>出厂备份</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="253"/>
        <source>save history date</source>
        <translation>保留用户及其数据</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="384"/>
        <source>By default, SSDS serve as system disks and HDDS serve as user data disks</source>
        <translation>默认安装，SSD磁盘作为系统盘，HDD磁盘作为用户数据盘</translation>
    </message>
</context>
<context>
    <name>KInstaller::ImgFrame</name>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="211"/>
        <source>Select install way</source>
        <translation>选择安装途径</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="212"/>
        <source>install from ghost</source>
        <translation>从Ghost安装</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="213"/>
        <source>install form live</source>
        <translation>从Live安装</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="127"/>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="214"/>
        <source>open file</source>
        <translation>打开文件</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="215"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallerMainWidget</name>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="252"/>
        <source>About to exit the installer, restart the computer.</source>
        <translation>即将退出安装程序，重启计算机。</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="254"/>
        <source>About to exit the installer and return to the trial interface.</source>
        <translation>即将退出安装程序，回到试用界面。</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="307"/>
        <source>Installer is about to exit and the computer will be shut down.</source>
        <translation>安装程序即将退出，计算机将关机。</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="531"/>
        <source>back</source>
        <translation>上一步</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="508"/>
        <source>quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="485"/>
        <source>keyboard</source>
        <translation>键盘</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallingFrame</name>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="151"/>
        <source>Log</source>
        <translation>安装日志</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="150"/>
        <source>The system is being installed, please do not turn off the computer</source>
        <translation>系统正在安装，请不要关闭计算机</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="185"/>
        <source>Install failed</source>
        <translation>安装失败</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="187"/>
        <source>Sorry, KylinOS cannot continue the installation. Please feed back the error log below so that we can better solve the problem for you.</source>
        <translation>非常抱歉kylinOS无法继续安装。请反馈下方错误日志，以便我们更好的为您解决问题.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="194"/>
        <source>Restart</source>
        <translation>退出</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallingOEMConfigFrame</name>
    <message>
        <location filename="../src/Installer_main/installingoemconfigframe.cpp" line="66"/>
        <source>Progressing system configuration</source>
        <translation>正在进行系统配置</translation>
    </message>
</context>
<context>
    <name>KInstaller::LanguageFrame</name>
    <message>
        <location filename="../src/plugins/KChoiceLanguage/languageframe.cpp" line="222"/>
        <source>Select Language</source>
        <translation>选择语言</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceLanguage/languageframe.cpp" line="223"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
</context>
<context>
    <name>KInstaller::LicenseFrame</name>
    <message>
        <location filename="../src/plugins/KyLicense/licenseframe.cpp" line="165"/>
        <source>Read License Agreement</source>
        <translation>阅读许可协议</translation>
    </message>
    <message>
        <location filename="../src/plugins/KyLicense/licenseframe.cpp" line="166"/>
        <source>I have read and agree to the terms of the agreement</source>
        <translation>我已经阅读并同意协议条款</translation>
    </message>
    <message>
        <location filename="../src/plugins/KyLicense/licenseframe.cpp" line="168"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
</context>
<context>
    <name>KInstaller::MainPartFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="600"/>
        <source>Custom install</source>
        <translatorcomment>自定义安装</translatorcomment>
        <translation>自定义安装</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="351"/>
        <source>Coexist Install</source>
        <translation>并存安装</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="419"/>
        <source>Confirm Custom Installation</source>
        <translation>确认自定义安装</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="312"/>
        <source>Formatted the whole disk</source>
        <translation>格式化整个磁盘。</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="302"/>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="436"/>
        <source>Start Installation</source>
        <translation>开始安装</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="314"/>
        <source>EFI partition #1 of %1 as vfat;
boot partition #2 of  %1 as ext4;
/ partition #3 of  %1 as ext4;
backup partition #4 of  %1 as ext4;
data partition #5 of  %1 as ext4;
swap partition #6 of  %1 as swap;
</source>
        <translation>%1 设备上第 1 分区为 EFI, 将设为 vfat;
%1 设备上第 2 分区为 boot, 将设为 ext4;
%1 设备上第 3 分区为 /, 将设为 ext4;
%1 设备上第 4 分区为 backup, 将设为 ext4;
%1 设备上第 5 分区为 data, 将设为 ext4;
%1 设备上第 6 分区为 swap, 将设为 swap;</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="321"/>
        <source>boot partition #1 of %1 as vfat;
extend partition #2 of  %1 ;
/ partition #5 of  %1 as ext4;
backup partition #6 of  %1 as ext4;
data partition #7 of  %1 as ext4;
swap partition #8 of  %1 as swap;
</source>
        <translation>%1 设备上第 1 分区为 boot, 将设为 ext4;
%1 设备上第 2 分区为 extend;
%1 设备上第 5 分区为 /, 将设为 ext4;
%1 设备上第 6 分区为 backup, 将设为 ext4;
%1 设备上第 7 分区为 data, 将设为 ext4;
%1 设备上第 8 分区为 swap, 将设为 swap;</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="427"/>
        <source>Confirm the above operations</source>
        <translation>确认以上操作</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="510"/>
        <source>InvalidId
</source>
        <translation>无效
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="513"/>
        <source>Boot filesystem invalid
</source>
        <translation>Boot分区文件系统不可用
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="528"/>
        <source>the EFI partition size should be set between 256MB and 2GB
</source>
        <translation>Efi分区大小应设置在256MB到2GB之间。</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="540"/>
        <source>Root partition size is greater than 15GiB,
but Huawei machines require greater than 25GiB.
</source>
        <translation>根分区大小大于15GiB，华为机器要求大于25GiB。
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="519"/>
        <source>Boot partition too small
</source>
        <translation>Boot分区太小
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="311"/>
        <source>Confirm Full Installation</source>
        <translation>确认全盘安装</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="337"/>
        <source> set to data device;
</source>
        <translation> 设为数据盘</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="381"/>
        <source>bootloader type of Legacy should not contain EFI partition!</source>
        <translation>系统为legacy启动方式时,不可含有efi分区!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="481"/>
        <source>BootLoader method %1 inconsistent with the disk partition table 
type %2.</source>
        <translation>引导加载程序方法 %1 与磁盘分区表 %2 不一致</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="484"/>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="489"/>
        <source>BootLoader method %1 inconsistent with the disk partition table 
type %2, cannot have efi partition.</source>
        <translation>引导加载程序方法 %1 与磁盘分区表 %2 不一致，不可使用efi分区</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="516"/>
        <source>Boot partition not in the first partition
</source>
        <translation>Boot分区必须为第一分区!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="522"/>
        <source>No boot partition
</source>
        <translation>未找到boot分区
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="525"/>
        <source>No Efi partition
</source>
        <translation>未找到Efi分区
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="531"/>
        <source>Only one EFI partition is allowed
</source>
        <translation>只允许一个EFI分区存在
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="534"/>
        <source>Efi partition number invalid
</source>
        <translation>Efi分区编号不可用
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="537"/>
        <source>No root partition
</source>
        <translation>未找到根分区。
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="553"/>
        <source>No backup partition
</source>
        <translation>未找到备份还原分区。
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="562"/>
        <source>This machine not support EFI partition
</source>
        <translation>当前这台机器不支持EFI分区!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="599"/>
        <source>Full install</source>
        <translation>全盘安装</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="544"/>
        <source>Partition too small
</source>
        <translation>分区太小
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="547"/>
        <source>BackUp partition too small
</source>
        <translation>备份还原分区太小
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="550"/>
        <source>Repeated mountpoint
</source>
        <translation>存在重复挂载的分区
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="565"/>
        <source>The filesystem type FAT16 or FAT32 is not fully-function filesystem,
except EFI partition, other partition not proposed</source>
        <translation>FAT16和FAT32是不完整的文件系统，
除了EFI分区外，其他分区不建议使用。</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="598"/>
        <source>Choose Installation Method</source>
        <translation>选择安装方式</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="462"/>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="602"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
</context>
<context>
    <name>KInstaller::MiddleFrameManager</name>
    <message>
        <location filename="../src/plugins/PluginService/ui_unit/middleframemanager.cpp" line="44"/>
        <source>next</source>
        <translatorcomment>下一步</translatorcomment>
        <translation>下一步</translation>
    </message>
</context>
<context>
    <name>KInstaller::ModifyPartitionFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="48"/>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="201"/>
        <source>unused</source>
        <translation>不使用</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="187"/>
        <source>Format partition.</source>
        <translation>格式化此分区。</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="188"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="190"/>
        <source>Used to:</source>
        <translation>用于：</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="191"/>
        <source>Modify Partition</source>
        <translation>修改分区</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="314"/>
        <source>The filesystem type of boot partition should be ext4 or ext3!</source>
        <translation>boot分区的格式只能为ext4或ext3!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="343"/>
        <source>The disk can only create one boot partition!</source>
        <translation>boot分区只能创建一个，后续再创建的boot分区无效!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="371"/>
        <source>The disk can only create one root partition!</source>
        <translation>根分区只能创建一个，后续再创建的根分区无效!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="387"/>
        <source>Mount point starts with &apos;/&apos;</source>
        <translation>挂载点要以“/”开头</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="472"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="196"/>
        <source>Mount point</source>
        <translation>挂载点</translation>
    </message>
</context>
<context>
    <name>KInstaller::PrepareInstallFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/prepareinstallframe.cpp" line="109"/>
        <source>Check it and click [Start Installation]</source>
        <translatorcomment>勾选后点击[开始安装]</translatorcomment>
        <translation>勾选后点击[开始安装]</translation>
    </message>
</context>
<context>
    <name>KInstaller::SlideShow</name>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="243"/>
        <source>Multi platform support, suitable for mainstream CPU platforms at home and abroad.</source>
        <translation>多平台支持，适配国内外主流CPU平台。</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="244"/>
        <source>Ukui desktop environment meets personalized needs.</source>
        <translation>UKUI桌面环境，满足个性化需求。</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="245"/>
        <source>The system is updated and upgraded more conveniently.</source>
        <translation>系统更新，升级更便捷。</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="246"/>
        <source>Security center, all-round protection of data security.</source>
        <translation>安全中心，全方位保护数据安全。</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="247"/>
        <source>Software store, featured software recommendation.</source>
        <translation>软件商店，精选软件推荐。</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="248"/>
        <source>Provide technical support and professional after-sales service.</source>
        <translation>技术支持，提供专业的售后服务。</translation>
    </message>
</context>
<context>
    <name>KInstaller::TableWidgetView</name>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="111"/>
        <source>This action will delect all partition,are you sure?</source>
        <translation>将清空现有分区表，确定清空？</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="294"/>
        <source>yes</source>
        <translation>是</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="294"/>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="327"/>
        <source>no</source>
        <translation>否</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="167"/>
        <source>Create partition table</source>
        <translation>创建分区表</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="321"/>
        <source>freespace</source>
        <translation>空闲</translation>
    </message>
</context>
<context>
    <name>KInstaller::TimeZoneFrame</name>
    <message>
        <location filename="../src/plugins/KTimeZone/timezoneframe.cpp" line="118"/>
        <source>Select Timezone</source>
        <translation>选择时区</translation>
    </message>
    <message>
        <location filename="../src/plugins/KTimeZone/timezoneframe.cpp" line="119"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
</context>
<context>
    <name>KInstaller::UserFrame</name>
    <message>
        <location filename="../src/plugins/KUserRegister/userframe.cpp" line="113"/>
        <source>Start Installation</source>
        <translation>开始安装</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userframe.cpp" line="109"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
</context>
<context>
    <name>KInstaller::appcheckframe</name>
    <message>
        <location filename="../src/plugins/KInstallAPP/appcheckframe.cpp" line="95"/>
        <source>Start Installation</source>
        <translation>开始安装</translation>
    </message>
    <message>
        <location filename="../src/plugins/KInstallAPP/appcheckframe.cpp" line="93"/>
        <source>Choose your app</source>
        <translation>选择你的应用</translation>
    </message>
    <message>
        <location filename="../src/plugins/KInstallAPP/appcheckframe.cpp" line="94"/>
        <source>Select the application you want to install to install with one click when using the system.</source>
        <translation>选择你想要安装的应用，以在使用系统时一键安装。</translation>
    </message>
</context>
<context>
    <name>KInstaller::conferquestion</name>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="105"/>
        <source>confer_mainTitle</source>
        <translation>安全问题</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="106"/>
        <source>You can reset your password by answering a security question when you forget your password. Make sure you remember the answer.</source>
        <translation>你可以在忘记密码时通过回答安全问题重置密码，请确保一定会记住答案。</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="109"/>
        <source>Which city were you born in?</source>
        <translation>您出生城市的名字？</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="110"/>
        <source>What is your childhood nickname?</source>
        <translation>您孩童时候的昵称？</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="111"/>
        <source>Which middle school did you graduate from?</source>
        <translation>您毕业于哪所中学？</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="112"/>
        <source>What is your father&apos;s name?</source>
        <translation>您父亲的名字？</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="113"/>
        <source>What is your mother&apos;s name?</source>
        <translation>您母亲的名字？</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="114"/>
        <source>When is your spouse&apos;s birthday?</source>
        <translation>您配偶的生日？</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="115"/>
        <source>What&apos;s your favorite animal?</source>
        <translation>您最喜欢的动物？</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="119"/>
        <source>question%1:</source>
        <translation>问题%1:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="120"/>
        <source>answer%1:</source>
        <translation>答案%1:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="126"/>
        <source>set later</source>
        <translation>稍后设置</translation>
    </message>
</context>
<context>
    <name>KInstaller::modeselect</name>
    <message>
        <location filename="../src/plugins/KUserRegister/modeselectframe.cpp" line="62"/>
        <source>Register account</source>
        <translation>创建帐户</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/modeselectframe.cpp" line="68"/>
        <source>Register immediately</source>
        <translation>立即创建</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/modeselectframe.cpp" line="76"/>
        <source>Register later</source>
        <translation>稍后创建</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/modeselectframe.cpp" line="77"/>
        <source>After the system installation is completed, an account will be created when entering the system.</source>
        <translation>将在系统安装完成后，进入系统时创建帐户。</translation>
    </message>
</context>
<context>
    <name>KInstaller::userregisterwidiget</name>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="377"/>
        <source>Two password entries are inconsistent!</source>
        <translation>两次密码输入不一致！</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="432"/>
        <source>Hostname should be more than 0 bits in length.</source>
        <translation>主机名的长度不可少于1个字符</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="434"/>
        <source>Hostname should be no more than 64 bits in length.</source>
        <translation>主机名的长度不可超过64个字符</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="436"/>
        <source>Hostname only letters,numbers,hyphen and dot notation are allowed</source>
        <translation>主机名只允许含有字母，数字，连接符或点符号</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="438"/>
        <source>Hostname must start with a number or a letter</source>
        <translation>主机名首部必须为字母或数字</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="440"/>
        <source>Hostname must end with a number or a letter</source>
        <translation>主机名尾部必须为字母或数字</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="442"/>
        <source>Hostname cannot have consecutive &apos; - &apos; and &apos; . &apos;</source>
        <translation>主机名中连字符和点符号不可相连</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="444"/>
        <source>Hostname cannot have consecutive &apos; . &apos;</source>
        <translation>主机名中不可含有连续的点符号</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="568"/>
        <source>Create User</source>
        <translation>创建用户</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="569"/>
        <source>username</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="570"/>
        <source>hostname</source>
        <translation>主机名</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="571"/>
        <source>new password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="572"/>
        <source>enter the password again</source>
        <translation>再次输入密码</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="573"/>
        <source>Automatic login on boot</source>
        <translation>开机自动登录</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="574"/>
        <source>Biometric [authentication] device detected / unified login support</source>
        <translation>检测到生物特征【认证】设备/统一登录支持，
请在生物特征管理工具/设置-登录选项中进行设置。</translation>
    </message>
</context>
<context>
    <name>KServer::EncryptSetFrame</name>
    <message>
        <source>password:</source>
        <translation type="obsolete">密码：</translation>
    </message>
    <message>
        <source>confirm password:</source>
        <translation type="obsolete">确认密码：</translation>
    </message>
    <message>
        <source>Please keep your password properly.If you forget it,
you will not be able to access the disk data.</source>
        <translation type="obsolete">请妥善保管您的密码。如忘记密码，将无法访问磁盘数据。</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="110"/>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="111"/>
        <source>password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="152"/>
        <source>The password contains less than two types of characters</source>
        <translation>密码包含的字符类型少于2种</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="172"/>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="173"/>
        <source>confirm password</source>
        <translation>确认密码</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="213"/>
        <source>The two passwords are different</source>
        <translation>俩次密码不一致</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="238"/>
        <source>Please keep your password properly.If you forget it,you will not be able to access the disk data.</source>
        <translation>请妥善保管您的密码，如忘记密码，将无法访问磁盘。</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="321"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="322"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="obsolete">关闭</translation>
    </message>
    <message>
        <source>Two password entries are inconsistent!</source>
        <translation type="obsolete">两次密码输入不一致！</translation>
    </message>
</context>
<context>
    <name>KServer::MessageBox</name>
    <message>
        <location filename="../src/plugins/PluginService/ui_unit/messagebox.cpp" line="183"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/ui_unit/messagebox.cpp" line="184"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/ui_unit/messagebox.cpp" line="276"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>KeyboardWidget</name>
    <message>
        <location filename="../src/plugins/VirtualKeyboard/src/keyboardwidget.ui" line="29"/>
        <source>KeyboardWidget</source>
        <translation>KeyboardWidget</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="9"/>
        <source>wd</source>
        <translation>西部数据</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="11"/>
        <source>seagate</source>
        <translation>希捷</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="13"/>
        <source>hitachi</source>
        <translation>日立</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="15"/>
        <source>samsung</source>
        <translation>三星</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="17"/>
        <source>toshiba</source>
        <translation>东芝</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="19"/>
        <source>fujitsu</source>
        <translation>富士通</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="21"/>
        <source>maxtor</source>
        <translation>迈拓</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="23"/>
        <source>IBM</source>
        <translation>IBM</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="25"/>
        <source>excelStor</source>
        <translation>易拓</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="27"/>
        <source>lenovo</source>
        <translation>联想</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="29"/>
        <source>other</source>
        <translation>其他</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="31"/>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="303"/>
        <source>Unknown</source>
        <translation>未知</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="80"/>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="314"/>
        <source>Freespace</source>
        <translation>空闲</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="294"/>
        <source>kylin data partition</source>
        <translation>数据盘</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="301"/>
        <source>Swap partition</source>
        <translation>交换分区</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/main.cpp" line="160"/>
        <source>Show debug informations</source>
        <translation>展现调试信息</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="191"/>
        <source>Extended partition %1 has 
</source>
        <translation>扩展分区%1有</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="193"/>
        <source>Create new partition %1,%2
</source>
        <translation>创建新分区 %1,%2
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="196"/>
        <source>Create new partition %1,%2,%3
</source>
        <translation>创建新分区 %1,%2,%3
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="201"/>
        <source>Delete partition %1
</source>
        <translation>删除分区 %1
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="205"/>
        <source>Format %1 partition, %2
</source>
        <translation>格式化分区 %1,%2
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="208"/>
        <source>Format partition %1,%2,%3
</source>
        <translation>格式化分区 %1,%2,%3
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="213"/>
        <source>%1 partition mountPoint %2
</source>
        <translation>%1 分区挂载点 %2
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="217"/>
        <source>New Partition Table %1
</source>
        <translation>新分区表 %1
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="220"/>
        <source>Reset size %1 partition
</source>
        <translation>重置分区%1大小</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/kcommand.cpp" line="21"/>
        <source>KCommand::m_cmdInstance is not init.</source>
        <translation>命令m_cmdInstance未初始化</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/kcommand.cpp" line="58"/>
        <location filename="../src/plugins/PluginService/kcommand.cpp" line="90"/>
        <source>WorkingPath is not found. 
</source>
        <translation>未找到工作目录</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/kcommand.cpp" line="82"/>
        <source>Shell file is empty, does not continue. 
</source>
        <translation>shell文件为空，任务停止。</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="164"/>
        <source>device</source>
        <translation>设备</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="164"/>
        <source>type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="164"/>
        <source>size</source>
        <translation>大小</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="164"/>
        <source>mounted</source>
        <translation>挂载点</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="165"/>
        <source>used</source>
        <translation>已用</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="165"/>
        <source>system</source>
        <translation>系统</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="165"/>
        <source>format</source>
        <translation>格式化</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="69"/>
        <source>Memory allocation error when setting</source>
        <translation>设置时发生内存分配错误</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="73"/>
        <source>Memory allocation error</source>
        <translation>内存分配错误</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="75"/>
        <source>The password is the same as the old one</source>
        <translation>这个密码和原来的相同</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="77"/>
        <source>The password is a palindrome</source>
        <translation>密码是一个回文</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="79"/>
        <source>The password differs with case changes only</source>
        <translation>密码仅包含大小写变更</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="81"/>
        <source>The password is too similar to the old one</source>
        <translation>密码与原来的太相似</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="83"/>
        <source>The password contains the user name in some form</source>
        <translation>密码包含了某种形式的用户名</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="85"/>
        <source>The password contains words from the real name of the user in some form</source>
        <translation>密码包含了某种形式的用户真实名称</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="87"/>
        <source>The password contains forbidden words in some form</source>
        <translation>密码包含了某种形式的禁用单词</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="92"/>
        <source>The password contains too few digits</source>
        <translation>密码包含的数字字符太少</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="97"/>
        <source>The password contains too few uppercase letters</source>
        <translation>密码包含的大写字母太少</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="102"/>
        <source>The password contains too few lowercase letters</source>
        <translation>密码包含的小写字母太少</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="107"/>
        <source>The password contains too few non-alphanumeric characters</source>
        <translation>密码包含的特殊字符数太少</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="112"/>
        <source>The password is too short</source>
        <translation>密码太短了</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="114"/>
        <source>The password is just rotated old one</source>
        <translation>密码仅是旧密码的反转</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="119"/>
        <source>The password does not contain enough character classes</source>
        <translation>密码未包含足够的字符类型</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="124"/>
        <source>The password contains too many same characters consecutively</source>
        <translation>密码包含了太多相同连续字符</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="129"/>
        <source>The password contains too many characters of the same class consecutively</source>
        <translation>密码包含了过多的同类型连续字符</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="90"/>
        <source>The password contains less than %1 digits</source>
        <translation>密码包含的数字字符少于 %1 位</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="95"/>
        <source>The password contains less than %1 uppercase letters</source>
        <translation>密码包含的大写字母少于 %1 位</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="100"/>
        <source>The password contains less than %1 lowercase letters</source>
        <translation>密码包含的小写字母少于 %1 位</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="105"/>
        <source>The password contains less than %1 non-alphanumeric characters</source>
        <translation>密码包含的特殊字符少于 %1 位</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="110"/>
        <source>The password is shorter than %1 characters</source>
        <translation>密码少于 %1 个字符</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="117"/>
        <source>The password contains less than %1 character classes</source>
        <translation>密码包含的字符类型少于 %1 种</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="122"/>
        <source>The password contains more than %1 same characters consecutively</source>
        <translation>密码包含了超过 %1 位相同连续字符</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="127"/>
        <source>The password contains more than %1 characters of the same class consecutively</source>
        <translation>密码包含了超过 %1 位同类型连续字符</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="132"/>
        <source>The password contains monotonic sequence longer than %1 characters</source>
        <translation>密码包含超过%1 个字符的单调序列</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="134"/>
        <source>The password contains too long of a monotonic character sequence</source>
        <translation>密码包含过长的单调字符序列</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="136"/>
        <source>No password supplied</source>
        <translation>密码未提供</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="138"/>
        <source>Cannot obtain random numbers from the RNG device</source>
        <translation>无法从 RNG 随机数生成设备获得随机数</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="140"/>
        <source>Password generation failed - required entropy too low for settings</source>
        <translation>密码生成失败 - 未达到设置所需的熵</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="143"/>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="145"/>
        <source>The password fails the dictionary check</source>
        <translation>密码未通过字典检查-密码基于字典中单词</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="149"/>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="151"/>
        <source>Unknown setting</source>
        <translation>未知设置</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="155"/>
        <source>Bad integer value of setting</source>
        <translation>错误的整数设置值</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="157"/>
        <source>Bad integer value</source>
        <translation>错误的整数设置值</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="161"/>
        <source>Setting %s is not of integer type</source>
        <translation>设置 %s 并非整数类型</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="163"/>
        <source>Setting is not of integer type</source>
        <translation>设置并非整数类型</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="167"/>
        <source>Setting %s is not of string type</source>
        <translation>设置 %s 并非字符串类型</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="169"/>
        <source>Setting is not of string type</source>
        <translation>设置并非字符串类型</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="171"/>
        <source>Opening the configuration file failed</source>
        <translation>打开配置文件失败</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="173"/>
        <source>The configuration file is malformed</source>
        <translation>配置文件格式不正确</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="175"/>
        <source>Fatal failure</source>
        <translation>致命的错误</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="177"/>
        <source>Unknown error</source>
        <translation>未知的错误</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="440"/>
        <source>unused</source>
        <translation>不使用</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="442"/>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="203"/>
        <source>kylin-data</source>
        <translation>用户数据分区</translation>
    </message>
</context>
</TS>
