<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="kk" sourcelanguage="en_AS">
<context>
    <name>DiskInfoView</name>
    <message>
        <location filename="../src/plugins/KPartition/frames/diskinfoview.cpp" line="184"/>
        <source>as data disk</source>
        <translation>Деректер дискісіне орнату</translation>
    </message>
</context>
<context>
    <name>KInstaller::CreatePartitionFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="417"/>
        <source>OK</source>
        <translatorcomment>确定</translatorcomment>
        <translation>Сенімдісің бе</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="419"/>
        <source>Used to:</source>
        <translation>Үшін:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="423"/>
        <source>Mount point</source>
        <translatorcomment>挂载点：</translatorcomment>
        <translation>Маунт нүктесі:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="425"/>
        <source>Location for the new partition</source>
        <translatorcomment>新分区的位置：</translatorcomment>
        <translation>Жаңа қалқаның орналасқан жері</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="420"/>
        <source>Create Partition</source>
        <translation>Жаңа қалқа жасау</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="361"/>
        <source>Mount point starts with &apos;/&apos;</source>
        <translation>Бекіту нүктесі «/» дегеннен басталуы тиіс.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="302"/>
        <source>The disk can only create one root partition!</source>
        <translation>Түбір бөлінуді тек қана жасауға болады, ал кейінірек жасалған түбір қалқа жарамсыз!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="319"/>
        <source>The filesystem type of boot partition should be ext4 or ext3!</source>
        <translation>Жүктеу қалқасы тек ext4 немесе ext3 форматында болуы мүмкін!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="343"/>
        <source>The disk can only create one boot partition!</source>
        <translation>Бәтеңке қалқасын тек қана жасауға болады, ал кейінірек жасалған жүктеу қалқасы жарамсыз!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="426"/>
        <source>End of this space</source>
        <translation>Қалған кеңістіктің құйрығы</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="427"/>
        <source>Beginning of this space</source>
        <translatorcomment>剩余空间头部</translatorcomment>
        <translation>Бос орынның қалған басы</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="429"/>
        <source>Type for the new partition:</source>
        <translatorcomment>新分区的类型：</translatorcomment>
        <translation>Жаңа қалқаның түрі:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="493"/>
        <source>Recommended efi partition size is between 256MiB and 2GiB.</source>
        <translation>EFI қалқа өлшемі 256 MiB және 2 GiB аралығында.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="517"/>
        <source>Recommended boot partition size is between 1GiB and 2GiB.
Note that the size must be greater than 500MiB.</source>
        <translation>Жүктеу қалқасының өлшемі 1 ГИБ-тан 2 ГИБ-қа дейінгі аралықта болуы ұсынылады және 500 MiB-ден кіші бола алмайды.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="520"/>
        <source>Root partition size is greater than 15GiB, 
but Huawei machines require greater than 25GiB.</source>
        <translation>Түпкі қалқа өлшемі 15 ГиБ-тан артық болуы тиіс, ал Huawei машиналары 25 ГиБ-тан артық болуы тиіс.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="538"/>
        <source>close</source>
        <translation>Тоқтату</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="435"/>
        <source>Size(MiB)</source>
        <translation>Өлшемі (MiB)</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="430"/>
        <source>Logical</source>
        <translatorcomment>逻辑分区</translatorcomment>
        <translation>Логикалық бөліну</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="431"/>
        <source>Primary</source>
        <translatorcomment>主分区</translatorcomment>
        <translation>Бастапқы бөліну</translation>
    </message>
</context>
<context>
    <name>KInstaller::CustomPartitionFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/custompartitionframe.cpp" line="228"/>
        <source>remove this partition?</source>
        <translation>Бұл бөлінуді жойғыңыз келетініне сенімдісіз бе?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/custompartitionframe.cpp" line="303"/>
        <source>Device for boot loader path:</source>
        <translation>Жүктеу жүктеу жолы:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/custompartitionframe.cpp" line="304"/>
        <source>Revert</source>
        <translatorcomment>还原</translatorcomment>
        <translation>қалпына келтіру</translation>
    </message>
</context>
<context>
    <name>KInstaller::CustomPartitiondelegate</name>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="81"/>
        <source>#%1 partition on the device %2 will be created.
</source>
        <translation>Құрылғыда % 1% 2 бөлінуі жасалады.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="85"/>
        <source>#%1 partition on the device %2 will be mounted %3.
</source>
        <translation>%% 1 қалқасы% 2 дегенде% 3 ретінде орнатылатын болады.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="90"/>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="99"/>
        <source>#%1 partition on the device %2 will be formated %3.
</source>
        <translation>%% 1 қалқасы% 2 дегенде% 3 ретінде пішімделеді.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="95"/>
        <source>#%1 partition on the device %2 will be deleted.
</source>
        <translation>% 1 қалқасы% 2 құрылғыда жойылады.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="103"/>
        <source>#%1 partition  on the device %2 will be mounted %3.
</source>
        <translation>%% 1 қалқасы% 2 дегенде% 3 ретінде орнатылатын болады.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="118"/>
        <source>%1 GPT new partition table will be created.
</source>
        <translation>% 1 GPT қалқалар кестесі жасалады.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/delegate/custom_partition_delegate.cpp" line="120"/>
        <source>%1 MsDos new partition table will be created.
</source>
        <translation>% 1 MSDos қалқалар кестесі жасалады.
</translation>
    </message>
</context>
<context>
    <name>KInstaller::FinishedFrame</name>
    <message>
        <location filename="../src/Installer_main/finishedInstall.cpp" line="80"/>
        <source>Installation Finished</source>
        <translation>Орнату аяқталды</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/finishedInstall.cpp" line="81"/>
        <source>Restart</source>
        <translation>Енді қайта жүктеу</translation>
    </message>
</context>
<context>
    <name>KInstaller::FullPartitionFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="150"/>
        <source>This action will  use the original old account as the default account. Please make sure you remember the password of the original account!</source>
        <translation>Бұл параметрді тексеру бастапқы жүйенің пайдаланушысы мен каталогының деректерін, сондай-ақ деректерді бөліп алудағы деректерді сақтайды. Егер нұсқа сәйкес келсе, жүйе жүйені белсендіру және белсендіру күйі туралы ақпаратты әдепкі бойынша сақтайды.
Бұл параметрді таңдау жаңа пайдаланушыларды жасауды, осы тіркелгілер үшін құпия сөздерді есте сақтауды талап етпейді.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="249"/>
        <source>Please choose custom way to install, disk size less than 50G!</source>
        <translation>Орнату үшін реттелетін әдісті таңдауыңызды сұраймын, дискінің өлшемі 50 Г-нан кем!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="250"/>
        <source>Full disk encryption</source>
        <translation>Толық диск шифрлауы</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="251"/>
        <source>lvm</source>
        <translation>Логикалық көлем (LVM)</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="252"/>
        <source>Factory backup</source>
        <translation>Зауыттың резервтік көшірмесі</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="253"/>
        <source>save history date</source>
        <translation>Пайдаланушылар мен олардың деректерін сақтау</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/quickpartitionframe.cpp" line="384"/>
        <source>By default, SSDS serve as system disks and HDDS serve as user data disks</source>
        <translation>Әдепкі бойынша, SCD дискісі жүйелік диск ретінде және HDD дискісі пайдаланушы деректер дискісі ретінде пайдаланылады</translation>
    </message>
</context>
<context>
    <name>KInstaller::ImgFrame</name>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="211"/>
        <source>Select install way</source>
        <translation>Орнату жолын таңдау</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="212"/>
        <source>install from ghost</source>
        <translation>Ghost қызметінен орнату</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="213"/>
        <source>install form live</source>
        <translation>Live қызметінен орнату</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="127"/>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="214"/>
        <source>open file</source>
        <translation>Файлды ашу</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceImg/imgframe.cpp" line="215"/>
        <source>Next</source>
        <translation>Келесі</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallerMainWidget</name>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="252"/>
        <source>About to exit the installer, restart the computer.</source>
        <translation>Орнатудан шығып, компьютерді қайта іске қосыңыз.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="254"/>
        <source>About to exit the installer and return to the trial interface.</source>
        <translation>Орнатушыдан шығып, сынақ экранына оралу.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="307"/>
        <source>Installer is about to exit and the computer will be shut down.</source>
        <translation>Орнату шамамен шығып, компьютер өшіріледі.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="531"/>
        <source>back</source>
        <translation>Алдыңғы қадам</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="508"/>
        <source>quit</source>
        <translation>шығу</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installermainwidget.cpp" line="485"/>
        <source>keyboard</source>
        <translation>пернетақта</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallingFrame</name>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="151"/>
        <source>Log</source>
        <translation>Орнату журналдары</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="150"/>
        <source>The system is being installed, please do not turn off the computer</source>
        <translation>Жүйені орнату, компьютерді өшіру</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="185"/>
        <source>Install failed</source>
        <translation>Орнату жаңылысы</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="187"/>
        <source>Sorry, KylinOS cannot continue the installation. Please feed back the error log below so that we can better solve the problem for you.</source>
        <translation>KylinOS орнатуды жалғастырмайтыны үшін кешірім сұраймыз. Мәселені сіз үшін жақсырақ шеше алуымыз үшін төмендегі қате журналымен кері байланысыңызды сұраймыз.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/installingframe.cpp" line="194"/>
        <source>Restart</source>
        <translation>шығу</translation>
    </message>
</context>
<context>
    <name>KInstaller::InstallingOEMConfigFrame</name>
    <message>
        <location filename="../src/Installer_main/installingoemconfigframe.cpp" line="66"/>
        <source>Progressing system configuration</source>
        <translation>Жүйе конфигурациясы жүріп жатыр</translation>
    </message>
</context>
<context>
    <name>KInstaller::LanguageFrame</name>
    <message>
        <location filename="../src/plugins/KChoiceLanguage/languageframe.cpp" line="222"/>
        <source>Select Language</source>
        <translation>Тілді таңдау</translation>
    </message>
    <message>
        <location filename="../src/plugins/KChoiceLanguage/languageframe.cpp" line="223"/>
        <source>Next</source>
        <translation>Келесі</translation>
    </message>
</context>
<context>
    <name>KInstaller::LicenseFrame</name>
    <message>
        <location filename="../src/plugins/KyLicense/licenseframe.cpp" line="165"/>
        <source>Read License Agreement</source>
        <translation>Лицензиялық шартты оқу</translation>
    </message>
    <message>
        <location filename="../src/plugins/KyLicense/licenseframe.cpp" line="166"/>
        <source>I have read and agree to the terms of the agreement</source>
        <translation>Мен келісім шарттарымен таныстым және келістім</translation>
    </message>
    <message>
        <location filename="../src/plugins/KyLicense/licenseframe.cpp" line="168"/>
        <source>Next</source>
        <translation>Келесі</translation>
    </message>
</context>
<context>
    <name>KInstaller::MainPartFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="600"/>
        <source>Custom install</source>
        <translatorcomment>自定义安装</translatorcomment>
        <translation>Реттелетін орнату</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="351"/>
        <source>Coexist Install</source>
        <translation>Қатар орнату</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="419"/>
        <source>Confirm Custom Installation</source>
        <translation>Реттелетін орнатуды растау</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="312"/>
        <source>Formatted the whole disk</source>
        <translation>Бүкіл дискіні пішімдеп таңдаңыз.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="302"/>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="436"/>
        <source>Start Installation</source>
        <translation>Орнатуды бастау</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="314"/>
        <source>EFI partition #1 of %1 as vfat;
boot partition #2 of  %1 as ext4;
/ partition #3 of  %1 as ext4;
backup partition #4 of  %1 as ext4;
data partition #5 of  %1 as ext4;
swap partition #6 of  %1 as swap;
</source>
        <translation>% 1 Құрылғыдағы бірінші қалқа - Vfat орнатылатын EFI;
% 1 Құрылғыдағы екінші қалқа - ext4 үшін орнатылатын жүктеу;
% 1 Құрылғыдағы үшінші қалқа ext4 үшін орнатылатын /;
% 1 Құрылғыдағы 4-ші қалқа резервтік көшірме болып табылады, ол ext4 үшін орнатылады;
% 1 Құрылғыдағы 5-ші қалқа - ext4 үшін орнатылатын деректер;
% 1 Құрылғыдағы 6-шы қалқа своп болып табылады және свопқа орнатылады;</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="321"/>
        <source>boot partition #1 of %1 as vfat;
extend partition #2 of  %1 ;
/ partition #5 of  %1 as ext4;
backup partition #6 of  %1 as ext4;
data partition #7 of  %1 as ext4;
swap partition #8 of  %1 as swap;
</source>
        <translation>% 1 Құрылғыдағы бірінші қалқа - ext4 орнатылған жүктеу;
% 1 құрылғыдағы 2-бөлік ұзартылды;
% 1 Құрылғыдағы 5-ші қалқа ext4 деп орнатылатын /;
% 1 Құрылғыдағы 6-шы қалқа ext4 үшін орнатылатын резервтік көшірме болып табылады;
% 1 Құрылғыдағы 7-ші қалқа - ext4 үшін орнатылатын деректер;
% 1 Құрылғыдағы 8-ші қалқа своп болып табылады, ол свопқа орнатылады;</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="427"/>
        <source>Confirm the above operations</source>
        <translation>Жоғарыда көрсетілген операцияларды растау</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="510"/>
        <source>InvalidId
</source>
        <translation>бос
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="513"/>
        <source>Boot filesystem invalid
</source>
        <translation>Жүктеу қалқасының файл жүйесі қолжетімсіз
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="528"/>
        <source>the EFI partition size should be set between 256MB and 2GB
</source>
        <translation>EFI қалқасының өлшемі 256Мб және 2 Гб аралығында белгіленуі тиіс.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="540"/>
        <source>Root partition size is greater than 15GiB,
but Huawei machines require greater than 25GiB.
</source>
        <translation>Тамыр қалқасының өлшемі 15 ГиБ-тан артық, ал Huawei машиналары 25 ГиБ-тан артық болуы керек.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="519"/>
        <source>Boot partition too small
</source>
        <translation>Жүктеу қалқасы тым кішкентай
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="311"/>
        <source>Confirm Full Installation</source>
        <translation>Толық орнатуды растау</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="337"/>
        <source> set to data device;
</source>
        <translation> Деректер дискісі ретінде орнату</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="381"/>
        <source>bootloader type of Legacy should not contain EFI partition!</source>
        <translation>Жүйе мұра болған кезде оның құрамында EFI қалқалары бола алмайды!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="481"/>
        <source>BootLoader method %1 inconsistent with the disk partition table 
type %2.</source>
        <translation>% 1 жүктеу әдісі дискілік қалқалау кестесіне сәйкес келмейді% 2</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="484"/>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="489"/>
        <source>BootLoader method %1 inconsistent with the disk partition table 
type %2, cannot have efi partition.</source>
        <translation>% 1 жүктеу әдісі дискілік қалқалау кестесіне сәйкес келмейді% 2 және EFI қалқалауы қолданылмайды</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="516"/>
        <source>Boot partition not in the first partition
</source>
        <translation>Бәтеңкенің қалқасы бірінші қалқа болуы тиіс!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="522"/>
        <source>No boot partition
</source>
        <translation>Жүктеу қалқасы табылмады
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="525"/>
        <source>No Efi partition
</source>
        <translation>EFI қалқасы табылмады
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="531"/>
        <source>Only one EFI partition is allowed
</source>
        <translation>Тек бір ғана ЕФҚ бөлінуіне рұқсат етіледі
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="534"/>
        <source>Efi partition number invalid
</source>
        <translation>EFI қалқаларын нөмірлеу қолжетімсіз
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="537"/>
        <source>No root partition
</source>
        <translation>Түбір қалқасы табылмады.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="553"/>
        <source>No backup partition
</source>
        <translation>Резервтік қалпына келтіру қалқасы табылмады.
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="562"/>
        <source>This machine not support EFI partition
</source>
        <translation>Қазіргі уақытта бұл машина EFI бөлінуін қолдамайды!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="599"/>
        <source>Full install</source>
        <translation>Толық орнату</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="544"/>
        <source>Partition too small
</source>
        <translation>Қалқалар тым кішкентай
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="547"/>
        <source>BackUp partition too small
</source>
        <translation>Резервтік көшірмені қалпына келтіру қалқасы тым кішкентай
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="550"/>
        <source>Repeated mountpoint
</source>
        <translation>Бірнеше рет орнатылған қалқалар бар
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="565"/>
        <source>The filesystem type FAT16 or FAT32 is not fully-function filesystem,
except EFI partition, other partition not proposed</source>
        <translation>FAT16 және FAT32 толық емес файлдық жүйелер болып табылады.
ЕFI қалқаларын қоспағанда, қалқалар ұсынылмайды.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="598"/>
        <source>Choose Installation Method</source>
        <translation>Орнату әдісін таңдау</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="462"/>
        <location filename="../src/plugins/KPartition/mainpartframe.cpp" line="602"/>
        <source>Next</source>
        <translation>Келесі</translation>
    </message>
</context>
<context>
    <name>KInstaller::MiddleFrameManager</name>
    <message>
        <location filename="../src/plugins/PluginService/ui_unit/middleframemanager.cpp" line="44"/>
        <source>next</source>
        <translatorcomment>下一步</translatorcomment>
        <translation>Келесі</translation>
    </message>
</context>
<context>
    <name>KInstaller::ModifyPartitionFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="48"/>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="201"/>
        <source>unused</source>
        <translation>Пайдаланылмайды</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="187"/>
        <source>Format partition.</source>
        <translation>Осы қалқаны пішімдеп таңдаңыз.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="188"/>
        <source>OK</source>
        <translation>Сенімдісің бе</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="190"/>
        <source>Used to:</source>
        <translation>Үшін:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="191"/>
        <source>Modify Partition</source>
        <translation>Қалқаларды өзгерту</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="314"/>
        <source>The filesystem type of boot partition should be ext4 or ext3!</source>
        <translation>Жүктеу қалқасы тек ext4 немесе ext3 форматында болуы мүмкін!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="343"/>
        <source>The disk can only create one boot partition!</source>
        <translation>Бәтеңке қалқасын тек қана жасауға болады, ал кейінірек жасалған жүктеу қалқасы жарамсыз!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="371"/>
        <source>The disk can only create one root partition!</source>
        <translation>Түбір бөлінуді тек қана жасауға болады, ал кейінірек жасалған түбір қалқа жарамсыз!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="387"/>
        <source>Mount point starts with &apos;/&apos;</source>
        <translation>Бекіту нүктесі «/» дегеннен басталуы тиіс.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="472"/>
        <source>close</source>
        <translation>Тоқтату</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="196"/>
        <source>Mount point</source>
        <translation>Маунт нүктесі</translation>
    </message>
</context>
<context>
    <name>KInstaller::PrepareInstallFrame</name>
    <message>
        <location filename="../src/plugins/KPartition/prepareinstallframe.cpp" line="109"/>
        <source>Check it and click [Start Installation]</source>
        <translatorcomment>勾选后点击[开始安装]</translatorcomment>
        <translation>Құсбелгі қойып, [Орнатуды бастау] түймешігін басыңыз</translation>
    </message>
</context>
<context>
    <name>KInstaller::SlideShow</name>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="243"/>
        <source>Multi platform support, suitable for mainstream CPU platforms at home and abroad.</source>
        <translation>Ел ішінде және шет елдерде mainstream CPU платформаларына бейімделген көп платформалы қолдау.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="244"/>
        <source>Ukui desktop environment meets personalized needs.</source>
        <translation>Жеке қажеттіліктерді қанағаттандыру үшін UKUI жұмыс үстелі ортасы.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="245"/>
        <source>The system is updated and upgraded more conveniently.</source>
        <translation>Жүйені жаңарту, жаңарту ыңғайлырақ.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="246"/>
        <source>Security center, all-round protection of data security.</source>
        <translation>Барлық аспектілерде деректердің қауіпсіздігін қорғауға арналған қауіпсіздік орталығы.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="247"/>
        <source>Software store, featured software recommendation.</source>
        <translation>Бағдарламалық қамтамасыз ету дүкені, танымал бағдарламалық қамтамасыз ету ұсыныстары.</translation>
    </message>
    <message>
        <source>Kirin software store, featured software recommendation.</source>
        <translation type="obsolete">Kylin Software Store, танымал бағдарламалық жасақтама ұсыныстары.</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="248"/>
        <source>Provide technical support and professional after-sales service.</source>
        <translation>Техникалық қамтамасыз ету, сатудан кейінгі кәсіби қызмет көрсету.</translation>
    </message>
</context>
<context>
    <name>KInstaller::TableWidgetView</name>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="111"/>
        <source>This action will delect all partition,are you sure?</source>
        <translation>Бұрыннан бар қалқалар кестесі босатыла ма, оны босатасыз ба?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="294"/>
        <source>yes</source>
        <translation>be</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="294"/>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="327"/>
        <source>no</source>
        <translation>1000000</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="167"/>
        <source>Create partition table</source>
        <translation>Бөліп алынған кестені жасау</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="321"/>
        <source>freespace</source>
        <translation>бос уақыт</translation>
    </message>
</context>
<context>
    <name>KInstaller::TimeZoneFrame</name>
    <message>
        <location filename="../src/plugins/KTimeZone/timezoneframe.cpp" line="118"/>
        <source>Select Timezone</source>
        <translation>Уақыт белдеуін таңдау</translation>
    </message>
    <message>
        <location filename="../src/plugins/KTimeZone/timezoneframe.cpp" line="119"/>
        <source>Next</source>
        <translation>Келесі</translation>
    </message>
</context>
<context>
    <name>KInstaller::UserFrame</name>
    <message>
        <location filename="../src/plugins/KUserRegister/userframe.cpp" line="113"/>
        <source>Start Installation</source>
        <translation>Орнатуды бастау</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userframe.cpp" line="109"/>
        <source>Next</source>
        <translation>Келесі</translation>
    </message>
</context>
<context>
    <name>KInstaller::appcheckframe</name>
    <message>
        <location filename="../src/plugins/KInstallAPP/appcheckframe.cpp" line="95"/>
        <source>Start Installation</source>
        <translation>Орнатуды бастау</translation>
    </message>
    <message>
        <location filename="../src/plugins/KInstallAPP/appcheckframe.cpp" line="93"/>
        <source>Choose your app</source>
        <translation>Бағдарламаны таңдау</translation>
    </message>
    <message>
        <location filename="../src/plugins/KInstallAPP/appcheckframe.cpp" line="94"/>
        <source>Select the application you want to install to install with one click when using the system.</source>
        <translation>Жүйені пайдалану кезінде бір басу арқылы орнату үшін орнатқыңыз келетін бағдарламаны таңдаңыз.</translation>
    </message>
</context>
<context>
    <name>KInstaller::conferquestion</name>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="105"/>
        <source>confer_mainTitle</source>
        <translation>Қауіпсіздік мәселелері</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="106"/>
        <source>You can reset your password by answering a security question when you forget your password. Make sure you remember the answer.</source>
        <translation>Құпия сөзді қауіпсіздік сұрағына жауап беру арқылы ұмытып кетсеңіз, жауабыңызды есте сақтағаныңызға көз жеткізіңіз.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="109"/>
        <source>Which city were you born in?</source>
        <translation>Сіз туған қаланың атауы ма?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="110"/>
        <source>What is your childhood nickname?</source>
        <translation>Балалық шағындағы лақап атыңыз қандай болды?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="111"/>
        <source>Which middle school did you graduate from?</source>
        <translation>Сіз қандай орта мектепті бітірдіңіз?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="112"/>
        <source>What is your father&apos;s name?</source>
        <translation>Сіздің әкеңіздің аты бар ма?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="113"/>
        <source>What is your mother&apos;s name?</source>
        <translation>Сіздің анаңыздың аты бар ма?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="114"/>
        <source>When is your spouse&apos;s birthday?</source>
        <translation>Сіздің жұбайыңыздың туған күні ме?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="115"/>
        <source>What&apos;s your favorite animal?</source>
        <translation>Сүйікті жануарыңыз бар ма?</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="119"/>
        <source>question%1:</source>
        <translation>% 1 деген сұрақ:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="120"/>
        <source>answer%1:</source>
        <translation>% 1 деген жауап:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/conferquestionframe.cpp" line="126"/>
        <source>set later</source>
        <translation>Кейінірек орнату</translation>
    </message>
</context>
<context>
    <name>KInstaller::modeselect</name>
    <message>
        <location filename="../src/plugins/KUserRegister/modeselectframe.cpp" line="62"/>
        <source>Register account</source>
        <translation>Тіркелгі жасау</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/modeselectframe.cpp" line="68"/>
        <source>Register immediately</source>
        <translation>Қазір жасау</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/modeselectframe.cpp" line="76"/>
        <source>Register later</source>
        <translation>Кейінірек жасау</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/modeselectframe.cpp" line="77"/>
        <source>After the system installation is completed, an account will be created when entering the system.</source>
        <translation>Жүйені орнату аяқталғаннан кейін жүйеге кіргенде тіркелгі жасалады.</translation>
    </message>
</context>
<context>
    <name>KInstaller::userregisterwidiget</name>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="377"/>
        <source>Two password entries are inconsistent!</source>
        <translation>Екі құпия сөз жазуы сәйкес келмейді!</translation>
    </message>
    <message>
        <source>Your hostname only letters,numbers,underscore and hyphen are allowed, no more than 64 bits in length.</source>
        <translation type="obsolete">Хост аттары тек әріптерге, сандарға, астын сызуға және дефис жасауға мүмкіндік береді және 64 биттен аспайды.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="432"/>
        <source>Hostname should be more than 0 bits in length.</source>
        <translation>Хост атауының ұзындығы 1 таңбадан кем болмауы тиіс</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="434"/>
        <source>Hostname should be no more than 64 bits in length.</source>
        <translation>Хост атауының ұзындығы 64 таңбадан аспайды</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="436"/>
        <source>Hostname only letters,numbers,hyphen and dot notation are allowed</source>
        <translation>Хост атауларында тек әріптерді, сандарды, қосқыштарды немесе нүкте символдарын қамтуға рұқсат етіледі</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="438"/>
        <source>Hostname must start with a number or a letter</source>
        <translation>Хост атауының басында әріп немесе нөмір болуы тиіс</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="440"/>
        <source>Hostname must end with a number or a letter</source>
        <translation>Қабылдаушы тарап атауының із қалдыратын бөлігі әріп немесе нөмір болуы тиіс</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="442"/>
        <source>Hostname cannot have consecutive &apos; - &apos; and &apos; . &apos;</source>
        <translation>Хост атауларындағы дефис пен нүкте символдарын біріктіру мүмкін емес</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="444"/>
        <source>Hostname cannot have consecutive &apos; . &apos;</source>
        <translation>Хост атауында бірізді нүкте символдары болмайды</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="568"/>
        <source>Create User</source>
        <translation>Пайдаланушыны жасау</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="569"/>
        <source>username</source>
        <translation>Пайдаланушы аты</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="570"/>
        <source>hostname</source>
        <translation>хост атауы</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="571"/>
        <source>new password</source>
        <translation>құпиясөз</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="572"/>
        <source>enter the password again</source>
        <translation>Құпиясөзді қайтадан енгізу</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="573"/>
        <source>Automatic login on boot</source>
        <translation>Іске қосу кезінде автоматты түрде кіру</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="574"/>
        <source>Biometric [authentication] device detected / unified login support</source>
        <translation>Биометриялық [Аутентификация] құрылғысы / біріздендірілген кіру қолдауы анықталды,
Оны Биометриялық басқару құралдары / Параметрлер - Кіру параметрлері бөлімінде орнатуыңызды сұраймыз.</translation>
    </message>
</context>
<context>
    <name>KServer::EncryptSetFrame</name>
    <message>
        <source>password:</source>
        <translation type="obsolete">Құпия сөз:</translation>
    </message>
    <message>
        <source>confirm password:</source>
        <translation type="obsolete">Құпия сөзді растау:</translation>
    </message>
    <message>
        <source>Please keep your password properly.If you forget it,
you will not be able to access the disk data.</source>
        <translation type="obsolete">Құпия сөзді қауіпсіз сақтауыңызды сұраймын. Құпиясөзді ұмытып кетсеңіз, диск деректеріне қатынаса алмайсыз.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="110"/>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="111"/>
        <source>password</source>
        <translation>құпиясөз</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="152"/>
        <source>The password contains less than two types of characters</source>
        <translation>Құпия сөздің құрамында 2 таңбадан кем түрлері бар</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="172"/>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="173"/>
        <source>confirm password</source>
        <translation>Парольді растау</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="213"/>
        <source>The two passwords are different</source>
        <translation>Парольдер сәйкес келмейді</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="238"/>
        <source>Please keep your password properly.If you forget it,you will not be able to access the disk data.</source>
        <translation>Құпия сөзді қауіпсіз ұстауыңызды сұраймын, егер оны ұмытсаңыз, дискіге кіре алмайсыз.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="321"/>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="322"/>
        <source>OK</source>
        <translation>Сенімдісің бе</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="obsolete">Тоқтату</translation>
    </message>
    <message>
        <source>Two password entries are inconsistent!</source>
        <translation type="obsolete">Екі құпия сөз жазуы сәйкес келмейді!</translation>
    </message>
</context>
<context>
    <name>KServer::MessageBox</name>
    <message>
        <location filename="../src/plugins/PluginService/ui_unit/messagebox.cpp" line="183"/>
        <source>Cancel</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/ui_unit/messagebox.cpp" line="184"/>
        <source>OK</source>
        <translation>Сенімдісің бе</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/ui_unit/messagebox.cpp" line="276"/>
        <source>close</source>
        <translation>Тоқтату</translation>
    </message>
</context>
<context>
    <name>KeyboardWidget</name>
    <message>
        <location filename="../src/plugins/VirtualKeyboard/src/keyboardwidget.ui" line="29"/>
        <source>KeyboardWidget</source>
        <translation>ПернетақтаWidget</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="9"/>
        <source>wd</source>
        <translation>Western Digital</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="11"/>
        <source>seagate</source>
        <translation>Сигейт</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="13"/>
        <source>hitachi</source>
        <translation>Хитачи</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="15"/>
        <source>samsung</source>
        <translation>Samsung</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="17"/>
        <source>toshiba</source>
        <translation>Тошиба</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="19"/>
        <source>fujitsu</source>
        <translation>Фудзицу</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="21"/>
        <source>maxtor</source>
        <translation>Макстор</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="23"/>
        <source>IBM</source>
        <translation>IBM</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="25"/>
        <source>excelStor</source>
        <translation>Жоғарыға оңай</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="27"/>
        <source>lenovo</source>
        <translation>бірлестік</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="29"/>
        <source>other</source>
        <translation>басқалары</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/disktype_name.cpp" line="31"/>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="303"/>
        <source>Unknown</source>
        <translation>Беймәлім</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="80"/>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="314"/>
        <source>Freespace</source>
        <translation>бос уақыт</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="294"/>
        <source>kylin data partition</source>
        <translation>Деректер дискісі</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/partition_unit.cpp" line="301"/>
        <source>Swap partition</source>
        <translation>Бөлінуді ауыстыру</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/main.cpp" line="160"/>
        <source>Show debug informations</source>
        <translation>Қате туралы мәліметті көрсетеді</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="191"/>
        <source>Extended partition %1 has 
</source>
        <translation>Кеңейтілген бөліну% 1 Иә</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="193"/>
        <source>Create new partition %1,%2
</source>
        <translation>% 1,% 2 жаңа қалталарды жасау
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="196"/>
        <source>Create new partition %1,%2,%3
</source>
        <translation>% 1,% 2,% 3 жаңа қалқалар жасау
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="201"/>
        <source>Delete partition %1
</source>
        <translation>% 1 қалқасын өшіру
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="205"/>
        <source>Format %1 partition, %2
</source>
        <translation>% 1,% 2 қалқасын пішімдеу
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="208"/>
        <source>Format partition %1,%2,%3
</source>
        <translation>% 1, % 2, % 3 қалқаларын пішімдеу
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="213"/>
        <source>%1 partition mountPoint %2
</source>
        <translation>% 1 Бөліп алу нүктесі% 2
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="217"/>
        <source>New Partition Table %1
</source>
        <translation>% 1 жаңа бөліну кестесі
</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/partman/operationdisk.cpp" line="220"/>
        <source>Reset size %1 partition
</source>
        <translation>% 1 қалқасын ысыру</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/kcommand.cpp" line="21"/>
        <source>KCommand::m_cmdInstance is not init.</source>
        <translation>Командалық m_cmdInstance басталмаған</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/kcommand.cpp" line="58"/>
        <location filename="../src/plugins/PluginService/kcommand.cpp" line="90"/>
        <source>WorkingPath is not found. 
</source>
        <translation>Жұмыс каталогы табылмады</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/kcommand.cpp" line="82"/>
        <source>Shell file is empty, does not continue. 
</source>
        <translation>Қабықша файлы бос және тапсырма тоқтатылады.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="164"/>
        <source>device</source>
        <translation>жабдықпен жабдықта, 10</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="164"/>
        <source>type</source>
        <translation>түрі</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="164"/>
        <source>size</source>
        <translation>өлшемі</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="164"/>
        <source>mounted</source>
        <translation>Маунт нүктесі</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="165"/>
        <source>used</source>
        <translation>Пайдаланылған</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="165"/>
        <source>system</source>
        <translation>жүйе</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/tablewidgetview.cpp" line="165"/>
        <source>format</source>
        <translation>пішімі</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="69"/>
        <source>Memory allocation error when setting</source>
        <translation>Орнату кезінде жадты бөлу қатесі пайда болды</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="73"/>
        <source>Memory allocation error</source>
        <translation>Жадыны бөлу қатесі</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="75"/>
        <source>The password is the same as the old one</source>
        <translation>Бұл құпия сөз түпнұсқамен бірдей</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="77"/>
        <source>The password is a palindrome</source>
        <translation>Пароль - палиндром</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="79"/>
        <source>The password differs with case changes only</source>
        <translation>Құпиясөздер тек қана кейс өзгерістерін қамтиды</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="81"/>
        <source>The password is too similar to the old one</source>
        <translation>Пароль түпнұсқаға тым ұқсас</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="83"/>
        <source>The password contains the user name in some form</source>
        <translation>Құпия сөз пайдаланушы атының кейбір түрін қамтиды</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="85"/>
        <source>The password contains words from the real name of the user in some form</source>
        <translation>Құпия сөз пайдаланушының шын атының қандай да бір түрін қамтиды</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="87"/>
        <source>The password contains forbidden words in some form</source>
        <translation>Парольде тыйым салынған сөздің кейбір түрі бар</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="92"/>
        <source>The password contains too few digits</source>
        <translation>Құпия сөздің құрамында сандық таңбалар тым аз</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="97"/>
        <source>The password contains too few uppercase letters</source>
        <translation>Құпия сөзде бас әріптер тым аз</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="102"/>
        <source>The password contains too few lowercase letters</source>
        <translation>Құпия сөздің құрамында кіші әріптер тым аз</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="107"/>
        <source>The password contains too few non-alphanumeric characters</source>
        <translation>Құпия сөзде арнайы таңбалар тым аз</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="112"/>
        <source>The password is too short</source>
        <translation>Пароль тым қысқа</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="114"/>
        <source>The password is just rotated old one</source>
        <translation>Құпиясөз — ескі құпия сөздің бұрылысы</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="119"/>
        <source>The password does not contain enough character classes</source>
        <translation>Құпия сөзде таңба түрлері жеткілікті емес</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="124"/>
        <source>The password contains too many same characters consecutively</source>
        <translation>Құпия сөзде бірдей таңбалар тым көп</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="129"/>
        <source>The password contains too many characters of the same class consecutively</source>
        <translation>Құпия сөзде бір типті тым көп бірізді таңбалар бар</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="90"/>
        <source>The password contains less than %1 digits</source>
        <translation>Құпия сөз сандық таңбалардың% 1 санынан аз санды қамтиды</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="95"/>
        <source>The password contains less than %1 uppercase letters</source>
        <translation>Құпия сөздің құрамында% 1 саннан кіші әріптер бар</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="100"/>
        <source>The password contains less than %1 lowercase letters</source>
        <translation>Құпия сөздің құрамында% 1 кіші әріптері аз</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="105"/>
        <source>The password contains less than %1 non-alphanumeric characters</source>
        <translation>Құпия сөздің құрамында% 1-ден кем арнайы таңбалар бар</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="110"/>
        <source>The password is shorter than %1 characters</source>
        <translation>% 1 таңбадан кем құпия сөз</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="117"/>
        <source>The password contains less than %1 character classes</source>
        <translation>Құпия сөздің құрамында% 1 таңба түрлері аз</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="122"/>
        <source>The password contains more than %1 same characters consecutively</source>
        <translation>Құпия сөзде бірдей таңбалардың % 1-ден артық болуы</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="127"/>
        <source>The password contains more than %1 characters of the same class consecutively</source>
        <translation>Құпиясөз бір типтегі% 1 таңбадан артық таңбаларды қамтиды</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="132"/>
        <source>The password contains monotonic sequence longer than %1 characters</source>
        <translation>Құпия сөзде% 1 таңбадан артық біртұтас тізбек бар</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="134"/>
        <source>The password contains too long of a monotonic character sequence</source>
        <translation>Пароль шамадан тыс ұзын монотоникалық таңбалар тізбегін қамтиды</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="136"/>
        <source>No password supplied</source>
        <translation>Пароль берілмеген</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="138"/>
        <source>Cannot obtain random numbers from the RNG device</source>
        <translation>RNG кездейсоқ сандарды генерациялау құрылғыларынан кездейсоқ сандарды ала алмады</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="140"/>
        <source>Password generation failed - required entropy too low for settings</source>
        <translation>Пароль генерациялау сәтсіз аяқталды - орнату үшін қажетті энтропияға қол жеткізілмеді</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="143"/>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="145"/>
        <source>The password fails the dictionary check</source>
        <translation>Пароль істен шыққан сөздікті тексеру - Пароль сөздік сөздерге негізделген</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="149"/>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="151"/>
        <source>Unknown setting</source>
        <translation>Беймәлім параметр</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="155"/>
        <source>Bad integer value of setting</source>
        <translation>Қате бүтін сан параметрінің мәні</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="157"/>
        <source>Bad integer value</source>
        <translation>Қате бүтін сан параметрінің мәні</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="161"/>
        <source>Setting %s is not of integer type</source>
        <translation>% s параметрі бүтін сан емес</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="163"/>
        <source>Setting is not of integer type</source>
        <translation>Параметр бүтін сан түрі емес</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="167"/>
        <source>Setting %s is not of string type</source>
        <translation>% s параметрі жол түрі емес</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="169"/>
        <source>Setting is not of string type</source>
        <translation>Параметр жол түрі емес</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="171"/>
        <source>Opening the configuration file failed</source>
        <translation>Баптау файлы ашылмады</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="173"/>
        <source>The configuration file is malformed</source>
        <translation>Баптау файлының пішімі дұрыс емес</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="175"/>
        <source>Fatal failure</source>
        <translation>Өлімге әкеліп соққан қате</translation>
    </message>
    <message>
        <location filename="../src/plugins/PluginService/sysInfo/kpasswordcheck.cpp" line="177"/>
        <source>Unknown error</source>
        <translation>Беймәлім қате</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="440"/>
        <source>unused</source>
        <translation>Пайдаланылмайды</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/createpartitionframe.cpp" line="442"/>
        <location filename="../src/plugins/KPartition/modifypartitionframe.cpp" line="203"/>
        <source>kylin-data</source>
        <translation>Пайдаланушы деректерінің бөлінулері</translation>
    </message>
</context>
<context>
    <name></name>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="309"/>
        <source>Two password entries are inconsistent!</source>
        <translation>Екі құпия сөз жазуы сәйкес келмейді!</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="72"/>
        <source>confirm password:</source>
        <translation>Құпия сөзді растау:</translation>
    </message>
    <message>
        <location filename="../src/Installer_main/uilt/slidershow.cpp" line="70"/>
        <source>Kirin software store, featured software recommendation.</source>
        <translation>Kylin Software Store, танымал бағдарламалық жасақтама ұсыныстары.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="71"/>
        <source>password:</source>
        <translation>Құпия сөз:</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="106"/>
        <source>Please keep your password properly.If you forget it,
you will not be able to access the disk data.</source>
        <translation>Құпия сөзді қауіпсіз сақтауыңызды сұраймын. Құпиясөзді ұмытып кетсеңіз, диск деректеріне қатынаса алмайсыз.</translation>
    </message>
    <message>
        <location filename="../src/plugins/KPartition/frames/encryptsetframe.cpp" line="254"/>
        <source>close</source>
        <translation>Тоқтату</translation>
    </message>
    <message>
        <location filename="../src/plugins/KUserRegister/userregisterframe.cpp" line="408"/>
        <source>Your hostname only letters,numbers,underscore and hyphen are allowed, no more than 64 bits in length.</source>
        <translation>Хост аттары тек әріптерге, сандарға, астын сызуға және дефис жасауға мүмкіндік береді және 64 биттен аспайды.</translation>
    </message>
</context>
</TS>
