<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>Search</name>
    <message>
        <location filename="../search.cpp" line="13"/>
        <location filename="../search.cpp" line="121"/>
        <source>Search</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="145"/>
        <source>Create index</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Search/Create index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="146"/>
        <source>Creating index can help you getting results quickly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="165"/>
        <source>Default web searching engine</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Search/Default web searching engine</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="169"/>
        <source>baidu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="170"/>
        <source>sougou</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="171"/>
        <source>360</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="179"/>
        <source>Block Folders</source>
        <translation type="unfinished"></translation>
        <extra-contents_path>/Search/Block Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="184"/>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="225"/>
        <source>Choose folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="338"/>
        <source>delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="393"/>
        <source>Directories</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="394"/>
        <source>select blocked folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="395"/>
        <source>Select</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="396"/>
        <source>Position: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="397"/>
        <source>FileName: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="398"/>
        <source>FileType: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="399"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="415"/>
        <location filename="../search.cpp" line="419"/>
        <location filename="../search.cpp" line="423"/>
        <location filename="../search.cpp" line="427"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="415"/>
        <source>Add blocked folder failed, choosen path is empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="419"/>
        <source>Add blocked folder failed, it is not in home path!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="423"/>
        <source>Add blocked folder failed, its parent dir is exist!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../search.cpp" line="427"/>
        <source>Add blocked folder failed, it has been already blocked!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
