<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>Search</name>
    <message>
        <location filename="../search.cpp" line="13"/>
        <location filename="../search.cpp" line="121"/>
        <source>Search</source>
        <translation>ᠪᠦᠬᠦ᠌ ᠪᠠᠢᠳᠠᠯ᠎ᠤᠨ ᠬᠠᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="145"/>
        <source>Create index</source>
        <translation>ᠬᠡᠯᠬᠢᠶᠡᠰᠦ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
        <extra-contents_path>/Search/Create index</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="146"/>
        <source>Creating index can help you getting results quickly.</source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠭᠰᠡᠨ᠎ᠦ ᠳᠠᠷᠠᠭ᠎ᠠ ᠬᠠᠢᠯᠲᠠ᠎ᠶᠢᠨ ᠦᠷ᠎ᠡ ᠳ᠋ᠦᠩ᠎ᠢ ᠬᠤᠷᠳᠤᠨ ᠤᠯᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="165"/>
        <source>Default web searching engine</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠢᠨᠲᠸᠷᠨ᠋ᠸᠲ᠎ᠦᠨ ᠡᠷᠢᠯᠳᠡ ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
        <extra-contents_path>/Search/Default web searching engine</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="169"/>
        <source>baidu</source>
        <translation>ᠪᠠᠢ ᠳ᠋ᠦ᠋</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="170"/>
        <source>sougou</source>
        <translation>ᠰᠸᠤ ᠭᠸᠦ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="171"/>
        <source>360</source>
        <translation>360</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="179"/>
        <source>Block Folders</source>
        <translation>ᠢᠯᠭᠠᠵᠤ ᠭᠠᠷᠭᠠᠭᠰᠠᠨ ᠴᠤᠮᠤᠭ</translation>
        <extra-contents_path>/Search/Block Folders</extra-contents_path>
    </message>
    <message>
        <location filename="../search.cpp" line="184"/>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation>ᠬᠠᠢᠯᠲᠠ᠎ᠪᠠᠷ ᠳᠠᠷᠠᠭᠠᠬᠢ ᠴᠤᠮᠤᠭ᠎ᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ᠌ ᠦᠬᠡᠢ ᠂ ᠨᠡᠮᠡᠬᠦ᠌ ᠪᠤᠶᠤ ᠬᠠᠰᠤᠬᠤ᠎ᠪᠠᠷ ᠳᠠᠮᠵᠢᠭᠤᠯᠤᠨ ᠢᠯᠭᠠᠵᠤ ᠭᠠᠷᠭᠠᠭᠰᠠᠨ ᠴᠤᠮᠤᠭ᠎ᠤᠨ ᠪᠠᠢᠷᠢ᠎ᠶᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="225"/>
        <source>Choose folder</source>
        <translation>ᠨᠡᠮᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="338"/>
        <source>delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="393"/>
        <source>Directories</source>
        <translation>ᠴᠤᠮᠤᠭ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="394"/>
        <source>select blocked folder</source>
        <translation>ᠢᠯᠭᠠᠵᠤ ᠭᠠᠷᠭᠠᠭᠰᠠᠨ ᠴᠤᠮᠤᠭ᠎ᠢ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="395"/>
        <source>Select</source>
        <translation>ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="396"/>
        <source>Position: </source>
        <translation>ᠪᠠᠢᠷᠢ ᠄ </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="397"/>
        <source>FileName: </source>
        <translation>ᠹᠠᠢᠯ᠎ᠤᠨ ᠨᠡᠷ᠎ᠡ ᠄ </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="398"/>
        <source>FileType: </source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ ᠮᠠᠶᠢᠭ </translation>
    </message>
    <message>
        <location filename="../search.cpp" line="399"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="415"/>
        <location filename="../search.cpp" line="419"/>
        <location filename="../search.cpp" line="423"/>
        <location filename="../search.cpp" line="427"/>
        <source>Warning</source>
        <translation type="unfinished">ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="415"/>
        <source>Add blocked folder failed, choosen path is empty!</source>
        <translation>ᠨᠡᠮᠡᠯᠳᠡ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠵᠠᠮ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="419"/>
        <source>Add blocked folder failed, it is not in home path!</source>
        <translation>ᠨᠡᠮᠡᠯᠳᠡ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠨᠡᠮᠡᠭᠰᠡᠨ ᠵᠠᠮ ᠲᠤᠰ ᠭᠠᠷᠴᠠᠭ᠎ᠤᠨ ᠳᠤᠤᠷ᠎ᠠ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="423"/>
        <source>Add blocked folder failed, its parent dir is exist!</source>
        <translation>ᠨᠡᠮᠡᠯᠳᠡ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠡᠬᠢ ᠭᠠᠷᠴᠠᠭ᠎ᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠨᠡᠮᠡᠭᠰᠡᠨ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../search.cpp" line="427"/>
        <source>Add blocked folder failed, it has been already blocked!</source>
        <translation>ᠨᠡᠮᠡᠯᠳᠡ ᠢᠯᠠᠭᠳᠠᠪᠠ ᠂ ᠡᠨᠡ ᠴᠤᠮᠤᠭ᠎ᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠨᠡᠮᠡᠴᠢᠬᠡᠭᠰᠡᠨ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
</context>
</TS>
