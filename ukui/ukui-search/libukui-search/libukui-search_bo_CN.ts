<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>QObject</name>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="112"/>
        <source>Content index incomplete.</source>
        <translation>ནང་དོན་གྱི་སྟོན་གྲངས་ཆ་མི་ཚང་བ།</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-search-task.cpp" line="96"/>
        <source>Warning, Can not find home path.</source>
        <translation>homeལམ་བུ་དཀར་ཆག་རྙེད་ཐབས་མེད།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppMatch</name>
    <message>
        <source>Application Description:</source>
        <translation type="vanished">应用描述：</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppSearch</name>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="306"/>
        <source>Application Description:</source>
        <translation>ཉེར་སྤྱོད་གོ་རིམ་གྱི་གསལ་བཤད།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppSearchPlugin</name>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="11"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="182"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="12"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="183"/>
        <source>Add Shortcut to Desktop</source>
        <translation>ཅོག་ངོས་སུ་མྱུར་འཐེབ་སྣོན་པ།</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="13"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="184"/>
        <source>Add Shortcut to Panel</source>
        <translation>ལས་འགན་གྱི་སྒྲོམ་ཐོག་མགྱོགས་མྱུར་གྱི་བྱེད་ཐབས་གསར་སྣོན་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="14"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="185"/>
        <source>Install</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="38"/>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="43"/>
        <source>Applications Search</source>
        <translation>ཉེར་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../libsearch/appsearch/app-search-plugin.cpp" line="123"/>
        <source>Application</source>
        <translation>ཉེར་སྤྱོད་བྱ་རིམ།</translation>
    </message>
    <message>
        <source>Application Description:</source>
        <translation type="vanished">应用描述：</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::AppSearchTask</name>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/app-search-task.cpp" line="21"/>
        <source>Application</source>
        <translation>ཉེར་སྤྱོད་བྱ་རིམ།</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/app-search-task.cpp" line="26"/>
        <source>Application search.</source>
        <translation>ཉེར་སྤྱོད་གོ་རིམ་འཚོལ་བཤེར།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::DirSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="224"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="364"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="225"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="365"/>
        <source>Open path</source>
        <translation>ཡིག་ཆའི་ཐབས་ལམ་འབྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="226"/>
        <source>Copy Path</source>
        <translation>ཡིག་ཆའི་ལམ་ཐིག་དཔར་བ།</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="245"/>
        <source>Dir Search</source>
        <translation>ཡིག་ཆའི་དཀར་ཆག།</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="305"/>
        <source>Directory</source>
        <translation>དཀར་ཆག</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="240"/>
        <source>Dir search.</source>
        <translation>དཀར་ཆག་འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <source>directory</source>
        <translation type="vanished">目录</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="342"/>
        <source>Path</source>
        <translation>ལམ་ཐིག</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="354"/>
        <source>Last time modified</source>
        <translation>ཐེངས་སྔ་མའི་བཟོ་བཅོས་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="366"/>
        <source>Copy path</source>
        <translation>ཡིག་ཆའི་ལམ་ཐིག་དཔར་བ།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileContengSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="407"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="624"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="408"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="625"/>
        <source>Open path</source>
        <translation>ཡིག་ཆའི་ཐབས་ལམ་འབྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="409"/>
        <source>Copy Path</source>
        <translation>ཡིག་ཆའི་ལམ་ཐིག་དཔར་བ།</translation>
    </message>
    <message>
        <source>File Content Search</source>
        <translation type="vanished">文本内容搜索</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="423"/>
        <source>File content search.</source>
        <translation>ཡིག་ཆའི་ནང་དོན་འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="428"/>
        <source>File content search</source>
        <translation>ཡིག་ཆའི་ནང་དོན་འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="486"/>
        <source>OCR</source>
        <translation>OCR</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="491"/>
        <source>File</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="602"/>
        <source>Path</source>
        <translation>ལམ་ཐིག</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="614"/>
        <source>Last time modified</source>
        <translation>ཐེངས་སྔ་མའི་བཟོ་བཅོས་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="626"/>
        <source>Copy path</source>
        <translation>ཡིག་ཆའི་ལམ་ཐིག་དཔར་བ།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileContentSearchTask</name>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="40"/>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="65"/>
        <source>File Content</source>
        <translation>ཡིག་ཆའི་ནང་དོན།</translation>
    </message>
    <message>
        <location filename="../../libsearch/searchinterface/searchtasks/file-content-search-task.cpp" line="45"/>
        <source>File Content Search</source>
        <translation>ཡིག་ཆའི་ནང་དོན་འཚོལ་ཞིབ།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FileSearchPlugin</name>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="14"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="164"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="15"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="165"/>
        <source>Open path</source>
        <translation>ཡིག་ཆའི་ཐབས་ལམ་འབྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="16"/>
        <source>Copy Path</source>
        <translation>ཡིག་ཆའི་ལམ་ཐིག་དཔར་བ།</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="35"/>
        <source>File Search</source>
        <translation>ཡིག་ཆ་འཚོལ་བཤེར།</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="30"/>
        <source>File search.</source>
        <translation>ཡིག་ཆ་འཚོལ་བཤེར།</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="77"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="189"/>
        <source>Yes</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="79"/>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="191"/>
        <source>Can not get a default application for opening %1.</source>
        <translation>%1གི་བཀོལ་སྤྱོད་མ་རྙེད།</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="105"/>
        <source>File</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="142"/>
        <source>Path</source>
        <translation>ལམ་ཐིག</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="154"/>
        <source>Last time modified</source>
        <translation>ཐེངས་སྔ་མའི་བཟོ་བཅོས་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/file-search-plugin.cpp" line="166"/>
        <source>Copy path</source>
        <translation>ཡིག་ཆའི་ལམ་ཐིག་དཔར་བ།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MailSearch</name>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="337"/>
        <source>From</source>
        <translation>བསྐུར་མཁན།</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="338"/>
        <source>Time</source>
        <translation>དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="339"/>
        <source>To</source>
        <translation>འབྱོར་མཁན།</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="340"/>
        <source>Cc</source>
        <translation>བཤུ་སྐྱེལ་བྱེད་མཁན།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MailSearchPlugin</name>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="28"/>
        <source>open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="37"/>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="42"/>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="47"/>
        <source>Mail Search</source>
        <translation>སྦྲག་རྫས་འཚོལ་བཤེར།</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="94"/>
        <source>Mail</source>
        <translation>སྦྲག་རྫས།</translation>
    </message>
    <message>
        <location filename="../../libsearch/mailsearch/mail-search-plugin.cpp" line="231"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::NoteSearch</name>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="190"/>
        <source>Note Description:</source>
        <translatorcomment>便签内容：</translatorcomment>
        <translation>སྟབས་བདེའི་ནང་དོན།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::NoteSearchPlugin</name>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="12"/>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="128"/>
        <source>Open</source>
        <translatorcomment>打开</translatorcomment>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="31"/>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="106"/>
        <source>Note Search</source>
        <translatorcomment>便签</translatorcomment>
        <translation>སྟབས་བདེ་བྱང་བུ།</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="26"/>
        <source>Note Search.</source>
        <translatorcomment>便签.</translatorcomment>
        <translation>སྟབས་བདེ་བྱང་བུ།</translation>
    </message>
    <message>
        <location filename="../../libsearch/notesearch/note-search-plugin.cpp" line="80"/>
        <source>Application</source>
        <translatorcomment>应用</translatorcomment>
        <translation>ཉེར་སྤྱོད་བྱ་རིམ།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchManager</name>
    <message>
        <location filename="../../libsearch/index/search-manager.cpp" line="68"/>
        <source>Path:</source>
        <translation>ལམ་ཐིག</translation>
    </message>
    <message>
        <location filename="../../libsearch/index/search-manager.cpp" line="69"/>
        <source>Modified time:</source>
        <translation>བཟོ་བཅོས་བརྒྱབ་པའི་དུས་ཚོད་ནི།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchTaskPluginManager</name>
    <message>
        <location filename="../../libsearch/pluginmanage/search-task-plugin-manager.cpp" line="68"/>
        <location filename="../../libsearch/pluginmanage/search-task-plugin-manager.cpp" line="82"/>
        <source>plugin type: %1, is disabled!</source>
        <translation>ལྷུ་ལག་གི་རིགས་ནི།%1བཀོལ་མི་ཆོག</translation>
    </message>
    <message>
        <location filename="../../libsearch/pluginmanage/search-task-plugin-manager.cpp" line="74"/>
        <location filename="../../libsearch/pluginmanage/search-task-plugin-manager.cpp" line="87"/>
        <source>plugin type: %1, is not registered!</source>
        <translation>ལྷུ་ལག་གི་རིགས།%1་ཐོ་འགོད་བྱས་མེད།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SettingsSearchPlugin</name>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="15"/>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="128"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="35"/>
        <source>Settings Search</source>
        <translation>བཀོད་སྒྲིག་རྣམ་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="30"/>
        <source>Settings search.</source>
        <translation>སྒྲིག་བཀོད་འཚོལ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../libsearch/settingsearch/settings-search-plugin.cpp" line="117"/>
        <source>Settings</source>
        <translation>སྒྲིག་བཀོད།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::UkuiSearchTaskPrivate</name>
    <message>
        <location filename="../../libsearch/searchinterface/ukui-search-task.cpp" line="91"/>
        <source>Current task uuid error or an unregistered plugin is used!</source>
        <translation>མིག་སྔར་ལས་འགནuuid་ནོར་འཁྲུལ་བྱུང་བའམ་ཡང་ན་ཐོ་འགོད་བྱས་མེད་པའི་ལྷུ་ལག་ཞིག་བཀོལ་སྤྱོད་བྱས།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::WebSearchPlugin</name>
    <message>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="10"/>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="114"/>
        <source>Start browser search</source>
        <translation>དྲ་ངོས་ནས་འཚོལ་ཞིབ་བྱེད་འགོ་འཛུགས་དགོས།</translation>
    </message>
    <message>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="22"/>
        <location filename="../../libsearch/websearch/web-search-plugin.cpp" line="27"/>
        <source>Web Page</source>
        <translation>དྲ་ངོས།</translation>
    </message>
</context>
</TS>
