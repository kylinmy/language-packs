<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>UkuiSearch::BestListWidget</name>
    <message>
        <location filename="../../frontend/view/best-list-view.cpp" line="312"/>
        <source>Best Matches</source>
        <translation>སྙོམས་སྒྲིག་ལེགས་ཤོས།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::CreateIndexAskDialog</name>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="41"/>
        <source>ukui-search</source>
        <translation>འཚོལ་བཤེར།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="70"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="94"/>
        <source>Creating index can help you getting results quickly, whether to create or not?</source>
        <translation>དཀར་ཆག་གསར་བཟོ་བྱས་ཚེ་འཚོལ་བྱ་མྱུར་དུ་རྙེད་ཐུབ། གསར་བཟོ་བྱའམ།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="105"/>
        <source>Don&apos;t remind</source>
        <translation>ད་ནས་གསལ་བརྡ་མི་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="116"/>
        <source>No</source>
        <translation>མིན།(N)</translation>
    </message>
    <message>
        <location filename="../../frontend/control/create-index-ask-dialog.cpp" line="118"/>
        <source>Yes</source>
        <translation>རེད།(Y)</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::FolderListItem</name>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="538"/>
        <source>Delete the folder out of blacklist</source>
        <translation>བསུབ་པ།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::MainWindow</name>
    <message>
        <location filename="../../frontend/mainwindow.cpp" line="69"/>
        <source>ukui-search</source>
        <translation>འཚོལ་བཤེར།</translation>
    </message>
    <message>
        <location filename="../../frontend/mainwindow.cpp" line="76"/>
        <source>Global Search</source>
        <translation>འཚོལ་བཤེར།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SearchLineEdit</name>
    <message>
        <location filename="../../frontend/control/search-line-edit.cpp" line="56"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::SettingsWidget</name>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="35"/>
        <source>ukui-search-settings</source>
        <translation>འཚོལ་བཤེར།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="81"/>
        <location filename="../../frontend/control/settings-widget.cpp" line="299"/>
        <location filename="../../frontend/control/settings-widget.cpp" line="503"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="108"/>
        <source>&lt;h2&gt;Settings&lt;/h2&gt;</source>
        <translation>&lt;h2&gt; སྒྲིག་འགོད། &lt;/h2&gt;</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="113"/>
        <source>&lt;h3&gt;Index State&lt;/h3&gt;</source>
        <translation>&lt;h3&gt;གསལ་བང་གི་རྣམ་པ།&lt;/h3&gt;</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="115"/>
        <location filename="../../frontend/control/settings-widget.cpp" line="117"/>
        <source>...</source>
        <translation>...</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="125"/>
        <source>&lt;h3&gt;File Index Settings&lt;/h3&gt;</source>
        <translation>&lt;h3&gt;ཡིག་ཆའི་གསལ་བང་སྒྲིག་འགོད། &lt;/h3&gt;</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="127"/>
        <source>Following folders will not be searched. You can set it by adding and removing folders.</source>
        <translation>འཚོལ་ཞིབ་ཀྱིས་གཤམ་གྱི་ཡིག་ཆའི་ཁུག་མ་ལ་ལྟ་ཞིབ་མི་བྱེད།ཡིག་ཁུག་ཁ་སྣོན་དང་བསུབ་པ་བརྒྱུད་ནས་ཡིག་ཆའི་དཀར་ཆག་སྒྲིག་བཀོད་བྱེད་ཆོག</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="136"/>
        <source>Add ignored folders</source>
        <translation>ཡིག་ཁུག་ནས་མིང་ཐོ་ནག་པོའི་བར་དུ་སྣོན་རོགས།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="157"/>
        <source>&lt;h3&gt;Search Engine Settings&lt;/h3&gt;</source>
        <translation>&lt;h3&gt;འཚོལ་བཤེར་ཆས་སྒྲིག་འགོད།&lt;/h3&gt;</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="159"/>
        <source>Please select search engine you preferred.</source>
        <translation>མཉམ་འབྲེལ་དྲ་རྒྱའི་འཚོལ་བཤེར་འཕྲུལ་ཆས་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="172"/>
        <source>baidu</source>
        <translation>པའེ་ཏུའུ།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="174"/>
        <source>sougou</source>
        <translation>སོའོ་གོའུ།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="176"/>
        <source>360</source>
        <translation>360</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="299"/>
        <source>Whether to delete this directory?</source>
        <translation>དཀར་ཆག་འདི་བསུབ་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="300"/>
        <source>Yes</source>
        <translation>རེད།(Y)</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="301"/>
        <source>No</source>
        <translation>མིན།(N)</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="359"/>
        <source>Creating ...</source>
        <translation>གསལ་བང་འདྲེན་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="362"/>
        <source>Done</source>
        <translation>འདྲེན་བྱང་ལེགས་འགྲུབ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="370"/>
        <source>Index Entry: %1</source>
        <translation>གསལ་བྱང་ཚན་པ།:%1</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="416"/>
        <source>Directories</source>
        <translation>ཡིག་སྣོད།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="417"/>
        <source>select blocked folder</source>
        <translation>བཀག་སྡོམ་བྱས་པའི་ཡིག་སྣོད་གདམ་གསེས།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="418"/>
        <source>Select</source>
        <translation>བདམས་ཐོན་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="419"/>
        <source>Position: </source>
        <translation>གོ་གནས་ནི། </translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="420"/>
        <source>FileName: </source>
        <translation>ཡིག་ཆའི་མིང་ནི། </translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="421"/>
        <source>FileType: </source>
        <translation>ཡིག་ཆའི་རིགས་དབྱིབས་ནི། </translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="422"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="487"/>
        <source>Choosen path is Empty!</source>
        <translation>བདམས་ཟིན་པའི་ལམ་ཐིག་མི་འདུག</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="491"/>
        <source>Choosen path is not in &quot;home&quot;!</source>
        <translation>ཁྱིམ་གྱི་དཀར་ཆག་ནང་གི་ཡིག་སྣོད་འདེམ་རོགས།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="495"/>
        <source>Its&apos; parent folder has been blocked!</source>
        <translation>རིམ་པ་གོང་མའི་ཡིག་སྣོད་གབ་ཟིན།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="499"/>
        <source>Set blocked folder failed!</source>
        <translation>བཀག་སྡོམ་བྱས་པའི་ཡིག་སྣོད་ལ་ཕམ་ཉེས་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../../frontend/control/settings-widget.cpp" line="504"/>
        <source>OK</source>
        <translation>འགྲིགས།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::UkuiSearchGui</name>
    <message>
        <location filename="../../frontend/ukui-search-gui.cpp" line="88"/>
        <source>Quit ukui-search application</source>
        <translation>ཉེར་སྤྱོད་གོ་རིམ་ལས་ཕྱིར་འཐེན་བྱ།</translation>
    </message>
    <message>
        <location filename="../../frontend/ukui-search-gui.cpp" line="91"/>
        <source>Show main window</source>
        <translation>སྒེའུ་ཁུང་གཙོ་བོ་མངོན་པ།</translation>
    </message>
</context>
<context>
    <name>UkuiSearch::WebSearchWidget</name>
    <message>
        <location filename="../../frontend/view/web-search-view.cpp" line="152"/>
        <source>Web Page</source>
        <translation>དྲ་ངོས་འཚོལ་བཤེར།</translation>
    </message>
</context>
</TS>
