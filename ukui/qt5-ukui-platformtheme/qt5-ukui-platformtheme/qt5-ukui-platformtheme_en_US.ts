<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>KyFileDialog</name>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="146"/>
        <source>Go Back</source>
        <translation>Go Back</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="153"/>
        <source>Go Forward</source>
        <translation>Go Forward</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="159"/>
        <source>Cd Up</source>
        <translation>Cd Up</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="166"/>
        <source>Search</source>
        <translation>Search</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="174"/>
        <source>View Type</source>
        <translation>View Type</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="184"/>
        <source>Sort Type</source>
        <translation>Sort Type</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="777"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="780"/>
        <source>Open</source>
        <translation>Open</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="781"/>
        <location filename="../widget/kyfiledialog.cpp" line="790"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="785"/>
        <source>Save as</source>
        <translation>Save as</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="787"/>
        <source>New Folder</source>
        <translation>New Folder</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="789"/>
        <source>Save</source>
        <translation>Save</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="824"/>
        <location filename="../widget/kyfiledialog.cpp" line="826"/>
        <source>Directories</source>
        <translation>Directories</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1010"/>
        <source>Warning</source>
        <translation>Warning</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1010"/>
        <source>exist, are you sure replace?</source>
        <translation>exist, are you sure replace?</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1013"/>
        <source>ok</source>
        <translation>ok</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1022"/>
        <source>no</source>
        <translation>no</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1452"/>
        <source>NewFolder</source>
        <translation>NewFolder</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1601"/>
        <source>Undo</source>
        <translation>Undo</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1608"/>
        <source>Redo</source>
        <translation>Redo</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1805"/>
        <source>warn</source>
        <translation>warn</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1805"/>
        <source>This operation is not supported.</source>
        <translation>This operation is not supported.</translation>
    </message>
</context>
<context>
    <name>KyFileDialogHelper</name>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1924"/>
        <source>Open File</source>
        <translation>Open File</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1925"/>
        <source>Save File</source>
        <translation>Save File</translation>
    </message>
    <message>
        <location filename="../widget/kyfiledialog.cpp" line="1938"/>
        <source>All Files (*)</source>
        <translation>All Files (*)</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <location filename="../../test/highlighted-icon-button/mainwindow.ui"/>
        <location filename="../../test/mps-style-application/mainwindow.ui"/>
        <location filename="../../test/system-settings/mainwindow.ui"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>test open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>selected uri</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>test show</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>test exec</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>test save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../test/filedialog/mainwindow.ui"/>
        <source>test static open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../test/highlighted-icon-button/mainwindow.ui"/>
        <location filename="../../test/mps-style-application/mainwindow.ui"/>
        <source>PushButton</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../test/highlighted-icon-button/mainwindow.ui"/>
        <source>use auto highlight icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../test/highlighted-icon-button/mainwindow.ui"/>
        <location filename="../../test/mps-style-application/mainwindow.ui"/>
        <source>...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../test/highlighted-icon-button/mainwindow.ui"/>
        <source>highlightOnly</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../test/highlighted-icon-button/mainwindow.ui"/>
        <source>bothDefaultAndHighlight</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../test/mps-style-application/mainwindow.ui"/>
        <source>RadioButton</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../test/system-settings/mainwindow.ui"/>
        <source>style</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../test/system-settings/mainwindow.ui"/>
        <source>icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../test/system-settings/mainwindow.ui"/>
        <source>menu opacity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../test/system-settings/mainwindow.ui"/>
        <source>font</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../widget/message-box.h" line="190"/>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/message-box.h" line="192"/>
        <source>Incompatible Qt Library Error</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QDialogButtonBox</name>
    <message>
        <location filename="../widget/message-box.cpp" line="405"/>
        <location filename="../widget/message-box.cpp" line="1075"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMessageBox</name>
    <message>
        <location filename="../widget/message-box.cpp" line="461"/>
        <location filename="../widget/message-box.cpp" line="866"/>
        <location filename="../widget/message-box.cpp" line="1376"/>
        <source>Show Details...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../widget/message-box.cpp" line="866"/>
        <location filename="../widget/message-box.cpp" line="1376"/>
        <source>Hide Details...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="19"/>
        <source>File Name</source>
        <translation>File Name</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="23"/>
        <source>Modified Date</source>
        <translation>Modified Date</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="27"/>
        <source>File Type</source>
        <translation>File Type</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="31"/>
        <source>File Size</source>
        <translation>File Size</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="35"/>
        <source>Original Path</source>
        <translation>Original Path</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="44"/>
        <source>Descending</source>
        <translation>Descending</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="49"/>
        <source>Ascending</source>
        <translation>Ascending</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="55"/>
        <source>Use global sorting</source>
        <translation>Use global sorting</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="75"/>
        <source>List View</source>
        <translation>List View</translation>
    </message>
    <message>
        <location filename="../widget/ui_kyfiledialog.cpp" line="76"/>
        <source>Icon View</source>
        <translation>Icon View</translation>
    </message>
</context>
<context>
    <name>UKUI::TabWidget::DefaultSlideAnimatorFactory</name>
    <message>
        <location filename="../../libqt5-ukui-style/animations/tabwidget/ukui-tabwidget-default-slide-animator-factory.h" line="49"/>
        <source>Default Slide</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libqt5-ukui-style/animations/tabwidget/ukui-tabwidget-default-slide-animator-factory.h" line="50"/>
        <source>Let tab widget switch with a slide animation.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
