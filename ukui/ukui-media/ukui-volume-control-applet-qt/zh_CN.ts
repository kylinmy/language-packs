<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>ApplicationVolumeWidget</name>
    <message>
        <location filename="../ukmedia_application_volume_widget.cpp" line="28"/>
        <source>Application Volume</source>
        <translation>应用</translation>
    </message>
    <message>
        <location filename="../ukmedia_application_volume_widget.cpp" line="30"/>
        <source>System Volume</source>
        <translation>系统音量</translation>
    </message>
    <message>
        <location filename="../ukmedia_application_volume_widget.cpp" line="82"/>
        <source>Sound Settings</source>
        <translation>声音设置</translation>
    </message>
</context>
<context>
    <name>DeviceSwitchWidget</name>
    <message>
        <source>Go Into Mini Mode</source>
        <translation type="vanished">进入Mini模式</translation>
    </message>
    <message>
        <source>Output volume control</source>
        <translation type="vanished">输出音量控制</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation type="vanished">静音</translation>
    </message>
    <message>
        <source>Sound preference(S)</source>
        <translation type="vanished">声音首选项</translation>
    </message>
    <message>
        <source>Device Volume</source>
        <translation type="vanished">设备音量</translation>
    </message>
    <message>
        <source>Application Volume</source>
        <translation type="vanished">应用音量</translation>
    </message>
    <message>
        <source>is using</source>
        <translation type="vanished">正在使用</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation type="vanished">蓝牙</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>Unable to connect to the sound system, please check whether the pulseaudio service is running!</source>
        <translation type="vanished">无法连接到系统声音，请检查pulseaudio服务是否正在运行！</translation>
    </message>
    <message>
        <source>Dummy output</source>
        <translation type="vanished">伪输出</translation>
    </message>
    <message>
        <source>Speaker (Realtek Audio)</source>
        <translation type="vanished">扬声器(Realtek Audio)</translation>
    </message>
    <message>
        <source>Headphone</source>
        <translation type="vanished">模拟耳机</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1709"/>
        <source>pa_context_subscribe() failed</source>
        <translation>pa_context_subscribe() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1732"/>
        <source>pa_context_get_card_info_list() failed</source>
        <translation>pa_context_get_card_info_list() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1739"/>
        <source>pa_context_get_sink_info_list() failed</source>
        <translation>pa_context_get_sink_info_list() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1746"/>
        <source>pa_context_get_source_info_list() failed</source>
        <translation>pa_context_get_source_info_list() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1472"/>
        <source>Failed to initialize stream_restore extension: %s</source>
        <translation>初始化stream_restore扩展失败：%s</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1487"/>
        <source>pa_ext_stream_restore_read() failed</source>
        <translation>pa_ext_stream_restore_read() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1580"/>
        <source>pa_context_get_sink_info_by_index() failed</source>
        <translation>pa_context_get_sink_info_by_index() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1593"/>
        <source>pa_context_get_source_info_by_index() failed</source>
        <translation>pa_context_get_source_info_by_index() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1669"/>
        <source>pa_context_get_card_info_by_index() failed</source>
        <translation>pa_context_get_card_info_by_index() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1177"/>
        <source>Card callback failure</source>
        <translation>卡片回调失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1205"/>
        <location filename="../ukmedia_volume_control.cpp" line="1325"/>
        <source>Sink callback failure</source>
        <translation>输出回调失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1284"/>
        <location filename="../ukmedia_volume_control.cpp" line="1345"/>
        <source>Source callback failure</source>
        <translation>输入回调失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="86"/>
        <source>Fatal Error: Unable to connect to PulseAudio</source>
        <translation>错误：不能连接到pulseaudio</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="200"/>
        <location filename="../ukmedia_volume_control.cpp" line="1642"/>
        <location filename="../ukmedia_volume_control.cpp" line="1718"/>
        <source>pa_context_get_server_info() failed</source>
        <translation>pa_context_get_server_info() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1366"/>
        <location filename="../ukmedia_volume_control.cpp" line="1862"/>
        <source>Sink input callback failure</source>
        <translation>水槽输入回调失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1385"/>
        <source>Source output callback failure</source>
        <translation>source-ouput回调失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1404"/>
        <source>Client callback failure</source>
        <translation>客户端回调失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1420"/>
        <location filename="../ukmedia_volume_control.cpp" line="1447"/>
        <source>Server info callback failure</source>
        <translation>服务端回调错误</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1505"/>
        <source>Failed to initialize device restore extension: %s</source>
        <translation>初始化设备信息失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1526"/>
        <source>pa_ext_device_restore_read_sink_formats() failed</source>
        <translation>pa_ext_device_restore_read_sink_formats（） 失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1544"/>
        <source>Failed to initialize device manager extension: %s</source>
        <translation>初始化设备管理器扩展失败：%s</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1563"/>
        <source>pa_ext_device_manager_read() failed</source>
        <translation>pa_ext_device_manager_read() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1606"/>
        <location filename="../ukmedia_volume_control.cpp" line="1619"/>
        <source>pa_context_get_sink_input_info() failed</source>
        <translation>pa_context_get_sink_input_info() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1632"/>
        <source>pa_context_get_client_info() failed</source>
        <translation>pa_context_get_client_info() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1725"/>
        <source>pa_context_client_info_list() failed</source>
        <translation>pa_context_client_info_list() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1752"/>
        <source>pa_context_get_sink_input_info_list() failed</source>
        <translation>pa_context_get_sink_input_info_list() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1759"/>
        <source>pa_context_get_source_output_info_list() failed</source>
        <translation>pa_context_get_source_output_info_list() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1774"/>
        <source>Connection failed, attempting reconnect</source>
        <translation>连接失败，尝试重新连接中</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1793"/>
        <source>moduleInfoCb callback failure</source>
        <translation>模块信息Cb回调失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1810"/>
        <source>Ukui Media Volume Control</source>
        <translation>UKUI 媒体音量控制</translation>
    </message>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="3280"/>
        <source>pa_context_load_module() failed</source>
        <translation>pa_context_load_module（） 失败</translation>
    </message>
</context>
<context>
    <name>UkmediaDeviceWidget</name>
    <message>
        <source>Input Device</source>
        <translation type="vanished">输入设备</translation>
    </message>
    <message>
        <source>Microphone</source>
        <translation type="vanished">麦克风</translation>
    </message>
    <message>
        <source>Output Device</source>
        <translation type="vanished">输出设备</translation>
    </message>
    <message>
        <source>Speaker Realtek Audio</source>
        <translation type="vanished">扬声器(Realtek Audio)</translation>
    </message>
    <message>
        <source>Input device can not be detected</source>
        <translation type="vanished">无法检测到输入设备</translation>
    </message>
</context>
<context>
    <name>UkmediaMainWidget</name>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="107"/>
        <source>Output volume control</source>
        <translation>输出音量控制</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation type="vanished">静音</translation>
    </message>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="109"/>
        <location filename="../ukmedia_main_widget.cpp" line="111"/>
        <source>Sound preference(S)</source>
        <translation>设置声音项</translation>
    </message>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="172"/>
        <source>System Volume</source>
        <translation>系统音量</translation>
    </message>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="174"/>
        <source>App Volume</source>
        <translation>应用音量</translation>
    </message>
    <message>
        <location filename="../ukmedia_main_widget.cpp" line="1078"/>
        <source>Current volume:</source>
        <translation>当前音量：</translation>
    </message>
</context>
<context>
    <name>UkmediaMiniMasterVolumeWidget</name>
    <message>
        <source>Speaker (Realtek Audio)</source>
        <translation type="vanished">扬声器(Realtek Audio)</translation>
    </message>
    <message>
        <source>Go Into Full Mode</source>
        <translation type="vanished">进入完整模式</translation>
    </message>
</context>
<context>
    <name>UkmediaSystemVolumeWidget</name>
    <message>
        <location filename="../ukmedia_system_volume_widget.cpp" line="58"/>
        <source>Volume</source>
        <translation>音量</translation>
    </message>
    <message>
        <location filename="../ukmedia_system_volume_widget.cpp" line="69"/>
        <source>Output</source>
        <translation>输出</translation>
    </message>
    <message>
        <location filename="../ukmedia_system_volume_widget.cpp" line="85"/>
        <source>Sound Settings</source>
        <translation>声音设置</translation>
    </message>
</context>
<context>
    <name>UkmediaVolumeControl</name>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="97"/>
        <location filename="../ukmedia_volume_control.cpp" line="121"/>
        <location filename="../ukmedia_volume_control.cpp" line="125"/>
        <location filename="../ukmedia_volume_control.cpp" line="138"/>
        <location filename="../ukmedia_volume_control.cpp" line="160"/>
        <location filename="../ukmedia_volume_control.cpp" line="252"/>
        <location filename="../ukmedia_volume_control.cpp" line="256"/>
        <location filename="../ukmedia_volume_control.cpp" line="269"/>
        <source>pa_context_set_sink_volume_by_index() failed</source>
        <translation>pa_context_set_sink_volume_by_index() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="287"/>
        <location filename="../ukmedia_volume_control.cpp" line="291"/>
        <source>pa_context_set_source_output_volume() failed</source>
        <translation>pa_context_set_source_output_volume() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="304"/>
        <source>pa_context_set_source_output_mute() failed</source>
        <translation>pa_context_set_source_output_mute() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="317"/>
        <source>pa_context_set_card_profile_by_index() failed</source>
        <translation>pa_context_set_card_profile_by_index() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="333"/>
        <source>pa_context_set_default_sink() failed</source>
        <translation>pa_context_set_default_sink() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="349"/>
        <source>pa_context_set_default_source() failed</source>
        <translation>pa_context_set_default_source() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="365"/>
        <source>pa_context_set_sink_port_by_name() failed</source>
        <translation>pa_context_set_sink_port_by_name() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="380"/>
        <source>pa_context_set_source_port_by_name() failed</source>
        <translation>pa_context_set_source_port_by_name() 执行失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="393"/>
        <source>pa_context_kill_sink_input() failed</source>
        <translation>pa_context_kill_sink_input（） 失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="422"/>
        <source> (plugged in)</source>
        <translation> (插上了)</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="426"/>
        <location filename="../ukmedia_volume_control.cpp" line="561"/>
        <source> (unavailable)</source>
        <translation> (不可用)</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="428"/>
        <location filename="../ukmedia_volume_control.cpp" line="558"/>
        <source> (unplugged)</source>
        <translation> 没有连接</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="734"/>
        <source>Failed to read data from stream</source>
        <translation>从流中读取数据失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="776"/>
        <source>Peak detect</source>
        <translation>峰值检测</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="777"/>
        <source>Failed to create monitoring stream</source>
        <translation>创建监控流失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="792"/>
        <source>Failed to connect monitoring stream</source>
        <translation>连接监控流失败</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="909"/>
        <source>Ignoring sink-input due to it being designated as an event and thus handled by the Event widget</source>
        <translation>忽略沉降输入，因为它被指定为事件，因此由事件小部件处理</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1148"/>
        <source>Establishing connection to PulseAudio. Please wait...</source>
        <translation>正在建立与PulseAudio的连接。请等待...</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1429"/>
        <source>pa_context_get_sink_info_by_name() failed</source>
        <translation>pa_context_get_sink_info_by_index() failed</translation>
    </message>
    <message>
        <location filename="../ukmedia_volume_control.cpp" line="1433"/>
        <source>pa_context_get_source_info_by_name() failed</source>
        <translation>pa_context_get_source_info_by_index() failed</translation>
    </message>
</context>
<context>
    <name>UkuiMediaSetHeadsetWidget</name>
    <message>
        <location filename="../ukui_media_set_headset_widget.cpp" line="36"/>
        <source>Sound Settings</source>
        <translation>声音设置</translation>
    </message>
    <message>
        <location filename="../ukui_media_set_headset_widget.cpp" line="37"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ukui_media_set_headset_widget.cpp" line="38"/>
        <source>Select Sound Device</source>
        <translation>选择输出设备</translation>
    </message>
    <message>
        <location filename="../ukui_media_set_headset_widget.cpp" line="66"/>
        <source>Headphone</source>
        <translation>耳机</translation>
    </message>
    <message>
        <location filename="../ukui_media_set_headset_widget.cpp" line="67"/>
        <source>Headset</source>
        <translation>耳麦</translation>
    </message>
    <message>
        <location filename="../ukui_media_set_headset_widget.cpp" line="68"/>
        <source>Microphone</source>
        <translation>麦克风</translation>
    </message>
</context>
</TS>
