<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>WlanConnect</name>
    <message>
        <location filename="../wlanconnect.ui" line="14"/>
        <location filename="../wlanconnect.cpp" line="97"/>
        <source>WlanConnect</source>
        <translation>无线局域网</translation>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="35"/>
        <location filename="../wlanconnect.cpp" line="168"/>
        <source>WLAN</source>
        <translation>无线局域网</translation>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="94"/>
        <location filename="../wlanconnect.cpp" line="170"/>
        <source>open</source>
        <translation>开启</translation>
        <extra-contents_path>/wlanconnect/open</extra-contents_path>
    </message>
    <message>
        <location filename="../wlanconnect.ui" line="147"/>
        <location filename="../wlanconnect.cpp" line="167"/>
        <source>Advanced settings</source>
        <translation>高级设置</translation>
        <extra-contents_path>/wlanconnect/Advanced settings&quot;</extra-contents_path>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="80"/>
        <source>ukui control center</source>
        <translation>控制面板</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="83"/>
        <source>ukui control center desktop message</source>
        <translation>控制面板桌面通知</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="192"/>
        <source>No wireless network card detected</source>
        <translation>未检测到无线网卡</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="330"/>
        <location filename="../wlanconnect.cpp" line="933"/>
        <location filename="../wlanconnect.cpp" line="995"/>
        <source>connected</source>
        <translation>已连接</translation>
    </message>
    <message>
        <location filename="../wlanconnect.cpp" line="879"/>
        <source>card</source>
        <translation>网卡</translation>
    </message>
</context>
</TS>
