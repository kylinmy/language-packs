<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>BlacklistItem</name>
    <message>
        <location filename="../blacklistitem.cpp" line="30"/>
        <source>Remove</source>
        <translation>ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠨ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>BlacklistPage</name>
    <message>
        <location filename="../blacklistpage.cpp" line="26"/>
        <source>Blacklist</source>
        <translation>ᠬᠠᠷ᠎ᠠ ᠳᠠᠩᠰᠠ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>ConnectDevListItem</name>
    <message>
        <location filename="../connectdevlistitem.cpp" line="30"/>
        <source>drag into blacklist</source>
        <translation>ᠬᠠᠷ᠎ᠠ ᠳᠠᠩᠰᠠᠨ ᠳᠤ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>ConnectdevPage</name>
    <message>
        <location filename="../connectdevpage.cpp" line="27"/>
        <source>Connect device</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠶᠢ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>MobileHotspot</name>
    <message>
        <location filename="../mobilehotspot.cpp" line="35"/>
        <source>MobileHotspot</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ</translation>
    </message>
    <message>
        <location filename="../mobilehotspot.cpp" line="101"/>
        <source>mobilehotspot</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ</translation>
        <extra-contents_path>/mobilehotspot/mobilehotspot</extra-contents_path>
    </message>
    <message>
        <location filename="../mobilehotspot.cpp" line="103"/>
        <source>mobilehotspot open</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ ᠡᠬᠢᠯᠡᠬᠦ</translation>
        <extra-contents_path>/mobilehotspot/mobilehotspot open</extra-contents_path>
    </message>
</context>
<context>
    <name>MobileHotspotWidget</name>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="38"/>
        <source>ukui control center</source>
        <translation>ᠡᠵᠡᠮᠰᠢᠯ ᠦᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="41"/>
        <source>ukui control center desktop message</source>
        <translation>ᠡᠵᠡᠮᠰᠢᠯ ᠦᠨ ᠬᠠᠪᠳᠠᠰᠤᠨ ᠤ ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤᠨ ᠮᠡᠳᠡᠭᠳᠡᠯ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="117"/>
        <source>wirless switch is close or no wireless device</source>
        <translation>ᠤᠲᠠᠰᠤ ᠦᠭᠡᠢ ᠨᠡᠭᠡᠭᠡᠯᠭᠡ ᠨᠢᠭᠡᠨᠲᠡ ᠬᠠᠭᠠᠭᠰᠠᠨ ᠪᠤᠶᠤ ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ ᠦᠨ ᠴᠢᠳᠠᠮᠵᠢ ᠲᠠᠶ ᠤᠲᠠᠰᠤ ᠦᠭᠡᠢ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠦ ᠺᠠᠷᠲ ᠪᠠᠢᠬᠤ ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="121"/>
        <source>start to close hotspot</source>
        <translation>ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ ᠢ ᠡᠬᠢᠯᠡᠵᠤ ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="130"/>
        <source>hotpots name or device is invalid</source>
        <translation>ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ ᠦᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠪᠤᠶᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <source>can not  create hotspot with password length less than eight!</source>
        <translation type="vanished">不能创建密码长度小于八位的热点！</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="137"/>
        <source>start to open hotspot </source>
        <translation>ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ ᠢ ᠡᠬᠢᠯᠡᠵᠤ ᠪᠠᠢᠭᠤᠯᠬᠤ </translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="257"/>
        <source>Contains at least 8 characters</source>
        <translation>ᠠᠳᠠᠭ ᠲᠤ ᠪᠠᠨ 8 ᠦᠰᠦᠭ ᠳᠡᠮᠳᠡᠭ ᠠᠭᠤᠯᠠᠭᠳᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="172"/>
        <source>Hotspot</source>
        <translation>ᠰᠢᠯᠵᠢᠮᠡᠯ ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="268"/>
        <location filename="../mobilehotspotwidget.cpp" line="582"/>
        <source>hotspot already close</source>
        <translation>ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ ᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠬᠠᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="408"/>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠭᠦ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="429"/>
        <source>Wi-Fi Name</source>
        <translation>Wi-Fi ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="452"/>
        <source>Password</source>
        <translation>ᠰᠦᠯᠵᠢᠶᠡᠨ ᠦ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="490"/>
        <source>Frequency band</source>
        <translation>ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶᠡᠨ ᠦ ᠳᠠᠪᠳᠠᠮᠵᠢ ᠶᠢᠨ ᠪᠦᠰᠡ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="516"/>
        <source>Net card</source>
        <translation>ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ ᠨᠧᠲ ᠦᠨ ᠦᠵᠦᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../mobilehotspotwidget.cpp" line="611"/>
        <location filename="../mobilehotspotwidget.cpp" line="619"/>
        <source>hotspot already open</source>
        <translation>ᠬᠠᠯᠠᠭᠤᠨ ᠴᠡᠭ ᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠨᠡᠬᠡᠬᠡᠪᠡ</translation>
    </message>
</context>
</TS>
