<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AddNetBtn</name>
    <message>
        <location filename="../addnetbtn.cpp" line="22"/>
        <source>Add WiredNetork</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>NetConnect</name>
    <message>
        <location filename="../netconnect.ui" line="50"/>
        <location filename="../netconnect.cpp" line="152"/>
        <source>Wired Network</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../netconnect.ui" line="112"/>
        <location filename="../netconnect.cpp" line="154"/>
        <source>open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠬᠦ</translation>
        <extra-contents_path>/netconnect/open</extra-contents_path>
    </message>
    <message>
        <location filename="../netconnect.ui" line="198"/>
        <location filename="../netconnect.cpp" line="151"/>
        <source>Advanced settings</source>
        <translation>ᠦᠨᠳᠦᠷ ᠵᠡᠷᠬᠡ ᠵᠢᠨ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ</translation>
        <extra-contents_path>/netconnect/Advanced settings&quot;</extra-contents_path>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="63"/>
        <source>ukui control center</source>
        <translation>ᠡᠵᠡᠮᠰᠢᠯ ᠦᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="66"/>
        <source>ukui control center desktop message</source>
        <translation>ᠡᠵᠡᠮᠰᠢᠯ ᠦᠨ ᠬᠠᠪᠳᠠᠰᠤᠨ ᠤ ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤᠨ ᠮᠡᠳᠡᠭᠳᠡᠯ</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="80"/>
        <source>WiredConnect</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="177"/>
        <source>No ethernet device avaliable</source>
        <translation>ᠤᠳᠠᠰᠤᠳᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠶᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="426"/>
        <location filename="../netconnect.cpp" line="833"/>
        <source>connected</source>
        <translation>ᠨᠢᠬᠡᠨᠳᠡ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <location filename="../netconnect.cpp" line="490"/>
        <source>card</source>
        <translation>ᠨᠧᠲ ᠦᠨ ᠺᠠᠷᠲ</translation>
    </message>
</context>
</TS>
