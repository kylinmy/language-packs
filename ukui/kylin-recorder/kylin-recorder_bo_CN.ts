<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>ClipWidget</name>
    <message>
        <location filename="../src/clipwidget.cpp" line="135"/>
        <source>Audition</source>
        <translation>ཚོད་ཉན།</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="137"/>
        <source>Pause</source>
        <translation>མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="140"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="143"/>
        <source>Finish</source>
        <translation>མཇུག་སྒྲིལ།</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="329"/>
        <source>Save New</source>
        <translation>ཉར་ཚགས་གསར་པ།</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="330"/>
        <source>Cover Current</source>
        <translation>མིག་སྔར་གྱི་ཡིག་ཆ་འགེབ་པ།</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="354"/>
        <source>Select a file storage directory</source>
        <translation>ཡིག་ཆ་གསོག་ཉར་གྱི་དཀར་ཆག་ཅིག་བདམས་པ་རེད།</translation>
    </message>
    <message>
        <source>The duration of the clip cannot be less than 1s, and you must drag at least one slider to start this activity!</source>
        <translation type="vanished">剪辑时间不得少于1s,且您必须拖拽至少一个滑块作为此活动开始!</translation>
    </message>
    <message>
        <source>New File:</source>
        <translation type="vanished">新文件:</translation>
    </message>
    <message>
        <source>The duration of the clip cannot be less than 1s, and the initial start and end positions cannot be used as the start and end positions of the clip!</source>
        <translation type="vanished">剪辑时长不得少于1s,且初始的开始和结束位置不能作为剪辑的开始和结束位置!</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="414"/>
        <source>Hint</source>
        <translation>གསལ་འདེབས་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Message</source>
        <translation type="vanished">消息</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="415"/>
        <source>This will overwrite the original file path,are you sure?</source>
        <translation>འདི་ལྟར་བྱས་ན་སྔར་གྱི་ཡིག་ཆའི་ལམ་བུ་ལས་བརྒལ་སྲིད་དམ། ཁྱོད་ཀྱིས་གཏན་འཁེལ་བྱེད་ཐུབ་བམ།</translation>
    </message>
    <message>
        <location filename="../src/clipwidget.cpp" line="417"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>The duration of the clip cannot be less than 1s, and the duration of the clip cannot be the duration of the original file!</source>
        <translation type="vanished">剪辑时长不能小于1s,剪辑时长不能与原时长相同!</translation>
    </message>
    <message>
        <source>This storage path is illegal!</source>
        <translation type="vanished">存储路径非法!</translation>
    </message>
    <message>
        <source>The file name cannot exceed 20 characters!</source>
        <translation type="vanished">文件名不得超过20个字符!</translation>
    </message>
    <message>
        <source>Clip Finished!</source>
        <translation type="vanished">剪辑完成!</translation>
    </message>
</context>
<context>
    <name>FileItem</name>
    <message>
        <location filename="../src/fileitem.cpp" line="34"/>
        <source>Play</source>
        <translation>གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="39"/>
        <source>Pause</source>
        <translation>མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="44"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="126"/>
        <location filename="../src/fileitem.cpp" line="381"/>
        <location filename="../src/fileitem.cpp" line="384"/>
        <location filename="../src/fileitem.cpp" line="390"/>
        <location filename="../src/fileitem.cpp" line="464"/>
        <location filename="../src/fileitem.cpp" line="528"/>
        <location filename="../src/fileitem.cpp" line="545"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="obsolete">最小化</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="49"/>
        <source>Clip</source>
        <translation>རྩོམ་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="54"/>
        <source>Flag</source>
        <translation>རྟགས་འགོད།</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="381"/>
        <source>Time is too short</source>
        <translation>དུས་ཚོད་ཐུང་དྲགས་པ།</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="391"/>
        <source>Unable to parse the waveform of audio file generated by non recorder！</source>
        <translation>སྒྲ་ཕབ་འཕྲུལ་ཆས་མིན་པ་ལས་བྱུང་བའི་སྒྲ་ཕབ་ཡིག་ཆའི་རླབས་ཕྲེང་ལ་དཔྱད་བསྡུར་བྱེད་ཐབས་བྲལ།</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="464"/>
        <source>Playing, please stop and delete!</source>
        <translation>གཏོང་བཞིན་པའི་སྐབས་སུ་མཚམས་བཞག་རྗེས་བསུབ་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="495"/>
        <source>Save as</source>
        <translation>གཞན་པར་གསོག་ཉར་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="496"/>
        <source>Open folder position</source>
        <translation>ཡིག་སྣོད་ཀྱི་ས་གནས་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="516"/>
        <source>Select a file storage directory</source>
        <translation>ཡིག་ཆ་གསོག་ཉར་གྱི་དཀར་ཆག་ཅིག་བདམས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/fileitem.cpp" line="127"/>
        <location filename="../src/fileitem.cpp" line="385"/>
        <location filename="../src/fileitem.cpp" line="529"/>
        <location filename="../src/fileitem.cpp" line="546"/>
        <source>The file path does not exist or has been deleted!</source>
        <translation>ཡིག་ཆའི་ལམ་ཕྲན་མེད་པའམ་ཡང་ན་བསུབ་ཟིན།</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="199"/>
        <location filename="../src/mainwindow.cpp" line="241"/>
        <location filename="../src/mainwindow.cpp" line="1470"/>
        <source>Recorder</source>
        <translation>སྒྲ་ཕབ།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="245"/>
        <source>Set</source>
        <translation>སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="246"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="247"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="251"/>
        <source>Menu</source>
        <translation>འདེམས་པང་།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="262"/>
        <source>Mini</source>
        <translation>ཆུང་གྲས།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="271"/>
        <source>Minimize</source>
        <translation>ཆེས་ཆུང་དུ་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="280"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="288"/>
        <location filename="../src/mainwindow.cpp" line="293"/>
        <location filename="../src/mainwindow.cpp" line="1503"/>
        <source>Recording</source>
        <translation>སྒྲ་ཕབ།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="372"/>
        <source>Finish</source>
        <translation>མཇུག་སྒྲིལ།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="385"/>
        <source>Flag</source>
        <translation>ཨང་རྟགས།</translation>
    </message>
    <message>
        <source>Sign</source>
        <translation type="vanished">标记</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="388"/>
        <source>Left</source>
        <translation>སྔོན་མ་གཅིག</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="395"/>
        <source>Right</source>
        <translation>རྗེས་མ་གཅིག</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="422"/>
        <source>File List</source>
        <translation>ཡིག་ཆའི་རེའུ་མིག</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="790"/>
        <source>None of the Recording File</source>
        <translation>སྒྲ་ཕབ་ཡིག་ཆ་གཅིག་ཀྱང་མེད།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1029"/>
        <location filename="../src/mainwindow.cpp" line="1229"/>
        <location filename="../src/mainwindow.cpp" line="1410"/>
        <location filename="../src/mainwindow.cpp" line="1576"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1029"/>
        <source>No input device detected!</source>
        <translation>ནང་འཇུག་སྒྲིག་ཆས་ལ་ཞིབ་དཔྱད་ཚད་ལེན་བྱས་མེད།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1229"/>
        <location filename="../src/mainwindow.cpp" line="1410"/>
        <source>Audio is playing, please stop and record again!</source>
        <translation>སྒྲ་ཕབ་དཀྲོལ་བཞིན་ཡོད། ཡང་བསྐྱར་མཚམས་བཞག་ནས་སྒྲ་ཕབ་རོགས།</translation>
    </message>
    <message>
        <source>There is audio playing, please stop after recording!</source>
        <translation type="vanished">音频正在播放,请停止后录音!</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1192"/>
        <source>pause</source>
        <translation>མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1197"/>
        <source>start</source>
        <translation>མུ་མཐུད།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1396"/>
        <source>Tips From Recorder</source>
        <translation>སྒྲ་ཕབ་འཕྲུལ་ཆས་ཀྱི་གསལ་འདེབས།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1576"/>
        <source>The file is not in the recording list,cannot be opened</source>
        <translation>ཡིག་ཆ་དེ་སྒྲ་ཕབ་ཀྱི་མིང་ཐོའི་ནང་བཀོད་མེད་པས་ཁ་ཕྱེ་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1588"/>
        <source>Using multichannel device</source>
        <translation>ཐབས་ལམ་མང་པོའི་སྒྲིག་ཆས་བཀོལ་སྤྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1590"/>
        <source>Microphone in use</source>
        <translation>བཀོལ་སྤྱོད་བྱེད་བཞིན་པའི་སྐད་སྦུག</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1598"/>
        <source>kylin-recorder</source>
        <translation>སྒྲ་ཕབ།</translation>
    </message>
</context>
<context>
    <name>MiniWindow</name>
    <message>
        <location filename="../src/miniwindow.cpp" line="96"/>
        <source>Recorder</source>
        <translation>སྒྲ་ཕབ།</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="100"/>
        <source>Recording</source>
        <translation>སྒྲ་ཕབ།</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="105"/>
        <source>Finish</source>
        <translation>མཇུག་སྒྲིལ།</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="112"/>
        <location filename="../src/miniwindow.cpp" line="237"/>
        <source>Pause</source>
        <translation>མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="124"/>
        <source>Restore</source>
        <translation>སླར་གསོ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="132"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="211"/>
        <location filename="../src/miniwindow.cpp" line="298"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="211"/>
        <location filename="../src/miniwindow.cpp" line="298"/>
        <source>Audio is playing, please stop and record again!</source>
        <translation>སྒྲ་ཕབ་དཀྲོལ་བཞིན་ཡོད། ཡང་བསྐྱར་མཚམས་བཞག་ནས་སྒྲ་ཕབ་རོགས།</translation>
    </message>
    <message>
        <source>There is audio playing, please stop after recording!</source>
        <translation type="vanished">音频在播放,请停止后再录音!</translation>
    </message>
    <message>
        <location filename="../src/miniwindow.cpp" line="242"/>
        <source>Start</source>
        <translation>མུ་མཐདུ་དུ།</translation>
    </message>
</context>
<context>
    <name>MyThread</name>
    <message>
        <location filename="../src/mythread.cpp" line="170"/>
        <location filename="../src/mythread.cpp" line="174"/>
        <location filename="../src/mythread.cpp" line="177"/>
        <location filename="../src/mythread.cpp" line="185"/>
        <location filename="../src/mythread.cpp" line="189"/>
        <location filename="../src/mythread.cpp" line="192"/>
        <source>/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="170"/>
        <location filename="../src/mythread.cpp" line="185"/>
        <source>.mp3</source>
        <translation>.mp3</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="174"/>
        <location filename="../src/mythread.cpp" line="189"/>
        <source>.wav</source>
        <translation>.wav</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="177"/>
        <location filename="../src/mythread.cpp" line="192"/>
        <source>.m4a</source>
        <translation>.m4a</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="337"/>
        <location filename="../src/mythread.cpp" line="340"/>
        <location filename="../src/mythread.cpp" line="342"/>
        <source>Recorder</source>
        <translation>སྒྲ་ཕབ།</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="577"/>
        <source>Select a file storage directory</source>
        <translation>ཡིག་ཆ་གསོག་ཉར་གྱི་དཀར་ཆག་ཅིག་བདམས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="581"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../src/mythread.cpp" line="581"/>
        <source>Do not enter illegal file name</source>
        <translation>ཁྲིམས་འགལ་གྱི་ཡིག་ཆའི་མིང་ནང་འཇུག་མི་ཆོག།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/recorderdatabase.cpp" line="30"/>
        <location filename="../src/recorderdatabase.cpp" line="72"/>
        <location filename="../src/recorderdatabase.cpp" line="120"/>
        <location filename="../src/recorderdatabase.cpp" line="161"/>
        <location filename="../src/recorderdatabase.cpp" line="189"/>
        <location filename="../src/recorderdatabase.cpp" line="241"/>
        <location filename="../src/recorderdatabase.cpp" line="289"/>
        <source>Database Error</source>
        <translation>གཞི་གྲངས་མཛོད་ཀྱི་ནོར་འཁྲུལ།</translation>
    </message>
</context>
<context>
    <name>Save</name>
    <message>
        <location filename="../src/save.cpp" line="134"/>
        <location filename="../src/save.cpp" line="140"/>
        <location filename="../src/save.cpp" line="151"/>
        <source>Select a file storage directory</source>
        <translation>ཡིག་ཆ་གསོག་ཉར་གྱི་དཀར་ཆག་ཅིག་བདམས་པ་རེད།</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../src/settings.cpp" line="49"/>
        <location filename="../src/settings.cpp" line="62"/>
        <source>Settings</source>
        <translation>སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="77"/>
        <source>Alter</source>
        <translation>བཟོ་བཅོས།</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="99"/>
        <source>Storage:</source>
        <translation>ཡིག་ཆ་གསོག་ཉར།</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="108"/>
        <location filename="../src/settings.cpp" line="111"/>
        <location filename="../src/settings.cpp" line="227"/>
        <location filename="../src/settings.cpp" line="243"/>
        <location filename="../src/settings.cpp" line="247"/>
        <source>Recorder</source>
        <translation>སྒྲ་ཕབ།</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="124"/>
        <source>Format:</source>
        <translation>ཡིག་ཆའི་རྣམ་གཞག་ནི།</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="129"/>
        <source>mp3</source>
        <translation>mp3</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="137"/>
        <source>Source:</source>
        <translation>སྒྲ་ཕབ་འབྱུང་ཁུངས་ནི།</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="143"/>
        <source>Microphone</source>
        <translation>སྐད་སྦུག</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="144"/>
        <source>System Inside</source>
        <translation>ནང་ཁུལ་གྱི་མ་ལག</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="222"/>
        <source>Select a file storage directory</source>
        <translation>ཡིག་ཆ་གསོག་ཉར་གྱི་དཀར་ཆག་ཅིག་བདམས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="267"/>
        <location filename="../src/settings.cpp" line="279"/>
        <location filename="../src/settings.cpp" line="286"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="267"/>
        <location filename="../src/settings.cpp" line="286"/>
        <source>This storage path is illegal!</source>
        <translation>གསོག་ཉར་ལམ་བུ་འདི་ནི་ཁྲིམས་དང་མི་མཐུན་པ།</translation>
    </message>
    <message>
        <location filename="../src/settings.cpp" line="280"/>
        <source>The file name cannot exceed 20 characters!</source>
        <translation>ཡིག་ཆའི་མིང་དེ་ཡི་གེ་20ལས་བརྒལ་མི་རུང་།</translation>
    </message>
</context>
<context>
    <name>TipWindow</name>
    <message>
        <source>Transcoding...</source>
        <translation type="vanished">解析中...</translation>
    </message>
    <message>
        <location filename="../src/tipwindow.cpp" line="19"/>
        <source>Analysis...</source>
        <translation>དབྱེ་ཞིབ་ ...</translation>
    </message>
</context>
<context>
    <name>Tools</name>
    <message>
        <location filename="../src/tools.cpp" line="21"/>
        <location filename="../src/tools.cpp" line="32"/>
        <location filename="../src/tools.cpp" line="39"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../src/tools.cpp" line="21"/>
        <location filename="../src/tools.cpp" line="39"/>
        <source>This storage path is illegal!</source>
        <translation>གསོག་ཉར་ལམ་བུ་འདི་ནི་ཁྲིམས་དང་མི་མཐུན་པ།</translation>
    </message>
    <message>
        <location filename="../src/tools.cpp" line="33"/>
        <source>The file name cannot exceed 20 characters!</source>
        <translation>ཡིག་ཆའི་མིང་དེ་ཡི་གེ་20ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../src/tools.cpp" line="59"/>
        <source>Recorder</source>
        <translation>སྒྲ་ཕབ།</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Setting</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="31"/>
        <source>Theme</source>
        <translation>བརྗོད་བྱ་གཙོ་བོ།</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="28"/>
        <location filename="../src/menumodule.cpp" line="128"/>
        <source>Settings</source>
        <translation>སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="34"/>
        <location filename="../src/menumodule.cpp" line="126"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="37"/>
        <location filename="../src/menumodule.cpp" line="124"/>
        <location filename="../src/menumodule.cpp" line="199"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="40"/>
        <location filename="../src/menumodule.cpp" line="122"/>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན་བྱ་རྒྱུ།</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="262"/>
        <source>Version: </source>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ། </translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="265"/>
        <location filename="../src/menumodule.cpp" line="329"/>
        <location filename="../src/menumodule.cpp" line="344"/>
        <source>Service &amp; Support: </source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར། </translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="280"/>
        <source>The UI is friendly and easy to operate. It supports by microphone ,playing and deleting in file list, and switching between Mini mode and Theme mode</source>
        <translation>མཚམས་ངོས་མཛའ་མཐུན་བཀོལ་སྤྱོད་སྟབས་བདེ་དང་།སྐད་སྦུག་སྒྲ་ཕབ་ལ་རྒྱབ་སྐྱོར་དང་།ཡིག་ཆའི་རེའུ་མིག་གཏོང་བ་དང་བསུབ་པར་རྒྱབ་སྐྱོར།ཆུང་གྲས་དང་བརྗོད་བྱ་གཙོ་བོའི་རྣམ་པ་བརྗེ་སྤོར་ལ་རྒྱབ་སྐྱོར་བཅས་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>The UI is friendly and easy to operate. It supports MP3 audio formats by microphone, playing and deleting in file list, and switching between Mini mode and Theme mode</source>
        <translation type="vanished">界面友好操作简单,支持麦克风录制MP3音频格式,支持文件列表中播放和删除,支持迷你和主题模式切换</translation>
    </message>
    <message>
        <source>The UI is friendly and easy to operate. It supports MP3 and WAV audio formats by microphone, playing and deleting in file list, and switching between Mini mode and Theme mode</source>
        <translation type="vanished">界面友好操作简单,支持麦克风录制MP3和WAV音频格式,支持文件列表中播放和删除,支持迷你模式和主题模式切换</translation>
    </message>
    <message>
        <location filename="../src/menumodule.h" line="43"/>
        <source>Recorder</source>
        <translation>སྒྲ་ཕབ།</translation>
    </message>
</context>
</TS>
