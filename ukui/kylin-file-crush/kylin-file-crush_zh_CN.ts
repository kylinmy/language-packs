<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AlertDialog</name>
    <message>
        <location filename="../alertdialog.ui" line="14"/>
        <source>Form</source>
        <translation>格式</translation>
    </message>
</context>
<context>
    <name>KAlertDialog</name>
    <message>
        <location filename="../kalertdialog.cpp" line="28"/>
        <source>The file you selected is a directory or does not have permissions, cannot be shredded!</source>
        <translation>您选择的文件是一个目录或者无权限，无法粉碎！</translation>
    </message>
    <message>
        <location filename="../kalertdialog.cpp" line="31"/>
        <source>sure</source>
        <translation>确定</translation>
    </message>
</context>
<context>
    <name>KSureDialog</name>
    <message>
        <location filename="../ksuredialog.cpp" line="26"/>
        <source>Are you sure to start crushing?</source>
        <translation>是否确定开始粉碎？</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="32"/>
        <source>Crushed files cannot be recovered!</source>
        <translation>粉碎的文件将不可恢复！</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="39"/>
        <source>sure</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../ksuredialog.cpp" line="47"/>
        <source>cancle</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>ShredDialog</name>
    <message>
        <source>Kylin Shred Manager</source>
        <translation type="vanished">文件粉碎机</translation>
    </message>
    <message>
        <source>Shred Manager</source>
        <translation type="vanished">文件粉碎机</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="303"/>
        <source>No file selected to be shredded</source>
        <translation>未选择要粉碎的文件</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="155"/>
        <source>Shred File</source>
        <translation>粉碎</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="37"/>
        <source>FileShredder</source>
        <translation>文件粉碎机</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="154"/>
        <location filename="../shreddialog.cpp" line="199"/>
        <source>The file is completely shredded and cannot be recovered</source>
        <translation>文件将被完全粉碎且不能恢复</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="156"/>
        <source>Deselect</source>
        <translation>移除</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="157"/>
        <source>Add File</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="158"/>
        <source>Note:File shredding cannot be cancelled, please exercise caution!</source>
        <translation>注意：文件粉碎操作不可取消，请谨慎操作！</translation>
    </message>
    <message>
        <source>Note: The file shredding process cannot be cancelled, please operate with caution!</source>
        <translation type="vanished">注意：文件粉碎操作不可取消，请谨慎操作！</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="231"/>
        <source>File Shredding ...</source>
        <translation>文件粉碎中...</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="288"/>
        <source>Please select the file to shred!</source>
        <translation>请选择要粉碎的文件！</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="293"/>
        <source>You did not select a file with permissions!</source>
        <translation>您选择的不是一个有权限的文件！</translation>
    </message>
    <message>
        <source>Shattering...</source>
        <translation type="vanished">粉碎中...</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="190"/>
        <source>Select file</source>
        <translation>选择文件</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="190"/>
        <source>All Files(*)</source>
        <translation>所有文件(*)</translation>
    </message>
    <message>
        <source>Select file!</source>
        <translation type="vanished">请选择文件！</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="253"/>
        <source>Shred successfully!</source>
        <translation>文件粉碎成功！</translation>
    </message>
    <message>
        <location filename="../shreddialog.cpp" line="266"/>
        <source>Shred failed!</source>
        <translation>文件粉碎失败！</translation>
    </message>
</context>
<context>
    <name>ShredManager</name>
    <message>
        <location filename="../shredmanager.cpp" line="50"/>
        <source>Shred Manager</source>
        <translation>文件粉碎机</translation>
    </message>
    <message>
        <location filename="../shredmanager.cpp" line="55"/>
        <source>Delete files makes it unable to recover</source>
        <translation>粉碎文件将无法恢复！</translation>
    </message>
</context>
</TS>
