<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>QObject</name>
    <message>
        <location filename="main.cpp" line="64"/>
        <source>No longer prompt</source>
        <translation>ད་ནས་བཟུང་སྐུལ་འདེད་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="main.cpp" line="66"/>
        <source>Activate now</source>
        <translation>ད་ལྟ་གྲུང་སྐུལ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="main.cpp" line="68"/>
        <location filename="main.cpp" line="71"/>
        <source>System activation</source>
        <translation>མ་ལག་གི་གྲུང་སྐུལ།</translation>
    </message>
    <message>
        <location filename="main.cpp" line="72"/>
        <source>The current system is not activated, please activate the system first!</source>
        <translation>ད་ལྟ་སྤྱོད་བཞིན་པའི་མ་ལག་ལ་གྲུང་སྐུལ་མ་བྱས་ན་སྔོན་ལ་མ་ལག་འདི་གྲུང་སྐུལ་བྱེད་རོགས།</translation>
    </message>
</context>
</TS>
