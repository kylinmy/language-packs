<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>BioProxy</name>
    <message>
        <location filename="../BioProxy.cpp" line="198"/>
        <source>FingerPrint</source>
        <translation>指纹</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="200"/>
        <source>FingerVein</source>
        <translation>手指纹路</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="202"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="204"/>
        <source>Face</source>
        <translation>面部</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="206"/>
        <source>VoicePrint</source>
        <translation>声纹</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="208"/>
        <source>QRCode</source>
        <translation>二维码</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="219"/>
        <source>Unplugging of %1 device detected</source>
        <translation>检测到 %1 设备被拔出</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="225"/>
        <source>%1 device insertion detected</source>
        <translation>%1 设备已连接</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="234"/>
        <source>ukui-biometric-manager</source>
        <translation>UKUI生物特征管理器</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="237"/>
        <source>biometric</source>
        <translation>生物特征</translation>
    </message>
</context>
<context>
    <name>BioWidget</name>
    <message>
        <location filename="../BioWidget.cpp" line="25"/>
        <source>The login options</source>
        <translation type="unfinished">登录选项</translation>
    </message>
</context>
<context>
    <name>BoxCreateDialog</name>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="32"/>
        <location filename="../BoxCreateDialog.cpp" line="57"/>
        <source>Create</source>
        <translation>新建保护箱</translation>
    </message>
    <message>
        <source>Create a protective box</source>
        <translation type="vanished">新建保护箱</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="65"/>
        <source>Encrypt       </source>
        <oldsource>Encrypt     </oldsource>
        <translation type="unfinished">加密       </translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="120"/>
        <source>Name          </source>
        <translation type="unfinished">名称          </translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="121"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="152"/>
        <location filename="../BoxCreateDialog.cpp" line="154"/>
        <location filename="../BoxCreateDialog.cpp" line="157"/>
        <location filename="../BoxCreateDialog.cpp" line="673"/>
        <source>Confirm</source>
        <translation>确认</translation>
    </message>
    <message>
        <source>Cancle</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="122"/>
        <source>Confirm </source>
        <translation>确认密码</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="153"/>
        <location filename="../BoxCreateDialog.cpp" line="155"/>
        <location filename="../BoxCreateDialog.cpp" line="158"/>
        <location filename="../BoxCreateDialog.cpp" line="674"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Passwd level</source>
        <translation type="vanished">密码级别</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="349"/>
        <source>Box name cannot be empty</source>
        <oldsource>Box name cannot be empty!</oldsource>
        <translation>框名否为空</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="367"/>
        <source>Box name has been exit</source>
        <oldsource>Box name has been exit!</oldsource>
        <translation>框名已存在</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="383"/>
        <location filename="../BoxCreateDialog.cpp" line="386"/>
        <location filename="../BoxCreateDialog.cpp" line="452"/>
        <location filename="../BoxCreateDialog.cpp" line="478"/>
        <source>Create box is failed</source>
        <oldsource>Create box is failed!</oldsource>
        <translation type="unfinished">保护箱创建失败</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="359"/>
        <source>Password cannot be empty</source>
        <oldsource>Password cannot be empty!</oldsource>
        <translation type="unfinished">密码不可为空</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="295"/>
        <location filename="../BoxCreateDialog.cpp" line="296"/>
        <location filename="../BoxCreateDialog.cpp" line="309"/>
        <location filename="../BoxCreateDialog.cpp" line="310"/>
        <source>Password length can not be higer than 32</source>
        <translation type="unfinished">密码长度不可高于32位</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="400"/>
        <source>Password length can not be less than 6</source>
        <oldsource>Passwd length can not be less than 6</oldsource>
        <translation type="unfinished">密码长度不可低于6位</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="408"/>
        <source>Confirm password cannot be empty</source>
        <translation type="unfinished">确认密码不可为空</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="419"/>
        <source>Password is not same as verify password</source>
        <oldsource>Password is not same as verify password!</oldsource>
        <translation type="unfinished">密码与确认密码不一致</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="437"/>
        <source>Create globalKey failed</source>
        <oldsource>Create globalKey failed!</oldsource>
        <translation type="unfinished">创建全局密钥失败</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="553"/>
        <location filename="../BoxCreateDialog.cpp" line="554"/>
        <location filename="../BoxCreateDialog.cpp" line="647"/>
        <source>Invaild name</source>
        <translation>不允许的特殊字符</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="581"/>
        <location filename="../BoxCreateDialog.cpp" line="582"/>
        <location filename="../BoxCreateDialog.cpp" line="652"/>
        <source>Invaild password</source>
        <translation>不允许的特殊字符</translation>
    </message>
    <message>
        <source>Low</source>
        <translation type="obsolete">低</translation>
    </message>
    <message>
        <source>Medium</source>
        <translation type="obsolete">中</translation>
    </message>
    <message>
        <source>High</source>
        <translation type="obsolete">高</translation>
    </message>
    <message>
        <source>Low password strength</source>
        <translation type="obsolete">密码强度低</translation>
    </message>
    <message>
        <source>Medium password strength</source>
        <translation type="obsolete">密码强度中</translation>
    </message>
    <message>
        <source>High password strength</source>
        <translation type="obsolete">密码强度高</translation>
    </message>
    <message>
        <source>Passwd level low</source>
        <translation type="vanished">密码强度低</translation>
    </message>
    <message>
        <source>Passwd level mid</source>
        <translation type="vanished">密码强度中</translation>
    </message>
    <message>
        <source>Passwd level high</source>
        <translation type="vanished">密码强度高</translation>
    </message>
</context>
<context>
    <name>BoxItemDelegate</name>
    <message>
        <source>Yes</source>
        <translation type="vanished">是</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">否</translation>
    </message>
</context>
<context>
    <name>BoxMessageDialog</name>
    <message>
        <source>Password setting is successful!</source>
        <translation type="obsolete">密码修改成功！</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="obsolete">确定</translation>
    </message>
</context>
<context>
    <name>BoxPasswdSetting</name>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="40"/>
        <location filename="../BoxPasswdSetting.cpp" line="99"/>
        <source>Password setting</source>
        <translation>修改保护箱密码</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="158"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="159"/>
        <location filename="../BoxPasswdSetting.cpp" line="418"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="160"/>
        <source>New Password</source>
        <translation>新密码</translation>
    </message>
    <message>
        <location filename="BoxPasswdSetting.cpp" line="178"/>
        <source></source>
        <oldsource>Confirm password</oldsource>
        <translation>确认密码</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="198"/>
        <location filename="../BoxPasswdSetting.cpp" line="200"/>
        <location filename="../BoxPasswdSetting.cpp" line="202"/>
        <location filename="../BoxPasswdSetting.cpp" line="769"/>
        <location filename="../BoxPasswdSetting.cpp" line="1126"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Please update the psw</source>
        <translation type="vanished">请导入密钥文件</translation>
    </message>
    <message>
        <source>Display</source>
        <translation type="vanished">上传</translation>
    </message>
    <message>
        <source>Psw</source>
        <translation type="vanished">密钥</translation>
    </message>
    <message>
        <source>Password is error!</source>
        <oldsource>Passwd is error!</oldsource>
        <translation type="obsolete">密码错误</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="542"/>
        <location filename="../BoxPasswdSetting.cpp" line="543"/>
        <location filename="../BoxPasswdSetting.cpp" line="900"/>
        <location filename="../BoxPasswdSetting.cpp" line="902"/>
        <source>Box umount failed</source>
        <translation>锁定保护箱失败</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="161"/>
        <source>Confirm     </source>
        <translation type="unfinished">新密码确认</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="197"/>
        <location filename="../BoxPasswdSetting.cpp" line="199"/>
        <location filename="../BoxPasswdSetting.cpp" line="201"/>
        <location filename="../BoxPasswdSetting.cpp" line="1125"/>
        <source>Confirm</source>
        <translation type="unfinished">确认</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="385"/>
        <location filename="../BoxPasswdSetting.cpp" line="386"/>
        <location filename="../BoxPasswdSetting.cpp" line="393"/>
        <location filename="../BoxPasswdSetting.cpp" line="394"/>
        <location filename="../BoxPasswdSetting.cpp" line="401"/>
        <location filename="../BoxPasswdSetting.cpp" line="402"/>
        <source>Password length can not be higer than 32</source>
        <translation type="unfinished">密码长度不可高于32位</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1125"/>
        <source> (O)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1126"/>
        <source> (C)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="216"/>
        <location filename="../BoxPasswdSetting.cpp" line="838"/>
        <source>Please import the secret key file</source>
        <translation type="unfinished">请导入密钥文件</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="228"/>
        <source>Import</source>
        <translation type="unfinished">导入</translation>
    </message>
    <message>
        <source>Password level</source>
        <oldsource>Passwd level</oldsource>
        <translation type="obsolete">密码级别</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="434"/>
        <source>Secret key</source>
        <translation type="unfinished">密钥</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="525"/>
        <location filename="../BoxPasswdSetting.cpp" line="526"/>
        <source>Password can not be empty</source>
        <oldsource>Password can not be empty!</oldsource>
        <translation type="unfinished">密码不可为空</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="570"/>
        <location filename="../BoxPasswdSetting.cpp" line="571"/>
        <location filename="../BoxPasswdSetting.cpp" line="581"/>
        <location filename="../BoxPasswdSetting.cpp" line="582"/>
        <source>Password is error</source>
        <oldsource>Passwd is error</oldsource>
        <translation type="unfinished">密码错误</translation>
    </message>
    <message>
        <source>New password can not be empty</source>
        <oldsource>New passwd can not be empty</oldsource>
        <translation type="obsolete">新密码不可为空</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="592"/>
        <location filename="../BoxPasswdSetting.cpp" line="593"/>
        <location filename="../BoxPasswdSetting.cpp" line="1024"/>
        <location filename="../BoxPasswdSetting.cpp" line="1025"/>
        <source>New password cannot be same as old password</source>
        <oldsource>New passwd cannot be same as old passwd</oldsource>
        <translation type="unfinished">新密码与旧密码不可相同</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="997"/>
        <location filename="../BoxPasswdSetting.cpp" line="999"/>
        <source>New password cannot be empty</source>
        <oldsource>New passwd cannot be empty</oldsource>
        <translation type="unfinished">新密码不可为空</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="612"/>
        <location filename="../BoxPasswdSetting.cpp" line="613"/>
        <location filename="../BoxPasswdSetting.cpp" line="1008"/>
        <location filename="../BoxPasswdSetting.cpp" line="1010"/>
        <source>New password length cannot less than 6</source>
        <oldsource>New passwd length cannot less than 6</oldsource>
        <translation type="unfinished">新密码长度不可小于6</translation>
    </message>
    <message>
        <source>Low</source>
        <translation type="obsolete">低</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="622"/>
        <location filename="../BoxPasswdSetting.cpp" line="623"/>
        <location filename="../BoxPasswdSetting.cpp" line="1035"/>
        <location filename="../BoxPasswdSetting.cpp" line="1037"/>
        <source>Verify password length cannot be empty</source>
        <oldsource>Verify passwd length cannot be empty</oldsource>
        <translation type="unfinished">确认密码不可为空</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="632"/>
        <location filename="../BoxPasswdSetting.cpp" line="633"/>
        <location filename="../BoxPasswdSetting.cpp" line="1046"/>
        <location filename="../BoxPasswdSetting.cpp" line="1048"/>
        <source>Verify password is not same as new password</source>
        <oldsource>Verify passwd is not same as new passwd</oldsource>
        <translation type="unfinished">新密码与确认密码不一致</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="649"/>
        <location filename="../BoxPasswdSetting.cpp" line="651"/>
        <source>Password setting is failed</source>
        <translation>密码设置失败</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="675"/>
        <location filename="../BoxPasswdSetting.cpp" line="677"/>
        <location filename="../BoxPasswdSetting.cpp" line="708"/>
        <location filename="../BoxPasswdSetting.cpp" line="710"/>
        <source>Mount is failed</source>
        <translation>解锁失败</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="757"/>
        <source>text file (*.txt)</source>
        <translation>文本文件(*.txt)</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="758"/>
        <source>all files (*)</source>
        <translation>所有文件 (*)</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="766"/>
        <source>FileName(N):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="767"/>
        <source>FileType:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="768"/>
        <source>Open</source>
        <translation type="unfinished">打开保护箱</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="770"/>
        <source>Look in:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Medium</source>
        <translation type="obsolete">中</translation>
    </message>
    <message>
        <source>High</source>
        <translation type="obsolete">高</translation>
    </message>
    <message>
        <source>Password setting is failed!</source>
        <oldsource>Passwd setting is failed!</oldsource>
        <translation type="obsolete">密码设置失败</translation>
    </message>
    <message>
        <source>Mount is failed!</source>
        <translation type="obsolete">解锁失败</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="754"/>
        <source>chose your file </source>
        <translation>选择密钥文件</translation>
    </message>
    <message>
        <source>text file (*.txt);; all files (*);; </source>
        <oldsource>text file (*.xls *.txt);; all files (*.*);; </oldsource>
        <translation type="obsolete">文本文件(*.txt);; 所有文件 (*);;</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="874"/>
        <location filename="../BoxPasswdSetting.cpp" line="876"/>
        <source>The secret key file path can not be empty</source>
        <translation type="unfinished">密钥路径不可为空</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="927"/>
        <location filename="../BoxPasswdSetting.cpp" line="929"/>
        <source>The secret key file is not exit</source>
        <translation>密钥文件不存在</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="946"/>
        <location filename="../BoxPasswdSetting.cpp" line="948"/>
        <source>The secret key file is unreadable</source>
        <translation>密钥文件没有读权限</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="978"/>
        <location filename="../BoxPasswdSetting.cpp" line="980"/>
        <source>The secret key file is wrong</source>
        <translation>密钥文件错误</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1064"/>
        <location filename="../BoxPasswdSetting.cpp" line="1066"/>
        <source>Set password by secret key file failed</source>
        <translation type="unfinished">密钥设置密码失败</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1099"/>
        <source>Ok</source>
        <translation type="unfinished">确定</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1102"/>
        <source>Password setting is successful!</source>
        <translation type="unfinished">密码修改成功！</translation>
    </message>
    <message>
        <source>Invaild name</source>
        <translation type="vanished">不允许的特殊字符</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1162"/>
        <source>Invaild password</source>
        <translation>不允许的特殊字符</translation>
    </message>
    <message>
        <source>Low password strength</source>
        <translation type="obsolete">密码强度低</translation>
    </message>
    <message>
        <source>Medium password strength</source>
        <translation type="obsolete">密码强度中</translation>
    </message>
    <message>
        <source>High password strength</source>
        <translation type="obsolete">密码强度高</translation>
    </message>
</context>
<context>
    <name>BoxRenameDialog</name>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="74"/>
        <source>Rename</source>
        <translation type="unfinished">重命名</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="131"/>
        <location filename="../BoxRenameDialog.cpp" line="133"/>
        <location filename="../BoxRenameDialog.cpp" line="136"/>
        <location filename="../BoxRenameDialog.cpp" line="443"/>
        <source>Confirm</source>
        <translation type="unfinished">确认</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="132"/>
        <location filename="../BoxRenameDialog.cpp" line="134"/>
        <location filename="../BoxRenameDialog.cpp" line="137"/>
        <location filename="../BoxRenameDialog.cpp" line="444"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="155"/>
        <source>Name</source>
        <translation type="unfinished">名称</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="156"/>
        <source>New Name</source>
        <translation type="unfinished">新名称</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="157"/>
        <source>Password</source>
        <translation type="unfinished">密码</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="231"/>
        <location filename="../BoxRenameDialog.cpp" line="232"/>
        <source>Password length can not be higer than 32</source>
        <translation type="unfinished">密码长度不可高于32位</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="285"/>
        <location filename="../BoxRenameDialog.cpp" line="287"/>
        <source>Box name cannot be empty</source>
        <oldsource>Box name cannot be empty!</oldsource>
        <translation type="unfinished">保护箱名称不可为空</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="356"/>
        <location filename="../BoxRenameDialog.cpp" line="357"/>
        <source>Password is error</source>
        <translation type="unfinished">密码错误</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="378"/>
        <location filename="../BoxRenameDialog.cpp" line="379"/>
        <source>File safe rename failed</source>
        <translation type="unfinished">重命名保护箱失败</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="394"/>
        <location filename="../BoxRenameDialog.cpp" line="396"/>
        <source>Mount is failed</source>
        <translation type="unfinished">解锁失败</translation>
    </message>
    <message>
        <source>Passwd cannot be empty!</source>
        <translation type="obsolete">密码不可为空</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="297"/>
        <location filename="../BoxRenameDialog.cpp" line="298"/>
        <source>The new name cannot be the same as the original name</source>
        <translation type="unfinished">新名称不能与原名称相同</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="310"/>
        <location filename="../BoxRenameDialog.cpp" line="311"/>
        <source>File Safe already exists</source>
        <translation type="unfinished">保护箱已存在</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="335"/>
        <location filename="../BoxRenameDialog.cpp" line="336"/>
        <source>Box umount failed</source>
        <translation type="unfinished">锁定保护箱失败</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="321"/>
        <source>Password can not be empty</source>
        <oldsource>Password can not be empty!</oldsource>
        <translation type="unfinished">密码不可为空</translation>
    </message>
    <message>
        <source>Password is error!</source>
        <oldsource>Passwd is error!</oldsource>
        <translation type="obsolete">密码错误</translation>
    </message>
    <message>
        <source>File safe rename failed!</source>
        <translation type="obsolete">重命名保护箱失败</translation>
    </message>
    <message>
        <source>Mount is failed!</source>
        <translation type="obsolete">解锁失败</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="425"/>
        <location filename="../BoxRenameDialog.cpp" line="426"/>
        <source>Invaild name</source>
        <translation type="unfinished">不允许的特殊字符</translation>
    </message>
</context>
<context>
    <name>BoxTableModel</name>
    <message>
        <source>name</source>
        <translation type="vanished">名称</translation>
    </message>
</context>
<context>
    <name>CTitleBar</name>
    <message>
        <location filename="../TitleBar.cpp" line="136"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../TitleBar.cpp" line="141"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../TitleBar.cpp" line="146"/>
        <source>Menu</source>
        <translation>菜单</translation>
    </message>
    <message>
        <location filename="../TitleBar.cpp" line="151"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
</context>
<context>
    <name>FirstCreatBoxMessageBox</name>
    <message>
        <source>File Safe</source>
        <translation type="vanished">文件保护箱</translation>
    </message>
    <message>
        <source>This is your first creat the protective box,please save your file!</source>
        <translation type="vanished">请妥善保存密钥文件，如果忘记密码，可以使用该密钥文件进行密码找回。</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="37"/>
        <source>Please keep the key file properly. If you forget the password, you can use the key file to retrieve the password</source>
        <translation type="unfinished">请妥善保存密钥文件，如果忘记密码，可以使用该密钥文件进行密码找回。</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="38"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="52"/>
        <source>save key file</source>
        <translation>保存密钥</translation>
    </message>
    <message>
        <source>key file(*.txt)</source>
        <translation type="vanished">密钥文件（*.txt）</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="59"/>
        <source>FileName(N):</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="60"/>
        <source>FileType:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="61"/>
        <source>Save(S)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="62"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="145"/>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="157"/>
        <source>critical</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="145"/>
        <source>save path failed</source>
        <translation>保存文件失败</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">是</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="157"/>
        <source>Disallowed special characters</source>
        <translation type="unfinished">不允许的特殊字符</translation>
    </message>
    <message>
        <source>Save Path</source>
        <translation type="vanished">保存文件</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">确认</translation>
    </message>
</context>
<context>
    <name>ModuleSwitchButton</name>
    <message>
        <location filename="../ModuleSwitchButton.cpp" line="44"/>
        <source>Set by password</source>
        <translation>通过密码设置</translation>
    </message>
    <message>
        <location filename="../ModuleSwitchButton.cpp" line="45"/>
        <source>Set by secret key</source>
        <translation>通过密钥设置</translation>
    </message>
</context>
<context>
    <name>PamAuthenticDialog</name>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="185"/>
        <location filename="../PamAuthenticDialog.cpp" line="188"/>
        <location filename="../PamAuthenticDialog.cpp" line="430"/>
        <source>User authentication is required to perform this operation</source>
        <translation type="unfinished">执行此次操作需要用户认证</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="196"/>
        <location filename="../PamAuthenticDialog.cpp" line="197"/>
        <source>Enter the user password to allow this operation</source>
        <translation type="unfinished">输入用户密码以允许此次操作</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="216"/>
        <location filename="../PamAuthenticDialog.cpp" line="217"/>
        <location filename="../PamAuthenticDialog.cpp" line="219"/>
        <location filename="../PamAuthenticDialog.cpp" line="403"/>
        <source>Authenticate</source>
        <oldsource>Authorization</oldsource>
        <translation type="unfinished">授权</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="278"/>
        <source>Password length can not be higer than 32</source>
        <translation type="unfinished">密码长度不可高于32位</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="417"/>
        <source>You have %1 more tries!</source>
        <translation type="unfinished">你还有%1次尝试的机会</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="386"/>
        <location filename="../PamAuthenticDialog.cpp" line="387"/>
        <source>Password can not be empty</source>
        <oldsource>Password can not be empty!</oldsource>
        <translation type="unfinished">密码不可为空</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="421"/>
        <source>Unable to login using a biometric device!</source>
        <translation type="unfinished">无法使用生物设备进行登录！</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="464"/>
        <location filename="../PamAuthenticDialog.cpp" line="465"/>
        <source>Password authentication failed</source>
        <translation type="unfinished">密码认证失败</translation>
    </message>
    <message>
        <source>Wrong password</source>
        <translation type="obsolete">密码认证失败</translation>
    </message>
    <message>
        <source>authorization</source>
        <translation type="obsolete">授权</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="222"/>
        <location filename="../PamAuthenticDialog.cpp" line="223"/>
        <source>Cancel</source>
        <translation type="unfinished">取消</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="225"/>
        <location filename="../PamAuthenticDialog.cpp" line="226"/>
        <source>Biometric authentication</source>
        <oldsource>Biometric</oldsource>
        <translation type="unfinished">生物识别验证</translation>
    </message>
</context>
<context>
    <name>PasswdAuthDialog</name>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="62"/>
        <source>Open</source>
        <translation>打开保护箱</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="65"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="113"/>
        <source>Name</source>
        <translation>名称</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="114"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="141"/>
        <location filename="../PasswdAuthDialog.cpp" line="143"/>
        <location filename="../PasswdAuthDialog.cpp" line="145"/>
        <location filename="../PasswdAuthDialog.cpp" line="328"/>
        <source>Confirm</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="142"/>
        <location filename="../PasswdAuthDialog.cpp" line="144"/>
        <location filename="../PasswdAuthDialog.cpp" line="146"/>
        <location filename="../PasswdAuthDialog.cpp" line="329"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="207"/>
        <source>Password length can not be higer than 32</source>
        <translation type="unfinished">密码长度不可高于32位</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="261"/>
        <location filename="../PasswdAuthDialog.cpp" line="262"/>
        <source>umount is error</source>
        <translation>取消挂载失败</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="273"/>
        <location filename="../PasswdAuthDialog.cpp" line="274"/>
        <source>Password can not be empty</source>
        <translation type="unfinished">密码不可为空</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="286"/>
        <location filename="../PasswdAuthDialog.cpp" line="287"/>
        <source>Password is error</source>
        <translation type="unfinished">密码错误</translation>
    </message>
    <message>
        <source>umount is error!</source>
        <translation type="vanished">锁定失败</translation>
    </message>
    <message>
        <source>Password can not be empty!</source>
        <oldsource>Passwd can not be empty!</oldsource>
        <translation type="obsolete">密码不可为空</translation>
    </message>
    <message>
        <source>Password is error!</source>
        <translation type="vanished">密码错误</translation>
    </message>
</context>
<context>
    <name>PasswdAuthMessagebox</name>
    <message>
        <source>Do you confirm to delete the box</source>
        <translation type="vanished">你确认删除保护箱吗？</translation>
    </message>
    <message>
        <source>Create a protective box</source>
        <translation type="obsolete">新建保护箱</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="155"/>
        <source>Delete the file safe %1 permanently?</source>
        <oldsource>Are you sure you want to delete the file s type=&quot;unfinished&quot;afe %1 permanently?</oldsource>
        <translation type="unfinished">你确认要永久删除保护箱%1吗？</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="194"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="197"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="199"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="390"/>
        <source>Confirm</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="195"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="198"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="200"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="391"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="196"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="272"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="273"/>
        <source>Password length can not be higer than 32</source>
        <translation type="unfinished">密码长度不可高于32位</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="354"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="355"/>
        <source>umount is error</source>
        <oldsource>umount is error!</oldsource>
        <translation type="unfinished">锁定失败</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="342"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="343"/>
        <source>Password can not be empty</source>
        <oldsource>Password can not be empty!</oldsource>
        <translation type="unfinished">密码不可为空</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="373"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="374"/>
        <source>Wrong password</source>
        <oldsource>Password is error!</oldsource>
        <translation type="unfinished">密码错误</translation>
    </message>
</context>
<context>
    <name>UmountBoxDialog</name>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="48"/>
        <source>After the file safe is locked, the content of the file in use may be lost. Please save it first!</source>
        <translation type="unfinished">保护箱锁定后可能会造成正在使用的文件内容丢失，请先保存文件！</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="55"/>
        <location filename="../UmountBoxDialog.cpp" line="87"/>
        <source>Lock</source>
        <translation>锁定保护箱</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="137"/>
        <location filename="../UmountBoxDialog.cpp" line="138"/>
        <source>There are files in the file safe that are being occupied and need to be unlocked to lock</source>
        <translation type="unfinished">当前保护箱中有文件正在被占用,需要解除占用才能锁定</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="166"/>
        <location filename="../UmountBoxDialog.cpp" line="169"/>
        <location filename="../UmountBoxDialog.cpp" line="172"/>
        <location filename="../UmountBoxDialog.cpp" line="274"/>
        <location filename="../UmountBoxDialog.cpp" line="275"/>
        <location filename="../UmountBoxDialog.cpp" line="276"/>
        <source>Hide</source>
        <translation type="unfinished">隐藏</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="267"/>
        <location filename="../UmountBoxDialog.cpp" line="268"/>
        <location filename="../UmountBoxDialog.cpp" line="269"/>
        <source>Display</source>
        <translation type="unfinished">显示</translation>
    </message>
    <message>
        <source>*Forced locking will cause file loss. Please save the file first!</source>
        <translation type="obsolete">*强制锁定会造成文件的丢失，请先保存文件！</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="49"/>
        <source>Enforce</source>
        <translation type="unfinished">继续锁定</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="164"/>
        <location filename="../UmountBoxDialog.cpp" line="167"/>
        <location filename="../UmountBoxDialog.cpp" line="170"/>
        <location filename="../UmountBoxDialog.cpp" line="294"/>
        <source>Mandatory lock</source>
        <translation type="unfinished">强制锁定</translation>
    </message>
    <message>
        <source>Please release these files which are openning!</source>
        <translation type="vanished">当前保护箱中有文件正在被占用,需要解除占用才能锁定</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="158"/>
        <location filename="../UmountBoxDialog.cpp" line="305"/>
        <source>Files being occupied (%1)</source>
        <oldsource>files which are openning (%1)</oldsource>
        <translation type="unfinished">正在被占用的文件 (%1)</translation>
    </message>
    <message>
        <source>files which are openning!</source>
        <translation type="vanished">正在被打开的文件</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="315"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="50"/>
        <location filename="../UmountBoxDialog.cpp" line="165"/>
        <location filename="../UmountBoxDialog.cpp" line="168"/>
        <location filename="../UmountBoxDialog.cpp" line="171"/>
        <location filename="../UmountBoxDialog.cpp" line="295"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
</context>
</TS>
