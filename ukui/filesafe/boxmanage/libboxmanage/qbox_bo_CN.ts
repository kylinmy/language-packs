<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>BioProxy</name>
    <message>
        <location filename="../BioProxy.cpp" line="198"/>
        <source>FingerPrint</source>
        <translation>མཛུབ་རིས།</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="200"/>
        <source>FingerVein</source>
        <translation>མཛུབ་རིས་ལམ།</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="202"/>
        <source>Iris</source>
        <translation>འཇའ་སྐྱི།</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="204"/>
        <source>Face</source>
        <translation>ངོ་གདོང་།</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="206"/>
        <source>VoicePrint</source>
        <translation>སྒྲ་གདངས་ཀྱི་པར་གཞི།</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="208"/>
        <source>QRCode</source>
        <translation>རྩ་གཉིས་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="219"/>
        <source>Unplugging of %1 device detected</source>
        <translation>ཞིབ་དཔྱད་ཚད་ལེན་བྱས་པའི་%1སྒྲིག་ཆས་བླངས་སོང་།</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="225"/>
        <source>%1 device insertion detected</source>
        <translation>%1སྒྲིག་ཆས་འབྲེལ་མཐུད་བྱས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="234"/>
        <source>ukui-biometric-manager</source>
        <translation>ukui-སྐྱེ་དངོས་ཁྱད་རྟགས་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <location filename="../BioProxy.cpp" line="237"/>
        <source>biometric</source>
        <translation>སྐྱེ་དངོས་ཁྱད་རྟགས།</translation>
    </message>
</context>
<context>
    <name>BioWidget</name>
    <message>
        <location filename="../BioWidget.cpp" line="25"/>
        <source>The login options</source>
        <translation>ཐོ་འགོད་ཀྱི་བསལ་འདེམས་ཀྱི་ཇུས་གཞི།</translation>
    </message>
</context>
<context>
    <name>BoxCreateDialog</name>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="32"/>
        <location filename="../BoxCreateDialog.cpp" line="57"/>
        <source>Create</source>
        <translation>སྲུང་སྐྱོབ་སྒམ་གསར་སྐྲུན་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="65"/>
        <source>Encrypt       </source>
        <translation>གསང་གྲངས་       </translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="120"/>
        <source>Name          </source>
        <translation>མིང་།          </translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="121"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="122"/>
        <source>Confirm </source>
        <translation>གསང་གྲངས་གཏན་འཁེལ་བྱ་དགོས། </translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="152"/>
        <location filename="../BoxCreateDialog.cpp" line="154"/>
        <location filename="../BoxCreateDialog.cpp" line="157"/>
        <location filename="../BoxCreateDialog.cpp" line="673"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source> (O)</source>
        <translation type="vanished"> (O)</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="153"/>
        <location filename="../BoxCreateDialog.cpp" line="155"/>
        <location filename="../BoxCreateDialog.cpp" line="158"/>
        <location filename="../BoxCreateDialog.cpp" line="674"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source> (C)</source>
        <translation type="vanished"> (C)</translation>
    </message>
    <message>
        <source>Passwd level</source>
        <translation type="vanished">འགག་སྒོ་ལས་བརྒལ་བའི་ཆུ་ཚད།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="295"/>
        <location filename="../BoxCreateDialog.cpp" line="296"/>
        <location filename="../BoxCreateDialog.cpp" line="309"/>
        <location filename="../BoxCreateDialog.cpp" line="310"/>
        <source>Password length can not be higer than 32</source>
        <translation>གསང་བའི་རིང་ཚད་ནི་32ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="349"/>
        <source>Box name cannot be empty</source>
        <translation>སྒམ་གྱི་མིང་སྟོང་བ་ཡིན་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="359"/>
        <source>Password cannot be empty</source>
        <translation>གསང་གྲངས་སྟོང་པ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="367"/>
        <source>Box name has been exit</source>
        <translation>སྒམ་གྱི་མིང་ཕྱིར་འཐེན་བྱས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="383"/>
        <location filename="../BoxCreateDialog.cpp" line="386"/>
        <location filename="../BoxCreateDialog.cpp" line="452"/>
        <location filename="../BoxCreateDialog.cpp" line="478"/>
        <source>Create box is failed</source>
        <translation>གསར་སྐྲུན་སྒམ་ལ་སྐྱོན་ཤོར་བ།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="400"/>
        <source>Password length can not be less than 6</source>
        <translation>གསང་བའི་རིང་ཚད་ནི་6ལས་ཉུང་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="408"/>
        <source>Confirm password cannot be empty</source>
        <translation>གཏན་འཁེལ་བྱས་པའི་གསང་གྲངས་ནི་སྟོང་བ་</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="419"/>
        <source>Password is not same as verify password</source>
        <translation>གསང་གྲངས་དང་ཞིབ་བཤེར་བྱས་པའི་གསང་གྲངས་མི་འདྲ།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="437"/>
        <source>Create globalKey failed</source>
        <translation>སའི་གོ་ལ་ཧྲིལ་པོའི་བོང་བུ་གསར་སྐྲུན་བྱེད་པར</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="553"/>
        <location filename="../BoxCreateDialog.cpp" line="554"/>
        <location filename="../BoxCreateDialog.cpp" line="647"/>
        <source>Invaild name</source>
        <translation>མི་ཆོག་པའི་ཁྱད་པར་ཅན་གྱི་ཡིག་རྟགས།</translation>
    </message>
    <message>
        <location filename="../BoxCreateDialog.cpp" line="581"/>
        <location filename="../BoxCreateDialog.cpp" line="582"/>
        <location filename="../BoxCreateDialog.cpp" line="652"/>
        <source>Invaild password</source>
        <translation>མི་ཆོག་པའི་ཁྱད་པར་ཅན་གྱི་ཡིག་རྟགས།</translation>
    </message>
    <message>
        <source>Low password strength</source>
        <translation type="vanished">གསང་གྲངས་ཀྱི་སྟོབས་ཤུགས་ཆུང་བ།</translation>
    </message>
    <message>
        <source>Medium password strength</source>
        <translation type="vanished">འབྲིང་རིམ་གྱི་གསང་གྲངས་སྟོབས་ཤུགས།</translation>
    </message>
    <message>
        <source>High password strength</source>
        <translation type="vanished">གསང་གྲངས་ཀྱི་སྟོབས་ཤུགས་ཆེ་བ།</translation>
    </message>
</context>
<context>
    <name>BoxPasswdSetting</name>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="40"/>
        <location filename="../BoxPasswdSetting.cpp" line="99"/>
        <source>Password setting</source>
        <translation>སྲུང་སྐྱོབ་སྒམ་གྱི་གསང་གྲངས་བཟོ་བཅོས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="158"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="159"/>
        <location filename="../BoxPasswdSetting.cpp" line="418"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="160"/>
        <source>New Password</source>
        <translation>གསང་གྲངས་གསར་པ།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="161"/>
        <source>Confirm     </source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།     </translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="197"/>
        <location filename="../BoxPasswdSetting.cpp" line="199"/>
        <location filename="../BoxPasswdSetting.cpp" line="201"/>
        <location filename="../BoxPasswdSetting.cpp" line="1125"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1125"/>
        <source> (O)</source>
        <translation> (O)</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="198"/>
        <location filename="../BoxPasswdSetting.cpp" line="200"/>
        <location filename="../BoxPasswdSetting.cpp" line="202"/>
        <location filename="../BoxPasswdSetting.cpp" line="769"/>
        <location filename="../BoxPasswdSetting.cpp" line="1126"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1126"/>
        <source> (C)</source>
        <translation> (C)</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="216"/>
        <location filename="../BoxPasswdSetting.cpp" line="838"/>
        <source>Please import the secret key file</source>
        <translation>གསང་བའི་ལྡེ་མིག་གི་ཡིག་ཆ་ནང་འདྲེན་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="228"/>
        <source>Import</source>
        <translation>ནང་འདྲེན་</translation>
    </message>
    <message>
        <source>Password level</source>
        <translation type="vanished">གསང་བའི་རིམ་པ།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="434"/>
        <source>Secret key</source>
        <translation>གསང་བའི་ལྡེ་མིག</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="525"/>
        <location filename="../BoxPasswdSetting.cpp" line="526"/>
        <source>Password can not be empty</source>
        <translation>གསང་གྲངས་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="542"/>
        <location filename="../BoxPasswdSetting.cpp" line="543"/>
        <location filename="../BoxPasswdSetting.cpp" line="900"/>
        <location filename="../BoxPasswdSetting.cpp" line="902"/>
        <source>Box umount failed</source>
        <translation>སྲུང་སྐྱོབ་སྒམ་ཆུང་གཏན་འཁེལབྱས་པ་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="570"/>
        <location filename="../BoxPasswdSetting.cpp" line="571"/>
        <location filename="../BoxPasswdSetting.cpp" line="581"/>
        <location filename="../BoxPasswdSetting.cpp" line="582"/>
        <source>Password is error</source>
        <translation>གསང་གྲངས་ནི་ནོར་འཁྲུལ་ཡིན།</translation>
    </message>
    <message>
        <source>New password can not be empty</source>
        <translation type="vanished">གསང་གྲངས་གསར་པ་སྟོང་པར་འགྱུར་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="592"/>
        <location filename="../BoxPasswdSetting.cpp" line="593"/>
        <location filename="../BoxPasswdSetting.cpp" line="1024"/>
        <location filename="../BoxPasswdSetting.cpp" line="1025"/>
        <source>New password cannot be same as old password</source>
        <translation>གསང་གྲངས་གསར་པ་དང་གསང་གྲངས་རྙིང་པ་གཅིག་མཚུངས་ཡོང་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="997"/>
        <location filename="../BoxPasswdSetting.cpp" line="999"/>
        <source>New password cannot be empty</source>
        <translation>གསང་གྲངས་གསར་པ་སྟོང་པ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="612"/>
        <location filename="../BoxPasswdSetting.cpp" line="613"/>
        <location filename="../BoxPasswdSetting.cpp" line="1008"/>
        <location filename="../BoxPasswdSetting.cpp" line="1010"/>
        <source>New password length cannot less than 6</source>
        <translation>གསང་གྲངས་གསར་པའི་རིང་ཚད་6ལས་ཉུང་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="385"/>
        <location filename="../BoxPasswdSetting.cpp" line="386"/>
        <location filename="../BoxPasswdSetting.cpp" line="393"/>
        <location filename="../BoxPasswdSetting.cpp" line="394"/>
        <location filename="../BoxPasswdSetting.cpp" line="401"/>
        <location filename="../BoxPasswdSetting.cpp" line="402"/>
        <source>Password length can not be higer than 32</source>
        <translation>གསང་བའི་རིང་ཚད་ནི་32ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="622"/>
        <location filename="../BoxPasswdSetting.cpp" line="623"/>
        <location filename="../BoxPasswdSetting.cpp" line="1035"/>
        <location filename="../BoxPasswdSetting.cpp" line="1037"/>
        <source>Verify password length cannot be empty</source>
        <translation>གསང་གྲངས་ཀྱི་རིང་ཚད་ལ་ཞིབ་བཤེར་བྱས་ན་སྟོང་</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="632"/>
        <location filename="../BoxPasswdSetting.cpp" line="633"/>
        <location filename="../BoxPasswdSetting.cpp" line="1046"/>
        <location filename="../BoxPasswdSetting.cpp" line="1048"/>
        <source>Verify password is not same as new password</source>
        <translation>གསང་གྲངས་ཞིབ་བཤེར་བྱ་རྒྱུ་དེ་གསང་གྲངས་གསར་པ་དང་མི་འདྲ།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="649"/>
        <location filename="../BoxPasswdSetting.cpp" line="651"/>
        <source>Password setting is failed</source>
        <translation>གསང་གྲངས་སྒྲིག་ཆས་ལ་སྐྱོན་ཤོར་བ།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="675"/>
        <location filename="../BoxPasswdSetting.cpp" line="677"/>
        <location filename="../BoxPasswdSetting.cpp" line="708"/>
        <location filename="../BoxPasswdSetting.cpp" line="710"/>
        <source>Mount is failed</source>
        <translation>ཟྭ་འབྱེད་པ་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="754"/>
        <source>chose your file </source>
        <translation>གསང་བའི་ཡིག་ཆ་འདེམས་པ། </translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="757"/>
        <source>text file (*.txt)</source>
        <translation>ཡིག་ཆའི་ཡིག་ཚགས་(*.txt)</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="758"/>
        <source>all files (*)</source>
        <translation>ཡིག་ཆ་ཡོད་ཚད་(*)</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="766"/>
        <source>FileName(N):</source>
        <translation>FileName(N):</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="767"/>
        <source>FileType:</source>
        <translation>ཡིག་ཆའི་རིགས་དབྱིབས་ནི།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="768"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="770"/>
        <source>Look in:</source>
        <translation>ནང་དུ་ལྟོས་དང་།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="874"/>
        <location filename="../BoxPasswdSetting.cpp" line="876"/>
        <source>The secret key file path can not be empty</source>
        <translation>གསང་བའི་འགག་རྩའི་ཡིག་ཚགས་ཀྱི་ལམ་བུ་སྟོང་པ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="927"/>
        <location filename="../BoxPasswdSetting.cpp" line="929"/>
        <source>The secret key file is not exit</source>
        <translation>གསང་བའི་ཡིག་ཆ་མི་འདུག</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="946"/>
        <location filename="../BoxPasswdSetting.cpp" line="948"/>
        <source>The secret key file is unreadable</source>
        <translation>གསང་བའི་ལྡེ་མིག་གི་ཡིག་ཆ་ནི་ལྟ་མི་ཐུབ་པ་ཞིག་ཡིན།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="978"/>
        <location filename="../BoxPasswdSetting.cpp" line="980"/>
        <source>The secret key file is wrong</source>
        <translation>གསང་བའི་ལྡེ་མིག་གི་ཡིག་ཆ་ནི་ནོར་འཁྲུལ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1064"/>
        <location filename="../BoxPasswdSetting.cpp" line="1066"/>
        <source>Set password by secret key file failed</source>
        <translation>གསང་བའི་ལྡེ་མིག་གི་ཡིག་ཚགས་ལ་བརྟེན་ནས་གསང་གྲངས་གཏན་འཁེལ</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1099"/>
        <source>Ok</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1102"/>
        <source>Password setting is successful!</source>
        <translation>གསང་གྲངས་བཀོད་སྒྲིག་ལེགས་འགྲུབ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../BoxPasswdSetting.cpp" line="1162"/>
        <source>Invaild password</source>
        <translation>མི་ཆོག་པའི་ཁྱད་པར་ཅན་གྱི་ཡིག་རྟགས།</translation>
    </message>
    <message>
        <source>Low password strength</source>
        <translation type="vanished">གསང་གྲངས་ཀྱི་སྟོབས་ཤུགས་ཆུང་བ།</translation>
    </message>
    <message>
        <source>Medium password strength</source>
        <translation type="vanished">འབྲིང་རིམ་གྱི་གསང་གྲངས་སྟོབས་ཤུགས།</translation>
    </message>
    <message>
        <source>High password strength</source>
        <translation type="vanished">གསང་གྲངས་ཀྱི་སྟོབས་ཤུགས་ཆེ་བ།</translation>
    </message>
</context>
<context>
    <name>BoxRenameDialog</name>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="74"/>
        <source>Rename</source>
        <translation>མིང་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="131"/>
        <location filename="../BoxRenameDialog.cpp" line="133"/>
        <location filename="../BoxRenameDialog.cpp" line="136"/>
        <location filename="../BoxRenameDialog.cpp" line="443"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source> (O)</source>
        <translation type="vanished"> (O)</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="132"/>
        <location filename="../BoxRenameDialog.cpp" line="134"/>
        <location filename="../BoxRenameDialog.cpp" line="137"/>
        <location filename="../BoxRenameDialog.cpp" line="444"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source> (C)</source>
        <translation type="vanished"> (C)</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="155"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="156"/>
        <source>New Name</source>
        <translation>མིང་གསར་པ།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="157"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="231"/>
        <location filename="../BoxRenameDialog.cpp" line="232"/>
        <source>Password length can not be higer than 32</source>
        <translation>གསང་བའི་རིང་ཚད་ནི་32ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="285"/>
        <location filename="../BoxRenameDialog.cpp" line="287"/>
        <source>Box name cannot be empty</source>
        <translation>སྒམ་གྱི་མིང་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="297"/>
        <location filename="../BoxRenameDialog.cpp" line="298"/>
        <source>The new name cannot be the same as the original name</source>
        <translation>མིང་གསར་པ་དང་མ་ཡིག་གི་མིང་གཅིག་མཚུངས་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="310"/>
        <location filename="../BoxRenameDialog.cpp" line="311"/>
        <source>File Safe already exists</source>
        <translation>ཡིག་ཆའི་བདེ་འཇགས་གནས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="321"/>
        <source>Password can not be empty</source>
        <translation>གསང་གྲངས་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="335"/>
        <location filename="../BoxRenameDialog.cpp" line="336"/>
        <source>Box umount failed</source>
        <translation>སྒམ་ཆུང་ལ་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="356"/>
        <location filename="../BoxRenameDialog.cpp" line="357"/>
        <source>Password is error</source>
        <translation>གསང་གྲངས་ནི་ནོར་འཁྲུལ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="378"/>
        <location filename="../BoxRenameDialog.cpp" line="379"/>
        <source>File safe rename failed</source>
        <translation>ཡིག་ཚགས་ཀྱི་བདེ་འཇགས་མིང་ལ་ཕམ་ཉེས་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="394"/>
        <location filename="../BoxRenameDialog.cpp" line="396"/>
        <source>Mount is failed</source>
        <translation>རི་བོ་ཆེན་པོ་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../BoxRenameDialog.cpp" line="425"/>
        <location filename="../BoxRenameDialog.cpp" line="426"/>
        <source>Invaild name</source>
        <translation>གོ་མི་ཐུབ་པའི་མིང་།</translation>
    </message>
</context>
<context>
    <name>CTitleBar</name>
    <message>
        <location filename="../TitleBar.cpp" line="136"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../TitleBar.cpp" line="141"/>
        <source>Minimize</source>
        <translation>ཆེས་ཆུང་དུ་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../TitleBar.cpp" line="146"/>
        <source>Menu</source>
        <translation>གཙོ་ངོས།</translation>
    </message>
    <message>
        <location filename="../TitleBar.cpp" line="151"/>
        <source>Return</source>
        <translation>ཕྱིར་སློག་པ།</translation>
    </message>
</context>
<context>
    <name>FirstCreatBoxMessageBox</name>
    <message>
        <source>question</source>
        <translation type="vanished">གནད་དོན།</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="37"/>
        <source>Please keep the key file properly. If you forget the password, you can use the key file to retrieve the password</source>
        <translation>ཁྱོད་ཀྱིས་ལྡེ་མིག་གི་ཡིག་ཆ་ཉར་ཚགས་ཡག་པོ་བྱེད་རོགས། གལ་ཏེ་ཁྱོད་ཀྱིས་གསང་གྲངས་བརྗེད་སོང་ན། ཁྱོད་ཀྱིས་ལྡེ་མིག་གི་ཡིག་ཆ་བཀོལ་ནས་གསང་གྲངས་ཕྱིར་ལེན་ཐུབ།</translation>
    </message>
    <message>
        <source>warning</source>
        <translation type="vanished">ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="38"/>
        <source>Save</source>
        <translation>ཉར་ཚགས།</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="52"/>
        <source>save key file</source>
        <translation>གསང་བའི་ལྡེ་མིག་ཉར་ཚགས་བྱོས།</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="59"/>
        <source>FileName(N):</source>
        <translation>FileName(N):</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="60"/>
        <source>FileType:</source>
        <translation>ཡིག་ཆའི་རིགས་དབྱིབས་ནི།</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="61"/>
        <source>Save(S)</source>
        <translation>གྲོན་ཆུང་བྱ་དགོས། (S)</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="62"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="145"/>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="157"/>
        <source>critical</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="145"/>
        <source>save path failed</source>
        <translation>ཡིག་ཆ་ཉར་ཚགས་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../FirstCreatBoxMessageBox.cpp" line="157"/>
        <source>Disallowed special characters</source>
        <translation>དམིགས་བསལ་གྱི་མི་སྣ་མེད་པར་བཟོ་བ།</translation>
    </message>
</context>
<context>
    <name>ModuleSwitchButton</name>
    <message>
        <location filename="../ModuleSwitchButton.cpp" line="44"/>
        <source>Set by password</source>
        <translation>གསང་གྲངས་ལ་བརྟེན་ནས་སྒྲིག་བཀོས་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../ModuleSwitchButton.cpp" line="45"/>
        <source>Set by secret key</source>
        <translation>གསང་བའི་ལྡེ་མིག་གིས་གཏན་འཁེལ་བྱས་པ།</translation>
    </message>
</context>
<context>
    <name>PamAuthenticDialog</name>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="185"/>
        <location filename="../PamAuthenticDialog.cpp" line="188"/>
        <location filename="../PamAuthenticDialog.cpp" line="430"/>
        <source>User authentication is required to perform this operation</source>
        <translation>ཐེངས་འདིའི་བྱ་སྤྱོད་ལ་སྤྱོད་མཁན་གྱི་བདེན་དཔང་ར་སྤྲོད་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="196"/>
        <location filename="../PamAuthenticDialog.cpp" line="197"/>
        <source>Enter the user password to allow this operation</source>
        <translation>སྤྱོད་མཁན་གྱི་གསང་གྲངས་ནང་འཇུག་བྱས་ནས་བཀོལ་སྤྱོད་འདི་བྱེད་དུ་</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="216"/>
        <location filename="../PamAuthenticDialog.cpp" line="217"/>
        <location filename="../PamAuthenticDialog.cpp" line="219"/>
        <location filename="../PamAuthenticDialog.cpp" line="403"/>
        <source>Authenticate</source>
        <translation>བདེན་དཔང་ར་སྤྲོད་བྱ་དགོས</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="222"/>
        <location filename="../PamAuthenticDialog.cpp" line="223"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="225"/>
        <location filename="../PamAuthenticDialog.cpp" line="226"/>
        <source>Biometric authentication</source>
        <translation>སྐྱེ་དངོས་དབྱེ་འབྱེད་ཀྱི་བདེན་དཔང་ར་སྤྲོད།</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="278"/>
        <source>Password length can not be higer than 32</source>
        <translation>གསང་བའི་རིང་ཚད་ནི་32ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="386"/>
        <location filename="../PamAuthenticDialog.cpp" line="387"/>
        <source>Password can not be empty</source>
        <translation>གསང་གྲངས་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="417"/>
        <source>You have %1 more tries!</source>
        <translation>ཁྱེད་ཚོར་ད་དུང་ཚོད་ལྟ་ཐེངས་1བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="421"/>
        <source>Unable to login using a biometric device!</source>
        <translation>སྐྱེ་དངོས་དབྱེ་འབྱེད་སྒྲིག་ཆས་བཀོལ་ནས་ཐོ་འགོད་བྱེད་ཐབས་བྲལ།</translation>
    </message>
    <message>
        <location filename="../PamAuthenticDialog.cpp" line="464"/>
        <location filename="../PamAuthenticDialog.cpp" line="465"/>
        <source>Password authentication failed</source>
        <translation>གསང་གྲངས་བདེན་དཔང་ར་སྤྲོད་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
</context>
<context>
    <name>PasswdAuthDialog</name>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="62"/>
        <source>Open</source>
        <translation>སྲུང་སྐྱོབ་སྒམ་སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="65"/>
        <source>Rename</source>
        <translation>མིང་བསྐྱར་འདོགས།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="113"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="114"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="141"/>
        <location filename="../PasswdAuthDialog.cpp" line="143"/>
        <location filename="../PasswdAuthDialog.cpp" line="145"/>
        <location filename="../PasswdAuthDialog.cpp" line="328"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="207"/>
        <source>Password length can not be higer than 32</source>
        <translation>གསང་བའི་རིང་ཚད་ནི་32ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <source> (O)</source>
        <translation type="vanished"> (O)</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="142"/>
        <location filename="../PasswdAuthDialog.cpp" line="144"/>
        <location filename="../PasswdAuthDialog.cpp" line="146"/>
        <location filename="../PasswdAuthDialog.cpp" line="329"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source> (C)</source>
        <translation type="vanished"> (C)</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="261"/>
        <location filename="../PasswdAuthDialog.cpp" line="262"/>
        <source>umount is error</source>
        <translation>འགེལ་ཆས་མེད་པར་བཟོས་པ་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="273"/>
        <location filename="../PasswdAuthDialog.cpp" line="274"/>
        <source>Password can not be empty</source>
        <translation>གསང་གྲངས་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthDialog.cpp" line="286"/>
        <location filename="../PasswdAuthDialog.cpp" line="287"/>
        <source>Password is error</source>
        <translation>གསང་གྲངས་ནི་ནོར་འཁྲུལ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>PasswdAuthMessagebox</name>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="155"/>
        <source>Delete the file safe %1 permanently?</source>
        <translation>དུས་གཏན་དུ་ཡིག་ཆའི་བདེ་འཇགས་ཀྱི་ཨང་གྲངས་1མ བསུབ་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="194"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="197"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="199"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="390"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source> (O)</source>
        <translation type="vanished"> (O)</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="195"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="198"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="200"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="391"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source> (C)</source>
        <translation type="vanished"> (C)</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="196"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="272"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="273"/>
        <source>Password length can not be higer than 32</source>
        <translation>གསང་བའི་རིང་ཚད་ནི་32ལས་བརྒལ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="342"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="343"/>
        <source>Password can not be empty</source>
        <translation>གསང་གྲངས་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="354"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="355"/>
        <source>umount is error</source>
        <translation>ཝུའུ་ཁི་ལན་ནི་ནོར་འཁྲུལ་རེད།</translation>
    </message>
    <message>
        <location filename="../PasswdAuthMessagebox.cpp" line="373"/>
        <location filename="../PasswdAuthMessagebox.cpp" line="374"/>
        <source>Wrong password</source>
        <translation>ནོར་འཁྲུལ་གྱི་གསང་གྲངས</translation>
    </message>
</context>
<context>
    <name>UmountBoxDialog</name>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="48"/>
        <source>After the file safe is locked, the content of the file in use may be lost. Please save it first!</source>
        <translation>ཡིག་ཆའི་ཉེན་འགོག་སྒམ་ལ་ཟྭ་བརྒྱབ་རྗེས་བེད་སྤྱོད་བྱེད་བཞིན་པའི་ཡིག་ཆའི་ནང་དོན་བོར་བརླག་ཏུ་འགྲོ་སྲིད། སྔོན་ལ་ཉར་ཚགས་ཡག་པོ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="49"/>
        <source>Enforce</source>
        <translation>བཙན་ཤེད་ཀྱིས་ལག་</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="50"/>
        <location filename="../UmountBoxDialog.cpp" line="165"/>
        <location filename="../UmountBoxDialog.cpp" line="168"/>
        <location filename="../UmountBoxDialog.cpp" line="171"/>
        <location filename="../UmountBoxDialog.cpp" line="295"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="55"/>
        <location filename="../UmountBoxDialog.cpp" line="87"/>
        <source>Lock</source>
        <translation>སྲུང་སྐྱོབ་སྒམ་ལ་ཟྭ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="137"/>
        <location filename="../UmountBoxDialog.cpp" line="138"/>
        <source>There are files in the file safe that are being occupied and need to be unlocked to lock</source>
        <translation>ཡིག་ཚགས་ཀྱི་ཉེན་འགོག་སྒམ་ནང་དུ་ཡིག་ཆ་བཟུང་ནས་ཟྭ་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <source>*Forced locking will cause file loss. Please save the file first!</source>
        <translation type="vanished">*བཙན་ཤེད་ཀྱིས་ཟྭ་བརྒྱབ་ན་ཡིག་ཚགས་ལ་གྱོང་གུན་བཟོ་སྲིད། སྔོན་ལ་ཡིག་ཆ་དེ་ཉར་ཚགས་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="158"/>
        <location filename="../UmountBoxDialog.cpp" line="305"/>
        <source>Files being occupied (%1)</source>
        <translation>བཟུང་སྤྱོད་བྱེད་བཞིན་པའི་ཡིག་ཆ། (%1)</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="164"/>
        <location filename="../UmountBoxDialog.cpp" line="167"/>
        <location filename="../UmountBoxDialog.cpp" line="170"/>
        <location filename="../UmountBoxDialog.cpp" line="294"/>
        <source>Mandatory lock</source>
        <translation>བཙན་ཤེད་ཀྱིས་ཟྭ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="166"/>
        <location filename="../UmountBoxDialog.cpp" line="169"/>
        <location filename="../UmountBoxDialog.cpp" line="172"/>
        <location filename="../UmountBoxDialog.cpp" line="274"/>
        <location filename="../UmountBoxDialog.cpp" line="275"/>
        <location filename="../UmountBoxDialog.cpp" line="276"/>
        <source>Hide</source>
        <translation>སྦས་སྐུང་བྱེད་</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="267"/>
        <location filename="../UmountBoxDialog.cpp" line="268"/>
        <location filename="../UmountBoxDialog.cpp" line="269"/>
        <source>Display</source>
        <translation>འགྲེམས་སྟོན།</translation>
    </message>
    <message>
        <location filename="../UmountBoxDialog.cpp" line="315"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
</TS>
