<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>Peony::FilesafeMenuPlugin</name>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="75"/>
        <source>Create New FileSafe</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="107"/>
        <source>Password Setting</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠤ᠋ᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="130"/>
        <source>FileSafe Lock</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠢ᠋ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="156"/>
        <source>Rename</source>
        <translation>ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠬᠦ</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="182"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.h" line="21"/>
        <source>Peony-Qt filesafe menu Extension</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠳᠠ ᠵᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ ᠢ᠋ ᠳᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.h" line="22"/>
        <source>filesafe Menu Extension.</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ ᠤ᠋ᠨ ᠲᠤᠪᠶᠤᠭ ᠤ᠋ᠨ ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠤᠨᠤᠭ.</translation>
    </message>
</context>
</TS>
