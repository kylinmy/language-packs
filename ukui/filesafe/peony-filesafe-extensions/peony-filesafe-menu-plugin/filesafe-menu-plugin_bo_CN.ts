<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Peony::FilesafeMenuPlugin</name>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="75"/>
        <source>Create New FileSafe</source>
        <translation>སྲུང་སྐྱོབ་སྒམ་གསར་པ་གསར་སྐྲུན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="107"/>
        <source>Password Setting</source>
        <translation>སྲུང་སྐྱོབ་སྒམ་གྱི་གསང་གྲངས་བཟོ་བཅོས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="130"/>
        <source>FileSafe Lock</source>
        <translation>སྲུང་སྐྱོབ་སྒམ་ལ་ཟྭ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="156"/>
        <source>Rename</source>
        <translation>མིང་བསྐྱར་འདོགས།</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.cpp" line="182"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.h" line="21"/>
        <source>Peony-Qt filesafe menu Extension</source>
        <translation>ཡིག་ཆ་སྲུང་སྐྱོབ་སྒམ་རྒྱ་བསྐྱེད་པ།</translation>
    </message>
    <message>
        <location filename="filesafe-menu-plugin.h" line="22"/>
        <source>filesafe Menu Extension.</source>
        <translation>སྲུང་སྐྱོབ་སྒམ་གྱི་གཙོ་ངོས་ལྷུ་ལག</translation>
    </message>
</context>
</TS>
