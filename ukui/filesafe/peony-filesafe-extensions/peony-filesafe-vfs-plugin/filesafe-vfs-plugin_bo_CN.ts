<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>QObject</name>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="548"/>
        <source>Filesafe</source>
        <translation>སྲུང་སྐྱོབ་སྒམ།</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="747"/>
        <location filename="filesafe-vfs-file.cpp" line="947"/>
        <location filename="filesafe-vfs-file.cpp" line="1182"/>
        <source>Virtual file directories do not support move and copy operations</source>
        <translation>རྟོག་བཟོའི་ཡིག་ཆའི་དཀར་ཆག་གིས་སྤོ་འགུལ་དང་བསྐྱར་དཔར་བཀོལ་སྤྱོད་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="980"/>
        <source>Virtual file directories do not support make symbolic link operations</source>
        <translation>རྟོག་བཟོའི་ཡིག་ཆའི་དཀར་ཆག་གིས་བརྡ་རྟགས་སྦྲེལ་མཐུད་བཀོལ་སྤྱོད་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="1244"/>
        <source>The virtual file system cannot be opened</source>
        <translation>རྟོག་བཟོའི་ཡིག་ཚགས་མ་ལག་གི་ཁ་ཕྱེ་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-plugin.h" line="41"/>
        <source>File-safe vfs of peony</source>
        <translation>peonyཡི་ཡིག་ཆའི་བདེ་འཇགས་VFSབཀོལ་བ།</translation>
    </message>
</context>
</TS>
