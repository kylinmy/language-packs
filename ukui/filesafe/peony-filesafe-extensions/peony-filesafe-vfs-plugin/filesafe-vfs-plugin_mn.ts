<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>Peony::FileSafeVfsRegister</name>
    <message>
        <source>filesafe</source>
        <translation type="vanished">文件保护箱</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>filesafe</source>
        <translation type="vanished">保护箱</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="548"/>
        <source>Filesafe</source>
        <translation>ᠬᠠᠮᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠢᠷᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="980"/>
        <source>Virtual file directories do not support make symbolic link operations</source>
        <translation>ᠵᠢᠱᠢᠮᠡᠭ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠭᠠᠷᠴᠠᠭ ᠳᠡᠮᠳᠡᠭ ᠤ᠋ᠨ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠦᠢᠯᠡᠳᠬᠦ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="747"/>
        <location filename="filesafe-vfs-file.cpp" line="947"/>
        <location filename="filesafe-vfs-file.cpp" line="1182"/>
        <source>Virtual file directories do not support move and copy operations</source>
        <translation>ᠵᠢᠰᠢᠮᠡᠭ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠵᠠᠮ ᠱᠤᠭᠤᠮ ᠨᠢ ᠰᠢᠯᠵᠢᠬᠦᠯᠬᠦ ᠪᠤᠶᠤ ᠺᠤᠫᠢᠳᠠᠬᠤ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-file.cpp" line="1244"/>
        <source>The virtual file system cannot be opened</source>
        <translation>ᠵᠢᠰᠢᠮᠡᠭ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="filesafe-vfs-plugin.h" line="41"/>
        <source>File-safe vfs of peony</source>
        <translation>peony ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠠᠶᠤᠯᠬᠦᠢ VFS ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠬᠡᠷᠡᠭᠯᠡᠨ᠎ᠡ</translation>
    </message>
</context>
</TS>
