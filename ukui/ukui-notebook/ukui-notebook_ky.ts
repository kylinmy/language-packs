<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name></name>
    <message>
        <location filename="../data/ukui-notebook.desktop.in.h" line="1"/>
        <source>Notes</source>
        <translation>Ноталар</translation>
    </message>
</context>
<context>
    <name>About</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Диалог</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">Жөнүндө</translation>
    </message>
    <message>
        <location filename="../../src/about.cpp" line="26"/>
        <source>Notes</source>
        <translation>Ноталар</translation>
    </message>
    <message>
        <source>Version: %1</source>
        <translation type="vanished">Версиясы: %1</translation>
    </message>
    <message>
        <source>Notes is a self-developed sidebar application plug-in, which provides a rich interface, convenient operation and stable functions, aiming at a friendly user experience.</source>
        <translation type="vanished">Ноталар достук колдонуучу тажрыйбасын көздөп, бай интерфейс, ыңгайлуу операция жана туруктуу милдеттерди камсыз кылат, өз алдынча иштелип чыккан сидербар колдонмо плагин болуп саналат.</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">Кызмат &amp;amp: </translation>
    </message>
</context>
<context>
    <name>CustomPushButtonGroup</name>
    <message>
        <location filename="../../src/custom_ui/custom_push_button_group.ui" line="14"/>
        <source>CustomPushButtonGroup</source>
        <translation>CustomPushButtonGroup</translation>
    </message>
</context>
<context>
    <name>EditPage</name>
    <message>
        <location filename="../../src/editPage.cpp" line="93"/>
        <source>Notes</source>
        <translation>Ноталар</translation>
    </message>
    <message>
        <source>Bold</source>
        <translation type="vanished">Кайраттуу</translation>
    </message>
    <message>
        <source>Italic</source>
        <translation type="vanished">Титалик</translation>
    </message>
    <message>
        <source>Underline</source>
        <translation type="vanished">Сызык</translation>
    </message>
    <message>
        <source>Strikeout</source>
        <translation type="vanished">Сокку</translation>
    </message>
    <message>
        <source>Unordered</source>
        <translation type="vanished">Тынымсыз</translation>
    </message>
    <message>
        <source>Ordered</source>
        <translation type="vanished">Буйрук берилген</translation>
    </message>
    <message>
        <source>Font Size</source>
        <translation type="vanished">Шрифт өлчөмү</translation>
    </message>
    <message>
        <source>Font Color</source>
        <translation type="vanished">Шрифт түсү</translation>
    </message>
    <message>
        <source>InsertPicture</source>
        <translation type="vanished">ИнсертПиктура</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="362"/>
        <source>undo</source>
        <translation>жокко</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="363"/>
        <source>redo</source>
        <translation>редо</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="364"/>
        <source>cut</source>
        <translation>кесилген</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="365"/>
        <source>copy</source>
        <translation>көчүрмөсү</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="366"/>
        <source>paste</source>
        <translation>паста</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="367"/>
        <source>copy to newpage</source>
        <translation>жаңы бетке көчүрүү</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="724"/>
        <source>Select an image</source>
        <translation>Сүрөт тандоо</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="726"/>
        <source>JPEG (*.jpg);; GIF (*.gif);; PNG (*.png);; BMP (*.bmp);; All (*)</source>
        <translation>JPEG (*.jpg); GIF (*.gif); ПНГ (*.png); BMP (*.bmp); Баары (*)</translation>
    </message>
</context>
<context>
    <name>Edit_page</name>
    <message>
        <location filename="../../src/editPage.ui" line="26"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <source>14</source>
        <translation type="vanished">14</translation>
    </message>
</context>
<context>
    <name>FontPropertyWidget</name>
    <message>
        <location filename="../../src/font_button/font_property_widget.ui" line="14"/>
        <source>FontPropertyWidget</source>
        <translation>FontPropertyWidget</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="15"/>
        <source>Bold</source>
        <translation>Кайраттуу</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="16"/>
        <source>Italic</source>
        <translation>Титалик</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="17"/>
        <source>Underline</source>
        <translation>Сызык</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="18"/>
        <source>Strikeout</source>
        <translation>Сокку</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="19"/>
        <source>Unordered</source>
        <translation>Тынымсыз</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="20"/>
        <source>Ordered</source>
        <translation>Буйрук берилген</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="21"/>
        <source>InsertPicture</source>
        <translation>ИнсертПиктура</translation>
    </message>
</context>
<context>
    <name>FontTwinButtonGroup</name>
    <message>
        <location filename="../../src/font_button/font_twin_button_group.cpp" line="21"/>
        <source>Font Size</source>
        <translation>Шрифт өлчөмү</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_twin_button_group.cpp" line="22"/>
        <source>Font Color</source>
        <translation>Шрифт түсү</translation>
    </message>
</context>
<context>
    <name>PaletteWidget</name>
    <message>
        <location filename="../../src/paletteWidget.ui" line="26"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/utils/utils.cpp" line="22"/>
        <source>show</source>
        <translation>көрсөтүү</translation>
    </message>
</context>
<context>
    <name>SelectColor</name>
    <message>
        <location filename="../../src/selectColorPage.ui" line="26"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
</context>
<context>
    <name>SetFontColor</name>
    <message>
        <location filename="../../src/setFontColorPage.ui" line="26"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
</context>
<context>
    <name>SetFontSize</name>
    <message>
        <location filename="../../src/setFontSizePage.ui" line="31"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../../src/widget.ui" line="14"/>
        <source>Widget</source>
        <translation>Виджет</translation>
    </message>
    <message>
        <location filename="../../src/widget.ui" line="63"/>
        <source>Note List</source>
        <translation>Эскертүү тизмеси</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="vanished">Жаңы</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="283"/>
        <source>Notes</source>
        <translation>Ноталар</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="616"/>
        <source>Help</source>
        <translation>Жардам</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="617"/>
        <source>About</source>
        <translation>Жөнүндө</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="618"/>
        <source>Empty Note</source>
        <translation>Бош нота</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="620"/>
        <source>Exit</source>
        <translation>Чыгуу</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="653"/>
        <source>Create New Note</source>
        <translation>Жаңы нота түзүү</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1123"/>
        <source>Enter search content</source>
        <translation>Издөө мазмунун киргизүү</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1621"/>
        <location filename="../../src/widget.cpp" line="1635"/>
        <source>delete this note</source>
        <translation>бул нотаны жоготуу</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1622"/>
        <location filename="../../src/widget.cpp" line="1638"/>
        <source>open this note</source>
        <translation>бул нотаны ачуу</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1623"/>
        <location filename="../../src/widget.cpp" line="1641"/>
        <source>clear note list</source>
        <translation>так нота тизмеси</translation>
    </message>
    <message>
        <source>Delete Selected Note</source>
        <translation type="vanished">Тандалган нотаны жоготуу</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="654"/>
        <source>Switch View</source>
        <translation>Көрүү которуу</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Жабуу</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">Минималдуу</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">Меню</translation>
    </message>
    <message>
        <source>Welcome to use Notes.</source>
        <translation type="vanished">Ноталарды колдонууга кош келиңиздер.</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Издөө</translation>
    </message>
    <message>
        <source>[picture]</source>
        <translation type="vanished">[Сүрөт]</translation>
    </message>
</context>
<context>
    <name>emptyNotes</name>
    <message>
        <location filename="../../src/emptyNotes.ui" line="32"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="190"/>
        <source>Are you sure empty notebook?</source>
        <translation>Сиз бош дәптер сөзсүз?</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="251"/>
        <source>No Tips</source>
        <translation>Кеңештер жок</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="305"/>
        <source>cancel</source>
        <translation>Жокко чыгаруу</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="328"/>
        <source>yes</source>
        <translation>Ооба</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.cpp" line="33"/>
        <source>emptyNotes</source>
        <translation>___</translation>
    </message>
</context>
<context>
    <name>fontButton</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Форма</translation>
    </message>
</context>
<context>
    <name>iconViewModeDelegate</name>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="260"/>
        <source>[picture]</source>
        <translation>[Сүрөт]</translation>
    </message>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="334"/>
        <source>Welcome to use Notes.</source>
        <translation>Ноталарды колдонууга кош келиңиздер.</translation>
    </message>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="363"/>
        <source>Today  </source>
        <translation>Бүгүн  </translation>
    </message>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="372"/>
        <source>Yesterday  </source>
        <translation>Кеше  </translation>
    </message>
</context>
<context>
    <name>listViewModeDelegate</name>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="308"/>
        <source>[picture]</source>
        <translation>[Сүрөт]</translation>
    </message>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="313"/>
        <source>Welcome to use Notes.</source>
        <translation>Ноталарды колдонууга кош келиңиздер.</translation>
    </message>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="385"/>
        <source>Today  </source>
        <translation>Бүгүн  </translation>
    </message>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="394"/>
        <source>Yesterday  </source>
        <translation>Кеше  </translation>
    </message>
</context>
<context>
    <name>noteExitWindow</name>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="31"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="83"/>
        <source>Are you sure to exit the note book?</source>
        <translation>Нота китебинен чыгууга сөзсүзбү?</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="104"/>
        <source>Close the desktop note page at the same time</source>
        <translation>Иш столунун нота бетин бир эле учурда жабуу</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="149"/>
        <source>No</source>
        <translation>Жок</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="171"/>
        <source>Yes</source>
        <translation>Ооба</translation>
    </message>
</context>
<context>
    <name>noteHead</name>
    <message>
        <location filename="../../src/noteHead.ui" line="14"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
</context>
<context>
    <name>noteHeadMenu</name>
    <message>
        <location filename="../../src/noteHeadMenu.ui" line="14"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="149"/>
        <source>Open note list</source>
        <translation>Нота тизмесин ачуу</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="150"/>
        <source>Always in the front</source>
        <translation>Ар дайым алдыңкы орунда</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="151"/>
        <source>Delete this note</source>
        <translation>Бул нотаны жоготуу</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="152"/>
        <source>Share</source>
        <translation>Үлүшү</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="99"/>
        <source>Create New Note</source>
        <translation>Жаңы нота түзүү</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="90"/>
        <source>Close</source>
        <translation>Жабуу</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="82"/>
        <location filename="../../src/noteHeadMenu.cpp" line="86"/>
        <source>Menu</source>
        <translation>Меню</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="74"/>
        <source>Palette</source>
        <translation>Палитра</translation>
    </message>
</context>
<context>
    <name>paletteButton</name>
    <message>
        <location filename="../../src/paletteButton.ui" line="32"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
</context>
</TS>
