<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>About</name>
    <message>
        <location filename="../../src/about.cpp" line="26"/>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>CustomPushButtonGroup</name>
    <message>
        <location filename="../../src/custom_ui/custom_push_button_group.ui" line="14"/>
        <source>CustomPushButtonGroup</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EditPage</name>
    <message>
        <location filename="../../src/editPage.cpp" line="93"/>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="362"/>
        <source>undo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="363"/>
        <source>redo</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="364"/>
        <source>cut</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="365"/>
        <source>copy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="366"/>
        <source>paste</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="367"/>
        <source>copy to newpage</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="724"/>
        <source>Select an image</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="726"/>
        <source>JPEG (*.jpg);; GIF (*.gif);; PNG (*.png);; BMP (*.bmp);; All (*)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Edit_page</name>
    <message>
        <location filename="../../src/editPage.ui" line="26"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FontPropertyWidget</name>
    <message>
        <location filename="../../src/font_button/font_property_widget.ui" line="14"/>
        <source>FontPropertyWidget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="15"/>
        <source>Bold</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="16"/>
        <source>Italic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="17"/>
        <source>Underline</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="18"/>
        <source>Strikeout</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="19"/>
        <source>Unordered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="20"/>
        <source>Ordered</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="21"/>
        <source>InsertPicture</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FontTwinButtonGroup</name>
    <message>
        <location filename="../../src/font_button/font_twin_button_group.cpp" line="21"/>
        <source>Font Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_twin_button_group.cpp" line="22"/>
        <source>Font Color</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PaletteWidget</name>
    <message>
        <location filename="../../src/paletteWidget.ui" line="26"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/utils/utils.cpp" line="22"/>
        <source>show</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectColor</name>
    <message>
        <location filename="../../src/selectColorPage.ui" line="26"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetFontColor</name>
    <message>
        <location filename="../../src/setFontColorPage.ui" line="26"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SetFontSize</name>
    <message>
        <location filename="../../src/setFontSizePage.ui" line="31"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../../src/widget.ui" line="14"/>
        <source>Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widget.ui" line="63"/>
        <source>Note List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="283"/>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="616"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="617"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="618"/>
        <source>Empty Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="620"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="653"/>
        <source>Create New Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1123"/>
        <source>Enter search content</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1621"/>
        <location filename="../../src/widget.cpp" line="1635"/>
        <source>delete this note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1622"/>
        <location filename="../../src/widget.cpp" line="1638"/>
        <source>open this note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1623"/>
        <location filename="../../src/widget.cpp" line="1641"/>
        <source>clear note list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="654"/>
        <source>Switch View</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>emptyNotes</name>
    <message>
        <location filename="../../src/emptyNotes.ui" line="32"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="190"/>
        <source>Are you sure empty notebook?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="251"/>
        <source>No Tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="305"/>
        <source>cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="328"/>
        <source>yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.cpp" line="33"/>
        <source>emptyNotes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>iconViewModeDelegate</name>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="260"/>
        <source>[picture]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="334"/>
        <source>Welcome to use Notes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="363"/>
        <source>Today  </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="372"/>
        <source>Yesterday  </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>listViewModeDelegate</name>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="308"/>
        <source>[picture]</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="313"/>
        <source>Welcome to use Notes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="385"/>
        <source>Today  </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="394"/>
        <source>Yesterday  </source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>noteExitWindow</name>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="31"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="83"/>
        <source>Are you sure to exit the note book?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="104"/>
        <source>Close the desktop note page at the same time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="149"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="171"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>noteHead</name>
    <message>
        <location filename="../../src/noteHead.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>noteHeadMenu</name>
    <message>
        <location filename="../../src/noteHeadMenu.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="149"/>
        <source>Open note list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="150"/>
        <source>Always in the front</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="151"/>
        <source>Delete this note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="152"/>
        <source>Share</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="99"/>
        <source>Create New Note</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="90"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="82"/>
        <location filename="../../src/noteHeadMenu.cpp" line="86"/>
        <source>Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="74"/>
        <source>Palette</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>paletteButton</name>
    <message>
        <location filename="../../src/paletteButton.ui" line="32"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name></name>
    <message>
        <location filename="../data/ukui-notebook.desktop.in.h" line="1"/>
        <source>Notes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
