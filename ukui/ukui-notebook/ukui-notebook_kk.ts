<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name></name>
    <message>
        <location filename="../data/ukui-notebook.desktop.in.h" line="1"/>
        <source>Notes</source>
        <translation>Жазбалар</translation>
    </message>
</context>
<context>
    <name>About</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Диалог</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">Шамамен</translation>
    </message>
    <message>
        <location filename="../../src/about.cpp" line="26"/>
        <source>Notes</source>
        <translation>Жазбалар</translation>
    </message>
    <message>
        <source>Version: %1</source>
        <translation type="vanished">Нұсқасы:% 1</translation>
    </message>
    <message>
        <source>Notes is a self-developed sidebar application plug-in, which provides a rich interface, convenient operation and stable functions, aiming at a friendly user experience.</source>
        <translation type="vanished">Ноталар - пайдаланушының ыңғайлы тәжірибесіне бағытталған бай интерфейсті, ыңғайлы жұмыс істеуді және тұрақты функцияларды қамтамасыз ететін өздігінен әзірленген бүйірлік қосымша қосылатын модулі.</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">Қызмет және қолдау: </translation>
    </message>
</context>
<context>
    <name>CustomPushButtonGroup</name>
    <message>
        <location filename="../../src/custom_ui/custom_push_button_group.ui" line="14"/>
        <source>CustomPushButtonGroup</source>
        <translation>CustomPushButtonGroup</translation>
    </message>
</context>
<context>
    <name>EditPage</name>
    <message>
        <location filename="../../src/editPage.cpp" line="93"/>
        <source>Notes</source>
        <translation>Жазбалар</translation>
    </message>
    <message>
        <source>Bold</source>
        <translation type="vanished">Қалың</translation>
    </message>
    <message>
        <source>Italic</source>
        <translation type="vanished">Италич</translation>
    </message>
    <message>
        <source>Underline</source>
        <translation type="vanished">Асты сызылсын</translation>
    </message>
    <message>
        <source>Strikeout</source>
        <translation type="vanished">Соққы</translation>
    </message>
    <message>
        <source>Unordered</source>
        <translation type="vanished">Unordered</translation>
    </message>
    <message>
        <source>Ordered</source>
        <translation type="vanished">Тапсырысты</translation>
    </message>
    <message>
        <source>Font Size</source>
        <translation type="vanished">Қаріп өлшемі</translation>
    </message>
    <message>
        <source>Font Color</source>
        <translation type="vanished">Қаріп түсі</translation>
    </message>
    <message>
        <source>InsertPicture</source>
        <translation type="vanished">КірістіруPicture</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="362"/>
        <source>undo</source>
        <translation>болдырмау</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="363"/>
        <source>redo</source>
        <translation>редо</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="364"/>
        <source>cut</source>
        <translation>қиып алу</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="365"/>
        <source>copy</source>
        <translation>көшіру</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="366"/>
        <source>paste</source>
        <translation>қою</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="367"/>
        <source>copy to newpage</source>
        <translation>жаңалыққа көшіру</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="724"/>
        <source>Select an image</source>
        <translation>Кескінді таңдау</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="726"/>
        <source>JPEG (*.jpg);; GIF (*.gif);; PNG (*.png);; BMP (*.bmp);; All (*)</source>
        <translation>JPEG (*.jpg); GIF (*.gif); PNG (*.png); BMP (*.bmp); Барлығы (*)</translation>
    </message>
</context>
<context>
    <name>Edit_page</name>
    <message>
        <location filename="../../src/editPage.ui" line="26"/>
        <source>Form</source>
        <translation>Пішін</translation>
    </message>
    <message>
        <source>14</source>
        <translation type="vanished">14</translation>
    </message>
</context>
<context>
    <name>FontPropertyWidget</name>
    <message>
        <location filename="../../src/font_button/font_property_widget.ui" line="14"/>
        <source>FontPropertyWidget</source>
        <translation>FontPropertyWidget</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="15"/>
        <source>Bold</source>
        <translation>Қалың</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="16"/>
        <source>Italic</source>
        <translation>Италич</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="17"/>
        <source>Underline</source>
        <translation>Асты сызылсын</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="18"/>
        <source>Strikeout</source>
        <translation>Соққы</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="19"/>
        <source>Unordered</source>
        <translation>Unordered</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="20"/>
        <source>Ordered</source>
        <translation>Тапсырысты</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="21"/>
        <source>InsertPicture</source>
        <translation>КірістіруPicture</translation>
    </message>
</context>
<context>
    <name>FontTwinButtonGroup</name>
    <message>
        <location filename="../../src/font_button/font_twin_button_group.cpp" line="21"/>
        <source>Font Size</source>
        <translation>Қаріп өлшемі</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_twin_button_group.cpp" line="22"/>
        <source>Font Color</source>
        <translation>Қаріп түсі</translation>
    </message>
</context>
<context>
    <name>PaletteWidget</name>
    <message>
        <location filename="../../src/paletteWidget.ui" line="26"/>
        <source>Form</source>
        <translation>Пішін</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/utils/utils.cpp" line="22"/>
        <source>show</source>
        <translation>&amp; Көрсету</translation>
    </message>
</context>
<context>
    <name>SelectColor</name>
    <message>
        <location filename="../../src/selectColorPage.ui" line="26"/>
        <source>Form</source>
        <translation>Пішін</translation>
    </message>
</context>
<context>
    <name>SetFontColor</name>
    <message>
        <location filename="../../src/setFontColorPage.ui" line="26"/>
        <source>Form</source>
        <translation>Пішін</translation>
    </message>
</context>
<context>
    <name>SetFontSize</name>
    <message>
        <location filename="../../src/setFontSizePage.ui" line="31"/>
        <source>Form</source>
        <translation>Пішін</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../../src/widget.ui" line="14"/>
        <source>Widget</source>
        <translation>Виджет</translation>
    </message>
    <message>
        <location filename="../../src/widget.ui" line="63"/>
        <source>Note List</source>
        <translation>Ескертпелер тізімі</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="vanished">Жаңа</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="283"/>
        <source>Notes</source>
        <translation>Жазбалар</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="616"/>
        <source>Help</source>
        <translation>Анықтама</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="617"/>
        <source>About</source>
        <translation>Шамамен</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="618"/>
        <source>Empty Note</source>
        <translation>&amp; Ð Ð°Ð1/2</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="620"/>
        <source>Exit</source>
        <translation>Шығу</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="653"/>
        <source>Create New Note</source>
        <translation>Жаңа жазба жасау</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1123"/>
        <source>Enter search content</source>
        <translation>Іздеу мазмұнын енгізу</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1621"/>
        <location filename="../../src/widget.cpp" line="1635"/>
        <source>delete this note</source>
        <translation>бұл жазбаны жою</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1622"/>
        <location filename="../../src/widget.cpp" line="1638"/>
        <source>open this note</source>
        <translation>осы жазбаны ашу</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1623"/>
        <location filename="../../src/widget.cpp" line="1641"/>
        <source>clear note list</source>
        <translation>түсінікті жазба тізімі</translation>
    </message>
    <message>
        <source>Delete Selected Note</source>
        <translation type="vanished">Таңдалған жазбаны жою</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="654"/>
        <source>Switch View</source>
        <translation>Көріністі ауыстыру</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Жабу</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">Кішірейту</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">Мәзір</translation>
    </message>
    <message>
        <source>Welcome to use Notes.</source>
        <translation type="vanished">Жазбаларды пайдалануға қош келдіңіз.</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Іздеу</translation>
    </message>
    <message>
        <source>[picture]</source>
        <translation type="vanished">[сурет]</translation>
    </message>
</context>
<context>
    <name>emptyNotes</name>
    <message>
        <location filename="../../src/emptyNotes.ui" line="32"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="190"/>
        <source>Are you sure empty notebook?</source>
        <translation>Бос жазу кітапшасы бар ма?</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="251"/>
        <source>No Tips</source>
        <translation>Кеңестер жоқ</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="305"/>
        <source>cancel</source>
        <translation>Болдырмау</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="328"/>
        <source>yes</source>
        <translation>Иә</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.cpp" line="33"/>
        <source>emptyNotes</source>
        <translation>бос Ескертулер</translation>
    </message>
</context>
<context>
    <name>fontButton</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Пішін</translation>
    </message>
</context>
<context>
    <name>iconViewModeDelegate</name>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="260"/>
        <source>[picture]</source>
        <translation>[сурет]</translation>
    </message>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="334"/>
        <source>Welcome to use Notes.</source>
        <translation>Жазбаларды пайдалануға қош келдіңіз.</translation>
    </message>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="363"/>
        <source>Today  </source>
        <translation>Бүгін  </translation>
    </message>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="372"/>
        <source>Yesterday  </source>
        <translation>Кеше  </translation>
    </message>
</context>
<context>
    <name>listViewModeDelegate</name>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="308"/>
        <source>[picture]</source>
        <translation>[сурет]</translation>
    </message>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="313"/>
        <source>Welcome to use Notes.</source>
        <translation>Жазбаларды пайдалануға қош келдіңіз.</translation>
    </message>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="385"/>
        <source>Today  </source>
        <translation>Бүгін  </translation>
    </message>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="394"/>
        <source>Yesterday  </source>
        <translation>Кеше  </translation>
    </message>
</context>
<context>
    <name>noteExitWindow</name>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="31"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="83"/>
        <source>Are you sure to exit the note book?</source>
        <translation>Жазба кітабынан міндетті түрде шығасыз ба?</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="104"/>
        <source>Close the desktop note page at the same time</source>
        <translation>Жұмыс үстелінің жазба бетін бір уақытта жабу</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="149"/>
        <source>No</source>
        <translation>Жоқ</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="171"/>
        <source>Yes</source>
        <translation>Иә</translation>
    </message>
</context>
<context>
    <name>noteHead</name>
    <message>
        <location filename="../../src/noteHead.ui" line="14"/>
        <source>Form</source>
        <translation>Пішін</translation>
    </message>
</context>
<context>
    <name>noteHeadMenu</name>
    <message>
        <location filename="../../src/noteHeadMenu.ui" line="14"/>
        <source>Form</source>
        <translation>Пішін</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="149"/>
        <source>Open note list</source>
        <translation>Жазбалар тізімін ашу</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="150"/>
        <source>Always in the front</source>
        <translation>Әрқашан алдыңғы жағында</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="151"/>
        <source>Delete this note</source>
        <translation>Бұл жазбаны жою</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="152"/>
        <source>Share</source>
        <translation>Ортақ пайдалану</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="99"/>
        <source>Create New Note</source>
        <translation>Жаңа жазба жасау</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="90"/>
        <source>Close</source>
        <translation>Жабу</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="82"/>
        <location filename="../../src/noteHeadMenu.cpp" line="86"/>
        <source>Menu</source>
        <translation>Мәзір</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="74"/>
        <source>Palette</source>
        <translation>Палет</translation>
    </message>
</context>
<context>
    <name>paletteButton</name>
    <message>
        <location filename="../../src/paletteButton.ui" line="32"/>
        <source>Form</source>
        <translation>Пішін</translation>
    </message>
</context>
</TS>
