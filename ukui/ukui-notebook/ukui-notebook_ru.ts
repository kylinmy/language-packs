<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru">
<context>
    <name></name>
    <message>
        <location filename="../data/ukui-notebook.desktop.in.h" line="1"/>
        <source>Notes</source>
        <translation>Примечания</translation>
    </message>
</context>
<context>
    <name>About</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">Диалог</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">Около</translation>
    </message>
    <message>
        <location filename="../../src/about.cpp" line="26"/>
        <source>Notes</source>
        <translation>Примечания</translation>
    </message>
    <message>
        <source>Version: %1</source>
        <translation type="vanished">Версия: %1</translation>
    </message>
    <message>
        <source>Notes is a self-developed sidebar application plug-in, which provides a rich interface, convenient operation and stable functions, aiming at a friendly user experience.</source>
        <translation type="vanished">Notes - это самостоятельно разработанный плагин приложения боковой панели, который обеспечивает богатый интерфейс, удобную работу и стабильные функции, нацеленные на дружественный пользовательский опыт.</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">Сервис и поддержка: </translation>
    </message>
</context>
<context>
    <name>CustomPushButtonGroup</name>
    <message>
        <location filename="../../src/custom_ui/custom_push_button_group.ui" line="14"/>
        <source>CustomPushButtonGroup</source>
        <translation>НастраиваемаяПушБаттонГруппа</translation>
    </message>
</context>
<context>
    <name>EditPage</name>
    <message>
        <location filename="../../src/editPage.cpp" line="93"/>
        <source>Notes</source>
        <translation>Примечания</translation>
    </message>
    <message>
        <source>Bold</source>
        <translation type="vanished">Смелый</translation>
    </message>
    <message>
        <source>Italic</source>
        <translation type="vanished">Курсив</translation>
    </message>
    <message>
        <source>Underline</source>
        <translation type="vanished">Подчеркнуть</translation>
    </message>
    <message>
        <source>Strikeout</source>
        <translation type="vanished">Вычеркивание</translation>
    </message>
    <message>
        <source>Unordered</source>
        <translation type="vanished">Неупорядоченный</translation>
    </message>
    <message>
        <source>Ordered</source>
        <translation type="vanished">Упорядоченный</translation>
    </message>
    <message>
        <source>Font Size</source>
        <translation type="vanished">Размер шрифта</translation>
    </message>
    <message>
        <source>Font Color</source>
        <translation type="vanished">Цвет шрифта</translation>
    </message>
    <message>
        <source>InsertPicture</source>
        <translation type="vanished">ВставкаФотография</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="362"/>
        <source>undo</source>
        <translation>отменить</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="363"/>
        <source>redo</source>
        <translation>повторить</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="364"/>
        <source>cut</source>
        <translation>резать</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="365"/>
        <source>copy</source>
        <translation>копировать</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="366"/>
        <source>paste</source>
        <translation>паста</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="367"/>
        <source>copy to newpage</source>
        <translation>скопировать на новую страницу</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="724"/>
        <source>Select an image</source>
        <translation>Выберите изображение</translation>
    </message>
    <message>
        <location filename="../../src/editPage.cpp" line="726"/>
        <source>JPEG (*.jpg);; GIF (*.gif);; PNG (*.png);; BMP (*.bmp);; All (*)</source>
        <translation>JPEG (*.jpg);; GIF (*.gif);; ПНГ (*.png);; БМП (*.bmp);; Все (*)</translation>
    </message>
</context>
<context>
    <name>Edit_page</name>
    <message>
        <location filename="../../src/editPage.ui" line="26"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <source>14</source>
        <translation type="vanished">14</translation>
    </message>
</context>
<context>
    <name>FontPropertyWidget</name>
    <message>
        <location filename="../../src/font_button/font_property_widget.ui" line="14"/>
        <source>FontPropertyWidget</source>
        <translation>FontPropertyWidget</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="15"/>
        <source>Bold</source>
        <translation>Смелый</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="16"/>
        <source>Italic</source>
        <translation>Курсив</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="17"/>
        <source>Underline</source>
        <translation>Подчеркнуть</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="18"/>
        <source>Strikeout</source>
        <translation>Вычеркивание</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="19"/>
        <source>Unordered</source>
        <translation>Неупорядоченный</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="20"/>
        <source>Ordered</source>
        <translation>Упорядоченный</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_property_widget.cpp" line="21"/>
        <source>InsertPicture</source>
        <translation>ВставкаФотография</translation>
    </message>
</context>
<context>
    <name>FontTwinButtonGroup</name>
    <message>
        <location filename="../../src/font_button/font_twin_button_group.cpp" line="21"/>
        <source>Font Size</source>
        <translation>Размер шрифта</translation>
    </message>
    <message>
        <location filename="../../src/font_button/font_twin_button_group.cpp" line="22"/>
        <source>Font Color</source>
        <translation>Цвет шрифта</translation>
    </message>
</context>
<context>
    <name>PaletteWidget</name>
    <message>
        <location filename="../../src/paletteWidget.ui" line="26"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/utils/utils.cpp" line="22"/>
        <source>show</source>
        <translation>показывать</translation>
    </message>
</context>
<context>
    <name>SelectColor</name>
    <message>
        <location filename="../../src/selectColorPage.ui" line="26"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
</context>
<context>
    <name>SetFontColor</name>
    <message>
        <location filename="../../src/setFontColorPage.ui" line="26"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
</context>
<context>
    <name>SetFontSize</name>
    <message>
        <location filename="../../src/setFontSizePage.ui" line="31"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../../src/widget.ui" line="14"/>
        <source>Widget</source>
        <translation>Виджет</translation>
    </message>
    <message>
        <location filename="../../src/widget.ui" line="63"/>
        <source>Note List</source>
        <translation>Список примечаний</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="vanished">Новые функции</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="283"/>
        <source>Notes</source>
        <translation>Примечания</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="616"/>
        <source>Help</source>
        <translation>Справка</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="617"/>
        <source>About</source>
        <translation>Около</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="618"/>
        <source>Empty Note</source>
        <translation>Пустая заметка</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="620"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="653"/>
        <source>Create New Note</source>
        <translation>Создать новую заметку</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1123"/>
        <source>Enter search content</source>
        <translation>Ввод содержимого поиска</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1621"/>
        <location filename="../../src/widget.cpp" line="1635"/>
        <source>delete this note</source>
        <translation>Удалите это примечание</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1622"/>
        <location filename="../../src/widget.cpp" line="1638"/>
        <source>open this note</source>
        <translation>Откройте эту заметку</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="1623"/>
        <location filename="../../src/widget.cpp" line="1641"/>
        <source>clear note list</source>
        <translation>очистить список заметок</translation>
    </message>
    <message>
        <source>Delete Selected Note</source>
        <translation type="vanished">Удаление выбранной заметки</translation>
    </message>
    <message>
        <location filename="../../src/widget.cpp" line="654"/>
        <source>Switch View</source>
        <translation>Переключить вид</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Закрывать</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation type="vanished">Минимизировать</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation type="vanished">Меню</translation>
    </message>
    <message>
        <source>Welcome to use Notes.</source>
        <translation type="vanished">Добро пожаловать в Заметки.</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">Искать</translation>
    </message>
    <message>
        <source>[picture]</source>
        <translation type="vanished">[рисунок]</translation>
    </message>
</context>
<context>
    <name>emptyNotes</name>
    <message>
        <location filename="../../src/emptyNotes.ui" line="32"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="190"/>
        <source>Are you sure empty notebook?</source>
        <translation>Вы уверены, что пустой блокнот?</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="251"/>
        <source>No Tips</source>
        <translation>Нет подсказок</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="305"/>
        <source>cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.ui" line="328"/>
        <source>yes</source>
        <translation>да</translation>
    </message>
    <message>
        <location filename="../../src/emptyNotes.cpp" line="33"/>
        <source>emptyNotes</source>
        <translation>emptyПримечания</translation>
    </message>
</context>
<context>
    <name>fontButton</name>
    <message>
        <source>Form</source>
        <translation type="vanished">Форма</translation>
    </message>
</context>
<context>
    <name>iconViewModeDelegate</name>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="260"/>
        <source>[picture]</source>
        <translation>[рисунок]</translation>
    </message>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="334"/>
        <source>Welcome to use Notes.</source>
        <translation>Добро пожаловать в Заметки.</translation>
    </message>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="363"/>
        <source>Today  </source>
        <translation>Сегодня  </translation>
    </message>
    <message>
        <location filename="../../src/iconViewModeDelegate.cpp" line="372"/>
        <source>Yesterday  </source>
        <translation>Вчера  </translation>
    </message>
</context>
<context>
    <name>listViewModeDelegate</name>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="308"/>
        <source>[picture]</source>
        <translation>[рисунок]</translation>
    </message>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="313"/>
        <source>Welcome to use Notes.</source>
        <translation>Добро пожаловать в Заметки.</translation>
    </message>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="385"/>
        <source>Today  </source>
        <translation>Сегодня  </translation>
    </message>
    <message>
        <location filename="../../src/listViewModeDelegate.cpp" line="394"/>
        <source>Yesterday  </source>
        <translation>Вчера  </translation>
    </message>
</context>
<context>
    <name>noteExitWindow</name>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="31"/>
        <source>Dialog</source>
        <translation>Диалог</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="83"/>
        <source>Are you sure to exit the note book?</source>
        <translation>Вы обязательно выйдете из записной книжки?</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="104"/>
        <source>Close the desktop note page at the same time</source>
        <translation>Одновременно закройте страницу заметки на рабочем столе</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="149"/>
        <source>No</source>
        <translation>Нет</translation>
    </message>
    <message>
        <location filename="../../src/noteExitWindow.ui" line="171"/>
        <source>Yes</source>
        <translation>Да</translation>
    </message>
</context>
<context>
    <name>noteHead</name>
    <message>
        <location filename="../../src/noteHead.ui" line="14"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
</context>
<context>
    <name>noteHeadMenu</name>
    <message>
        <location filename="../../src/noteHeadMenu.ui" line="14"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="149"/>
        <source>Open note list</source>
        <translation>Открыть список заметок</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="150"/>
        <source>Always in the front</source>
        <translation>Всегда впереди</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="151"/>
        <source>Delete this note</source>
        <translation>Удалить это примечание</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="152"/>
        <source>Share</source>
        <translation>Предоставить общий доступ</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="99"/>
        <source>Create New Note</source>
        <translation>Создать новую заметку</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="90"/>
        <source>Close</source>
        <translation>Закрывать</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="82"/>
        <location filename="../../src/noteHeadMenu.cpp" line="86"/>
        <source>Menu</source>
        <translation>Меню</translation>
    </message>
    <message>
        <location filename="../../src/noteHeadMenu.cpp" line="74"/>
        <source>Palette</source>
        <translation>Палитра</translation>
    </message>
</context>
<context>
    <name>paletteButton</name>
    <message>
        <location filename="../../src/paletteButton.ui" line="32"/>
        <source>Form</source>
        <translation>Форма</translation>
    </message>
</context>
</TS>
