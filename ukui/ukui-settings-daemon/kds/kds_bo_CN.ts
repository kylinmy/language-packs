<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>KDSWidget</name>
    <message>
        <location filename="../kdswidget.ui" line="90"/>
        <source>System Screen Projection</source>
        <translation>མ་ལག་གི་བརྙན་ཤེལ།</translation>
    </message>
    <message>
        <location filename="../kdswidget.ui" line="121"/>
        <source>FirstOutput:</source>
        <translation>ཐོག་མའི་བརྙན་ཤེལ།</translation>
    </message>
    <message>
        <location filename="../kdswidget.cpp" line="223"/>
        <source>First Display</source>
        <translation>གློག་ཀླད་ཀྱི་འཆར་ངོས་ཁོ་ན་འཆར།</translation>
    </message>
    <message>
        <location filename="../kdswidget.cpp" line="228"/>
        <source>Mirror Display</source>
        <translation>ཤེལ་བརྙན།</translation>
    </message>
    <message>
        <location filename="../kdswidget.cpp" line="233"/>
        <source>Extend Display</source>
        <translation>རྒྱ་སྐྱེད།</translation>
    </message>
    <message>
        <location filename="../kdswidget.cpp" line="238"/>
        <source>Vice Display</source>
        <translation>འཆར་ངོས་གཉིས་པ་མ་གཏོགས་མི་འཆར།</translation>
    </message>
    <message>
        <source>N/A</source>
        <translation type="vanished">N/A</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../widget.ui" line="14"/>
        <source>KDS</source>
        <translation>མ་ལག་བརྙན་ཤེལ།</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="95"/>
        <source>System Screen Projection</source>
        <translation>མ་ལག་གི་བརྙན་ཤེལ།</translation>
    </message>
    <message>
        <location filename="../widget.ui" line="126"/>
        <source>FirstOutput:</source>
        <translation>ཐོག་མའི་བརྙན་ཤེལ།</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="137"/>
        <source>First Screen</source>
        <translation>གློག་ཀླད་ཀྱི་འཆར་ངོས་ཁོ་ན་འཆར།</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="145"/>
        <source>Clone Screen</source>
        <translation>བརྙན་ཤེལ།</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="153"/>
        <source>Extend Screen</source>
        <translation>རྒྱ་སྐྱེད།</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="161"/>
        <source>Vice Screen</source>
        <translation>འཆར་ངོས་གཉིས་པ་མ་གཏོགས་མི་འཆར།</translation>
    </message>
    <message>
        <location filename="../widget.cpp" line="161"/>
        <source>None</source>
        <translation>གཅིག་ཀྱང་མེད།</translation>
    </message>
</context>
</TS>
