<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk" sourcelanguage="en">
<context>
    <name>BiometricAuthWidget</name>
    <message>
        <location filename="../BiometricAuth/biometricauthwidget.cpp" line="107"/>
        <source>Current device: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricauthwidget.cpp" line="179"/>
        <source>Identify failed, Please retry.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>BiometricDevicesWidget</name>
    <message>
        <location filename="../BiometricAuth/biometricdeviceswidget.cpp" line="51"/>
        <source>Please select the biometric device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceswidget.cpp" line="56"/>
        <source>Device type:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceswidget.cpp" line="73"/>
        <source>Device name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceswidget.cpp" line="83"/>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ConfForm</name>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="37"/>
        <source>edit network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="115"/>
        <source>LAN name: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="116"/>
        <source>Method: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="117"/>
        <source>Address: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="118"/>
        <source>Netmask: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="119"/>
        <source>Gateway: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="120"/>
        <source>DNS 1: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="121"/>
        <source>DNS 2: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="123"/>
        <source>Edit Conn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="124"/>
        <location filename="../kylin-nm/src/confform.cpp" line="126"/>
        <source>Auto(DHCP)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="125"/>
        <location filename="../kylin-nm/src/confform.cpp" line="127"/>
        <source>Manual</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="137"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="138"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="139"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="253"/>
        <source>Can not create new wired network for without wired card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="270"/>
        <source>New network already created</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="320"/>
        <source>New network settings already finished</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="329"/>
        <source>New settings already effective</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="353"/>
        <source>Edit Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/confform.cpp" line="389"/>
        <source>Add Wired Network</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeviceType</name>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="38"/>
        <source>FingerPrint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="40"/>
        <source>FingerVein</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="42"/>
        <source>Iris</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="44"/>
        <source>Face</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="46"/>
        <source>VoicePrint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../BiometricAuth/biometricdeviceinfo.cpp" line="48"/>
        <source>QRCode</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifi</name>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifi.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifi.cpp" line="76"/>
        <source>Add Hidden Wi-Fi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifi.cpp" line="77"/>
        <source>Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifi.cpp" line="78"/>
        <source>Wi-Fi name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifi.cpp" line="79"/>
        <source>Wi-Fi security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifi.cpp" line="80"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifi.cpp" line="81"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifi.cpp" line="83"/>
        <source>C_reate…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifi.cpp" line="104"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifi.cpp" line="105"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiLeap</name>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="68"/>
        <source>Add hidden Wi-Fi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="69"/>
        <source>Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="70"/>
        <source>Network name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="71"/>
        <source>Wi-Fi security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="72"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="73"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="74"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="75"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="77"/>
        <source>C_reate…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="95"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="96"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="97"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="98"/>
        <source>WEP 128-bit Passphrase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="100"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifileap.cpp" line="101"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecFast</name>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="83"/>
        <source>Add hidden Wi-Fi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="84"/>
        <source>Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="85"/>
        <source>Network name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="86"/>
        <source>Wi-Fi security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="87"/>
        <source>Authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="88"/>
        <source>Anonymous identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="89"/>
        <source>Allow automatic PAC pro_visioning</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="90"/>
        <source>PAC file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="91"/>
        <source>Inner authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="92"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="93"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="94"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="95"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="100"/>
        <source>C_reate…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="118"/>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="148"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="119"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="120"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="121"/>
        <source>WEP 128-bit Passphrase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="123"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="124"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="136"/>
        <source>Tunneled TLS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="137"/>
        <source>Protected EAP (PEAP)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="143"/>
        <source>Anonymous</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="144"/>
        <source>Authenticated</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecfast.cpp" line="145"/>
        <source>Both</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecLeap</name>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="73"/>
        <source>Add hidden Wi-Fi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="74"/>
        <source>Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="75"/>
        <source>Network name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="76"/>
        <source>Wi-Fi security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="77"/>
        <source>Authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="78"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="79"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="80"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="81"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="83"/>
        <source>C_reate…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="101"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="102"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="103"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="104"/>
        <source>WEP 128-bit Passphrase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="106"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="107"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="119"/>
        <source>Tunneled TLS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecleap.cpp" line="120"/>
        <source>Protected EAP (PEAP)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecPeap</name>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="91"/>
        <source>Add hidden Wi-Fi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="92"/>
        <source>Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="93"/>
        <source>Network name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="94"/>
        <source>Wi-Fi security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="95"/>
        <source>Authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="96"/>
        <source>Anonymous identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="97"/>
        <source>Domain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="98"/>
        <source>CA certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="99"/>
        <source>CA certificate password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="100"/>
        <source>No CA certificate is required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="101"/>
        <source>PEAP version</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="102"/>
        <source>Inner authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="103"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="104"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="105"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="106"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="126"/>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="149"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="127"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="128"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="129"/>
        <source>WEP 128-bit Passphrase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="131"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="132"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="144"/>
        <source>Tunneled TLS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="145"/>
        <source>Protected EAP (PEAP)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="150"/>
        <source>Choose from file</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="153"/>
        <source>Automatic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="154"/>
        <source>Version 0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpeap.cpp" line="155"/>
        <source>Version 1</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecPwd</name>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="74"/>
        <source>Add hidden Wi-Fi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="75"/>
        <source>Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="76"/>
        <source>Network name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="77"/>
        <source>Wi-Fi security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="78"/>
        <source>Authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="79"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="80"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="81"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="82"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="84"/>
        <source>C_reate…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="102"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="103"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="104"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="105"/>
        <source>WEP 128-bit Passphrase</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="107"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="108"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="120"/>
        <source>Tunneled TLS</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisecpwd.cpp" line="121"/>
        <source>Protected EAP (PEAP)</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecTls</name>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="90"/>
        <source>Add hidden Wi-Fi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="91"/>
        <source>Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="92"/>
        <source>Network name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="93"/>
        <source>Wi-Fi security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="94"/>
        <source>Authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="95"/>
        <source>Identity</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="96"/>
        <source>Domain</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="97"/>
        <source>CA certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="98"/>
        <source>CA certificate password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="99"/>
        <source>No CA certificate is required</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="100"/>
        <source>User certificate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="101"/>
        <source>User certificate password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="102"/>
        <source>User private key</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="103"/>
        <source>User key password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="104"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="105"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="107"/>
        <source>C_reate…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="125"/>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="148"/>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="152"/>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="156"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="126"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="127"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="128"/>
        <source>WEP 128-bit Passphrase</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="130"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="131"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="143"/>
        <source>Tunneled TLS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="144"/>
        <source>Protected EAP (PEAP)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="149"/>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="153"/>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectls.cpp" line="157"/>
        <source>Choose from file</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiSecTunnelTLS</name>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="89"/>
        <source>Add hidden Wi-Fi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="90"/>
        <source>Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="91"/>
        <source>Network name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="92"/>
        <source>Wi-Fi security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="93"/>
        <source>Authentication</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="94"/>
        <source>Anonymous identity</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="95"/>
        <source>Domain</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="96"/>
        <source>CA certificate</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="97"/>
        <source>CA certificate password</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="98"/>
        <source>No CA certificate is required</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="99"/>
        <source>Inner authentication</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="100"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="101"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="102"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="103"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="105"/>
        <source>C_reate…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="123"/>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="146"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="124"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="125"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="126"/>
        <source>WEP 128-bit Passphrase</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="128"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="129"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="141"/>
        <source>Tunneled TLS</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="142"/>
        <source>Protected EAP (PEAP)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifisectunneltls.cpp" line="147"/>
        <source>Choose from file</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiWep</name>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="73"/>
        <source>Add hidden Wi-Fi</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="74"/>
        <source>Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="75"/>
        <source>Network name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="76"/>
        <source>Wi-Fi security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="77"/>
        <source>Key</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="78"/>
        <source>WEP index</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="79"/>
        <source>Authentication</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="80"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="81"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="83"/>
        <source>C_reate…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="101"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="102"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="103"/>
        <source>WEP 40/128-bit Key (Hex or ASCII)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="104"/>
        <source>WEP 128-bit Passphrase</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="106"/>
        <source>Dynamic WEP (802.1X)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="107"/>
        <source>WPA &amp; WPA2 Enterprise</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="115"/>
        <source>1(default)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="121"/>
        <source>Open System</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwep.cpp" line="122"/>
        <source>Shared Key</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DlgConnHidWifiWpa</name>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.ui" line="14"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.cpp" line="81"/>
        <source>Add Hidden Wi-Fi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.cpp" line="82"/>
        <source>Connection</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.cpp" line="83"/>
        <source>Wi-Fi name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.cpp" line="84"/>
        <source>Wi-Fi security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.cpp" line="85"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.cpp" line="86"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.cpp" line="87"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.cpp" line="89"/>
        <source>C_reate…</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.cpp" line="112"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/wireless-security/dlgconnhidwifiwpa.cpp" line="113"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DlgHotspotCreate</name>
    <message>
        <location filename="../kylin-nm/hot-spot/dlghotspotcreate.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/hot-spot/dlghotspotcreate.cpp" line="46"/>
        <source>Create Hotspot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/hot-spot/dlghotspotcreate.cpp" line="47"/>
        <source>Network name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/hot-spot/dlghotspotcreate.cpp" line="48"/>
        <source>Wi-Fi security</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/hot-spot/dlghotspotcreate.cpp" line="49"/>
        <source>Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/hot-spot/dlghotspotcreate.cpp" line="50"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/hot-spot/dlghotspotcreate.cpp" line="51"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/hot-spot/dlghotspotcreate.cpp" line="54"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/hot-spot/dlghotspotcreate.cpp" line="55"/>
        <source>WPA &amp; WPA2 Personal</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FakeDialog</name>
    <message>
        <location filename="../common/fakedialog.cpp" line="63"/>
        <source>cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GreeterWindow</name>
    <message>
        <source>Power dialog</source>
        <translation type="vanished">电源对话框</translation>
    </message>
    <message>
        <source>On-screen keyboard, providing virtual keyboard function</source>
        <translation type="vanished">屏幕键盘，提供虚拟键盘功能</translation>
    </message>
    <message>
        <source>Set the desktop environment for the selected user to log in.If the user is logged in, it will take effect after logging in again</source>
        <translation type="vanished">设置选中用户登录后的桌面环境，如果用户已经登录，则会在重新登录后生效</translation>
    </message>
    <message>
        <source>Set the language of the selected user after logging in. If the user is logged in, it will take effect after logging in again.</source>
        <translation type="vanished">设置选中用户登录后的语言，如果用户已登录，则在重新登录后生效</translation>
    </message>
    <message>
        <location filename="../greeter/greeterwindow.cpp" line="896"/>
        <location filename="../greeter/greeterwindow.cpp" line="1241"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>GreeterWrapper</name>
    <message>
        <location filename="../greeter/greeterwrapper.cpp" line="132"/>
        <source>failed to start session.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>IconEdit</name>
    <message>
        <location filename="../greeter/iconedit.cpp" line="211"/>
        <location filename="../greeter/iconedit.cpp" line="253"/>
        <source>Password: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/iconedit.cpp" line="213"/>
        <location filename="../greeter/iconedit.cpp" line="255"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KeyboardWidget</name>
    <message>
        <location filename="../VirtualKeyboard/src/keyboardwidget.ui" line="29"/>
        <source>KeyboardWidget</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>KylinNM</name>
    <message>
        <location filename="../kylin-nm/src/kylinnm.ui" line="14"/>
        <source>kylin-nm</source>
        <translation></translation>
    </message>
    <message>
        <source>Ethernet Networks</source>
        <translation type="vanished">可用有线网络列表</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="292"/>
        <source>New LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wifi Networks</source>
        <translation type="vanished">加入其他网络</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="319"/>
        <source>Hide WiFi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="349"/>
        <source>No usable network in the list</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ethernet</source>
        <translation type="vanished">有线网络</translation>
    </message>
    <message>
        <source>有线网络</source>
        <translation type="vanished">有线网络</translation>
    </message>
    <message>
        <source>Wifi</source>
        <translation type="vanished">Wifi</translation>
    </message>
    <message>
        <source>无线网络</source>
        <translation type="vanished">无线网络</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="436"/>
        <source>HotSpot</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="445"/>
        <source>FlyMode</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="597"/>
        <source>Advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="596"/>
        <source>Show KylinNM</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="283"/>
        <source>Inactivated LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="307"/>
        <source>Other WLAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="413"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="417"/>
        <source>LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="423"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="427"/>
        <source>WLAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1202"/>
        <source>No wireless card detected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1239"/>
        <source>Activated LAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1315"/>
        <source>Activated WLAN</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1353"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1443"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1597"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="2279"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="2370"/>
        <source>Not connected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1356"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1445"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1515"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1516"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1600"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1680"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1833"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="2281"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="2372"/>
        <source>Disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1496"/>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1657"/>
        <source>NetOn,</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1541"/>
        <source>No Other Wired Network Scheme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="1702"/>
        <source>No Other Wireless Network Scheme</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="2188"/>
        <source>Wired net is disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="2218"/>
        <source>Wi-Fi is disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="2558"/>
        <source>Conn Ethernet Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="2578"/>
        <source>Conn Ethernet Fail</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="2603"/>
        <source>Conn Wifi Success</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/kylinnm.cpp" line="2622"/>
        <source>Confirm your Wi-Fi password or usable of wireless card</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginOptionsWidget</name>
    <message>
        <location filename="../greeter/loginoptionswidget.cpp" line="49"/>
        <source>Login Options</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">密码</translation>
    </message>
    <message>
        <source>Wechat</source>
        <translation type="vanished">微信</translation>
    </message>
    <message>
        <location filename="../greeter/loginoptionswidget.cpp" line="516"/>
        <source>Identify device removed!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginWindow</name>
    <message>
        <source>logged in</source>
        <translation type="vanished">已登录</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="613"/>
        <source>login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Incorrect user name, please input again</source>
        <translation type="vanished">用户名不正确，请重新输入</translation>
    </message>
    <message>
        <source>Biometric Authentication</source>
        <translation type="vanished">生物识别认证</translation>
    </message>
    <message>
        <source>Password Authentication</source>
        <translation type="vanished">密码认证</translation>
    </message>
    <message>
        <source>Other Devices</source>
        <translation type="vanished">其他设备</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="867"/>
        <source>Retry</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please enter your password or enroll your fingerprint </source>
        <translation type="vanished">请输入密码或者录入指纹</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="718"/>
        <source>Password: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="821"/>
        <source>User name input error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="824"/>
        <location filename="../greeter/loginwindow.cpp" line="829"/>
        <source>Authentication failure, Please try again</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Biometric/code scanning authentication failed too many times, please enter the password.</source>
        <translation type="vanished">生物/扫码验证失败达最大次数，请使用密码解锁</translation>
    </message>
    <message>
        <source>Biometric/code scan authentication failed too many times, please enter the password.</source>
        <translation type="vanished">生物/扫码验证失败达最大次数，请使用密码解锁</translation>
    </message>
    <message>
        <source>Bioauth/code scan authentication failed, you still have %1 verification opportunities</source>
        <translation type="vanished">生物/扫码验证失败，您还有%1次尝试机会</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1261"/>
        <source>NET Exception</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1343"/>
        <location filename="../greeter/loginwindow.cpp" line="1344"/>
        <location filename="../greeter/loginwindow.cpp" line="1417"/>
        <location filename="../greeter/loginwindow.cpp" line="1418"/>
        <source>Please try again in %1 minutes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1353"/>
        <location filename="../greeter/loginwindow.cpp" line="1354"/>
        <location filename="../greeter/loginwindow.cpp" line="1427"/>
        <location filename="../greeter/loginwindow.cpp" line="1428"/>
        <source>Please try again in %1 seconds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1362"/>
        <location filename="../greeter/loginwindow.cpp" line="1363"/>
        <location filename="../greeter/loginwindow.cpp" line="1436"/>
        <location filename="../greeter/loginwindow.cpp" line="1437"/>
        <source>Account locked permanently.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="755"/>
        <source>Password cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="333"/>
        <source>Verify face recognition or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="338"/>
        <source>Press fingerprint or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="343"/>
        <source>Verify voiceprint or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="348"/>
        <source>Verify finger vein or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="353"/>
        <source>Verify iris or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="715"/>
        <source>Input Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1099"/>
        <location filename="../greeter/loginwindow.cpp" line="1238"/>
        <source>Failed to verify %1, please enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1104"/>
        <location filename="../greeter/loginwindow.cpp" line="1245"/>
        <source>Unable to verify %1, please enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1263"/>
        <source>Abnormal network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use the bound wechat scanning code or enter the password to log in</source>
        <translation type="vanished">使用绑定的微信扫码或输入密码登录</translation>
    </message>
    <message>
        <source>Failed to verify %1, please enter password.</source>
        <translation type="vanished">验证%1失败，请输入密码.</translation>
    </message>
    <message>
        <source>Unable to verify %1, please enter password.</source>
        <translation type="vanished">无法验证%1，请输入密码.</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="1251"/>
        <source>Failed to verify %1, you still have %2 verification opportunities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Incorrect password, please input again</source>
        <translation type="obsolete">密码错误，请重新输入</translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="242"/>
        <location filename="../greeter/loginwindow.cpp" line="427"/>
        <location filename="../greeter/loginwindow.cpp" line="863"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="358"/>
        <source>Use the bound wechat scanning code or enter the password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/loginwindow.cpp" line="722"/>
        <source>Username</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../display-switch/mainwindow.ui" line="14"/>
        <location filename="../display-switch/ui_mainwindow.h" line="127"/>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/mainwindow.cpp" line="204"/>
        <source>%1</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OneConnForm</name>
    <message>
        <location filename="../kylin-nm/src/oneconnform.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="157"/>
        <source>Automatically join the network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="42"/>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="43"/>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="44"/>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="46"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="45"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="47"/>
        <source>Input Password...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>自动连接</source>
        <translation type="vanished">自动连接</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="423"/>
        <source>Connect to Hidden Wi-Fi Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rate</source>
        <translation type="obsolete">速率</translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="556"/>
        <source>None</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="558"/>
        <source>WiFi Security：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="559"/>
        <source>Signal：</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="560"/>
        <source>MAC：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/oneconnform.cpp" line="837"/>
        <source>Conn Wifi Failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OneLancForm</name>
    <message>
        <location filename="../kylin-nm/src/onelancform.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/onelancform.cpp" line="31"/>
        <location filename="../kylin-nm/src/onelancform.cpp" line="32"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/onelancform.cpp" line="34"/>
        <source>Disconnect</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/onelancform.cpp" line="289"/>
        <location filename="../kylin-nm/src/onelancform.cpp" line="293"/>
        <source>No Configuration</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/onelancform.cpp" line="296"/>
        <source>IPv4：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/onelancform.cpp" line="297"/>
        <source>IPv6：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/onelancform.cpp" line="298"/>
        <source>BandWidth：</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../kylin-nm/src/onelancform.cpp" line="299"/>
        <source>MAC：</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PowerManager</name>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="347"/>
        <source>Switch User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="418"/>
        <source>Shut Down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="436"/>
        <source>Hibernate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="456"/>
        <source>Suspend</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sleep</source>
        <translation type="vanished">休眠</translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="348"/>
        <location filename="../greeter/powerwindow.cpp" line="401"/>
        <source>Restart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/powerwindow.cpp" line="349"/>
        <source>Power Off</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Surewidget</name>
    <message>
        <location filename="../greeter/surewidget.ui" line="14"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.ui" line="51"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.ui" line="97"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.ui" line="110"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/surewidget.cpp" line="10"/>
        <source>Multiple users are logged in at the same time.Are you sure you want to reboot this system?</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UsersModel</name>
    <message>
        <location filename="../greeter/usersmodel.cpp" line="58"/>
        <source>Guest Session</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/usersmodel.cpp" line="48"/>
        <location filename="../greeter/usersmodel.cpp" line="50"/>
        <source>Guest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../greeter/usersmodel.cpp" line="76"/>
        <location filename="../greeter/usersmodel.cpp" line="78"/>
        <location filename="../greeter/usersmodel.cpp" line="85"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Utils</name>
    <message>
        <location filename="../kylin-nm/src/utils.cpp" line="89"/>
        <source>kylin network applet desktop message</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
