<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name></name>
    <message>
        <location filename="../kylin-printer.desktop.in.h" line="1"/>
        <source>Printers</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ</translation>
    </message>
    <message>
        <location filename="../backend/data/kylin-printer-applet.desktop.in.h" line="1"/>
        <source>Printers-backend</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠤᠨ ᠠᠷᠤ ᠳᠠᠪᠴᠠᠩ ᠳᠤ ᠬᠢ ᠠᠬᠢᠴᠠ</translation>
    </message>
</context>
<context>
    <name>AddPrinterWindow</name>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="117"/>
        <source>Add Printer</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="132"/>
        <source>Auto</source>
        <translation>ᠠᠦ᠋ᠲ᠋ᠤ᠋ ᠪᠠᠷ ᠡᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="133"/>
        <location filename="../ui/add_printer_window.cpp" line="170"/>
        <source>Manual</source>
        <translation>ᠭᠠᠷ ᠢᠶᠠᠷ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="145"/>
        <source>Device List</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠶᠢᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="171"/>
        <source>Protocol</source>
        <translation>ᠭᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="172"/>
        <source>Address</source>
        <translation>ᠬᠠᠶ᠋ᠢᠭ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="178"/>
        <source>Search</source>
        <translation>ᠡᠷᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="182"/>
        <source>socket</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="183"/>
        <source>ipp</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="184"/>
        <source>http</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="190"/>
        <source>name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="191"/>
        <source>location</source>
        <translation>ᠪᠠᠢᠷᠢ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="192"/>
        <source>driver</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <source>no PPD</source>
        <translation type="vanished">无驱动文件</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="197"/>
        <source>forward</source>
        <translation>ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="226"/>
        <location filename="../ui/add_printer_window.cpp" line="240"/>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="226"/>
        <source>Add printer failed: no PPD selected!</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠢ ᠨᠡᠮᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ: ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ ᠢ ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="240"/>
        <source>Add printer failed，please retry after a while.</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠢ ᠨᠡᠮᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="243"/>
        <location filename="../ui/add_printer_window.cpp" line="442"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="246"/>
        <location filename="../ui/add_printer_window.cpp" line="254"/>
        <location filename="../ui/add_printer_window.cpp" line="264"/>
        <source>Hint</source>
        <translation>ᠠᠨᠭᠬᠠᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="246"/>
        <source>Add printer successfully，printer a test page？</source>
        <translation>ᠨᠡᠮᠡᠵᠤ ᠴᠢᠳᠠᠪᠠ ᠂ ᠳᠤᠷᠰᠢᠬᠤ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠶᠢ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠤᠤ？</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="248"/>
        <source>Print test page</source>
        <translation>ᠳᠤᠷᠰᠢᠬᠤ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠶᠢ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="249"/>
        <source>Check Printer</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠶᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="254"/>
        <source>Is the test page printed successfully?</source>
        <translation>ᠲᠤᠷᠰᠢᠬᠤ ᠬᠠᠭᠤᠳᠠᠰᠤ ᠶᠢ ᠫᠷᠢᠨᠲᠸ᠋ᠷᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="256"/>
        <source>Yes</source>
        <translation>ᠳᠡᠢᠮᠤ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="257"/>
        <source>No</source>
        <translation>ᠦᠭᠡᠢ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="264"/>
        <source>Test print failed.Do you want to change a printer driver?</source>
        <translation>ᠲᠤᠷᠰᠢᠵᠤ ᠫᠷᠢᠨᠲᠸ᠋ᠷᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ ᠂ ᠬᠦᠳᠡᠯᠭᠡᠬᠦᠷ ᠦᠨ ᠳᠦᠰᠦᠯ ᠢ ᠦᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="266"/>
        <source>Change Driver</source>
        <translation>ᠬᠦᠳᠡᠯᠭᠡᠬᠦᠷ ᠢ ᠵᠠᠰᠠᠵᠤ ᠦᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="267"/>
        <location filename="../ui/add_printer_window.cpp" line="443"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="290"/>
        <source>Searching...</source>
        <translation>ᠬᠠᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="299"/>
        <location filename="../ui/add_printer_window.cpp" line="351"/>
        <source>Searching printers...</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠢ ᠶᠠᠭ ᠬᠠᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="331"/>
        <source>Can not find this Printer!</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠢ ᠡᠷᠢᠵᠤ ᠤᠯᠬᠤ ᠶᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠭᠡᠢ!</translation>
    </message>
    <message>
        <source>Please select a deb package.</source>
        <translation type="vanished">请选择deb驱动包</translation>
    </message>
    <message>
        <source>Deb File(*.deb)</source>
        <translation type="vanished">安装包(*.deb)</translation>
    </message>
    <message>
        <source>Choose</source>
        <translation type="vanished">选择</translation>
    </message>
    <message>
        <source>Searching driver...</source>
        <translation type="vanished">正在搜索驱动程序……</translation>
    </message>
    <message>
        <source>Install package failed，please retry.</source>
        <translation type="vanished">安装驱动包失败，请重试！</translation>
    </message>
    <message>
        <source>No driver package selected,Continue?</source>
        <translation type="vanished">没有选择驱动包，是否继续？</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="418"/>
        <source>Searching printer driver...</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠤᠨ ᠬᠦᠳᠡᠯᠭᠡᠬᠦᠷ ᠢ ᠶᠠᠭ ᠤᠭᠰᠠᠷᠴᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="440"/>
        <source>Install driver package automatically failed,continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Printer Name Cannot Contains &apos;/\&apos;&quot;?#&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation type="vanished">打印机名称不能包含&apos;/\&apos;&quot;?#&apos;,而且不能为空，且少于128个字母!</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="440"/>
        <location filename="../ui/add_printer_window.cpp" line="498"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="500"/>
        <source>Close</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <source>Exist Same Name Printer!</source>
        <translation type="vanished">存在同名打印机！</translation>
    </message>
    <message>
        <source>Printer Name Illegal!</source>
        <translation type="vanished">打印机名称不合法！</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.ui" line="26"/>
        <source>AddPrinterWindow</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AutoSearchResultModel</name>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="637"/>
        <source>network</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
</context>
<context>
    <name>BaseNotifyDialog</name>
    <message>
        <location filename="../ui/main_win_ui/base_notify_dialog.ui" line="35"/>
        <source>Dialog</source>
        <translation>ᠴᠤᠩᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/base_notify_dialog.ui" line="124"/>
        <source>TextLabel</source>
        <translation>ᠱᠤᠰᠢᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>ChoosePpdComboBox</name>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="15"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="22"/>
        <source>Choose PPD</source>
        <translation>ᠬᠦᠳᠡᠯᠭᠡᠬᠦᠷ ᠢ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="16"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="25"/>
        <source>Choose from the PPD library</source>
        <translation>ᠬᠦᠳᠡᠯᠭᠡᠭᠦᠷ ᠦᠨ ᠬᠦᠮᠦᠷᠭᠡ ᠡᠴᠡ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="17"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="29"/>
        <source>Add local PPD</source>
        <translation>ᠳᠤᠰ ᠭᠠᠵᠠᠷ ᠲᠤ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="48"/>
        <source>Please select a deb package.</source>
        <translation>deb ᠬᠦᠳᠡᠯᠭᠡᠭᠦᠷ ᠦᠨ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠶᠢ ᠰᠤᠩᠭᠤᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="50"/>
        <source>Deb File(*.deb)</source>
        <translation>ᠤᠭᠰᠠᠷᠬᠤ ᠪᠠᠭᠯᠠᠭ᠎ᠠ(*.deb)</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="51"/>
        <source>Choose</source>
        <translation>ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="52"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="69"/>
        <source>Searching driver...</source>
        <translation>ᠬᠦᠳᠡᠯᠭᠡᠭᠦᠷ ᠦᠨ ᠫᠷᠦᠭᠷᠡᠮ ᠢ ᠶᠠᠭ ᠬᠠᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="91"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="91"/>
        <source>Install package failed，please retry.</source>
        <translation>ᠬᠦᠳᠡᠯᠭᠡᠭᠦᠷ ᠦᠨ ᠪᠠᠭᠯᠠᠭ᠎ᠠ ᠶᠢ ᠤᠭᠰᠠᠷᠴᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ ᠂ ᠳᠠᠬᠢᠵᠤ ᠲᠤᠷᠰᠢᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="92"/>
        <source>Yes</source>
        <translation>ᠮᠦᠨ</translation>
    </message>
    <message>
        <source>No driver package selected,Continue?</source>
        <translation type="vanished">没有选择驱动包，是否继续？</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">确定</translation>
    </message>
</context>
<context>
    <name>ConfigIPAddressDialog</name>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>ᠴᠤᠩᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="247"/>
        <source>初始地址:api.kylinos.cn</source>
        <translation>ᠠᠩᠬᠠᠨ ᠤ ᠬᠠᠶ᠋ᠢᠭ:api.kylinos.cn</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="24"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <source>Save</source>
        <translatorcomment>保存</translatorcomment>
        <translation type="obsolete">保存</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="23"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Apply</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Server Address</source>
        <translation type="vanished">服务器地址</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="20"/>
        <source>Configure</source>
        <translatorcomment>配置</translatorcomment>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="22"/>
        <source>Service address:</source>
        <translatorcomment>服务器地址：</translatorcomment>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠦᠨ ᠬᠠᠶᠢᠭ:</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="36"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="36"/>
        <source>Config init failed</source>
        <translatorcomment>设置初始化失败！</translatorcomment>
        <translation>ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠬᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="39"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>CupsDebugLoggingCheckbox</name>
    <message>
        <location filename="../ui/cups_debug_logging_checkbox.cpp" line="8"/>
        <source>Retain debug info for troubleshooting</source>
        <translatorcomment>保留调试信息用于故障排查</translatorcomment>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DebInstallWindow</name>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="32"/>
        <source>Form</source>
        <translation>ᠹᠣᠣᠮ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="75"/>
        <source>Driver</source>
        <translation>ᠬᠦᠳᠡᠯᠬᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <source>Choose</source>
        <translation type="vanished">选择</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="143"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Foward</source>
        <translation type="vanished">下一步</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="23"/>
        <source>Modify Printer Driver</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠤᠨ ᠬᠦᠳᠡᠯᠭᠡᠬᠦᠷ ᠢ ᠦᠭᠡᠷᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="54"/>
        <source>Modify PPD failed: no PPD selected!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Choose A Deb, Or Forward Directly!</source>
        <translation type="vanished">选择安装包，或直接下一步！</translation>
    </message>
    <message>
        <source>Please select a deb package.</source>
        <translation type="vanished">请选择deb驱动包</translation>
    </message>
    <message>
        <source>Deb File(*.deb)</source>
        <translation type="vanished">安装包(*.deb)</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="54"/>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <source>Add printer failed: no PPD selected!</source>
        <translation type="obsolete">添加打印机失败：没有选择驱动！</translation>
    </message>
    <message>
        <source>Searching driver...</source>
        <translation type="vanished">正在搜索驱动程序……</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>Install package failed，please retry.</source>
        <translation type="vanished">安装驱动包失败，请重试！</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="162"/>
        <source>Confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source>No driver package selected,Continue?</source>
        <translation type="vanished">没有选择驱动包，是否继续？</translation>
    </message>
</context>
<context>
    <name>DeviceListButton</name>
    <message>
        <source>Off</source>
        <translation type="vanished">断开</translation>
    </message>
    <message>
        <source>On</source>
        <translation type="vanished">已连接</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="46"/>
        <location filename="../ui/device_list_button.cpp" line="261"/>
        <location filename="../ui/device_list_button.cpp" line="293"/>
        <source>Default</source>
        <translation>ᠠᠶᠠᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="109"/>
        <location filename="../ui/device_list_button.cpp" line="139"/>
        <source>Set Default</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="111"/>
        <location filename="../ui/device_list_button.cpp" line="142"/>
        <source>Enabled</source>
        <translatorcomment>启用</translatorcomment>
        <translation>ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="115"/>
        <location filename="../ui/device_list_button.cpp" line="145"/>
        <source>Shared</source>
        <translatorcomment>共享</translatorcomment>
        <translation>ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>DeviceMap</name>
    <message>
        <location filename="../device_manager/device_map.cpp" line="194"/>
        <source>Printer</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ</translation>
    </message>
    <message>
        <location filename="../device_manager/device_map.cpp" line="196"/>
        <source>Printers</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ</translation>
    </message>
    <message>
        <location filename="../device_manager/device_map.cpp" line="198"/>
        <source>plug-in:</source>
        <translation>ᠬᠠᠪᠴᠢᠭᠤᠯᠬᠤ:</translation>
    </message>
    <message>
        <location filename="../device_manager/device_map.cpp" line="200"/>
        <source>unplugged:</source>
        <translation>ᠰᠤᠭᠤᠯᠵᠤ ᠠᠪᠬᠤ:</translation>
    </message>
</context>
<context>
    <name>EmptyWidget</name>
    <message>
        <source>No Valid Printer,</source>
        <translation type="vanished">无可用打印机，</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/right_widget.cpp" line="101"/>
        <source>Please click &quot;+&quot; button to add a printer.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>EventNotifyMonitor</name>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="430"/>
        <location filename="../backend/event_notify_monitor.cpp" line="482"/>
        <source>Printer </source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ </translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="454"/>
        <location filename="../backend/event_notify_monitor.cpp" line="469"/>
        <source>Job:</source>
        <translation>ᠡᠬᠦᠷᠬᠡ:</translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="456"/>
        <source>created！</source>
        <translation>ᠪᠠᠢᠭᠤᠯᠪᠠ！</translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="471"/>
        <source>completed！</source>
        <translation>ᠳᠠᠭᠤᠰᠪᠠ！</translation>
    </message>
</context>
<context>
    <name>JobManagerModel</name>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="25"/>
        <source>Test Page</source>
        <translation>ᠲᠤᠷᠰᠢᠬᠤ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="39"/>
        <source>unknown</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="28"/>
        <source>untitled</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>JobManagerWindow</name>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="240"/>
        <source>Printer</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="387"/>
        <source>Cancel print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠶᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="390"/>
        <source>Delete print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠶᠢ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="393"/>
        <source>Hold print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠶᠢ ᠲᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="396"/>
        <source>Release print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠶᠢ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="399"/>
        <source>Repaint</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="415"/>
        <location filename="../ui/job_manager_window.cpp" line="421"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="415"/>
        <source>Set error: Job status has been updated!</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠯᠳᠠ ᠪᠤᠷᠤᠭᠤ: ᠡᠩᠨᠡᠭᠡ ᠶᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠰᠢᠨᠡᠴᠢᠯᠡᠪᠡ!</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="417"/>
        <location filename="../ui/job_manager_window.cpp" line="423"/>
        <source>Sure</source>
        <translation>ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="421"/>
        <source>Cannot move job to itself!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>JobMenu</name>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="78"/>
        <source>Cancel print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠶᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="80"/>
        <source>Delete print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠶᠢ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="82"/>
        <source>Hold print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠶᠢ ᠲᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="84"/>
        <source>Release print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ ᠶᠢ ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="86"/>
        <source>Repaint</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="88"/>
        <source>Use other printer...</source>
        <translation>ᠪᠤᠰᠤᠳ ᠫᠷᠢᠨᠲ᠋ᠧᠷ ᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ...</translation>
    </message>
</context>
<context>
    <name>LeftWidget</name>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="20"/>
        <source>Printer</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="57"/>
        <source>Device List</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠶᠢᠨ ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="219"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="366"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <source>This action will delete the job queue too!</source>
        <translation type="vanished">这个操作也会导致打印队列被清空！</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="215"/>
        <source>Are you sure to delete &quot;%1&quot;?</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ%1 ᠵᠢ/ ᠢ᠋ ᠯᠠᠪᠳᠠᠢ ᠤᠰᠠᠳᠬᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="220"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="364"/>
        <source>Set Default Failed!</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="368"/>
        <source>Close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>MainWinPropertyWidget</name>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="43"/>
        <source>Printer</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="72"/>
        <source>Property</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠳᠤ ᠴᠢᠨᠠᠷ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="79"/>
        <source>Job List</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="86"/>
        <source>PrintTest</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Model:</source>
        <translation type="vanished">型号：</translation>
    </message>
    <message>
        <source>Status:</source>
        <translation type="vanished">状态：</translation>
    </message>
    <message>
        <source>ON/OFF</source>
        <translation type="vanished">已连接/断开</translation>
    </message>
    <message>
        <source>Location:</source>
        <translation type="vanished">位置：</translation>
    </message>
    <message>
        <source>Driver:</source>
        <translation type="vanished">驱动：</translation>
    </message>
    <message>
        <source>Modify Driver</source>
        <translation type="vanished">修改驱动</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="179"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="181"/>
        <source>Idle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="183"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="185"/>
        <source>Printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="187"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="189"/>
        <source>Stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="194"/>
        <source>Unknown</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="219"/>
        <source>Printer Name Cannot Be Null!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="220"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="230"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="248"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="257"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="276"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="222"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="232"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="250"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="259"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="229"/>
        <source>Printer Name Cannot Contains &apos;/\&apos;&quot;?#&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="247"/>
        <source>Exist Same Name Printer!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="256"/>
        <source>Printer Name Illegal!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="274"/>
        <source>Are you sure to rename </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="277"/>
        <source>This action will delete the job queue too!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="279"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.ui" line="26"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.cpp" line="92"/>
        <source>Printer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="218"/>
        <source>Delete Failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="220"/>
        <location filename="../ui/mainwindow.cpp" line="259"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="222"/>
        <location filename="../ui/mainwindow.cpp" line="261"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="259"/>
        <source>Add printer failed，please retry after a while.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Hint</source>
        <translation type="vanished">提示</translation>
    </message>
    <message>
        <source>Add printer successfully，printer a test page？</source>
        <translation type="vanished">添加成功，是否打印测试页？</translation>
    </message>
    <message>
        <source>Print test page</source>
        <translation type="vanished">打印测试页</translation>
    </message>
    <message>
        <source>Check Printer</source>
        <translation type="vanished">查看设备</translation>
    </message>
    <message>
        <source>Is the test page printed successfully?</source>
        <translation type="vanished">打印测试页是否成功？</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">否</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">是</translation>
    </message>
    <message>
        <source>Test print failed.Do you want to change a printer driver?</source>
        <translation type="vanished">测试打印失败，是否更改驱动方案？</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Change Driver</source>
        <translation type="vanished">修改驱动</translation>
    </message>
    <message>
        <source>Printer is initializing...</source>
        <translation type="vanished">打印机正在启动…</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="326"/>
        <source>Sure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="324"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="319"/>
        <source>Try to connect the Printer...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="324"/>
        <source>Open Printer Failed,Please Del And Add Printer, Then Try Again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Printting...</source>
        <translation type="vanished">正在打印…</translation>
    </message>
</context>
<context>
    <name>ManualInstallWindow</name>
    <message>
        <source>Manual Install DebPackage</source>
        <translation type="vanished">手动安装打印机驱动</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>You Can&apos;t Install Multiply DebPackages!</source>
        <translation type="vanished">您不能同时安装多个包!</translation>
    </message>
    <message>
        <source>Sure</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <source>Please Choose DebPackage!!!</source>
        <translation type="vanished">请选择deb包!!!</translation>
    </message>
    <message>
        <source>Path Cannot Be Null!</source>
        <translation type="vanished">路径不能为空！</translation>
    </message>
    <message>
        <source>Manual Install Printetr Driver</source>
        <translation type="vanished">手动安装打印机驱动</translation>
    </message>
    <message>
        <source>OR You Can Download From WebSite:</source>
        <translation type="vanished">或者您可以去相关网址下载:</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation type="vanished">名称：</translation>
    </message>
    <message>
        <source>Location:</source>
        <translation type="vanished">位置：</translation>
    </message>
    <message>
        <source>Office:</source>
        <translation type="vanished">办公室：</translation>
    </message>
    <message>
        <source>Driver:</source>
        <translation type="vanished">驱动：</translation>
    </message>
    <message>
        <source>Manual Install Drivers</source>
        <translation type="vanished">手动选择驱动方案</translation>
    </message>
    <message>
        <source>You Cannot Input Inleagal Letters!</source>
        <translation type="vanished">不能输入非法字符!</translation>
    </message>
    <message>
        <source>Please Choose A DebPackage!</source>
        <translation type="vanished">请选择一个deb包</translation>
    </message>
    <message>
        <source>Packages Has Been Installed!</source>
        <translation type="vanished">已经安装过包!</translation>
    </message>
    <message>
        <source>Install Succeed, ReMatch Drivers!</source>
        <translation type="vanished">安装成功，重新匹配驱动!</translation>
    </message>
    <message>
        <source>Install Failed!</source>
        <translation type="vanished">安装失败！</translation>
    </message>
    <message>
        <source>Printer Name Should Not Be Null!</source>
        <translation type="vanished">打印机名称不可为空!</translation>
    </message>
</context>
<context>
    <name>MenuModule</name>
    <message>
        <location filename="../ui/menumodule.cpp" line="60"/>
        <location filename="../ui/menumodule.cpp" line="101"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="62"/>
        <location filename="../ui/menumodule.cpp" line="98"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="64"/>
        <location filename="../ui/menumodule.cpp" line="104"/>
        <source>Configure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="66"/>
        <location filename="../ui/menumodule.cpp" line="94"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="231"/>
        <source>Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="283"/>
        <location filename="../ui/menumodule.cpp" line="291"/>
        <source>Service &amp; Support: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/menumodule.h" line="58"/>
        <source>Printer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NewPopWindow</name>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.ui" line="32"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="40"/>
        <source>Printer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="59"/>
        <source>Print Test Page</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="60"/>
        <source>View Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="61"/>
        <source>Manual Install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="128"/>
        <source>Installing......</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="143"/>
        <source>Successful installation!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="164"/>
        <source>Installation failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="188"/>
        <source>Printer Detected:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Waring</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="205"/>
        <source>Is the test page printed successfully?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="207"/>
        <source>No</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="206"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="205"/>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="212"/>
        <source>Hint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="212"/>
        <source>Test print failed.Do you want to change a printer driver?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="214"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="213"/>
        <source>Change Driver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Printer is running...</source>
        <translation type="vanished">打印机正在启动...</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="226"/>
        <source>Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="226"/>
        <source>Failed to start, please try to add the printer again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="229"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PropertyListWindow</name>
    <message>
        <source>PrinterProperty</source>
        <translation type="vanished">打印机属性</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation type="vanished">名称：</translation>
    </message>
    <message>
        <source>Location:</source>
        <translation type="vanished">位置：</translation>
    </message>
    <message>
        <source>Status:</source>
        <translation type="vanished">状态：</translation>
    </message>
    <message>
        <source>Modify</source>
        <translation type="vanished">修改</translation>
    </message>
    <message>
        <source>Driection:</source>
        <translation type="vanished">打印方向：</translation>
    </message>
    <message>
        <source>Dual:</source>
        <translation type="vanished">双面打印：</translation>
    </message>
    <message>
        <source>Pages per Side:</source>
        <translation type="vanished">每面页数：</translation>
    </message>
    <message>
        <source>Ink Rank:</source>
        <translation type="vanished">墨水等级：</translation>
    </message>
    <message>
        <source>Output Order:</source>
        <translation type="vanished">输出顺序：</translation>
    </message>
    <message>
        <source>Scale To Fit Size:</source>
        <translation type="vanished">缩放到合适尺寸：</translation>
    </message>
    <message>
        <source>Idle</source>
        <translation type="vanished">空闲</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation type="vanished">忙碌</translation>
    </message>
    <message>
        <source>Stopped</source>
        <translation type="vanished">停止</translation>
    </message>
    <message>
        <source>Advanced Options</source>
        <translation type="vanished">高级选项</translation>
    </message>
    <message>
        <source>Quality</source>
        <translation type="vanished">质量</translation>
    </message>
    <message>
        <source>Duplex</source>
        <translation type="vanished">双面打印</translation>
    </message>
    <message>
        <source>Error happened, all options restored!.</source>
        <translation type="vanished">错误发生，所有选项重置！</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="10"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Printer Name Cannot Contains &apos;/\&apos;&quot;?#&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation type="vanished">打印机名称不能包含&apos;/\&apos;&quot;?#&apos;,而且不能为空，且少于128个字母!</translation>
    </message>
    <message>
        <source>Driver:</source>
        <translation type="vanished">驱动：</translation>
    </message>
    <message>
        <source>URI:</source>
        <translation type="vanished">URI：</translation>
    </message>
    <message>
        <source>PageSize:</source>
        <translation type="vanished">大小：</translation>
    </message>
    <message>
        <source>Resolution:</source>
        <translation type="vanished">分辨率：</translation>
    </message>
    <message>
        <source>Source:</source>
        <translation type="vanished">来源：</translation>
    </message>
    <message>
        <source>PageType:</source>
        <translation type="vanished">纸张类型：</translation>
    </message>
    <message>
        <source>Color:</source>
        <translation type="vanished">颜色：</translation>
    </message>
    <message>
        <source>Quality:</source>
        <translation type="vanished">质量：</translation>
    </message>
    <message>
        <source>Direction:</source>
        <translation type="vanished">打印方向：</translation>
    </message>
    <message>
        <source>Duplex:</source>
        <translation type="vanished">双面打印：</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">名称</translation>
    </message>
    <message>
        <source>Location</source>
        <translation type="vanished">位置</translation>
    </message>
    <message>
        <source>Status</source>
        <translation type="vanished">状态</translation>
    </message>
    <message>
        <source>Unknow</source>
        <translation type="vanished">未知</translation>
    </message>
    <message>
        <source>Driver</source>
        <translation type="vanished">驱动</translation>
    </message>
    <message>
        <source>URI</source>
        <translation type="vanished">URI</translation>
    </message>
    <message>
        <source>PageSize</source>
        <translation type="vanished">大小</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation type="vanished">分辨率</translation>
    </message>
    <message>
        <source>Source</source>
        <translation type="vanished">来源</translation>
    </message>
    <message>
        <source>PageType</source>
        <translation type="vanished">纸张类型</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">颜色</translation>
    </message>
    <message>
        <source>Mono</source>
        <translation type="vanished">单色</translation>
    </message>
    <message>
        <source>Printer Name Cannot Contains &apos;\/&apos;&quot;?#&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation type="vanished">打印机名称不能包含&apos;\/&apos;&quot;?#&apos;,而且不能为空，且少于128个字母!</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>Exist Same Name Printer!</source>
        <translation type="vanished">存在同名打印机！</translation>
    </message>
    <message>
        <source>Printer Name Illegal!</source>
        <translation type="vanished">打印机名称不合法！</translation>
    </message>
    <message>
        <source>Form</source>
        <translation type="vanished">ui标题</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation type="vanished">ui标签</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="9"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Add</source>
        <translation type="vanished">修改</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.ui" line="26"/>
        <source>PropertyListWindow</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>PropertyManagerModel</name>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="127"/>
        <source>Model:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="128"/>
        <source>Status:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="129"/>
        <source>Location:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="130"/>
        <source>Driver:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>PropertyWindow</name>
    <message>
        <source>Printer Driver</source>
        <translation type="vanished">打印机驱动</translation>
    </message>
    <message>
        <source>Printer Property</source>
        <translation type="vanished">打印机属性</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation type="vanished">名称：</translation>
    </message>
    <message>
        <source>Location:</source>
        <translation type="vanished">位置：</translation>
    </message>
    <message>
        <source>Driver:</source>
        <translation type="vanished">驱动：</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="197"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="342"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="358"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="369"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Printer Is Initializing...</source>
        <translation type="vanished">打印机正在启动...</translation>
    </message>
    <message>
        <source>Sure</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>Open Printer Failed,Please Del And Add Printer, Then Try Again!</source>
        <translation type="vanished">启动打印机失败，请重新添加打印机！</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="20"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="59"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="122"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="245"/>
        <source>base</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="22"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="249"/>
        <source>advanced</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="40"/>
        <source>PrinterProperty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="75"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="130"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="275"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="76"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="133"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="279"/>
        <source>Location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="77"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="137"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="284"/>
        <source>Status</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="78"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="157"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="288"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="326"/>
        <source>Driver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="81"/>
        <source>Modify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="83"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="160"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="292"/>
        <source>URI</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="139"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="141"/>
        <source>Idle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="143"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="145"/>
        <source>Printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="147"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="149"/>
        <source>Stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="197"/>
        <source>Error happened, all options restored!.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="199"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="341"/>
        <source>Printer Name Cannot Contains &apos;/\&apos;&quot;?#&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="344"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="360"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="371"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="357"/>
        <source>Exist Same Name Printer!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="368"/>
        <source>Printer Name Illegal!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="154"/>
        <source>Unknow</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ProperytItemWidget</name>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="214"/>
        <source>Idle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="217"/>
        <source>Printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="220"/>
        <source>Stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="223"/>
        <source>Unknow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="obsolete">未知</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ui/job_manager_window.h" line="30"/>
        <source>id</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="31"/>
        <source>user</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="32"/>
        <source>title</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="33"/>
        <source>printer name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="34"/>
        <source>size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="35"/>
        <source>create time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="36"/>
        <source>job state</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="42"/>
        <source>Job is waiting to be printed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="43"/>
        <source>Job is held for printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="44"/>
        <source>Job is currently printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="45"/>
        <source>Job has been stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="46"/>
        <source>Job has been canceled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="47"/>
        <source>Job has aborted due to error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="48"/>
        <source>Job has completed successfully</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QuadBtnsTitleBar</name>
    <message>
        <location filename="../ui/main_win_ui/quad_btns_title_bar.cpp" line="16"/>
        <source>menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/quad_btns_title_bar.cpp" line="25"/>
        <source>minimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/quad_btns_title_bar.cpp" line="39"/>
        <source>full screen</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/quad_btns_title_bar.cpp" line="53"/>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RenamePrinterDialog</name>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="26"/>
        <source>RenamePrinterDialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="148"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="167"/>
        <source>Ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.cpp" line="11"/>
        <source>Printer Name</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SelectPpdDialog</name>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="24"/>
        <source>Printer Driver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="96"/>
        <source>Reading Drivers，Please Wait...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="203"/>
        <source>Please Choose Model And Vender!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="204"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="206"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Config failure!</source>
        <translation type="vanished">设置失败！</translation>
    </message>
    <message>
        <source>Unset!</source>
        <translation type="vanished">未设置！</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="26"/>
        <source>SelectPpdDialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="316"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="354"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SuccedFailWindow</name>
    <message>
        <source>Printer Driver</source>
        <translation type="vanished">打印机驱动</translation>
    </message>
    <message>
        <source>Printer Diver</source>
        <translation type="vanished">打印机驱动</translation>
    </message>
    <message>
        <source>Print Test</source>
        <translation type="vanished">打印测试</translation>
    </message>
    <message>
        <source>Check Device</source>
        <translation type="vanished">查看设备</translation>
    </message>
    <message>
        <source>Reinstall</source>
        <translation type="vanished">重新安装</translation>
    </message>
    <message>
        <source>Cloud Print</source>
        <translation type="vanished">使用云打印</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>Printer Is Initializing...</source>
        <translation type="vanished">打印机正在启动...</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">确认</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>Printer Open Failed, Please Try To Reset This Window</source>
        <translation type="vanished">打印机启动失败,尝试关闭窗口再次打开!</translation>
    </message>
</context>
<context>
    <name>SystemNotification</name>
    <message>
        <location filename="../util/system_notification.cpp" line="21"/>
        <source>Printer Info</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../util/system_notification.cpp" line="32"/>
        <source>Printer</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TrayWin</name>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="33"/>
        <source>Minimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="39"/>
        <source>Maximize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="45"/>
        <source>Quit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UkuiPrinterManager</name>
    <message>
        <source>Test Page</source>
        <translation type="obsolete">测试页</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="336"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1414"/>
        <source>Idle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="338"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1416"/>
        <source>Printing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="340"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1418"/>
        <source>Stopped</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="360"/>
        <source>Printer Name Cannot Contains &apos;/\&apos;&quot;?#&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="377"/>
        <source>Exist Same Name Printer!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="380"/>
        <source>Printer name is not case sensitive!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>addPrinterWindow</name>
    <message>
        <source>Printer</source>
        <translation type="vanished">添加打印机</translation>
    </message>
    <message>
        <source>Add Printer</source>
        <translation type="vanished">添加打印机</translation>
    </message>
    <message>
        <source>Network</source>
        <translation type="vanished">网络</translation>
    </message>
    <message>
        <source>USB</source>
        <translation type="vanished">USB</translation>
    </message>
    <message>
        <source>Device List</source>
        <translation type="vanished">设备列表</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation type="vanished">刷新</translation>
    </message>
    <message>
        <source>Address:</source>
        <translation type="vanished">地址：</translation>
    </message>
    <message>
        <source>search</source>
        <translation type="vanished">查找</translation>
    </message>
    <message>
        <source>Available printers:</source>
        <translation type="vanished">可用网络打印机</translation>
    </message>
    <message>
        <source>name</source>
        <translation type="vanished">名称</translation>
    </message>
    <message>
        <source>location</source>
        <translation type="vanished">位置</translation>
    </message>
    <message>
        <source>driver</source>
        <translation type="vanished">驱动</translation>
    </message>
    <message>
        <source>change</source>
        <translation type="vanished">选择</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>forward</source>
        <translation type="vanished">添加</translation>
    </message>
    <message>
        <source>Error</source>
        <translation type="vanished">错误</translation>
    </message>
    <message>
        <source>Add printer failed，please retry after a while.</source>
        <translation type="vanished">添加打印机失败，请重试!</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Hint</source>
        <translation type="vanished">提示</translation>
    </message>
    <message>
        <source>Add printer successfully，printer a test page？</source>
        <translation type="vanished">添加成功，是否打印测试页？</translation>
    </message>
    <message>
        <source>Print test page</source>
        <translation type="vanished">打印测试页</translation>
    </message>
    <message>
        <source>Check Printer</source>
        <translation type="vanished">查看设备</translation>
    </message>
    <message>
        <source>Is the test page printed successfully?</source>
        <translation type="vanished">打印测试页是否成功？</translation>
    </message>
    <message>
        <source>No</source>
        <translation type="vanished">否</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">是</translation>
    </message>
    <message>
        <source>Test print failed.Do you want to change a printer driver?</source>
        <translation type="vanished">测试打印失败，是否更改驱动方案？</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Change Driver</source>
        <translation type="vanished">修改驱动</translation>
    </message>
    <message>
        <source>Searching...</source>
        <translation type="vanished">搜索中……</translation>
    </message>
    <message>
        <source>Can not find this Printer!</source>
        <translation type="vanished">无法找到打印机</translation>
    </message>
    <message>
        <source>Please select a deb package.</source>
        <translation type="vanished">请选择deb驱动包</translation>
    </message>
    <message>
        <source>Deb File(*.deb)</source>
        <translation type="vanished">安装包(*.deb)</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">打开</translation>
    </message>
    <message>
        <source>Searching driver...</source>
        <translation type="vanished">正在搜索驱动程序……</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">警告</translation>
    </message>
    <message>
        <source>Install package failed，please retry.</source>
        <translation type="vanished">安装驱动包失败，请重试！</translation>
    </message>
    <message>
        <source>No driver package selected,Continue?</source>
        <translation type="vanished">没有选择驱动包，是否继续？</translation>
    </message>
    <message>
        <source>Searching printers...</source>
        <translation type="vanished">正在搜索打印机……</translation>
    </message>
    <message>
        <source>Choose</source>
        <translation type="vanished">选择</translation>
    </message>
    <message>
        <source>Searching printer driver...</source>
        <translation type="vanished">正在安装打印机驱动……</translation>
    </message>
    <message>
        <source>Install driver package automatically failed,continue?</source>
        <translation type="vanished">服务器无精准匹配驱动包，是否继续本地驱动安装?</translation>
    </message>
    <message>
        <source>Printer Name Cannot Contains &apos;/\&apos;&quot;?#&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation type="vanished">打印机名称不能包含&apos;/\&apos;&quot;?#&apos;,而且不能为空，且少于128个字母!</translation>
    </message>
    <message>
        <source>Printer Name Cannot Contains &apos;\/&apos;&quot;?#&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation type="vanished">打印机名称不能包含&apos;\/&apos;&quot;?#&apos;,而且不能为空，且少于128个字母!</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>Exist Same Name Printer!</source>
        <translation type="vanished">存在同名打印机！</translation>
    </message>
    <message>
        <source>Printer Name Illegal!</source>
        <translation type="vanished">打印机名称不合法！</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Printer</source>
        <translation type="vanished">打印机</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="vanished">主题</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">帮助</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Config</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation type="vanished">自动</translation>
    </message>
    <message>
        <source>Light</source>
        <translation type="vanished">浅色</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation type="vanished">深色</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本号</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
</context>
<context>
    <name>ukuiPrinter</name>
    <message>
        <source>Idle</source>
        <translation type="vanished">空闲</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation type="vanished">忙碌</translation>
    </message>
    <message>
        <source>Stopped</source>
        <translation type="vanished">停止</translation>
    </message>
</context>
<context>
    <name>ukuiPrinterManager</name>
    <message>
        <source>Test Page</source>
        <translation type="vanished">测试页</translation>
    </message>
    <message>
        <source>Idle</source>
        <translation type="vanished">空闲</translation>
    </message>
    <message>
        <source>Printing</source>
        <translation type="vanished">忙碌</translation>
    </message>
    <message>
        <source>Stopped</source>
        <translation type="vanished">停止</translation>
    </message>
</context>
</TS>
