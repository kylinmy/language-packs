<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name></name>
    <message>
        <location filename="../kylin-printer.desktop.in.h" line="1"/>
        <source>Printers</source>
        <translation>پرىنتېرلاش</translation>
    </message>
    <message>
        <location filename="../backend/data/kylin-printer-applet.desktop.in.h" line="1"/>
        <source>Printers-backend</source>
        <translation>پرىنتېرلاش-ئارقا قوشۇمچىسى</translation>
    </message>
</context>
<context>
    <name>AddPrinterWindow</name>
    <message>
        <location filename="../ui/add_printer_window.ui" line="26"/>
        <source>AddPrinterWindow</source>
        <translation>AddPrinterWindow</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="117"/>
        <source>Add Printer</source>
        <translation>پرىنتېرلاش خەت چوڭ-كىچىكلىكىنى قوشۇش</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="132"/>
        <source>Auto</source>
        <translation>ئاپتۇماتىك</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="133"/>
        <location filename="../ui/add_printer_window.cpp" line="170"/>
        <source>Manual</source>
        <translation>قوللانما</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="145"/>
        <source>Device List</source>
        <translation>ئۈسكۈنە تىزىملىكى</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="171"/>
        <source>Protocol</source>
        <translation>كېلىشىم</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="172"/>
        <source>Address</source>
        <translation>ئادرېس</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="178"/>
        <source>Search</source>
        <translation>ئىزدە</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="182"/>
        <source>socket</source>
        <translation>socket</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="183"/>
        <source>ipp</source>
        <translation>ipp</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="184"/>
        <source>http</source>
        <translation>http</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="190"/>
        <source>name</source>
        <translation>ئىسىم</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="191"/>
        <source>location</source>
        <translation>ئورنى</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="192"/>
        <source>driver</source>
        <translation>شوپۇر</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="197"/>
        <source>forward</source>
        <translation>ئالدىغا</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="226"/>
        <location filename="../ui/add_printer_window.cpp" line="240"/>
        <source>Error</source>
        <translation>خاتالىق</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="226"/>
        <source>Add printer failed: no PPD selected!</source>
        <translation>پرىنتېرنى قوشۇش مەغلۇپ بولدى: PPD تاللانمىدى!</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="240"/>
        <source>Add printer failed，please retry after a while.</source>
        <translation>پرىنتېرنى قوشۇۋالسىڭىز مەغلۇپ بولىدۇ، بىر مەزگىل ئۆتكەندىن كېيىن قايتا قايتا قىلىڭ.</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="243"/>
        <location filename="../ui/add_printer_window.cpp" line="442"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="246"/>
        <location filename="../ui/add_printer_window.cpp" line="254"/>
        <location filename="../ui/add_printer_window.cpp" line="264"/>
        <source>Hint</source>
        <translation>ئەسكەرتىش</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="246"/>
        <source>Add printer successfully，printer a test page？</source>
        <translation>پرىنتېرنى مۇۋەپپەقىيەتلىك قوشىمىز،پرىنتېردا سىناق بېتى بارمۇ؟</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="248"/>
        <source>Print test page</source>
        <translation>بېسىپ چىقىرىش سىنىقى بېتى</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="249"/>
        <source>Check Printer</source>
        <translation>پرىنتېرلاشنى تەكشۈرۈش</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="254"/>
        <source>Is the test page printed successfully?</source>
        <translation>سىناق بېتى مۇۋەپپەقىيەتلىك بېسىلىپ بولدىمۇ؟</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="256"/>
        <source>Yes</source>
        <translation>شۇنداق</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="257"/>
        <source>No</source>
        <translation>ياق</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="264"/>
        <source>Test print failed.Do you want to change a printer driver?</source>
        <translation>سىناق بېسىپ چىقىرىش مەغلۇپ بولدى. پرىنتېر شوپۇرىنى ئالماشتۇرامسىز؟</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="266"/>
        <source>Change Driver</source>
        <translation>قوزغاچىنى ئۆزگەرتىش</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="267"/>
        <location filename="../ui/add_printer_window.cpp" line="443"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="290"/>
        <source>Searching...</source>
        <translation>ئىزدىنىۋاتىدۇ...</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="299"/>
        <location filename="../ui/add_printer_window.cpp" line="351"/>
        <source>Searching printers...</source>
        <translation>پرىنتېرلار ئاختۇرۇۋاتىدۇ...</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="331"/>
        <source>Can not find this Printer!</source>
        <translation>بۇ پرىنتېرنى تاپالمىدىم!</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="418"/>
        <source>Searching printer driver...</source>
        <translation>پرىنتېر شوپۇرى ئاختۇرۇۋاتىدۇ...</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="440"/>
        <location filename="../ui/add_printer_window.cpp" line="499"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="440"/>
        <source>Install driver package automatically failed,continue?</source>
        <translation>شوپۇرلۇق بوغچىسىنى قاچىلاش ئاپتوماتىك مەغلۇپ بولدى، داۋاملاشتۇرامسىز؟</translation>
    </message>
    <message>
        <source>Printer Name Cannot Contains &apos;/\&apos;&quot;?#&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation type="vanished">پرىنتېر نامى &apos;/\&apos;?#&apos;, 0 دىن ئارتۇق ھەرىپى، 128 ھەرپتىن كىچىك بولالمايدۇ !</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="501"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
    <message>
        <source>Exist Same Name Printer!</source>
        <translation type="vanished">مەۋجۇتلۇق ئوخشاش ئىسىمدىكى پرىنتېرلاش ماشىنىسى!</translation>
    </message>
    <message>
        <source>Printer Name Illegal!</source>
        <translation type="vanished">پرىنتېرلاش نامى قانۇنسىز!</translation>
    </message>
</context>
<context>
    <name>AutoSearchResultModel</name>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="639"/>
        <source>network</source>
        <translation>تور</translation>
    </message>
</context>
<context>
    <name>BaseNotifyDialog</name>
    <message>
        <location filename="../ui/main_win_ui/base_notify_dialog.ui" line="35"/>
        <source>Dialog</source>
        <translation>دىئالوگ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/base_notify_dialog.ui" line="124"/>
        <source>TextLabel</source>
        <translation>TextLabel</translation>
    </message>
</context>
<context>
    <name>ChoosePpdComboBox</name>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="15"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="22"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="137"/>
        <source>Choose PPD</source>
        <translation>PPD نى تاللاش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="16"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="25"/>
        <source>Choose from the PPD library</source>
        <translation>PPD ئامبىرىدىن تاللاش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="17"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="29"/>
        <source>Add local PPD</source>
        <translation>يەرلىك PPD قوشۇش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="48"/>
        <source>Please select a deb package.</source>
        <translation>Deb يۈرۈشلۈكىنى تاللاڭ.</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="50"/>
        <source>Deb File(*.deb)</source>
        <translation>Deb ھۆججىتى(*.deb)</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="51"/>
        <source>Choose</source>
        <translation>تاللاش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="52"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="69"/>
        <source>Searching driver...</source>
        <translation>ئىزدەۋاتقان شوپۇر...</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="91"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="91"/>
        <source>Install package failed，please retry.</source>
        <translation>قاچىلاش بوغچىسى مەغلۇپ بولدى، قايتا قايتا قاچىلاڭ.</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="92"/>
        <source>Yes</source>
        <translation>شۇنداق</translation>
    </message>
    <message>
        <source>No driver package selected,Continue?</source>
        <translation type="vanished">شوپۇرلۇق بوغچىسى تاللىمىدى،داۋاملاشتۇرامسىز؟</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">جەزىملەشتۈرۈش</translation>
    </message>
</context>
<context>
    <name>ConfigIPAddressDialog</name>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>دىئالوگ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="247"/>
        <source>初始地址:api.kylinos.cn</source>
        <translation>初始址:api.kylinos.cn</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="22"/>
        <source>Configure</source>
        <translation>سەپلىمە</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="23"/>
        <source>Service address:</source>
        <translation>مۇلازىمەت ئادرېسى:</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="24"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="25"/>
        <source>Ok</source>
        <translation>ماقۇل</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="37"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="37"/>
        <source>Config init failed</source>
        <translation>سەپلىمە init مەغلۇپ بولدى</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="40"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
</context>
<context>
    <name>CupsDebugLoggingCheckbox</name>
    <message>
        <location filename="../ui/cups_debug_logging_checkbox.cpp" line="8"/>
        <source>Retain debug info for troubleshooting</source>
        <translation>چاتاق يوقلاش ئۈچۈن دېففېكت ئۇچۇرىنى ساقلاپ قېلىش</translation>
    </message>
</context>
<context>
    <name>DebInstallWindow</name>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="32"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="75"/>
        <source>Driver</source>
        <translation>شوپۇر</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="143"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="162"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="23"/>
        <source>Modify Printer Driver</source>
        <translation>پرىنتېر قوزغاغۇچنى ئۆزگەرتىش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="54"/>
        <source>Error</source>
        <translation>خاتالىق</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="54"/>
        <source>Modify PPD failed: no PPD selected!</source>
        <translation>PPD نى ئۆزگەرتىش مەغلۇپ بولدى: PPD تاللانمىدى!</translation>
    </message>
</context>
<context>
    <name>DeviceListButton</name>
    <message>
        <location filename="../ui/device_list_button.cpp" line="34"/>
        <source>Idle</source>
        <translation>بىكارچىلىق</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="37"/>
        <source>Printing</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="40"/>
        <source>Stopped</source>
        <translation>توختى</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="43"/>
        <source>Unknow</source>
        <translation>ئۇقۇشمىغانلار</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="64"/>
        <location filename="../ui/device_list_button.cpp" line="281"/>
        <location filename="../ui/device_list_button.cpp" line="313"/>
        <source>Default</source>
        <translation>كۆڭۈلدىكى سۆز</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="129"/>
        <location filename="../ui/device_list_button.cpp" line="159"/>
        <source>Set Default</source>
        <translation>كۆڭۈلدىكىنى بەلگىلەش</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="131"/>
        <location filename="../ui/device_list_button.cpp" line="162"/>
        <source>Enabled</source>
        <translation>قوزغىتىش</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="135"/>
        <location filename="../ui/device_list_button.cpp" line="165"/>
        <source>Shared</source>
        <translation>ئورتاق بەھرىلىنىش</translation>
    </message>
</context>
<context>
    <name>DeviceMap</name>
    <message>
        <location filename="../device_manager/device_map.cpp" line="290"/>
        <source>Printer</source>
        <translation>پرىنتېرلاش</translation>
    </message>
    <message>
        <location filename="../device_manager/device_map.cpp" line="292"/>
        <source>Printers</source>
        <translation>پرىنتېرلاش</translation>
    </message>
    <message>
        <location filename="../device_manager/device_map.cpp" line="294"/>
        <source>plug-in:</source>
        <translation>قىستۇرما:</translation>
    </message>
    <message>
        <location filename="../device_manager/device_map.cpp" line="296"/>
        <source>unplugged:</source>
        <translation>توك مەنبەسىدىن ئايرىلدى:</translation>
    </message>
</context>
<context>
    <name>EmptyWidget</name>
    <message>
        <location filename="../ui/main_win_ui/right_widget.cpp" line="101"/>
        <source>Please click &quot;+&quot; button to add a printer.</source>
        <translation>پرىنتېر قوشۇش ئۈچۈن «+» كونۇپكىسىنى چېكىڭ.</translation>
    </message>
</context>
<context>
    <name>EventNotifyMonitor</name>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="430"/>
        <location filename="../backend/event_notify_monitor.cpp" line="482"/>
        <source>Printer </source>
        <translation>پرىنتېرلاش </translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="454"/>
        <location filename="../backend/event_notify_monitor.cpp" line="469"/>
        <source>Job:</source>
        <translation>خىزمەت ئورنى:</translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="456"/>
        <source>created！</source>
        <translation>يارىتىلغان!</translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="471"/>
        <source>completed！</source>
        <translation>تاماملاندى!</translation>
    </message>
</context>
<context>
    <name>JobManagerModel</name>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="25"/>
        <source>Test Page</source>
        <translation>سىنا بېتى</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="28"/>
        <source>untitled</source>
        <translation>خەتسىز</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="39"/>
        <source>unknown</source>
        <translation>نامەلۇم</translation>
    </message>
</context>
<context>
    <name>JobManagerWindow</name>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="237"/>
        <source>Printer</source>
        <translation>پرىنتېرلاش</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="381"/>
        <source>Cancel print</source>
        <translation>بېسىپ چىقىرىشنى بىكار قىلىش</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="384"/>
        <source>Delete print</source>
        <translation>بېسىپ چىقىرىشنى ئۆچۈرۈش</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="387"/>
        <source>Hold print</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="390"/>
        <source>Release print</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="393"/>
        <source>Repaint</source>
        <translation>قايتا قاز</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="409"/>
        <location filename="../ui/job_manager_window.cpp" line="415"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="409"/>
        <source>Set error: Job status has been updated!</source>
        <translation>بەلگىلەش خاتالىقى: خىزمەت ھالىتى يېڭىلاندى!</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="411"/>
        <location filename="../ui/job_manager_window.cpp" line="417"/>
        <source>Sure</source>
        <translation>ئەلۋەتتە</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="415"/>
        <source>Cannot move job to itself!</source>
        <translation>خىزمەتنى ئۆزىگە يۆتكىيەلمەيدۇ!</translation>
    </message>
</context>
<context>
    <name>JobMenu</name>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="78"/>
        <source>Cancel print</source>
        <translation>بېسىپ چىقىرىشنى بىكار قىلىش</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="80"/>
        <source>Delete print</source>
        <translation>بېسىپ چىقىرىشنى ئۆچۈرۈش</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="82"/>
        <source>Hold print</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="84"/>
        <source>Release print</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="86"/>
        <source>Repaint</source>
        <translation>قايتا قاز</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="88"/>
        <source>Use other printer...</source>
        <translation>باشقا پرىنتېرنى ئىشلىتىڭ...</translation>
    </message>
</context>
<context>
    <name>LeftWidget</name>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="20"/>
        <source>Printer</source>
        <translation>پرىنتېرلاش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="57"/>
        <source>Device List</source>
        <translation>ئۈسكۈنە تىزىملىكى</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="214"/>
        <source>Are you sure to delete &quot;%1&quot;?</source>
        <translation>«٪1»نى چوقۇم ئۆچۈرەمسىز؟</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">شۇنداق</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="218"/>
        <source>Delete</source>
        <translation>ئۆچۈر</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="219"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="363"/>
        <source>Set Default Failed!</source>
        <translation>كۆڭۈلدىكىنى بەلگىلەش مەغلۇپ بولدى!</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="365"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="367"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
</context>
<context>
    <name>MainWinPropertyWidget</name>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.ui" line="26"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="43"/>
        <source>Printer</source>
        <translation>پرىنتېرلاش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="72"/>
        <source>Property</source>
        <translation>مال-مۈلۈك</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="79"/>
        <source>Job List</source>
        <translation>خىزمەت تىزىملىكى</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="86"/>
        <source>PrintTest</source>
        <translation>PrintTest</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="183"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="185"/>
        <source>Idle</source>
        <translation>بىكارچىلىق</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="187"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="189"/>
        <source>Printing</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="191"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="193"/>
        <source>Stopped</source>
        <translation>توختى</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="198"/>
        <source>Unknown</source>
        <translation>نامەلۇم</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="223"/>
        <source>Printer Name Cannot Be Null!</source>
        <translation>پرىنتېرلاش نامى Null بولالمايدۇ!</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="224"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="234"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="252"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="261"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="280"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="226"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="236"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="254"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="263"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="233"/>
        <source>Printer Name Cannot Contains &apos;/\&apos;&quot;?#&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation>پرىنتېر نامى &apos;/\&apos;?#&apos;, 0 دىن ئارتۇق ھەرىپى، 128 ھەرپتىن كىچىك بولالمايدۇ !</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="251"/>
        <source>Exist Same Name Printer!</source>
        <translation>مەۋجۇتلۇق ئوخشاش ئىسىمدىكى پرىنتېرلاش ماشىنىسى!</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="260"/>
        <source>Printer Name Illegal!</source>
        <translation>پرىنتېرلاش نامى قانۇنسىز!</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="278"/>
        <source>Are you sure to rename </source>
        <translation>سىز چوقۇم قايتىدىن ئىسىم قويامسىز؟ </translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="281"/>
        <source>This action will delete the job queue too!</source>
        <translation>بۇ ھەرىكەتمۇ خىزمەت قاتارىنى ئۆچۈرىۋېتىدۇ!</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="283"/>
        <source>Yes</source>
        <translation>شۇنداق</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.cpp" line="92"/>
        <source>Printer</source>
        <translation>پرىنتېرلاش</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="218"/>
        <source>Delete Failed!</source>
        <translation>ئۆچۈرۈش مەغلۇپ بولدى!</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="220"/>
        <location filename="../ui/mainwindow.cpp" line="259"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="222"/>
        <location filename="../ui/mainwindow.cpp" line="261"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="259"/>
        <source>Add printer failed，please retry after a while.</source>
        <translation>پرىنتېرنى قوشۇۋالسىڭىز مەغلۇپ بولىدۇ، بىر مەزگىل ئۆتكەندىن كېيىن قايتا قايتا قىلىڭ.</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="319"/>
        <source>Try to connect the Printer...</source>
        <translation>پرىنتېرنى ئۇلاشقا تىرىشىڭ...</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="324"/>
        <source>Error</source>
        <translation>خاتالىق</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="324"/>
        <source>Open Printer Failed,Please Del And Add Printer, Then Try Again!</source>
        <translation>پرىنتېرنى ئېچىش مەغلۇپ بولدى، Del دىن خەت باسقۇچىنى قوشۇۋېلىڭ، ئاندىن قايتا سىناپ بېقىڭ!</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="326"/>
        <source>Sure</source>
        <translation>ئەلۋەتتە</translation>
    </message>
</context>
<context>
    <name>MenuModule</name>
    <message>
        <location filename="../ui/menumodule.cpp" line="56"/>
        <location filename="../ui/menumodule.cpp" line="97"/>
        <source>Help</source>
        <translation>ياردەم</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="58"/>
        <location filename="../ui/menumodule.cpp" line="94"/>
        <source>About</source>
        <translation>ھەققىدە</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="60"/>
        <location filename="../ui/menumodule.cpp" line="100"/>
        <source>Configure</source>
        <translation>سەپلىمە</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="62"/>
        <location filename="../ui/menumodule.cpp" line="90"/>
        <source>Quit</source>
        <translation>چېكىنىش</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">نەشرى: </translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">مۇلازىمەت &gt; قوللاش: </translation>
    </message>
    <message>
        <location filename="../ui/menumodule.h" line="58"/>
        <source>Printer</source>
        <translation>پرىنتېرلاش</translation>
    </message>
</context>
<context>
    <name>NewPopWindow</name>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.ui" line="32"/>
        <source>Form</source>
        <translation>جەدۋەل</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="40"/>
        <source>Printer</source>
        <translation>پرىنتېرلاش</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="59"/>
        <source>Print Test Page</source>
        <translation>بېسىپ چىقىرىش سىناق بېتى</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="60"/>
        <source>View Device</source>
        <translation>ئۈسكۈنىنى كۆرۈش</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="61"/>
        <source>Manual Install</source>
        <translation>قولدا قاچىلاش</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="128"/>
        <source>Installing......</source>
        <translation>قاچىلاش ......</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="143"/>
        <source>Successful installation!</source>
        <translation>مۇۋەپپىقىيەتلىك قاچىلاش!</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="164"/>
        <source>Installation failed!</source>
        <translation>قاچىلاش مەغلۇپ بولدى!</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="188"/>
        <source>Printer Detected:</source>
        <translation>پرىنتېر بايقالدى:</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="205"/>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="212"/>
        <source>Hint</source>
        <translation>ئەسكەرتىش</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="205"/>
        <source>Is the test page printed successfully?</source>
        <translation>سىناق بېتى مۇۋەپپەقىيەتلىك بېسىلىپ بولدىمۇ؟</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="206"/>
        <source>Yes</source>
        <translation>شۇنداق</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="207"/>
        <source>No</source>
        <translation>ياق</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="212"/>
        <source>Test print failed.Do you want to change a printer driver?</source>
        <translation>سىناق بېسىپ چىقىرىش مەغلۇپ بولدى. پرىنتېر شوپۇرىنى ئالماشتۇرامسىز؟</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="213"/>
        <source>Change Driver</source>
        <translation>قوزغاچىنى ئۆزگەرتىش</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="214"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="226"/>
        <source>Error</source>
        <translation>خاتالىق</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="226"/>
        <source>Failed to start, please try to add the printer again!</source>
        <translation>باشلانمىدى، پرىنتېرنى يەنە بىر قېتىم قوشۇشنى سىناپ بېقىڭ!</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="229"/>
        <source>Ok</source>
        <translation>ماقۇل</translation>
    </message>
</context>
<context>
    <name>PropertyListWindow</name>
    <message>
        <location filename="../ui/new_property_window/property_list_window.ui" line="26"/>
        <source>PropertyListWindow</source>
        <translation>PropertyListWindow</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="9"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="10"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
</context>
<context>
    <name>PropertyManagerModel</name>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="127"/>
        <source>Model:</source>
        <translation>مودېل:</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="128"/>
        <source>Status:</source>
        <translation>ئەھۋالى:</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="129"/>
        <source>Location:</source>
        <translation>ئورنى:</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="130"/>
        <source>Driver:</source>
        <translation>شوپۇر:</translation>
    </message>
</context>
<context>
    <name>PropertyWindow</name>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="20"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="59"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="122"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="245"/>
        <source>base</source>
        <translation>بازا</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="22"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="249"/>
        <source>advanced</source>
        <translation>ئىلغار</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="40"/>
        <source>PrinterProperty</source>
        <translation>پرىنتېرلاشProperty</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="75"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="130"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="275"/>
        <source>Name</source>
        <translation>ئىسىم-فامىلىسى</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="76"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="133"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="279"/>
        <source>Location</source>
        <translation>ئورنى</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="77"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="137"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="284"/>
        <source>Status</source>
        <translation>ھالەت</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="78"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="157"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="288"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="326"/>
        <source>Driver</source>
        <translation>شوپۇر</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="81"/>
        <source>Modify</source>
        <translation>ئۆزگەرتىش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="83"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="160"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="292"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="139"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="141"/>
        <source>Idle</source>
        <translation>بىكارچىلىق</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="143"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="145"/>
        <source>Printing</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="147"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="149"/>
        <source>Stopped</source>
        <translation>توختى</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="154"/>
        <source>Unknow</source>
        <translation>ئۇقۇشمىغانلار</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="197"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="342"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="358"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="369"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="197"/>
        <source>Error happened, all options restored!.</source>
        <translation>خاتالىق يۈز بەردى، بارلىق تاللاشلار ئەسلىگە كەلدى!.</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="199"/>
        <source>Confirm</source>
        <translation>جەزىملەشتۈرۈش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="341"/>
        <source>Printer Name Cannot Contains &apos;/\&apos;&quot;?#&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation>پرىنتېر نامى &apos;/\&apos;?#&apos;, 0 دىن ئارتۇق ھەرىپى، 128 ھەرپتىن كىچىك بولالمايدۇ !</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="344"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="360"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="371"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="357"/>
        <source>Exist Same Name Printer!</source>
        <translation>مەۋجۇتلۇق ئوخشاش ئىسىمدىكى پرىنتېرلاش ماشىنىسى!</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="368"/>
        <source>Printer Name Illegal!</source>
        <translation>پرىنتېرلاش نامى قانۇنسىز!</translation>
    </message>
</context>
<context>
    <name>ProperytItemWidget</name>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="214"/>
        <source>Idle</source>
        <translation>بىكارچىلىق</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="217"/>
        <source>Printing</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="220"/>
        <source>Stopped</source>
        <translation>توختى</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="223"/>
        <source>Unknow</source>
        <translation>ئۇقۇشمىغانلار</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ui/job_manager_window.h" line="30"/>
        <source>id</source>
        <translation>id</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="31"/>
        <source>user</source>
        <translation>ئىشلەتكۈچى</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="32"/>
        <source>title</source>
        <translation>تېما</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="33"/>
        <source>printer name</source>
        <translation>پرىنتېرلاش نامى</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="34"/>
        <source>size</source>
        <translation>چوڭ - كىچىكلىكى</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="35"/>
        <source>create time</source>
        <translation>ۋاقىت يارىتىش</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="36"/>
        <source>job state</source>
        <translation>خىزمەت ھالىتى</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="42"/>
        <source>Job is waiting to be printed</source>
        <translation>خىزمەت بىسىلىشنى كۈتمەكتە</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="43"/>
        <source>Job is held for printing</source>
        <translation>باسمىچىلىق خىزمىتى ئېلىپ بېرىلىدۇ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="44"/>
        <source>Job is currently printing</source>
        <translation>خىزمەت ھازىر باستۇرىۋاتىدۇ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="45"/>
        <source>Job has been stopped</source>
        <translation>خىزمەت توختىتىلدى</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="46"/>
        <source>Job has been canceled</source>
        <translation>خىزمەت ئەمەلدىن قالدۇرۇلدى</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="47"/>
        <source>Job has aborted due to error</source>
        <translation>خاتالىق سەۋەبىدىن خىزمەتتىن توختىدى</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="48"/>
        <source>Job has completed successfully</source>
        <translation>خىزمەت مۇۋەپپەقىيەتلىك تاماملاندى</translation>
    </message>
</context>
<context>
    <name>QuadBtnsTitleBar</name>
    <message>
        <location filename="../ui/main_win_ui/quad_btns_title_bar.cpp" line="16"/>
        <source>menu</source>
        <translation>تىزىملىك</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/quad_btns_title_bar.cpp" line="25"/>
        <source>minimize</source>
        <translation>كىچىكلىتىش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/quad_btns_title_bar.cpp" line="39"/>
        <source>full screen</source>
        <translation>پۈتۈن ئېكران</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/quad_btns_title_bar.cpp" line="53"/>
        <source>close</source>
        <translation>يېپىش</translation>
    </message>
</context>
<context>
    <name>RenamePrinterDialog</name>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="26"/>
        <source>RenamePrinterDialog</source>
        <translation>RenamePrinterDialog</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="148"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="167"/>
        <source>Ok</source>
        <translation>ماقۇل</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.cpp" line="11"/>
        <source>Printer Name</source>
        <translation>پرىنتېرلاش نامى</translation>
    </message>
</context>
<context>
    <name>SelectPpdDialog</name>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="26"/>
        <source>SelectPpdDialog</source>
        <translation>SelectPpdDialog</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="203"/>
        <source>Cancel</source>
        <translation>ئەمەلدىن قالدۇرۇش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="247"/>
        <source>Apply</source>
        <translation>ئىلتىماس قىلىش</translation>
    </message>
    <message>
        <source>Printer Driver</source>
        <translation type="vanished">پرىنتېرلاش شوپۇرى</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="22"/>
        <source>Select Driver</source>
        <translation>شوپۇر تاللاش</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="42"/>
        <source>driver</source>
        <translation>شوپۇر</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="43"/>
        <source>vendor</source>
        <translation>تىخنىكچى</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="182"/>
        <source>Reading Drivers，Please Wait...</source>
        <translation>كۆرۈش شوپۇرلار،كۈتۈڭ...</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="291"/>
        <source>Please Choose Model And Vender!</source>
        <translation>Model And Vender نى تاللاڭ!</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="292"/>
        <source>Warning</source>
        <translation>دىققەت</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="294"/>
        <source>Close</source>
        <translation>ياپ</translation>
    </message>
</context>
<context>
    <name>SystemNotification</name>
    <message>
        <location filename="../util/system_notification.cpp" line="21"/>
        <source>Printer Info</source>
        <translation>پرىنتېرلاش ئۇچۇرى</translation>
    </message>
    <message>
        <location filename="../util/system_notification.cpp" line="32"/>
        <source>Printer</source>
        <translation>پرىنتېرلاش</translation>
    </message>
</context>
<context>
    <name>TrayWin</name>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="33"/>
        <source>Minimize</source>
        <translation>كىچىكلىتىش</translation>
    </message>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="39"/>
        <source>Maximize</source>
        <translation>ئەڭ چوڭ چەككە</translation>
    </message>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="45"/>
        <source>Quit</source>
        <translation>چېكىنىش</translation>
    </message>
</context>
<context>
    <name>UkuiPrinterManager</name>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="336"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1414"/>
        <source>Idle</source>
        <translation>بىكارچىلىق</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="338"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1416"/>
        <source>Printing</source>
        <translation>بېسىپ چىقىرىش</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="340"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1418"/>
        <source>Stopped</source>
        <translation>توختى</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="360"/>
        <source>Printer Name Cannot Contains &apos;/\&apos;&quot;?#&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation>پرىنتېر نامى &apos;/\&apos;?#&apos;, 0 دىن ئارتۇق ھەرىپى، 128 ھەرپتىن كىچىك بولالمايدۇ !</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="377"/>
        <source>Exist Same Name Printer!</source>
        <translation>مەۋجۇتلۇق ئوخشاش ئىسىمدىكى پرىنتېرلاش ماشىنىسى!</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="380"/>
        <source>Printer name is not case sensitive!</source>
        <translation>پرىنتېرلاش نامى دىلرابا سەزگۈر ئەمەس!</translation>
    </message>
</context>
</TS>
