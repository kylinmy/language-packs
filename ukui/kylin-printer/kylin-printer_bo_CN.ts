<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name></name>
    <message>
        <location filename="../kylin-printer.desktop.in.h" line="1"/>
        <source>Printers</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../backend/data/kylin-printer-applet.desktop.in.h" line="1"/>
        <source>Printers-backend</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར་གྱི་རྒྱབ་རྟེན་</translation>
    </message>
</context>
<context>
    <name>AddPrinterWindow</name>
    <message>
        <location filename="../ui/add_printer_window.ui" line="26"/>
        <source>AddPrinterWindow</source>
        <translation>ཁ་སྣོན་བྱས་པའི་ཁ་སྣོན་བྱས་པའི་ཁ་སྣོན་བྱས་པའི་</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="117"/>
        <source>Add Printer</source>
        <translation>དཔར་ཆས་ཁ་སྣོན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="132"/>
        <source>Auto</source>
        <translation>རང་འགུལ་གྱིས་རླངས་</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="133"/>
        <location filename="../ui/add_printer_window.cpp" line="170"/>
        <source>Manual</source>
        <translation>ལག་དེབ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="145"/>
        <source>Device List</source>
        <translation>སྒྲིག་ཆས་ཀྱི་རེའུ་མིག</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="171"/>
        <source>Protocol</source>
        <translation>གྲོས་ཆོད།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="172"/>
        <source>Address</source>
        <translation>སྡོད་གནས།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="178"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="182"/>
        <source>socket</source>
        <translation>བསྒར་ཁུང་།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="183"/>
        <source>ipp</source>
        <translation>ipp</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="184"/>
        <source>http</source>
        <translation>http</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="190"/>
        <source>name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="191"/>
        <source>location</source>
        <translation>གནས་ཡུལ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="192"/>
        <source>driver</source>
        <translation>ཁ་ལོ་བ།</translation>
    </message>
    <message>
        <source>no PPD</source>
        <translation type="vanished">PPDམི་དགོས།PPD</translation>
    </message>
    <message>
        <source>Choose PPD</source>
        <translation type="vanished">PPDབདམས་པ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="197"/>
        <source>forward</source>
        <translation>མདུན་དུ་སྐྱོད་པ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="226"/>
        <location filename="../ui/add_printer_window.cpp" line="240"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="226"/>
        <source>Add printer failed: no PPD selected!</source>
        <translation>ཁ་སྣོན་བྱས་པའི་དཔར་ཆས་ལ་ཕམ་ཁ་བྱུང་བ་སྟེ། PPDབདམས་མེད་</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="240"/>
        <source>Add printer failed，please retry after a while.</source>
        <translation>དཔར་ཆས་ཁ་སྣོན་བྱས་ནས་ཕམ་སོང་། ཅུང་ཙམ་འགོར་རྗེས་ཡང་བསྐྱར་ཐེངས་གཅིག་ལ་བསྐྱར་དུ་ཞིབ་བཤེར་བྱེད</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="243"/>
        <location filename="../ui/add_printer_window.cpp" line="442"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="246"/>
        <location filename="../ui/add_printer_window.cpp" line="254"/>
        <location filename="../ui/add_printer_window.cpp" line="264"/>
        <source>Hint</source>
        <translation>གསལ་འདེབས་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="246"/>
        <source>Add printer successfully，printer a test page？</source>
        <translation>དཔར་ཆས་ལེགས་འགྲུབ་བྱུང་ན་དཔར་ཆས་ལ་ཚོད་ལྟའི་ཤོག་ལྷེ་ཞིག་ཡོད་དམ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="248"/>
        <source>Print test page</source>
        <translation>པར་སྐྲུན་ཚོད་ལྟའི་ཤོག་ལྷེ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="249"/>
        <source>Check Printer</source>
        <translation>ཞིབ་བཤེར་བྱེད་པའི་དཔར་ཆས།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="254"/>
        <source>Is the test page printed successfully?</source>
        <translation>ཚད་ལེན་ཚོད་ལྟའི་ཤོག་ལྷེ་དེ་ལེགས་འགྲུབ་བྱུང་ཡོད་དམ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="256"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="257"/>
        <source>No</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="264"/>
        <source>Test print failed.Do you want to change a printer driver?</source>
        <translation>ཚོད་ལྟའི་པར་སྐྲུན་ལ་ཕམ་ཉེས་བྱུང་། ཁྱོད་ཀྱིས་དཔར་ཆས་ཀྱི་ཁ་ལོ་བ་ཞིག་བརྗེ་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="266"/>
        <source>Change Driver</source>
        <translation>ཁ་ལོ་བ་བརྗེ་བ།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="267"/>
        <location filename="../ui/add_printer_window.cpp" line="443"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="290"/>
        <source>Searching...</source>
        <translation>འཚོལ་ཞིབ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="299"/>
        <location filename="../ui/add_printer_window.cpp" line="351"/>
        <source>Searching printers...</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར་འཚོལ་ཞིབ་བྱེད་པ...</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="331"/>
        <source>Can not find this Printer!</source>
        <translation>དཔར་ཆས་འདི་རྙེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Please select a deb package.</source>
        <translation type="vanished">ཁྱེད་ཀྱིས་བདེ་མོ་མཛོད་ཀྱི་ཁུག་མ་ཞིག་འདེམས་རོགས།</translation>
    </message>
    <message>
        <source>Deb File(*.deb)</source>
        <translation type="vanished">Deb File(*.deb)</translation>
    </message>
    <message>
        <source>Choose</source>
        <translation type="vanished">གདམ་གསེས་བྱེད་པ</translation>
    </message>
    <message>
        <source>Searching driver...</source>
        <translation type="vanished">འཚོལ་ཞིབ་ཁ་ལོ་བ་ ...</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="440"/>
        <location filename="../ui/add_printer_window.cpp" line="499"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <source>Install package failed，please retry.</source>
        <translation type="vanished">སྒྲིག་སྦྱོར་བྱས་ནས་ཕམ་ཉེས་བྱུང་ན་བསྐྱར་དུ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>No driver package selected,Continue?</source>
        <translation type="vanished">ཁ་ལོ་བའི་ཐུམ་སྒྲིལ་མ་བདམས་པར་མུ་མཐུད་དུ་རྒྱུན་འཁྱོངས་བྱེད</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="418"/>
        <source>Searching printer driver...</source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར་གྱི་ཁ་ལོ་བ་འཚོལ་ཞིབ</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="440"/>
        <source>Install driver package automatically failed,continue?</source>
        <translation>ཁ་ལོ་བའི་ཐུམ་སྒྲིལ་རང་འགུལ་གྱིས་ཕམ་ཉེས་བྱུང་ནས་མུ་མཐུད་དུ་རྒྱུན་འཁྱོངས</translation>
    </message>
    <message>
        <source>Printer Name Cannot Contains &apos;/\&apos;&quot;?#&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation type="vanished">པར་འདེབས་འཕྲུལ་འཁོར་གྱི་མིང་ནང་དུ་&quot;/\&quot;#&quot;དང་འཕྲིན་ཡིག་0ལྷག་ཙམ་ཡོད་པ་དེ་འཕྲིན་ཡིག་128ལས་ཉུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="501"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Exist Same Name Printer!</source>
        <translation type="vanished">མིང་གཅིག་མཚུངས་ཡིན་པའི་དཔར་ཆས་ཡོད་</translation>
    </message>
    <message>
        <source>Printer Name Illegal!</source>
        <translation type="vanished">པར་འདེབས་འཕྲུལ་འཁོར་གྱི་མིང་ཁྲིམས་དང་མི་མཐུན</translation>
    </message>
</context>
<context>
    <name>AutoSearchResultModel</name>
    <message>
        <location filename="../ui/add_printer_window.cpp" line="639"/>
        <source>network</source>
        <translation>དྲ་རྒྱ།</translation>
    </message>
</context>
<context>
    <name>BaseNotifyDialog</name>
    <message>
        <location filename="../ui/main_win_ui/base_notify_dialog.ui" line="35"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/base_notify_dialog.ui" line="124"/>
        <source>TextLabel</source>
        <translation>ཡི་གེ་ལ་པེར་གྱིས་བཤད་རྒྱུར</translation>
    </message>
</context>
<context>
    <name>ChoosePpdComboBox</name>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="15"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="22"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="137"/>
        <source>Choose PPD</source>
        <translation>PPDབདམས་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="16"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="25"/>
        <source>Choose from the PPD library</source>
        <translation>PPDདཔེ་མཛོད་ཁང་ནས་གདམ་གསེས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="17"/>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="29"/>
        <source>Add local PPD</source>
        <translation>ས་གནས་དེ་གའི་PPDབསྣན་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="48"/>
        <source>Please select a deb package.</source>
        <translation>ཁྱེད་ཀྱིས་བདེ་མོ་མཛོད་ཀྱི་ཁུག་མ་ཞིག་འདེམས་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="50"/>
        <source>Deb File(*.deb)</source>
        <translation>Deb File(*.deb)</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="51"/>
        <source>Choose</source>
        <translation>གདམ་གསེས་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="52"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="69"/>
        <source>Searching driver...</source>
        <translation>འཚོལ་ཞིབ་ཁ་ལོ་བ་ ...</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="91"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="91"/>
        <source>Install package failed，please retry.</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་ནས་ཕམ་ཉེས་བྱུང་ན་བསྐྱར་དུ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/choose_ppd_combo_box.cpp" line="92"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <source>No driver package selected,Continue?</source>
        <translation type="vanished">ཁ་ལོ་བའི་ཐུམ་སྒྲིལ་མ་བདམས་པར་མུ་མཐུད་དུ་རྒྱུན་འཁྱོངས་བྱེད</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation type="vanished">གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>ConfigIPAddressDialog</name>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.ui" line="247"/>
        <source>初始地址:api.kylinos.cn</source>
        <translation>ཁ་ན་api.kylinos.cn</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="22"/>
        <source>Configure</source>
        <translation>བཀོད་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="23"/>
        <source>Service address:</source>
        <translation>ཞབས་ཞུའི་ས་གནས་ནི།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="24"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="25"/>
        <source>Ok</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="37"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="37"/>
        <source>Config init failed</source>
        <translation>ཁུང་ཙིའི་ནང་དུ་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/config_ip_address_dialog.cpp" line="40"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>CupsDebugLoggingCheckbox</name>
    <message>
        <location filename="../ui/cups_debug_logging_checkbox.cpp" line="8"/>
        <source>Retain debug info for troubleshooting</source>
        <translation>གྲལ་བཤེར་བྱེད་པར་ཚོད་ལྟ་བྱེད་པའི་ཆ་འཕྲིན་སོར་འཇོག་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>DebInstallWindow</name>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="32"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="75"/>
        <source>Driver</source>
        <translation>ཁ་ལོ་བ།</translation>
    </message>
    <message>
        <source>Choose</source>
        <translation type="vanished">གདམ་གསེས་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="143"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Foward</source>
        <translation type="vanished">གླེན་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="23"/>
        <source>Modify Printer Driver</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར་གྱི་ཁ་ལོ་བར་བཟོ་བཅོས་རྒྱག</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="54"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.cpp" line="54"/>
        <source>Modify PPD failed: no PPD selected!</source>
        <translation>PPDལ་བཟོ་བཅོས་བརྒྱབ་ནས་ཕམ་ཁ་བྱུང་བ་སྟེ། PPDབདམས་ཐོན་མ་བྱུང་།</translation>
    </message>
    <message>
        <source>Choose A Deb, Or Forward Directly!</source>
        <translation type="vanished">བུ་ལོན་ཞིག་བདམས་པའམ་ཡང་ན་ཐད་ཀར་མདུན་དུ་བསྐྱོད་དགོས།</translation>
    </message>
    <message>
        <source>Please select a deb package.</source>
        <translation type="vanished">ཁྱེད་ཀྱིས་བདེ་མོ་མཛོད་ཀྱི་ཁུག་མ་ཞིག་འདེམས་རོགས།</translation>
    </message>
    <message>
        <source>Deb File(*.deb)</source>
        <translation type="vanished">Deb File(*.deb)</translation>
    </message>
    <message>
        <source>Searching driver...</source>
        <translation type="vanished">འཚོལ་ཞིབ་ཁ་ལོ་བ་ ...</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="vanished">ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <source>Install package failed，please retry.</source>
        <translation type="vanished">སྒྲིག་སྦྱོར་བྱས་ནས་ཕམ་ཉེས་བྱུང་ན་བསྐྱར་དུ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/deb_install_window.ui" line="162"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>No driver package selected,Continue?</source>
        <translation type="vanished">ཁ་ལོ་བའི་ཐུམ་སྒྲིལ་མ་བདམས་པར་མུ་མཐུད་དུ་རྒྱུན་འཁྱོངས་བྱེད</translation>
    </message>
</context>
<context>
    <name>DeviceListButton</name>
    <message>
        <location filename="../ui/device_list_button.cpp" line="34"/>
        <source>Idle</source>
        <translation>སྒྱིད་ལུག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="37"/>
        <source>Printing</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས།</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="40"/>
        <source>Stopped</source>
        <translation>བཀག་འགོག་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="43"/>
        <source>Unknow</source>
        <translation>ཤེས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="64"/>
        <location filename="../ui/device_list_button.cpp" line="281"/>
        <location filename="../ui/device_list_button.cpp" line="313"/>
        <source>Default</source>
        <translation>ཁ་ཆད་དང་འགལ་</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="129"/>
        <location filename="../ui/device_list_button.cpp" line="159"/>
        <source>Set Default</source>
        <translation>ཁ་ཆད་དང་འགལ་བའི་གནས</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="131"/>
        <location filename="../ui/device_list_button.cpp" line="162"/>
        <source>Enabled</source>
        <translation>ནུས་པ་ཐོན་པ།</translation>
    </message>
    <message>
        <location filename="../ui/device_list_button.cpp" line="135"/>
        <location filename="../ui/device_list_button.cpp" line="165"/>
        <source>Shared</source>
        <translation>ཐུན་མོང་དུ་ལོངས་</translation>
    </message>
</context>
<context>
    <name>DeviceMap</name>
    <message>
        <location filename="../device_manager/device_map.cpp" line="290"/>
        <source>Printer</source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../device_manager/device_map.cpp" line="292"/>
        <source>Printers</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../device_manager/device_map.cpp" line="294"/>
        <source>plug-in:</source>
        <translation>plug-in:</translation>
    </message>
    <message>
        <location filename="../device_manager/device_map.cpp" line="296"/>
        <source>unplugged:</source>
        <translation>ཁ་གསལ་མེད་པ་སྟེ།</translation>
    </message>
</context>
<context>
    <name>EmptyWidget</name>
    <message>
        <location filename="../ui/main_win_ui/right_widget.cpp" line="101"/>
        <source>Please click &quot;+&quot; button to add a printer.</source>
        <translation>&quot;+&quot;མཐེབ་གཅུས་བརྒྱབ་ནས་དཔར་ཆས་ཤིག་ཁ་སྣོན་བྱེད་རོགས།</translation>
    </message>
</context>
<context>
    <name>EventNotifyMonitor</name>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="430"/>
        <location filename="../backend/event_notify_monitor.cpp" line="482"/>
        <source>Printer </source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར། </translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="454"/>
        <location filename="../backend/event_notify_monitor.cpp" line="469"/>
        <source>Job:</source>
        <translation>ལས་ཀ་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="456"/>
        <source>created！</source>
        <translation>གསར་སྐྲུན་བྱས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../backend/event_notify_monitor.cpp" line="471"/>
        <source>completed！</source>
        <translation>ལེགས་གྲུབ་བྱུང་བ་རེད།</translation>
    </message>
</context>
<context>
    <name>JobManagerModel</name>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="25"/>
        <source>Test Page</source>
        <translation>ཚོད་ལྟའི་ཤོག་ལྷེ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="28"/>
        <source>untitled</source>
        <translation>ཡིག་རྨོངས་སེལ་བ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="39"/>
        <source>unknown</source>
        <translation>ཤེས་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>JobManagerWindow</name>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="237"/>
        <source>Printer</source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="381"/>
        <source>Cancel print</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས་མེད་པར</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="384"/>
        <source>Delete print</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="387"/>
        <source>Hold print</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས་ཉར་འཛིན</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="390"/>
        <source>Release print</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས་འགྲེམས་སྤེལ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="393"/>
        <source>Repaint</source>
        <translation>བསྐྱར་དུ་བྱུགས་པ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="409"/>
        <location filename="../ui/job_manager_window.cpp" line="415"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="409"/>
        <source>Set error: Job status has been updated!</source>
        <translation>ནོར་འཁྲུལ་བྱུང་བ་སྟེ། ལས་གནས་ཀྱི་གནས་ཚུལ་གསར་སྒྱུར་བྱས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="411"/>
        <location filename="../ui/job_manager_window.cpp" line="417"/>
        <source>Sure</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="415"/>
        <source>Cannot move job to itself!</source>
        <translation>ལས་ཀ་རང་ཉིད་ལ་སྤོ་སྒྱུར་བྱེད་མི་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>JobMenu</name>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="78"/>
        <source>Cancel print</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས་མེད་པར</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="80"/>
        <source>Delete print</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="82"/>
        <source>Hold print</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས་ཉར་འཛིན</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="84"/>
        <source>Release print</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས་འགྲེམས་སྤེལ</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="86"/>
        <source>Repaint</source>
        <translation>བསྐྱར་དུ་བྱུགས་པ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.cpp" line="88"/>
        <source>Use other printer...</source>
        <translation>དཔར་ཆས་གཞན་དག་བཀོལ་སྤྱོད་བྱེད་དགོས།</translation>
    </message>
</context>
<context>
    <name>LeftWidget</name>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="20"/>
        <source>Printer</source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="57"/>
        <source>Device List</source>
        <translation>སྒྲིག་ཆས་ཀྱི་རེའུ་མིག</translation>
    </message>
    <message>
        <source>Are you sure to delete </source>
        <translation type="vanished">ཁྱོད་ཀྱིས་ངེས་པར་དུ་བསུབ་དགོས་པ་ཡིན་ན། </translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="365"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <source>This action will delete the job queue too!</source>
        <translation type="vanished">བྱ་སྤྱོད་འདིས་ཀྱང་ལས་གནས་ཀྱི་གྲལ་བསྒྲིགས་ནས་བསུབ་རྒྱུ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="214"/>
        <source>Are you sure to delete &quot;%1&quot;?</source>
        <translation>ཁྱོད་ཀྱིས་ངེས་པར་དུ་&quot;%1&quot;བསུབ་དགོས་སམ།</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">རེད།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="218"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="219"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="363"/>
        <source>Set Default Failed!</source>
        <translation>སྔོན་ལ་ཕམ་ཁ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/left_widget.cpp" line="367"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>MainWinPropertyWidget</name>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.ui" line="26"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="43"/>
        <source>Printer</source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="72"/>
        <source>Property</source>
        <translation>རྒྱུ་ནོར།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="79"/>
        <source>Job List</source>
        <translation>ལས་གནས་ཀྱི་མིང་ཐོ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="86"/>
        <source>PrintTest</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="183"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="185"/>
        <source>Idle</source>
        <translation>སྒྱིད་ལུག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="187"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="189"/>
        <source>Printing</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="191"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="193"/>
        <source>Stopped</source>
        <translation>བཀག་འགོག་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="198"/>
        <source>Unknown</source>
        <translation>ཤེས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="223"/>
        <source>Printer Name Cannot Be Null!</source>
        <translation>དཔར་ཆས་ཀྱི་མིང་ནི་གོ་མི་སྲིད་པ་ཞིག་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="224"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="234"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="252"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="261"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="280"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="226"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="236"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="254"/>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="263"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="233"/>
        <source>Printer Name Cannot Contains &apos;/\&apos;&quot;?#&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར་གྱི་མིང་ནང་དུ་&quot;/\&quot;#&quot;དང་འཕྲིན་ཡིག་0ལྷག་ཙམ་ཡོད་པ་དེ་འཕྲིན་ཡིག་128ལས་ཉུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="251"/>
        <source>Exist Same Name Printer!</source>
        <translation>མིང་གཅིག་མཚུངས་ཡིན་པའི་དཔར་ཆས་ཡོད་</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="260"/>
        <source>Printer Name Illegal!</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར་གྱི་མིང་ཁྲིམས་དང་མི་མཐུན</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="278"/>
        <source>Are you sure to rename </source>
        <translation>ཁྱོད་ཀྱིས་ངེས་པར་དུ་མིང་བསྒྱུར་དགོས་པ་ཡིན་ན </translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="281"/>
        <source>This action will delete the job queue too!</source>
        <translation>བྱ་སྤྱོད་འདིས་ཀྱང་ལས་གནས་ཀྱི་གྲལ་བསྒྲིགས་ནས་བསུབ་རྒྱུ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="283"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.cpp" line="92"/>
        <source>Printer</source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="218"/>
        <source>Delete Failed!</source>
        <translation>ཕམ་ཁ་བྱུང་བ་དེ་བསུབ་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="220"/>
        <location filename="../ui/mainwindow.cpp" line="259"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="222"/>
        <location filename="../ui/mainwindow.cpp" line="261"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="259"/>
        <source>Add printer failed，please retry after a while.</source>
        <translation>དཔར་ཆས་ཁ་སྣོན་བྱས་ནས་ཕམ་སོང་། ཅུང་ཙམ་འགོར་རྗེས་ཡང་བསྐྱར་ཐེངས་གཅིག་ལ་བསྐྱར་དུ་ཞིབ་བཤེར་བྱེད</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="319"/>
        <source>Try to connect the Printer...</source>
        <translation>ཐབས་བརྒྱ་ཇུས་སྟོང་གིས་དཔར་ཆས་སྦྲེལ་མཐུད་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="324"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="324"/>
        <source>Open Printer Failed,Please Del And Add Printer, Then Try Again!</source>
        <translation>ཁ་ཕྱེས་པའི་དཔར་ཆས་ལ་སྐྱོན་ཤོར་བ་དང་། ཏེ་ཨར་དང་དཔར་ཆས་བསྣན་རྗེས་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="326"/>
        <source>Sure</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>MenuModule</name>
    <message>
        <location filename="../ui/menumodule.cpp" line="56"/>
        <location filename="../ui/menumodule.cpp" line="97"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="58"/>
        <location filename="../ui/menumodule.cpp" line="94"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="60"/>
        <location filename="../ui/menumodule.cpp" line="100"/>
        <source>Configure</source>
        <translation>བཀོད་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/menumodule.cpp" line="62"/>
        <location filename="../ui/menumodule.cpp" line="90"/>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན་བྱ་རྒྱུ།</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">པར་གཞི་འདི་ལྟ་སྟེ། </translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation type="vanished">ཞབས་ཞུ་ &amp; Support: </translation>
    </message>
    <message>
        <location filename="../ui/menumodule.h" line="58"/>
        <source>Printer</source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
</context>
<context>
    <name>NewPopWindow</name>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.ui" line="32"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="40"/>
        <source>Printer</source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="59"/>
        <source>Print Test Page</source>
        <translation>པར་སྐྲུན་ཚོད་ལྟའི་ཤོག་ལྷེ།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="60"/>
        <source>View Device</source>
        <translation>ལྟ་ཀློག་སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="61"/>
        <source>Manual Install</source>
        <translation>ལག་ཤེས་སྒྲིག་སྦྱོར།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="128"/>
        <source>Installing......</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པ......</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="143"/>
        <source>Successful installation!</source>
        <translation>སྒྲིག་སྦྱོར་ལེགས་འགྲུབ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="164"/>
        <source>Installation failed!</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་ཀྱང་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="188"/>
        <source>Printer Detected:</source>
        <translation>ཞིབ་དཔྱད་ཚད་ལེན་བྱས་པའི་པར་འདེབས་འཕྲུལ་འཁོར</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="205"/>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="212"/>
        <source>Hint</source>
        <translation>གསལ་འདེབས་བྱེད་པ</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="205"/>
        <source>Is the test page printed successfully?</source>
        <translation>ཚད་ལེན་ཚོད་ལྟའི་ཤོག་ལྷེ་དེ་ལེགས་འགྲུབ་བྱུང་ཡོད་དམ།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="206"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="207"/>
        <source>No</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="212"/>
        <source>Test print failed.Do you want to change a printer driver?</source>
        <translation>ཚོད་ལྟའི་པར་སྐྲུན་ལ་ཕམ་ཉེས་བྱུང་། ཁྱོད་ཀྱིས་དཔར་ཆས་ཀྱི་ཁ་ལོ་བ་ཞིག་བརྗེ་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="213"/>
        <source>Change Driver</source>
        <translation>ཁ་ལོ་བ་བརྗེ་བ།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="214"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="226"/>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="226"/>
        <source>Failed to start, please try to add the printer again!</source>
        <translation>འགོ་བརྩམས་མ་ཐུབ་ན་ ཡང་བསྐྱར་དཔར་ཆས་ཁ་སྣོན་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/new_pop_window/new_pop_window.cpp" line="229"/>
        <source>Ok</source>
        <translation>འགྲིགས།</translation>
    </message>
</context>
<context>
    <name>PropertyListWindow</name>
    <message>
        <location filename="../ui/new_property_window/property_list_window.ui" line="26"/>
        <source>PropertyListWindow</source>
        <translation>རྒྱུ་ནོར་གྱི་མིང་ཐོའི་རླུང་ཕྱོགས་</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="9"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_list_window.cpp" line="10"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>PropertyManagerModel</name>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="127"/>
        <source>Model:</source>
        <translation>དཔེ་དབྱིབས་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="128"/>
        <source>Status:</source>
        <translation>གོ་གནས་ནི།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="129"/>
        <source>Location:</source>
        <translation>གནས་ཡུལ་ནི།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/main_win_property_widget.cpp" line="130"/>
        <source>Driver:</source>
        <translation>ཁ་ལོ་བ་ནི།</translation>
    </message>
</context>
<context>
    <name>PropertyWindow</name>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="20"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="59"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="122"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="245"/>
        <source>base</source>
        <translation>རྟེན་གཞི།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="22"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="249"/>
        <source>advanced</source>
        <translation>སྔོན་ཐོན་རང་བཞིན།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="40"/>
        <source>PrinterProperty</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར་གྱི་ཁ་ལོ་བ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="75"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="130"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="275"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="76"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="133"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="279"/>
        <source>Location</source>
        <translation>གནས་ཡུལ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="77"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="137"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="284"/>
        <source>Status</source>
        <translation>གནས་ཚུལ་གྱི་གནས་</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="78"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="157"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="288"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="326"/>
        <source>Driver</source>
        <translation>ཁ་ལོ་བ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="81"/>
        <source>Modify</source>
        <translation>བཟོ་བཅོས་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="83"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="160"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="292"/>
        <source>URI</source>
        <translation>URI</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="139"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="141"/>
        <source>Idle</source>
        <translation>སྒྱིད་ལུག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="143"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="145"/>
        <source>Printing</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="147"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="149"/>
        <source>Stopped</source>
        <translation>བཀག་འགོག་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="154"/>
        <source>Unknow</source>
        <translation>ཤེས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="197"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="342"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="358"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="369"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="197"/>
        <source>Error happened, all options restored!.</source>
        <translation>ནོར་འཁྲུལ་བྱུང་བས་བསལ་འདེམས་ཚང་མ་སླར་གསོ་བྱས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="199"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="341"/>
        <source>Printer Name Cannot Contains &apos;/\&apos;&quot;?#&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར་གྱི་མིང་ནང་དུ་&quot;/\&quot;#&quot;དང་འཕྲིན་ཡིག་0ལྷག་ཙམ་ཡོད་པ་དེ་འཕྲིན་ཡིག་128ལས་ཉུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="344"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="360"/>
        <location filename="../ui/main_win_ui/property_window.cpp" line="371"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="357"/>
        <source>Exist Same Name Printer!</source>
        <translation>མིང་གཅིག་མཚུངས་ཡིན་པའི་དཔར་ཆས་ཡོད་</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/property_window.cpp" line="368"/>
        <source>Printer Name Illegal!</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར་གྱི་མིང་ཁྲིམས་དང་མི་མཐུན</translation>
    </message>
</context>
<context>
    <name>ProperytItemWidget</name>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="214"/>
        <source>Idle</source>
        <translation>སྒྱིད་ལུག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="217"/>
        <source>Printing</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས།</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="220"/>
        <source>Stopped</source>
        <translation>བཀག་འགོག་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../ui/new_property_window/property_itemwidget.cpp" line="223"/>
        <source>Unknow</source>
        <translation>ཤེས་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../ui/job_manager_window.h" line="30"/>
        <source>id</source>
        <translation>ཐོབ་ཐང་ལག་ཁྱེར།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="31"/>
        <source>user</source>
        <translation>སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="32"/>
        <source>title</source>
        <translation>ཁ་བྱང་།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="33"/>
        <source>printer name</source>
        <translation>དཔར་ཆས་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="34"/>
        <source>size</source>
        <translation>ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="35"/>
        <source>create time</source>
        <translation>དུས་ཚོད་གསར་སྐྲུན་བྱེད་</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="36"/>
        <source>job state</source>
        <translation>ལས་གནས་ཀྱི་གནས་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="42"/>
        <source>Job is waiting to be printed</source>
        <translation>ལས་ཀ་ནི་པར་སྐྲུན་བྱེད་པར་སྒུག་ཡོད།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="43"/>
        <source>Job is held for printing</source>
        <translation>པར་སྐྲུན་ལས་རིགས་ཀྱི་ལས་འགན་འཁུར་རྒྱུ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="44"/>
        <source>Job is currently printing</source>
        <translation>མིག་སྔར་པར་སྐྲུན་བྱེད་བཞིན་པའི་ལས་གནས།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="45"/>
        <source>Job has been stopped</source>
        <translation>ལས་ཀ་བྱེད་མཚམས་བཞག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="46"/>
        <source>Job has been canceled</source>
        <translation>ལས་ཀ་མེད་པར་བཟོས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="47"/>
        <source>Job has aborted due to error</source>
        <translation>ནོར་འཁྲུལ་བྱུང་བའི་རྐྱེན་གྱིས་ལས་ཀ་ཤོར་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../ui/job_manager_window.h" line="48"/>
        <source>Job has completed successfully</source>
        <translation>ལས་ཀ་བདེ་བླག་ངང་ལེགས་གྲུབ་བྱུང་ཡོད།</translation>
    </message>
</context>
<context>
    <name>QuadBtnsTitleBar</name>
    <message>
        <location filename="../ui/main_win_ui/quad_btns_title_bar.cpp" line="16"/>
        <source>menu</source>
        <translation>ཟས་ཐོ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/quad_btns_title_bar.cpp" line="25"/>
        <source>minimize</source>
        <translation>ཉུང་དུ་གཏོང་གང་ཐུབ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/quad_btns_title_bar.cpp" line="39"/>
        <source>full screen</source>
        <translation>བརྙན་ཤེལ་ཆ་ཚང་།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/quad_btns_title_bar.cpp" line="53"/>
        <source>close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>RenamePrinterDialog</name>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="26"/>
        <source>RenamePrinterDialog</source>
        <translation>མིང་ལ་པར་རྒྱག་མཁན་གྱི་ནད་རྟགས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="148"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.ui" line="167"/>
        <source>Ok</source>
        <translation>འགྲིགས།</translation>
    </message>
    <message>
        <location filename="../ui/rename_printer_dialog.cpp" line="11"/>
        <source>Printer Name</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར་གྱི་མིང་།</translation>
    </message>
</context>
<context>
    <name>SelectPpdDialog</name>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="26"/>
        <source>SelectPpdDialog</source>
        <translation>PppdDialogབདམས་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="203"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.ui" line="247"/>
        <source>Apply</source>
        <translation>རེ་ཞུ་</translation>
    </message>
    <message>
        <source>Printer Driver</source>
        <translation type="vanished">དཔར་འདེབས་འཕྲུལ་འཁོར་གྱི་ཁ་ལོ</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="22"/>
        <source>Select Driver</source>
        <translation>ཁ་ལོ་བ་གདམ་གསེས་</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="42"/>
        <source>driver</source>
        <translation>ཁ་ལོ་བ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="43"/>
        <source>vendor</source>
        <translation>ཚོང་པ་ཉི་ཚེ་</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="182"/>
        <source>Reading Drivers，Please Wait...</source>
        <translation>དཔེ་ཆ་ཀློག་པའི་ཁ་ལོ་བ་རྣམས་ཀྱིས་རེ་སྒུག་</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="291"/>
        <source>Please Choose Model And Vender!</source>
        <translation>ཁྱོད་ཀྱིས་དཔེ་དབྱིབས་དང་འཁོན་ལན་བས་ཁྱོད་ལ་གདམ་ག་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="292"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/main_win_ui/select_ppd_dialog.cpp" line="294"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>SystemNotification</name>
    <message>
        <location filename="../util/system_notification.cpp" line="21"/>
        <source>Printer Info</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར་གྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../util/system_notification.cpp" line="32"/>
        <source>Printer</source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
</context>
<context>
    <name>TrayWin</name>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="33"/>
        <source>Minimize</source>
        <translation>ཉུང་དུ་གཏོང་གང་ཐུབ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="39"/>
        <source>Maximize</source>
        <translation>ཚད་གཞི་མཐོ་ཤོས་ཀྱི་སྒོ་ནས</translation>
    </message>
    <message>
        <location filename="../backend/ui/tray_win.cpp" line="45"/>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན་བྱ་རྒྱུ།</translation>
    </message>
</context>
<context>
    <name>UkuiPrinterManager</name>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="336"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1414"/>
        <source>Idle</source>
        <translation>སྒྱིད་ལུག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="338"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1416"/>
        <source>Printing</source>
        <translation>པར་སྐྲུན་ཡིག་རིགས།</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="340"/>
        <location filename="../printer_manager/ukui_printer.cpp" line="1418"/>
        <source>Stopped</source>
        <translation>བཀག་འགོག་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="360"/>
        <source>Printer Name Cannot Contains &apos;/\&apos;&quot;?#&apos;, And More Than 0 Letter, Less Than 128 Letters !</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར་གྱི་མིང་ནང་དུ་&quot;/\&quot;#&quot;དང་འཕྲིན་ཡིག་0ལྷག་ཙམ་ཡོད་པ་དེ་འཕྲིན་ཡིག་128ལས་ཉུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="377"/>
        <source>Exist Same Name Printer!</source>
        <translation>མིང་གཅིག་མཚུངས་ཡིན་པའི་དཔར་ཆས་ཡོད་</translation>
    </message>
    <message>
        <location filename="../printer_manager/ukui_printer.cpp" line="380"/>
        <source>Printer name is not case sensitive!</source>
        <translation>དཔར་ཆས་ཀྱི་མིང་ལ་དཔེ་མཚོན་གྱི་ཚོར་བ་རྣོན་པོ་མེད།</translation>
    </message>
</context>
</TS>
