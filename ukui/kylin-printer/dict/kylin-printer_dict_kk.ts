<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>DictionaryForChinesePrinter</name>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="7"/>
        <source>letter</source>
        <translation>хат</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="8"/>
        <source>Page Size</source>
        <translation>Бет өлшемі</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="9"/>
        <source>Resolution</source>
        <translation>Резолюция</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="10"/>
        <source>Paper Source</source>
        <translation>Қағаз көзі</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="11"/>
        <source>Source</source>
        <translation>Дереккөз</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="12"/>
        <source>PageType</source>
        <translation>Беттипі</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="13"/>
        <source>Media Type</source>
        <translation>Мультимедиа түрі</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="14"/>
        <source>Color</source>
        <translation>Түс</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="15"/>
        <source>Direction</source>
        <translation>Бағыттау</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="16"/>
        <source>Duplex</source>
        <translation>Дуплекс</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="17"/>
        <source>Pages per Side</source>
        <translation>Әр жағына арналған беттер</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="18"/>
        <source>Ink Rank</source>
        <translation>Сия атағы</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="19"/>
        <source>Output Order</source>
        <translation>Шығыс реті</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="20"/>
        <source>legal</source>
        <translation>заңды</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="21"/>
        <source>statement</source>
        <translation>өтiнiш</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="22"/>
        <source>executive</source>
        <translation>атқарушы</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="23"/>
        <source>monarch</source>
        <translation>монарх</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="24"/>
        <source>com10</source>
        <translation>com10</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="25"/>
        <source>auto</source>
        <translation>авто</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="26"/>
        <source>multi-purpose tray</source>
        <translation>көп мақсатты науа</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="27"/>
        <source>drawer 1</source>
        <translation>1 жәшік</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="28"/>
        <source>drawer 2</source>
        <translation>2 жәшік</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="29"/>
        <source>plain paper</source>
        <translation>кәдімгі қағаз</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="30"/>
        <source>left</source>
        <translation>сол жақта</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="31"/>
        <source>right</source>
        <translation>оң жақта</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="32"/>
        <source>top</source>
        <translation>жоғарғы жағы</translation>
    </message>
    <message>
        <location filename="../dictionary/dictionary_for_chinese_printer.cpp" line="33"/>
        <source>bindingedge</source>
        <translation>байланыстыру</translation>
    </message>
</context>
</TS>
