<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>ChatMsg</name>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="137"/>
        <source>Resend</source>
        <translation>བསྐྱར་དུ་བསྐུར་བ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="138"/>
        <source>Copy</source>
        <translation>འདྲ་བཤུས་</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="139"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="140"/>
        <source>Open Directory</source>
        <translation>སྒོ་འབྱེད་དཀར་ཆག</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="141"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="954"/>
        <source>Save As</source>
        <translation>གཞན་དུ་ཉར་བ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="142"/>
        <source>Delete</source>
        <translation>ཟིན་ཐོ་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="143"/>
        <source>Clear All</source>
        <translation>ཁ་བརྡའི་ཟིན་ཐོ་གཙང་སེལ་བྱས།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="216"/>
        <source>File</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="225"/>
        <source>Folder</source>
        <translation>ཡིག་སྣོད།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="234"/>
        <source>Screen Shot</source>
        <translation>པར་དྲས་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="243"/>
        <source>History Message</source>
        <translation>ལོ་རྒྱུས་ཀྱི་ཟིན་ཐོ།།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="260"/>
        <source>Send</source>
        <translation>སྐུར་བ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="498"/>
        <source>Message send failed, please check whether IP connection is successful!</source>
        <translation>གནས་ཚུལ་བསྐུར་བ་ཕམ་སོང་།IPའབྲེལ་མཐུད་ལེགས་འགྲུབ་བྱུང་ཡོད་མེད་ལ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="611"/>
        <source>Can not write file</source>
        <translation>ཡིག་ཆ་འབྲི་མི་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="808"/>
        <source>No such file or directory!</source>
        <translation>འདི་ལྟ་བུའི་ཡིག་ཆ་དང་ཡིག་སྣོད་མེད།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="974"/>
        <source>Delete the currently selected message?</source>
        <translation>མིག་སྔར་བདམས་ཟིན་པའི་ཆ་འཕྲིན་བསུབ་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="977"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1005"/>
        <source>No</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1054"/>
        <source>folder</source>
        <translation>ཡིག་སྣོད།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="976"/>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1004"/>
        <source>Yes</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1002"/>
        <source>Clear all current messages?</source>
        <translation>མིག་སྔའི་ཆ་འཕྲིན་ཚང་མ་གཙང་སེལ་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1024"/>
        <source>Send Files</source>
        <translation>ཡིག་ཆ་བསྐུར་བ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatmsg/chatmsg.cpp" line="1051"/>
        <source>Send Folders</source>
        <translation>ཡིག་སྣོད་སྐུར་སྐྱེལ་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>ChatSearch</name>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="289"/>
        <source>Delete</source>
        <translation>ཟིན་ཐོ་བསུབ་པ།</translation>
    </message>
    <message>
        <source>Clear All</source>
        <translation type="obsolete">清空聊天记录</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="175"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="187"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="317"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="369"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="477"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="198"/>
        <source>All</source>
        <translation>ཚང་མ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="205"/>
        <source>File</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="210"/>
        <source>Image/Video</source>
        <translation>པར་རིས་དང་བརྙན་ཕབ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="218"/>
        <source>Link</source>
        <translation>སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="224"/>
        <source>canael</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="229"/>
        <source>sure</source>
        <translation>བདམས་ཟིན་པ་དག་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="239"/>
        <source>DeleteMenu</source>
        <translation>འདེམས་པང་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="131"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="250"/>
        <source>Batch delete</source>
        <translation>སྡེབ་འབོར་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="47"/>
        <source>Chat content</source>
        <translation>ཁ་བརྡའི་ནང་དོན།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="132"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="252"/>
        <source>Clear all messages</source>
        <translation>ཆ་འཕྲིན་ཡོད་ཚད་གཙང་སེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="158"/>
        <source>Chat Content</source>
        <translation>ཁ་བརྡའི་ནང་དོན།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="290"/>
        <source>Choose Delete</source>
        <translation>བདམས་ནས་ཟིན་ཐོ་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="291"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="292"/>
        <source>Open Directory</source>
        <translation>སྒོ་འབྱེད་དཀར་ཆག</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="696"/>
        <source>No such file or directory!</source>
        <translation>ཡིག་ཆ་འདི་དང་ཡིག་ཁུག་འདི་མི་འདུག</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="757"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="967"/>
        <source>Delete the currently selected message?</source>
        <translation>མིག་སྔར་བདམས་ཟིན་པའི་ཆ་འཕྲིན་བསུབ་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="760"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="798"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="970"/>
        <source>No</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="759"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="797"/>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="969"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location filename="../../src/view/chatsearch/chat_search.cpp" line="795"/>
        <source>Clear all current messages?</source>
        <translation>མིག་སྔའི་ཆ་འཕྲིན་ཚང་མ་གཙང་སེལ་བྱེད་དགོས་སམ།</translation>
    </message>
</context>
<context>
    <name>Control</name>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <location filename="../../src/controller/control.cpp" line="284"/>
        <source>Anonymous</source>
        <translation>མིང་བཀབ་པ།</translation>
    </message>
</context>
<context>
    <name>FriendInfoWid</name>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="150"/>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="177"/>
        <source>Messages</source>
        <translation>ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="186"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="215"/>
        <source>Username</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="224"/>
        <source>IP Address</source>
        <translation>IPས་གནས།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="234"/>
        <source>Nickname</source>
        <translation>མཚང་མིང་།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendinfowid.cpp" line="332"/>
        <source>Add</source>
        <translation>མིང་ཁ་སྣོན་བརྒྱབ་པ།</translation>
    </message>
</context>
<context>
    <name>FriendListView</name>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="96"/>
        <source>Start Chat</source>
        <translation>གླེང་མོལ་བྱེད་འགོ་ཚུགས།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="97"/>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="150"/>
        <source>Set to Top</source>
        <translation>རྩེ་མོར་གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="98"/>
        <source>Change Nickname</source>
        <translation>གྲོགས་པོའི་ཟུར་མཆན་བཟོ་བཅོས་བྱས།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="99"/>
        <source>View Info</source>
        <translation>ལྟ་ཀློག་གི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="100"/>
        <source>Delete Friend</source>
        <translation>གྲོགས་པོ་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/friendlist.cpp" line="153"/>
        <source>Cancel the Top</source>
        <translation>གོང་རིམ་མེད་པར་བཟོ་བ།</translation>
    </message>
</context>
<context>
    <name>KyView</name>
    <message>
        <location filename="../../src/view/kyview.cpp" line="133"/>
        <source>Messages</source>
        <translation>ཆ་འཕྲིན།</translation>
    </message>
</context>
<context>
    <name>LocalInfo</name>
    <message>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="174"/>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="187"/>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="262"/>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="349"/>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="378"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="160"/>
        <source>Modify Name</source>
        <translation>མིང་ལ་བཟོ་བཅོས་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localinfo.cpp" line="169"/>
        <source>Open Directory</source>
        <translation>སྒོ་འབྱེད་དཀར་ཆག</translation>
    </message>
</context>
<context>
    <name>LocalUpdateName</name>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="157"/>
        <source>Set Username</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་སྒྲིག་བཀོལ།</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="158"/>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="209"/>
        <source>Please enter username</source>
        <translation>ཁྱེད་ཀྱིས་སྤྱོད་མཁན་གྱི་མིང་ནང་འཇུག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="161"/>
        <source>Change nickname</source>
        <translation>མཚང་མིང་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="162"/>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="211"/>
        <source>Please enter friend nickname</source>
        <translation>གྲོགས་པོའི་མཚང་མིང་ནང་དུ་འཇུག་རོགས།</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="165"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="170"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="177"/>
        <source>Skip</source>
        <translation>མཆོངས་བ།</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="214"/>
        <source>The length of user name is less than 20 words</source>
        <translation>The length of user name is less than 20 words</translation>
    </message>
    <message>
        <location filename="../../src/view/localinfo/localupdatename.cpp" line="216"/>
        <source>Please do not enter special characters</source>
        <translation>ཡིག་རྟགས་ཁྱད་པར་ཅན་བཏགས་ནས་སྤྱོད་མཁན་གྱི་མིང་བྱེད་མི་རུང་།</translation>
    </message>
</context>
<context>
    <name>SearchMsgDelegate</name>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/search_msg_delegate.cpp" line="44"/>
        <source> relevant chat records</source>
        <translation> དོན་ཚན་འབྲེལ་ཡོད་ཀྱི་ཁ་བརྡའི་ཟིན་ཐོ།</translation>
    </message>
</context>
<context>
    <name>SearchPage</name>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="122"/>
        <source>Start Chat</source>
        <translation>གླེང་མོལ་བྱེད་འགོ་ཚུགས།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="123"/>
        <source>Set to Top</source>
        <translation>རྩེ་མོར་གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="124"/>
        <source>Change Nickname</source>
        <translation>གྲོགས་པོའི་ཟུར་མཆན་བཟོ་བཅོས་བྱས།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="125"/>
        <source>View Info</source>
        <translation>ལྟ་ཀློག་གི་རྒྱུ་ཆ།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="126"/>
        <source>Delete Friend</source>
        <translation>གྲོགས་པོ་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="164"/>
        <source>Friend</source>
        <translation>གྲོགས་པོ།</translation>
    </message>
    <message>
        <location filename="../../src/view/friendlist/homepagesearch/searchpage.cpp" line="168"/>
        <source>Chat Record</source>
        <translation>ཁ་བརྡའི་ཟིན་ཐོ།</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="142"/>
        <source>Minimize</source>
        <translation>ཉུང་དུ་གཏོང་གང་ཐུབ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="143"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titlebar.cpp" line="146"/>
        <source>Messages</source>
        <translation>ཆ་འཕྲིན།</translation>
    </message>
</context>
<context>
    <name>TitleSeting</name>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="103"/>
        <source>File Save Directory</source>
        <translation>ཡིག་ཆ་ཉར་ཚགས་བྱས་པའི་དཀར་ཆག</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="116"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="232"/>
        <source>Change Directory</source>
        <translation>འགྱུར་ལྡོག་བྱུང་བའི་དཀར་ཆག</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="123"/>
        <source>Clear All Chat Messages</source>
        <translation>ཁ་བརྡའི་ཆ་འཕྲིན་ཚང་མ་གཙང་སེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="130"/>
        <source>Clear the Cache</source>
        <translation>མྱུར་ཚད་གཙང་སེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Modified successfully</source>
        <translation type="vanished">修改成功</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="251"/>
        <source>Modified Successfully</source>
        <translation>བཟོ་བཅོས་ལེགས་འགྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="292"/>
        <source>Clear all messages？</source>
        <translation>ཆ་འཕྲིན་ཚང་མ་གཙང་སེལ་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="295"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="322"/>
        <source>No</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="294"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="323"/>
        <source>Yes</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="238"/>
        <source>The save path can only be a dir under the home dir!</source>
        <translation>ཉར་ཚགས་ཐབས་ལམ་ནི་ཁྱིམ་གྱི་དཀར་ཆག་འོག་གི་གསང་བ་མིན་པའི་དཀར་ཆག་ཅིག་ཡིན།!</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="306"/>
        <source>Cleared</source>
        <translation>གཙང་བཤེར་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="320"/>
        <source>Clean the cache information such as images/videos/documents?</source>
        <translation>པར་རིས་དང་། བརྙན་ཕབ། ཡིག་ཆ་སོགས་མགྱོགས་མྱུར་གྱི་ཆ་འཕྲིན་གཙང་བཤེར་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="336"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="360"/>
        <source>Clean Up Complete</source>
        <translation>གཙང་བཤེར་ལེགས་འགྲུབ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Clean up complete</source>
        <translation type="vanished">清理完成</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="387"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="394"/>
        <location filename="../../src/view/titlebar/titleseting.cpp" line="402"/>
        <source>Please do not save the file in this directory</source>
        <translation>ཁྱོད་ཀྱིས་དཀར་ཆག་འདིའི་ནང་གི་ཡིག་ཆ་ཉར་ཚགས་མ་བྱེད།</translation>
    </message>
</context>
<context>
    <name>TrayIconWid</name>
    <message>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="157"/>
        <source>Ignore</source>
        <translation>སྣང་མེད་དུ་བཞག་པའི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="124"/>
        <location filename="../../src/view/trayicon/trayiconwid.cpp" line="168"/>
        <source>Messages</source>
        <translation>དཔེ་ཆ་བརྒྱུད་སྤྲོད།</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="53"/>
        <source>Menu</source>
        <translation>འདེམས་པང་།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="67"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="148"/>
        <source>Settings</source>
        <translation>སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="69"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="146"/>
        <source>Theme</source>
        <translation>བརྗོད་བྱ་གཙོ་བོ།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="71"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="144"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="73"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="142"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="81"/>
        <source>Follow the Theme</source>
        <translation>བརྗོད་གཞི་དང་བསྟུན།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="85"/>
        <source>Light Theme</source>
        <translation>མདོག་སྐྱ་བོའི་ཁ་བྱང་།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="89"/>
        <source>Dark Theme</source>
        <translation>མདོག་ཟབ་པའི་བརྗོད་གཞི།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="301"/>
        <source>Version: </source>
        <translation>པར་གཞིའི་ཨང་གྲངས། </translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="306"/>
        <source>Message provides text chat and file transfer functions in the LAN. There is no need to build a server. It supports multiple people to interact at the same time and send and receive in parallel.</source>
        <translation>ཆི་ལིན་བརྒྱུད་བསྒྲགས་དཔེ་ཆས་ཁྱབ་ཆུང་དྲ་རྒྱའི་ནང་གི་ཡི་གེའི་ཁ་བརྡ་དང་ཡིག་ཆ་བརྒྱུད་གཏོང་གི་ནུས་པ་འདོན་སྤྲོད་བྱེད་པ་དང་།ཞབས་ཞུའི་འཕྲུལ་ཆས་འཛུགས་མི་དགོས།་མི་མང་པོ་ཞིག་གིས་དུས་མཉམ་དུ་ཕན་ཚུན་བརྗེ་རེས་དང་གཏོང་ལེན་མཉམ་གཤིབ་བྱེད་པར་རྒྱབ་སྐྱོར་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="411"/>
        <location filename="../../src/view/titlebar/menumodule.cpp" line="431"/>
        <source>Service &amp; Support: </source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར་ཚོགས་པ། </translation>
    </message>
    <message>
        <location filename="../../src/view/titlebar/menumodule.h" line="65"/>
        <source>Messages</source>
        <translation>དཔེ་ཆ་བརྒྱུད་སྤྲོད།</translation>
    </message>
</context>
</TS>
