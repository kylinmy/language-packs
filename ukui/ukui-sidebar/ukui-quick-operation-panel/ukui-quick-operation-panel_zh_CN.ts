<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>ModifybluetoothWidget</name>
    <message>
        <source>Not connected</source>
        <translation type="obsolete">未连接</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/accountinformation.cpp" line="17"/>
        <source>administrators</source>
        <translation>管理员</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/accountinformation.cpp" line="18"/>
        <source>standard users</source>
        <translation>普通用户</translation>
    </message>
    <message>
        <source>Standard users</source>
        <translation type="vanished">普通用户</translation>
    </message>
    <message>
        <source>Auto rotate</source>
        <translation type="vanished">自动旋转</translation>
    </message>
    <message>
        <source>eyeshield</source>
        <translation type="vanished">护眼模式</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="350"/>
        <source>NotiToggle</source>
        <translation>勿扰模式</translation>
    </message>
    <message>
        <source>pad</source>
        <translation type="vanished">平板模式</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="336"/>
        <source>Energy</source>
        <translation>节能模式</translation>
    </message>
    <message>
        <source>setting</source>
        <translation type="vanished">系统设置</translation>
    </message>
    <message>
        <source>Eyeshield</source>
        <translation type="vanished">护眼模式</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="354"/>
        <source>Pad</source>
        <translation>平板模式</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="334"/>
        <source>Setting</source>
        <translation>系统设置</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/powershow.cpp" line="56"/>
        <source>Electricity surplus</source>
        <translation>电量剩余</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/powershow.cpp" line="57"/>
        <source>Charging</source>
        <translation>正在充电</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/wifiwidget.h" line="78"/>
        <source>Wired connection</source>
        <translation>有线连接</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/bluetoothwidgetModify.cpp" line="26"/>
        <location filename="../../src/plugins/ukui-quick-operation-panel/wifiwidget.h" line="79"/>
        <source>Not connected</source>
        <translation>未连接</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/bluetoothwidgetModify.cpp" line="27"/>
        <location filename="../../src/plugins/ukui-quick-operation-panel/wifiwidget.h" line="80"/>
        <source>Closed</source>
        <translation>已关闭</translation>
    </message>
    <message>
        <source>AutoRotate</source>
        <translation type="obsolete">自动旋转</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="352"/>
        <source>Autorotate</source>
        <translation>自动旋转</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="346"/>
        <source>screenshot</source>
        <translation>快速截图</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="348"/>
        <source>clipboard</source>
        <translation>剪贴板</translation>
    </message>
    <message>
        <source>Eye care mode</source>
        <translation type="vanished">护眼模式</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="344"/>
        <source>Night mode</source>
        <translation>夜间模式</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="340"/>
        <source>flight</source>
        <translation>飞行模式</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="342"/>
        <source>projectscreen</source>
        <translation>投屏</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="338"/>
        <source>Bluetooth</source>
        <translation>蓝牙</translation>
    </message>
</context>
</TS>
