<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>ModifybluetoothWidget</name>
    <message>
        <source>Not connected</source>
        <translation type="obsolete">未连接</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/accountinformation.cpp" line="17"/>
        <source>administrators</source>
        <translation>དོ་དམ་མི་སྣ།</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/accountinformation.cpp" line="18"/>
        <source>standard users</source>
        <translation>ཚད་ལྡན་སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <source>Standard users</source>
        <translation type="vanished">普通用户</translation>
    </message>
    <message>
        <source>Auto rotate</source>
        <translation type="vanished">自动旋转</translation>
    </message>
    <message>
        <source>eyeshield</source>
        <translation type="vanished">护眼模式</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="350"/>
        <source>NotiToggle</source>
        <translation>ཟིང་མེད་རྣམ་པ།</translation>
    </message>
    <message>
        <source>pad</source>
        <translation type="vanished">平板模式</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="336"/>
        <source>Energy</source>
        <translation>ནུས་ཁུངས།</translation>
    </message>
    <message>
        <source>setting</source>
        <translation type="vanished">系统设置</translation>
    </message>
    <message>
        <source>Eyeshield</source>
        <translation type="vanished">护眼模式</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="354"/>
        <source>Pad</source>
        <translation>ཁ་གདན་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="334"/>
        <source>Setting</source>
        <translation>བཀོད་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/powershow.cpp" line="56"/>
        <source>Electricity surplus</source>
        <translation>གློག་ཤུགས་ལྷག་མ།</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/powershow.cpp" line="57"/>
        <source>Charging</source>
        <translation>རིན་བསྡུའི་གྲོན་དངུལ།</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/wifiwidget.h" line="78"/>
        <source>Wired connection</source>
        <translation>སྐུད་ཡོད་སྦྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/bluetoothwidgetModify.cpp" line="26"/>
        <location filename="../../src/plugins/ukui-quick-operation-panel/wifiwidget.h" line="79"/>
        <source>Not connected</source>
        <translation>འབྲེལ་མཐུད་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/bluetoothwidgetModify.cpp" line="27"/>
        <location filename="../../src/plugins/ukui-quick-operation-panel/wifiwidget.h" line="80"/>
        <source>Closed</source>
        <translation>བཀག་སྡོམ་བྱས་པ།</translation>
    </message>
    <message>
        <source>AutoRotate</source>
        <translation type="obsolete">自动旋转</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="352"/>
        <source>Autorotate</source>
        <translation>རང་འགུལ་གྱིས་རླངས་འཁོར་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="346"/>
        <source>screenshot</source>
        <translation>བརྙན་ཤེལ་གྱི་པར་རིས།</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="348"/>
        <source>clipboard</source>
        <translation>འདྲུད་པང་ལེབ།</translation>
    </message>
    <message>
        <source>Eye care mode</source>
        <translation type="vanished">护眼模式</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="344"/>
        <source>Night mode</source>
        <translation>མཚན་མོའི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="340"/>
        <source>flight</source>
        <translation>འཕུར་སྐྱོད་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="342"/>
        <source>projectscreen</source>
        <translation>རྣམ་གྲངས་ཀྱི་བརྙན་ཤེལ།</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-quick-operation-panel/templatewidget.cpp" line="338"/>
        <source>Bluetooth</source>
        <translation>ཁ་དོག་སྔོན་པོ།</translation>
    </message>
</context>
</TS>
