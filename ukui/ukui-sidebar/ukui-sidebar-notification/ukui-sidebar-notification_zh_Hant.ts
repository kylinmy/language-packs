<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>QObject</name>
    <message>
        <source>Notification center</source>
        <translation type="vanished">通知中心</translation>
    </message>
    <message>
        <source>Important notice</source>
        <translation type="vanished">重要的通知</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/notificationPlugin.cpp" line="161"/>
        <source>Clean up</source>
        <translation>收拾</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/notificationPlugin.cpp" line="156"/>
        <source>Recent news</source>
        <translation>近期新聞</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/notificationPlugin.cpp" line="189"/>
        <source>Set up</source>
        <translation>建立</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/notificationPlugin.cpp" line="528"/>
        <source>No new notifications</source>
        <translation>沒有新通知</translation>
    </message>
    <message>
        <source>No unimportant notice</source>
        <translation type="vanished">没有不重要的通知</translation>
    </message>
    <message>
        <source>Unimportant notice</source>
        <translation type="vanished">不重要的通知</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/appmsg.cpp" line="592"/>
        <source> fold</source>
        <translation> 倍</translation>
    </message>
</context>
<context>
    <name>SingleMsg</name>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/singlemsg.cpp" line="153"/>
        <source>now</source>
        <translation>現在</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/singlemsg.cpp" line="355"/>
        <source>Yesterday </source>
        <translation>昨天 </translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/singlemsg.cpp" line="431"/>
        <source>In addition </source>
        <translation>另外 </translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/singlemsg.cpp" line="431"/>
        <source> notification</source>
        <translation> 通知</translation>
    </message>
    <message>
        <source>Pack up </source>
        <translation type="obsolete">收起 </translation>
    </message>
    <message>
        <source>In addition</source>
        <translation type="obsolete">还有</translation>
    </message>
</context>
<context>
    <name>TakeInBoxToolButton</name>
    <message>
        <source>Enter unimportant news</source>
        <translation type="obsolete">进入不重要通知</translation>
    </message>
    <message>
        <source>Quit unimportant news</source>
        <translation type="obsolete">退出不重要通知</translation>
    </message>
</context>
</TS>
