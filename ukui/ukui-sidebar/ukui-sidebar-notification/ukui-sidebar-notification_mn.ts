<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>QObject</name>
    <message>
        <source>Notification center</source>
        <translation type="vanished">通知中心</translation>
    </message>
    <message>
        <source>Important notice</source>
        <translation type="vanished">重要的通知</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/notificationPlugin.cpp" line="161"/>
        <source>Clean up</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/notificationPlugin.cpp" line="156"/>
        <source>Recent news</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤ᠋ᠨ ᠰᠢᠨ᠎ᠡ ᠮᠡᠳᠡᠭᠡ</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/notificationPlugin.cpp" line="189"/>
        <source>Set up</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/notificationPlugin.cpp" line="528"/>
        <source>No new notifications</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠮᠡᠳᠡᠭᠳᠡᠯ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>No unimportant notice</source>
        <translation type="vanished">没有不重要的通知</translation>
    </message>
    <message>
        <source>Unimportant notice</source>
        <translation type="vanished">不重要的通知</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/appmsg.cpp" line="592"/>
        <source> fold</source>
        <translation> ᠡᠪᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>SingleMsg</name>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/singlemsg.cpp" line="153"/>
        <source>now</source>
        <translation>ᠤᠳᠤ</translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/singlemsg.cpp" line="355"/>
        <source>Yesterday </source>
        <translation>ᠦᠴᠦᠬᠡᠳᠦᠷ </translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/singlemsg.cpp" line="431"/>
        <source>In addition </source>
        <translation>ᠪᠠᠰᠠ </translation>
    </message>
    <message>
        <location filename="../../src/plugins/ukui-sidebar-notification/singlemsg.cpp" line="431"/>
        <source> notification</source>
        <translation> ᠮᠡᠳᠡᠭᠳᠡᠯ</translation>
    </message>
    <message>
        <source>Pack up </source>
        <translation type="obsolete">收起 </translation>
    </message>
    <message>
        <source>In addition</source>
        <translation type="obsolete">还有</translation>
    </message>
</context>
<context>
    <name>TakeInBoxToolButton</name>
    <message>
        <source>Enter unimportant news</source>
        <translation type="obsolete">进入不重要通知</translation>
    </message>
    <message>
        <source>Quit unimportant news</source>
        <translation type="obsolete">退出不重要通知</translation>
    </message>
</context>
</TS>
