<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>QObject</name>
    <message>
        <location filename="../src/main.cpp" line="47"/>
        <source>ukui-sidebar is already running!</source>
        <translation>ukui-sidebar 已经在运行！</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="52"/>
        <source>show or hide sidebar widget</source>
        <translation>显示或隐藏侧边栏小部件</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="53"/>
        <source>show sidebar widget</source>
        <translation>显示侧边栏小部件</translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="54"/>
        <source>hide sidebar widget</source>
        <translation>隐藏侧边栏小部件</translation>
    </message>
    <message>
        <location filename="../src/widget.cpp" line="161"/>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <location filename="../src/widget.cpp" line="164"/>
        <source>Set up notification center</source>
        <translation>设置通知中心</translation>
    </message>
</context>
<context>
    <name>SendMessageWorker</name>
    <message>
        <source>System prompt</source>
        <translation type="vanished">系统提示</translation>
    </message>
    <message>
        <source>Gesture interaction tutorial</source>
        <translation type="vanished">手势交互教程</translation>
    </message>
    <message>
        <source>Welcome to the Qilin Apocalypse learning platform and go to system settings - gesture guidance to start the experience journey</source>
        <translation type="vanished">欢迎使用麒麟天启学习平台，前往系统设置-手势引导，开启体验之旅</translation>
    </message>
    <message>
        <source>user &apos;s manual.</source>
        <translation type="vanished">用户手册</translation>
    </message>
    <message>
        <source>User help.</source>
        <translation type="vanished">用户帮助</translation>
    </message>
    <message>
        <source>Press Fn + F1 to view the user manual and play with the system.</source>
        <translation type="vanished">按 Fn + F1查看用户手册，玩转系统。</translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <source>ClipBoard</source>
        <translation type="vanished">剪贴板</translation>
    </message>
    <message>
        <source>kylin Apocalypse education platform.</source>
        <translation type="vanished">麒麟天启教育平台</translation>
    </message>
    <message>
        <source>System prompt.</source>
        <translation type="vanished">系统提示</translation>
    </message>
    <message>
        <source>Welcome to kylin Apocalypse education platform.</source>
        <translation type="vanished">欢迎使用麒麟天启教育平台。</translation>
    </message>
    <message>
        <source>user &apos;s manual.</source>
        <translation type="vanished">用户手册</translation>
    </message>
    <message>
        <source>User help.</source>
        <translation type="vanished">用户帮助</translation>
    </message>
    <message>
        <source>Press F1 to view the user manual and play with the system.</source>
        <translation type="vanished">按F1查看用户手册，玩转系统。</translation>
    </message>
    <message>
        <source>Sidebar</source>
        <translation type="vanished">侧边栏</translation>
    </message>
    <message>
        <location filename="../src/widget.cpp" line="192"/>
        <source>ukui-sidebar</source>
        <translation>侧边栏</translation>
    </message>
</context>
<context>
    <name>WidgetIntel</name>
    <message>
        <source>kylin Apocalypse education platform.</source>
        <translation type="obsolete">麒麟天启教育平台</translation>
    </message>
    <message>
        <source>System prompt.</source>
        <translation type="vanished">系统提示</translation>
    </message>
    <message>
        <source>Welcome to kylin Apocalypse education platform.</source>
        <translation type="obsolete">欢迎使用麒麟天启教育平台。</translation>
    </message>
    <message>
        <source>System prompt</source>
        <translation type="vanished">系统提示</translation>
    </message>
    <message>
        <source>Gesture interaction tutorial</source>
        <translation type="vanished">手势交互教程</translation>
    </message>
    <message>
        <source>Welcome to the Qilin Apocalypse learning platform and go to system settings - gesture guidance to start the experience journey</source>
        <translation type="vanished">欢迎使用麒麟天启学习平台，前往系统设置-手势引导，开启体验之旅</translation>
    </message>
    <message>
        <source>user &apos;s manual.</source>
        <translation type="vanished">用户手册</translation>
    </message>
    <message>
        <source>User help.</source>
        <translation type="vanished">用户帮助</translation>
    </message>
    <message>
        <source>Press Fn + F1 to view the user manual and play with the system.</source>
        <translation type="vanished">按 Fn + F1查看用户手册，玩转系统。</translation>
    </message>
    <message>
        <source>Press F1 to view the user manual and play with the system.</source>
        <translation type="vanished">按F1查看用户手册，玩转系统。</translation>
    </message>
    <message>
        <source> Kylin Oracle education platform.</source>
        <translation type="vanished">麒麟天启教育平台</translation>
    </message>
    <message>
        <source>Welcome to Kylin Apocalypse education platform.</source>
        <translation type="vanished">欢迎使用麒麟天启教育平台</translation>
    </message>
    <message>
        <source>Sidebar</source>
        <translation type="vanished">侧边栏</translation>
    </message>
</context>
<context>
    <name>Widget_MAJOR</name>
    <message>
        <source>Sidebar</source>
        <translation type="vanished">侧边栏</translation>
    </message>
</context>
<context>
    <name>sidebarPluginsWidgets</name>
    <message>
        <source>Clipboard</source>
        <translation type="vanished">剪贴板</translation>
    </message>
    <message>
        <source>Plugins</source>
        <translation type="vanished">小插件</translation>
    </message>
    <message>
        <source>Notebook</source>
        <translation type="vanished">笔记本</translation>
    </message>
    <message>
        <source>Alarm clock</source>
        <translation type="vanished">闹钟</translation>
    </message>
    <message>
        <source>Feedback</source>
        <translation type="vanished">用户反馈</translation>
    </message>
</context>
</TS>
