<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>BaseStyle</name>
    <message>
        <source>Services and Supports</source>
        <translation type="vanished">服务与支持</translation>
    </message>
</context>
<context>
    <name>ContactPage</name>
    <message>
        <location filename="../src/contact_page.cpp" line="49"/>
        <source>Contact Us</source>
        <translation>ང་ཚོ་དང་འབྲེལ་གཏུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/contact_page.cpp" line="61"/>
        <source>Support Team</source>
        <translation>ཞབས་ཞུ་དང་རོགས་སྐྱོར་རུ་ཁག</translation>
    </message>
    <message>
        <source>KylinOS Support Team</source>
        <translation type="vanished">服务与支持团队</translation>
    </message>
    <message>
        <location filename="../src/contact_page.cpp" line="79"/>
        <source>Tel</source>
        <translation>གློག་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../src/contact_page.cpp" line="95"/>
        <source>E-mail</source>
        <translation>གློག་རྡུལ་སྦྲག་ཡིག</translation>
    </message>
    <message>
        <location filename="../src/contact_page.cpp" line="111"/>
        <source>Kylin WeChat Official Account</source>
        <translation>སྤྱི་རྟགས་ཆི་ལིན་མཉེན་ཆས་ལག་རྩལ་ཞབས་ཞུ།</translation>
    </message>
</context>
<context>
    <name>ContactPageStyle</name>
    <message>
        <location filename="../src/contact_page_style.ui" line="32"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="72"/>
        <location filename="../src/contact_page_style.cpp" line="26"/>
        <source>Contact Us</source>
        <translation>ང་ཚོ་དང་འབྲེལ་གཏུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="242"/>
        <location filename="../src/contact_page_style.cpp" line="37"/>
        <source>Support Team</source>
        <translation>ཞབས་ཞུ་དང་རོགས་སྐྱོར་རུ་ཁག</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="269"/>
        <source>KylinOS Support Team</source>
        <translation>ཅིན་ལིན་ནོ་སི་རོགས་སྐྱོར་རུ་ཁག</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="337"/>
        <location filename="../src/contact_page_style.cpp" line="53"/>
        <source>Tel</source>
        <translation>གློག་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="364"/>
        <source>400-089-1870</source>
        <translation>400-089-1870</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="432"/>
        <location filename="../src/contact_page_style.cpp" line="69"/>
        <source>E-mail</source>
        <translation>གློག་རྡུལ་སྦྲག་ཡིག</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="459"/>
        <source>support@kylinos.cn</source>
        <translation>support@kylinos.cn</translation>
    </message>
    <message>
        <location filename="../src/contact_page_style.ui" line="586"/>
        <source>Kylin WeChat Official Account</source>
        <translation>སྤྱི་རྟགས་ཆི་ལིན་མཉེན་ཆས་ལག་རྩལ་ཞབས་ཞུ།</translation>
    </message>
</context>
<context>
    <name>CustomNaviButton</name>
    <message>
        <location filename="../src/customNaviButton.cpp" line="40"/>
        <source>Intro</source>
        <translatorcomment>软件介绍</translatorcomment>
        <translation>མཉན་ཆས་ངོ་སྤྲོད།</translation>
    </message>
    <message>
        <location filename="../src/customNaviButton.cpp" line="45"/>
        <source>Message</source>
        <translatorcomment>留言咨询</translatorcomment>
        <translation>སྐད་འཇོག་བློ་འདྲི།</translation>
    </message>
    <message>
        <location filename="../src/customNaviButton.cpp" line="50"/>
        <source>Contact</source>
        <translatorcomment>联系我们</translatorcomment>
        <translation>འབྲེལ་གཏུག་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../src/customNaviButton.cpp" line="55"/>
        <source>Guidance</source>
        <translatorcomment>自助支持</translatorcomment>
        <translation>རང་འདེམས་རྒྱབ་སྐྱོར།</translation>
    </message>
    <message>
        <location filename="../src/customNaviButton.cpp" line="60"/>
        <source>Customer</source>
        <translatorcomment>在线客服</translatorcomment>
        <translation>དྲ་ཐོག་ཞབས་ཞུ།</translation>
    </message>
</context>
<context>
    <name>DIYSupportPage</name>
    <message>
        <location filename="../src/diysupport_page.cpp" line="92"/>
        <source>Click </source>
        <translation>མཐེབ་གནོན་བྱེད། </translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="99"/>
        <source>to know more about support</source>
        <translation>སྔར་ལས་མང་བའི་ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར་གྱི་ནང་དོན་ལ་རྒྱུས་ལོན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="111"/>
        <source>，to KylinOS Official Web</source>
        <translation>KylinOSགཞུང་ཕྱོགས་དྲ་ཚིགས་ཀྱི་ལག་རྩལ་རྒྱབ་སྐྱོར་ཤོག་ངོས་སུ་ལྡིང་།</translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="116"/>
        <source>Users can solve problems through the ways below.</source>
        <translation>སྤྱོད་མཁན་གྱིས་གཤམ་གྱི་བྱེད་ཐབས་ལ་བརྟེན་ནས་གནད་དོན་ཐག་གཅོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="132"/>
        <source>To KylinOS Official Web</source>
        <translation>To KylinOS Official Web</translation>
    </message>
    <message>
        <source>Go to the KylinOS Official Web</source>
        <translation type="vanished">前往麒麟官网</translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="139"/>
        <source>Access the Web, find the answers of the 
normal problems.</source>
        <translation>སྤྱོད་མཁན་གྱིས་གཞུང་ཕྱོགས་དྲ་བར་བཅར་འདྲི་བྱས་པ་བརྒྱུད་ནས་རང་འགུལ་གྱིས་ལག་རྩལ་དང་རོགས་རམ་འཚོལ་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="156"/>
        <source>Guidance</source>
        <translation>རོགས་རམ་ལག་དེབ།</translation>
    </message>
    <message>
        <location filename="../src/diysupport_page.cpp" line="162"/>
        <source>Check Manuals to solve the problem.</source>
        <translation>རོགས་རམ་ལག་དེབ་འཚོལ་ཞིབ་བྱས་ནས་རྒྱབ་སྐྱོར་དང་རོགས་རམ་འཚོལ་བ།</translation>
    </message>
</context>
<context>
    <name>FileItemInit</name>
    <message>
        <location filename="../src/file_item_init.cpp" line="41"/>
        <source>Del</source>
        <translation>བསུབ་པ།</translation>
    </message>
</context>
<context>
    <name>GuidancePageStyle</name>
    <message>
        <location filename="../src/guidance_page_style.ui" line="32"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="160"/>
        <location filename="../src/guidance_page_style.cpp" line="37"/>
        <source>Click </source>
        <translation>མཐེབ་གནོན་བྱེད་པ། </translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="167"/>
        <location filename="../src/guidance_page_style.cpp" line="45"/>
        <source>to know more about support</source>
        <translation>རྒྱབ་སྐྱོར་གྱི་གནས་ཚུལ་ལ་རྒྱུས་ལོན་སྔར་ལས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="174"/>
        <location filename="../src/guidance_page_style.cpp" line="51"/>
        <source>，to KylinOS Official Web</source>
        <translation>KylinOSགཞུང་ཕྱོགས་དྲ་ཚིགས་ཀྱི་ལག་རྩལ་རྒྱབ་སྐྱོར་ཤོག་ངོས་སུ་ལྡིང་།</translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="248"/>
        <location filename="../src/guidance_page_style.cpp" line="54"/>
        <source>You can get your answers through the ways below</source>
        <translation>སྤྱོད་མཁན་གྱིས་གཤམ་གྱི་བྱེད་ཐབས་བརྒྱུད་ནས་ལག་རྩལ་གྱི་རྒྱབ་སྐྱོར་དང་ཞབས་ཞུ་འཚོལ་ཆོག</translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="446"/>
        <location filename="../src/guidance_page_style.cpp" line="63"/>
        <source>To KylinOS Official Web</source>
        <translation>ཆི་ལིན་གཞུང་ཕྱོགས་དྲ་ཚིགས་སུ་སོང་།</translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="484"/>
        <location filename="../src/guidance_page_style.cpp" line="67"/>
        <source>Access the Web, find the answers.</source>
        <translation>སྤྱོད་མཁན་གྱིས་གཞུང་ཕྱོགས་དྲ་བར་བཅར་འདྲི་བྱས་པ་བརྒྱུད་ནས་རང་འགུལ་གྱིས་ལག་རྩལ་དང་རོགས་རམ་འཚོལ་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="643"/>
        <location filename="../src/guidance_page_style.cpp" line="78"/>
        <source>Guidance</source>
        <translation>རོགས་རམ་ལག་དེབ།</translation>
    </message>
    <message>
        <location filename="../src/guidance_page_style.ui" line="681"/>
        <location filename="../src/guidance_page_style.cpp" line="82"/>
        <source>Check Manuals to solve the problem.</source>
        <translation>ལག་དེབ་ལ་ཞིབ་བཤེར་བྱས་ནས་གནད་དོན་ཐག་གཅོད་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Official WeChat</source>
        <translation type="obsolete">企业微信</translation>
    </message>
</context>
<context>
    <name>IntroPageStyle</name>
    <message>
        <location filename="../src/intro_page_style.ui" line="32"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="160"/>
        <location filename="../src/intro_page_style.cpp" line="28"/>
        <source>Click </source>
        <translation>མཐེབ་གནོན་བྱེད་པ། </translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="167"/>
        <location filename="../src/intro_page_style.cpp" line="36"/>
        <source>to know more about support</source>
        <translation>རྒྱབ་སྐྱོར་གྱི་གནས་ཚུལ་ལ་རྒྱུས་ལོན་སྔར་ལས་མང་བ།</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="174"/>
        <location filename="../src/intro_page_style.cpp" line="42"/>
        <source>，to KylinOS Official Web</source>
        <translation>KylinOSགཞུང་ཕྱོགས་དྲ་ཚིགས་ཀྱི་ལག་རྩལ་རྒྱབ་སྐྱོར་ཤོག་ངོས་སུ་ལྡིང་།</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="248"/>
        <location filename="../src/intro_page_style.cpp" line="45"/>
        <source>You can get your answers through the ways below</source>
        <translation>སྤྱོད་མཁན་གྱིས་གཤམ་གྱི་བྱེད་ཐབས་བརྒྱུད་ནས་ལག་རྩལ་གྱི་རྒྱབ་སྐྱོར་དང་ཞབས་ཞུ་འཚོལ་ཆོག</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="442"/>
        <location filename="../src/intro_page_style.cpp" line="54"/>
        <source>Official WeChat</source>
        <translation>གཞུང་ཕྱོགས་ཀྱི་འཕྲིན་ཕྲན།</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="451"/>
        <location filename="../src/intro_page_style.cpp" line="58"/>
        <source>Add our Team&apos;s WeChat</source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར་ཚོགས་པ་སྣོན་པ།</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="458"/>
        <location filename="../src/intro_page_style.cpp" line="63"/>
        <source>Get an online response.</source>
        <translation>ཁེ་ལས་ཀྱི་སྐད་འཕྲིན་ཨང་གྲངས།དྲ་ཐོག་ན་ཡོད་པ་འཚོལ་བ།རུ་ཁག</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="610"/>
        <location filename="../src/intro_page_style.cpp" line="74"/>
        <source>Online</source>
        <translation>དྲ་ཐོག་ཞབས་ཞུ་བར་འབྲེལ་བ་བྱོས།</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="619"/>
        <location filename="../src/intro_page_style.cpp" line="79"/>
        <source>Leave message</source>
        <translation>ཞབས་ཞུ་དང་ལག་རྩལ་དང་ལེན་འཚོལ་བ།</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="626"/>
        <location filename="../src/intro_page_style.cpp" line="83"/>
        <source>to get support.</source>
        <translation>གཞི་ནས་རྒྱབ་སྐྱོར་འཐོབ་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="760"/>
        <location filename="../src/intro_page_style.cpp" line="94"/>
        <source>Mail</source>
        <translation>བློ་འདྲི་བསྐུར་བ།</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="769"/>
        <location filename="../src/intro_page_style.cpp" line="98"/>
        <source>Send mail to</source>
        <translation>སྦྲག་ཡིག་གཞུང་ཕྱོགས་ཀྱི་ཡིག་སྒམ་དུ་བསྐུར་ནས་འཚོལ་དང་།</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="776"/>
        <location filename="../src/intro_page_style.cpp" line="103"/>
        <source>get support.</source>
        <translation>ཞབས་ཞུ་དང་ལག་རྩལ་དང་ལེན།</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="976"/>
        <location filename="../src/intro_page_style.cpp" line="114"/>
        <source>Telephone</source>
        <translation>ཁ་པར་བློ་འདྲི།</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="985"/>
        <location filename="../src/intro_page_style.cpp" line="118"/>
        <source>5*8 hours hotline.</source>
        <translation>5x8དུས་ཚོད་འབོད་བརྡ་ལྟེ་གནས་ཀྱི་ཁ་པར་རྒྱབ་སྐྱོར།
དང་ལེན།</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="1132"/>
        <location filename="../src/intro_page_style.cpp" line="134"/>
        <source>Guidance</source>
        <translation>རངའགུལ་རྒྱབ་སྐྱོར།</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="1141"/>
        <location filename="../src/intro_page_style.cpp" line="138"/>
        <source>Check Manuals to solve</source>
        <translation>རོགས་རམ་ལག་དེབ་ཀྱི་ནང་དོན་འཚོལ་ཞིབ་དང་འདྲི་རྩད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/intro_page_style.ui" line="1148"/>
        <location filename="../src/intro_page_style.cpp" line="143"/>
        <source>the problem.</source>
        <translation>གནད་དོན་ཐག་གཅོད།</translation>
    </message>
</context>
<context>
    <name>LocalInfo</name>
    <message>
        <location filename="../src/localInfo.cpp" line="148"/>
        <source>【显卡名称】：%1</source>
        <translation>“མངོན་གསལ་བྱང་བུའི་མིང་།”ནི།1%</translation>
    </message>
    <message>
        <location filename="../src/localInfo.cpp" line="156"/>
        <source>【显卡驱动名称】：%1</source>
        <translation>མངོན་གསལ་བྱང་བུའི་སྒུལ་ཤུགས་ཀྱི་མིང་ནི།1%</translation>
    </message>
    <message>
        <location filename="../src/localInfo.cpp" line="163"/>
        <source>【显卡数量】：%1 个</source>
        <translation>“བྱང་བུའི་ཁ་གྲངས”ནི་1%ཡིན་པ།</translation>
    </message>
</context>
<context>
    <name>MainPage</name>
    <message>
        <location filename="../src/main_page.cpp" line="84"/>
        <source>Click </source>
        <translation>མཐེབ་གནོན་བྱེད་པ། </translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="90"/>
        <source>to know more about support</source>
        <translation>སྔར་ལས་མང་བའི་ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར་གྱི་ནང་དོན་ལ་རྒྱུས་ལོན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="102"/>
        <source>，to KylinOS Official Web</source>
        <translation>KylinOSགཞུང་ཕྱོགས་དྲ་ཚིགས་ཀྱི་ལག་རྩལ་རྒྱབ་སྐྱོར་ཤོག་ངོས་སུ་ལྡིང་།</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="107"/>
        <source>You can get your answers through the ways below</source>
        <translation>སྤྱོད་མཁན་གྱིས་གཤམ་གྱི་བྱེད་ཐབས་བརྒྱུད་ནས་ལག་རྩལ་གྱི་རྒྱབ་སྐྱོར་དང་ཞབས་ཞུ་འཚོལ་ཆོག</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="121"/>
        <source>Official WeChat</source>
        <translation>གཞུང་ཕྱོགས་ཀྱི་འཕྲིན་ཕྲན།</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="128"/>
        <source>Add our Team&apos;s WeChat
 Get an online response.</source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར་ཚོགས་པ་སྣོན་པ།ཁེ་ལས་ཀྱི་སྐད་འཕྲིན་ཨང་གྲངས།
དྲ་ཐོག་ནས་སྒྲ་གྲགས་པར་འཚོལ་བ།</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="164"/>
        <source>Online</source>
        <translation>དྲ་ཐོག་ཞབས་ཞུ་བར་འབྲེལ་བ་བྱོས།</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="171"/>
        <source>Leave message
 to get support.</source>
        <translation>ཞབས་ཞུ་དང་ལག་རྩལ་འཚོལ་བ།</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="208"/>
        <source>Mail</source>
        <translation>བློ་འདྲི་བསྐུར་བ།</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="215"/>
        <source>Send mail to
get support.</source>
        <translation>སྦྲག་ཡིག་གཞུང་ཕྱོགས་ཀྱི་ཡིག་སྒམ་དུ་བསྐུར་ནས་འཚོལ་དང་།ཞབས་ཞུ་དང་ལག་རྩལ་དང་ལེན།</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="251"/>
        <source>Telephone</source>
        <translation>ཁ་པར་འདྲི་རྩད།</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="258"/>
        <source>5*8 hours hotline.</source>
        <translation>5x858དུས་ཚོད་འབོད་བརྡ་ལྟེ་གནས་ཀྱི་ཁ་པར་རྒྱབ་སྐྱོར།
དང་ལེན།</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="294"/>
        <source>Guidance</source>
        <translation>རང་འགུལ་རྒྱབ་སྐྱོར།</translation>
    </message>
    <message>
        <location filename="../src/main_page.cpp" line="301"/>
        <source>Check Manuals to solve
the problem.</source>
        <translation>རོགས་རམ་ལག་དེབ་ཀྱི་ནང་དོན་འཚོལ་ཞིབ་དང་འདྲི་རྩད་བྱེད་པ།
གནད་དོན་ཐག་གཅོད།</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="64"/>
        <location filename="../src/mainwindow.cpp" line="72"/>
        <location filename="../src/mainwindow.cpp" line="390"/>
        <source>Services and Supports</source>
        <translatorcomment>服务与支持</translatorcomment>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="398"/>
        <location filename="../src/mainwindow.cpp" line="487"/>
        <source>Intro</source>
        <translation>མཉན་ཆས་ངོ་སྤྲོད།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="399"/>
        <location filename="../src/mainwindow.cpp" line="490"/>
        <source>Message</source>
        <translation>སྐད་འཇོག་བློ་འདྲི།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="400"/>
        <location filename="../src/mainwindow.cpp" line="493"/>
        <source>Contact</source>
        <translation>འབྲེལ་གཏུག་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="401"/>
        <location filename="../src/mainwindow.cpp" line="496"/>
        <source>Guidance</source>
        <translation>རང་འདེམས་རྒྱབ་སྐྱོར།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="402"/>
        <location filename="../src/mainwindow.cpp" line="499"/>
        <source>Customer</source>
        <translation>དྲ་ཐོག་ཞབས་ཞུ།</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="38"/>
        <source>MainWindow</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>MessagePageStyle</name>
    <message>
        <location filename="../src/messagePageStyle.ui" line="32"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="232"/>
        <location filename="../src/messagePageStyle.ui" line="468"/>
        <location filename="../src/messagePageStyle.ui" line="1332"/>
        <source>*</source>
        <translation>*</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="273"/>
        <location filename="../src/messagePageStyle.ui" line="509"/>
        <location filename="../src/messagePageStyle.ui" line="686"/>
        <location filename="../src/messagePageStyle.ui" line="806"/>
        <location filename="../src/messagePageStyle.ui" line="888"/>
        <location filename="../src/messagePageStyle.ui" line="913"/>
        <location filename="../src/messagePageStyle.ui" line="1158"/>
        <location filename="../src/messagePageStyle.ui" line="1373"/>
        <location filename="../src/messagePageStyle.ui" line="1512"/>
        <location filename="../src/messagePageStyle.ui" line="1632"/>
        <location filename="../src/messagePageStyle.ui" line="1823"/>
        <source>TextLabel</source>
        <translation>ཡིག་དེབ་ཤོག་བྱང་།</translation>
    </message>
    <message>
        <source>    Please describe your problem</source>
        <translation type="vanished">    请详细描述要咨询的问题</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="574"/>
        <source>Please describe your problem</source>
        <translation>ཁྱེད་ཀྱི་གནད་དོན་ཞིབ་བརྗོད་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="830"/>
        <location filename="../src/messagePageStyle.ui" line="2030"/>
        <location filename="../src/messagePageStyle.ui" line="2071"/>
        <source>PushButton</source>
        <translation>PushButton</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="1195"/>
        <location filename="../src/messagePageStyle.cpp" line="430"/>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="1214"/>
        <location filename="../src/messagePageStyle.cpp" line="172"/>
        <source>No</source>
        <translation>མིན།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.ui" line="1968"/>
        <location filename="../src/messagePageStyle.ui" line="1989"/>
        <source>CheckBox</source>
        <translation>CheckBox</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="79"/>
        <source>Submitting...</source>
        <translation>སྤྲོད་བཞིན་པའི་སྒང་ཡིན།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="84"/>
        <source>Submit Successed!</source>
        <translation>ལེགས་འགྲུབ་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="93"/>
        <source>Question*</source>
        <translation>བློ་འདྲིའི་རིགས།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="97"/>
        <location filename="../src/messagePageStyle.cpp" line="588"/>
        <source>System</source>
        <translation>མ་ལག་གི་གནད་དོན།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="98"/>
        <source>Suggestion</source>
        <translation>བསམ་འཆར་གྲོས་གཞི།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="99"/>
        <source>Bussiness</source>
        <translation>ཚོང་དོན་མཉམ་ལས།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="100"/>
        <source>Others</source>
        <translation>གཞན་དག</translation>
    </message>
    <message>
        <source>Content*</source>
        <translation type="vanished">咨询内容*</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="114"/>
        <source>Word Limit</source>
        <translation>ཡིག་རྟགས་ཀྱི་ཚད་ཐིག་ལ་ཐོན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="134"/>
        <source>Upload</source>
        <translation>ཟུར་ཡིག་ཡར་བསྐུར།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="140"/>
        <source>Browse</source>
        <translation>ལྟ་ཀློག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="145"/>
        <source>All attachmens size should smaller than 10MB and numbers less than 5</source>
        <translation>ཟུར་གཏོགས་མི་སྣ་ཡོངས་ཀྱི་གཞི་ཁྱོན་ནི་10MBལས་ཆུང་བ་དང་། མི་གྲངས་5ལས་ཉུང་དགོས།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="146"/>
        <source>Support format:.gif *.jpg *.png *.pptx *.wps *.xlsx *.pdf *.txt *.docx</source>
        <translation>རྒྱབ་སྐྱོར་རྣམ་གཞག་:.gif *.jpg *.png *.pptx *.wps *.xlsx *.pdf .txt *.docx</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="165"/>
        <source>UploadLog</source>
        <translation>ཡར་བསྐུར་ཉིན་ཐོ།</translation>
    </message>
    <message>
        <source>Mail*</source>
        <translation type="vanished">邮件*</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="194"/>
        <source>Leave your E-mail to get response</source>
        <translation>ང་ཚོས་སྦྲགས་སྲིད་བརྒྱུད་ནས་མཇུག་འབྲས་ལྡོག་འདྲེན་བྱེད།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="200"/>
        <source>Incorrect E-mail address.</source>
        <translation>ཡང་དག་མིན་པའི་གློག་རྡུལ་ཡིག་སྒམ་གྱི་ས་གནས།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="220"/>
        <source>Tel</source>
        <translation>གློག་འཕྲིན།</translation>
    </message>
    <message>
        <source>Leave your Tel to get response.</source>
        <translation type="vanished">用于反馈处理进度和结果的联系电话</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="231"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <source>What should I call you.</source>
        <translation type="vanished">联系人姓名</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="254"/>
        <source>Os_rel</source>
        <translation>བཀོལ་སྤྱོད་མ་ལག</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="254"/>
        <location filename="../src/messagePageStyle.cpp" line="263"/>
        <location filename="../src/messagePageStyle.cpp" line="270"/>
        <source>:</source>
        <translation>:</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="263"/>
        <source>Deskenv</source>
        <translation>ཅོག་ཙེའི་སྟེང་གི་ཁོར་ཡུག</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="270"/>
        <source>Lang</source>
        <translation>མ་ལག་གི་སྐད་བརྡ།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="328"/>
        <source>Upload my </source>
        <translation>ང་ཐོབ་པར་འཐད་པ་བྱུང་། </translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="333"/>
        <source>System Info</source>
        <translation>མ་ལག་གི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="340"/>
        <source>Commit</source>
        <translation>གོང་སྤྲོད།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="345"/>
        <source>Reset</source>
        <translation>བསྐྱར་དུ་བཀོད་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="429"/>
        <source>Tips:</source>
        <translation>གསལ་འདེབས།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="428"/>
        <source>You Can Submit 1 time per 30s!</source>
        <translation>སྐར་ཆ་སུམ་ཅུའི་ནང་ཐེངས་གཅིག་མ་གཏོགས་སྤྲོད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="105"/>
        <source>Content</source>
        <translation>ནང་དོན་འདྲི་རྩད།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="187"/>
        <source>Mail</source>
        <translation>སྦྲག་རྫས།</translation>
    </message>
    <message>
        <location filename="../src/messagePageStyle.cpp" line="832"/>
        <source>select file</source>
        <translation>ཡིག་ཆ་གདམ་གསེས་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>MyPushButton</name>
    <message>
        <source>Intro</source>
        <translation type="vanished">软件介绍</translation>
    </message>
    <message>
        <source>Message</source>
        <translation type="vanished">留言咨询</translation>
    </message>
    <message>
        <source>Contact</source>
        <translation type="vanished">联系我们</translation>
    </message>
    <message>
        <source>Guidance</source>
        <translation type="vanished">自助服务</translation>
    </message>
    <message>
        <source>Customer</source>
        <translation type="vanished">在线客服</translation>
    </message>
</context>
<context>
    <name>NavigateBar</name>
    <message>
        <location filename="../src/navigatebar.ui" line="32"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <source>Services and Supports</source>
        <translatorcomment>服务与支持</translatorcomment>
        <translation type="vanished">服务与支持</translation>
    </message>
</context>
<context>
    <name>SubmitBase</name>
    <message>
        <location filename="../src/submitBase.cpp" line="93"/>
        <source>Server Connect Overtime!</source>
        <translation>ཡིག་སྒམ་ཞབས་ཞུའི་འཕྲུལ་ཆས་དང་སྦྲེལ་མཐུད་དུས་ཚོད་ལས་བརྒལ་འདུག!</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="94"/>
        <source>与邮箱服务器链接超时</source>
        <translation>ཡིག་སྒམ་ཞབས་ཞུ་ཆས་དང་འབྲེལ་མཐུད་དུས་བརྒལ།</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="97"/>
        <source>FeedBack Overtime!</source>
        <translation>smtpཕྱིར་ལྡོག་ཆ་འཕྲིན་དུས་བརྒལ།!</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="98"/>
        <source>smtp返回信息超时</source>
        <translation>smtpཕྱིར་ལྡོག་ཆ་འཕྲིན་དུས་བརྒལ།</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="101"/>
        <source>Server Unready!</source>
        <translation>ཞབས་ཞུའི་ཡོ་བྱད་ལ་ལྟ་ཞིབ་བྱེད་མི་ཐུབ།!</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="102"/>
        <source>mail服务器未准备好</source>
        <translation>Mailཞབས་ཞུ་ཆས་གྲ་སྒྲིག་ཡག་པོ་བྱས་མེད།</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="105"/>
        <source>Login Failed!</source>
        <translation>smtpཐོ་འགོད་བྱས་ཀྱང་ཕམ་ཁ་བྱུང་བ་རེད།!</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="106"/>
        <source>邮箱登录失败</source>
        <translation>ཞབས་ཞུ་ཆས་ཐོ་འགོད་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="109"/>
        <source>Mail Set Failed!</source>
        <translation>ཞབས་ཞུ་ཆས་ཐོ་འགོད་བྱས་པ་ཕམ་སོང་།!</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="110"/>
        <source>邮箱用户设置失败</source>
        <translation>ཡིག་སྒམ་སྤྱོད་མཁན་བཀོད་སྒྲིག་ལ་ཕམ་ཉེས་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="113"/>
        <source>Mail Send Failed!</source>
        <translation>སྐད་བཞག་པ་ཕམ་སོང་།!</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="114"/>
        <source>邮件发送失败</source>
        <translation>སྦྲག་རྫས་བསྐུར་བ་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="117"/>
        <source>Mail Quit Failed!</source>
        <translation>ཞབས་ཞུ་ཆས་ཐོ་འགོད་བྱས་པ་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="118"/>
        <source>邮件退出失败</source>
        <translation>སྦྲག་རྫས་ཕྱིར་འཐེན་ཕམ་པ།</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="121"/>
        <source>Local Info Unready!</source>
        <translation>ས་གནས་དེ་གའི་ཆ་འཕྲིན་ལ་གོ་བ་ལེན་མི་ཐུབ།!</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="122"/>
        <source>本机信息未获取完毕</source>
        <translation>འཕྲུལ་ཆས་འདིའི་ཆ་འཕྲིན་ཐོབ་མ་ཚར།</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="125"/>
        <source>Unknown Error!</source>
        <translation>ཤེས་མེད་པའི་ནོར་འཁྲུལ།!</translation>
    </message>
    <message>
        <location filename="../src/submitBase.cpp" line="126"/>
        <source>未知错误</source>
        <translation>མ་ཤེས་པའི་ནོར་འཁྲུལ།</translation>
    </message>
</context>
<context>
    <name>T::menuModule</name>
    <message>
        <location filename="../src/menumodule.h" line="83"/>
        <source>Services and Supports</source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར།</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/main.cpp" line="95"/>
        <source>kylinservicesupport</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="99"/>
        <source>show kylin-service-support test</source>
        <translation>show kylin-service-support test</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <location filename="../src/menumodule.cpp" line="57"/>
        <source>standard</source>
        <translation>standard</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="59"/>
        <source>scientific</source>
        <translation>ཚན་རིག་དང་མཐུན་པ།</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="61"/>
        <source>exchange rate</source>
        <translation>དངུལ་བརྗེས་འཛའ་ཐང་།</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="67"/>
        <source>Theme</source>
        <translation>བརྗོད་བྱ་གཙོ་བོ།</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="69"/>
        <location filename="../src/menumodule.cpp" line="94"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="71"/>
        <location filename="../src/menumodule.cpp" line="91"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="73"/>
        <location filename="../src/menumodule.cpp" line="88"/>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན་བྱ་རྒྱུ།</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="131"/>
        <source>Service and Support</source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར།</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="131"/>
        <source>version：1.0.24</source>
        <translation>པར་གཞི:1.0.24</translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="189"/>
        <source>Version: </source>
        <translation>པར་གཞིའི་ཨང་གྲངས། </translation>
    </message>
    <message>
        <location filename="../src/menumodule.cpp" line="200"/>
        <location filename="../src/menumodule.cpp" line="207"/>
        <location filename="../src/menumodule.cpp" line="247"/>
        <location filename="../src/menumodule.cpp" line="254"/>
        <source>Service &amp; Support: </source>
        <translation>ཞབས་ཞུ་རྒྱབ་སྐྱོར་ཚོགས་པ། </translation>
    </message>
    <message>
        <location filename="../src/menumodule.h" line="83"/>
        <source>Services and Supports</source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར།</translation>
    </message>
</context>
</TS>
