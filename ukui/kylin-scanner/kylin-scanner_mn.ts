<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>AboutDialog</name>
    <message>
        <source>Scanner is an interface-friendly scanner, which could be also used as one-click beautification, intelligent correction and text recognition tools.</source>
        <translation type="vanished">扫描是一款可用于普通扫描、一键美化、智能纠偏和文字识别的界面友好扫描仪软件。</translation>
    </message>
    <message>
        <source>Service &amp; Support : </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
</context>
<context>
    <name>DefaultConnectFailedPageWidget</name>
    <message>
        <source>Not detect scanners, please connect scanners firstly!</source>
        <translation type="vanished">未检测到可用扫描设备，请先连接扫描设备</translation>
    </message>
    <message>
        <source>Connect</source>
        <translation type="vanished">连接扫描仪</translation>
    </message>
</context>
<context>
    <name>DefaultConnectSuccessPageLeftWidget</name>
    <message>
        <source>Connect scanners, please click scan button to start scanning.</source>
        <translation type="vanished">已连接扫描设备，点击按钮开始扫描</translation>
    </message>
</context>
<context>
    <name>DefaultConnectSuccessPageRightWidget</name>
    <message>
        <source>Begin Scan</source>
        <translation type="vanished">开始扫描</translation>
    </message>
    <message>
        <source>Device</source>
        <translation type="vanished">设备</translation>
    </message>
    <message>
        <source>Scanner device</source>
        <translation type="vanished">扫描仪设备</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">延时</translation>
    </message>
    <message>
        <source>File settings</source>
        <translation type="vanished">文件预设</translation>
    </message>
    <message>
        <source>Pages</source>
        <translation type="vanished">页数</translation>
    </message>
    <message>
        <source>Type</source>
        <translation type="vanished">类型</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">色彩</translation>
    </message>
    <message>
        <source>Resolution</source>
        <translation type="vanished">分辨率</translation>
    </message>
    <message>
        <source>Size</source>
        <translation type="vanished">尺寸</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="vanished">格式</translation>
    </message>
    <message>
        <source>Name</source>
        <translation type="vanished">名称</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="vanished">扫描至</translation>
    </message>
    <message>
        <source>Send email</source>
        <translation type="vanished">发送至邮箱</translation>
    </message>
    <message>
        <source>Save as</source>
        <translation type="vanished">另存为</translation>
    </message>
</context>
<context>
    <name>DetectPageWidget</name>
    <message>
        <source>Detect scanners, please waiting ...</source>
        <translation type="vanished">检测扫描设备中，请稍后 ……</translation>
    </message>
    <message>
        <location filename="../src/detectpagewidget.cpp" line="44"/>
        <source>Detect scanners, please waiting</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FailedPageWidget</name>
    <message>
        <location filename="../src/failedpagewidget.cpp" line="42"/>
        <source>Not detect scanners, please connect scanners firstly!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/failedpagewidget.cpp" line="45"/>
        <source>Connect</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ImageOperationOCR</name>
    <message>
        <location filename="../src/imageOp/imageoperationocr.cpp" line="24"/>
        <location filename="../src/imageOp/imageoperationocr.cpp" line="32"/>
        <source>Unable to read text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KYCAboutDialog</name>
    <message>
        <location filename="../src/about/about.ui" line="26"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scanner is an interface-friendly scanner, which could be also used as one-click beautification, intelligent correction and text recognition tools.</source>
        <translation type="vanished">扫描是一款可用于普通扫描、一键美化、智能纠偏和文字识别的界面友好扫描仪软件。</translation>
    </message>
    <message>
        <source>Service &amp; Support : </source>
        <translation type="vanished">服务与支持团队：</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本</translation>
    </message>
    <message>
        <source>Scanner</source>
        <translation type="vanished">扫描</translation>
    </message>
</context>
<context>
    <name>LeftSuccessPageWidget</name>
    <message>
        <location filename="../src/leftsuccesspagewidget.cpp" line="58"/>
        <source>Connect scanners, please click scan button to start scanning.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <location filename="../src/mainwidget.cpp" line="76"/>
        <source>kylin-scanner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="207"/>
        <source>Scanner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="210"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is a new scanner connect, please restart this application manually. </source>
        <translation type="vanished">存在新设备连接，请手动重启应用使用该新设备。</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="462"/>
        <location filename="../src/mainwidget.cpp" line="482"/>
        <location filename="../src/mainwidget.cpp" line="520"/>
        <source>There is a new scanner connect, redetect all scanners, please wait a moment. </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="493"/>
        <location filename="../src/mainwidget.cpp" line="564"/>
        <source>No available device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="577"/>
        <location filename="../src/mainwidget.cpp" line="595"/>
        <source>device </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="577"/>
        <location filename="../src/mainwidget.cpp" line="595"/>
        <source> has been disconnect.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="674"/>
        <source>Single</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="717"/>
        <source>Invalid argument, please change arguments or switch other scanners.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="733"/>
        <source>Device busy, please wait or switch other scanners.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="735"/>
        <source>Document feeder out of documents, please place papers and scan again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="737"/>
        <source>Scan operation has been cancelled.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scan failed, please check your scanner or switch other scanners.</source>
        <translation type="vanished">扫描失败，请检查当前扫描仪连接或切换到其他扫描仪。</translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="784"/>
        <source>Running beauty ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="806"/>
        <source>Running rectify ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>NoMailDialog</name>
    <message>
        <location filename="../src/sendmail.cpp" line="52"/>
        <location filename="../src/sendmail.cpp" line="84"/>
        <source>No email client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="58"/>
        <source>Scanner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="61"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="98"/>
        <source>Not find email client in the system, please install email client firstly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="113"/>
        <source>Install</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="110"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OcrObject</name>
    <message>
        <location filename="../src/ocrobject.cpp" line="16"/>
        <location filename="../src/ocrobject.cpp" line="26"/>
        <source>Unable to read text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>OcrThread</name>
    <message>
        <location filename="../src/mainwidget.cpp" line="1026"/>
        <location filename="../src/mainwidget.cpp" line="1039"/>
        <source>Unable to read text</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>User </source>
        <translation type="vanished">用户</translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="287"/>
        <source>Question</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="284"/>
        <source>Current </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="284"/>
        <source> User</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="285"/>
        <source> has already opened kylin-scanner, open will close </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/singleapplication.cpp" line="286"/>
        <source>&apos;s operations. Are you continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="738"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="745"/>
        <source>Gray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="751"/>
        <source>Lineart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="800"/>
        <source>Default Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="879"/>
        <source>Flatbed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="889"/>
        <source>ADF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="898"/>
        <source>ADF Front</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="907"/>
        <source>ADF Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="916"/>
        <source>ADF Duplex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="966"/>
        <location filename="../src/saneobject.cpp" line="1030"/>
        <source>4800 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="969"/>
        <location filename="../src/saneobject.cpp" line="1034"/>
        <source>2400 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="972"/>
        <location filename="../src/saneobject.cpp" line="1038"/>
        <source>1200 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="975"/>
        <location filename="../src/saneobject.cpp" line="1042"/>
        <source>600 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="978"/>
        <location filename="../src/saneobject.cpp" line="1046"/>
        <source>300 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="981"/>
        <location filename="../src/saneobject.cpp" line="1050"/>
        <source>200 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="984"/>
        <location filename="../src/saneobject.cpp" line="1054"/>
        <source>150 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="987"/>
        <location filename="../src/saneobject.cpp" line="1058"/>
        <source>100 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="990"/>
        <location filename="../src/saneobject.cpp" line="1062"/>
        <source>75 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="1008"/>
        <location filename="../src/saneobject.cpp" line="1078"/>
        <source>Auto</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RunningDialog</name>
    <message>
        <location filename="../src/runningdialog.cpp" line="53"/>
        <location filename="../src/runningdialog.cpp" line="139"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/runningdialog.cpp" line="83"/>
        <location filename="../src/runningdialog.cpp" line="169"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SaneObject</name>
    <message>
        <location filename="../src/saneobject.cpp" line="2033"/>
        <source>Default Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2025"/>
        <source>Flatbed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2027"/>
        <source>ADF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2029"/>
        <source>ADF Front</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2031"/>
        <source>ADF Back</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Lineart</source>
        <translation type="vanished">黑白</translation>
    </message>
    <message>
        <source>Gray</source>
        <translation type="vanished">灰度</translation>
    </message>
    <message>
        <source>Color</source>
        <translation type="vanished">彩色</translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2035"/>
        <source>ADF Duplex</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2101"/>
        <source>Auto</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2105"/>
        <source>75 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2107"/>
        <source>150 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2109"/>
        <source>200 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2111"/>
        <source>300 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2113"/>
        <source>600 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2115"/>
        <source>1200 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2117"/>
        <source>2400 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2119"/>
        <source>4800 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/saneobject.cpp" line="2303"/>
        <source>Multiple</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScanDialog</name>
    <message>
        <location filename="../src/scandialog.cpp" line="28"/>
        <location filename="../src/scandialog.cpp" line="31"/>
        <source>Scanner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="34"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="52"/>
        <location filename="../src/scandialog.cpp" line="153"/>
        <source>Number of pages scanning: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Number of pages being scanned: </source>
        <translation type="vanished">正在扫描页数：</translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="61"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="122"/>
        <source>Canceling scan，please wait a moment!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scandialog.cpp" line="151"/>
        <source>Multiple</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScanSettingsWidget</name>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="522"/>
        <source>Begin Scan</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="545"/>
        <source>Scanner device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="476"/>
        <location filename="../src/scansettingswidget.cpp" line="674"/>
        <source>Time</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="560"/>
        <source>File settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="474"/>
        <source>Device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="110"/>
        <source>Select a directory</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="132"/>
        <source>Currently user has no permission to modify directory </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="192"/>
        <location filename="../src/scansettingswidget.cpp" line="745"/>
        <source>Multiple</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="475"/>
        <source>Pages</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="477"/>
        <source>Type</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="236"/>
        <location filename="../src/scansettingswidget.cpp" line="798"/>
        <source>Color</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="479"/>
        <source>Resolution</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="480"/>
        <source>Size</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="481"/>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="482"/>
        <source>Name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="567"/>
        <source>scanner01</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="483"/>
        <source>Save</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="488"/>
        <source>Mail to</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Send email</source>
        <translation type="vanished">发送至邮箱</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="586"/>
        <location filename="../src/scansettingswidget.cpp" line="935"/>
        <source>Save as</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No available scanners</source>
        <translation type="vanished">无可用设备</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="203"/>
        <location filename="../src/scansettingswidget.cpp" line="745"/>
        <source>Single</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="249"/>
        <source>4800 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="250"/>
        <source>2400 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="251"/>
        <source>1200 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="253"/>
        <source>This resolution will take a loog time to scan, please choose carelly.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="278"/>
        <location filename="../src/scansettingswidget.cpp" line="397"/>
        <source>cannot contain &apos;/&apos; character.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="284"/>
        <location filename="../src/scansettingswidget.cpp" line="402"/>
        <source>cannot save as hidden file.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="351"/>
        <location filename="../src/scansettingswidget.cpp" line="927"/>
        <source>Store text</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="361"/>
        <source>Save as dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="447"/>
        <source>The file </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="448"/>
        <source> already exists, do you want to overwrite it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="450"/>
        <source>tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="478"/>
        <source>Colour</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="759"/>
        <source>3s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="759"/>
        <source>5s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="759"/>
        <source>7s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="759"/>
        <source>10s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="759"/>
        <source>15s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="776"/>
        <source>Flatbed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="776"/>
        <source>ADF</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="798"/>
        <source>Gray</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1166"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="238"/>
        <location filename="../src/scansettingswidget.cpp" line="798"/>
        <source>Lineart</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="820"/>
        <source>75 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="820"/>
        <source>100 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="820"/>
        <source>150 dpi</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="837"/>
        <source>Resolution is empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="852"/>
        <source>A4</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="852"/>
        <source>A5</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Scanning images&apos;s length cannot be large than 252</source>
        <translation type="vanished">扫描文档名称的长度不能超过252。</translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1161"/>
        <source>Scanner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/scansettingswidget.cpp" line="1165"/>
        <source>Yes</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ScanThread</name>
    <message>
        <location filename="../src/mainwidget.cpp" line="874"/>
        <source>Multiple</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="921"/>
        <source>3s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="924"/>
        <source>5s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="927"/>
        <source>7s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="930"/>
        <source>10s</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwidget.cpp" line="933"/>
        <source>15s</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SendMailDialog</name>
    <message>
        <location filename="../src/sendmail.cpp" line="216"/>
        <location filename="../src/sendmail.cpp" line="240"/>
        <source>Select email client</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="222"/>
        <source>Scanner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="225"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="254"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/sendmail.cpp" line="257"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ShowImageWidget</name>
    <message>
        <location filename="../src/showimagewidget.cpp" line="817"/>
        <source>Running beauty ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/showimagewidget.cpp" line="852"/>
        <source>Running rectify ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="29"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="109"/>
        <location filename="../src/titlebar/titlebar.h" line="83"/>
        <source>kylin-scanner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="112"/>
        <location filename="../src/titlebar/titlebar.cpp" line="170"/>
        <source>Scanner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="175"/>
        <source>Option</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="178"/>
        <source>...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="219"/>
        <source>Minimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="260"/>
        <location filename="../src/titlebar/titlebar.cpp" line="114"/>
        <source>Maximize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.ui" line="307"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Maxmize</source>
        <translation type="vanished">最大化</translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="37"/>
        <source>Help</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="41"/>
        <source>About</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="44"/>
        <source>Version: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="45"/>
        <source>Message provides text chat and file transfer functions in the LAN. There is no need to build a server. It supports multiple people to interact at the same time and send and receive in parallel.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="56"/>
        <source>Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="111"/>
        <source>Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="168"/>
        <source>The current document is not saved. Do you want to save it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="174"/>
        <source>Straight &amp;Exit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/titlebar/titlebar.cpp" line="176"/>
        <source>&amp;Save Exit</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ToolBarWidget</name>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="36"/>
        <source>Beauty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="41"/>
        <source>Rectify</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="46"/>
        <source>OCR</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="53"/>
        <source>Crop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="58"/>
        <source>Rotate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="63"/>
        <source>Mirror</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="68"/>
        <source>Watermark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="85"/>
        <source>ZoomIn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/toolbarwidget.cpp" line="80"/>
        <source>ZoomOut</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UsbHotplugThread</name>
    <message>
        <source>device </source>
        <translation type="vanished">设备 </translation>
    </message>
    <message>
        <source> has been disconnect.</source>
        <translation type="vanished"> 已经断开连接！</translation>
    </message>
    <message>
        <source>device has been disconnect.</source>
        <translation type="vanished">设备已断开！</translation>
    </message>
    <message>
        <location filename="../src/usbhotplugthread.cpp" line="71"/>
        <source>Scanner has been disconnect.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>WatermarkDialog</name>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="27"/>
        <location filename="../src/watermarkdialog.cpp" line="33"/>
        <source>Scanner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="36"/>
        <source>Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="50"/>
        <source>Add watermark</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="66"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/watermarkdialog.cpp" line="69"/>
        <source>Confirm</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../src/main.cpp" line="201"/>
        <source>Open file &lt;filename&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="202"/>
        <source>Filename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/main.cpp" line="206"/>
        <source>Hide scan settings widget</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>showOcrWidget</name>
    <message>
        <location filename="../src/showocrwidget.cpp" line="30"/>
        <location filename="../src/showocrwidget.cpp" line="146"/>
        <location filename="../src/showocrwidget.cpp" line="154"/>
        <source>The document is in character recognition ...</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
