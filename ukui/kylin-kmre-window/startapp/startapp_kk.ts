<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>Dialog</name>
    <message>
        <source>Dialog</source>
        <translation type="vanished">窗口</translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="14"/>
        <source>Launch window of kmre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.ui" line="46"/>
        <source>Preparing Kmre Env</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="76"/>
        <source>Init Kmre Env</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="143"/>
        <source>The kernel does not support Kmre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="146"/>
        <source>Virtual machine is not supported in Kmre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="149"/>
        <source>Kmre is corrupted, please check if other Android compatible env are installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="152"/>
        <source>Please do not use root</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="155"/>
        <source>The current graphics card is not supported in Kmre</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="158"/>
        <source>Kmre only supports Kylin OS for the time being</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="161"/>
        <source>Kmre does not support the current CPU temporarily</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="164"/>
        <source>The current graphics card cannot support more apps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="167"/>
        <source>The current CPU cannot support running more apps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="170"/>
        <source>The current hardware environment cannot support running more apps</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="173"/>
        <source>Exception of dbus service interface to be accessed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="176"/>
        <source>For better experience, please close an android app and then open it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="179"/>
        <source>The current startup item does not support Kmre, please use default startup item</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="363"/>
        <source>Service startup failed: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="369"/>
        <source>No service found: %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="375"/>
        <source>Image configuration not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="381"/>
        <source>Image not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="387"/>
        <source>Image loading failed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../dialog.cpp" line="393"/>
        <source>Image mismatch</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../main.cpp" line="634"/>
        <location filename="../main.cpp" line="662"/>
        <source>Tips</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="638"/>
        <source>The %1 package is not installed;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="640"/>
        <source>After installing the dependency package, re-install the kylin-kmre-modules-dkms package to meet the environment required by KMRE, then restart your computer and try to experience KMRE;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../main.cpp" line="666"/>
        <source>binder not found, please confirm the environment required by KMRE.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
