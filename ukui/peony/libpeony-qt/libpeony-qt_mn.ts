<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>ConnectServerDialog</name>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="14"/>
        <source>Connect to Sever</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠡᠷ ᠲᠤ᠌ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="32"/>
        <source>Domain</source>
        <translation>ᠨᠸᠲ ᠤ᠋ᠨ ᠬᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="39"/>
        <source>Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="55"/>
        <source>Save Password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠴᠡᠬᠡᠵᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="62"/>
        <source>User</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="82"/>
        <source>Anonymous</source>
        <translation>ᠪᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠡᠷ᠎ᠡ ᠪᠡᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.cpp" line="35"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.cpp" line="36"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
</context>
<context>
    <name>FileLabelModel</name>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="47"/>
        <source>Red</source>
        <translation>ᠤᠯᠠᠭᠠᠨ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="48"/>
        <source>Orange</source>
        <translation>ᠤᠯᠠᠪᠳᠤᠷ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="49"/>
        <source>Yellow</source>
        <translation>ᠰᠢᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="50"/>
        <source>Green</source>
        <translation>ᠨᠤᠭᠤᠭᠠᠨ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="51"/>
        <source>Blue</source>
        <translation>ᠬᠦᠬᠡ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="52"/>
        <source>Purple</source>
        <translation>ᠬᠦᠷᠡᠩ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="53"/>
        <source>Gray</source>
        <translation>ᠰᠠᠭᠠᠷᠠᠯ</translation>
    </message>
    <message>
        <source>Transparent</source>
        <translation type="vanished">无颜色</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="120"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="345"/>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="120"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="345"/>
        <source>Label or color is duplicated.</source>
        <translation>ᠱᠤᠰᠢᠭ᠎ᠠ ᠡᠰᠡᠪᠡᠯ ᠦᠩᠭᠡ ᠳᠠᠪᠬᠤᠷᠳᠠᠪᠠ.</translation>
    </message>
</context>
<context>
    <name>Format_Dialog</name>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>ᠹᠤᠷᠮᠠᠲᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="32"/>
        <source>rom_size</source>
        <translation>ᠠᠭᠤᠯᠠᠭᠳᠠᠬᠤᠨ ᠤ᠋ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="45"/>
        <source>system</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠰᠢᠰᠲ᠋ᠧᠮ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="59"/>
        <source>vfat/fat32</source>
        <translation>vfat/fat32</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="64"/>
        <source>exfat</source>
        <translation>exfat</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="69"/>
        <source>ntfs</source>
        <translation>NTFS</translation>
    </message>
    <message>
        <source>vfat</source>
        <translation type="vanished">VFAT</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="74"/>
        <source>ext4</source>
        <translation>Ext4</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="88"/>
        <source>device_name</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="114"/>
        <source>clean it total</source>
        <translation>ᠪᠦᠷᠢᠨ ᠪᠠᠯᠠᠯᠠᠬᠤ( ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠨᠡᠯᠢᠶᠡᠳ ᠤᠷᠳᠤ᠂ ᠪᠠᠳᠤᠯᠠᠭᠠᠷᠠᠢ!)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="127"/>
        <source>ok</source>
        <translation>ok</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="140"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="179"/>
        <source>TextLabel</source>
        <translation>ᠠᠭᠤᠯᠠᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <source>qmesg_notify</source>
        <translation type="vanished">通知</translation>
    </message>
    <message>
        <source>Format operation has been finished successfully.</source>
        <translation type="vanished">格式化操作已成功完成。</translation>
    </message>
    <message>
        <source>Sorry, the format operation is failed!</source>
        <translation type="vanished">很遗憾，格式化操作失败了，您可以重新试下！</translation>
    </message>
    <message>
        <source>Formatting this volume will erase all data on it. Please backup all retained data before formatting. Do you want to continue ?</source>
        <translation type="vanished">格式化此卷将清除其上的所有数据。请在格式化之前备份所有保留的数据。您想继续吗?</translation>
    </message>
    <message>
        <source>format</source>
        <translation type="vanished">格式化</translation>
    </message>
    <message>
        <source>begin format</source>
        <translation type="vanished">开始</translation>
    </message>
    <message>
        <source>format_success</source>
        <translation type="vanished">格式化成功!</translation>
    </message>
    <message>
        <source>format_err</source>
        <translation type="vanished">格式化失败!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="57"/>
        <source>Format</source>
        <translation>ᠹᠤᠷᠲᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="70"/>
        <source>Rom size:</source>
        <translation>ᠠᠭᠤᠯᠠᠭᠳᠠᠬᠤᠨ ᠤ᠋ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="76"/>
        <source>Filesystem:</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠰᠢᠰᠲ᠋ᠧᠮ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="86"/>
        <source>Disk name:</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠦᠷᠮᠵᠢ ᠵᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="116"/>
        <source>Completely erase(Time is longer, please confirm!)</source>
        <translation>ᠪᠦᠷᠢᠨ ᠪᠠᠯᠠᠯᠠᠬᠤ( ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠨᠡᠯᠢᠶᠡᠳ ᠤᠷᠳᠤ᠂ ᠪᠠᠳᠤᠯᠠᠭᠠᠷᠠᠢ!)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="126"/>
        <source>Set password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="127"/>
        <source>Set password for volume based on LUKS (only ext4)</source>
        <translation>ext4 ᠬᠤᠪᠢᠶᠢᠠᠷᠢ ᠬᠡᠰᠡᠭ ᠲᠤ᠌LUKS ᠲᠤ᠌/ ᠳ᠋ᠤ᠌ ᠰᠠᠭᠤᠷᠢᠯᠠᠭᠰᠠᠨ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="140"/>
        <source>Cancel</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="141"/>
        <source>OK</source>
        <translation>ok</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="244"/>
        <source>Data</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="411"/>
        <source>Enter Password:</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠤᠷᠤᠭᠤᠯᠬᠤ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="428"/>
        <source>Password too short, please retype a password more than 6 characters</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠬᠡᠳᠦ ᠪᠤᠭᠤᠨᠢ᠂6 ᠤᠷᠤᠨ ᠡᠴᠡ ᠳᠡᠭᠡᠭᠰᠢ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1051"/>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1051"/>
        <source>Block not existed!</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠤᠷᠤᠰᠢᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1092"/>
        <source>Formatting. Do not close this window</source>
        <translation>ᠶᠠᠭ ᠹᠤᠷᠲᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠪᠢᠳᠡᠬᠡᠢ ᠬᠠᠭᠠᠭᠠᠷᠠᠢ</translation>
    </message>
</context>
<context>
    <name>MainProgressBar</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="382"/>
        <source>File operation</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="421"/>
        <source>starting ...</source>
        <translation>ᠶᠠᠭ ᠡᠬᠢᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="398"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="515"/>
        <source>cancel all file operations</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="399"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="516"/>
        <source>Are you sure want to cancel all file operations</source>
        <translation>ᠲᠠ ᠪᠦᠬᠦᠢᠯᠡ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ ᠤᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="401"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="518"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="402"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="519"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="605"/>
        <source>canceling ...</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="608"/>
        <source>sync ...</source>
        <translation>ᠶᠠᠭ ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
</context>
<context>
    <name>OtherButton</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="703"/>
        <source>Other queue</source>
        <translation>ᠪᠤᠰᠤᠳ ᠡᠩᠨᠡᠭᠡ</translation>
    </message>
</context>
<context>
    <name>Peony::AdvanceSearchBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="55"/>
        <source>Key Words</source>
        <translation>ᠵᠠᠩᠭᠢᠯᠠᠭ᠎ᠠ ᠵᠢᠨ ᠦᠬᠡᠰ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="58"/>
        <source>input key words...</source>
        <translation>ᠵᠠᠩᠭᠢᠯᠠᠭ᠎ᠠ ᠵᠢᠨ ᠦᠬᠡᠰ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠬᠤ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="59"/>
        <source>Search Location</source>
        <translation>ᠬᠠᠢᠯᠳᠠ ᠵᠢᠨ ᠵᠠᠮ ᠱᠤᠭᠤᠮ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="61"/>
        <source>choose search path...</source>
        <translation>ᠬᠠᠢᠬᠤ ᠤᠷᠤᠨ ᠪᠠᠢᠷᠢ ᠵᠢ ᠰᠤᠩᠭᠤᠬᠤ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="68"/>
        <source>browse</source>
        <translation>ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="69"/>
        <source>File Type</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="71"/>
        <source>Choose File Type</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="76"/>
        <source>Modify Time</source>
        <translation>ᠵᠠᠰᠠᠭᠰᠠᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="78"/>
        <source>Choose Modify Time</source>
        <translation>ᠵᠠᠰᠠᠭᠰᠠᠨ ᠴᠠᠭ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="83"/>
        <source>File Size</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="85"/>
        <source>Choose file size</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠵᠢ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="90"/>
        <source>show hidden file</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠠᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="91"/>
        <source>go back</source>
        <translation>ᠤᠬᠤᠷᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="92"/>
        <source>hidden advance search page</source>
        <translation>ᠦᠨᠳᠦᠷ ᠳᠡᠰ ᠤ᠋ᠨ ᠬᠠᠢᠯᠳᠠ ᠵᠢᠨ ᠵᠤᠯᠭᠠᠭᠤᠷ ᠢ᠋ ᠨᠢᠭᠤᠴᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="94"/>
        <source>file name</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="95"/>
        <source>content</source>
        <translation>ᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="100"/>
        <source>search</source>
        <translation>ᠬᠠᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="101"/>
        <source>start search</source>
        <translation>ᠬᠠᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="174"/>
        <source>Select path</source>
        <translation>ᠵᠠᠮ ᠱᠤᠭᠤᠮ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="193"/>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="202"/>
        <source>Operate Tips</source>
        <translation>ᠰᠠᠨᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="194"/>
        <source>Have no key words or search location!</source>
        <translation>ᠵᠠᠩᠭᠢᠯᠠᠭ᠎ᠠ ᠵᠢᠨ ᠦᠭᠡ ᠪᠤᠶᠤ ᠵᠠᠮ ᠱᠤᠭᠤᠮ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="203"/>
        <source>Search file name or content at least choose one!</source>
        <translation>ᠬᠠᠢᠬᠤ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ ᠪᠤᠯᠤᠨ ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢᠨ ᠠᠯᠢ ᠨᠢᠭᠡ ᠵᠢ ᠳᠤᠭᠳᠠᠭᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <source>Search content or file name at least choose one!</source>
        <translation type="vanished">搜索文件名或者内容请至少指定一个！</translation>
    </message>
    <message>
        <source>all</source>
        <translation type="vanished">全部</translation>
    </message>
    <message>
        <source>file folder</source>
        <translation type="vanished">文件夹</translation>
    </message>
    <message>
        <source>image</source>
        <translation type="vanished">图片</translation>
    </message>
    <message>
        <source>video</source>
        <translation type="vanished">视频</translation>
    </message>
    <message>
        <source>text file</source>
        <translation type="vanished">文本</translation>
    </message>
    <message>
        <source>audio</source>
        <translation type="vanished">音频</translation>
    </message>
    <message>
        <source>others</source>
        <translation type="vanished">其它</translation>
    </message>
    <message>
        <source>wps file</source>
        <translation type="vanished">WPS文件</translation>
    </message>
    <message>
        <source>today</source>
        <translation type="vanished">今天</translation>
    </message>
    <message>
        <source>this week</source>
        <translation type="vanished">本周</translation>
    </message>
    <message>
        <source>this month</source>
        <translation type="vanished">本月</translation>
    </message>
    <message>
        <source>this year</source>
        <translation type="vanished">今年</translation>
    </message>
    <message>
        <source>year ago</source>
        <translation type="vanished">去年</translation>
    </message>
    <message>
        <source>tiny(0-16K)</source>
        <translation type="vanished">极小(0-16K)</translation>
    </message>
    <message>
        <source>small(16k-1M)</source>
        <translation type="vanished">较小(16k-1M)</translation>
    </message>
    <message>
        <source>medium(1M-100M)</source>
        <translation type="vanished">中等(1M-100M)</translation>
    </message>
    <message>
        <source>big(100M-1G)</source>
        <translation type="vanished">较大(100M-1G)</translation>
    </message>
    <message>
        <source>large(&gt;1G)</source>
        <translation type="vanished">极大(&gt;1G)</translation>
    </message>
</context>
<context>
    <name>Peony::AdvancedLocationBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/advanced-location-bar.cpp" line="175"/>
        <source>Search Content...</source>
        <translation>ᠠᠭᠤᠯᠭ᠎ᠠ ᠵᠢ ᠬᠠᠢᠬᠤ...</translation>
    </message>
</context>
<context>
    <name>Peony::AllFileLaunchDialog</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="341"/>
        <source>Choose new application</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠡ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ ᠵᠢ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="343"/>
        <source>Choose an Application to open this file</source>
        <translation>ᠨᠢᠭᠡ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ ᠰᠤᠩᠭᠤᠵᠤ ᠡᠨᠡ ᠹᠠᠢᠯ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="350"/>
        <source>apply now</source>
        <translation>ᠳᠠᠷᠤᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="356"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="357"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>Peony::AudioPlayManager</name>
    <message>
        <source>Operation file Warning</source>
        <translation type="vanished">文件操作警告</translation>
    </message>
</context>
<context>
    <name>Peony::BasicPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="859"/>
        <source>Choose a custom icon</source>
        <translation>ᠦᠪᠡᠰᠦᠪᠡᠨ ᠳᠤᠭᠳᠠᠭᠠᠭᠰᠠᠨ ᠢᠺᠦᠨ ᠵᠢᠷᠤᠭ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="385"/>
        <source>Type:</source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ:</translation>
    </message>
    <message>
        <source>Display Name:</source>
        <translation type="vanished">名称：</translation>
    </message>
    <message>
        <source>Location:</source>
        <translation type="vanished">路径：</translation>
    </message>
    <message>
        <source>Overview:</source>
        <translation type="vanished">概览：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="236"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="237"/>
        <source>Change</source>
        <translation>ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="291"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="292"/>
        <source>Location</source>
        <translation>ᠪᠠᠢᠷᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="334"/>
        <source>move</source>
        <translation>ᠰᠢᠯᠵᠢᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="391"/>
        <source>symbolLink</source>
        <translation>ᠲᠦᠳᠡ ᠠᠷᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="395"/>
        <source>Folder</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="402"/>
        <source>Include:</source>
        <translation>ᠪᠠᠭᠳᠠᠭᠠᠬᠤ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="406"/>
        <source>Open with:</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ ᠠᠷᠭ᠎ᠠ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="412"/>
        <source>Description:</source>
        <translation>ᠳᠦᠷᠰᠦᠯᠡᠬᠦ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="424"/>
        <source>Select multiple files</source>
        <translation>ᠤᠯᠠᠨ ᠹᠠᠢᠯ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="429"/>
        <source>Size:</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ:</translation>
    </message>
    <message>
        <source>Total size:</source>
        <translation type="vanished">实际大小：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="430"/>
        <source>Space Useage:</source>
        <translation>ᠡᠵᠡᠯᠡᠬᠦ ᠤᠷᠤᠨ ᠵᠠᠢ:</translation>
    </message>
    <message>
        <source>yyyy-MM-dd, HH:mm:ss</source>
        <translation type="vanished">yyyy年MM月dd日, HH:mm:ss</translation>
    </message>
    <message>
        <source>yyyy-MM-dd, hh:mm:ss AP</source>
        <translation type="vanished">yyyy年MM月dd日, hh:mm:ss AP</translation>
    </message>
    <message>
        <source>Time Created:</source>
        <translation type="vanished">创建时间：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="172"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="465"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="471"/>
        <source>Time Modified:</source>
        <translation>ᠵᠠᠰᠠᠭᠰᠠᠨ ᠴᠠᠭ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="466"/>
        <source>Time Access:</source>
        <translation>ᠠᠢᠯᠴᠢᠯᠠᠭᠰᠠᠨ ᠴᠠᠭ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="494"/>
        <source>Readonly</source>
        <translation>ᠵᠦᠪᠬᠡᠨ ᠤᠩᠰᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="495"/>
        <source>Hidden</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="512"/>
        <source>Property:</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠳᠤ ᠴᠢᠨᠠᠷ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="601"/>
        <source>usershare</source>
        <translation>ᠳᠤᠰ ᠮᠠᠰᠢᠨ ᠢ᠋ ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="696"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="900"/>
        <source>%1 (%2 Bytes)</source>
        <translation>%1 (%2 ᠪᠠᠢᠲ)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="732"/>
        <source>Choose a new folder:</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠡ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ ᠵᠢ ᠰᠤᠩᠭᠤᠬᠤ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="739"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="744"/>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="739"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="744"/>
        <source>cannot move a folder to itself !</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ ᠵᠢ ᠳᠡᠬᠦᠨ ᠤ᠋ ᠳᠤᠳᠤᠭᠠᠳᠤ ᠳ᠋ᠤ᠌ ᠰᠢᠯᠵᠢᠬᠦᠯᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="893"/>
        <source>%1 Bytes</source>
        <translation>%1 ᠪᠠᠢᠲ</translation>
    </message>
    <message>
        <source>%1 KB (%2 Bytes)</source>
        <translation type="vanished">%1 KB (%2 字节)</translation>
    </message>
    <message>
        <source>%1 MB (%2 Bytes)</source>
        <translation type="vanished">%1 MB (%2 字节)</translation>
    </message>
    <message>
        <source>%1 GB (%2 Bytes)</source>
        <translation type="vanished">%1 GB (%2 字节)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="909"/>
        <source>%1 files, %2 folders</source>
        <translation>%1 ᠹᠠᠢᠯ, %2 ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="997"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="999"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1004"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1006"/>
        <source>Can&apos;t get remote file information</source>
        <translation>ᠠᠯᠤᠰ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠵᠢ ᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>%1 files (include root files), %2 hidden</source>
        <translation type="vanished">共%1个文件（包括顶层目录），有%2个隐藏文件</translation>
    </message>
    <message>
        <source>%1 total</source>
        <translation type="vanished">共%1</translation>
    </message>
</context>
<context>
    <name>Peony::ComputerPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="93"/>
        <source>CPU Name:</source>
        <translation>ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠦᠷ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="94"/>
        <source>CPU Core:</source>
        <translation>ᠭᠤᠤᠯ ᠲᠤᠭ᠎ᠠ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="95"/>
        <source>Memory Size:</source>
        <translation>ᠳᠤᠳᠤᠭᠠᠳᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠭᠤᠷ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="107"/>
        <source>User Name: </source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ: </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="108"/>
        <source>Desktop: </source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠤᠷᠴᠢᠨ: </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="116"/>
        <source>You should mount this volume first</source>
        <translation>ᠲᠠ ᠳᠤᠰ ᠡᠪᠬᠡᠮᠡᠯ ᠢ᠋ ᠠᠴᠢᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠠᠶᠢ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="129"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="213"/>
        <source>Name: </source>
        <translation>ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ: </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="129"/>
        <source>File System</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤᠨ ᠱᠢᠰᠲ᠋ᠧᠮ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="129"/>
        <source>Data</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="130"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="218"/>
        <source>Total Space: </source>
        <translation>ᠶᠡᠷᠦᠩᠬᠡᠢ ᠡᠵᠡᠯᠡᠬᠦᠨ: </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="131"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="219"/>
        <source>Used Space: </source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠤᠷᠤᠨ ᠵᠠᠢ: </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="132"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="220"/>
        <source>Free Space: </source>
        <translation>ᠦᠯᠡᠳᠡᠭᠰᠡᠨ ᠤᠷᠤᠨ ᠵᠠᠢ: </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="133"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="221"/>
        <source>Type: </source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ: </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="230"/>
        <source>Kylin Burner</source>
        <translation>ᠺᠸᠯᠤᠳᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="236"/>
        <source>Open with: 	</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ: 	</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="243"/>
        <source>Unknown</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ</translation>
    </message>
</context>
<context>
    <name>Peony::ConnectServerDialog</name>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="134"/>
        <source>connect to server</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠡᠷ ᠲᠤ᠌ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="159"/>
        <source>ip</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="161"/>
        <source>port</source>
        <translation>ᠦᠵᠦᠬᠦᠷ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="162"/>
        <source>type</source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="192"/>
        <source>Personal Collection server:</source>
        <translation>ᠬᠤᠪᠢ ᠵᠢᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠭᠰᠠᠨ ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="205"/>
        <source>add</source>
        <translation>ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="206"/>
        <source>delete</source>
        <translatorcomment>连接</translatorcomment>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="207"/>
        <source>connect</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>Peony::ConnectServerLogin</name>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="382"/>
        <source>The login user</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠪᠡᠶ᠎ᠡ ᠵᠢᠨ ᠭᠠᠷᠤᠯ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="392"/>
        <source>Please enter the %1&apos;s user name and password of the server.</source>
        <translation>%1 ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠪᠠ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="401"/>
        <source>User&apos;s identity</source>
        <translation>ᠪᠡᠶ᠎ᠡ ᠵᠢᠨ ᠭᠠᠷᠤᠯ ᠢ᠋ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="402"/>
        <source>guest</source>
        <translation>ᠵᠢᠭᠤᠯᠴᠢᠨ( ᠪᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠡᠷ᠎ᠡ ᠪᠡᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="403"/>
        <source>Registered users</source>
        <translation>ᠪᠦᠷᠢᠳᠬᠡᠬᠦᠯᠦᠭᠰᠡᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="420"/>
        <source>name</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="421"/>
        <source>password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="422"/>
        <source>Remember the password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠢ᠋ ᠴᠡᠬᠡᠵᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="442"/>
        <source>cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="443"/>
        <source>ok</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>Peony::CreateLinkInternalPlugin</name>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="115"/>
        <source>Create Link to Desktop</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠲᠦᠳᠡ ᠠᠷᠭ᠎ᠠ ᠳ᠋ᠤ᠌ ᠢᠯᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="141"/>
        <source>Create Link to...</source>
        <translation>ᠲᠦᠳᠡ ᠠᠷᠭ᠎ᠠ ᠵᠢ ᠢᠯᠡᠬᠡᠬᠡᠳ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="144"/>
        <source>Choose a Directory to Create Link</source>
        <translation>ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠭᠠᠷᠴᠠᠭ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Peony-Qt Create Link Extension</source>
        <translation type="vanished">创建链接</translation>
    </message>
    <message>
        <source>Create Link Menu Extension.</source>
        <translation type="vanished">创建链接.</translation>
    </message>
</context>
<context>
    <name>Peony::CreateSharedFileLinkMenuPlugin</name>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="233"/>
        <source>Create Link to Desktop</source>
        <translation>ᠰᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠲᠦᠳᠡ ᠠᠷᠭ᠎ᠠ ᠳ᠋ᠤ᠌ ᠢᠯᠡᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>Peony::CreateTemplateOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="62"/>
        <source>NewFile</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="78"/>
        <source>Create file</source>
        <translation>ᠹᠠᠢᠯ ᠪᠠᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="90"/>
        <source>NewFolder</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="109"/>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="142"/>
        <source>Create file error</source>
        <translation>ᠹᠠᠢᠯ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠢ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
</context>
<context>
    <name>Peony::CustomErrorHandler</name>
    <message>
        <location filename="../../libpeony-qt/custom-error-handler.cpp" line="40"/>
        <source>Is Error Handled?</source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠵᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠭᠰᠡᠨ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/custom-error-handler.cpp" line="44"/>
        <source>Error not be handled correctly</source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠵᠢ ᠵᠦᠪ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠬᠡ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>Peony::DefaultOpenWithWidget</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="400"/>
        <source>No default app</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠨᠡᠬᠡᠬᠡᠬᠦ ᠠᠷᠭ᠎ᠠ ᠵᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>Peony::DefaultPreviewPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="71"/>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="186"/>
        <source>Select the file you want to preview...</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠦᠵᠡᠬᠦ ᠭᠡᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠹᠠᠢᠯ ᠢ᠋ᠶ᠋ᠡᠨ ᠰᠤᠩᠭᠤᠭᠠᠷᠠᠢ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="177"/>
        <source>Can not preview this file.</source>
        <translation>ᠳᠤᠰ ᠹᠠᠢᠯ ᠢ᠋ ᠦᠵᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <source>Can not preivew this file.</source>
        <translation type="vanished">不能预览该文件</translation>
    </message>
</context>
<context>
    <name>Peony::DefaultPreviewPageFactory</name>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page-factory.h" line="50"/>
        <source>Default Preview</source>
        <translation>ᠨᠠᠷᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page-factory.h" line="53"/>
        <source>This is the Default Preview of peony-qt</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠠᠷᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠵᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
</context>
<context>
    <name>Peony::DetailsPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="174"/>
        <source>Name:</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="177"/>
        <source>File type:</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="193"/>
        <source>Location:</source>
        <translation>ᠵᠠᠮ ᠱᠤᠭᠤᠮ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="207"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="214"/>
        <source>yyyy-MM-dd, HH:mm:ss</source>
        <translation>yyyy ᠤᠨ-MM ᠰᠠᠷ᠎ᠠ-dd ᠡᠳᠦᠷ, HH:mm:ss</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="199"/>
        <source>Create time:</source>
        <translation>ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠴᠠᠭ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="204"/>
        <source>Modify time:</source>
        <translation>ᠵᠠᠰᠠᠭᠰᠠᠨ ᠴᠠᠭ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="212"/>
        <source>yyyy-MM-dd, hh:mm:ss AP</source>
        <translation>yyyy ᠤᠨ-MM ᠰᠠᠷ᠎ᠠ-dd ᠡᠳᠦᠷ, HH:mm:ssAP</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="221"/>
        <source>File size:</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="228"/>
        <source>Width:</source>
        <translation>ᠥᠷᠭᠡᠨ ᠦ᠌ ᠬᠡᠮᠵᠢᠶ᠎ᠡ᠄</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="231"/>
        <source>Height:</source>
        <translation>ᠥᠨᠳᠥᠷ ᠦ᠋ᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ᠄</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="239"/>
        <source>Owner</source>
        <translation>ᠦᠮᠴᠢᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="240"/>
        <source>Owner:</source>
        <translation>ᠦᠮᠴᠢᠯᠡᠭᠴᠢ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="242"/>
        <source>Computer</source>
        <translation>ᠺᠤᠮᠫᠢᠦᠲᠸᠷ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="243"/>
        <source>Computer:</source>
        <translation>ᠺᠤᠮᠫᠢᠦᠲᠸᠷ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="288"/>
        <source>%1 (this computer)</source>
        <translation>%1 ( ᠳᠤᠰ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="295"/>
        <source>Unknown</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="324"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="325"/>
        <source>Can&apos;t get remote file information</source>
        <translation>ᠠᠯᠤᠰ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠵᠢ ᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="334"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="335"/>
        <source>%1 px</source>
        <translation>%1ᠫᠢᠺᠰᠧᠯ</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::IconView</name>
    <message>
        <source>Icon View</source>
        <translation type="vanished">图标视图</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/icon-view/icon-view.cpp" line="290"/>
        <source>warn</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/icon-view/icon-view.cpp" line="290"/>
        <source>This operation is not supported.</source>
        <translation>ᠳᠤᠰ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ.</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::IconView2</name>
    <message>
        <source>Icon View</source>
        <translation type="vanished">图标视图</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::ListView</name>
    <message>
        <source>List View</source>
        <translation type="vanished">列表视图</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/list-view/list-view.cpp" line="482"/>
        <source>warn</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/list-view/list-view.cpp" line="482"/>
        <source>This operation is not supported.</source>
        <translation>ᠳᠤᠰ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ.</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::ListView2</name>
    <message>
        <source>List View</source>
        <translation type="vanished">列表视图</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryViewFactoryManager</name>
    <message>
        <source>Icon View</source>
        <translation type="vanished">图标视图</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryViewMenu</name>
    <message>
        <source>Open in &amp;New Window</source>
        <translation type="vanished">在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <source>Open in New &amp;Tab</source>
        <translation type="vanished">在新标签页中打开(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="282"/>
        <source>Add to bookmark</source>
        <translation>ᠲᠦᠷᠬᠡᠨ ᠠᠢᠯᠴᠢᠯᠠᠬᠤ ᠳ᠋ᠤ᠌ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
    <message>
        <source>&amp;Open &quot;%1&quot;</source>
        <translation type="vanished">打开“%1”(&amp;O)</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; in &amp;New Window</source>
        <translation type="vanished">在新窗口中打开“%1”(&amp;N)</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; in New &amp;Tab</source>
        <translation type="vanished">在新标签页中打开“%1”(&amp;T)</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; with...</source>
        <translation type="vanished">选用其它应用打开“%1”...</translation>
    </message>
    <message>
        <source>&amp;More applications...</source>
        <translation type="vanished">更多应用...(&amp;M)</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation type="vanished">打开(&amp;O)</translation>
    </message>
    <message>
        <source>Open &amp;with...</source>
        <translation type="vanished">打开方式(&amp;W)...</translation>
    </message>
    <message>
        <source>&amp;Open %1 selected files</source>
        <translation type="vanished">打开%1个选中文件(&amp;O)</translation>
    </message>
    <message>
        <source>&amp;New...</source>
        <translation type="vanished">新建...(&amp;N)</translation>
    </message>
    <message>
        <source>Empty &amp;File</source>
        <translation type="vanished">空文件(&amp;E)</translation>
    </message>
    <message>
        <source>&amp;Folder</source>
        <translation type="vanished">文件夹(&amp;F)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="563"/>
        <source>New Folder</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <source>Icon View</source>
        <translation type="vanished">图标视图</translation>
    </message>
    <message>
        <source>List View</source>
        <translation type="vanished">列表视图</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="589"/>
        <source>View Type...</source>
        <translation>ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="608"/>
        <source>Sort By...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="613"/>
        <source>Name</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="615"/>
        <source>File Type</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="616"/>
        <source>File Size</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="454"/>
        <source>New...</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="248"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="332"/>
        <source>Open in New Window</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠴᠤᠩᠬᠤᠨ ᠳᠡᠭᠡᠷ᠎ᠡ ᠨᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="256"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="340"/>
        <source>Open in New Tab</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠱᠤᠰᠢᠭ᠎ᠠ ᠵᠢᠨ ᠨᠢᠭᠤᠷ ᠳᠡᠭᠡᠷ᠎ᠡ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="301"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="347"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="399"/>
        <source>Open</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="311"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="358"/>
        <source>Open with...</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ ᠠᠷᠭ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="325"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="393"/>
        <source>More applications...</source>
        <translation>ᠤᠯᠠᠮ ᠤᠯᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠡ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="407"/>
        <source>Open %1 selected files</source>
        <translation>%1 ᠰᠤᠩᠭᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="547"/>
        <source>Empty File</source>
        <translation>ᠬᠤᠭᠤᠰᠤᠨ ᠲᠸᠺᠰᠲ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="559"/>
        <source>Folder</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="614"/>
        <source>Modified Date</source>
        <translation>ᠵᠠᠰᠠᠭᠰᠠᠨ ᠡᠳᠦᠷ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="617"/>
        <source>Orignal Path</source>
        <translation>ᠤᠭ ᠵᠠᠮ ᠱᠤᠭᠤᠮ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="642"/>
        <source>Sort Order...</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="649"/>
        <source>Ascending Order</source>
        <translation>ᠦᠭᠰᠦᠬᠦ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="648"/>
        <source>Descending Order</source>
        <translation>ᠪᠠᠭᠤᠷᠠᠬᠤ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="662"/>
        <source>Sort Preferences...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="666"/>
        <source>Folder First</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ ᠳᠡᠷᠢᠬᠦᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="675"/>
        <source>Chinese First</source>
        <translation>ᠬᠢᠳᠠᠳ ᠬᠡᠯᠡ ᠳᠡᠷᠢᠬᠦᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="684"/>
        <source>Show Hidden</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠠᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="719"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="726"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="835"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="873"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1037"/>
        <source>Copy</source>
        <translation>ᠺᠤᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1093"/>
        <source>File:&quot;%1&quot; is not exist, did you moved or deleted it?</source>
        <translation>&quot;%1&quot; ᠹᠠᠢᠯ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ᠂ ᠲᠠ ᠬᠠᠰᠤᠬᠤ ᠪᠤᠶᠤ ᠡᠰᠡᠪᠡᠯ ᠦᠭᠡᠷ᠎ᠡ ᠭᠠᠵᠠᠷ ᠰᠢᠯᠵᠢᠬᠦᠯᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1113"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1122"/>
        <source>Peony-Qt filesafe menu Extension</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠳᠠ ᠵᠢᠨ ᠬᠠᠢᠷᠴᠠᠭ ᠢ᠋ ᠳᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="vanished">复制(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="739"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1041"/>
        <source>Cut</source>
        <translation>ᠬᠠᠢᠴᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="762"/>
        <source>Delete to trash</source>
        <translation>ᠬᠠᠰᠤᠵᠤ ᠬᠤᠭᠯᠠᠭᠤᠷ ᠲᠤ᠌ ᠤᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="807"/>
        <source>Paste</source>
        <translation>ᠨᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="847"/>
        <source>Refresh</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="857"/>
        <source>Select All</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="907"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="943"/>
        <source>Properties</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="980"/>
        <source>format</source>
        <translation>ᠹᠤᠷᠲᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1018"/>
        <source>Restore</source>
        <translation>ᠪᠤᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="773"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="830"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1026"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1092"/>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <source>File:&quot;%1 is not exist, did you moved or deleted it?</source>
        <translation type="vanished">文件：&quot;%1&quot; 不存在，您是否已经移动或者删除了它？</translation>
    </message>
    <message>
        <source>File original path not exist, are you deleted or moved it?</source>
        <translation type="vanished">文件原始路径未找到，您是否已经移动或删除了它？</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="vanished">剪切(&amp;T)</translation>
    </message>
    <message>
        <source>&amp;Delete to trash</source>
        <translation type="vanished">删除到回收站(&amp;D)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="775"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="785"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="792"/>
        <source>Delete forever</source>
        <translation>ᠦᠨᠢᠳᠡ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="799"/>
        <source>Rename</source>
        <translation>ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠬᠦ</translation>
    </message>
    <message>
        <source>Select &amp;All</source>
        <translation type="vanished">全选(&amp;A)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="865"/>
        <source>Reverse Select</source>
        <translation>ᠤᠷᠪᠠᠭᠤ ᠪᠡᠷ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <source>P&amp;roperties</source>
        <translation type="vanished">属性(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">删除(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="vanished">重命名(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="vanished">粘贴(&amp;P)</translation>
    </message>
    <message>
        <source>&amp;Refresh</source>
        <translation type="vanished">刷新(&amp;R)</translation>
    </message>
    <message>
        <source>&amp;Properties</source>
        <translation type="vanished">属性(&amp;P)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1004"/>
        <source>&amp;Clean the Trash</source>
        <translation>ᠬᠤᠭᠯᠠᠭᠤᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ(&amp;C)</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1008"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1029"/>
        <source>Delete Permanently</source>
        <translation>ᠦᠨᠢᠳᠡ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1008"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1029"/>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation>ᠲᠠ ᠨᠡᠬᠡᠷᠡᠨ ᠡᠳᠡᠭᠡᠷ ᠹᠠᠢᠯ ᠢ᠋ ᠬᠠᠰᠤᠵᠤ ᠬᠠᠶᠠᠬᠤ ᠤᠤ? ᠬᠠᠰᠤᠵᠤ ᠡᠬᠢᠯᠡᠭᠰᠡᠬᠡᠷ ᠡᠳᠡᠭᠡᠷ ᠹᠠᠢᠯ ᠢ᠋ ᠳᠠᠬᠢᠵᠤ ᠰᠡᠷᠬᠦᠬᠡᠵᠤ ᠳᠡᠢᠯᠬᠦ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <source>&amp;Restore</source>
        <translation type="vanished">还原(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1048"/>
        <source>Clean All</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1064"/>
        <source>Open Parent Folder in New Window</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠴᠤᠩᠬᠤᠨ ᠳᠡᠭᠡᠷ᠎ᠡ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠪᠠᠢᠬᠤ ᠭᠠᠷᠴᠠᠭ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryViewWidget</name>
    <message>
        <source>Directory View</source>
        <translation type="vanished">文件视图</translation>
    </message>
</context>
<context>
    <name>Peony::FMWindow</name>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="92"/>
        <source>File Manager</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="170"/>
        <source>advanced search</source>
        <translation>ᠦᠨᠳᠦᠷ ᠳᠡᠰ ᠤ᠋ᠨ ᠬᠠᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="173"/>
        <source>clear record</source>
        <translation>ᠲᠡᠤᠬᠡ ᠵᠢ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="278"/>
        <source>Loaing... Press Esc to stop a loading.</source>
        <translation>ᠠᠴᠢᠶᠠᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...Esc ᠳᠠᠷᠤᠭᠤᠯ ᠢ᠋ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠦᠬᠡᠢᠰᠬᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="394"/>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation>ᠵᠤᠬᠢᠶᠠᠭᠴᠢ:
Yue Lan &lt;lanyue@kylinos.cn&gt;
Meihong He &lt;hemeihong@kylinos.cn&gt;

 ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠡᠷᠬᠡ ᠨᠢ(C): 2019-2020, ᠲᠢᠶᠠᠨᠵᠢᠨ ᠤᠤ ᠴᠢ ᠯᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠵᠢᠨ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠬᠢᠵᠠᠭᠠᠷᠳᠤ ᠺᠤᠮᠫᠠᠨᠢ.</translation>
    </message>
    <message>
        <source>Author: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <source>Ctrl+H</source>
        <comment>Show|Hidden</comment>
        <translation type="vanished">Ctrl+H</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="323"/>
        <source>Undo</source>
        <translation>ᠪᠤᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="330"/>
        <source>Redo</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠬᠢᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="393"/>
        <source>Peony Qt</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠤᠷ</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019-2020,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="447"/>
        <source>New Folder</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>Peony::FileCopy</name>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="145"/>
        <location filename="../../libpeony-qt/file-copy.cpp" line="153"/>
        <location filename="../../libpeony-qt/file-copy.cpp" line="164"/>
        <source>Error in source or destination file path!</source>
        <translation>ᠡᠬᠢ ᠡᠬᠦᠰᠪᠦᠷᠢ ᠬᠠᠶᠢᠭ ᠡᠰᠡᠬᠦᠯ᠎ᠡ ᠬᠠᠷᠠᠯᠳᠠᠳᠤ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠵᠠᠮ ᠱᠤᠭᠤᠮ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="190"/>
        <source>The dest file &quot;%1&quot; has existed!</source>
        <translation>ᠬᠠᠷᠠᠯᠳᠠᠳᠤ ᠹᠠᠢᠯ &quot;%1&quot; ᠨᠢᠭᠡᠨᠳᠡ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="219"/>
        <source>Vfat/FAT32 file systems do not support a single file that occupies more than 4 GB space!</source>
        <translation>Vfat/FAT32 ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠰᠢᠰᠲ᠋ᠧᠮ ᠲᠠᠩ ᠭᠠᠭᠴᠠ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠡᠵᠡᠯᠡᠬᠦ ᠤᠷᠤᠨ ᠵᠠᠢ4 GB ᠡᠴᠡ ᠶᠡᠬᠡ ᠪᠠᠢᠬᠤ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="260"/>
        <source>Error opening source or destination file!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="309"/>
        <source>Please check whether the device has been removed!</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠭᠳᠡᠭᠰᠡᠨ ᠡᠰᠡᠬᠦ ᠵᠢ ᠰᠢᠯᠭᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="311"/>
        <source>Write file error: There is no avaliable disk space for device!</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠪᠢᠴᠢᠵᠤ ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠨᠢ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ: ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠳᠡᠭᠡᠷ᠎ᠡ ᠬᠦᠷᠦᠯᠴᠡᠬᠡ ᠲᠠᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠤᠷᠤᠨ ᠵᠠᠢ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <source>Please confirm that the device controls are insufficient!</source>
        <translation type="vanished">请确认设备空间是否足够!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="366"/>
        <source>File opening failure</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Reading and Writing files are inconsistent!</source>
        <translation type="vanished">读和写文件不一致！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="326"/>
        <source>operation cancel</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>Peony::FileCopyOperation</name>
    <message>
        <source>File copy</source>
        <translation type="vanished">文件复制</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="171"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="366"/>
        <source>File copy error</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠺᠤᠫᠢᠳᠠᠭᠰᠠᠨ ᠨᠢ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="374"/>
        <source>Cannot opening file, permission denied!</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ᠂ ᠡᠷᠬᠡ ᠬᠦᠷᠬᠦ ᠦᠬᠡᠢ!</translation>
    </message>
</context>
<context>
    <name>Peony::FileDeleteOperation</name>
    <message>
        <source>File delete</source>
        <translation type="vanished">文件删除</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="75"/>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="101"/>
        <source>File delete error</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠬᠠᠰᠤᠭᠰᠠᠨ ᠨᠢ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="140"/>
        <source>Delete file error</source>
        <translation>ᠬᠠᠰᠤᠭᠰᠠᠨ ᠨᠢ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="143"/>
        <source>Invalid Operation! Can not delete &quot;%1&quot;.</source>
        <translation>ᠳᠦᠷᠢᠮ ᠵᠦᠷᠢᠴᠡᠭᠰᠡᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ!&quot;%1&quot; ᠢ᠋/ ᠵᠢ ᠬᠠᠰᠤᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
</context>
<context>
    <name>Peony::FileEnumerator</name>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="563"/>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <source>Did not find target path, do you move or deleted it?</source>
        <translation type="vanished">未找到目标路径，您是否已经移动或删除了它？</translation>
    </message>
</context>
<context>
    <name>Peony::FileInfo</name>
    <message>
        <location filename="../../libpeony-qt/file-info.cpp" line="291"/>
        <source>data</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
</context>
<context>
    <name>Peony::FileInfoJob</name>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="268"/>
        <source>Trash</source>
        <translation>ᠬᠤᠭᠯᠠᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="270"/>
        <source>Computer</source>
        <translation>ᠺᠤᠮᠫᠢᠦᠲᠸᠷ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="272"/>
        <source>Network</source>
        <translation>ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="274"/>
        <source>Recent</source>
        <translation>ᠰᠡᠬᠦᠯ ᠤ᠋ᠨ / ᠤᠢᠷ᠎ᠠ ᠵᠢᠨ</translation>
    </message>
</context>
<context>
    <name>Peony::FileInformationLabel</name>
    <message>
        <source>File location:</source>
        <translation type="vanished">文件位置：</translation>
    </message>
    <message>
        <source>File size:</source>
        <translation type="vanished">文件大小：</translation>
    </message>
    <message>
        <source>Modify time:</source>
        <translation type="vanished">修改时间：</translation>
    </message>
</context>
<context>
    <name>Peony::FileItem</name>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="225"/>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="284"/>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="296"/>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="301"/>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="272"/>
        <source>Open Link failed</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="273"/>
        <source>File not exist, do you want to delete the link file?</source>
        <translation>ᠬᠠᠷᠠᠯᠳᠠᠳᠤ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ, ᠡᠨᠡᠬᠦ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠲᠦᠳᠡ ᠠᠷᠭ᠎ᠠ ᠵᠢ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="285"/>
        <source>Can not open path &quot;%1&quot;，permission denied.</source>
        <translation>&quot;%1&quot; ᠵᠠᠮ ᠱᠤᠭᠤᠮ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠡᠷᠬᠡ ᠳᠡᠪᠴᠢᠭᠳᠡᠪᠡ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="295"/>
        <source>Can not find path &quot;%1&quot;，are you moved or renamed it?</source>
        <translation>&quot;%1&quot; ᠵᠠᠮ ᠱᠤᠭᠤᠮ ᠢ᠋ ᠡᠷᠢᠵᠤ ᠤᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠲᠠ ᠨᠢᠭᠡᠨᠳᠡ ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠭᠰᠡᠨ ᠶᠤᠮ ᠤᠤ ᠡᠰᠡᠪᠡᠯ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠳᠦᠭᠰᠡᠨ ᠶᠤᠮ ᠤᠤ?</translation>
    </message>
    <message>
        <source>Can not find path &quot;%1&quot; .</source>
        <translation type="vanished">找不到路径: &quot;%1&quot; 。</translation>
    </message>
</context>
<context>
    <name>Peony::FileItemModel</name>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="308"/>
        <source>child(ren)</source>
        <translation>ᠰᠠᠯᠠᠭ᠎ᠠ ᠵᠢᠨ ᠳᠦᠷᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="296"/>
        <source>Symbol Link, </source>
        <translation>ᠲᠦᠳᠡ ᠠᠷᠭ᠎ᠠ, </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="342"/>
        <source>File Name</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="346"/>
        <source>Delete Date</source>
        <translation>ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠵᠢ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="351"/>
        <source>File Size</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="353"/>
        <source>Original Path</source>
        <translation>ᠤᠭ ᠤ᠋ᠨ ᠵᠠᠮ ᠱᠤᠭᠤᠮ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="349"/>
        <source>File Type</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="347"/>
        <source>Modified Date</source>
        <translation>ᠵᠠᠰᠠᠭᠰᠠᠨ ᠡᠳᠦᠷ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>Peony::FileLabelInternalMenuPlugin</name>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="181"/>
        <source>Add File Label...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="204"/>
        <source>Delete All Label</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ ᠳᠡᠮᠳᠡᠭ ᠢ᠋ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <source>Peony File Labels Menu Extension</source>
        <translation type="vanished">文件标记</translation>
    </message>
    <message>
        <source>Tag a File with Menu.</source>
        <translation type="vanished">菜单中增加标记功能.</translation>
    </message>
</context>
<context>
    <name>Peony::FileLauchDialog</name>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="47"/>
        <source>Applications</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠬᠡᠨ ᠤ᠋ ᠫᠡᠷᠦᠭᠷᠡᠮ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="48"/>
        <source>Choose an Application to open this file</source>
        <translation>ᠨᠢᠭᠡ ᠬᠡᠷᠡᠭᠯᠡᠬᠡ ᠵᠢ ᠰᠤᠩᠭᠤᠵᠤ ᠡᠨᠡᠬᠦ ᠹᠠᠢᠯ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="56"/>
        <source>Set as Default</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="64"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="65"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>Peony::FileLaunchAction</name>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="144"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="251"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="444"/>
        <source>Execute Directly</source>
        <translation>ᠰᠢᠭ᠋ᠤᠳ ᠶᠠᠪᠤᠭᠳᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="145"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="252"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="445"/>
        <source>Execute in Terminal</source>
        <translation>ᠦᠵᠦᠬᠦᠷ ᠲᠤ᠌ ᠶᠠᠪᠤᠭᠳᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="148"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="256"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="449"/>
        <source>Detected launching an executable file %1, you want?</source>
        <translation>ᠶᠠᠭ ᠨᠢᠭᠡ ᠬᠦᠢᠴᠡᠳᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠹᠠᠢᠯ%1 ᠢ᠋/ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ, ᠲᠠ ᠶᠠᠭᠤ ᠵᠢ ᠬᠦᠰᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ?</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="166"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="289"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="482"/>
        <source>Open Failed</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="166"/>
        <source>Can not open %1, file not exist, is it deleted?</source>
        <translation>%1 ᠢ᠋/ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ, ᠹᠠᠢᠯ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ, ᠨᠢᠭᠡᠨᠳᠡ ᠬᠠᠰᠤᠭᠳᠠᠭᠰᠠᠨ ᠡᠰᠡᠬᠦ ᠵᠢ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <source>File not exist, is it deleted or moved to other path?</source>
        <translation type="vanished">文件不存在，您是否已将其删除或挪动位置？</translation>
    </message>
    <message>
        <source>Can not open %1</source>
        <translation type="vanished">不能打开%1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="250"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="443"/>
        <source>By Default App</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠨᠡᠬᠡᠬᠡᠬᠦ ᠠᠷᠭ᠎ᠠ ᠵᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="255"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="448"/>
        <source>Launch Options</source>
        <translation>ᠬᠦᠢᠴᠡᠳᠬᠡᠬᠦ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="279"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="472"/>
        <source>Open Link failed</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="280"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="473"/>
        <source>File not exist, do you want to delete the link file?</source>
        <translation>ᠬᠠᠷᠠᠯᠳᠠᠳᠤ ᠹᠠᠢᠯ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ, ᠲᠠ ᠳᠤᠰ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠵᠢ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="290"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="483"/>
        <source>Can not open %1, Please confirm you have the right authority.</source>
        <translation>%1 ᠢ᠋/ ᠵᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ, ᠲᠠ ᠡᠷᠬᠡ ᠵᠢ ᠵᠦᠪ ᠨᠡᠬᠡᠬᠡᠭᠰᠡᠨ ᠡᠰᠡᠬᠦ ᠵᠢ ᠰᠢᠯᠭᠠᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="294"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="487"/>
        <source>Open App failed</source>
        <translation>ᠲᠦᠳᠡ ᠠᠷᠭ᠎ᠠ ᠠᠰᠠᠭᠤᠳᠠᠯ ᠲᠠᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="295"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="488"/>
        <source>The linked app is changed or uninstalled, so it can not work correctly. 
Do you want to delete the link file?</source>
        <translation>ᠳᠤᠰ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠵᠢᠨ ᠵᠢᠭᠠᠭᠰᠠᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠡ ᠨᠢᠭᠡᠨᠳᠡ ᠵᠠᠰᠠᠭᠳᠠᠭᠰᠠᠨ ᠪᠤᠶᠤ ᠪᠠᠭᠤᠯᠭᠠᠭᠳᠠᠪᠠ, ᠡᠢᠮᠤ ᠡᠴᠡ ᠳᠤᠰ ᠲᠦᠳᠡ ᠠᠷᠭ᠎ᠠ ᠬᠡᠪ ᠤ᠋ᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ.
ᠳᠤᠰ ᠲᠦᠳᠡ ᠠᠷᠭ᠎ᠠ ᠵᠢ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="305"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="498"/>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="306"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="498"/>
        <source>Can not get a default application for opening %1, do you want open it with text format?</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠨᠡᠬᠡᠬᠡᠬᠦ%1 ᠤ᠋ᠨ/ ᠵᠢᠨ ᠬᠡᠷᠡᠭᠯᠡᠬᠡ ᠵᠢ ᠡᠷᠢᠵᠤ ᠤᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ, ᠲᠸᠺᠰᠲ ᠤ᠋ᠨ ᠨᠠᠢᠷᠠᠭᠤᠯᠤᠭᠤᠷ ᠢ᠋ ᠬᠡᠷᠡᠭᠯᠡᠵᠤ ᠨᠡᠬᠡᠬᠡᠬᠦ ᠤᠤ?</translation>
    </message>
</context>
<context>
    <name>Peony::FileLinkOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-link-operation.cpp" line="39"/>
        <location filename="../../libpeony-qt/file-operation/file-link-operation.cpp" line="42"/>
        <source>Symbolic Link</source>
        <translation>ᠲᠦᠳᠡ ᠠᠷᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-link-operation.cpp" line="76"/>
        <source>Link file error</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠵᠢ ᠪᠠᠢᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Link file</source>
        <translation type="vanished">创建文件链接</translation>
    </message>
</context>
<context>
    <name>Peony::FileMoveOperation</name>
    <message>
        <source>Invalid move operation, cannot move a file itself.</source>
        <translation type="vanished">非法的移动操作，不能自移动到自身。</translation>
    </message>
    <message>
        <source>Move file</source>
        <translation type="vanished">文件移动</translation>
    </message>
    <message>
        <source>Create file</source>
        <translation type="vanished">文件创建</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="144"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="297"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="417"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="668"/>
        <source>Move file error</source>
        <translation>ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠭᠰᠡᠨ ᠹᠠᠢᠯ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="817"/>
        <source>Create file error</source>
        <translation>ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1090"/>
        <source>Invalid Operation.</source>
        <translation>ᠳᠦᠷᠢᠮ ᠳ᠋ᠤ᠌ ᠨᠡᠢᠴᠡᠬᠦ ᠦᠬᠡᠢ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1105"/>
        <source>File delete error</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠬᠠᠰᠤᠭᠰᠠᠨ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <source>File delete</source>
        <translation type="vanished">文件删除</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1107"/>
        <source>Invalid Operation</source>
        <translation>ᠳᠦᠷᠢᠮ ᠳ᠋ᠤ᠌ ᠨᠡᠢᠴᠡᠬᠦ ᠦᠬᠡᠢ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationAfterProgressPage</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="362"/>
        <source>&amp;More Details</source>
        <translation>ᠨᠠᠷᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ(&amp;M)</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialog</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="45"/>
        <source>File Operation Error</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="53"/>
        <source>unkwon</source>
        <translation>ᠦᠯᠦ ᠮᠡᠳᠡᠬᠦ ᠰᠢᠯᠳᠠᠭᠠᠨ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="54"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="55"/>
        <source>null</source>
        <translation>ᠬᠤᠭᠤᠰᠤᠨ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="57"/>
        <source>Error message:</source>
        <translation>ᠪᠤᠷᠤᠭᠤ ᠮᠡᠳᠡᠬᠡᠷ ᠵᠠᠩᠬᠢ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="58"/>
        <source>Source File:</source>
        <translation>ᠡᠬᠢ ᠡᠬᠦᠰᠪᠦᠷᠢ ᠹᠠᠢᠯ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="59"/>
        <source>Dest File:</source>
        <translation>ᠬᠠᠷᠠᠯᠳᠠᠳᠤ ᠹᠠᠢᠯ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="63"/>
        <source>Ignore</source>
        <translation>ᠤᠮᠳᠤᠭᠠᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="64"/>
        <source>Ignore All</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠤᠮᠳᠤᠭᠠᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="65"/>
        <source>Overwrite</source>
        <translation>ᠪᠦᠷᠬᠦᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="66"/>
        <source>Overwrite All</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠪᠦᠷᠬᠦᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="67"/>
        <source>Backup</source>
        <translation>ᠨᠦᠬᠡᠴᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="68"/>
        <source>Backup All</source>
        <translation>ᠪᠦᠬᠦᠨ ᠢ᠋ ᠨᠦᠬᠡᠴᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="69"/>
        <source>&amp;Retry</source>
        <translation>ᠳᠠᠬᠢᠨ ᠳᠤᠷᠰᠢᠬᠤ(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="70"/>
        <source>&amp;Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ(&amp;C)</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogConflict</name>
    <message>
        <source>This location already contains a file with the same name.</source>
        <translation type="vanished">目标文件夹里已经包含有同名文件</translation>
    </message>
    <message>
        <source>Please select the file to keep</source>
        <translation type="vanished">请选择要保留的文件</translation>
    </message>
    <message>
        <source>This location already contains the file,</source>
        <translation type="vanished">这里已包含此文件</translation>
    </message>
    <message>
        <source>Do you want to override it?</source>
        <translation type="vanished">你确定要覆盖它吗</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="42"/>
        <source>Replace</source>
        <translation>ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="51"/>
        <source>Ignore</source>
        <translation>ᠤᠮᠳᠤᠭᠠᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="69"/>
        <source>Do the same</source>
        <translation>ᠪᠦᠬᠦ ᠬᠡᠷᠡᠭᠯᠡᠬᠡ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="92"/>
        <source>&lt;p&gt;This location already contains the file &apos;%1&apos;, Do you want to override it?&lt;/p&gt;</source>
        <translation>&lt;p&gt; ᠳᠤᠰ ᠪᠠᠢᠷᠢᠨ ᠳ᠋ᠤ᠌ ᠨᠢᠭᠡᠨᠳᠡ&apos;%1&apos; ᠨᠡᠷᠡᠢᠳᠦᠯ ᠲᠠᠢ ᠹᠠᠢᠯ ᠠᠭᠤᠯᠠᠭᠳᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ, ᠲᠠ ᠳᠡᠬᠦᠨ ᠢ᠋ ᠰᠤᠯᠢᠬᠤ ᠤᠤ?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="98"/>
        <source>Unexpected error from %1 to %2</source>
        <translation>%1 ᠡᠴᠡ %2 ᠬᠦᠷᠬᠦ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ</translation>
    </message>
    <message>
        <source>Then do the same thing in a similar situation</source>
        <translation type="vanished">之后类似情况执行相同操作</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="60"/>
        <source>Backup</source>
        <translation>ᠨᠦᠬᠡᠴᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogNotSupported</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="280"/>
        <source>Yes</source>
        <translation>ᠮᠦᠨ（ᠳᠡᠢᠮᠤ）</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="288"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="296"/>
        <source>Do the same</source>
        <translation>ᠪᠦᠬᠦ ᠬᠡᠷᠡᠭᠯᠡᠬᠡ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="325"/>
        <source>Make sure the disk is not full or write protected and that the file is not protected</source>
        <translation>ᠰᠤᠷᠢᠨᠵᠢᠨ ᠳ᠋ᠢᠰᠺ ᠳᠦᠬᠦᠷᠡᠬᠡ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠳᠠ ᠵᠢ ᠪᠢᠴᠢᠭᠰᠡᠨ ᠦᠬᠡᠢ, ᠹᠠᠢᠯ ᠪᠠᠰᠠ ᠬᠡᠷᠡᠭᠯᠡᠭᠳᠡᠭᠡ ᠦᠬᠡᠢ ᠵᠢ ᠪᠠᠳᠤᠯᠠᠭᠠᠷᠠᠢ</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogWarning</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="181"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="189"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="214"/>
        <source>Make sure the disk is not full or write protected and that the file is not protected</source>
        <translation>ᠰᠤᠷᠢᠨᠵᠢᠨ ᠳ᠋ᠢᠰᠺ ᠳᠦᠬᠦᠷᠡᠬᠡ ᠦᠬᠡᠢ ᠪᠤᠶᠤ ᠬᠠᠮᠠᠭᠠᠯᠠᠯᠳᠠ ᠵᠢ ᠪᠢᠴᠢᠭᠰᠡᠨ ᠦᠬᠡᠢ, ᠹᠠᠢᠯ ᠪᠠᠰᠠ ᠬᠡᠷᠡᠭᠯᠡᠭᠳᠡᠭᠡ ᠦᠬᠡᠢ ᠵᠢ ᠪᠠᠳᠤᠯᠠᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Please make sure the disk is not full or not is write protected, or file is not being used.</source>
        <translation type="vanished">请确保磁盘未满或未被写保护或未被使用。</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationInfo</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="718"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="720"/>
        <source>Symbolic Link</source>
        <translation>ᠲᠦᠳᠡ ᠠᠷᠭ᠎ᠠ</translation>
    </message>
    <message>
        <source> - Symbolic Link</source>
        <translation type="vanished">-快捷方式</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationManager</name>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="189"/>
        <source>Warn</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="189"/>
        <source>&apos;%1&apos; is occupied，you cannot operate!</source>
        <translation>&apos;%1&apos; ᠨᠢᠭᠡᠨᠳᠡ ᠡᠵᠡᠯᠡᠭᠳᠡᠭᠰᠡᠨ, ᠲᠠ ᠠᠵᠢᠯᠯᠠᠭᠤᠯᠬᠤ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="208"/>
        <source>No, go to settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="209"/>
        <source>Do you want to put selected %1 item(s) into trash?</source>
        <translation>ᠰᠤᠩᠭᠤᠭᠳᠠᠭᠰᠠᠨ%1 ᠳᠦᠷᠦᠯ ᠢ᠋ ᠯᠠᠪᠳᠠᠢ ᠬᠤᠭᠯᠠᠭᠤᠷ ᠲᠤ᠌ ᠬᠢᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="296"/>
        <source>Can&apos;t delete.</source>
        <translation>ᠬᠠᠰᠤᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="297"/>
        <source>You can&apos;t delete a file whenthe file is doing another operation</source>
        <translation>ᠶᠠᠭ ᠪᠤᠰᠤᠳ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠶᠠᠪᠤᠭᠳᠠᠭᠤᠯᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠹᠠᠢᠯ ᠢ᠋ ᠬᠠᠰᠤᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="374"/>
        <source>File Operation is Busy</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠠᠪ ᠦᠬᠡᠢ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="375"/>
        <source>There have been one or more fileoperation(s) executing before. Youroperation will wait for executinguntil it/them done. If you really want to execute file operations parallelly anyway, you can change the default option &quot;Allow Parallel&quot; in option menu.</source>
        <translation>ᠳᠤᠰ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠬᠦᠢᠴᠡᠳᠬᠡᠬᠦ ᠡᠴᠡ ᠡᠮᠦᠨ᠎ᠡ ᠦᠬᠡᠷ᠎ᠡ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠳᠠᠭᠤᠰᠬᠠᠭ᠎ᠠ ᠦᠬᠡᠢ ᠪᠠᠢᠨ᠎ᠠ, ᠡᠨᠡ ᠨᠢ ᠳᠡᠭᠡᠷ᠎ᠡ ᠨᠢᠭᠡ ᠤᠳᠠᠭᠠᠨ ᠤ᠋ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠳᠠᠭᠤᠰᠬᠠᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠰᠠᠶᠢ ᠬᠦᠢᠴᠡᠳᠬᠡᠭᠳᠡᠨ᠎ᠡ, ᠲᠠ ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ ᠤ᠋ ᠲᠤᠪᠶᠤᠭ ᠲᠡᠬᠢ&quot; ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠵᠤ ᠬᠦᠢᠴᠡᠳᠬᠡᠬᠦ&quot; ᠭᠡᠬᠦ ᠳᠠᠷᠤᠭᠤᠯ ᠢ᠋ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠪᠠᠢᠷᠢᠯᠠᠭᠤᠯᠤᠯᠳᠠ ᠵᠢ ᠦᠭᠡᠷᠡᠴᠢᠯᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ.</translation>
    </message>
    <message>
        <source>The system cannot hibernate or sleep</source>
        <translation type="vanished">无法进入休眠或睡眠模式</translation>
    </message>
    <message>
        <source>The file operation is in progress.                                         Ensure that the file operation is complete or canceled before hibernating or sleeping</source>
        <translation type="vanished">文件操作进行中,\
进入休眠或者睡眠之前，请先确保文件操作已完成或者取消</translation>
    </message>
    <message>
        <source>There have been one or more fileoperation(s) executing before. Youroperation will wait for executinguntil it/them done.</source>
        <translation type="vanished">在执行该操作之前有操作未完成，它需要等待上一个操作完成后再执行。</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationPreparePage</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="298"/>
        <source>counting:</source>
        <translation>ᠨᠡᠢᠳᠡ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="299"/>
        <source>state:</source>
        <translation>ᠪᠠᠢᠳᠠᠯ:</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationProgressPage</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="321"/>
        <source>&amp;More Details</source>
        <translation>ᠨᠠᠷᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ(&amp;M)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="332"/>
        <source>From:</source>
        <translation>ᠡᠨᠳᠡ ᠡᠴᠡ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="333"/>
        <source>To:</source>
        <translation>ᠬᠦᠷᠬᠦ:</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationProgressWizard</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="55"/>
        <source>File Manager</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="59"/>
        <source>&amp;Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="68"/>
        <source>Preparing...</source>
        <translation>ᠪᠡᠯᠡᠳᠬᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="71"/>
        <source>Handling...</source>
        <translation>ᠶᠠᠭ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="74"/>
        <source>Clearing...</source>
        <translation>ᠶᠠᠭ ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="77"/>
        <source>Rollbacking...</source>
        <translation>ᠪᠤᠴᠠᠭᠠᠳ ᠦᠩᠬᠦᠷᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="81"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="94"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="120"/>
        <source>File Operation</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="95"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="121"/>
        <source>A file operation is running backend...</source>
        <translation>ᠨᠢᠭᠡ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠤᠳᠤ ᠶᠠᠭ ᠠᠷᠤ ᠵᠢᠨ ᠳᠠᠪᠴᠠᠩ ᠳ᠋ᠤ᠌ ᠶᠠᠪᠤᠭᠳᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="160"/>
        <source>%1 files, %2</source>
        <translation>%1 ᠹᠠᠢᠯ, ᠨᠡᠢᠳᠡ %2</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="260"/>
        <source>%1 done, %2 total, %3 of %4.</source>
        <translation>ᠬᠦᠢᠴᠡᠳᠬᠡᠭᠰᠡᠨ%1 ᠨᠡᠢᠳᠡ%2，%4 ᠳᠤᠮᠳᠠᠬᠢ%3 ᠳ᠋ᠤᠭᠠᠷ/ ᠳ᠋ᠦ᠍ᠭᠡᠷ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="203"/>
        <source>clearing: %1, %2 of %3</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ: %1,%3 ᠳᠤᠮᠳᠠᠬᠢ %2 ᠳ᠋ᠤᠭᠠᠷ/ ᠳ᠋ᠦ᠍ᠭᠡᠷ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="248"/>
        <source>copying...</source>
        <translation>ᠺᠤᠫᠢᠳᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="278"/>
        <source>Syncing...</source>
        <translation>ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
</context>
<context>
    <name>Peony::FilePreviewPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="218"/>
        <source>File Name:</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="223"/>
        <source>File Type:</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="227"/>
        <source>Time Access:</source>
        <translation>ᠠᠢᠯᠴᠢᠯᠠᠭᠰᠠᠨ ᠴᠠᠭ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="231"/>
        <source>Time Modified:</source>
        <translation>ᠵᠠᠰᠠᠭᠰᠠᠨ ᠴᠠᠭ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="237"/>
        <source>Children Count:</source>
        <translation>ᠪᠠᠭᠠᠳᠠᠬᠤ ᠹᠠᠢᠯ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="242"/>
        <source>Size:</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="247"/>
        <source>Image resolution:</source>
        <translation>ᠢᠯᠭᠠᠮᠵᠢ ᠵᠢᠨ ᠬᠡᠮᠵᠢᠶ᠎ᠡ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="251"/>
        <source>color model:</source>
        <translation>ᠦᠩᠬᠡ ᠵᠢᠨ ᠵᠠᠭᠪᠤᠷ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="312"/>
        <source>usershare</source>
        <translation>ᠳᠤᠰ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Image size:</source>
        <translation type="vanished">图片尺寸：</translation>
    </message>
    <message>
        <source>Image format:</source>
        <translation type="vanished">图片格式：</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="343"/>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="344"/>
        <source>%1x%2</source>
        <translation>%1x%2</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="396"/>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="397"/>
        <source>%1 total, %2 hidden</source>
        <translation>ᠨᠡᠢᠳᠡ%1 ᠳᠦᠷᠦᠯ, ᠡᠬᠦᠨ ᠳ᠋ᠤ᠌%2 ᠨᠢᠭᠤᠴᠠᠯᠠᠭᠰᠠᠨ ᠹᠠᠢᠯ</translation>
    </message>
</context>
<context>
    <name>Peony::FileRenameDialog</name>
    <message>
        <source>Names automatically add serial Numbers (e.g., 1,2,3...)</source>
        <translation type="vanished">名称后自动添加序号（如:1,2,3...）</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>New file name</source>
        <translation type="vanished">文件名</translation>
    </message>
    <message>
        <source>Please enter the file name</source>
        <translation type="vanished">请输入文件名</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
</context>
<context>
    <name>Peony::FileRenameOperation</name>
    <message>
        <source>Rename file</source>
        <translation type="vanished">文件重命名</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="74"/>
        <source>File Rename error</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠳᠦᠭᠰᠡᠨ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="75"/>
        <source>Invalid file name %1%2%3 .</source>
        <translation>ᠳᠦᠷᠢᠮ ᠳ᠋ᠤ᠌ ᠨᠡᠢᠴᠡᠬᠦ ᠦᠬᠡᠢ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ%1%2%3.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="91"/>
        <source>The file %1%2%3 will be hidden when you refresh or change directory!</source>
        <translation>ᠹᠠᠢᠯ%1%2%3 ᠵᠠᠮ ᠱᠤᠭᠤᠮ ᠢ᠋ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ ᠪᠤᠶᠤ ᠰᠤᠯᠢᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠢᠭᠤᠴᠠᠯᠠᠭᠳᠠᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <source>The file %1%2%3 will be hidden when you refresh or rsort!</source>
        <translation type="vanished">文件 %1%2%3 在刷新或者排序后将会被隐藏!</translation>
    </message>
    <message>
        <source>The file %1%2%3 will be hidden!</source>
        <translation type="vanished">文件%1%2%3将被隐藏!</translation>
    </message>
    <message>
        <source>Invalid file name &quot;%1&quot; </source>
        <translation type="vanished">文件名 &quot;%1&quot; 不合法</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="90"/>
        <source>File Rename warning</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠬᠦ ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <source>The file &quot;%1&quot; will be hidden!</source>
        <translation type="vanished">文件 &quot;%1&quot; 将会被隐藏！</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="166"/>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="196"/>
        <source>Rename file error</source>
        <translation>ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠳᠦᠭᠰᠡᠨ ᠹᠠᠢᠯ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
</context>
<context>
    <name>Peony::FileTrashOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="68"/>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="91"/>
        <source>trash:///</source>
        <translation>trash:///</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="71"/>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="94"/>
        <source>Trash file error</source>
        <translation>ᠬᠠᠰᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠬᠤᠭᠯᠠᠭᠤᠷ ᠲᠤ᠌ ᠬᠦᠷᠬᠦ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="74"/>
        <source>Invalid Operation! Can not trash &quot;%1&quot;.</source>
        <translation>ᠳᠦᠷᠢᠮ ᠳ᠋ᠤ᠌ ᠨᠡᠢᠴᠡᠬᠦ ᠦᠬᠡᠢ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ!&quot;%1&quot; ᠢ᠋/ ᠵᠢ ᠬᠤᠷᠢᠶᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="102"/>
        <source>Can not trash</source>
        <translation>ᠬᠤᠷᠢᠶᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="103"/>
        <source>Can not trash files more than 10GB, would you like to delete it permanently?</source>
        <translation>10GB ᠡᠴᠡ ᠲᠤᠮᠤ ᠹᠠᠢᠯ ᠢ᠋ ᠬᠤᠷᠢᠶᠠᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ, ᠦᠨᠢᠳᠡ ᠬᠠᠰᠤᠬᠤ ᠱᠠᠭᠠᠷᠳᠠᠯᠭ᠎ᠠ ᠲᠠᠢ ᠤᠤ?</translation>
    </message>
    <message>
        <source>The user does not have read and write rights to the file &apos;%1&apos; and cannot delete it to the Recycle Bin.</source>
        <translation type="vanished">用户对当前文件 %1 没有读写权限，无法删除到回收站。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="171"/>
        <source>Can not trash this file, would you like to delete it permanently?</source>
        <translation>ᠳᠤᠰ ᠹᠠᠢᠯ ᠢ᠋ ᠬᠤᠷᠢᠶᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ, ᠦᠨᠢᠳᠡ ᠬᠠᠰᠤᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <source>Can not trash %1, would you like to delete this file permanently?</source>
        <translation type="vanished">不能回收%1, 是否永久删除?</translation>
    </message>
    <message>
        <source>. Are you sure you want to permanently delete the file</source>
        <translation type="vanished">，你确定要永久删除文件吗？</translation>
    </message>
    <message>
        <source>The user does not have read and write rights to the file &apos;%s&apos; and cannot delete it to the Recycle Bin.</source>
        <translation type="vanished">用户对当前文件 %s 没有读写权限，无法删除到回收站。</translation>
    </message>
    <message>
        <source>Trash file</source>
        <translation type="vanished">删除文件到回收站</translation>
    </message>
</context>
<context>
    <name>Peony::FileUntrashOperation</name>
    <message>
        <source>Untrash file</source>
        <translation type="vanished">撤销删除的文件</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-untrash-operation.cpp" line="157"/>
        <source>Untrash file error</source>
        <translation>ᠬᠤᠭᠯᠠᠭᠤᠷ ᠡᠴᠡ ᠹᠠᠢᠯ ᠢ᠋ ᠪᠤᠴᠠᠭᠠᠭᠰᠠᠨ ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
</context>
<context>
    <name>Peony::GlobalSettings</name>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="74"/>
        <location filename="../../libpeony-qt/global-settings.cpp" line="310"/>
        <source>yyyy/MM/dd</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="75"/>
        <location filename="../../libpeony-qt/global-settings.cpp" line="303"/>
        <source>HH:mm:ss</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="300"/>
        <source>AP hh:mm:ss</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="313"/>
        <source>yyyy-MM-dd</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Peony::LocationBar</name>
    <message>
        <source>click the blank area for edit</source>
        <translation type="vanished">点击空白区域编辑路径</translation>
    </message>
    <message>
        <source>Computer</source>
        <translation type="obsolete">计算机</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="305"/>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation>&quot;%2&quot; ᠡᠴᠡ&quot;%1&quot; ᠢ᠋/ ᠵᠢ ᠬᠠᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="333"/>
        <source>File System</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤᠨ ᠱᠢᠰᠲ᠋ᠧᠮ</translation>
    </message>
    <message>
        <source>&amp;Copy Directory</source>
        <translation type="vanished">拷贝路径(&amp;C)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="412"/>
        <source>Open In New Tab</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠱᠤᠰᠢᠭ᠎ᠠ ᠵᠢᠨ ᠨᠢᠭᠤᠷ ᠳᠡᠭᠡᠷ᠎ᠡ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="416"/>
        <source>Open In New Window</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠴᠤᠩᠬᠤᠨ ᠳᠡᠭᠡᠷ᠎ᠡ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Open In New &amp;Tab</source>
        <translation type="vanished">在新标签页中打开(&amp;T)</translation>
    </message>
    <message>
        <source>Open In &amp;New Window</source>
        <translation type="vanished">在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="410"/>
        <source>Copy Directory</source>
        <translation>ᠵᠠᠮ ᠱᠤᠭᠤᠮ ᠢ᠋ ᠺᠤᠫᠢᠳᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Peony::MountOperation</name>
    <message>
        <location filename="../../libpeony-qt/mount-operation.cpp" line="90"/>
        <source>Operation Cancelled</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠦᠬᠡᠢᠰᠬᠡᠭᠳᠡᠪᠡ</translation>
    </message>
</context>
<context>
    <name>Peony::NavigationToolBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="35"/>
        <source>Go Back</source>
        <translation>ᠤᠬᠤᠷᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="39"/>
        <source>Go Forward</source>
        <translation>ᠤᠷᠤᠭᠰᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="43"/>
        <source>History</source>
        <translation>ᠲᠡᠤᠬᠡ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="73"/>
        <source>Clear History</source>
        <translation>ᠲᠡᠤᠬᠡ ᠵᠢ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="88"/>
        <source>Cd Up</source>
        <translation>ᠦᠭᠡᠳᠡ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="94"/>
        <source>Refresh</source>
        <translation>ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>Peony::NewFileLaunchDialog</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="217"/>
        <source>Choose new application</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠨᠢᠭᠡ ᠬᠡᠷᠡᠭᠯᠡᠬᠡ ᠵᠢ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="219"/>
        <source>Choose an Application to open this file</source>
        <translation>ᠨᠢᠭᠡ ᠬᠡᠷᠡᠭᠯᠡᠬᠡ ᠵᠢ ᠰᠤᠩᠭᠤᠵᠤ ᠡᠨᠡᠬᠦ ᠹᠠᠢᠯ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="226"/>
        <source>apply now</source>
        <translation>ᠳᠠᠷᠤᠢ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="232"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="233"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>Peony::OpenWithPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="93"/>
        <source>How do you want to open %1%2 files ?</source>
        <translation>ᠲᠠ ᠶᠠᠮᠠᠷ ᠠᠷᠭ᠎ᠠ ᠪᠡᠷ%1%2 ᠹᠠᠢᠯ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ ᠬᠦᠰᠡᠯ ᠲᠠᠢ ᠪᠤᠢ?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="98"/>
        <source>Default open with:</source>
        <translation>ᠠᠶᠠᠳᠠᠯ ᠨᠡᠬᠡᠬᠡᠬᠦ ᠠᠷᠭ᠎ᠠ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="117"/>
        <source>Other:</source>
        <translation>ᠪᠤᠰᠤᠳ:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="158"/>
        <source>Choose other application</source>
        <translation>ᠪᠤᠰᠤᠳ ᠬᠡᠷᠡᠭᠯᠡᠬᠡ ᠵᠢ ᠰᠤᠩᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="174"/>
        <source>Go to application center</source>
        <translation>ᠰᠣᠹᠲ᠋ᠧᠠᠢᠢᠷ ᠤ᠋ᠨ ᠲᠦᠪ ᠲᠤ᠌ ᠤᠴᠢᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Peony::PathEdit</name>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/path-bar/path-edit.cpp" line="59"/>
        <source>Go To</source>
        <translation>ᠦᠰᠦᠷᠴᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Peony::PermissionsPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="134"/>
        <source>User or Group</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠪᠤᠶᠤ ᠬᠡᠰᠡᠭ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="134"/>
        <source>Type</source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <source>Readable</source>
        <translation type="vanished">可读</translation>
    </message>
    <message>
        <source>Writeable</source>
        <translation type="vanished">可写</translation>
    </message>
    <message>
        <source>Excuteable</source>
        <translation type="vanished">可执行</translation>
    </message>
    <message>
        <source>File: %1</source>
        <translation type="vanished">文件：%1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="80"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="156"/>
        <source>Target: %1</source>
        <translation>ᠡᠰᠡᠷᠬᠦ ᠡᠲᠡᠭᠡᠳ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ: %1</translation>
    </message>
    <message>
        <source>Read and Write</source>
        <translation type="vanished">读写</translation>
    </message>
    <message>
        <source>Readonly</source>
        <translation type="vanished">只读</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="134"/>
        <source>Read</source>
        <translation>ᠤᠩᠰᠢᠵᠤ ᠪᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="134"/>
        <source>Write</source>
        <translation>ᠪᠢᠴᠢᠵᠤ ᠪᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="134"/>
        <source>Executable</source>
        <translation>ᠬᠦᠢᠴᠡᠳᠬᠡᠵᠤ ᠪᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="178"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="187"/>
        <source>Can not get the permission info.</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠡᠷᠬᠡ ᠲᠠᠢ ᠬᠠᠮᠢᠶᠠᠳᠠᠢ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠵᠢ ᠤᠯᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="248"/>
        <source>(Me)</source>
        <translation>( ᠪᠢ)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="317"/>
        <source>Others</source>
        <translation>ᠪᠤᠰᠤᠳ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="320"/>
        <source>Owner</source>
        <translation>ᠦᠮᠴᠢᠯᠡᠭᠴᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="322"/>
        <source>Group</source>
        <translation>ᠳᠤᠭᠤᠢᠯᠠᠩ ᠬᠤᠪᠢᠶᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="324"/>
        <source>Other</source>
        <translation>ᠪᠤᠰᠤᠳ</translation>
    </message>
    <message>
        <source>Other Users</source>
        <translation type="vanished">其它用户</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="335"/>
        <source>You can not change the access of this file.</source>
        <translation>ᠲᠠ ᠳᠤᠰ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠡᠷᠬᠡ ᠵᠢ ᠵᠠᠰᠠᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="339"/>
        <source>Me</source>
        <translation>ᠪᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="343"/>
        <source>User</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ</translation>
    </message>
</context>
<context>
    <name>Peony::PropertiesWindow</name>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="275"/>
        <source>Trash</source>
        <translation>ᠬᠤᠭᠯᠠᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="279"/>
        <source>Recent</source>
        <translation>ᠰᠡᠬᠦᠯ ᠤ᠋ᠨ / ᠤᠢᠷ᠎ᠠ ᠵᠢᠨ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="287"/>
        <source>Selected</source>
        <translation>ᠰᠤᠩᠭᠤᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="287"/>
        <source> %1 Files</source>
        <translation> %1 ᠹᠠᠢᠯ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="293"/>
        <source>usershare</source>
        <translation>ᠳᠤᠰ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="300"/>
        <source>Data</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="307"/>
        <source>Properties</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="403"/>
        <source>Ok</source>
        <translation>Ok</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="404"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">关闭</translation>
    </message>
</context>
<context>
    <name>Peony::RecentAndTrashPropertiesPage</name>
    <message>
        <source>Show confirm dialog while trashing: </source>
        <translation type="vanished">删除到回收站时弹出确认框:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="117"/>
        <source>Show confirm dialog while trashing.</source>
        <translation>ᠬᠠᠰᠤᠭᠠᠳ ᠬᠤᠭᠯᠠᠭᠤᠷ ᠲᠤ᠌ ᠬᠢᠬᠦ ᠦᠶ᠎ᠡ ᠳ᠋ᠤ᠌ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠬᠦᠷᠢᠶ᠎ᠡ ᠭᠠᠷᠴᠤ ᠢᠷᠡᠬᠦ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="140"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="146"/>
        <source>Origin Path: </source>
        <translation>ᠤᠭ ᠵᠠᠮ ᠱᠤᠭᠤᠮ: </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="178"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="211"/>
        <source>Deletion Date: </source>
        <translation>ᠬᠠᠰᠤᠭᠰᠠᠨ ᠡᠳᠦᠷ ᠰᠠᠷ᠎ᠠ: </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="167"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="226"/>
        <source>Size: </source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ: </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="220"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="227"/>
        <source>Original Location: </source>
        <translation>ᠤᠭ ᠵᠠᠮ ᠱᠤᠭᠤᠮ: </translation>
    </message>
</context>
<context>
    <name>Peony::SearchBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="47"/>
        <source>Input the search key of files you would like to find.</source>
        <translation>ᠵᠠᠩᠬᠢᠯᠠᠭ᠎ᠠ ᠦᠭᠡᠰ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠪᠡᠷ ᠳᠠᠮᠵᠢᠭᠤᠯᠵᠤ ᠲᠠᠨ ᠤ᠋ ᠬᠠᠢᠬᠤ ᠬᠡᠵᠤ ᠪᠠᠢᠭ᠎ᠠ ᠹᠠᠢᠯ ᠢ᠋ ᠬᠠᠢᠭᠠᠷᠠᠢ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="83"/>
        <source>Input search key...</source>
        <translation>ᠵᠠᠩᠬᠢᠯᠠᠭ᠎ᠠ ᠦᠭᠡ ᠵᠢ ᠤᠷᠤᠭᠤᠯᠬᠤ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="121"/>
        <source>advance search</source>
        <translation>ᠦᠨᠳᠦᠷ ᠳᠡᠰ ᠤ᠋ᠨ ᠬᠠᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="122"/>
        <source>clear record</source>
        <translation>ᠲᠡᠤᠬᠡ ᠵᠢ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>Peony::SearchBarContainer</name>
    <message>
        <source>Choose File Type</source>
        <translation type="vanished">选择文件类型</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.cpp" line="70"/>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.cpp" line="200"/>
        <source>Clear</source>
        <translation>ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="88"/>
        <source>all</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ/ ᠪᠦᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="88"/>
        <source>file folder</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="88"/>
        <source>image</source>
        <translation>ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="89"/>
        <source>video</source>
        <translation>ᠸᠢᠳᠢᠤ᠋</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="89"/>
        <source>text file</source>
        <translation>ᠲᠸᠺᠰᠲ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="89"/>
        <source>audio</source>
        <translation>ᠠᠦ᠋ᠳᠢᠤ᠋</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="89"/>
        <source>others</source>
        <translation>ᠪᠤᠰᠤᠳ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="89"/>
        <source>wps file</source>
        <translation>WPS ᠹᠠᠢᠯ</translation>
    </message>
</context>
<context>
    <name>Peony::SharedFileLinkOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/shared-file-link-operation.cpp" line="44"/>
        <location filename="../../libpeony-qt/file-operation/shared-file-link-operation.cpp" line="47"/>
        <source>Symbolic Link</source>
        <translation>ᠲᠦᠳᠡ ᠠᠷᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarFavoriteItem</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-favorite-item.cpp" line="86"/>
        <source>Favorite</source>
        <translation>ᠳᠦᠷᠭᠡᠨ ᠠᠢᠯᠴᠢᠯᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarFileSystemItem</name>
    <message>
        <source>Computer</source>
        <translation type="vanished">计算机</translation>
    </message>
    <message>
        <source>File System</source>
        <translation type="vanished">文件系统</translation>
    </message>
    <message>
        <source>data</source>
        <translation type="vanished">数据盘</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="169"/>
        <source>Data</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarMenu</name>
    <message>
        <source>&amp;Properties</source>
        <translation type="vanished">属性(&amp;P)</translation>
    </message>
    <message>
        <source>P&amp;roperties</source>
        <translation type="vanished">属性(&amp;R)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="54"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="77"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="103"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="118"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="228"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="275"/>
        <source>Properties</source>
        <translation>ᠬᠠᠷᠢᠶᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="88"/>
        <source>Delete Symbolic</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="134"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="142"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="269"/>
        <source>Unmount</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="151"/>
        <source>Eject</source>
        <translation>ᠦᠰᠦᠷᠴᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <source>&amp;Delete Symbolic</source>
        <translation type="vanished">删除(&amp;D)</translation>
    </message>
    <message>
        <source>&amp;Unmount</source>
        <translation type="vanished">卸载(&amp;U)</translation>
    </message>
    <message>
        <source>&amp;Eject</source>
        <translation type="vanished">弹出(&amp;E)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="195"/>
        <source>format</source>
        <translation>ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarModel</name>
    <message>
        <source>Shared Data</source>
        <translation type="vanished">共享数据</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-model.cpp" line="98"/>
        <source>Network</source>
        <translation>ᠲᠤᠤᠷ ᠰᠦᠯᠵᠢᠶ᠎ᠡ</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarPersonalItem</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-personal-item.cpp" line="42"/>
        <source>Personal</source>
        <translation>ᠬᠤᠪᠢ ᠬᠦᠮᠦᠨ</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarSeparatorItem</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-separator-item.h" line="68"/>
        <source>(No Sub Directory)</source>
        <translation>（ᠬᠤᠭᠤᠰᠤᠨ）</translation>
    </message>
</context>
<context>
    <name>Peony::StatusBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="94"/>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="100"/>
        <source>; %1 folders</source>
        <translation>；%1 ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="95"/>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="102"/>
        <source>; %1 files, %2 total</source>
        <translation>；%1 ᠹᠠᠢᠯ，ᠨᠡᠢᠳᠡ%2</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="97"/>
        <source>; %1 folder</source>
        <translation>；%1 ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="98"/>
        <source>; %1 file, %2</source>
        <translation>；%1 ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ，%2</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="105"/>
        <source>%1 selected</source>
        <translation>ᠰᠤᠩᠭᠤᠭᠳᠠᠭᠰᠠᠨ%1 ᠳᠦᠷᠦᠯ</translation>
    </message>
</context>
<context>
    <name>Peony::SyncThread</name>
    <message>
        <location filename="../../libpeony-qt/sync-thread.cpp" line="44"/>
        <source>notify</source>
        <translatorcomment>温馨提示</translatorcomment>
        <translation>ᠳᠤᠳᠤᠨᠤ ᠰᠠᠨᠠᠭᠤᠯᠭ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>Peony::ToolBar</name>
    <message>
        <source>Open in new &amp;Window</source>
        <translation type="vanished">在新窗口中打开(&amp;W)</translation>
    </message>
    <message>
        <source>Open in &amp;New window</source>
        <translation type="vanished">在新窗口中打开(&amp;N)</translation>
    </message>
    <message>
        <source>Open in new &amp;Tab</source>
        <translation type="vanished">在新标签页中打开(&amp;T)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="138"/>
        <source>Sort Type</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠯᠠᠯ ᠤ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="140"/>
        <source>File Name</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="146"/>
        <source>File Type</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="149"/>
        <source>File Size</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="143"/>
        <source>Modified Date</source>
        <translation>ᠵᠠᠰᠠᠭᠰᠠᠨ ᠡᠳᠦᠷ ᠰᠠᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="72"/>
        <source>Open in New window</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠴᠤᠩᠬᠤᠨ ᠳᠡᠭᠡᠷ᠎ᠡ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="74"/>
        <source>Open in new Tab</source>
        <translation>ᠰᠢᠨ᠎ᠡ ᠱᠤᠰᠢᠭ᠎ᠠ ᠵᠢᠨ ᠨᠢᠭᠤᠷ ᠳᠡᠭᠡᠷ᠎ᠡ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="160"/>
        <source>Ascending</source>
        <translation>ᠦᠭᠰᠦᠬᠦ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="156"/>
        <source>Descending</source>
        <translation>ᠪᠠᠭᠠᠰᠬᠠᠬᠤ ᠳᠠᠷᠠᠭᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="190"/>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="347"/>
        <source>Copy</source>
        <translation>ᠺᠤᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="193"/>
        <source>Paste</source>
        <translation>ᠨᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="196"/>
        <source>Cut</source>
        <translation>ᠬᠠᠢᠴᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="199"/>
        <source>Trash</source>
        <translation>ᠬᠤᠭᠯᠠᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="216"/>
        <source>Clean Trash</source>
        <translation>ᠬᠤᠭᠯᠠᠭᠤᠷ ᠢ᠋ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="vanished">删除文件警告</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="218"/>
        <source>Delete Permanently</source>
        <translation>ᠦᠨᠢᠳᠡ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="218"/>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation>ᠲᠠ ᠨᠡᠬᠡᠷᠡᠨ ᠡᠳᠡᠭᠡᠷ ᠹᠠᠢᠯ ᠢ᠋ ᠬᠠᠰᠤᠵᠤ ᠬᠠᠶᠠᠬᠤ ᠤᠤ? ᠬᠠᠰᠤᠵᠤ ᠡᠬᠢᠯᠡᠭᠰᠡᠬᠡᠷ ᠡᠳᠡᠭᠡᠷ ᠹᠠᠢᠯ ᠢ᠋ ᠳᠠᠬᠢᠵᠤ ᠰᠡᠷᠬᠦᠬᠡᠵᠤ ᠳᠡᠢᠯᠬᠦ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="229"/>
        <source>Restore</source>
        <translation>ᠪᠤᠴᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="278"/>
        <source>Options</source>
        <translation>ᠰᠤᠩᠭᠤᠭᠳᠠᠬᠤᠨ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="281"/>
        <source>Forbid Thumbnail</source>
        <translation>ᠠᠪᠴᠢᠭᠤᠯᠤᠯ ᠵᠢᠷᠤᠭ ᠬᠡᠷᠡᠭᠯᠡᠬᠦ ᠵᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="288"/>
        <source>Show Hidden</source>
        <translation>ᠨᠢᠭᠤᠴᠠᠯᠠᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="295"/>
        <source>Resident in Backend</source>
        <translation>ᠠᠷᠤ ᠵᠢᠨ ᠳᠠᠪᠴᠠᠩ ᠳ᠋ᠤ᠌ ᠪᠠᠢᠬᠤ ᠰᠠᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="296"/>
        <source>Let the program still run after closing the last window. This will reduce the time for the next launch, but it will also consume resources in backend.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="308"/>
        <source>&amp;Help</source>
        <translation>ᠬᠠᠮᠵᠢᠬᠤ/ ᠳᠤᠰᠠᠯᠠᠬᠤ(&amp;H)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="314"/>
        <source>&amp;About...</source>
        <translation>ᠲᠤᠬᠠᠢ(&amp;A)...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="316"/>
        <source>Peony Qt</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="317"/>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="unfinished">ᠵᠤᠬᠢᠶᠠᠭᠴᠢ:
Yue Lan &lt;lanyue@kylinos.cn&gt;
Meihong He &lt;hemeihong@kylinos.cn&gt;

 ᠬᠡᠪᠯᠡᠯ ᠤ᠋ᠨ ᠡᠷᠬᠡ ᠨᠢ(C): 2019-2020, ᠲᠢᠶᠠᠨᠵᠢᠨ ᠤᠤ ᠴᠢ ᠯᠢᠨ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠬᠢ ᠵᠢᠨ ᠮᠡᠷᠭᠡᠵᠢᠯ ᠤ᠋ᠨ ᠬᠢᠵᠠᠭᠠᠷᠳᠤ ᠺᠤᠮᠫᠠᠨᠢ.</translation>
    </message>
    <message>
        <source>Author: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019,天津麒麟信息技术有限公司.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">作者:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

版权所有(C): 2019,天津麒麟信息技术有限公司.</translation>
    </message>
</context>
<context>
    <name>Peony::VolumeManager</name>
    <message>
        <location filename="../../libpeony-qt/volume-manager.cpp" line="150"/>
        <source>Error</source>
        <translation>ᠪᠤᠷᠤᠭᠤ</translation>
    </message>
</context>
<context>
    <name>ProgressBar</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="737"/>
        <source>starting ...</source>
        <translation>ᠶᠠᠭ ᠡᠬᠢᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="824"/>
        <source>canceling ...</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="826"/>
        <source>sync ...</source>
        <translation>ᠶᠠᠭ ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <source>cancel file operation</source>
        <translation type="vanished">取消文件操作</translation>
    </message>
    <message>
        <source>Are you sure want to cancel the current selected file operation</source>
        <translation type="vanished">你确定要取消当前选中的文件操作</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="40"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="60"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="91"/>
        <source>Icon View</source>
        <translation>ᠰᠢᠪᠠᠭ᠎ᠠ ᠵᠢᠨ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="46"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="97"/>
        <source>Show the folder children as icons.</source>
        <translation>ᠢᠺᠦᠨ ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠪᠡᠷ ᠭᠠᠷᠴᠠᠭ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="42"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="62"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="93"/>
        <source>List View</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠳ᠋ᠠᠬᠢ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="48"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="99"/>
        <source>Show the folder children as rows in a list.</source>
        <translation>ᠵᠢᠭᠰᠠᠭᠠᠯᠳᠠ ᠵᠢᠨ ᠬᠡᠯᠪᠡᠷᠢ ᠪᠡᠷ ᠭᠠᠷᠴᠠᠭ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ.</translation>
    </message>
    <message>
        <source>Basic Preview Page</source>
        <translation type="vanished">基本</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page-factory.h" line="40"/>
        <source>Basic</source>
        <translation>ᠦᠨᠳᠦᠰᠦᠨ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page-factory.h" line="46"/>
        <source>Show the basic file properties, and allow you to modify the access and name.</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠦᠨᠳᠦᠰᠦᠨ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠵᠤ, ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠢ᠋ ᠵᠠᠰᠠᠬᠤ ᠵᠢ ᠵᠦᠪᠰᠢᠶᠡᠷᠡᠬᠦ.</translation>
    </message>
    <message>
        <source>Permissions Page</source>
        <translation type="vanished">权限</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page-factory.h" line="41"/>
        <source>Permissions</source>
        <translation>ᠡᠷᠬᠡ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page-factory.h" line="47"/>
        <source>Show and modify file&apos;s permission, owner and group.</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠡᠷᠬᠡ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ ᠪᠤᠶᠤ ᠵᠠᠰᠠᠬᠤ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="177"/>
        <source>Can not trash</source>
        <translation>ᠬᠤᠷᠢᠶᠠᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="171"/>
        <source>Can not trash these files. You can delete them permanently. Are you sure doing that?</source>
        <translation>ᠡᠳᠡᠭᠡᠷ ᠹᠠᠢᠯ ᠢ᠋ ᠪᠦᠬᠦᠨ ᠢ᠋ᠶ᠋ᠡᠷ ᠨᠢ ᠬᠤᠭᠯᠠᠭᠤᠷ ᠲᠤ᠌ ᠬᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ, ᠡᠳᠡᠭᠡᠷ ᠹᠠᠢᠯ ᠢ᠋ ᠦᠨᠢᠳᠡ ᠬᠠᠰᠤᠬᠤ ᠵᠢ ᠰᠤᠩᠭᠤᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ, ᠢᠩᠬᠢᠵᠤ ᠬᠢᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="175"/>
        <source>Can not trash files more than 10GB, would you like to delete it permanently?</source>
        <translation>10GB ᠡᠴᠡ ᠲᠤᠮᠤ ᠹᠠᠢᠯ ᠢ᠋ ᠬᠤᠷᠢᠶᠠᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ, ᠦᠨᠢᠳᠡ ᠬᠠᠰᠤᠬᠤ ᠱᠠᠭᠠᠷᠳᠠᠯᠭ᠎ᠠ ᠲᠠᠢ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="295"/>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="297"/>
        <source>Delete Permanently</source>
        <translation>ᠦᠨᠢᠳᠡ ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="295"/>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="297"/>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation>ᠲᠠ ᠨᠡᠬᠡᠷᠡᠨ ᠡᠳᠡᠭᠡᠷ ᠹᠠᠢᠯ ᠢ᠋ ᠬᠠᠰᠤᠵᠤ ᠬᠠᠶᠠᠬᠤ ᠤᠤ? ᠬᠠᠰᠤᠵᠤ ᠡᠬᠢᠯᠡᠭᠰᠡᠬᠡᠷ ᠡᠳᠡᠭᠡᠷ ᠹᠠᠢᠯ ᠢ᠋ ᠳᠠᠬᠢᠵᠤ ᠰᠡᠷᠬᠦᠬᠡᠵᠤ ᠳᠡᠢᠯᠬᠦ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <source>Computer Properties Page</source>
        <translation type="vanished">计算机</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page-factory.h" line="41"/>
        <source>Computer Properties</source>
        <translation>ᠺᠤᠮᠫᠢᠦᠲᠸᠷ ᠤ᠋ᠨ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page-factory.h" line="47"/>
        <source>Show the computer properties or items in computer.</source>
        <translation>ᠺᠤᠮᠫᠢᠦᠲᠸᠷ ᠤ᠋ᠨ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠪᠤᠶᠤ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ ᠤ᠋ᠨ ᠳᠤᠮᠳᠠᠬᠢ ᠳᠦᠷᠦᠯ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ.</translation>
    </message>
    <message>
        <source>Trash and Recent Properties Page</source>
        <translation type="vanished">最近/回收</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page-factory.h" line="40"/>
        <source>Trash and Recent</source>
        <translation>ᠬᠤᠭᠯᠠᠭᠤᠷ/ ᠤᠢᠷ᠎ᠠ ᠵᠢᠨ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page-factory.h" line="46"/>
        <source>Show the file properties or items in trash or recent.</source>
        <translation>&quot; ᠬᠤᠭᠯᠠᠭᠤᠷ&quot; ᠪᠤᠶᠤ&quot; ᠤᠢᠷ᠎ᠠ ᠵᠢᠨ&quot; ᠳᠤᠮᠳᠠᠬᠢ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠷᠢᠶᠠᠯᠠᠯ ᠡᠰᠡᠪᠡᠯ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ.</translation>
    </message>
    <message>
        <source>eject device failed</source>
        <translation type="vanished">弹出设备失败</translation>
    </message>
    <message>
        <source>Please check whether the device is occupied and then eject the device again</source>
        <translation type="vanished">请检查设备是否正在使用,确认没有使用后再次弹出</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="382"/>
        <source>Format failed</source>
        <translation>ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="384"/>
        <source>YES</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="818"/>
        <source>Formatting successful! But failed to set the device name.</source>
        <translation>ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠪᠠ! ᠬᠡᠷᠡᠭᠰᠡᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="832"/>
        <source>qmesg_notify</source>
        <translation>ᠮᠡᠳᠡᠭᠳᠡᠯ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="816"/>
        <source>Format operation has been finished successfully.</source>
        <translation>ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠬᠤ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠠᠮᠵᠢᠯᠳᠠ ᠲᠠᠢ ᠳᠠᠭᠤᠰᠬᠠᠪᠠ.</translation>
    </message>
    <message>
        <source>Formatting successful! Description Failed to set the device name.</source>
        <translation type="vanished">格式化成功！设备名设置失败。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="832"/>
        <source>Sorry, the format operation is failed!</source>
        <translation>ᠠᠭᠤᠴᠢᠯᠠᠭᠠᠷᠠᠢ, ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠬᠤ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠢᠯᠠᠭᠳᠠᠪᠠ, ᠲᠠ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="845"/>
        <source>Formatting this volume will erase all data on it. Please backup all retained data before formatting. Do you want to continue ?</source>
        <translation>ᠳᠤᠰ ᠡᠪᠬᠡᠮᠡᠯ ᠢ᠋ ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠬᠤ ᠨᠢ ᠳᠡᠬᠦᠨ ᠳᠡᠭᠡᠷᠡᠬᠢ ᠪᠦᠬᠦᠢᠯᠡ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠴᠡᠪᠡᠷᠯᠡᠬᠦ ᠪᠤᠯᠤᠨ᠎ᠠ, ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠬᠤ ᠡᠴᠡ ᠪᠡᠨ ᠡᠮᠦᠨ᠎ᠡ ᠦᠯᠡᠳᠡᠭᠡᠬᠦ ᠶᠤᠰᠤᠳᠠᠢ ᠪᠦᠬᠦᠢᠯᠡ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠨᠦᠭᠡᠴᠡᠭᠡᠯᠡᠷᠡᠢ. ᠲᠠ ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠨ ᠬᠦᠢᠴᠡᠳᠬᠡᠬᠦ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="816"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="818"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="847"/>
        <source>format</source>
        <translation>ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="849"/>
        <source>begin format</source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="851"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/sync-thread.cpp" line="41"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="751"/>
        <source>File Manager</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠤᠷ</translation>
    </message>
    <message>
        <source>Default search vfs of peony</source>
        <translation type="vanished">默认文件搜索</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1449"/>
        <source>Force unmount failed</source>
        <translation>ᠠᠯᠪᠠ ᠪᠡᠷ ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="129"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1449"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="380"/>
        <source>Error: %1
</source>
        <translation>ᠪᠤᠷᠤᠭᠤ: %1
</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="135"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1452"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1502"/>
        <source>Data synchronization is complete,the device has been unmount successfully!</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠪᠠ, ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠪᠠᠭᠤᠯᠭᠠᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="124"/>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="129"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1481"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1484"/>
        <source>Unmount failed</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1469"/>
        <source>Not authorized to perform operation.</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠡᠷᠬᠡ ᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="124"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1481"/>
        <source>Unable to unmount it, you may need to close some programs, such as: GParted etc.</source>
        <translation>ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ, ᠲᠠ ᠤᠷᠢᠳᠠᠪᠠᠷ ᠨᠢᠭᠡ ᠪᠦᠯᠦᠭ ᠫᠡᠷᠦᠭᠷᠡᠮ ᠢ᠋ ᠬᠠᠭᠠᠬᠤ ᠴᠢᠬᠤᠯᠠᠳᠠᠢ, ᠬᠤᠪᠢᠶᠠᠷᠢ ᠬᠡᠰᠡᠭ ᠤ᠋ᠨ ᠨᠠᠢᠷᠠᠭᠤᠯᠤᠭᠤᠷ ᠵᠡᠷᠭᠡ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1484"/>
        <source>Error: %1
Do you want to unmount forcely?</source>
        <translation>ᠪᠤᠷᠤᠭᠤ: %1
ᠠᠯᠪᠠ ᠪᠡᠷ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Eject Anyway</source>
        <translation type="vanished">无论如何弹出</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="993"/>
        <source>The device has been mount successfully!</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠠᠴᠢᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1180"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1213"/>
        <source>Data synchronization is complete and the device can be safely unplugged!</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠪᠠ, ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠠᠮᠤᠷ ᠳᠦᠪᠰᠢᠨ ᠢ᠋ᠶ᠋ᠡᠷ ᠰᠤᠭᠤᠯᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <source>Unable to eject %1</source>
        <translation type="vanished">无法弹出 %1</translation>
    </message>
    <message>
        <source>PeonyNotify</source>
        <translation type="vanished">文件管理器通知</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1175"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1207"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1469"/>
        <source>Eject failed</source>
        <translation>ᠦᠰᠦᠷᠴᠤ ᠭᠠᠷᠴᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="261"/>
        <source>favorite</source>
        <translation>ᠳᠦᠷᠭᠡᠨ ᠠᠢᠯᠴᠢᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="294"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="299"/>
        <source>File is not existed.</source>
        <translation>ᠹᠠᠢᠯ ᠤᠷᠤᠰᠢᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="307"/>
        <source>Share Data</source>
        <translation>ᠲᠤᠰ ᠮᠠᠰᠢᠨ ᠤ᠋ ᠬᠠᠮᠳᠤᠪᠠᠷ ᠡᠳ᠋ᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="353"/>
        <source>Operation not supported</source>
        <translation>ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="447"/>
        <source>The virtual file system does not support folder creation</source>
        <translation>ᠵᠢᠰᠢᠮᠡᠭ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠳᠤᠤᠷ᠎ᠠ ᠰᠢᠨ᠎ᠡ ᠪᠡᠷ ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="519"/>
        <source>Can not create a symbolic file for vfs location</source>
        <translation>ᠵᠢᠰᠢᠮᠡᠭ ᠭᠠᠷᠴᠠᠭ ᠤ᠋ᠨ ᠳᠤᠤᠷ᠎ᠠ ᠲᠦᠳᠡ ᠠᠷᠭ᠎ᠠ ᠵᠢ ᠪᠠᠢᠭᠤᠯᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="526"/>
        <source>Symbolic Link</source>
        <translation>ᠲᠦᠳᠡ ᠠᠷᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="538"/>
        <source>Can not create symbolic file here, %1</source>
        <translation>ᠡᠨᠳᠡ ᠲᠦᠳᠡ ᠠᠷᠭ᠎ᠠ ᠪᠠᠢᠭᠤᠯᠬᠤ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ, %1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="547"/>
        <source>Can not add a file to favorite directory.</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠬᠠᠪᠳᠠᠰᠤᠨ ᠳ᠋ᠤ᠌ ᠨᠡᠮᠡᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="605"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="613"/>
        <source>The virtual file system cannot be opened</source>
        <translation>ᠵᠢᠰᠢᠮᠡᠭ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠰᠢᠰᠲ᠋ᠧᠮ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠬᠦ ᠵᠢᠨ ᠠᠷᠭ᠎ᠠ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="434"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="462"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="477"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="563"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="581"/>
        <source>Virtual file directories do not support move and copy operations</source>
        <translation>ᠵᠢᠰᠢᠮᠡᠭ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠵᠠᠮ ᠱᠤᠭᠤᠮ ᠨᠢ ᠰᠢᠯᠵᠢᠬᠦᠯᠬᠦ ᠪᠤᠶᠤ ᠺᠤᠫᠢᠳᠠᠬᠤ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-register.h" line="43"/>
        <source>Default favorite vfs of peony</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page-factory.h" line="38"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page-factory.h" line="44"/>
        <source>Details</source>
        <translation>ᠳᠡᠯᠭᠡᠷᠡᠩᠭᠦᠢ ᠮᠡᠳᠡᠭᠡ ᠵᠠᠩᠭᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/mark-properties-page-factory.h" line="40"/>
        <source>Mark</source>
        <translation>ᠳᠡᠮᠳᠡᠭ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/mark-properties-page-factory.h" line="46"/>
        <source>mark this file.</source>
        <translation>ᠡᠨᠡᠬᠦ ᠹᠠᠢᠯ ᠳ᠋ᠤ᠌ ᠳᠡᠮᠳᠡᠭ ᠳᠠᠯᠪᠢᠬᠤ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page-factory.h" line="40"/>
        <source>Open With</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ ᠠᠷᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page-factory.h" line="46"/>
        <source>open with.</source>
        <translation>ᠨᠡᠬᠡᠬᠡᠬᠦ ᠠᠷᠭ᠎ᠠ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/sync-thread.cpp" line="11"/>
        <source>It need to synchronize before operating the device,place wait!</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠠᠵᠢᠯᠯᠠᠭᠤᠯᠬᠤ ᠡᠴᠡ ᠡᠮᠦᠨ᠎ᠡ ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢ ᠢᠵᠢᠯ ᠠᠯᠬᠤᠮᠴᠢᠯᠠᠬᠤ ᠱᠠᠭᠠᠷᠳᠠᠯᠭ᠎ᠠ ᠲᠠᠢ, ᠲᠦᠷ ᠬᠦᠯᠢᠶᠡᠬᠡᠷᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="407"/>
        <source>permission denied</source>
        <translation>ᠡᠷᠬᠡ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="400"/>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="413"/>
        <source>file not found</source>
        <translation>ᠳᠤᠰ ᠹᠠᠢᠯ ᠢ᠋ ᠤᠯᠵᠤ ᠮᠡᠳᠡᠭᠰᠡᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="281"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="298"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="314"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="490"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="155"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="177"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="199"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="206"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="222"/>
        <source>duplicate</source>
        <translation>ᠬᠠᠭᠤᠯᠪᠤᠷᠢ ᠳᠡᠪᠳᠡᠷ</translation>
    </message>
    <message>
        <source>Error when getting information for file : No target file found</source>
        <translation type="vanished">获取文件信息时出现错误：没有目标文件。</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-utils.cpp" line="345"/>
        <source>data</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="186"/>
        <location filename="../../libpeony-qt/vfs/search-vfs-uri-parser.cpp" line="110"/>
        <source>Computer</source>
        <translation>ᠺᠤᠮᠫᠢᠦᠲᠸᠷ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="249"/>
        <source>File System</source>
        <translation>ᠹᠠᠢᠯ᠎ᠤᠨ ᠱᠢᠰᠲ᠋ᠧᠮ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="254"/>
        <source>Data</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢᠨ ᠲᠠᠪᠠᠭ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="379"/>
        <source>Failed to open file &quot;%1&quot;: insufficient permissions.</source>
        <translation>&quot;%1&quot; ᠹᠠᠢᠯ ᠢ᠋ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ:ᠡᠷᠬᠡ ᠬᠦᠷᠦᠯᠴᠡᠬᠦ ᠦᠬᠡᠢ.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="390"/>
        <source>File “%1” does not exist. Please check whether the file has been deleted.</source>
        <translation>ᠹᠠᠢᠯ“%1” ᠤᠷᠤᠰᠢᠬᠤ ᠦᠬᠡᠢ, ᠹᠠᠢᠯ ᠬᠠᠰᠤᠭᠳᠠᠭᠰᠠᠨ ᠡᠰᠡᠬᠦ ᠵᠢ ᠰᠢᠯᠭᠠᠵᠤ ᠦᠵᠡᠬᠡᠷᠡᠢ.</translation>
    </message>
</context>
</TS>
