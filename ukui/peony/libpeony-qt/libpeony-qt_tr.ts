<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr_TR">
<context>
    <name>ConnectServerDialog</name>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="14"/>
        <source>Connect to Sever</source>
        <translation>Sunucuya Bağlan</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="32"/>
        <source>Domain</source>
        <translation>Alan Adı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="39"/>
        <source>Password</source>
        <translation>Parola</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="55"/>
        <source>Save Password</source>
        <translation>Parolayı Kaydet</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="62"/>
        <source>User</source>
        <translation>Kullanıcı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.ui" line="82"/>
        <source>Anonymous</source>
        <translation>Misafir</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.cpp" line="35"/>
        <source>Ok</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-server-dialog.cpp" line="36"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
</context>
<context>
    <name>FileLabelModel</name>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="47"/>
        <source>Red</source>
        <translation>Kırmızı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="48"/>
        <source>Orange</source>
        <translation>Turuncu</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="49"/>
        <source>Yellow</source>
        <translation>Sarı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="50"/>
        <source>Green</source>
        <translation>Yeşil</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="51"/>
        <source>Blue</source>
        <translation>Mavi</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="52"/>
        <source>Purple</source>
        <translation>Mor</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="53"/>
        <source>Gray</source>
        <translation>Gri</translation>
    </message>
    <message>
        <source>Transparent</source>
        <translation type="vanished">Şeffaf</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="obsolete">Dosya Silme Uyarısı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="120"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="345"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="120"/>
        <location filename="../../libpeony-qt/model/file-label-model.cpp" line="345"/>
        <source>Label or color is duplicated.</source>
        <translation>Etiket veya renk yinelendi.</translation>
    </message>
</context>
<context>
    <name>Format_Dialog</name>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="20"/>
        <source>Dialog</source>
        <translation>Diyalog</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="32"/>
        <source>rom_size</source>
        <translation>Rom boyutu</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="45"/>
        <source>system</source>
        <translation>Sistem</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="59"/>
        <source>vfat/fat32</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="64"/>
        <source>exfat</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="69"/>
        <source>ntfs</source>
        <translation>Ntfs</translation>
    </message>
    <message>
        <source>vfat</source>
        <translation type="vanished">Vfat</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="74"/>
        <source>ext4</source>
        <translation>Ext4</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="88"/>
        <source>device_name</source>
        <translation>Aygıt Adı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="114"/>
        <source>clean it total</source>
        <translation>Toplamı Temizle</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="127"/>
        <source>ok</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="140"/>
        <source>close</source>
        <translation>Kapat</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.ui" line="179"/>
        <source>TextLabel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>qmesg_notify</source>
        <translation type="vanished">Qmesg bildir</translation>
    </message>
    <message>
        <source>Format operation has been finished successfully.</source>
        <translation type="vanished">Biçimlendirme işlemi başarıyla tamamlandı.</translation>
    </message>
    <message>
        <source>Sorry, the format operation is failed!</source>
        <translation type="vanished">Üzgünüz, formatlama işlemi başarısız oldu!</translation>
    </message>
    <message>
        <source>Formatting this volume will erase all data on it. Please backup all retained data before formatting. Do you want to continue ?</source>
        <translation type="vanished">Bu birimi biçimlendirmek, içindeki tüm verileri silecektir. Biçimlendirmeden önce lütfen saklanan tüm verileri yedekleyin. Devam etmek istiyor musun?</translation>
    </message>
    <message>
        <source>format</source>
        <translation type="vanished">Biçim</translation>
    </message>
    <message>
        <source>begin format</source>
        <translation type="vanished">biçimlendirmeye başla</translation>
    </message>
    <message>
        <source>format_success</source>
        <translation type="obsolete">Biçimlentirme başarılı</translation>
    </message>
    <message>
        <source>format_err</source>
        <translation type="obsolete">Biçimlendirme hatalı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="57"/>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="70"/>
        <source>Rom size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="76"/>
        <source>Filesystem:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="86"/>
        <source>Disk name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="116"/>
        <source>Completely erase(Time is longer, please confirm!)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="126"/>
        <source>Set password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="127"/>
        <source>Set password for volume based on LUKS (only ext4)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="140"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="141"/>
        <source>OK</source>
        <translation type="unfinished">Tamam</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="244"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="411"/>
        <source>Enter Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="428"/>
        <source>Password too short, please retype a password more than 6 characters</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1051"/>
        <source>Error</source>
        <translation type="unfinished">Hata</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1051"/>
        <source>Block not existed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="1092"/>
        <source>Formatting. Do not close this window</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainProgressBar</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="382"/>
        <source>File operation</source>
        <translation>Dosya işlemi</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="421"/>
        <source>starting ...</source>
        <translation>Başlatılıyor...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="398"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="515"/>
        <source>cancel all file operations</source>
        <translation>Tüm dosya işlemlerini iptal et</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="399"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="516"/>
        <source>Are you sure want to cancel all file operations</source>
        <translation>Tüm dosya işlemlerini iptal etmek istediğinizden emin misiniz</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="605"/>
        <source>canceling ...</source>
        <translation>İptal ediliyor...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="608"/>
        <source>sync ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="401"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="518"/>
        <source>OK</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="402"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="519"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
</context>
<context>
    <name>OtherButton</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="703"/>
        <source>Other queue</source>
        <translation type="unfinished">Diğer sıra</translation>
    </message>
</context>
<context>
    <name>Peony::AdvanceSearchBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="55"/>
        <source>Key Words</source>
        <translation>Anahtar Kelimeleri</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="58"/>
        <source>input key words...</source>
        <translation>Anahtar kelimeler gir...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="59"/>
        <source>Search Location</source>
        <translation>Arama Konumu</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="61"/>
        <source>choose search path...</source>
        <translation>Arama yolu seç...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="68"/>
        <source>browse</source>
        <translation>Araştır</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="69"/>
        <source>File Type</source>
        <translation>Dosya Türü</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="71"/>
        <source>Choose File Type</source>
        <translation>Dosya Türü Seç</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="76"/>
        <source>Modify Time</source>
        <translation>Değiştirme Zamanı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="78"/>
        <source>Choose Modify Time</source>
        <translation>Değiştirme Zamanı Seç</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="83"/>
        <source>File Size</source>
        <translation>Dosya Boyutu</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="85"/>
        <source>Choose file size</source>
        <translation>Dosya boyutu seç</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="90"/>
        <source>show hidden file</source>
        <translation>Gizli dosyaları göster</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="91"/>
        <source>go back</source>
        <translation>Geri git</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="92"/>
        <source>hidden advance search page</source>
        <translation>Gizli gelişmiş arama sayfası</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="94"/>
        <source>file name</source>
        <translation>Dosya adı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="95"/>
        <source>content</source>
        <translation>İçerik</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="100"/>
        <source>search</source>
        <translation>Ara</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="101"/>
        <source>start search</source>
        <translation>Aramayı başlat</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="174"/>
        <source>Select path</source>
        <translation>Yol seçin</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="193"/>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="202"/>
        <source>Operate Tips</source>
        <translation>İşlem İpuçları</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="194"/>
        <source>Have no key words or search location!</source>
        <translation>Anahtar kelimeler veya arama konumu yok!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/advance-search-bar.cpp" line="203"/>
        <source>Search file name or content at least choose one!</source>
        <translation>Dosya adını veya içeriğini arayın en azından birini seçin!</translation>
    </message>
    <message>
        <source>Search content or file name at least choose one!</source>
        <translation type="vanished">İçeriği veya dosya adını en az birini seçin!</translation>
    </message>
    <message>
        <source>all</source>
        <translation type="vanished">Tümü</translation>
    </message>
    <message>
        <source>file folder</source>
        <translation type="vanished">Dosya klasör</translation>
    </message>
    <message>
        <source>image</source>
        <translation type="vanished">Resim</translation>
    </message>
    <message>
        <source>video</source>
        <translation type="vanished">Video</translation>
    </message>
    <message>
        <source>text file</source>
        <translation type="vanished">Metin dosyası</translation>
    </message>
    <message>
        <source>audio</source>
        <translation type="vanished">Ses</translation>
    </message>
    <message>
        <source>others</source>
        <translation type="vanished">Diğerleri</translation>
    </message>
    <message>
        <source>wps file</source>
        <translation type="obsolete">Wps Dosyası</translation>
    </message>
    <message>
        <source>today</source>
        <translation type="vanished">Bugün</translation>
    </message>
    <message>
        <source>this week</source>
        <translation type="vanished">Bu hafta</translation>
    </message>
    <message>
        <source>this month</source>
        <translation type="vanished">Bu ay</translation>
    </message>
    <message>
        <source>this year</source>
        <translation type="vanished">Bu yıl</translation>
    </message>
    <message>
        <source>year ago</source>
        <translation type="vanished">Önceki yıl</translation>
    </message>
    <message>
        <source>tiny(0-16K)</source>
        <translation type="vanished">Çok küçük(0-16K)</translation>
    </message>
    <message>
        <source>small(16k-1M)</source>
        <translation type="vanished">Küçük(16k-1M)</translation>
    </message>
    <message>
        <source>medium(1M-100M)</source>
        <translation type="vanished">Orta(1M-100M)</translation>
    </message>
    <message>
        <source>big(100M-1G)</source>
        <translation type="vanished">Büyük(100M-1G)</translation>
    </message>
    <message>
        <source>large(&gt;1G)</source>
        <translation type="vanished">Çok büyük(&gt;1G)</translation>
    </message>
</context>
<context>
    <name>Peony::AdvancedLocationBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/advanced-location-bar.cpp" line="175"/>
        <source>Search Content...</source>
        <translation>İçerik Ara...</translation>
    </message>
</context>
<context>
    <name>Peony::AllFileLaunchDialog</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="341"/>
        <source>Choose new application</source>
        <translation type="unfinished">Yeni uygulama seçin</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="343"/>
        <source>Choose an Application to open this file</source>
        <translation type="unfinished">Bu dosyayı açmak için bir Uygulama seçin</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="350"/>
        <source>apply now</source>
        <translation type="unfinished">Şimdi uygula</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="356"/>
        <source>OK</source>
        <translation type="unfinished">Tamam</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="357"/>
        <source>Cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
</context>
<context>
    <name>Peony::AudioPlayManager</name>
    <message>
        <source>Operation file Warning</source>
        <translation type="vanished">İşlem dosyası Uyarısı</translation>
    </message>
</context>
<context>
    <name>Peony::BasicPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="859"/>
        <source>Choose a custom icon</source>
        <translation>Özel bir simge seçin</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="385"/>
        <source>Type:</source>
        <translation>Tür:</translation>
    </message>
    <message>
        <source>Display Name:</source>
        <translation type="vanished">Ekran Adı:</translation>
    </message>
    <message>
        <source>Location:</source>
        <translation type="vanished">Konum:</translation>
    </message>
    <message>
        <source>Overview:</source>
        <translation type="vanished">Genel Bakış:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="236"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="237"/>
        <source>Change</source>
        <translation>Değiştir</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="291"/>
        <source>Name</source>
        <translation>İsim</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="292"/>
        <source>Location</source>
        <translation>Konum</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="334"/>
        <source>move</source>
        <translation>taşı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="391"/>
        <source>symbolLink</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="402"/>
        <source>Include:</source>
        <translation>Dahil et:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="406"/>
        <source>Open with:</source>
        <translation>İle aç:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="412"/>
        <source>Description:</source>
        <translation>Açıklama:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="424"/>
        <source>Select multiple files</source>
        <translation>Birden çok dosya seçin</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="429"/>
        <source>Size:</source>
        <translation>Boyut:</translation>
    </message>
    <message>
        <source>Total size:</source>
        <translation type="vanished">Toplam boyut:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="430"/>
        <source>Space Useage:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Time Created:</source>
        <translation type="vanished">Oluşturma Zamanı:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="172"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="465"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="471"/>
        <source>Time Modified:</source>
        <translation>Değiştirme Zamanı:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="395"/>
        <source>Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="466"/>
        <source>Time Access:</source>
        <translation>Erişim Zamanı:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="494"/>
        <source>Readonly</source>
        <translation>Salt okunur</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="495"/>
        <source>Hidden</source>
        <translation>Gizli</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="512"/>
        <source>Property:</source>
        <translation>Özellik:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="601"/>
        <source>usershare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="696"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="900"/>
        <source>%1 (%2 Bytes)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="732"/>
        <source>Choose a new folder:</source>
        <translation>Yeni bir klasör seçin:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="739"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="744"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="739"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="744"/>
        <source>cannot move a folder to itself !</source>
        <translation>Bir klasör kendisine taşınamaz!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="893"/>
        <source>%1 Bytes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="909"/>
        <source>%1 files, %2 folders</source>
        <translation>%1 dosya, %2 klasör</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="997"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="999"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1004"/>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page.cpp" line="1006"/>
        <source>Can&apos;t get remote file information</source>
        <translation>Uzak dosya bilgisi alınamıyor</translation>
    </message>
    <message>
        <source>%1 files (include root files), %2 hidden</source>
        <translation type="vanished">%1 dosya (kök dosyaları dahil),%2 gizli</translation>
    </message>
    <message>
        <source>%1 total</source>
        <translation type="vanished">Toplam %1</translation>
    </message>
</context>
<context>
    <name>Peony::ComputerPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="93"/>
        <source>CPU Name:</source>
        <translation>CPU Adı:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="94"/>
        <source>CPU Core:</source>
        <translation>CPU Çekirdeği:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="95"/>
        <source>Memory Size:</source>
        <translation>Hafıza Boyutu:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="107"/>
        <source>User Name: </source>
        <translation>Kullanıcı Adı: </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="108"/>
        <source>Desktop: </source>
        <translation>Masaüstü: </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="116"/>
        <source>You should mount this volume first</source>
        <translation>Önce bu birimi bağlamalısınız</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="129"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="213"/>
        <source>Name: </source>
        <translation>İsim: </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="129"/>
        <source>File System</source>
        <translation>Dosya Sistemi</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="129"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="130"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="218"/>
        <source>Total Space: </source>
        <translation>Toplam Alan: </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="131"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="219"/>
        <source>Used Space: </source>
        <translation>Kullanılan Alan: </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="132"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="220"/>
        <source>Free Space: </source>
        <translation>Boş Alan: </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="133"/>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="221"/>
        <source>Type: </source>
        <translation>Tür: </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="230"/>
        <source>Kylin Burner</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="236"/>
        <source>Open with: 	</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page.cpp" line="243"/>
        <source>Unknown</source>
        <translation>Bilinmeyen</translation>
    </message>
</context>
<context>
    <name>Peony::ConnectServerDialog</name>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="134"/>
        <source>connect to server</source>
        <translation>sunucuya bağlan</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="159"/>
        <source>ip</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="161"/>
        <source>port</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="162"/>
        <source>type</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="192"/>
        <source>Personal Collection server:</source>
        <translation>Kişisel Koleksiyon sunucusu:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="205"/>
        <source>add</source>
        <translation>ekle</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="206"/>
        <source>delete</source>
        <translation>sil</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="207"/>
        <source>connect</source>
        <translation>bağlan</translation>
    </message>
</context>
<context>
    <name>Peony::ConnectServerLogin</name>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="382"/>
        <source>The login user</source>
        <translation>Oturum açan kullanıcı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="392"/>
        <source>Please enter the %1&apos;s user name and password of the server.</source>
        <translation>Lütfen %1&apos;in sunucunun kullanıcı adını ve şifresini girin.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="401"/>
        <source>User&apos;s identity</source>
        <translation>Kullanıcının kimliği</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="402"/>
        <source>guest</source>
        <translation>misafir</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="403"/>
        <source>Registered users</source>
        <translation>Kayıtlı kullanıcılar</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="420"/>
        <source>name</source>
        <translation>isim</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="421"/>
        <source>password</source>
        <translation>parola</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="422"/>
        <source>Remember the password</source>
        <translation>Parolayı hatırla</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="442"/>
        <source>cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/connect-to-server-dialog.cpp" line="443"/>
        <source>ok</source>
        <translation>Tamam</translation>
    </message>
</context>
<context>
    <name>Peony::CreateLinkInternalPlugin</name>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="115"/>
        <source>Create Link to Desktop</source>
        <translation>Masaüstünde Link Oluştur</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="141"/>
        <source>Create Link to...</source>
        <translation>Linki Oluştur...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="144"/>
        <source>Choose a Directory to Create Link</source>
        <translation>Bağlantı Oluşturmak İçin Bir Dizin Seçin</translation>
    </message>
    <message>
        <source>Peony-Qt Create Link Extension</source>
        <translation type="vanished">Dosya Yöneticisi Bağlantı Uzantısı Oluştur</translation>
    </message>
    <message>
        <source>Create Link Menu Extension.</source>
        <translation type="vanished">Bağlantı Menüsü Uzantısı Oluştur.</translation>
    </message>
</context>
<context>
    <name>Peony::CreateSharedFileLinkMenuPlugin</name>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="233"/>
        <source>Create Link to Desktop</source>
        <translation type="unfinished">Masaüstünde Link Oluştur</translation>
    </message>
</context>
<context>
    <name>Peony::CreateTemplateOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="62"/>
        <source>NewFile</source>
        <translation>Yeni Dosya</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="78"/>
        <source>Create file</source>
        <translation>Dosya oluştur</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="90"/>
        <source>NewFolder</source>
        <translation>Yeni Klasör</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="109"/>
        <location filename="../../libpeony-qt/file-operation/create-template-operation.cpp" line="142"/>
        <source>Create file error</source>
        <translation>Dosya oluşturma hatası</translation>
    </message>
</context>
<context>
    <name>Peony::CustomErrorHandler</name>
    <message>
        <location filename="../../libpeony-qt/custom-error-handler.cpp" line="40"/>
        <source>Is Error Handled?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/custom-error-handler.cpp" line="44"/>
        <source>Error not be handled correctly</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DefaultOpenWithWidget</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="400"/>
        <source>No default app</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DefaultPreviewPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="71"/>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="186"/>
        <source>Select the file you want to preview...</source>
        <translation>Önizlemek istediğiniz dosyayı seçin ...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="177"/>
        <source>Can not preview this file.</source>
        <translation>Bu dosya önizlemesi görüntülenemiyor.</translation>
    </message>
    <message>
        <source>Can not preivew this file.</source>
        <translation type="vanished">Bu dosya önizlemesi görüntülenemiyor.</translation>
    </message>
</context>
<context>
    <name>Peony::DefaultPreviewPageFactory</name>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page-factory.h" line="50"/>
        <source>Default Preview</source>
        <translation>Varsayılan Önizleme</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page-factory.h" line="53"/>
        <source>This is the Default Preview of peony-qt</source>
        <translation>Bu peony-qt Varsayılan Önizlemesi</translation>
    </message>
</context>
<context>
    <name>Peony::DetailsPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="174"/>
        <source>Name:</source>
        <translation>İsim:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="177"/>
        <source>File type:</source>
        <translation>Dosya türü:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="193"/>
        <source>Location:</source>
        <translation>Konum:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="207"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="214"/>
        <source>yyyy-MM-dd, HH:mm:ss</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="199"/>
        <source>Create time:</source>
        <translation>Oluşturma zamanı:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="204"/>
        <source>Modify time:</source>
        <translation>Değiştirme zamanı:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="212"/>
        <source>yyyy-MM-dd, hh:mm:ss AP</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="221"/>
        <source>File size:</source>
        <translation>Dosya boyutu:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="228"/>
        <source>Width:</source>
        <translation>Genişlik:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="231"/>
        <source>Height:</source>
        <translation>Yükseklik:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="239"/>
        <source>Owner</source>
        <translation>Sahibi</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="240"/>
        <source>Owner:</source>
        <translation>Sahibi:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="242"/>
        <source>Computer</source>
        <translation>Bilgisayar</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="243"/>
        <source>Computer:</source>
        <translation>Bilgisayar:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="288"/>
        <source>%1 (this computer)</source>
        <translation>%1 (bu bilgisayar)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="295"/>
        <source>Unknown</source>
        <translation>Bilinmeyen</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="324"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="325"/>
        <source>Can&apos;t get remote file information</source>
        <translation>Uzak dosya bilgisi alınamıyor</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="334"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page.cpp" line="335"/>
        <source>%1 px</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::IconView</name>
    <message>
        <source>Icon View</source>
        <translation type="vanished">Simge Görünümü</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/icon-view/icon-view.cpp" line="290"/>
        <source>warn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/icon-view/icon-view.cpp" line="290"/>
        <source>This operation is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::IconView2</name>
    <message>
        <source>Icon View</source>
        <translation type="vanished">Simge Görünümü</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::ListView</name>
    <message>
        <source>List View</source>
        <translation type="vanished">Liste Görünümü</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/list-view/list-view.cpp" line="482"/>
        <source>warn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/view/list-view/list-view.cpp" line="482"/>
        <source>This operation is not supported.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::ListView2</name>
    <message>
        <source>List View</source>
        <translation type="vanished">Liste Görünümü</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryViewFactoryManager</name>
    <message>
        <source>Icon View</source>
        <translation type="vanished">Simge Görünümü</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryViewMenu</name>
    <message>
        <source>Open in &amp;New Window</source>
        <translation type="vanished">Yeni Pen&amp;cerede Aç</translation>
    </message>
    <message>
        <source>Open in New &amp;Tab</source>
        <translation type="vanished">&amp;Yeni Sekmede Aç</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="282"/>
        <source>Add to bookmark</source>
        <translation>Yer işaretlerine ekle</translation>
    </message>
    <message>
        <source>&amp;Open &quot;%1&quot;</source>
        <translation type="vanished">“%1” Aç</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; in &amp;New Window</source>
        <translation type="vanished">“%1” Yeni Pencerede Aç</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; in New &amp;Tab</source>
        <translation type="vanished">“%1” Yeni Sekmede Aç</translation>
    </message>
    <message>
        <source>Open &quot;%1&quot; with...</source>
        <translation type="vanished">“%1” ile aç...</translation>
    </message>
    <message>
        <source>&amp;More applications...</source>
        <translation type="vanished">&amp;Daha Fazla Uygulama...</translation>
    </message>
    <message>
        <source>&amp;Open</source>
        <translation type="vanished">&amp;Aç</translation>
    </message>
    <message>
        <source>Open &amp;with...</source>
        <translation type="vanished">&amp;İle aç...</translation>
    </message>
    <message>
        <source>&amp;Open %1 selected files</source>
        <translation type="vanished">%1 dosy&amp;a seçildi</translation>
    </message>
    <message>
        <source>&amp;New...</source>
        <translation type="vanished">&amp;Yeni</translation>
    </message>
    <message>
        <source>Empty &amp;File</source>
        <translation type="vanished">&amp;Boş Dosya</translation>
    </message>
    <message>
        <source>&amp;Folder</source>
        <translation type="vanished">&amp;Klasör</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="563"/>
        <source>New Folder</source>
        <translation>Yeni Klasör</translation>
    </message>
    <message>
        <source>Icon View</source>
        <translation type="vanished">Simge Görünümü</translation>
    </message>
    <message>
        <source>List View</source>
        <translation type="vanished">Liste Görünümü</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="589"/>
        <source>View Type...</source>
        <translation>Görünüm Türü...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="608"/>
        <source>Sort By...</source>
        <translation>Sırala...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="613"/>
        <source>Name</source>
        <translation>İsim</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="615"/>
        <source>File Type</source>
        <translation>Dosya Türü</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="616"/>
        <source>File Size</source>
        <translation>Dosya Boyutu</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="454"/>
        <source>New...</source>
        <translation>Yeni...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="248"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="332"/>
        <source>Open in New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="256"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="340"/>
        <source>Open in New Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="301"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="347"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="399"/>
        <source>Open</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="311"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="358"/>
        <source>Open with...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="325"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="393"/>
        <source>More applications...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="407"/>
        <source>Open %1 selected files</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="547"/>
        <source>Empty File</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="559"/>
        <source>Folder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="614"/>
        <source>Modified Date</source>
        <translation>Değiştirme Saati</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="617"/>
        <source>Orignal Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="642"/>
        <source>Sort Order...</source>
        <translation>Sıralama Düzeni...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="649"/>
        <source>Ascending Order</source>
        <translation>Artan Düzen</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="648"/>
        <source>Descending Order</source>
        <translation>Azalan Düzen</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="662"/>
        <source>Sort Preferences...</source>
        <translation>Tercihleri Sırala...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="666"/>
        <source>Folder First</source>
        <translation>Önce Klasör</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="675"/>
        <source>Chinese First</source>
        <translation>Önce Çince</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="684"/>
        <source>Show Hidden</source>
        <translation>Gizlileri Göster</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="719"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="726"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="835"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="873"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1037"/>
        <source>Copy</source>
        <translation type="unfinished">Kopyala</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1093"/>
        <source>File:&quot;%1&quot; is not exist, did you moved or deleted it?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1113"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1122"/>
        <source>Peony-Qt filesafe menu Extension</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Copy</source>
        <translation type="vanished">&amp;Kopyala</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="739"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1041"/>
        <source>Cut</source>
        <translation>Kes</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="762"/>
        <source>Delete to trash</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="807"/>
        <source>Paste</source>
        <translation type="unfinished">Yapıştır</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="847"/>
        <source>Refresh</source>
        <translation type="unfinished">Yenile</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="857"/>
        <source>Select All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="907"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="943"/>
        <source>Properties</source>
        <translation>Özellikler</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="980"/>
        <source>format</source>
        <translation type="unfinished">Biçim</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1018"/>
        <source>Restore</source>
        <translation type="unfinished">Onar</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="773"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="830"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1026"/>
        <source>Delete</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1092"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <source>File:&quot;%1 is not exist, did you moved or deleted it?</source>
        <translation type="vanished">Dosya: &quot;%1 mevcut değil, onu taşıdınız yada sildiniz mi?</translation>
    </message>
    <message>
        <source>Cu&amp;t</source>
        <translation type="vanished">&amp;Kes</translation>
    </message>
    <message>
        <source>&amp;Delete to trash</source>
        <translation type="vanished">Çöp Kutus&amp;una Gönder</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="775"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="785"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="792"/>
        <source>Delete forever</source>
        <translation>Kalıcı Olarak Sil</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="799"/>
        <source>Rename</source>
        <translation>Yeniden Adlandır</translation>
    </message>
    <message>
        <source>Select &amp;All</source>
        <translation type="vanished">&amp;Tümünü Seç</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="865"/>
        <source>Reverse Select</source>
        <translation>Seçimi Ters Çevir</translation>
    </message>
    <message>
        <source>P&amp;roperties</source>
        <translation type="obsolete">&amp;Özellikler</translation>
    </message>
    <message>
        <source>&amp;Delete</source>
        <translation type="vanished">&amp;Sil</translation>
    </message>
    <message>
        <source>&amp;Rename</source>
        <translation type="vanished">&amp;Yeniden Adlandır</translation>
    </message>
    <message>
        <source>&amp;Paste</source>
        <translation type="vanished">&amp;Yapıştır</translation>
    </message>
    <message>
        <source>&amp;Refresh</source>
        <translation type="vanished">Yeni&amp;le</translation>
    </message>
    <message>
        <source>&amp;Properties</source>
        <translation type="vanished">Özellikler</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1004"/>
        <source>&amp;Clean the Trash</source>
        <translation>Çö&amp;pü Temizle</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="obsolete">Dosyayı Sil Uyarısı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1008"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1029"/>
        <source>Delete Permanently</source>
        <translation>Kalıcı Olarak Sil</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1008"/>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1029"/>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation>Bu dosyaları silmek istediğinizden emin misiniz? Bir silme işlemini başlattığınızda, silinen dosyalar bir daha geri yüklenmeyecektir.</translation>
    </message>
    <message>
        <source>&amp;Restore</source>
        <translation type="vanished">O&amp;nar</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1048"/>
        <source>Clean All</source>
        <translation>Tümünü Temizle</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/directory-view-menu/directory-view-menu.cpp" line="1064"/>
        <source>Open Parent Folder in New Window</source>
        <translation>Üst Klasörü Yeni Pencerede Aç</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryViewWidget</name>
    <message>
        <source>Directory View</source>
        <translation type="vanished">Dizin Görünümü</translation>
    </message>
</context>
<context>
    <name>Peony::FMWindow</name>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="92"/>
        <source>File Manager</source>
        <translation>Dosya Yöneticisi</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="170"/>
        <source>advanced search</source>
        <translation>Gelişmiş arama</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="173"/>
        <source>clear record</source>
        <translation>Kaydı temizle</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="278"/>
        <source>Loaing... Press Esc to stop a loading.</source>
        <translation>Yükleniyor ... Yüklemeyi durdurmak için Esc tuşuna basın.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="394"/>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation>Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</translation>
    </message>
    <message>
        <source>Author: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</translation>
    </message>
    <message>
        <source>Ctrl+H</source>
        <comment>Show|Hidden</comment>
        <translation type="vanished">Ctrl+H</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="323"/>
        <source>Undo</source>
        <translation>Geri</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="330"/>
        <source>Redo</source>
        <translation>İleri</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="393"/>
        <source>Peony Qt</source>
        <translation>Dosya Yöneticisi</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/fm-window.cpp" line="447"/>
        <source>New Folder</source>
        <translation>Yeni Klasör</translation>
    </message>
</context>
<context>
    <name>Peony::FileCopy</name>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="145"/>
        <location filename="../../libpeony-qt/file-copy.cpp" line="153"/>
        <location filename="../../libpeony-qt/file-copy.cpp" line="164"/>
        <source>Error in source or destination file path!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="190"/>
        <source>The dest file &quot;%1&quot; has existed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="219"/>
        <source>Vfat/FAT32 file systems do not support a single file that occupies more than 4 GB space!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="260"/>
        <source>Error opening source or destination file!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="309"/>
        <source>Please check whether the device has been removed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="311"/>
        <source>Write file error: There is no avaliable disk space for device!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="366"/>
        <source>File opening failure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-copy.cpp" line="326"/>
        <source>operation cancel</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileCopyOperation</name>
    <message>
        <source>File copy</source>
        <translation type="obsolete">Dosya kopyalama</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="171"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="366"/>
        <source>File copy error</source>
        <translation>Dosya kopyalama hatası</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="374"/>
        <source>Cannot opening file, permission denied!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileDeleteOperation</name>
    <message>
        <source>File delete</source>
        <translation type="obsolete">Dosya silme</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="75"/>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="101"/>
        <source>File delete error</source>
        <translation>Dosya silme hatası</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="140"/>
        <source>Delete file error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-delete-operation.cpp" line="143"/>
        <source>Invalid Operation! Can not delete &quot;%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileEnumerator</name>
    <message>
        <source>Delete file Warning</source>
        <translation type="obsolete">Dosyayı Sil Uyarısı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="563"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <source>Did not find target path, do you move or deleted it?</source>
        <translation type="obsolete">Hedef yolu bulunamadı, taşıdınız yada sildiniz mi?</translation>
    </message>
</context>
<context>
    <name>Peony::FileInfo</name>
    <message>
        <location filename="../../libpeony-qt/file-info.cpp" line="291"/>
        <source>data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::FileInfoJob</name>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="268"/>
        <source>Trash</source>
        <translation type="unfinished">Çöp</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="270"/>
        <source>Computer</source>
        <translation type="unfinished">Bilgisayar</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="272"/>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-info-job.cpp" line="274"/>
        <source>Recent</source>
        <translation type="unfinished">En son</translation>
    </message>
</context>
<context>
    <name>Peony::FileInformationLabel</name>
    <message>
        <source>File location:</source>
        <translation type="obsolete">Dosya konumu:</translation>
    </message>
    <message>
        <source>File size:</source>
        <translation type="obsolete">Dosya boyutu:</translation>
    </message>
    <message>
        <source>Modify time:</source>
        <translation type="obsolete">Değiştirme zamanı:</translation>
    </message>
</context>
<context>
    <name>Peony::FileItem</name>
    <message>
        <source>Delete file Warning</source>
        <translation type="obsolete">Dosyayı Sil Uyarısı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="225"/>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="284"/>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="296"/>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="301"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="272"/>
        <source>Open Link failed</source>
        <translation>Açık Bağlantı başarısız</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="273"/>
        <source>File not exist, do you want to delete the link file?</source>
        <translation>Dosya mevcut değil, bağlantı dosyasını silmek istiyor musunuz?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="285"/>
        <source>Can not open path &quot;%1&quot;，permission denied.</source>
        <translation>%1 yolu açılamıyor izni reddedildi.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item.cpp" line="295"/>
        <source>Can not find path &quot;%1&quot;，are you moved or renamed it?</source>
        <translation>%1 yolu bulunamıyor taşındınız mı yoksa yeniden adlandırdınız mı?</translation>
    </message>
</context>
<context>
    <name>Peony::FileItemModel</name>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="308"/>
        <source>child(ren)</source>
        <translation>Çocuklar</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="296"/>
        <source>Symbol Link, </source>
        <translation>Sembolik Bağlantı, </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="342"/>
        <source>File Name</source>
        <translation>Dosya Adı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="346"/>
        <source>Delete Date</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="351"/>
        <source>File Size</source>
        <translation>Dosya Boyutu</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="353"/>
        <source>Original Path</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="349"/>
        <source>File Type</source>
        <translation>Dosya Türü</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/file-item-model.cpp" line="347"/>
        <source>Modified Date</source>
        <translation>Değiştirme Tarihi</translation>
    </message>
</context>
<context>
    <name>Peony::FileLabelInternalMenuPlugin</name>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="181"/>
        <source>Add File Label...</source>
        <translation>Dosya Etiketi Ekle...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/menu-plugin-manager.cpp" line="204"/>
        <source>Delete All Label</source>
        <translation>Tüm Etiketleri Sil</translation>
    </message>
    <message>
        <source>Peony File Labels Menu Extension</source>
        <translation type="vanished">Peony Dosya Etiketleri Menü Uzantısı</translation>
    </message>
    <message>
        <source>Tag a File with Menu.</source>
        <translation type="vanished">Dosyayı Menü ile etiketleyin.</translation>
    </message>
</context>
<context>
    <name>Peony::FileLauchDialog</name>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="47"/>
        <source>Applications</source>
        <translation>Uygulamalar</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="48"/>
        <source>Choose an Application to open this file</source>
        <translation>Bu dosyayı açmak için bir Uygulama seçin</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="56"/>
        <source>Set as Default</source>
        <translation>Varsayılan Olarak Ayarla</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="64"/>
        <source>OK</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-lauch-dialog.cpp" line="65"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
</context>
<context>
    <name>Peony::FileLaunchAction</name>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="144"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="251"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="444"/>
        <source>Execute Directly</source>
        <translation>Doğrudan Çalıştır</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="145"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="252"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="445"/>
        <source>Execute in Terminal</source>
        <translation>Terminalde Çalıştır</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="148"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="256"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="449"/>
        <source>Detected launching an executable file %1, you want?</source>
        <translation>%1 çalıştırılabilir bir dosyayı başlatmayı mı istediniz?</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="obsolete">Dosyayı Sil Uyarısı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="166"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="289"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="482"/>
        <source>Open Failed</source>
        <translation>Açma Hatası</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="166"/>
        <source>Can not open %1, file not exist, is it deleted?</source>
        <translation>%1 açılamıyor, dosya mevcut değil, silinmiş olabilir mi?</translation>
    </message>
    <message>
        <source>File not exist, is it deleted or moved to other path?</source>
        <translation type="vanished">Dosya mevcut değil, silinmiş veya başka bir yere taşınmış olabilir</translation>
    </message>
    <message>
        <source>Can not open %1</source>
        <translation type="vanished">%1 açılamadı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="250"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="443"/>
        <source>By Default App</source>
        <translation>Varsayılan Uygulamaya Göre</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="255"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="448"/>
        <source>Launch Options</source>
        <translation>Başlatma Ayarları</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="279"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="472"/>
        <source>Open Link failed</source>
        <translation>Açık Bağlantı başarısız</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="280"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="473"/>
        <source>File not exist, do you want to delete the link file?</source>
        <translation>Dosya mevcut değil, bağlantı dosyasını silmek istiyor musunuz?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="290"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="483"/>
        <source>Can not open %1, Please confirm you have the right authority.</source>
        <translation>%1 açılamıyor, lütfen yetkilerinizi kontrol edin.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="294"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="487"/>
        <source>Open App failed</source>
        <translation>Uygulama Açılamadı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="295"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="488"/>
        <source>The linked app is changed or uninstalled, so it can not work correctly. 
Do you want to delete the link file?</source>
        <translation>Bağlı uygulama değiştirilir veya kaldırılır, bu nedenle düzgün çalışamaz.
Bağlantı dosyasını silmek istiyor musunuz?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="305"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="498"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="306"/>
        <location filename="../../libpeony-qt/file-launcher/file-launch-action.cpp" line="498"/>
        <source>Can not get a default application for opening %1, do you want open it with text format?</source>
        <translation>%1 dosyasını açmak için varsayılan bir uygulama alınamıyor, metin biçiminde açmak istiyor musunuz?</translation>
    </message>
</context>
<context>
    <name>Peony::FileLinkOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-link-operation.cpp" line="39"/>
        <location filename="../../libpeony-qt/file-operation/file-link-operation.cpp" line="42"/>
        <source>Symbolic Link</source>
        <translation>Sembolik Bağ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-link-operation.cpp" line="76"/>
        <source>Link file error</source>
        <translation>Link dosya hatası</translation>
    </message>
    <message>
        <source>Link file</source>
        <translation type="obsolete">Dosyayı bağla</translation>
    </message>
</context>
<context>
    <name>Peony::FileMoveOperation</name>
    <message>
        <source>Invalid move operation, cannot move a file itself.</source>
        <translation type="vanished">Geçersiz taşıma işlemi, bir dosyanın kendisini taşıyamaz.</translation>
    </message>
    <message>
        <source>Move file</source>
        <translation type="obsolete">Dosyayı taşı</translation>
    </message>
    <message>
        <source>Create file</source>
        <translation type="obsolete">Dosya oluştur</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="144"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="297"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="417"/>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="668"/>
        <source>Move file error</source>
        <translation>Dosya oynatma hatası</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="817"/>
        <source>Create file error</source>
        <translation>Dosya oluşturma hatası</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1090"/>
        <source>Invalid Operation.</source>
        <translation>Geçersiz İşlem.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1105"/>
        <source>File delete error</source>
        <translation>Dosya silme hatası</translation>
    </message>
    <message>
        <source>File delete</source>
        <translation type="obsolete">Dosya silme</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-move-operation.cpp" line="1107"/>
        <source>Invalid Operation</source>
        <translation>Geçersiz İşlem</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationAfterProgressPage</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="362"/>
        <source>&amp;More Details</source>
        <translation>&amp;Daha Fazla Ayrıntı</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialog</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="45"/>
        <source>File Operation Error</source>
        <translation>Dosya İşlem Hatası</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="53"/>
        <source>unkwon</source>
        <translation>Bilinmeyen</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="54"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="55"/>
        <source>null</source>
        <translation>Boş</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="57"/>
        <source>Error message:</source>
        <translation>Hata Mesajı:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="58"/>
        <source>Source File:</source>
        <translation>Kaynak Dosya:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="59"/>
        <source>Dest File:</source>
        <translation>Hedef Dosya:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="63"/>
        <source>Ignore</source>
        <translation>Görmezden Gel</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="64"/>
        <source>Ignore All</source>
        <translation>Tümünü Görmezden Gel</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="65"/>
        <source>Overwrite</source>
        <translation>Üzerine Yaz</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="66"/>
        <source>Overwrite All</source>
        <translation>Tümünün Üzerine Yaz</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="67"/>
        <source>Backup</source>
        <translation>Yedekle</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="68"/>
        <source>Backup All</source>
        <translation>Tümünü Yedekle</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="69"/>
        <source>&amp;Retry</source>
        <translation>&amp;Yeniden Dene</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialog.cpp" line="70"/>
        <source>&amp;Cancel</source>
        <translation>&amp;İptal</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogConflict</name>
    <message>
        <source>This location already contains a file with the same name.</source>
        <translation type="obsolete">Bu konum zaten aynı ada sahip bir dosya içeriyor.</translation>
    </message>
    <message>
        <source>Please select the file to keep</source>
        <translation type="obsolete">Lütfen saklanacak dosyayı seçin</translation>
    </message>
    <message>
        <source>This location already contains the file,</source>
        <translation type="vanished">Bu konum zaten dosyayı içeriyor,</translation>
    </message>
    <message>
        <source>Do you want to override it?</source>
        <translation type="vanished">Geçersiz kılmak istiyor musunuz?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="42"/>
        <source>Replace</source>
        <translation>Değiştir</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="51"/>
        <source>Ignore</source>
        <translation>Görmezden Gel</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="69"/>
        <source>Do the same</source>
        <translation>Aynısını yap</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="92"/>
        <source>&lt;p&gt;This location already contains the file &apos;%1&apos;, Do you want to override it?&lt;/p&gt;</source>
        <translation>&lt;p&gt;Bu konum zaten &apos;%1&apos; dosyasını içeriyor, onu geçersiz kılmak ister misiniz?&lt;/p&gt;</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="98"/>
        <source>Unexpected error from %1 to %2</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Then do the same thing in a similar situation</source>
        <translation type="obsolete">Sonra benzer bir durumda aynı şeyi yapın</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="60"/>
        <source>Backup</source>
        <translation>Yedekle</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Tamam</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogNotSupported</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="280"/>
        <source>Yes</source>
        <translation>Evet</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="288"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="296"/>
        <source>Do the same</source>
        <translation>Aynısını yap</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="325"/>
        <source>Make sure the disk is not full or write protected and that the file is not protected</source>
        <translation>Diskin dolu olmadığından veya yazma korumalı olmadığından ve dosyanın korumalı olmadığından emin olun</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogWarning</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="181"/>
        <source>OK</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="189"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="214"/>
        <source>Make sure the disk is not full or write protected and that the file is not protected</source>
        <translation>Diskin dolu olmadığından veya yazma korumalı olmadığından ve dosyanın korumalı olmadığından emin olun</translation>
    </message>
    <message>
        <source>Please make sure the disk is not full or not is write protected, or file is not being used.</source>
        <translation type="obsolete">Lütfen diskin dolu olmadığından veya yazmaya karşı korumalı olmadığından veya dosyanın kullanılmadığından emin olun.</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationInfo</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="718"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="720"/>
        <source>Symbolic Link</source>
        <translation>Sembolik Bağ</translation>
    </message>
    <message>
        <source> - Symbolic Link</source>
        <translation type="obsolete"> - Symbolik Link</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationManager</name>
    <message>
        <source>Delete file Warning</source>
        <translation type="obsolete">Dosyayı Sil Uyarısı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="189"/>
        <source>Warn</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="189"/>
        <source>&apos;%1&apos; is occupied，you cannot operate!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="208"/>
        <source>No, go to settings</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="209"/>
        <source>Do you want to put selected %1 item(s) into trash?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="296"/>
        <source>Can&apos;t delete.</source>
        <translation>Silinmedi.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="297"/>
        <source>You can&apos;t delete a file whenthe file is doing another operation</source>
        <translation>Dosya başka bir işlem yaparken dosyayı silemezsiniz</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="374"/>
        <source>File Operation is Busy</source>
        <translation>Dosya İşlemi Meşgul</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-manager.cpp" line="375"/>
        <source>There have been one or more fileoperation(s) executing before. Youroperation will wait for executinguntil it/them done. If you really want to execute file operations parallelly anyway, you can change the default option &quot;Allow Parallel&quot; in option menu.</source>
        <translation>Daha önce bir veya daha fazla dosya işlemi yürütülüyor. İşleminiz gerçekleşene kadar yürütülmesini bekleyecektir. Gerçekten de dosya işlemlerini paralel olarak yürütmek istiyorsanız, seçenek menüsünde varsayılan &quot;Paralele İzin Ver&quot; seçeneğini değiştirebilirsiniz.</translation>
    </message>
    <message>
        <source>There have been one or more fileoperation(s) executing before. Youroperation will wait for executinguntil it/them done.</source>
        <translation type="vanished">Daha önce bir veya daha fazla dosya işlemi yürütüldü. Operasyonunuz tamamlanana kadar yürütmeyi bekleyecektir.</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationPreparePage</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="298"/>
        <source>counting:</source>
        <translation>Hesaplama:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="299"/>
        <source>state:</source>
        <translation>Durum:</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationProgressPage</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="321"/>
        <source>&amp;More Details</source>
        <translation>&amp;Daha Fazla Detay</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="332"/>
        <source>From:</source>
        <translation>den:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="333"/>
        <source>To:</source>
        <translation>-e:</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationProgressWizard</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="55"/>
        <source>File Manager</source>
        <translation>Dosya Yöneticisi</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="59"/>
        <source>&amp;Cancel</source>
        <translation>&amp;İptal</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="68"/>
        <source>Preparing...</source>
        <translation>Hazırlanıyor...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="71"/>
        <source>Handling...</source>
        <translation>Taşınıyor...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="74"/>
        <source>Clearing...</source>
        <translation>Temizleniyor...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="77"/>
        <source>Rollbacking...</source>
        <translation>Geri sarılıyor...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="81"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="94"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="120"/>
        <source>File Operation</source>
        <translation>Dosya İşlemi</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="95"/>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="121"/>
        <source>A file operation is running backend...</source>
        <translation>Bir dosya işlemi arka uçta çalışıyor...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="160"/>
        <source>%1 files, %2</source>
        <translation>%1 dosya, %2</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="260"/>
        <source>%1 done, %2 total, %3 of %4.</source>
        <translation>%1 bitti,toplam %2, %4 de %3.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="203"/>
        <source>clearing: %1, %2 of %3</source>
        <translation>Temizlenen:%1, %3 de %2</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="248"/>
        <source>copying...</source>
        <translation>Kopyalanıyor...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-wizard.cpp" line="278"/>
        <source>Syncing...</source>
        <translation>Senkronize ediliyor...</translation>
    </message>
</context>
<context>
    <name>Peony::FilePreviewPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="218"/>
        <source>File Name:</source>
        <translation>Dosya Adı:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="223"/>
        <source>File Type:</source>
        <translation>Dosya Türü:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="227"/>
        <source>Time Access:</source>
        <translation>Erişim Zamanı:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="231"/>
        <source>Time Modified:</source>
        <translation>Değiştirme Zamanı:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="237"/>
        <source>Children Count:</source>
        <translation>Çocuk Sayısı:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="242"/>
        <source>Size:</source>
        <translation>Boyut:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="247"/>
        <source>Image resolution:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="251"/>
        <source>color model:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="312"/>
        <source>usershare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Image size:</source>
        <translation type="vanished">Resim Boyutu:</translation>
    </message>
    <message>
        <source>Image format:</source>
        <translation type="vanished">Resim Formatı:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="343"/>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="344"/>
        <source>%1x%2</source>
        <translation>%1x%2</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="396"/>
        <location filename="../../libpeony-qt/controls/preview-page/default-preview-page/default-preview-page.cpp" line="397"/>
        <source>%1 total, %2 hidden</source>
        <translation>toplam %1, %2 gizli</translation>
    </message>
</context>
<context>
    <name>Peony::FileRenameDialog</name>
    <message>
        <source>Names automatically add serial Numbers (e.g., 1,2,3...)</source>
        <translation type="obsolete">İsimler otomatik olarak seri Numaralar ekler (ör. 1,2,3 ...)</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>New file name</source>
        <translation type="obsolete">Yeni dosya adı</translation>
    </message>
    <message>
        <source>Please enter the file name</source>
        <translation type="obsolete">Lütfen dosya adını girin</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="obsolete">Tamam</translation>
    </message>
</context>
<context>
    <name>Peony::FileRenameOperation</name>
    <message>
        <source>Rename file</source>
        <translation type="obsolete">Dosyayı yeniden isimlendir</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="74"/>
        <source>File Rename error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="75"/>
        <source>Invalid file name %1%2%3 .</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="90"/>
        <source>File Rename warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="91"/>
        <source>The file %1%2%3 will be hidden when you refresh or change directory!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="166"/>
        <location filename="../../libpeony-qt/file-operation/file-rename-operation.cpp" line="196"/>
        <source>Rename file error</source>
        <translation>Dosya yeniden adlandırma hatası</translation>
    </message>
</context>
<context>
    <name>Peony::FileTrashOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="68"/>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="91"/>
        <source>trash:///</source>
        <translation>Çöp:///</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="71"/>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="94"/>
        <source>Trash file error</source>
        <translation>Çöp dosyası hatası</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="74"/>
        <source>Invalid Operation! Can not trash &quot;%1&quot;.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="102"/>
        <source>Can not trash</source>
        <translation type="unfinished">Çöp Silinemez</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="103"/>
        <source>Can not trash files more than 10GB, would you like to delete it permanently?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The user does not have read and write rights to the file &apos;%1&apos; and cannot delete it to the Recycle Bin.</source>
        <translation type="vanished">Kullanıcının &apos;%1&apos; dosyası için okuma ve yazma hakları yok ve onu Geri Dönüşüm Kutusu&apos;na silemiyor.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-trash-operation.cpp" line="171"/>
        <source>Can not trash this file, would you like to delete it permanently?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Can not trash %1, would you like to delete this file permanently?</source>
        <translation type="vanished">%1 çöp kutusuna atılamıyor, bu dosyayı kalıcı olarak silmek ister misiniz?</translation>
    </message>
    <message>
        <source>. Are you sure you want to permanently delete the file</source>
        <translation type="vanished">. Dosyayı kalıcı olarak silmek istediğinizden emin misiniz</translation>
    </message>
    <message>
        <source>Trash file</source>
        <translation type="obsolete">Çöp dosyası</translation>
    </message>
</context>
<context>
    <name>Peony::FileUntrashOperation</name>
    <message>
        <source>Untrash file</source>
        <translation type="obsolete">Dosyayı geri al</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-untrash-operation.cpp" line="157"/>
        <source>Untrash file error</source>
        <translation>Dosya geri alma hatası</translation>
    </message>
</context>
<context>
    <name>Peony::GlobalSettings</name>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="74"/>
        <location filename="../../libpeony-qt/global-settings.cpp" line="310"/>
        <source>yyyy/MM/dd</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="75"/>
        <location filename="../../libpeony-qt/global-settings.cpp" line="303"/>
        <source>HH:mm:ss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="300"/>
        <source>AP hh:mm:ss</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/global-settings.cpp" line="313"/>
        <source>yyyy-MM-dd</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::LocationBar</name>
    <message>
        <source>click the blank area for edit</source>
        <translation type="vanished">Düzenlemek için boş alanı tıklayın</translation>
    </message>
    <message>
        <source>Computer</source>
        <translation type="obsolete">Bilgisayar</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="305"/>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation>%2 de %1 ara</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="333"/>
        <source>File System</source>
        <translation>Dosya Sistemi</translation>
    </message>
    <message>
        <source>&amp;Copy Directory</source>
        <translation type="vanished">&amp;Dizini Kopyala</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="412"/>
        <source>Open In New Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="416"/>
        <source>Open In New Window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Open In New &amp;Tab</source>
        <translation type="vanished">Yeni &amp;Sekmede Aç</translation>
    </message>
    <message>
        <source>Open In &amp;New Window</source>
        <translation type="vanished">Yeni Penc&amp;erede Aç</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/location-bar/location-bar.cpp" line="410"/>
        <source>Copy Directory</source>
        <translation>Dizini Kopyala</translation>
    </message>
</context>
<context>
    <name>Peony::MountOperation</name>
    <message>
        <location filename="../../libpeony-qt/mount-operation.cpp" line="90"/>
        <source>Operation Cancelled</source>
        <translation>İşlem İptal Edildi</translation>
    </message>
</context>
<context>
    <name>Peony::NavigationToolBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="35"/>
        <source>Go Back</source>
        <translation>Geri Git</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="39"/>
        <source>Go Forward</source>
        <translation>İleri Git</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="43"/>
        <source>History</source>
        <translation>Geçmiş</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="73"/>
        <source>Clear History</source>
        <translation>Geçmişi Temizle</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="88"/>
        <source>Cd Up</source>
        <translation>Yukarı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/navigation-tool-bar.cpp" line="94"/>
        <source>Refresh</source>
        <translation>Yenile</translation>
    </message>
</context>
<context>
    <name>Peony::NewFileLaunchDialog</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="217"/>
        <source>Choose new application</source>
        <translation>Yeni uygulama seçin</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="219"/>
        <source>Choose an Application to open this file</source>
        <translation>Bu dosyayı açmak için bir Uygulama seçin</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="226"/>
        <source>apply now</source>
        <translation>Şimdi uygula</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="232"/>
        <source>OK</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="233"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
</context>
<context>
    <name>Peony::OpenWithPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="93"/>
        <source>How do you want to open %1%2 files ?</source>
        <translation>%1%2 dosyalarını nasıl açmak istersiniz?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="98"/>
        <source>Default open with:</source>
        <translation>Varsayılan şununla aç:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="117"/>
        <source>Other:</source>
        <translation>Diğer:</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="158"/>
        <source>Choose other application</source>
        <translation>Başka bir uygulama seçin</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page.cpp" line="174"/>
        <source>Go to application center</source>
        <translation>Uygulama merkezine git</translation>
    </message>
</context>
<context>
    <name>Peony::PathEdit</name>
    <message>
        <location filename="../../libpeony-qt/controls/navigation-bar/path-bar/path-edit.cpp" line="59"/>
        <source>Go To</source>
        <translation>Git</translation>
    </message>
</context>
<context>
    <name>Peony::PermissionsPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="134"/>
        <source>User or Group</source>
        <translation>Kullanıcı veya Grup</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="134"/>
        <source>Type</source>
        <translation>Tür</translation>
    </message>
    <message>
        <source>Readable</source>
        <translation type="vanished">Okunabilir</translation>
    </message>
    <message>
        <source>Writeable</source>
        <translation type="vanished">Yazılabilir</translation>
    </message>
    <message>
        <source>Excuteable</source>
        <translation type="vanished">Çalıştırılabilir</translation>
    </message>
    <message>
        <source>File: %1</source>
        <translation type="vanished">Dosya:%1</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="80"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="156"/>
        <source>Target: %1</source>
        <translation></translation>
    </message>
    <message>
        <source>Read and Write</source>
        <translation type="vanished">Okuma ve Yazma</translation>
    </message>
    <message>
        <source>Readonly</source>
        <translation type="vanished">Salt Okunur</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="134"/>
        <source>Read</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="134"/>
        <source>Write</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="134"/>
        <source>Executable</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="178"/>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="187"/>
        <source>Can not get the permission info.</source>
        <translation>İzin bilgileri alınamıyor.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="248"/>
        <source>(Me)</source>
        <translation>(Ben)</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="317"/>
        <source>Others</source>
        <translation>Diğerleri</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="320"/>
        <source>Owner</source>
        <translation>Sahibi</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="322"/>
        <source>Group</source>
        <translation>Grup</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="324"/>
        <source>Other</source>
        <translation>Diğer</translation>
    </message>
    <message>
        <source>Other Users</source>
        <translation type="vanished">Diğer Kullanıcılar</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="335"/>
        <source>You can not change the access of this file.</source>
        <translation>Bu dosyanın erişimini değiştiremezsiniz.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="339"/>
        <source>Me</source>
        <translation>Ben</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page.cpp" line="343"/>
        <source>User</source>
        <translation>Kullanıcı</translation>
    </message>
</context>
<context>
    <name>Peony::PropertiesWindow</name>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="275"/>
        <source>Trash</source>
        <translation>Çöp</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="279"/>
        <source>Recent</source>
        <translation>En son</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="287"/>
        <source>Selected</source>
        <translation>Seçilen</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="287"/>
        <source> %1 Files</source>
        <translation> %1 Dosyalar</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="293"/>
        <source>usershare</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="300"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="307"/>
        <source>Properties</source>
        <translation>Özellikler</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="403"/>
        <source>Ok</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/properties-window.cpp" line="404"/>
        <source>Cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="vanished">Kapat</translation>
    </message>
</context>
<context>
    <name>Peony::RecentAndTrashPropertiesPage</name>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="117"/>
        <source>Show confirm dialog while trashing.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="140"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="146"/>
        <source>Origin Path: </source>
        <translation>Kaynak Yolu: </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="178"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="211"/>
        <source>Deletion Date: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="167"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="226"/>
        <source>Size: </source>
        <translation>Boyut: </translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="220"/>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page.cpp" line="227"/>
        <source>Original Location: </source>
        <translation>Orjinal Konum: </translation>
    </message>
</context>
<context>
    <name>Peony::SearchBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="47"/>
        <source>Input the search key of files you would like to find.</source>
        <translation>Bulmak istediğiniz dosyaların arama anahtarını girin.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="83"/>
        <source>Input search key...</source>
        <translation>Arama anahtarını girin...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="121"/>
        <source>advance search</source>
        <translation>Gelişmiş arama</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar.cpp" line="122"/>
        <source>clear record</source>
        <translation>Kaydı temizle</translation>
    </message>
</context>
<context>
    <name>Peony::SearchBarContainer</name>
    <message>
        <source>Choose File Type</source>
        <translation type="vanished">Dosya Türü Seç</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.cpp" line="70"/>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.cpp" line="200"/>
        <source>Clear</source>
        <translation>Temizle</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="88"/>
        <source>all</source>
        <translation>Tümü</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="88"/>
        <source>file folder</source>
        <translation>Dosya klasörü</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="88"/>
        <source>image</source>
        <translation>Resim</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="89"/>
        <source>video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="89"/>
        <source>text file</source>
        <translation>Metin dosyası</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="89"/>
        <source>audio</source>
        <translation>Ses</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="89"/>
        <source>others</source>
        <translation>Diğerleri</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/search-bar-container.h" line="89"/>
        <source>wps file</source>
        <translation>Wps Dosyası</translation>
    </message>
</context>
<context>
    <name>Peony::SharedFileLinkOperation</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/shared-file-link-operation.cpp" line="44"/>
        <location filename="../../libpeony-qt/file-operation/shared-file-link-operation.cpp" line="47"/>
        <source>Symbolic Link</source>
        <translation type="unfinished">Sembolik Bağ</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarFavoriteItem</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-favorite-item.cpp" line="86"/>
        <source>Favorite</source>
        <translation>Sık Kullanılan</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarFileSystemItem</name>
    <message>
        <source>Computer</source>
        <translation type="vanished">Bilgisayar</translation>
    </message>
    <message>
        <source>File System</source>
        <translation type="vanished">Dosya Sistemi</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="169"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SideBarMenu</name>
    <message>
        <source>&amp;Properties</source>
        <translation type="vanished">Özellikler</translation>
    </message>
    <message>
        <source>P&amp;roperties</source>
        <translation type="obsolete">Özellikler</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="54"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="77"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="103"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="118"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="228"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="275"/>
        <source>Properties</source>
        <translation>Özellikler</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="88"/>
        <source>Delete Symbolic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="134"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="142"/>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="269"/>
        <source>Unmount</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="151"/>
        <source>Eject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&amp;Delete Symbolic</source>
        <translation type="vanished">Sembolik B&amp;ağı Sil</translation>
    </message>
    <message>
        <source>&amp;Unmount</source>
        <translation type="vanished">Ba&amp;ğı Kaldır</translation>
    </message>
    <message>
        <source>&amp;Eject</source>
        <translation type="vanished">&amp;Çıkar</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/menu/side-bar-menu/side-bar-menu.cpp" line="195"/>
        <source>format</source>
        <translation>Biçim</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarModel</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-model.cpp" line="98"/>
        <source>Network</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::SideBarPersonalItem</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-personal-item.cpp" line="42"/>
        <source>Personal</source>
        <translation>Kişisel</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarSeparatorItem</name>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-separator-item.h" line="68"/>
        <source>(No Sub Directory)</source>
        <translation>(Alt Dizin Yok)</translation>
    </message>
</context>
<context>
    <name>Peony::StatusBar</name>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="94"/>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="100"/>
        <source>; %1 folders</source>
        <translation>; %1 klasör</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="95"/>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="102"/>
        <source>; %1 files, %2 total</source>
        <translation>; %1 dosya, toplam %2</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="97"/>
        <source>; %1 folder</source>
        <translation>; %1 klasör</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="98"/>
        <source>; %1 file, %2</source>
        <translation>; %1 dosya, %2</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/status-bar/status-bar.cpp" line="105"/>
        <source>%1 selected</source>
        <translation>%1 seçildi</translation>
    </message>
</context>
<context>
    <name>Peony::SyncThread</name>
    <message>
        <location filename="../../libpeony-qt/sync-thread.cpp" line="44"/>
        <source>notify</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>Peony::ToolBar</name>
    <message>
        <source>Open in new &amp;Window</source>
        <translation type="vanished">Yeni Pencerede Aç</translation>
    </message>
    <message>
        <source>Open in &amp;New window</source>
        <translation type="vanished">Yeni Pen&amp;cerede Aç</translation>
    </message>
    <message>
        <source>Open in new &amp;Tab</source>
        <translation type="vanished">Yeni S&amp;ekmede Aç</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="138"/>
        <source>Sort Type</source>
        <translation>Sıralama Türü</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="140"/>
        <source>File Name</source>
        <translation>Dosya Adı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="146"/>
        <source>File Type</source>
        <translation>Dosya Türü</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="149"/>
        <source>File Size</source>
        <translation>Dosya Boyutu</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="143"/>
        <source>Modified Date</source>
        <translation>Değiştirme Tarihi</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="72"/>
        <source>Open in New window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="74"/>
        <source>Open in new Tab</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="160"/>
        <source>Ascending</source>
        <translation>Artıyor</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="156"/>
        <source>Descending</source>
        <translation>Azalıyor</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="190"/>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="347"/>
        <source>Copy</source>
        <translation>Kopyala</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="193"/>
        <source>Paste</source>
        <translation>Yapıştır</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="196"/>
        <source>Cut</source>
        <translation>Kes</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="199"/>
        <source>Trash</source>
        <translation>Çöp</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="216"/>
        <source>Clean Trash</source>
        <translation>Çöpü Temizle</translation>
    </message>
    <message>
        <source>Delete file Warning</source>
        <translation type="obsolete">Dosyayı Sil Uyarısı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="218"/>
        <source>Delete Permanently</source>
        <translation>Kalıcı Olarak Sil</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="218"/>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation>Bu dosyaları silmek istediğinizden emin misiniz? Bir silme işlemini başlattığınızda, silinen dosyalar bir daha geri yüklenmeyecektir.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="229"/>
        <source>Restore</source>
        <translation>Onar</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="278"/>
        <source>Options</source>
        <translation>Seçenekler</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="281"/>
        <source>Forbid Thumbnail</source>
        <translation>Küçük Resmi Yasakla</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="288"/>
        <source>Show Hidden</source>
        <translation>Gizli Dosyaları Göster</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="295"/>
        <source>Resident in Backend</source>
        <translation>Arka Uçta Yerleşik</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="296"/>
        <source>Let the program still run after closing the last window. This will reduce the time for the next launch, but it will also consume resources in backend.</source>
        <translation>Son pencereyi kapattıktan sonra programın çalışmaya devam etmesine izin verin. Bu, bir sonraki lansman süresini azaltır, ancak arka uçtaki kaynakları da tüketir.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="308"/>
        <source>&amp;Help</source>
        <translation>&amp;Yardım</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="314"/>
        <source>&amp;About...</source>
        <translation>&amp;Hakkında...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="316"/>
        <source>Peony Qt</source>
        <translation>Dosya Yönetici</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/tool-bar/tool-bar.cpp" line="317"/>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation>Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</translation>
    </message>
    <message>
        <source>Author: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</translation>
    </message>
    <message>
        <source>Authour: 
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation type="vanished">Yazar:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</translation>
    </message>
</context>
<context>
    <name>Peony::VolumeManager</name>
    <message>
        <location filename="../../libpeony-qt/volume-manager.cpp" line="150"/>
        <source>Error</source>
        <translation>Hata</translation>
    </message>
</context>
<context>
    <name>ProgressBar</name>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="737"/>
        <source>starting ...</source>
        <translation>Başlatılıyor...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="824"/>
        <source>canceling ...</source>
        <translation>İptal ediliyor...</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-progress-bar.cpp" line="826"/>
        <source>sync ...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>cancel file operation</source>
        <translation type="vanished">Dosya işlemini iptal et</translation>
    </message>
    <message>
        <source>Are you sure want to cancel the current selected file operation</source>
        <translation type="vanished">Mevcut seçili dosya işlemini iptal etmek istediğinizden emin misiniz</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">Tamam</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">İptal</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="40"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="60"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="91"/>
        <source>Icon View</source>
        <translation>Simge Görünümü</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="46"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/icon-view-factory.h" line="97"/>
        <source>Show the folder children as icons.</source>
        <translation>Klasörleri simgeler olarak göster.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="42"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="62"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="93"/>
        <source>List View</source>
        <translation>Liste Görünümü</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="48"/>
        <location filename="../../libpeony-qt/controls/directory-view/directory-view-factory/list-view-factory.h" line="99"/>
        <source>Show the folder children as rows in a list.</source>
        <translation>Alt klasörü bir listede satır olarak göster.</translation>
    </message>
    <message>
        <source>Basic Preview Page</source>
        <translation type="vanished">Temel Önizleme Sayfası</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page-factory.h" line="40"/>
        <source>Basic</source>
        <translation>Temel</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/basic-properties-page-factory.h" line="46"/>
        <source>Show the basic file properties, and allow you to modify the access and name.</source>
        <translation>Temel dosya özelliklerini gösterin ve erişimi ve adı değiştirmenize izin verin.</translation>
    </message>
    <message>
        <source>Permissions Page</source>
        <translation type="vanished">İzinler Sayfası</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page-factory.h" line="41"/>
        <source>Permissions</source>
        <translation>İzinler</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/permissions-properties-page-factory.h" line="47"/>
        <source>Show and modify file&apos;s permission, owner and group.</source>
        <translation>Dosya iznini, sahibini ve grubunu göster ve değiştir.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="177"/>
        <source>Can not trash</source>
        <translation>Çöp Silinemez</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="171"/>
        <source>Can not trash these files. You can delete them permanently. Are you sure doing that?</source>
        <translation>Bu dosyalar çöp kutusuna gönderilemiyor. Bunları kalıcı olarak silebilirsiniz. Bunu yaptığınızdan emin misiniz?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="175"/>
        <source>Can not trash files more than 10GB, would you like to delete it permanently?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="295"/>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="297"/>
        <source>Delete Permanently</source>
        <translation>Kalıcı Olarak Sil</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="295"/>
        <location filename="../../libpeony-qt/convenient-utils/file-operation-utils.cpp" line="297"/>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="unfinished">Bu dosyaları silmek istediğinizden emin misiniz? Bir silme işlemini başlattığınızda, silinen dosyalar bir daha geri yüklenmeyecektir.</translation>
    </message>
    <message>
        <source>Are you sure that you want to delete %1? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="vanished">%1&apos;i silmek istediğinizden emin misiniz? Bir silme işlemini başlattığınızda, silinen dosyalar bir daha geri yüklenmeyecektir.</translation>
    </message>
    <message>
        <source>Are you sure that you want to delete these %1 files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="vanished">Bu %1 dosyayı silmek istediğinizden emin misiniz? Bir silme işlemini başlattığınızda, silinen dosyalar bir daha geri yüklenmeyecektir.</translation>
    </message>
    <message>
        <source>Computer Properties Page</source>
        <translation type="vanished">Bilgisayar Özellikleri Sayfası</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page-factory.h" line="41"/>
        <source>Computer Properties</source>
        <translation>Bilgisayar özellikleri</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/computer-properties-page-factory.h" line="47"/>
        <source>Show the computer properties or items in computer.</source>
        <translation>Bilgisayar özelliklerini veya bilgisayardaki öğeleri gösterir.</translation>
    </message>
    <message>
        <source>Trash and Recent Properties Page</source>
        <translation type="vanished">Çöp Kutusu ve Son Özellikler Sayfası</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page-factory.h" line="40"/>
        <source>Trash and Recent</source>
        <translation>Çöp Kutusu ve En Son</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/recent-and-trash-properties-page-factory.h" line="46"/>
        <source>Show the file properties or items in trash or recent.</source>
        <translation>Dosya özelliklerini veya öğelerini çöp kutusunda veya son zamanlarda gösterin.</translation>
    </message>
    <message>
        <source>eject device failed</source>
        <translation type="obsolete">Aygıtı çıkarma başarısız oldu</translation>
    </message>
    <message>
        <source>Please check whether the device is occupied and then eject the device again</source>
        <translation type="obsolete">Lütfen cihazın dolu olup olmadığını kontrol edin ve ardından cihazı tekrar çıkarın</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="382"/>
        <source>Format failed</source>
        <translation>Biçim hatalı</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="384"/>
        <source>YES</source>
        <translation>EVET</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="818"/>
        <source>Formatting successful! But failed to set the device name.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="832"/>
        <source>qmesg_notify</source>
        <translation type="unfinished">Qmesg bildir</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="816"/>
        <source>Format operation has been finished successfully.</source>
        <translation type="unfinished">Biçimlendirme işlemi başarıyla tamamlandı.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="832"/>
        <source>Sorry, the format operation is failed!</source>
        <translation type="unfinished">Üzgünüz, formatlama işlemi başarısız oldu!</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="845"/>
        <source>Formatting this volume will erase all data on it. Please backup all retained data before formatting. Do you want to continue ?</source>
        <translation type="unfinished">Bu birimi biçimlendirmek, içindeki tüm verileri silecektir. Biçimlendirmeden önce lütfen saklanan tüm verileri yedekleyin. Devam etmek istiyor musun?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="816"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="818"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="847"/>
        <source>format</source>
        <translation type="unfinished">Biçim</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="849"/>
        <source>begin format</source>
        <translation type="unfinished">biçimlendirmeye başla</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="851"/>
        <source>close</source>
        <translation type="unfinished">Kapat</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/sync-thread.cpp" line="41"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="751"/>
        <source>File Manager</source>
        <translation>Dosya Yöneticisi</translation>
    </message>
    <message>
        <source>Default search vfs of peony</source>
        <translation type="obsolete">Peony için varsayılan arama vfs&apos;si</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1449"/>
        <source>Force unmount failed</source>
        <translation>Zorla ayrılma başarısız oldu</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="129"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1449"/>
        <location filename="../../libpeony-qt/windows/format_dialog.cpp" line="380"/>
        <source>Error: %1
</source>
        <translation>Hata: %1
</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="135"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1452"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1502"/>
        <source>Data synchronization is complete,the device has been unmount successfully!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="124"/>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="129"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1481"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1484"/>
        <source>Unmount failed</source>
        <translation>Ayrılma Hatası</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1469"/>
        <source>Not authorized to perform operation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-net-work-item.cpp" line="124"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1481"/>
        <source>Unable to unmount it, you may need to close some programs, such as: GParted etc.</source>
        <translation>Bağlantısını kesemiyorsanız, bazı programları kapatmanız gerekebilir, örneğin: GParted vb.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1484"/>
        <source>Error: %1
Do you want to unmount forcely?</source>
        <translation>Hata: %1
Zorla ayrılmak ister misin?</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1175"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1207"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1469"/>
        <source>Eject failed</source>
        <translation>Çıkarma hatalı</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">İptal</translation>
    </message>
    <message>
        <source>Eject Anyway</source>
        <translation type="vanished">Yine de Çıkar</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="993"/>
        <source>The device has been mount successfully!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1180"/>
        <location filename="../../libpeony-qt/volumeManager.cpp" line="1213"/>
        <source>Data synchronization is complete and the device can be safely unplugged!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unable to eject %1</source>
        <translation type="vanished">%1 çıkarılamıyor</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="261"/>
        <source>favorite</source>
        <translation>favori</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="294"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="299"/>
        <source>File is not existed.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="307"/>
        <source>Share Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="353"/>
        <source>Operation not supported</source>
        <translation>İşlem desteklenmedi</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="447"/>
        <source>The virtual file system does not support folder creation</source>
        <translation>Sanal dosya sistemi klasör oluşturmayı desteklemiyor</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="519"/>
        <source>Can not create a symbolic file for vfs location</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="526"/>
        <source>Symbolic Link</source>
        <translation type="unfinished">Sembolik Bağ</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="538"/>
        <source>Can not create symbolic file here, %1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="547"/>
        <source>Can not add a file to favorite directory.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="605"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="613"/>
        <source>The virtual file system cannot be opened</source>
        <translation>Sanal dosya sistemi açılamaz</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="434"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="462"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="477"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="563"/>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-file.cpp" line="581"/>
        <source>Virtual file directories do not support move and copy operations</source>
        <translation>Sanal dosya dizinleri taşıma ve kopyalama işlemlerini desteklemez</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/vfs/favorite-vfs-register.h" line="43"/>
        <source>Default favorite vfs of peony</source>
        <translation>Peony varsayılan favori vfs</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/mark-properties-page-factory.h" line="40"/>
        <source>Mark</source>
        <translation>İşaretle</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/mark-properties-page-factory.h" line="46"/>
        <source>mark this file.</source>
        <translation>bu dosyayı işaretle.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page-factory.h" line="40"/>
        <source>Open With</source>
        <translation>İle aç</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/open-with-properties-page-factory.h" line="46"/>
        <source>open with.</source>
        <translation>ile aç.</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page-factory.h" line="38"/>
        <location filename="../../libpeony-qt/controls/property-page/details-properties-page-factory.h" line="44"/>
        <source>Details</source>
        <translation>Detaylar</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/sync-thread.cpp" line="11"/>
        <source>It need to synchronize before operating the device,place wait!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="407"/>
        <source>permission denied</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="400"/>
        <location filename="../../libpeony-qt/file-enumerator.cpp" line="413"/>
        <source>file not found</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="281"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="298"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="314"/>
        <location filename="../../libpeony-qt/file-operation/file-copy-operation.cpp" line="490"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="155"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="177"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="199"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="206"/>
        <location filename="../../libpeony-qt/file-utils.cpp" line="222"/>
        <source>duplicate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-utils.cpp" line="345"/>
        <source>data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="186"/>
        <location filename="../../libpeony-qt/vfs/search-vfs-uri-parser.cpp" line="110"/>
        <source>Computer</source>
        <translation type="unfinished">Bilgisayar</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="249"/>
        <source>File System</source>
        <translation type="unfinished">Dosya Sistemi</translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/model/side-bar-file-system-item.cpp" line="254"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="379"/>
        <source>Failed to open file &quot;%1&quot;: insufficient permissions.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../../libpeony-qt/file-operation/file-operation-error-dialogs.cpp" line="390"/>
        <source>File “%1” does not exist. Please check whether the file has been deleted.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
