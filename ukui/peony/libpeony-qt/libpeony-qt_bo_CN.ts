<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>ConnectServerDialog</name>
    <message>
        <source>Ok</source>
        <translation>གཏན་འཁེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>User</source>
        <translation>སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <source>Save Password</source>
        <translation>གསང་ཨང་ཉར་བ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>Domain</source>
        <translation>ཁོངས་མིང་།</translation>
    </message>
    <message>
        <source>Connect to Sever</source>
        <translation>ཞབས་ཞུའི་འཕྲུལ་ཆས་ལ་སྦྲེལ་བ།</translation>
    </message>
    <message>
        <source>Anonymous</source>
        <translation>རང་མིང་མི་འགོད་པ།</translation>
    </message>
    <message>
        <source>Password</source>
        <translation>གསང་ཨང་།</translation>
    </message>
</context>
<context>
    <name>DiscControl</name>
    <message>
        <source> is busy!</source>
        <translation> བྲེལ་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>is busy!</source>
        <translation>བྲེལ་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source> not support udf at present.</source>
        <translation> མིག་སྔར་udfརྣམ་གཞག་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <source>unmount disc failed before udf format.</source>
        <translation>udfའོད་སྡེར་རྣམ་པར་འཇོག་པའི་སྔོན་ལ་ཕྱིར་ལེན་ཕམ་པ།</translation>
    </message>
    <message>
        <source>is not properly formatted.</source>
        <translation>ཆ་ཚང་རྣམ་པར་འཇོག་མ་ཐུབ།</translation>
    </message>
    <message>
        <source>Can not found newfs_udf tool.</source>
        <translation>newfs_udfལག་ཆ་རྙེད་ཐབས་བྲེལ།</translation>
    </message>
    <message>
        <source>preparation failed before DVD-RW udf format.</source>
        <translation>DVD-RWཡི་འོད་སྡེར་udfརྣམ་གཞག་ཏུ་གྱུར་བར་གྲ་སྒྲིག་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>FileLabelModel</name>
    <message>
        <source>Red</source>
        <translation>དམར་པོ།</translation>
    </message>
    <message>
        <source>Blue</source>
        <translation>སྔོན་པོ།</translation>
    </message>
    <message>
        <source>Gray</source>
        <translation>སྐྱ་ཚད།</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>Green</source>
        <translation>ལྗང་མདོག</translation>
    </message>
    <message>
        <source>Orange</source>
        <translation>ཚ་ལུ་མའི་མདོག</translation>
    </message>
    <message>
        <source>Purple</source>
        <translation>སྨུག་པོ།</translation>
    </message>
    <message>
        <source>Yellow</source>
        <translation>སེར་པོ།</translation>
    </message>
    <message>
        <source>Label or color is duplicated.</source>
        <translation>མཚོན་རྟགས་སམ་ཁ་དོག་བསྐྱར་ཟློས།</translation>
    </message>
</context>
<context>
    <name>Format_Dialog</name>
    <message>
        <source>ok</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>ext4</source>
        <translation>ext4</translation>
    </message>
    <message>
        <source>ntfs</source>
        <translation>NTFS</translation>
    </message>
    <message>
        <source>vfat</source>
        <translation type="vanished">VFAT</translation>
    </message>
    <message>
        <source>close</source>
        <translation>ཁ་རྒྱག</translation>
    </message>
    <message>
        <source>Dialog</source>
        <translation>རྣམ་བཞག་ཅན།</translation>
    </message>
    <message>
        <source>clean it total</source>
        <translation>ཡོངས་སུ་སུབ་པ། (དུས་ཡུན་ཅུང་རིང་། གཏན་འཁེལ་གནང་རོགས།)</translation>
    </message>
    <message>
        <source>rom_size</source>
        <translation>ཤོང་ཚད་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>system</source>
        <translation>ཡིག་ཆའི་རྒྱུད་ཁོངས།</translation>
    </message>
    <message>
        <source>device_name</source>
        <translation>སྒྲིག་ཆས་མིང་།</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation>ཤོང་ཚད།</translation>
    </message>
    <message>
        <source>vfat/fat32</source>
        <translation>vfat/fat32</translation>
    </message>
    <message>
        <source>exfat</source>
        <translation>exfat</translation>
    </message>
    <message>
        <source>Formatting. Do not close this window</source>
        <translation>རྣམ་གཞག་བཟོ་བ།ཞིན་ཡོད་པས་ཁ་རྒྱག་མི་རུང་།</translation>
    </message>
    <message>
        <source>Format</source>
        <translation>རྣམ་གཞག</translation>
    </message>
    <message>
        <source>Rom size:</source>
        <translation>ཤོང་ཚད་ཆེ་ཆུང་ནི།</translation>
    </message>
    <message>
        <source>Filesystem:</source>
        <translation>ཡིག་ཚགས་མ་ལག་ནི།</translation>
    </message>
    <message>
        <source>Disk name:</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <source>Completely erase(Time is longer, please confirm!)</source>
        <translation>རྦད་དེ་མེད་པར་བཟོ་དགོས། (དུས་ཚོད་ཅུང་རིང་བས་ཁྱེད་ཀྱིས་གཏན་འཁེལ་གནང་རོགས། )</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ཁ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>Data</source>
        <translation>གཞི་གྲངས་སྡེར།</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>Block not existed!</source>
        <translation>བཀག་སྡོམ་བྱས་མེད་པ་རེད།</translation>
    </message>
    <message>
        <source>Set password</source>
        <translation>གསང་གྲངས་གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Set password for volume based on LUKS (only ext4)</source>
        <translation>LUKS(ext4)རྨང་གཞིར་བྱས་པའི་གྲངས་འབོར་གྱི་གསང་གྲངས་གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Enter Password:</source>
        <translation>གསང་གྲངས་ནང་འཇུག་བྱ་རྒྱུ་སྟེ།</translation>
    </message>
    <message>
        <source>Password too short, please retype a password more than 6 characters</source>
        <translation>གསང་གྲངས་ཐུང་དྲགས་པས་ཡི་གེ་6ལས་བརྒལ་བའི་གསང་གྲངས་བསྐྱར་དུ་པར་རྒྱག་རོགས།</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="unfinished">ཉེན་བརྡ་བཏང་བ།</translation>
    </message>
    <message>
        <source>Device name cannot start with a decimal point, Please re-enter!</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་སིལ་གྲངས་ཚེག་གིས་འགོ་འཛུགས་མི་རུང་། བསྐྱར་དུ་ནང་འཇུག་གནང་རོགས།</translation>
    </message>
</context>
<context>
    <name>KyFileDialogRename</name>
    <message>
        <source>Renaming &quot;%1&quot;</source>
        <translation>མིང་བསྐྱར་འདོགས་བྱེད་བཞིན་ཡོད། &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Renaming failed, the reason is: %1</source>
        <translation>མིང་བསྐྱར་དུ་བཏགས་པ་ཕམ་སོང་།རྒྱུ་མཚན་ནི།: %1</translation>
    </message>
    <message>
        <source>Filename too long</source>
        <translation>ཡིག་ཆའི་མིང་རིང་དྲགས་པ།</translation>
    </message>
    <message>
        <source>Copying &quot;%1&quot;</source>
        <translation>འདྲ་ཕམ་བྱེད་བཞིན་ཡོད། &quot;%1&quot;</translation>
    </message>
    <message>
        <source>To &quot;%1&quot;</source>
        <translation>&quot;%1&quot;ལ་སླེབས་པ།</translation>
    </message>
    <message>
        <source>Copying failed, the reason is: %1</source>
        <translation>འདྲ་ཕབ་ཕམ་སོང་།རྒྱུ་མཚན་ནི།: %1</translation>
    </message>
    <message>
        <source>Moving &quot;%1&quot;</source>
        <translation>སྤོ་འགུལ་བྱེད་བཞིནཡོད། &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Moving failed, the reason is: %1</source>
        <translation>སྤོ་འགུལ་ཕམ་སོང་།རྒྱུ་མཚན་ནི: %1</translation>
    </message>
    <message>
        <source>File operation error:</source>
        <translation>ཡིག་ཆ་བཀོལ་སྤྱོད་ནོར་འཁྲུལ།:</translation>
    </message>
    <message>
        <source>The reason is: %1</source>
        <translation>རྒྱུ་མཚན་ནི།: %1</translation>
    </message>
    <message>
        <source>Skip</source>
        <translation>མཆོངས་པ།</translation>
    </message>
    <message>
        <source>Skip All</source>
        <translation>མ་ཚང་མཆོངས་པ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>དོར་བ།</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="unfinished">མིང་བསྐྱར་འདོགས།</translation>
    </message>
    <message>
        <source>Please enter a new name</source>
        <translation>ཡིག་ཆ་གསར་བའི་མིང་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
</context>
<context>
    <name>MainProgressBar</name>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>དོར་བ།</translation>
    </message>
    <message>
        <source>Are you sure want to cancel all file operations</source>
        <translation>ཁྱོས་ཡིག་ཆའི་བཀོལ་སྤྱོད་ཐམས་ཅད་རྩིས་མེད་གཏང་རྒྱུ་ཡིན་ཡིན་ནམ།</translation>
    </message>
    <message>
        <source>canceling ...</source>
        <translation>འདོར་བ།...</translation>
    </message>
    <message>
        <source>starting ...</source>
        <translation>ད་ལྟ་འགོ་ཚུགས་བཞིན་འདུག་་་</translation>
    </message>
    <message>
        <source>cancel all file operations</source>
        <translation>ཡིག་ཆ་བཀོལ་སྤྱོད་ཡོད་ཚད་རྩིས་མེད།</translation>
    </message>
    <message>
        <source>File operation</source>
        <translation>ཡིག་ཆ་བཀོལ་སྤྱོད།</translation>
    </message>
    <message>
        <source>sync ...</source>
        <translation>དུས་མཉམ་ ...</translation>
    </message>
</context>
<context>
    <name>OtherButton</name>
    <message>
        <source>Other queue</source>
        <translation>རིམ་བསྟར་གཞན།</translation>
    </message>
</context>
<context>
    <name>Peony::AdvanceSearchBar</name>
    <message>
        <source>all</source>
        <translation type="vanished">ཚང་མ།</translation>
    </message>
    <message>
        <source>start search</source>
        <translation>འཚོལ་བཤེར་འགོ་བརྩམས།</translation>
    </message>
    <message>
        <source>this year</source>
        <translation type="vanished">ད་ལོ།</translation>
    </message>
    <message>
        <source>this week</source>
        <translation type="vanished">གཟའ་འདི།</translation>
    </message>
    <message>
        <source>audio</source>
        <translation type="vanished">སྒྲ་ཟློས།</translation>
    </message>
    <message>
        <source>image</source>
        <translation type="vanished">པར་རིས།།</translation>
    </message>
    <message>
        <source>today</source>
        <translation type="vanished">དེ་རིང་།</translation>
    </message>
    <message>
        <source>video</source>
        <translation type="vanished">བརྙན་ལམ།</translation>
    </message>
    <message>
        <source>File Size</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>File Type</source>
        <translation>ཡིག་ཆའི་རིགས་གྲས།</translation>
    </message>
    <message>
        <source>file name</source>
        <translation>ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <source>show hidden file</source>
        <translation>སྦས་པའི་ཡིག་ཆ་མངོན་པ།</translation>
    </message>
    <message>
        <source>input key words...</source>
        <translation>གནད་ཚིག་ནང་འཇུག</translation>
    </message>
    <message>
        <source>Have no key words or search location!</source>
        <translation>གནད་ཡིག་དང་རྒྱུ་ལམ་མི་འདུག</translation>
    </message>
    <message>
        <source>Choose file size</source>
        <translation>ཡིག་ཆའིཆེ་ཆུང་འདེམ་པ།</translation>
    </message>
    <message>
        <source>Choose File Type</source>
        <translation>ཡིག་ཆའི་རིགས་འདེམ་པ།</translation>
    </message>
    <message>
        <source>medium(1M-100M)</source>
        <translation type="vanished">འབྲིང(1M-100M)</translation>
    </message>
    <message>
        <source>this month</source>
        <translation type="vanished">ཟླ་འདི།</translation>
    </message>
    <message>
        <source>big(100M-1G)</source>
        <translation type="vanished">ཅུང་ཆེ(100M-1G)</translation>
    </message>
    <message>
        <source>browse</source>
        <translation>མིག་བཤར།</translation>
    </message>
    <message>
        <source>file folder</source>
        <translation type="vanished">ཡིག་ཁུག</translation>
    </message>
    <message>
        <source>hidden advance search page</source>
        <translation>མཐོ་རིམ་འཚོལ་བཤེར་ངོས་གབ་པ།</translation>
    </message>
    <message>
        <source>wps file</source>
        <translation type="vanished">WPSཡིག་ཆ།</translation>
    </message>
    <message>
        <source>others</source>
        <translation type="vanished">གཞན་དག</translation>
    </message>
    <message>
        <source>search</source>
        <translation>བཤེར་འཚོལ།</translation>
    </message>
    <message>
        <source>Select path</source>
        <translation>རྒྱུ་ལམ་གདམ་གསེས།</translation>
    </message>
    <message>
        <source>content</source>
        <translation>ནང་དོན།</translation>
    </message>
    <message>
        <source>Key Words</source>
        <translation>གནད་ཚིག</translation>
    </message>
    <message>
        <source>choose search path...</source>
        <translation>འཚོལ་གནས་གདམ་པ།</translation>
    </message>
    <message>
        <source>tiny(0-16K)</source>
        <translation type="vanished">ཆེས་ཆུང(0-16K)</translation>
    </message>
    <message>
        <source>year ago</source>
        <translation type="vanished">ལོ་གཅིག་གི་སྔོན།</translation>
    </message>
    <message>
        <source>Search Location</source>
        <translation>རྒྱུ་ལམ་འཚོལ་བཤེར།</translation>
    </message>
    <message>
        <source>large(&gt;1G)</source>
        <translation type="vanished">ཆེས་ཆེ(&gt;1G)</translation>
    </message>
    <message>
        <source>Modify Time</source>
        <translation>དུས་ཚོད་བཟོ་བཅོས།</translation>
    </message>
    <message>
        <source>go back</source>
        <translation>ཕྱིར་ལོག</translation>
    </message>
    <message>
        <source>Choose Modify Time</source>
        <translation>དུས་ཚོད་བཅོས་བསྒྱུར་གདམ་པ།</translation>
    </message>
    <message>
        <source>small(16k-1M)</source>
        <translation type="vanished">ཆུང་ཆུང་(16k-1M</translation>
    </message>
    <message>
        <source>Operate Tips</source>
        <translation>གསལ་བརྡ།</translation>
    </message>
    <message>
        <source>Search file name or content at least choose one!</source>
        <translation>ཡིག་ཆའི་མིང་དང་ནང་དོན་འཚོལ་བཤེར་མ་མཐའ་ཡང་གཅིག་གཏན་འཁེལ་དགོས།</translation>
    </message>
    <message>
        <source>text file</source>
        <translation type="vanished">ཡིག་རྐྱང་ཡིག་ཆ།</translation>
    </message>
</context>
<context>
    <name>Peony::AdvancedLocationBar</name>
    <message>
        <source>Search Content...</source>
        <translation>འཚོལ་བཤེར་ནང་དོན།</translation>
    </message>
</context>
<context>
    <name>Peony::AllFileLaunchDialog</name>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Choose new application</source>
        <translation>ཉེར་སྤྱོད་གསར་བ་ཞིག་འདེམ་པ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>apply now</source>
        <translation>མྱུར་དུ་ཉེར་སྤྱོད།</translation>
    </message>
    <message>
        <source>Choose an Application to open this file</source>
        <translation>ཉེར་སྤྱོད་ཞིག་གདམ་པ་དང་ཡིག་ཆ་འདི་ཁ་ཕྱེ་བ།</translation>
    </message>
</context>
<context>
    <name>Peony::BasicPropertiesPage</name>
    <message>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <source>move</source>
        <translation>སྤོ་བ།</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>Size:</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>Type:</source>
        <translation>རིགས་གྲས།</translation>
    </message>
    <message>
        <source>Open with:</source>
        <translation>ཁ་འབྱེད་སྟངས།</translation>
    </message>
    <message>
        <source>Choose a new folder:</source>
        <translation>ཡིག་ཁུག་གསར་པ་ཞིག་འདེམ།</translation>
    </message>
    <message>
        <source>Time Access:</source>
        <translation>ཆུ་ཚོད་འདྲི་བ།</translation>
    </message>
    <message>
        <source>%1 Bytes</source>
        <translation>%1 ཡིག་ཚིགས།</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>བཟོ་བཅོས།</translation>
    </message>
    <message>
        <source>Include:</source>
        <translation>ཚུད་དེ།</translation>
    </message>
    <message>
        <source>Can&apos;t get remote file information</source>
        <translation>རྒྱང་སྦྲེལ་ཡིག་ཆའི་ཆ་འཕྲིན་ལེན་མ་ཐུབ།</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation>གསལ་བཤད།:</translation>
    </message>
    <message>
        <source>cannot move a folder to itself !</source>
        <translation>ཡིག་ཁུག་གཅིག་ནས་ནང་ཁུལ་དུ་སྤོ་འགུལ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Hidden</source>
        <translation>གབ་པ།</translation>
    </message>
    <message>
        <source>Location</source>
        <translation>ས་གནས།</translation>
    </message>
    <message>
        <source>Property:</source>
        <translation>གཏོགས་གཤིས།</translation>
    </message>
    <message>
        <source>Space Useage:</source>
        <translation>བར་སྟོངས་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>symbolLink</source>
        <translation>མྱུར་ལམ་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <source>Choose a custom icon</source>
        <translation>རང་སྒྲུབ་རྟགས་རིས་འདེམ་པ།</translation>
    </message>
    <message>
        <source>yyyy-MM-dd, HH:mm:ss</source>
        <translation type="vanished">yyyyལོMMཟླddཉིན, HH:mm:ss</translation>
    </message>
    <message>
        <source>Time Modified:</source>
        <translation>བཅོས་བསྒྱུར་དུས་ཚོད།</translation>
    </message>
    <message>
        <source>Readonly</source>
        <translation>ཀློག་པ་ཁོ་ན།</translation>
    </message>
    <message>
        <source>%1 files, %2 folders</source>
        <translation>%1ཡིག་ཆ། %2 ཡིག་ཁུག</translation>
    </message>
    <message>
        <source>%1 (%2 Bytes)</source>
        <translation>%1 (%2 ཡིག་ཚིགས)</translation>
    </message>
    <message>
        <source>yyyy-MM-dd, hh:mm:ss AP</source>
        <translation type="vanished">yyyyལོMMཟླddཉིན, hh:mm:ss AP</translation>
    </message>
    <message>
        <source>Select multiple files</source>
        <translation>ཡིག་ཆ་མང་པོ་འདེམ་པ།</translation>
    </message>
    <message>
        <source>Folder</source>
        <translation>ཡིག་སྣོད།</translation>
    </message>
    <message>
        <source>usershare</source>
        <translation>སྤྱོད་མཁན་གྱིས་མཉམ་སྤྱོད་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>Peony::ComputerPropertiesPage</name>
    <message>
        <source>Total Space: </source>
        <translation>ཤོང་ཚད་ཡོངས། </translation>
    </message>
    <message>
        <source>Kylin Burner</source>
        <translation>ཕབ་སྐྲུན།</translation>
    </message>
    <message>
        <source>You should mount this volume first</source>
        <translation>ཤོག་དྲིལ་འདི་བཀལ་ན་གཞི་ནས་ཆ་འཕྲིན་ལྟ་ཐུབ།</translation>
    </message>
    <message>
        <source>User Name: </source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་། </translation>
    </message>
    <message>
        <source>Name: </source>
        <translation>དབྱེ་ཁུལ་གྱི་མིང་། </translation>
    </message>
    <message>
        <source>Type: </source>
        <translation>རིགས་གྲས།: </translation>
    </message>
    <message>
        <source>File System</source>
        <translation>ཡིག་ཆའི་རྒྱུད་ཁོངས།</translation>
    </message>
    <message>
        <source>CPU Core:</source>
        <translation>སྲོག་སྙིང་གྲངས།</translation>
    </message>
    <message>
        <source>CPU Name:</source>
        <translation>སྒྲིག་་གཅོད་ཆས།</translation>
    </message>
    <message>
        <source>Open with: 	</source>
        <translation>ཁ་འབྱེད་སྟངས། 	</translation>
    </message>
    <message>
        <source>Desktop: </source>
        <translation>ཅོག་ངོས་ཀྱི་ཁོར་ཡུག </translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation>མི་ཤེས་པའི་དབྱེ་ཁུལ།</translation>
    </message>
    <message>
        <source>Memory Size:</source>
        <translation>ནང་ཁོག</translation>
    </message>
    <message>
        <source>Free Space: </source>
        <translation>བར་སྟོང་ལྷག་མ། </translation>
    </message>
    <message>
        <source>Used Space: </source>
        <translation>བཀོལ་སྤྱོད་བར་སྟོངས། </translation>
    </message>
    <message>
        <source>Data</source>
        <translation>གཞི་གྲངས་སྡེར།</translation>
    </message>
</context>
<context>
    <name>Peony::ConnectServerDialog</name>
    <message>
        <source>ip</source>
        <translation>ཞབས་ཞུ་ཆས།</translation>
    </message>
    <message>
        <source>add</source>
        <translation>ཁ་སྣོན།</translation>
    </message>
    <message>
        <source>port</source>
        <translation>མཐུད་ཁ།</translation>
    </message>
    <message>
        <source>type</source>
        <translation>རིགས་རྣམ།</translation>
    </message>
    <message>
        <source>delete</source>
        <translation>འདོར་བ།</translation>
    </message>
    <message>
        <source>connect</source>
        <translation>འབྲེལ་མཐུད།</translation>
    </message>
    <message>
        <source>connect to server</source>
        <translation>ཞབས་ཞུའི་འཕྲུལ་ཆས་ལ་སྦྲེལ་བ།</translation>
    </message>
    <message>
        <source>Personal Collection server:</source>
        <translation>སྒེར་གྱི་གསོག་ཉར་ཞབས་ཞུ་ཆས།</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation type="unfinished">ཉེན་བརྡ་བཏང་བ།</translation>
    </message>
    <message>
        <source>ip input error, please re-enter!</source>
        <translation>ipནང་འཇུག་ནོར་འཁྲུལ་བྱུང་བས་ཡང་བསྐྱར་ནང་འཇུག་གནང་རོགས།</translation>
    </message>
    <message>
        <source>port input error, please re-enter!</source>
        <translation>མཐུད་སྣེའི་ཨང་རྟགས་ནང་འཇུག་ནོར་འཁྲུལ་བྱུང་བས་ཡང་བསྐྱར་ནང་འཇུག་གནང་རོགས།</translation>
    </message>
</context>
<context>
    <name>Peony::ConnectServerLogin</name>
    <message>
        <source>ok</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>name</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <source>guest</source>
        <translation>ཡུལ་སྐོར་བ （མིང་མེད་ཐོ་འཇུག）</translation>
    </message>
    <message>
        <source>The login user</source>
        <translation>ཐོ་འཇུག་ཐོབ་ཐང་།</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation>འདོར་བ།</translation>
    </message>
    <message>
        <source>Registered users</source>
        <translation>སྤྱོད་མཁན་ཐོ་འགོད།</translation>
    </message>
    <message>
        <source>Please enter the %1&apos;s user name and password of the server.</source>
        <translation>ཞབས་ཞུ་ཆས་ %1ཡི་སྤྱོད་མཁན་མིང་དང་གསང་ཨང་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>User&apos;s identity</source>
        <translation>འབྲེལ་མཐུད་ཐོབ་ཐང་།</translation>
    </message>
    <message>
        <source>password</source>
        <translation>གསང་ཨང་།</translation>
    </message>
    <message>
        <source>Remember the password</source>
        <translation>གསང་ཨང་དེ་སེམས་ལ་ཟུངས།</translation>
    </message>
</context>
<context>
    <name>Peony::CreateLinkInternalPlugin</name>
    <message>
        <source>Choose a Directory to Create Link</source>
        <translation>སྦྲེལ་མཐུད་གསར་སྐྲུན་གྱི་དཀར་ཆག་འདེམ་པ།</translation>
    </message>
    <message>
        <source>Create Link to Desktop</source>
        <translation>སྒྲོག་ངོས་ཀྱི་མྱུར་ལམ་རི་མོར་བསྐུར་བ།</translation>
    </message>
    <message>
        <source>Create Link to...</source>
        <translation>མྱུར་ལམ་རི་མོ་བསྐུར་ནས</translation>
    </message>
    <message>
        <source>Create Link Menu Extension.</source>
        <translation type="vanished">སྦྲེལ་མཐུད་གསར་སྐྲུན །</translation>
    </message>
    <message>
        <source>Peony-Qt Create Link Extension</source>
        <translation type="vanished">སྦྲེལ་མཐུད་གསར་སྐྲུན །</translation>
    </message>
</context>
<context>
    <name>Peony::CreateSharedFileLinkMenuPlugin</name>
    <message>
        <source>Create Link to Desktop</source>
        <translation>ཅོག་ངོས་སུ་བསྐུར་ན་སྟབས་བདེ་རེད།</translation>
    </message>
</context>
<context>
    <name>Peony::CreateTemplateOperation</name>
    <message>
        <source>NewFile</source>
        <translation>ཡིག་ཆ་གསར་བཟོ།</translation>
    </message>
    <message>
        <source>Create file</source>
        <translation>ཡིག་ཆ་བཟོ་བ།</translation>
    </message>
    <message>
        <source>NewFolder</source>
        <translation>དཀར་ཆག་གསར་བ།</translation>
    </message>
    <message>
        <source>Create file error</source>
        <translation>ཡིག་ཆ་གསར་སྐྲུན་ནོར་བ།</translation>
    </message>
</context>
<context>
    <name>Peony::CustomErrorHandler</name>
    <message>
        <source>Is Error Handled?</source>
        <translation>ནོར་འཁྲུལ་ཐག་གཅོད་བྱས་ཡོད་དམ།</translation>
    </message>
    <message>
        <source>Error not be handled correctly</source>
        <translation>ནོར་འཁྲུལ་ཐག་གཅོད་ཡང་དག་མ་བྱས་པ།</translation>
    </message>
</context>
<context>
    <name>Peony::DefaultOpenWithWidget</name>
    <message>
        <source>No default app</source>
        <translation>ཐོག་མའི་ཁ་འབྱེད་ཐབས་བཀོད་སྒྲིག་བྱས་མེད།</translation>
    </message>
</context>
<context>
    <name>Peony::DefaultPreviewPage</name>
    <message>
        <source>Can not preview this file.</source>
        <translation>ཡིག་ཆ་འདི་སྔོན་ལྟ་བྱ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Select the file you want to preview...</source>
        <translation>ཁྱོད་ཀྱིས་སྔོན་ལྟ་བསམ་པའི་ཡིག་ཆ་འདེམ་པ།</translation>
    </message>
</context>
<context>
    <name>Peony::DefaultPreviewPageFactory</name>
    <message>
        <source>This is the Default Preview of peony-qt</source>
        <translation>ཡིག་ཆའི་ཞིབ་ཆའི་ཆ་འཕྲིན་མངོན་པ།</translation>
    </message>
    <message>
        <source>Default Preview</source>
        <translation>ཞིབ་ཕྲའི་ཆ་འཕྲིན།</translation>
    </message>
</context>
<context>
    <name>Peony::DetailsPropertiesPage</name>
    <message>
        <source>%1 px</source>
        <translation>%1བརྙན་རྒྱུ།</translation>
    </message>
    <message>
        <source>Name:</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <source>Owner</source>
        <translation>བདག་པོ།</translation>
    </message>
    <message>
        <source>Create time:</source>
        <translation>གསར་བཟོའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <source>Computer:</source>
        <translation>རྩིས་ཆས།</translation>
    </message>
    <message>
        <source>Can&apos;t get remote file information</source>
        <translation>རྒྱང་སྦྲེལ་ཡིག་ཆའི་ཆ་འཕྲིན་ལེན་མ་ཐུབ།</translation>
    </message>
    <message>
        <source>Owner:</source>
        <translation>བདག་པོ།</translation>
    </message>
    <message>
        <source>Width:</source>
        <translation>ཞེང་ཚད།</translation>
    </message>
    <message>
        <source>Computer</source>
        <translation>རྩིས་ཆས།</translation>
    </message>
    <message>
        <source>File size:</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>File type:</source>
        <translation>ཡིག་ཆའི་རིགས་གྲས།</translation>
    </message>
    <message>
        <source>Location:</source>
        <translation>གནས་ས།</translation>
    </message>
    <message>
        <source>%1 (this computer)</source>
        <translation>%1 (གློག་ཀླད་འདི)</translation>
    </message>
    <message>
        <source>yyyy-MM-dd, HH:mm:ss</source>
        <translation>yyyyལོའི་ཟླMMཚེས་ddཉིན། HH:mm:ss</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation>མི་ཤེས་པའི་དབྱེ་ཁུལ།</translation>
    </message>
    <message>
        <source>yyyy-MM-dd, hh:mm:ss AP</source>
        <translation>yyyyལོའི་ཟླMMཚེས་ddཉིན། hh:mm:ss AP</translation>
    </message>
    <message>
        <source>Modify time:</source>
        <translation>བཅོས་བསྒྱུར་དུས་ཚོད།</translation>
    </message>
    <message>
        <source>Height:</source>
        <translation>མཐོ་ཚད།</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::IconView</name>
    <message>
        <source>Icon View</source>
        <translation type="vanished">རིས་རྟགས་མཐོང་རིས།</translation>
    </message>
    <message>
        <source>warn</source>
        <translation>ཉེན་བརྡ་བཏང་བ།</translation>
    </message>
    <message>
        <source>This operation is not supported.</source>
        <translation>གཤགས་བཅོས་འདི་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryView::ListView</name>
    <message>
        <source>List View</source>
        <translation type="vanished">རེའུ་མིག་གི་མཐོང་རིས།</translation>
    </message>
    <message>
        <source>warn</source>
        <translation>ཉེན་བརྡ་བཏང་བ།</translation>
    </message>
    <message>
        <source>This operation is not supported.</source>
        <translation>གཤགས་བཅོས་འདི་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
</context>
<context>
    <name>Peony::DirectoryViewMenu</name>
    <message>
        <source>Cut</source>
        <translation>དྲས་གཏུབ།</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>འདྲ་ཕབ།</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>ཁ་འབྱེད།</translation>
    </message>
    <message>
        <source>More applications...</source>
        <translation>ཉེར་སྤྱོད་སྔར་ལས་མང་བ།</translation>
    </message>
    <message>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation>སྦྱར་བ།</translation>
    </message>
    <message>
        <source>Ascending Order</source>
        <translation>འཕར་རིམ།</translation>
    </message>
    <message>
        <source>Descending Order</source>
        <translation>འགྲིབ་རིམ།</translation>
    </message>
    <message>
        <source>Folder First</source>
        <translation>ཡིག་ཆ་སྔོན་ལ།</translation>
    </message>
    <message>
        <source>File:&quot;%1&quot; is not exist, did you moved or deleted it?</source>
        <translation>ཡིག་ཆའི་%1་མི་འདུག་ཁྱོད་ཀྱིས་གསུབ་པའམ་ཡང་ན་གནས་གཞན་ལ་སྤོར་བ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <source>Delete forever</source>
        <translation>གཏན་དུ་སུབ་པ།</translation>
    </message>
    <message>
        <source>Open Parent Folder in New Window</source>
        <translation>སྒེའུ་ཁུང་གསར་པའི་ཁྲོད་དཀར་ཆག་ཐམས་ཅད་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>File Size</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>File Type</source>
        <translation>ཡིག་ཆའི་རིགས་གྲས།</translation>
    </message>
    <message>
        <source>Sort Order...</source>
        <translation type="vanished">གོ་རིམ་སྒྲིག་པ།</translation>
    </message>
    <message>
        <source>Delete to trash</source>
        <translation>སྙིགས་སྣོད་དུ་དོར་བ།</translation>
    </message>
    <message>
        <source>Add to bookmark</source>
        <translation>ཉར་ཚགས་ཁུག་ཏུ་བླུགས་པ།</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <source>Show Hidden</source>
        <translation>སྦས་པའི་ཡིག་ཆ་མངོན་པ།</translation>
    </message>
    <message>
        <source>New Folder</source>
        <translation>ཡིག་ཁུག་གསར་འཛུགས།</translation>
    </message>
    <message>
        <source>Folder</source>
        <translation>ཡིག་ཁུག</translation>
    </message>
    <message>
        <source>New...</source>
        <translation type="vanished">གསར་འཛུགས།...</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation>མིང་བསྐྱར་འདོགས།</translation>
    </message>
    <message>
        <source>Delete Permanently</source>
        <translation>བརྟན་འདོར།</translation>
    </message>
    <message>
        <source>Sort Preferences...</source>
        <translation type="vanished">རིམ་སྒྲིག་ལེགས།</translation>
    </message>
    <message>
        <source>Sort By...</source>
        <translation type="vanished">རིམ་སྒྲིག་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation>ངོ་བོ།</translation>
    </message>
    <message>
        <source>format</source>
        <translation>རྣམ་གཞག</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation>ཕྱིར་ལོག</translation>
    </message>
    <message>
        <source>Open in New Tab</source>
        <translation>དོང་འཛར་གསར་པའི་ཁ་འབྱེད་པ།</translation>
    </message>
    <message>
        <source>Reverse Select</source>
        <translation>ཕྱིར་འདེམ།</translation>
    </message>
    <message>
        <source>Open with...</source>
        <translation>ཁ་འབྱེད་སྟངས་...</translation>
    </message>
    <message>
        <source>Open %1 selected files</source>
        <translation>གདམ་ཟིནཡིག་ཆ%1ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>Clean All</source>
        <translation>ཐམས་ཅད་གཙང་སེལ།</translation>
    </message>
    <message>
        <source>Select All</source>
        <translation>ཡོད་ཚད་འདེམ་པ།</translation>
    </message>
    <message>
        <source>Empty File</source>
        <translation>ཡིག་ཆ་སྟོང་བ།</translation>
    </message>
    <message>
        <source>&amp;Clean the Trash</source>
        <translation>སྙིགས་སྣོད་གཙང་སེལ། (&amp;C)</translation>
    </message>
    <message>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation>ཁྱོད་ཀྱིས་ཡིག་ཆ་འདི་དག་གསུབ་རྒྱུ་ཡིན་ནམ། གལ་ཏར་སུབ་ཚར་ན། ཡིག་ཆ་འདི་དག་ནམ་ཡང་སླར་གསོ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Chinese First</source>
        <translation>རྒྱ་ཡིག་སྔོན་དུ།</translation>
    </message>
    <message>
        <source>Open in New Window</source>
        <translation>སྒེའུ་ཁུང་གསར་པ་ནས་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>Modified Date</source>
        <translation>ཟླ་ཚེས་བཅོས་བསྒྱུར།</translation>
    </message>
    <message>
        <source>View Type...</source>
        <translation type="vanished">མཐོང་རིས་རིགས་རྣམ།</translation>
    </message>
    <message>
        <source>Peony-Qt filesafe menu Extension</source>
        <translation>ཡིག་ཆ་སྲུང་སྐྱོབ་སྒམ་རྒྱ་བསྐྱེད་པ།</translation>
    </message>
    <message>
        <source>Orignal Path</source>
        <translation>གདོད་མའི་འགྲོ་ལམ།</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="unfinished">གསར་འཛུགས།</translation>
    </message>
    <message>
        <source>View Type</source>
        <translation type="unfinished">མཐོང་རིས་རིགས་རྣམ།</translation>
    </message>
    <message>
        <source>Sort By</source>
        <translation type="unfinished">རིམ་སྒྲིག་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <source>Sort Order</source>
        <translation type="unfinished">གོ་རིམ་སྒྲིག་པ།</translation>
    </message>
    <message>
        <source>Sort Preferences</source>
        <translation type="unfinished">རིམ་སྒྲིག་ལེགས།</translation>
    </message>
    <message>
        <source>Peony File Labels Menu Extension</source>
        <translation type="unfinished">ཡིག་ཆའི་མཚོན་རྟགས།</translation>
    </message>
</context>
<context>
    <name>Peony::FMWindow</name>
    <message>
        <source>Redo</source>
        <translation>བསྐྱར་བཟོ།</translation>
    </message>
    <message>
        <source>Undo</source>
        <translation>འདོར་བ།</translation>
    </message>
    <message>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019-2020, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation>རྩོམ་པ་པོ།  
Yue Lan &lt;lanyue@kylinos.cn&gt;
 Meihong He &lt;hemeihong@kylinos.cn&gt;
པར་དབང་ཡོད་ཚད (C): 2019-2020ཐེན་ཅིན་ཆི་ལིན་ཆ་འཕྲིན་ལག་རྩལ་ཚད་ཡོད་ཀུང་སི།</translation>
    </message>
    <message>
        <source>clear record</source>
        <translation>ལོ་རྒྱུས་གཙང་སེལ།</translation>
    </message>
    <message>
        <source>New Folder</source>
        <translation>ཡིག་ཁུག་གསར་འཛུགས།</translation>
    </message>
    <message>
        <source>File Manager</source>
        <translation>ཡིག་ཆ་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <source>Loaing... Press Esc to stop a loading.</source>
        <translation>གསར་སྣོན་བྱེད་སྒང་ཡིན། Escམནན་ནས་རྩིས་མེད་གཏོང་།</translation>
    </message>
    <message>
        <source>advanced search</source>
        <translation>མཐོ་རིམ་འཚོལ་བཤེར།</translation>
    </message>
    <message>
        <source>Peony Qt</source>
        <translation>ཡིག་ཆ་དོ་དམ་ཆས།</translation>
    </message>
</context>
<context>
    <name>Peony::FileCopy</name>
    <message>
        <source>The dest file &quot;%1&quot; has existed!</source>
        <translation>དམིགས་བཟུང་ཡིག་ཆ་%1གནས་ཡོད།</translation>
    </message>
    <message>
        <source>Error in source or destination file path!</source>
        <translation>ཁུངས་གནས་ཡུལ་ལམ་ཡང་ན་དམིགས་བཟུང་ཡིག་ཆ་རྒྱུ་ལམ་ནོར་བ།</translation>
    </message>
    <message>
        <source>Error opening source or destination file!</source>
        <translation type="vanished">ཁུངས་ཡིག་ཆའམ་དམིགས་བཟུང་ཡིག་ཆ་ཁ་ཕྱེ་བ་ནོར་བ།</translation>
    </message>
    <message>
        <source>Reading and Writing files are inconsistent!</source>
        <translation type="vanished">ཡིག་ཆ་ཀློག་པ་དང་འབྲི་བ་མི་གཅིག</translation>
    </message>
    <message>
        <source>operation cancel</source>
        <translation>བཀོལ་སྤྱོད་རྩིས་མེད།</translation>
    </message>
    <message>
        <source>Vfat/FAT32 file systems do not support a single file that occupies more than 4 GB space!</source>
        <translation>vfat/fat32ཡིག་ཆའི་མ་ལག་གིས་ཡིག་ཆ་རྐྱང་བས་བཟུང་བའི་བར་སྟོང4gལས་ཆེ་བར་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <source>Please check whether the device has been removed!</source>
        <translation>སྒྲིག་ཆས་འདི་མེད་པར་བཟོས་ཡོད་མེད་ལ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>File opening failure</source>
        <translation>ཡིག་ཆ་ཁ་ཕྱེ་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <source>Write file error: There is no avaliable disk space for device!</source>
        <translation>ཡིག་ཆ་འབྲི་ནོར་ཤོར་བ་སྟེ། སྒྲིག་ཆས་ལ་གོ་མི་སྲིད་པའི་ཁབ་ལེན་གྱི་བར་སྟོང་མེད།</translation>
    </message>
</context>
<context>
    <name>Peony::FileCopyOperation</name>
    <message>
        <source>Cannot opening file, permission denied!</source>
        <translation>ཡིག་ཆ་ཁ་ཕྱེ་ཐབས་བྲལ། དབང་ཚད་མི་འདང་།</translation>
    </message>
    <message>
        <source>File copy error</source>
        <translation>ཡིག་ཆ་པར་སློག་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>Create folder %1 failed: %2</source>
        <translation>ཡིག་ཁུག་ %1 གསར་བཟོ་ཕམ་སོང་།: %2</translation>
    </message>
    <message>
        <source>File:%1 was not found.</source>
        <translation>ཡིག་ཆ།: %1 རྙེད་མ་སོང་།</translation>
    </message>
</context>
<context>
    <name>Peony::FileDeleteOperation</name>
    <message>
        <source>File delete error</source>
        <translation>ཡིག་ཆ་བསུབ་པ་ནོར་བ།</translation>
    </message>
    <message>
        <source>Delete file error</source>
        <translation>ཡིག་ཆའི་ནོར་འཁྲུལ་མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>Invalid Operation! Can not delete &quot;%1&quot;.</source>
        <translation>གོ་མི་ཆོད་པའི་བྱ་སྤྱོད་ཅིག་རེད། &quot;%1&quot;བསུབ་མི་རུང་།</translation>
    </message>
</context>
<context>
    <name>Peony::FileEnumerator</name>
    <message>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
</context>
<context>
    <name>Peony::FileInfo</name>
    <message>
        <source>data</source>
        <translation>གཞི་གྲངས་སྡེར།</translation>
    </message>
</context>
<context>
    <name>Peony::FileInfoJob</name>
    <message>
        <source>Trash</source>
        <translation>གད་སྙིགས་བླུགས་སྣོད།</translation>
    </message>
    <message>
        <source>Computer</source>
        <translation>རྩིས་འཁོར།</translation>
    </message>
    <message>
        <source>Network</source>
        <translation>དྲ་རྒྱ།</translation>
    </message>
    <message>
        <source>Recent</source>
        <translation>ཉེ་ཆར།</translation>
    </message>
</context>
<context>
    <name>Peony::FileItem</name>
    <message>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>Can not find path &quot;%1&quot;，are you moved or renamed it?</source>
        <translation>རྒྱུ་ལམ་མ་རྙེད། ：&quot;%1&quot;ཁྱེད་རང་སྤོ་འགུལ་དང་མིང་བསྐྱར་འདོགས་བྱ་འམ།</translation>
    </message>
    <message>
        <source>Open Link failed</source>
        <translation>སྦྲེལ་མཐུད་ཁ་ཕྱེ་ནས་ཕམ་སོང་།</translation>
    </message>
    <message>
        <source>File not exist, do you want to delete the link file?</source>
        <translation>དམིགས་བཟུང་ཡིག་ཁུག་བསྡད་མི་འདུག ཕན་མེད་མྱུར་ལམ་རི་མོ་འདི་གསུབ་རྒྱུ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <source>Can not open path &quot;%1&quot;，permission denied.</source>
        <translation>རྒྱུ་ལམ&quot;%1&quot;ཁ་ཕྱེ་བ་ཕམ་ཉེས་བྱུང་། དབང་ཚད་ཁས་ལེན་མ་བྱས།</translation>
    </message>
</context>
<context>
    <name>Peony::FileItemModel</name>
    <message>
        <source>Symbol Link, </source>
        <translation>མགྱོགས་མྱུར་བྱེད་སྟངས། </translation>
    </message>
    <message>
        <source>child(ren)</source>
        <translation>སྒེར་གྱི་བུ་གྲངས།</translation>
    </message>
    <message>
        <source>File Name</source>
        <translation>ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <source>File Size</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>File Type</source>
        <translation>ཡིག་ཆའི་རིགས་གྲས།</translation>
    </message>
    <message>
        <source>Delete Date</source>
        <translation>ཟླ་ཚེས་བསུབ་པ།</translation>
    </message>
    <message>
        <source>Modified Date</source>
        <translation>ཟླ་ཚེས་བཅོས་བསྒྱུར།</translation>
    </message>
    <message>
        <source>Original Path</source>
        <translation>ཐོག་མའི་འགྲོ་ལམ།</translation>
    </message>
</context>
<context>
    <name>Peony::FileLabelInternalMenuPlugin</name>
    <message>
        <source>Tag a File with Menu.</source>
        <translation type="vanished">འདེམ་བྱང་ཁྲོད་ནུས་པའི་མཚོན་རྟགས་སྣོན་པ།</translation>
    </message>
    <message>
        <source>Delete All Label</source>
        <translation>མཚོན་རྟགས་ཆ་ཚང་བསུབ་པ།</translation>
    </message>
    <message>
        <source>Add File Label...</source>
        <translation type="vanished">མཚོན་རྟགས་སྣོན་པ།</translation>
    </message>
    <message>
        <source>Peony File Labels Menu Extension</source>
        <translation type="vanished">ཡིག་ཆའི་མཚོན་རྟགས།</translation>
    </message>
    <message>
        <source>Add File Label</source>
        <translation type="unfinished">མཚོན་རྟགས་སྣོན་པ།</translation>
    </message>
</context>
<context>
    <name>Peony::FileLauchDialog</name>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>Set as Default</source>
        <translation>སོར་བཞག་རྩིས་ཐོ་ལ་བཀོད།</translation>
    </message>
    <message>
        <source>Applications</source>
        <translation>ཉེར་སྤྱོད་བྱ་རིམ།</translation>
    </message>
    <message>
        <source>Choose an Application to open this file</source>
        <translation>ཉེར་སྤྱོད་ཞིག་གདམ་པ་དང་ཡིག་ཆ་འདི་ཁ་ཕྱེ་བ།</translation>
    </message>
</context>
<context>
    <name>Peony::FileLaunchAction</name>
    <message>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>The linked app is changed or uninstalled, so it can not work correctly. 
Do you want to delete the link file?</source>
        <translation>སྦྲེལ་མཐུད་འདིས་བསྟན་པའི་ཉེར་སྤྱོད་བསྱུར་བཅོས་སམ་བཤིག་འདོན་བྱས་ཚར། དེའི་རྐྱེན་གྱིས་མྱུར་ལམ་རི་མོ་རྒྱུན་ལྡན་ལས་ཀ་བྱེད་ཐབས་བྲལ། 
མྱུར་ལམ་རི་མོ་འདི་སུབ་རྒྱུ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <source>By Default App</source>
        <translation>སོར་བཞག་ཁ་ཕྱེ་ཐབས་བེད་སྤྱོད་གཏོང་།</translation>
    </message>
    <message>
        <source>Can not open %1, file not exist, is it deleted?</source>
        <translation>%1ཁ་ཕྱེ་མ་ཐུབ། ཡིག་ཆ་མི་འདུག ཁྱེད་ཀྱིས་གསུབས་ཚར་མིན་གཏན་འཁེལ་བྱོས།</translation>
    </message>
    <message>
        <source>Open Failed</source>
        <translation>ཁ་ཕྱེ་བ་ཕམ་པ།</translation>
    </message>
    <message>
        <source>Detected launching an executable file %1, you want?</source>
        <translation>ལག་བསྟར་རུང་བའི་ཡིག་ཆ་ཞིག་ཁ་ཕྱེ་བཞིནའདུག%1  ཁྱེད་ཀྱི་རེ་བར།</translation>
    </message>
    <message>
        <source>Open Link failed</source>
        <translation>མྱུར་ལམ་རི་མོ་ཁ་ཕྱེ་བ་ཕམ་ཉེས་བྱུང་།</translation>
    </message>
    <message>
        <source>Execute in Terminal</source>
        <translation>མཐའ་སྣེ་ནས་འཁོར་སྐྱོད་བྱེད།</translation>
    </message>
    <message>
        <source>Can not get a default application for opening %1, do you want open it with text format?</source>
        <translation>སོར་བཞག་་ཁ་ཕྱེ་བའི་%1ཉེར་སྤྱོད་རྙེད་སོན་མ་བྱུང་། ཡིག་ཆ་རྩོམ་སྒྲིག་ཆས་བཀོལ་ནས་ཁ་ཕྱེ་རྒྱུ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <source>Can not open %1, Please confirm you have the right authority.</source>
        <translation>%1ཁ་ཕྱེ་མ་ཐུབ། ཁྱེད་ཀྱིས་ཡང་དག་པའི་དབང་ཚད་གཏན་འཁེལ་བྱོས།</translation>
    </message>
    <message>
        <source>Execute Directly</source>
        <translation>ཐད་ཀར་འཁོར་སྐྱོད།</translation>
    </message>
    <message>
        <source>Open App failed</source>
        <translation>མགྱོགས་མྱུར་བྱེད་ཐབས་ལ་གནད་དོན་ཡོད།</translation>
    </message>
    <message>
        <source>File not exist, do you want to delete the link file?</source>
        <translation>དམིགས་བཟུང་ཡིག་ཁུག་བསྡད་མི་འདུག ཕན་མེད་མྱུར་ལམ་རི་མོ་འདི་གསུབ་རྒྱུ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <source>Launch Options</source>
        <translation>གདམ་བྱ་ལག་བསྟར།</translation>
    </message>
</context>
<context>
    <name>Peony::FileLinkOperation</name>
    <message>
        <source>Link file error</source>
        <translation>ཡིག་ཆའི་སྦྲེལ་མཐུད་བཟོ་བ་ཕམ་ཉེས་བྱུང་།</translation>
    </message>
    <message>
        <source>Symbolic Link</source>
        <translation>མགྱོགས་མྱུར་བྱེད་ཐབས།</translation>
    </message>
</context>
<context>
    <name>Peony::FileMoveOperation</name>
    <message>
        <source>File delete error</source>
        <translation>ཡིག་ཆ་བསུབ་པ་ནོར་བ།</translation>
    </message>
    <message>
        <source>Invalid Operation.</source>
        <translation>ནུས་མེད་ཀྱི་བཀོལ་སྤྱོད།</translation>
    </message>
    <message>
        <source>Move file error</source>
        <translation>ཡིག་ཆ་སྤོ་སྒུལ་ནོར་བ།</translation>
    </message>
    <message>
        <source>Create file error</source>
        <translation>ཡིག་ཆ་གསར་སྐྲུན་ནོར་བ།</translation>
    </message>
    <message>
        <source>Invalid Operation</source>
        <translation>ནུས་མེད་ཀྱི་བཀོལ་སྤྱོད།</translation>
    </message>
    <message>
        <source>Cannot opening file, permission denied!</source>
        <translation type="unfinished">ཡིག་ཆ་ཁ་ཕྱེ་ཐབས་བྲལ། དབང་ཚད་མི་འདང་།</translation>
    </message>
    <message>
        <source>File:%1 was not found.</source>
        <translation>ཡིག་ཆ།: %1རྙེད་མ་སོང་།</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationAfterProgressPage</name>
    <message>
        <source>&amp;More Details</source>
        <translation>ཞིབ་ཕྲའི་ཆ་འཕྲིན། (&amp;M)</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialog</name>
    <message>
        <source>null</source>
        <translation>སྟོང་པ།</translation>
    </message>
    <message>
        <source>&amp;Retry</source>
        <translation>ཡང་བསྐྱར་ཚོད་ལེན།(&amp;R)</translation>
    </message>
    <message>
        <source>Backup All</source>
        <translation>ཆ་ཚང་གྲབས་ཉར།</translation>
    </message>
    <message>
        <source>Source File:</source>
        <translation>ཁུངས་ཡིག་ཆ།</translation>
    </message>
    <message>
        <source>Backup</source>
        <translation>གྲབས་ཉར།</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>སྣང་མེད་དུ་བཞག</translation>
    </message>
    <message>
        <source>Error message:</source>
        <translation>ནོར་འཁྲུལ་བརྡ་འཕྲིན།</translation>
    </message>
    <message>
        <source>Dest File:</source>
        <translation>དམིགས་བཟུང་ཡིག་ཆ།</translation>
    </message>
    <message>
        <source>Overwrite All</source>
        <translation>ཚང་ཁེབས།</translation>
    </message>
    <message>
        <source>Ignore All</source>
        <translation>ཚང་མ་སྣང་མེད་དུ་བཞག</translation>
    </message>
    <message>
        <source>unkwon</source>
        <translation>རྒྱུ་རྐྱེན་མི་ཤེས།</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>ཕྱིར་འདོན།(&amp;C)</translation>
    </message>
    <message>
        <source>Overwrite</source>
        <translation>འགེབས་པ།</translation>
    </message>
    <message>
        <source>File Operation Error</source>
        <translation>ཡིག་ཆ་བཀོལ་སྤྱོད་ནོར་འཁྲུལ།</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogConflict</name>
    <message>
        <source>This location already contains the file,</source>
        <translation type="vanished">འདི་ན་ཡིག་ཆ་འདི་ཚུད་ཡོད།</translation>
    </message>
    <message>
        <source>Backup</source>
        <translation>གྲབས་ཉར།</translation>
    </message>
    <message>
        <source>Ignore</source>
        <translation>སྣང་མེད་དུ་བཞག</translation>
    </message>
    <message>
        <source>Do you want to override it?</source>
        <translation type="vanished">ཁྱེད་ཀྱིས་འགེབས་སུབ་བྱ་ཐག་ཆོད་ཡིན་ནམ།</translation>
    </message>
    <message>
        <source>Replace</source>
        <translation>བརྗེ་བ།</translation>
    </message>
    <message>
        <source>&lt;p&gt;This location already contains the file &apos;%1&apos;, Do you want to override it?&lt;/p&gt;</source>
        <translation>&lt;p&gt;གནས་འདིར་མིང་ལ་“%1”ཡི་ཡིག་ཆ་ཚུད་ཟིན། ཁྱེད་ཀྱིས་བརྗེ་སྤོར་བྱ་ཐག་ཆོད་ཡིན་ནམ།&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Do the same</source>
        <translation>ཉེར་སྤྱོད་ཆ་ཚང་།</translation>
    </message>
    <message>
        <source>Unexpected error from %1 to %2</source>
        <translation>%1%ནས་%2བར་གྱི་བསམ་ཡུལ་ལས་འདས་པའི་ནོར་འཁྲུལ།</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogNotSupported</name>
    <message>
        <source>Yes</source>
        <translation>རེད།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>Do the same</source>
        <translation type="vanished">ཉེར་སྤྱོད་ཆ་ཚང་།</translation>
    </message>
    <message>
        <source>Make sure the disk is not full or write protected and that the file is not protected</source>
        <translation>སྡུད་སྡེར་ཁ་མ་གང་བའམ་སྲུང་སྐྱོབ་མ་བྱས་པར་ཡིག་ཆ་བེད་སྤྱོད་མ་བཏང་བ་ཨེ་ཡིན་ལྟོས།</translation>
    </message>
    <message>
        <source>No</source>
        <translation>མིན།</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationErrorDialogWarning</name>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>Make sure the disk is not full or write protected and that the file is not protected</source>
        <translation>སྡུད་སྡེར་ཁ་གང་མེད་པའམ་ཡང་ན་སྲུང་སྐྱོབ་བྲིས་མེད་པ་མ་ཟད་ཡིག་ཆ་བཀོལ་སྤྱོད་བྱས་མེད་པ་གཏན་འཁེལ་བྱེད་རོགས།</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationInfo</name>
    <message>
        <source>Symbolic Link</source>
        <translation>མྱུར་ཐབས།</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationManager</name>
    <message>
        <source>Do you want to put selected %1 item(s) into trash?</source>
        <translation>ཁྱེད་ཀྱིས་འདེམ་ཟིན་པའི %1སྙིགས་སྣོད་དུ་འདོར་རྒྱུ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <source>Can&apos;t delete.</source>
        <translation>བསུབ་མི་རུང་།</translation>
    </message>
    <message>
        <source>No, go to settings</source>
        <translation type="vanished">མིན། མཆོང་ནས་སྒྲིག་བཀོད་ལ་བསྒྱུར།</translation>
    </message>
    <message>
        <source>You can&apos;t delete a file whenthe file is doing another operation</source>
        <translation>གཞན་ནས་སྤྱོད་བཞིན་པའི་ཡིག་ཆ་ཞིག་བསུབ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>There have been one or more fileoperation(s) executing before. Youroperation will wait for executinguntil it/them done. If you really want to execute file operations parallelly anyway, you can change the default option &quot;Allow Parallel&quot; in option menu.</source>
        <translation>བཀོལ་སྤྱོད་འདི་ལག་བསྟར་མ་བྱས་སྔོན་ལ་བཀོལ་སྤྱོད་མ་ཚར་བ་ཡོད། བཀོལ་སྤྱོད་སྔོན་མ་ཚར་རྗེས་གཞི་ནས་ལག་བསྟར་བྱེད་ཐུབ། གལ་ཏར་ཁྱོད་ཀྱིས་ཡིག་ཆ་བཀོལ་སྤྱོད་མཉམ་པོར་བྱ་བསམ་ན།ཁྱོད་ཀྱིས་འདེམ་ཐོའི་ཁྲོད་&quot;བཀོལ་སྤྱོད་མཉམ་པོར་བྱ&quot;འདེམ་དགོས།</translation>
    </message>
    <message>
        <source>File Operation is Busy</source>
        <translation>བཀོལ་སྤྱོད་བྲེལ་འཚུབ་ཆེ།</translation>
    </message>
    <message>
        <source>Warn</source>
        <translation>ཉེན་བརྡ་བཏང་བ།</translation>
    </message>
    <message>
        <source>&apos;%1&apos; is occupied，you cannot operate!</source>
        <translation>&apos;%1&apos;བཟུང་ཡོད་པས་ཁྱེད་ཚོས་འཁོར་སྐྱོད་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="unfinished">གཏན་འཁེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>དོར་བ།</translation>
    </message>
    <message>
        <source>Do not show again</source>
        <translation>མངོན་འཆར་མི་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationPreparePage</name>
    <message>
        <source>state:</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <source>counting:</source>
        <translation>སྡོམ་རྩིས།</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationProgressPage</name>
    <message>
        <source>To:</source>
        <translation>སླེབས་བྱུང་།</translation>
    </message>
    <message>
        <source>From:</source>
        <translation>གཏོང་མཁན།</translation>
    </message>
    <message>
        <source>&amp;More Details</source>
        <translation>ཞིབ་ཕྲའི་ཆ་འཕྲིན། (&amp;M)</translation>
    </message>
</context>
<context>
    <name>Peony::FileOperationProgressWizard</name>
    <message>
        <source>copying...</source>
        <translation>བསྐྱར་དཔར་ཁྲོད་དུ།</translation>
    </message>
    <message>
        <source>%1 files, %2</source>
        <translation>%1ཡིག་ཆ། སྡོམ%2</translation>
    </message>
    <message>
        <source>A file operation is running backend...</source>
        <translation>ཡིག་ཆ་ཞིག་གི་བཀོལ་སྤྱོད་རྒྱབ་སྟེགས་ནས་འཁོར་སྐྱོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>Handling...</source>
        <translation>ཐག་གཅོད་བཞིན་པ།</translation>
    </message>
    <message>
        <source>%1 done, %2 total, %3 of %4.</source>
        <translation>ལེགས་གྲུབ%1 སྡོམ%2   %4ཁྲོད་ཀྱི་ཨང%3པ།</translation>
    </message>
    <message>
        <source>File Manager</source>
        <translation>ཡིག་ཆ་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <source>Rollbacking...</source>
        <translation>ཕྱིར་འགྲིལ་སྒང་།</translation>
    </message>
    <message>
        <source>Clearing...</source>
        <translation>གཙང་སེལ་བྱེད་སྒང་ཡིན།</translation>
    </message>
    <message>
        <source>Syncing...</source>
        <translation>མཉམ་སྤོ་བྱེད་སྒང་ཡིན།</translation>
    </message>
    <message>
        <source>&amp;Cancel</source>
        <translation>ལེན་པ།(&amp;C)</translation>
    </message>
    <message>
        <source>File Operation</source>
        <translation>ཡིག་ཆ་བཀོལ་སྤྱོད།</translation>
    </message>
    <message>
        <source>Preparing...</source>
        <translation>སྟ་གོན་བྱེད་བཞིན་པ།...</translation>
    </message>
    <message>
        <source>clearing: %1, %2 of %3</source>
        <translation>གཙང་སེལ་བྱེད་སྒང་ཡིན། ：%1，%3ཁྲོད་ཀྱི་%2པ།</translation>
    </message>
</context>
<context>
    <name>Peony::FilePreviewPage</name>
    <message>
        <source>%1x%2</source>
        <translation>%1x%2</translation>
    </message>
    <message>
        <source>Size:</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>color model:</source>
        <translation>ཚོན་མདོག་དཔེ་རྣམ།</translation>
    </message>
    <message>
        <source>Time Access:</source>
        <translation>ཆུ་ཚོད་འདྲི་བ།</translation>
    </message>
    <message>
        <source>Children Count:</source>
        <translation>ཁོངས་འདུ་ཡིག་ཆ།</translation>
    </message>
    <message>
        <source>Image resolution:</source>
        <translation>འབྱེད་ཕྱོད།</translation>
    </message>
    <message>
        <source>File Name:</source>
        <translation>ཡིག་ཆ་མིང་།</translation>
    </message>
    <message>
        <source>File Type:</source>
        <translation>ཡིག་ཆའི་རིགས་གྲས།</translation>
    </message>
    <message>
        <source>%1 total, %2 hidden</source>
        <translation>སྡོམ་རིགས%1དང་དེའི་ཁྲོད་%2ཡི་གབ་སྦས་ཡིག་ཆ།</translation>
    </message>
    <message>
        <source>Time Modified:</source>
        <translation>བཅོས་བསྒྱུར་དུས་ཚོད།</translation>
    </message>
    <message>
        <source>usershare</source>
        <translation>སྤྱོད་མཁན་གྱིས་མཉམ་སྤྱོད་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>Peony::FileRenameOperation</name>
    <message>
        <source>File Rename error</source>
        <translation>ཡིག་ཆའི་མིང་བསྐྱར་དུ་བཏགས་པ་ནོར་འདུག</translation>
    </message>
    <message>
        <source>Rename file error</source>
        <translation>ཡིག་ཆའི་མིང་བསྐྱར་འདོགས་ནོར་བ།</translation>
    </message>
    <message>
        <source>Invalid file name &quot;%1&quot; </source>
        <translation type="vanished">ནུས་མེད་ཀྱི་ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <source>The file &quot;%1&quot; will be hidden!</source>
        <translation type="vanished">ཡིག་ཆ &quot;%1&quot;ཡུན་ཙམ་ནས་གབ་རྒྱུ་རེད།</translation>
    </message>
    <message>
        <source>File Rename warning</source>
        <translation>ཡིག་ཆའི་མིང་བསྐྱར་འདོགས་ཉེན་བརྡ།</translation>
    </message>
    <message>
        <source>Invalid file name %1%2%3 .</source>
        <translation>ཁྲིམས་འགལ་གྱི་ཡིག་ཆའི་མིང་།%1%2%3 .</translation>
    </message>
    <message>
        <source>The file %1%2%3 will be hidden when you refresh or change directory!</source>
        <translation>ཁྱོད་ཀྱིས་དཀར་ཆག་གསར་བཟོ་བྱེད་པའམ་ཡང་ན་བརྗེ་བའི་སྐབས་སུ་ཡིག་ཆ་1%2%3སྦས་སྐུང་བྱེད་སྲིད།</translation>
    </message>
</context>
<context>
    <name>Peony::FileTrashOperation</name>
    <message>
        <source>trash:///</source>
        <translation>trash:///</translation>
    </message>
    <message>
        <source>Can not trash this file, would you like to delete it permanently?</source>
        <translation>ཡིག་ཆ་འདི་ཕྱིར་བསྡུ་མི་རུང་། ཡུན་རིང་ལ་གསུབ་དགོས་སམ།</translation>
    </message>
    <message>
        <source>The user does not have read and write rights to the file &apos;%1&apos; and cannot delete it to the Recycle Bin.</source>
        <translation type="vanished">སྤྱོད་མཁན་གྱིས་མིག་སྔའི་ཡིག་ཆ་%1 ཀློག་འབྲིའི་དབང་ཆ་མི་འདུག སྙིགས་སྣོད་འདུ་འདོར་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Trash file error</source>
        <translation>ཡིག་ཆ་སྙིགས་སྣོད་དུ་གཡུགས་པ་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>Invalid Operation! Can not trash &quot;%1&quot;.</source>
        <translation>གོ་མི་ཆོད་པའི་བྱ་སྤྱོད་ཅིག་རེད། &quot;%1&quot;གད་སྙིགས་བླུགས་མི་རུང་།</translation>
    </message>
    <message>
        <source>Can not trash</source>
        <translation>གད་སྙིགས་བླུགས་མི་ཆོག</translation>
    </message>
    <message>
        <source>Can not trash files more than 10GB, would you like to delete it permanently?</source>
        <translation>གད་སྙིགས་བླུགས་པའི་ཡིག་ཆ་10GBལས་བརྒལ་མི་རུང་། ཁྱེད་ཀྱིས་དུས་གཏན་དུ་བསུབ་ན་ཆོག་གམ།</translation>
    </message>
</context>
<context>
    <name>Peony::FileUntrashOperation</name>
    <message>
        <source>Untrash file error</source>
        <translation>སྙིགས་སྣོད་ནས་ཡིག་ཆ་སླར་གསོ་ཕམ་ཉེས་བྱུང་།</translation>
    </message>
</context>
<context>
    <name>Peony::GlobalSettings</name>
    <message>
        <source>HH:mm:ss</source>
        <translation type="unfinished">HH:mm:ss</translation>
    </message>
    <message>
        <source>yyyy-MM-dd</source>
        <translation></translation>
    </message>
    <message>
        <source>yyyy/MM/dd</source>
        <translation></translation>
    </message>
    <message>
        <source>AP hh:mm:ss</source>
        <translation>AP hh:mm:ss</translation>
    </message>
</context>
<context>
    <name>Peony::LocationBar</name>
    <message>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation>%2ཁྲོད་%1འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <source>File System</source>
        <translation type="vanished">ཡིག་ཆའི་རྒྱུད་ཁོངས།</translation>
    </message>
    <message>
        <source>Open In New Tab</source>
        <translation>ཤོག་བྱང་གསར་བའི་ནང་དུ་ཁ་ཕྱེ།</translation>
    </message>
    <message>
        <source>Open In New Window</source>
        <translation>སྒེའུ་ཁུང་གསར་བའི་ཁྲོད་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>Copy Directory</source>
        <translation>པར་སློག་རྒྱུ་ལམ།</translation>
    </message>
    <message>
        <source>Computer</source>
        <translation type="obsolete">རྩིས་ཆས།</translation>
    </message>
</context>
<context>
    <name>Peony::MountOperation</name>
    <message>
        <source>Operation Cancelled</source>
        <translation>བཀོལ་སྤྱོད་མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>Login failed, unknown username or password error, please re-enter!</source>
        <translation>ཐོ་འགོད་ཕམ་ཉེས་བྱུང་བ་དང་། མི་ཤེས་པའི་སྤྱོད་མཁན་གྱི་མིང་ངམ་གསང་ཨང་ནོར་འཁྲུལ་བྱུང་བས་བསྐྱར་དུ་ནང་འཇུག་གནང་རོགས།</translation>
    </message>
</context>
<context>
    <name>Peony::NavigationToolBar</name>
    <message>
        <source>Cd Up</source>
        <translation>སྟེང་ཕྱོགས།</translation>
    </message>
    <message>
        <source>Clear History</source>
        <translation>ལོ་རྒྱུས་ཟིན་ཐོ་གཙང་སེལ།</translation>
    </message>
    <message>
        <source>Refresh</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
    <message>
        <source>Go Forward</source>
        <translation>མདུན་སྐྱོད།</translation>
    </message>
    <message>
        <source>Go Back</source>
        <translation>ཕྱིར་ལོག</translation>
    </message>
    <message>
        <source>History</source>
        <translation>ལོ་རྒྱུས།</translation>
    </message>
</context>
<context>
    <name>Peony::NewFileLaunchDialog</name>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Choose new application</source>
        <translation>ཉེར་སྤྱོད་གསར་བ་ཞིག་འདེམ་པ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>apply now</source>
        <translation>མྱུར་དུ་ཉེར་སྤྱོད།</translation>
    </message>
    <message>
        <source>Choose an Application to open this file</source>
        <translation>ཉེར་སྤྱོད་ཞིག་གདམ་པ་དང་ཡིག་ཆ་འདི་ཁ་ཕྱེ་བ།</translation>
    </message>
</context>
<context>
    <name>Peony::OpenWithPropertiesPage</name>
    <message>
        <source>Other:</source>
        <translation>གཞན།</translation>
    </message>
    <message>
        <source>How do you want to open %1%2 files ?</source>
        <translation>ཁྱོད་ཀྱི་རེ་བར་ཡིག་ཆ%1%2ཁ་འབྱེད་ཐབས་གང་ཡིན་ནམ།</translation>
    </message>
    <message>
        <source>Default open with:</source>
        <translation>ཁ་འབྱེད་རྣམ་པ་སོར་བཞག</translation>
    </message>
    <message>
        <source>Go to application center</source>
        <translation>མཉེན་ཆས་ལྟེ་བར་འགྲོ་བ།</translation>
    </message>
    <message>
        <source>Choose other application</source>
        <translation>ཉེར་སྤྱོད་གཞན་འདེམ་པ།</translation>
    </message>
</context>
<context>
    <name>Peony::PathEdit</name>
    <message>
        <source>Go To</source>
        <translation>མཆོང་བ།</translation>
    </message>
</context>
<context>
    <name>Peony::PermissionsPropertiesPage</name>
    <message>
        <source>Me</source>
        <translation>ང་།</translation>
    </message>
    <message>
        <source>(Me)</source>
        <translation>(ང)</translation>
    </message>
    <message>
        <source>Read</source>
        <translation>ཀློག་ཐུབ་པ།</translation>
    </message>
    <message>
        <source>Type</source>
        <translation>རིགས་གྲས།</translation>
    </message>
    <message>
        <source>User</source>
        <translation>སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <source>Group</source>
        <translation>སྤྱོད་མཁན་ཚོགས་པ།</translation>
    </message>
    <message>
        <source>Other</source>
        <translation>གཞན་དག</translation>
    </message>
    <message>
        <source>Owner</source>
        <translation>བདག་པོ།</translation>
    </message>
    <message>
        <source>Write</source>
        <translation>འབྲི་བ།</translation>
    </message>
    <message>
        <source>Can not get the permission info.</source>
        <translation>ཡིག་ཆའི་སབྲེལ་ཡོད་དབང་ཚད་ལོན་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Others</source>
        <translation>གཞན་དག</translation>
    </message>
    <message>
        <source>You can not change the access of this file.</source>
        <translation>ཁྱོས་ཡིག་ཆའི་དབང་ཚད་བཅོས་བསྒྱུར་བྱེད་ཐབས་བྲལ།</translation>
    </message>
    <message>
        <source>User or Group</source>
        <translation>སྤྱོད་མཁན་ནམ་ཚོ།</translation>
    </message>
    <message>
        <source>Executable</source>
        <translation>ལག་བསྟར་ཐུབ་པའི་ཡིག་ཆ།</translation>
    </message>
    <message>
        <source>Target: %1</source>
        <translation>བྱ་ཡུལ་གྱི་མིང་།%1</translation>
    </message>
</context>
<context>
    <name>Peony::PropertiesWindow</name>
    <message>
        <source>Ok</source>
        <translation>གཏན་འཁེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Trash</source>
        <translation>སྙིགས་སྒམ།</translation>
    </message>
    <message>
        <source> %1 Files</source>
        <translation> %1ཡིག་ཆ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>དོར་བ།</translation>
    </message>
    <message>
        <source>Recent</source>
        <translation>ཉེ་ཆར།</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation>ངོ་བོ།</translation>
    </message>
    <message>
        <source>Selected</source>
        <translation>འདེམ་པ།</translation>
    </message>
    <message>
        <source>usershare</source>
        <translation>སྤྱོད་མཁན་གྱིས་མཉམ་སྤྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Data</source>
        <translation>གཞི་གྲངས་སྡེར།</translation>
    </message>
</context>
<context>
    <name>Peony::RecentAndTrashPropertiesPage</name>
    <message>
        <source>Origin Path: </source>
        <translation>སྔར་གྱི་རྒྱུ་ལམ། </translation>
    </message>
    <message>
        <source>Size: </source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་། </translation>
    </message>
    <message>
        <source>Deletion Date: </source>
        <translation>སུབ་པའི་ཟླ་ཚེས། </translation>
    </message>
    <message>
        <source>Original Location: </source>
        <translation>ཐོག་མའི་གནས་ས། </translation>
    </message>
    <message>
        <source>Show confirm dialog while trashing.</source>
        <translation>སྙིགས་སྣོད་དུ་འདོར་སྐབས་གཏན་འཁེལ་བྱང་ཐོན་པ།</translation>
    </message>
</context>
<context>
    <name>Peony::SearchBar</name>
    <message>
        <source>Input search key...</source>
        <translation>གནད་ཚིག་ནང་འཇུག</translation>
    </message>
    <message>
        <source>clear record</source>
        <translation>ལོ་རྒྱུས་གཙང་སེལ།</translation>
    </message>
    <message>
        <source>Input the search key of files you would like to find.</source>
        <translation>གནད་ཚིག་ནང་འཇུག་གིས་རང་ལ་དགོས་པའི་ཡིག་ཆ་འཚོལ་བ།</translation>
    </message>
    <message>
        <source>advance search</source>
        <translation>མཐོ་རིམ་འཚོལ་བཤེར།</translation>
    </message>
</context>
<context>
    <name>Peony::SearchBarContainer</name>
    <message>
        <source>all</source>
        <translation>ཚང་མ།</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>གཙང་སེལ།</translation>
    </message>
    <message>
        <source>audio</source>
        <translation>སྒྲ་ཟློས།</translation>
    </message>
    <message>
        <source>image</source>
        <translation>པར་རིས།</translation>
    </message>
    <message>
        <source>video</source>
        <translation>བརྙན་ལམ།</translation>
    </message>
    <message>
        <source>file folder</source>
        <translation>ཡིག་ཁུག</translation>
    </message>
    <message>
        <source>wps file</source>
        <translation>WPSཡིག་ཆ།</translation>
    </message>
    <message>
        <source>others</source>
        <translation>གཞན་དག</translation>
    </message>
    <message>
        <source>text file</source>
        <translation>ཡིག་དེབ།</translation>
    </message>
</context>
<context>
    <name>Peony::SharedFileLinkOperation</name>
    <message>
        <source>Symbolic Link</source>
        <translation>མྱུར་ཐབས།</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarFavoriteItem</name>
    <message>
        <source>Favorite</source>
        <translation>མགྱོགས་མྱུར་འཚམས་འདྲི།</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarFileSystemItem</name>
    <message>
        <source>File System</source>
        <translation type="vanished">ཡིག་ཆའི་རྒྱུད་ཁོངས།</translation>
    </message>
    <message>
        <source>Computer</source>
        <translation type="vanished">རྩིས་ཆས།</translation>
    </message>
    <message>
        <source>Data</source>
        <translation>གཞི་གྲངས་སྡེར།</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarMenu</name>
    <message>
        <source>Eject</source>
        <translation>སྒོར་བུད།</translation>
    </message>
    <message>
        <source>Properties</source>
        <translation>ངོ་བོ།</translation>
    </message>
    <message>
        <source>format</source>
        <translation>རྣམ་གཞག</translation>
    </message>
    <message>
        <source>Unmount</source>
        <translation>ལེན་པ།</translation>
    </message>
    <message>
        <source>Delete Symbolic</source>
        <translation>སུབ་པ།</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarModel</name>
    <message>
        <source>Network</source>
        <translation>དྲ་བ།</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarPersonalItem</name>
    <message>
        <source>Personal</source>
        <translation>མི་སྒེར།</translation>
    </message>
</context>
<context>
    <name>Peony::SideBarSeparatorItem</name>
    <message>
        <source>(No Sub Directory)</source>
        <translation>སྟོང་བ།</translation>
    </message>
</context>
<context>
    <name>Peony::StatusBar</name>
    <message>
        <source>; %1 folders</source>
        <translation>%1ཡིག་ཁུག</translation>
    </message>
    <message>
        <source>; %1 file, %2</source>
        <translation>%1ཡིག་ཁུག  %2</translation>
    </message>
    <message>
        <source>; %1 files, %2 total</source>
        <translation>%1 ཡིག་ཁུག  སྡོམ %2</translation>
    </message>
    <message>
        <source>; %1 folder</source>
        <translation>%1 ཡིག་ཁུག</translation>
    </message>
    <message>
        <source>%1 selected</source>
        <translation>%1གདམ་ཐོན་བྱུང་།</translation>
    </message>
</context>
<context>
    <name>Peony::SyncThread</name>
    <message>
        <source>notify</source>
        <translation>དྲན་སྐུལ་གསལ་བརྡ།</translation>
    </message>
</context>
<context>
    <name>Peony::ToolBar</name>
    <message>
        <source>Cut</source>
        <translation>དྲས་གཏུབ།</translation>
    </message>
    <message>
        <source>Copy</source>
        <translation>འདྲ་ཕབ།</translation>
    </message>
    <message>
        <source>Clean Trash</source>
        <translation>སྙིགས་སྣོད་གཙང་སེལ།</translation>
    </message>
    <message>
        <source>&amp;Help</source>
        <translation>རོགས་རམ། (&amp;H)</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation>སྦྱར་བ།</translation>
    </message>
    <message>
        <source>Trash</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <source>File Name</source>
        <translation>ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <source>File Size</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>File Type</source>
        <translation>ཡིག་ཆའི་རིགས་གྲས།</translation>
    </message>
    <message>
        <source>Let the program still run after closing the last window. This will reduce the time for the next launch, but it will also consume resources in backend.</source>
        <translation>སྒེའུ་ཁུང་ཐམས་ཅད་སྒོ་བརྒྱབ་རྗེས་ཡིག་ཆ་དོ་དམ་ཆས་མུ་མཐུད་འཁོར་སྐྱོད་བྱེད་དུ་བཙུག་ན། ཐེངས་རྗེས་མར་སྒོ་འབྱེད་དུས་ཚོད་ཐུང་དུ་གཏོང་ཐུབ། ཡིན་ཡང་ཐོན་ཁུངས་དུས་ཀུན་ཏུ་སྤྱོད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>Show Hidden</source>
        <translation>སྦས་པའི་ཡིག་ཆ་མངོན་པ།</translation>
    </message>
    <message>
        <source>Forbid Thumbnail</source>
        <translation>བསྡུས་རིས་བཀོལ་མི་ཆོག</translation>
    </message>
    <message>
        <source>Delete Permanently</source>
        <translation type="vanished">བརྟན་འདོར།</translation>
    </message>
    <message>
        <source>Resident in Backend</source>
        <translation>རྒྱུན་སྡོད་རྒྱབ་སྟེགས།</translation>
    </message>
    <message>
        <source>Options</source>
        <translation>འདེམས་ཚན།</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation>ཕྱིར་ལོག</translation>
    </message>
    <message>
        <source>Open in new Tab</source>
        <translation>རྟགས་གསར་བའི་ངོས་ཁྲོད་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>Sort Type</source>
        <translation>རིམ་སྒྲིག་རིགས་གྲས།</translation>
    </message>
    <message>
        <source>&amp;About...</source>
        <translation>སྐོར། (&amp;A)</translation>
    </message>
    <message>
        <source>Descending</source>
        <translation>རིམ་འབེབས།</translation>
    </message>
    <message>
        <source>Ascending</source>
        <translation>འཕེལ་རིམ།</translation>
    </message>
    <message>
        <source>Peony Qt</source>
        <translation>ཡིག་ཆ་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <source>Open in New window</source>
        <translation>སྒེའུ་ཁུང་གསར་བའི་ཁྲོད་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="vanished">ཁྱོད་ཀྱིས་ཡིག་ཆ་འདི་དག་གསུབ་རྒྱུ་ཡིན་ནམ། གལ་ཏར་སུབ་ཚར་ན། ཡིག་ཆ་འདི་དག་ནམ་ཡང་སླར་གསོ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2019, Tianjin KYLIN Information Technology Co., Ltd.</source>
        <translation>རྩོམ་པ་པོ། 
 Yue Lan &lt;lanyue@kylinos.cn&gt;
 Meihong He &lt;hemeihong@kylinos.cn&gt;
པར་དབང་ཡོད་ཚད (C): 2019-2020ཐེན་ཅིན་ཆི་ལིན་ཆ་འཕྲིན་ལག་རྩལ་ཚད་ཡོད་ཀུང་སི།</translation>
    </message>
    <message>
        <source>Modified Date</source>
        <translation>ཟླ་ཚེས་བཅོས་བསྒྱུར།</translation>
    </message>
</context>
<context>
    <name>Peony::UserShareInfoManager</name>
    <message>
        <source>Warning</source>
        <translation>ཉེན་བརྡ་བཏང་བ།</translation>
    </message>
</context>
<context>
    <name>Peony::VolumeManager</name>
    <message>
        <source>Error</source>
        <translation>ནོར་འཁྲུལ།</translation>
    </message>
</context>
<context>
    <name>ProgressBar</name>
    <message>
        <source>canceling ...</source>
        <translation>འདོར་བ།...</translation>
    </message>
    <message>
        <source>starting ...</source>
        <translation>ད་ལྟ་འགོ་ཚུགས་བཞིན་འདུག་་་</translation>
    </message>
    <message>
        <source>sync ...</source>
        <translation>དུས་མཉམ་ ...</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>YES</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>Mark</source>
        <translation>མཚོན་རྟགས།</translation>
    </message>
    <message>
        <source>Basic</source>
        <translation>གཞི་རྩ།</translation>
    </message>
    <message>
        <source>begin format</source>
        <translation>འགོ་རྩོམ་པ།</translation>
    </message>
    <message>
        <source>close</source>
        <translation>ཁ་རྒྱག</translation>
    </message>
    <message>
        <source>Show the computer properties or items in computer.</source>
        <translation>རྩིས་འཁོར་རང་བཞིན་མངོན་འཆར་རམ་རྩིས་འཁོར་ཁྲོད་ཀྱི་རྣམ་གྲངས།</translation>
    </message>
    <message>
        <source>open with.</source>
        <translation>ཁ་འབྱེད་སྟངས།</translation>
    </message>
    <message>
        <source>The virtual file system cannot be opened</source>
        <translation>རྟོག་བཟོ་ཡིག་ཆའི་རྒྱུད་ཁོངས་ཁ་ཕྱེ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Computer Properties</source>
        <translation>རྩིས་འཁོར་རང་བཞིན།</translation>
    </message>
    <message>
        <source>Permissions</source>
        <translation>དབང་ཚད།</translation>
    </message>
    <message>
        <source>Unable to eject %1</source>
        <translation type="vanished"> %1འཕར་མཆོང་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Eject failed</source>
        <translation>འཕར་མཆོང་ཕམ་ཉེས་བྱུང་།</translation>
    </message>
    <message>
        <source>Show and modify file&apos;s permission, owner and group.</source>
        <translation>ཡིག་ཆའི་དབང་ཚད་ལྟ་ཞིབ་དང་བཅོས་བསྒྱུར།</translation>
    </message>
    <message>
        <source>Show the basic file properties, and allow you to modify the access and name.</source>
        <translation>ཡིག་ཆའི་ཁྲོད་ཀྱི་གཞི་རྩའི་རང་བཞིན་མངོན་པ་དང་། ཡིག་ཆའི་མིང་བརྗེ་ཆོག་པ།</translation>
    </message>
    <message>
        <source>Format operation has been finished successfully.</source>
        <translation>རྣམ་བཞག་ཅན་གྱི་བཀོལ་སྤྱོད་ལེགས་གྲུབ་བྱུང་།</translation>
    </message>
    <message>
        <source>Unmount failed</source>
        <translation>བཤིག་འདོན་ཕམ་ཉེས་བྱུང་།</translation>
    </message>
    <message>
        <source>qmesg_notify</source>
        <translation>བརྡ་ཐོ།</translation>
    </message>
    <message>
        <source>Data synchronization is complete and the device can be safely unplugged!</source>
        <translation>གཞི་གྲངས་མཉམ་སྤོ་ལེགས་གྲུབ། སྒྲིག་ཆས་བདེ་འཇགས་ངང་ལེན་ཆོག</translation>
    </message>
    <message>
        <source>Formatting this volume will erase all data on it. Please backup all retained data before formatting. Do you want to continue ?</source>
        <translation>ཤོག་དྲིལ་འདི་རྣམ་བཞག་ཅན་བྱས་ན་སྟེང་གི་གཞི་གྲངས་ཐམས་ཅད་མེད་པར་འགྱུར། ཁྱོད་ཀྱིས་རྣམ་བཞག་ཅན་དུ་བསྒྱུར་བའི་སྔོན་ལ་གཞི་གྲངས་གྲབས་གསོག་བྱོས། མུ་མཐུད་ཡིན་ནམ།</translation>
    </message>
    <message>
        <source>Symbolic Link</source>
        <translation>མྱུར་ཐབས།</translation>
    </message>
    <message>
        <source>Force unmount failed</source>
        <translation>བཙན་གྱིས་བཤིག་འདོན་ཕམ་ཉེས་བྱུང་།</translation>
    </message>
    <message>
        <source>Unable to unmount it, you may need to close some programs, such as: GParted etc.</source>
        <translation>བཤིག་འདོན་ཕམ་ཉེས་བྱུང་། ཁྱེད་ཀྱིས་སྔོན་ལ་བྱ་རིམ་འགའ་སྒོ་རྒྱག་དགོས་འདུག དཔེར་ན་དབྱེ་ཁུལ་རྩོམ་སྒྲིག་ཆས་ལྟ་བུ།</translation>
    </message>
    <message>
        <source>Delete Permanently</source>
        <translation>བརྟན་འདོར།</translation>
    </message>
    <message>
        <source>Icon View</source>
        <translation>རིས་རྟགས་མཐོང་རིས།</translation>
    </message>
    <message>
        <source>file not found</source>
        <translation>ཡིག་ཆ་མ་རྙེད།</translation>
    </message>
    <message>
        <source>Error: %1
</source>
        <translation>ནོར་འཁྲུལ།%1
</translation>
    </message>
    <message>
        <source>permission denied</source>
        <translation>དབང་ཚད་འགོག་པ།</translation>
    </message>
    <message>
        <source>Can not trash these files. You can delete them permanently. Are you sure doing that?</source>
        <translation>ཡིག་ཆ་འདི་དག་སྙིགས་སྣོད་དུ་འཇོག་མི་ཐུབ་པས། ཡིག་འདི་དག་གཏན་དུ་སུབ་རྒྱུ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <source>format</source>
        <translation>རྣམ་གཞག</translation>
    </message>
    <message>
        <source>duplicate</source>
        <translation>བུ་དཔེ།</translation>
    </message>
    <message>
        <source>File Manager</source>
        <translation>ཡིག་ཆ་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <source>Virtual file directories do not support move and copy operations</source>
        <translation>རྟོག་བཟོ་ཡིག་ཆ་རྒྱུ་ལམ་སྤོ་འགུལ་དང་པར་སློག་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Share Data</source>
        <translation>རང་གནས་མཉམ་སྤྱོད།</translation>
    </message>
    <message>
        <source>favorite</source>
        <translation>མགྱོགས་མྱུར་འཚམས་འདྲི།</translation>
    </message>
    <message>
        <source>Show the folder children as rows in a list.</source>
        <translation>རེའུ་མིག་རྣམ་པས་དཀརར་ཆག་སྟོན་པ།</translation>
    </message>
    <message>
        <source>List View</source>
        <translation>རེའུ་མིག་གི་མཐོང་རིས།</translation>
    </message>
    <message>
        <source>Default search vfs of peony</source>
        <translation type="vanished">སོར་བཞག་ཡིག་ཆ་འཚོལ་བཤེར།</translation>
    </message>
    <message>
        <source>Details</source>
        <translation>ཆ་འཕྲིན་ཞིབ་ཕྲ།</translation>
    </message>
    <message>
        <source>Operation not supported</source>
        <translation>བཀོལ་སྤྱོད་འདིར་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <source>Format failed</source>
        <translation>རྣམ་གཞག་ཅན་དུ་སྒྱུར་བ་ཕམ་པ།</translation>
    </message>
    <message>
        <source>Can not create a symbolic file for vfs location</source>
        <translation>རྟོག་བཟོ་དཀར་ཆག་ལ་མྱུར་ལམ་རི་མོ་བཟོ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Sorry, the format operation is failed!</source>
        <translation>རྣམ་བཞག་བཟོ་བ་ཕམ་ཉེས་བྱུང་། ཡང་བསྐྱར་ཚོད་ལྟ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Open With</source>
        <translation>ཁ་འབྱེད་སྟངས།</translation>
    </message>
    <message>
        <source>Trash and Recent</source>
        <translation>སྙིགས་སྣོད། /ཉེ་ཆར།</translation>
    </message>
    <message>
        <source>Show the file properties or items in trash or recent.</source>
        <translation>སྙིགས་སྣོད་དང་ཉེ་ཆར་ཁྲོད་ཀྱི་ཡིག་ཆའི་རང་བཞིན་དང་རྣམ་གྲངས་མངོན་པ།</translation>
    </message>
    <message>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation>ཁྱེད་ཀྱིས་ཡིག་ཆ་འདི་དག་བསུབ་རྒྱུ་ཡིན་ནམ་།བསུབ་མགོ་བརྩམས་མ་ཐག་ཡིག་ཆ་འདི་དག་སླར་གསོ་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>mark this file.</source>
        <translation>ཡིག་ཆ་འདིར་རྟགས་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>PeonyNotify</source>
        <translation type="vanished">ཡིག་ཆ་དོ་དམ་ཆས་ཀྱི་བརྡ་ཐོ།</translation>
    </message>
    <message>
        <source>The virtual file system does not support folder creation</source>
        <translation>རྟོག་བཟོ་ཡིག་ཆའི་རྒྱུད་ཁོངས་ནང་ཡིག་ཁུག་གསར་བཟོ་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>It need to synchronize before operating the device,place wait!</source>
        <translation>སྒྲིག་ཆས་བཀོལ་སྤྱོད་སྔོན་ལ་གཞི་གྲངས་མཉམ་སྤོ་བྱ་དགོས། ཁྱུག་ཙམ་སྒུག་དང་།</translation>
    </message>
    <message>
        <source>Default favorite vfs of peony</source>
        <translation>ཡིག་ཆ་དོ་དམ་ཆས་ཀྱི་རྟོག་བཟོ་ཡིག་ཆའི་རྒྱུད་ཁོངས་ཉར་གསོག་ཁུག་ཏུ་བཞག་ཡོད།</translation>
    </message>
    <message>
        <source>Can not create symbolic file here, %1</source>
        <translation>འདི་ནས་མྱུར་ལམ་རི་མོ་བཟོ་མི་ཐུབ།  %1</translation>
    </message>
    <message>
        <source>Show the folder children as icons.</source>
        <translation>རིས་དབྱིབས་རྣམ་པས་དཀར་ཆག་འགོད་པ།</translation>
    </message>
    <message>
        <source>Data synchronization is complete,the device has been unmount successfully!</source>
        <translation>གཞི་གྲངས་མཉམ་སྤོ་ལེགས་གྲུབ། སྒྲིག་ཆས་བདེ་ལེགས་ངང་བཤིག་འདོན་ཐུབ་སོང་།</translation>
    </message>
    <message>
        <source>Can not trash</source>
        <translation>སླར་བསྡུ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>Can not trash files more than 10GB, would you like to delete it permanently?</source>
        <translation>གད་སྙིགས་བླུགས་པའི་ཡིག་ཆ་10GBལས་བརྒལ་མི་རུང་། ཁྱེད་ཀྱིས་དུས་གཏན་དུ་བསུབ་ན་ཆོག་གམ།</translation>
    </message>
    <message>
        <source>Failed to open file &quot;%1&quot;: insufficient permissions.</source>
        <translation>%1དབང་ཚད་མ་འདང་བའི་ཡིག་ཆ་ཁ་ཕྱེ་མ་ཐུབ་པ་རེད།</translation>
    </message>
    <message>
        <source>File “%1” does not exist. Please check whether the file has been deleted.</source>
        <translation>&quot;%1&quot;ཡིག་ཆ་མེད། ཁྱོད་ཀྱིས་ཡིག་ཆ་དེ་བསུབ་ཡོད་མེད་ལ་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>data</source>
        <translation>གཞི་གྲངས་སྡེར།</translation>
    </message>
    <message>
        <source>Computer</source>
        <translation>རྩིས་འཁོར།</translation>
    </message>
    <message>
        <source>File System</source>
        <translation>ཡིག་ཆའི་མ་ལག</translation>
    </message>
    <message>
        <source>The device has been mount successfully!</source>
        <translation>སྒྲིག་ཆས་འདི་བདེ་བླག་ངང་སྒྲིག་སྦྱོར་བྱས་པ་རེད།</translation>
    </message>
    <message>
        <source>Not authorized to perform operation.</source>
        <translation>གཉེར་སྐྱོང་བྱེད་དབང་མེད་པ།</translation>
    </message>
    <message>
        <source>Error: %1
Do you want to unmount forcely?</source>
        <translation>ནོར་འཁྲུལ། %1
ཁྱོད་ཀྱིས་ནུས་ཤུགས་ཡོད་རྒུ་བཏོན་ནས་ནུས་ཤུགས་ཡོད་རྒུ་བཏོན་ན་འདོད་དམ།</translation>
    </message>
    <message>
        <source>Formatting successful! But failed to set the device name.</source>
        <translation>རྣམ་གཞག་ལེགས་འགྲུབ་བྱུང་བ་རེད། འོན་ཀྱང་སྒྲིག་ཆས་ཀྱི་མིང་གཏན་འཁེལ་བྱེད་མ་ཐུབ་པ་རེད།</translation>
    </message>
    <message>
        <source>Data</source>
        <translation>གཞི་གྲངས་སྡེར།</translation>
    </message>
    <message>
        <source>File is not existed.</source>
        <translation>ཡིག་ཆ་མེད་པ།</translation>
    </message>
    <message>
        <source>Can not add a file to favorite directory.</source>
        <translation>ཆེས་དགའ་བའི་དཀར་ཆག་ནང་དུ་ཡིག་ཆ་ཞིག་ཁ་སྣོན་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>burn operation has been cancelled</source>
        <translation>བརྐོས་ཕབ་བཀོལ་སྤྱོད་མེད་པར་བཟོས་པ།</translation>
    </message>
    <message>
        <source> is busy!</source>
        <translation> བྲེལ་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>དོར་བ།</translation>
    </message>
    <message>
        <source>Do you want to empty the recycle bin and delete the files permanently? Once it has begun there is no way to restore them.</source>
        <translation>ཁྱོད་ཀྱིས་སྙིགས་སྒམ་གྱི་ནང་གི་ཡིག་ཆ་ཚང་མ་བསུབ་རྒྱུའི་རེ་བ་བྱེད་ཀྱི་ཡོད་དམ། འགོ་བརྩམས་མ་ཐག་སླར་གསོ་བྱེད་ཐབས་བྲལ་བ།</translation>
    </message>
    <message>
        <source>Trash</source>
        <translation>སྙིགས་སྒམ།</translation>
    </message>
    <message>
        <source>Recent</source>
        <translation type="unfinished">ཉེ་ཆར།</translation>
    </message>
    <message>
        <source>Failed to activate device: Incorrect passphrase</source>
        <translation>གསང་ཨང་ནོར་བས་སྒྲིག་ཆས་ཀྱི་ཁ་ཕྱེ་ཐབས་བྲལ་བ།</translation>
    </message>
</context>
<context>
    <name>UdfFormatDialog</name>
    <message>
        <source>Format</source>
        <translation type="unfinished">རྣམ་གཞག</translation>
    </message>
    <message>
        <source>Disc Type:</source>
        <translation>འོད་སྡེར་གྱི་རིགས།:</translation>
    </message>
    <message>
        <source>Device Name:</source>
        <translation>སྒྲིག་ཆས་མིང་།:</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished">དོར་བ།</translation>
    </message>
    <message>
        <source>Unknow</source>
        <translation>མ་ཤེས་པའི་རིགས།</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>ཐ་ཚིག</translation>
    </message>
    <message>
        <source>The disc name cannot be set to empty, please re-enter it!</source>
        <translation>འོད་སྡེར་གྱི་མིང་དེ་སྟོང་པར་བཀོད་སྒྲིག་བྱེད་ཐབས་བྲལ་བས་བསྐྱར་དུ་ནང་འཇུག་གནང་རོགས།</translation>
    </message>
    <message>
        <source>Format operation has been finished successfully.</source>
        <translation type="unfinished">རྣམ་བཞག་ཅན་གྱི་བཀོལ་སྤྱོད་ལེགས་གྲུབ་བྱུང་།</translation>
    </message>
    <message>
        <source>Sorry, the format operation is failed!</source>
        <translation>རྣམ་བཞག་བཟོ་བ་ཕམ་ཉེས་བྱུང་བས་ཡང་བསྐྱར་ཚོད་ལྟ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Failed</source>
        <translation>ཕམ་པ།</translation>
    </message>
    <message>
        <source>Formatting. Do not close this window</source>
        <translation>རྣམ་གཞག་བཟོ་བཞིན་ཡོད་པས་ཁ་རྒྱག་མི་རུང་།</translation>
    </message>
    <message>
        <source>Formatting this disc will erase all data on it. Please backup all retained data before formatting. Do you want to continue ?</source>
        <translation>འོད་སྡེར་རྣམ་པར་གཞག་ན་དེ་སྟེང་གི་གཞི་གྲངས་ཡོད་ཚད་གཙང་དག་བཟོ་བས། རྣམ་པར་འཇོག་པའི་སྔོན་ལ་གྲབས་ཉར་བྱེད་དགོས།མུ་མཐུད་དུ་བྱེད་རྒྱུ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <source>Begin Format</source>
        <translation>རྣམ་པར་འཇོག་འགོ་བརྩམས་པ།</translation>
    </message>
    <message>
        <source>Close</source>
        <translation type="unfinished">ཁ་རྒྱག</translation>
    </message>
</context>
</TS>
