<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AboutDialog</name>
    <message>
        <source>none</source>
        <translation type="vanished">མེད།</translation>
    </message>
    <message>
        <source>Peony</source>
        <translation>ཡིག་ཆ་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <source>Dialog</source>
        <translation>གླེང་སྒྲོམ།</translation>
    </message>
    <message>
        <source>Version number: %1</source>
        <translation>པར་གཞིའི་ཨང་རྟགས།%1</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར་ཚོགས་པ།： </translation>
    </message>
    <message>
        <source>Peony is a graphical software to help users manage system files. It provides common file operation functions for users, such as file viewing, file copy, paste, cut, delete, rename, file selection, application opening, file search, file sorting, file preview, etc. it is convenient for users to manage system files intuitively on the interface.</source>
        <translation>ཡིག་ཆ་དོ་དམ་ཆས་ནི་སྤྱོད་མཁན་ལ་རྒྱུད་ཁོངས་ཡིག་ཆ་དོ་དམ་ལ་རོགས་སྐྱོར་བྱེད་པའི་རིས་གཟུགས་མཉེན་ཆས་ཞིག་ཡིན། སྤྱོད་མཁན་ལ་རྒྱུན་བཀོལ་གྱི་ཡིག་ཆ་བཀོལ་སྤྱོད་ནུས་པ་འདོན་སྤྲོད་བྱེད། དཔེར་ན་ཡིག་ཆ་བཤེར་ལྟ་བྱ་དགོས་ན། ཡིག་ཆ་པར་སློག སྦྱར་བ། དྲས་གཏུབ། གསུབ་པ། མིང་བསྐྱར་འདོགས། ཡིག་ཁ་འབྱེད་ཐབས་གདམ་གསེས། ཡིག་ཆ་འཚོལ་བཤེར། ཡིག་ཆ་རིམ་སྒྲིག ཡིག་ཆ་སྔོན་ལྟ་བཅས་སྤྱོད་མཁན་ལ་སྟབས་བདེ་བསྐྲུན་ཡོད།</translation>
    </message>
    <message>
        <source>TextLabel</source>
        <translation>གདོང་འཛར།</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt; &lt;head&gt; &lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt; &lt;style type=&quot;text/css&quot;&gt;
p, li {དཀར་པོའི་བར་སྟོང་། སྔོན་ཚུད་ནས་ཐུམ་སྒྲིལ།}
&lt;/style&gt; &lt;/head&gt; &lt;body style=&quot; font-family:&apos;Noto Sans CJK SC&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px; font-size:11pt;&quot;&gt; &lt;br /&gt; &lt;/p&gt; &lt;/body&gt; &lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>FileLabelBox</name>
    <message>
        <source>Create New Label</source>
        <translation>མཚོན་རྟགས་གསར་སྐྲུན།</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation>མིང་བསྐྱར་འདོགས།</translation>
    </message>
    <message>
        <source>Edit Color</source>
        <translation>རྩོམ་སྒྲིག་ཁ་དོག</translation>
    </message>
</context>
<context>
    <name>HeaderBar</name>
    <message>
        <source>Don&apos;t find any terminal, please install at least one terminal!</source>
        <translation>མཐའ་སྣེར་བསྒར་ཡིག་ཅི་ཡང་མེད། ཁྱོད་ཀྱིས་མ་མཐའ་གཅིག་སྒྲིག་འཇུག་ཁག་ཐེག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Option</source>
        <translation>འདེམས་ཚན།</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>འཚོལ་བ།</translation>
    </message>
    <message>
        <source>Sort Type</source>
        <translation>རིམ་སྒྲིག་རིགས་གྲས།</translation>
    </message>
    <message>
        <source>View Type</source>
        <translation>གཟུགས་རིས་རིགས་རྣམ།</translation>
    </message>
    <message>
        <source>Go Forward</source>
        <translation>མདུན་སྐྱོད།</translation>
    </message>
    <message>
        <source>Go Back</source>
        <translation>ཕྱིར་ལོག</translation>
    </message>
    <message>
        <source>Operate Tips</source>
        <translation>གསལ་བརྡ།</translation>
    </message>
    <message>
        <source>Restore</source>
        <translation>སླར་གསོ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation>ཆེས་ཆེ་བསྒྱུར།</translation>
    </message>
</context>
<context>
    <name>HeaderBarContainer</name>
    <message>
        <source>Close</source>
        <translation>ཁ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>ཆེས་ཆུང་འགྱུར།</translation>
    </message>
    <message>
        <source>Maximize/Restore</source>
        <translation type="vanished">ཆེས་ཆེ་བ/སླར་གསོ།</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Redo</source>
        <translation>བསྐྱར་བཟོ།</translation>
    </message>
    <message>
        <source>Undo</source>
        <translation>འདོར་བ།</translation>
    </message>
    <message>
        <source>Trash has no file need to be cleaned.</source>
        <translation type="vanished">སྙིགས་སྣོད་དུ་གཙང་སེལ་བྱེད་དགོས་པའི་ཡིག་ཆ་མེད།</translation>
    </message>
    <message>
        <source>New Folder</source>
        <translation>ཡིག་ཁུག་གསར་འཛུགས།</translation>
    </message>
    <message>
        <source>Search</source>
        <translation>འཚོལ་བ།</translation>
    </message>
    <message>
        <source>Delete Permanently</source>
        <translation type="vanished">བརྟན་འདོར།</translation>
    </message>
    <message>
        <source>Tips info</source>
        <translation type="vanished">ལྷག་བསམ་གསལ་འདེབས།</translation>
    </message>
    <message>
        <source>File Manager</source>
        <translation>ཡིག་ཆ་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <source>Are you sure that you want to delete these files? Once you start a deletion, the files deleting will never be restored again.</source>
        <translation type="vanished">ཁྱོད་ཀྱིས་ཡིག་ཆ་འདི་དག་གསུབ་རྒྱུ་ཡིན་ནམ། གལ་ཏར་སུབ་ཚར་ན། ཡིག་ཆ་འདི་དག་ནམ་ཡང་སླར་གསོ་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>warn</source>
        <translation>ཉེན་བརྡ་བཏང་བ།</translation>
    </message>
    <message>
        <source>This operation is not supported.</source>
        <translation>བཀོལ་སྤྱོད་འདི་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
</context>
<context>
    <name>NavigationSideBar</name>
    <message>
        <source>Open In New Tab</source>
        <translation>ཤོག་བྱང་གསར་བའི་ནང་དུ་ཁ་ཕྱེ།</translation>
    </message>
    <message>
        <source>Can not open %1, %2</source>
        <translation>%1, %2ཁ་ཕྱེ་ཐུབ་མེད།</translation>
    </message>
    <message>
        <source>Open In New Window</source>
        <translation>སྒེའུ་ཁུང་གསར་བའི་ཁྲོད་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>Tips</source>
        <translation>གསལ་འདེབས་བྱེད་ཐབས།</translation>
    </message>
    <message>
        <source>This is an abnormal Udisk, please fix it or format it</source>
        <translation>འདི་ནི་རྒྱུན་ལྡན་མིན་པའི་Uསྡེར་ཞིག་ཡིན་པས་ཞིག་གསོ་དང་རྣམ་གཞག་ཅན་དུ་གཏོང་རོགས།</translation>
    </message>
    <message>
        <source>This is an empty drive, please insert a Disc.</source>
        <translation>འདི་ནི་སྟོང་བའི་སྒུལ་ཤུགས་ཤིག་རེད། ཁྱེད་ཀྱིས་འོད་སྡེར་ཞིག་འཇོག་རོགས།</translation>
    </message>
    <message>
        <source>warn</source>
        <translation>ཉེན་བརྡ་བཏང་བ།</translation>
    </message>
    <message>
        <source>This operation is not supported.</source>
        <translation>གཤགས་བཅོས་འདི་ལ་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <source>The device is in busy state, please perform this operation later.</source>
        <translation>སྒྲིག་ཆས་འདི་བྲེལ་བ་ཆེ་བས་རྗེས་སུ་གཤགས་བཅོས་འདི་བྱེད་རོགས།</translation>
    </message>
</context>
<context>
    <name>NavigationSideBarContainer</name>
    <message>
        <source>All tags...</source>
        <translation>མཚོན་རྟགས་ཡོངས་རྫོགས།</translation>
    </message>
</context>
<context>
    <name>NavigationTabBar</name>
    <message>
        <source>Search &quot;%1&quot; in &quot;%2&quot;</source>
        <translation>%2ཁྲོད་%1འཚོལ་ཞིབ།</translation>
    </message>
</context>
<context>
    <name>OperationMenu</name>
    <message>
        <source>Help</source>
        <translation>རོགས་རམ།</translation>
    </message>
    <message>
        <source>About</source>
        <translation>འབྲེལ་ཡོད།</translation>
    </message>
    <message>
        <source>Keep Allow</source>
        <translation>སྒེའུ་ཁུང་འགོ་འཇོག</translation>
    </message>
    <message>
        <source>Show Hidden</source>
        <translation>སྦས་པའི་ཡིག་ཆ་མངོན་པ།</translation>
    </message>
    <message>
        <source>Resident in Backend</source>
        <translation>རྒྱུན་སྡོད་རྒྱབ་སྟེགས།</translation>
    </message>
    <message>
        <source>Parallel Operations</source>
        <translation>བཀོལ་སྤྱོད་དང་ལག་བསྟར་བྱེད་ཆོག</translation>
    </message>
    <message>
        <source>Forbid thumbnailing</source>
        <translation>བསྡུས་རིས་བཀོལ་མི་ཆོག</translation>
    </message>
    <message>
        <source>Show File Extension</source>
        <translation>ཡིག་ཆ་ཁྱབ་གདལ་དུ་གཏོང་བ་མངོན་པ།</translation>
    </message>
</context>
<context>
    <name>OperationMenuEditWidget</name>
    <message>
        <source>cut</source>
        <translation>དྲས་གཏུབ་རྣམ་པ།</translation>
    </message>
    <message>
        <source>Edit</source>
        <translation>རྩོམ་སྒྲིག</translation>
    </message>
    <message>
        <source>copy</source>
        <translation>འདྲ་བཤུས།</translation>
    </message>
    <message>
        <source>paste</source>
        <translation>སྦྱར་བ།</translation>
    </message>
    <message>
        <source>trash</source>
        <translation>བསུབ་པ།</translation>
    </message>
</context>
<context>
    <name>PeonyApplication</name>
    <message>
        <source>Show properties</source>
        <translation>ཡིག་ཆའི་རང་གཤིས་སྒེའུ་ཁུང་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>Close all peony-qt windows and quit</source>
        <translation>སྒེའུ་ཁུང་ཐམས་ཅད་སྒོ་བརྒྱབ་ནས་ཕྱིར་འབུད།</translation>
    </message>
    <message>
        <source>Show items</source>
        <translation>ཡིག་ཆ་ཡོད་པའི་དཀར་ཆག་ཁ་ཕྱེ་བ་དང་ཁོ་ཚོ་འདེམ་དགོས།</translation>
    </message>
    <message>
        <source>Show folders</source>
        <translation>ཡིག་ཁུག་གི་ནང་དོན་མངོན་དགོས།</translation>
    </message>
    <message>
        <source>Peony Qt</source>
        <translation>ཡིག་ཆ་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <source>peony-qt</source>
        <translation>ཡིག་ཆ་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <source>Files or directories to open</source>
        <translation>ཁ་ཕྱེ་དགོས་པའི་ཡིག་ཆའམ་ཡིག་ཁུག།</translation>
    </message>
    <message>
        <source>Warning</source>
        <translation>ཉེན་བརྡ།</translation>
    </message>
    <message>
        <source>[FILE1, FILE2,...]</source>
        <translation>ཡིག་ཆ༡ ཡིག་ཆ༢</translation>
    </message>
    <message>
        <source>Peony-Qt can not get the system&apos;s icon theme. There are 2 reasons might lead to this problem:

1. Peony-Qt might be running as root, that means you have the higher permission and can do some things which normally forbidden. But, you should learn that if you were in a root, the virtual file system will lose some featrue such as you can not use &quot;My Computer&quot;, the theme and icons might also went wrong. So, run peony-qt in a root is not recommended.

2. You are using a non-qt theme for your system but you didn&apos;t install the platform theme plugin for qt&apos;s applications. If you are using gtk-theme, try installing the qt5-gtk2-platformtheme package to resolve this problem.</source>
        <translation>ཡིག་ཆ་དོ་དམ་ཆས་ཀྱིས་རྒྱུད་ཁོངས་པར་རྟགས་བརྗོད་གཞི་རྙེད་མ་ཐུབ་པའི་རྒྱུ་རྐྱེན་ནི།
 ༡ དོ་དམ་པའི་སྤྱོད་མཁན་གྱིས་ཡིག་ཆ་དོ་དམ་ཆས་སྤྱོད་པའི་སྐབས་ལ་འཁེལ་བ། འདི་ལས་ཁྱེད་ཀྱི་དབང་ཚད་གཞན་ལས་མཐོ་བ་མཚོན་མོང། འོན་ཀྱང་ཁྱེད་ཀྱིས་ངེས་པར་རང་ཉིད་ཀྱིས་སྤྱོད་མཁན་ཕལ་བའི་དབང་ཚད་འགའ་ཤས་ཤོར་བ་འང་ངོས་ཟིན་ཐུབ་དགོས། དཔེར་ན། བདག་གི་རྩིས་འཁོརའམ་རྒྱུད་ཁོངས་བརྗོད་གཞི། གལ་ཏེ་གནས་ཚུལ་ཁྱད་པར་ཅན་མིན་ན། ཁྱོད་ཀྱིས་དོ་དམ་པའི་སྤྱོད་མཁན་གྱིས་ཡིག་ཆ་དོ་དམ་ཆས་ཁ་མ་འབྱེད། 
༢ ཁྱོད་ཀྱིས་བཀོལ་བའི་རྒྱུད་ཁོངས་བརྗོད་གཞིར་qtསོར་བཞག་བརྗོད་གཞིས་རྒྱབ་སྐྱོར་མི་བྱེད་པ་དང་། ཁྱོད་ལ་གཞན་གྱི་འབྲེལ་ཡོད་སྟེགས་བུ་ནང་འཇུག་བྱས་མེད་ན། གལ་སྲིད་ཁྱོས་Gtkབརྗོད་གཞི་རྒྱུད་ཁོངས་ཀྱི་བརྗོད་གཞིར་བེད་སྤྱོད་བྱེད་བཞིན་ཡོད་ན། qt5-gtk2-platformthemeསྒྲིག་འཇུག་བྱས་ན་གནད་དོན་ཐག་གཅོད་བྱེད་ཐུབ།</translation>
    </message>
    <message>
        <source>Author:
	Yue Lan &lt;lanyue@kylinos.cn&gt;
	Meihong He &lt;hemeihong@kylinos.cn&gt;

Copyright (C): 2020, KylinSoft Co., Ltd.</source>
        <translation>རྩོམ་པ་པོ།  
Yue Lan &lt;lanyue@kylinos.cn&gt;
 Meihong He &lt;hemeihong@kylinos.cn&gt;
པར་དབང་ཡོད་ཚད (C): 2020 ཆི་ལིན་མཉེན་ཆས་ཚད་ཡོད་ཀུང་ཟི།</translation>
    </message>
</context>
<context>
    <name>SortTypeMenu</name>
    <message>
        <source>File Name</source>
        <translation>ཡིག་ཆའི་མིང་།</translation>
    </message>
    <message>
        <source>File Size</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>File Type</source>
        <translation>ཡིག་ཆའི་རིགས་གྲས།</translation>
    </message>
    <message>
        <source>Descending</source>
        <translation>རིམ་འབེབས།</translation>
    </message>
    <message>
        <source>Ascending</source>
        <translation>འཕེལ་རིམ།</translation>
    </message>
    <message>
        <source>Modified Date</source>
        <translation>ཟླ་ཚེས་བཅོས་བསྒྱུར།</translation>
    </message>
    <message>
        <source>Use global sorting</source>
        <translation>ཁྱོན་ཡོངས་ཀྱི་གོ་རིམ་བེད་སྤྱོད།</translation>
    </message>
    <message>
        <source>Original Path</source>
        <translation>ཐོག་མའི་འགྲོ་ལམ།</translation>
    </message>
</context>
<context>
    <name>TabStatusBar</name>
    <message>
        <source> %1 items </source>
        <translation> %1རྣམ་གྲངས་སྒེར། </translation>
    </message>
    <message>
        <source> selected %1 items</source>
        <translation> %1རྣམ་གྲངས་སྒེར་འདེམ་པ།</translation>
    </message>
    <message>
        <source> selected %1 items    %2</source>
        <translation> %1རྣམ་གྲངས་སྒེར་འདེམ་པ།    %2</translation>
    </message>
</context>
<context>
    <name>TabWidget</name>
    <message>
        <source>is</source>
        <translation>ཡིན།</translation>
    </message>
    <message>
        <source>all</source>
        <translation>ཚང་མ།</translation>
    </message>
    <message>
        <source>name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <source>type</source>
        <translation>རིགས་རྣམ།</translation>
    </message>
    <message>
        <source>Clear</source>
        <translation>གཙང་སེལ།</translation>
    </message>
    <message>
        <source>this year</source>
        <translation>ད་ལོ།</translation>
    </message>
    <message>
        <source>this week</source>
        <translation>གཟའ་འདི།</translation>
    </message>
    <message>
        <source>Trash</source>
        <translation>སྙིགས་སྒམ།</translation>
    </message>
    <message>
        <source>audio</source>
        <translation>སྒྲ་ཟློས།</translation>
    </message>
    <message>
        <source>image</source>
        <translation>པར་རིས།</translation>
    </message>
    <message>
        <source>today</source>
        <translation>དེ་རིང་།</translation>
    </message>
    <message>
        <source>video</source>
        <translation>བརྙན་ལམ།</translation>
    </message>
    <message>
        <source>file size</source>
        <translation>ཡིག་ཆའི་ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>big(128M-1G)</source>
        <translation>ཆེ(128M-1G)</translation>
    </message>
    <message>
        <source>Filter</source>
        <translation>འཚག་འདེམས།</translation>
    </message>
    <message>
        <source>medium(1M-128M)</source>
        <translation>འབྲིང་(1M-128M)</translation>
    </message>
    <message>
        <source>this month</source>
        <translation>ཟླ་འདི།</translation>
    </message>
    <message>
        <source>contains</source>
        <translation>ཚུད་པ།</translation>
    </message>
    <message>
        <source>Search recursively</source>
        <translation>རིམ་ལོག་འཚོལ་བཤེར།</translation>
    </message>
    <message>
        <source>file folder</source>
        <translation>ཡིག་ཁུག</translation>
    </message>
    <message>
        <source>Close Filter.</source>
        <translation>འཚག་འདེམས་སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>wps file</source>
        <translation>WPSཡིག་ཆ།</translation>
    </message>
    <message>
        <source>others</source>
        <translation>གཞན་དག</translation>
    </message>
    <message>
        <source>empty(0K)</source>
        <translation>སྟོང་པ(0K)</translation>
    </message>
    <message>
        <source>large(1-4G)</source>
        <translation>ཆེན་པོ(1-4G)</translation>
    </message>
    <message>
        <source>great(&gt;4G)</source>
        <translation>ཧ་ཅང་ཆེན་པོ།(&gt;4G)</translation>
    </message>
    <message>
        <source>Recover</source>
        <translation>སླར་གསོ།</translation>
    </message>
    <message>
        <source>Select path</source>
        <translation type="vanished">རྒྱུ་ལམ་གདམ་གསེས།</translation>
    </message>
    <message>
        <source>Choose other path to search.</source>
        <translation>གཞན་གྱི་འཚོལ་བཤེར་བརྒྱུད་ལམ་འདེམ་པ།</translation>
    </message>
    <message>
        <source>tiny(0-16K)</source>
        <translation>ཆེས་ཆུང(0-16K)</translation>
    </message>
    <message>
        <source>year ago</source>
        <translation>ལོ་གཅིག་གི་སྔོན།</translation>
    </message>
    <message>
        <source>modify time</source>
        <translation>ཆུ་ཚོད་བཅོས་བསྒྱུར།</translation>
    </message>
    <message>
        <source>Please input key words...</source>
        <translation>གནད་ཚིག་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>small(16k-1M)</source>
        <translation>ཆུང་ཆུང་(16k-1M</translation>
    </message>
    <message>
        <source>text file</source>
        <translation>ཡིག་ཆའི་ཡིག་ཚགས།</translation>
    </message>
    <message>
        <source>Select Path</source>
        <translation>ལམ་བུ་བདམས་པ།</translation>
    </message>
    <message>
        <source>Open failed</source>
        <translation>སྒོ་ཕྱེ་ནས་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <source>Open directory failed, you have no permission!</source>
        <translation>ཡིག་ཁུག་གི་ཁ་ཕྱེ་ནས་ཕམ་སོང་། ཁྱོད་ལ་དཀར་ཆག་འདིའི་དབང་ཆ་མེད།</translation>
    </message>
</context>
</TS>
