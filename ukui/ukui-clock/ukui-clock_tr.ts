<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr_TR">
<context>
    <name>About</name>
    <message>
        <location filename="../about.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../about.ui" line="87"/>
        <location filename="../about.ui" line="162"/>
        <source>Alarm</source>
        <translation>Saat</translation>
    </message>
    <message>
        <source>Kylin Alarm</source>
        <translation type="vanished">kylin saat</translation>
    </message>
    <message>
        <source>Clock</source>
        <translation type="vanished">kapat</translation>
    </message>
    <message>
        <source>Kylin Clock</source>
        <translation type="vanished">kylin saat</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="35"/>
        <source>About</source>
        <translation>hakkında</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="57"/>
        <source>Version: </source>
        <translation></translation>
    </message>
    <message>
        <source>Version: 2020.1.0</source>
        <translation type="vanished">Versyon: 2020.1.8</translation>
    </message>
    <message>
        <location filename="../about.cpp" line="116"/>
        <location filename="../about.cpp" line="124"/>
        <source>Service &amp; Support: </source>
        <translation></translation>
    </message>
    <message>
        <source>Support and service team: support@kylinos.cn</source>
        <translation type="vanished">Destek ve hizmet ekibi:support@kylinos.cn</translation>
    </message>
</context>
<context>
    <name>Clock</name>
    <message>
        <source>Clock</source>
        <translation type="vanished">Saat</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="460"/>
        <location filename="../clock.ui" line="582"/>
        <location filename="../clock.cpp" line="867"/>
        <location filename="../clock.cpp" line="965"/>
        <location filename="../clock.cpp" line="2596"/>
        <location filename="../clock.cpp" line="2913"/>
        <source>start</source>
        <translation>Başlat</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="350"/>
        <location filename="../clock.ui" line="600"/>
        <location filename="../clock.ui" line="616"/>
        <source>00:00:00</source>
        <translation></translation>
    </message>
    <message>
        <source>5min</source>
        <translation type="vanished">5 dk</translation>
    </message>
    <message>
        <source>15min</source>
        <translation type="vanished">15 dk</translation>
    </message>
    <message>
        <source>25min</source>
        <translation type="vanished">25 dk</translation>
    </message>
    <message>
        <source>30min</source>
        <translation type="vanished">30 dk</translation>
    </message>
    <message>
        <source>60min</source>
        <translation type="vanished">60 dk</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="482"/>
        <location filename="../clock.cpp" line="749"/>
        <location filename="../clock.cpp" line="2601"/>
        <location filename="../clock.cpp" line="2823"/>
        <source>suspend</source>
        <translation>Askıya Al</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="394"/>
        <source>icon</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.ui" line="511"/>
        <source>add</source>
        <translation>Ekle</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="554"/>
        <source>no alarm</source>
        <translation>no Alarm</translation>
    </message>
    <message>
        <source>delete</source>
        <translation type="vanished">Sil</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="715"/>
        <source>save</source>
        <translation>Cts</translation>
    </message>
    <message>
        <source>Remind</source>
        <translation type="vanished">Hatırlat</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="413"/>
        <location filename="../clock.cpp" line="1577"/>
        <location filename="../clock.cpp" line="2720"/>
        <location filename="../clock.cpp" line="2797"/>
        <source>PM</source>
        <translation>ÖS</translation>
    </message>
    <message>
        <source>add alarm</source>
        <translation type="vanished">Alarm Ekle</translation>
    </message>
    <message>
        <source>Remaining time</source>
        <translation type="vanished">Kalan süre</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="791"/>
        <source>reset</source>
        <translation>Sıfırla</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="638"/>
        <location filename="../clock.cpp" line="770"/>
        <location filename="../clock.cpp" line="895"/>
        <source>count</source>
        <translation>İşaret</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="213"/>
        <location filename="../clock.cpp" line="123"/>
        <location filename="../clock.cpp" line="2433"/>
        <location filename="../clock.cpp" line="2501"/>
        <source>Count down</source>
        <translation>Geri Sayım</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="14"/>
        <location filename="../clock.ui" line="48"/>
        <location filename="../clock.ui" line="178"/>
        <source>Alarm</source>
        <translation>Alarm</translation>
    </message>
    <message>
        <source>Stopwatch</source>
        <translation type="vanished">Kronometre</translation>
    </message>
    <message>
        <source>deletealarm</source>
        <translation type="vanished">Alarmı Sil</translation>
    </message>
    <message>
        <source>Preservation</source>
        <translation type="vanished">Kaydet</translation>
    </message>
    <message>
        <source>12hour43minThe bell rings</source>
        <translation type="vanished">12 saat 43 dk zil çalıyor</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="693"/>
        <source>cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <source>New alarm</source>
        <translation type="vanished">Yeni Alarm</translation>
    </message>
    <message>
        <source>  Name</source>
        <translation type="vanished">  İsim</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="357"/>
        <source>  repeat</source>
        <translation>  Tekrarla</translation>
    </message>
    <message>
        <source>  Remind</source>
        <translation type="vanished">  Hatırlat</translation>
    </message>
    <message>
        <source>  ring time</source>
        <translation type="vanished">  Çalma Zamanı</translation>
    </message>
    <message>
        <source> ring time</source>
        <translation type="vanished"> Çalma Zamanı</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="751"/>
        <source>On</source>
        <translation>Açık</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="vanished">Devam Et</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="896"/>
        <source>interval </source>
        <translation>Aralık </translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1584"/>
        <location filename="../clock.cpp" line="2722"/>
        <location filename="../clock.cpp" line="2804"/>
        <source>AM</source>
        <translation>ÖÖ</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1450"/>
        <source>2min</source>
        <translation>2 dk</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="365"/>
        <source>  remind</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1452"/>
        <source>3min</source>
        <translation>3 dk</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1454"/>
        <source>4min</source>
        <translation>4 dk</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1456"/>
        <source>6min</source>
        <translation>6 dk</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1463"/>
        <location filename="../clock.cpp" line="1646"/>
        <location filename="../clock.cpp" line="1996"/>
        <location filename="../clock.cpp" line="2100"/>
        <location filename="../clock.cpp" line="2101"/>
        <location filename="../clock.cpp" line="3122"/>
        <location filename="../clock.cpp" line="3160"/>
        <location filename="../clock.cpp" line="3161"/>
        <source>No repetition</source>
        <translation>Tekrar yok</translation>
    </message>
    <message>
        <source> Seconds to close</source>
        <translation type="vanished"> saniye sonra kapanacak</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1644"/>
        <location filename="../clock.cpp" line="1705"/>
        <location filename="../clock.cpp" line="3123"/>
        <location filename="../clock.cpp" line="3175"/>
        <location filename="../clock.cpp" line="3176"/>
        <source>Workingday</source>
        <translation>İş günü</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1707"/>
        <source>(default)</source>
        <translation>(varsayılan)</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1771"/>
        <location filename="../clock.cpp" line="1828"/>
        <source>Please set alarm name!</source>
        <translation>Lütfen alarm adını ayarlayın!</translation>
    </message>
    <message>
        <source>hour </source>
        <translation type="vanished">Saat </translation>
    </message>
    <message>
        <source> min bell rings</source>
        <translation type="vanished"> dk sonra alarm çalar</translation>
    </message>
    <message>
        <source>Edit alarm clock</source>
        <translation type="vanished">Alarm saatini düzenle</translation>
    </message>
    <message>
        <source>点击闹钟显示剩余时间</source>
        <translation type="vanished">Alarm saatini düzenle</translation>
    </message>
    <message>
        <source>Count</source>
        <translation type="vanished">Geri Sayım</translation>
    </message>
    <message>
        <location filename="../clock.ui" line="248"/>
        <location filename="../clock.cpp" line="127"/>
        <source>Watch</source>
        <translation>Kronometre</translation>
    </message>
    <message>
        <source> days </source>
        <translation type="vanished"> gün </translation>
    </message>
    <message>
        <source> hour </source>
        <translation type="vanished"> Saat </translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1993"/>
        <source>glass</source>
        <translation>Bardak</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="344"/>
        <location filename="../clock.cpp" line="346"/>
        <location filename="../clock.cpp" line="361"/>
        <source>  bell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1256"/>
        <source>Minimize</source>
        <translation>küçültmek</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="240"/>
        <location filename="../clock.cpp" line="1273"/>
        <source>Quit</source>
        <translation>Çık</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1285"/>
        <source>Menu</source>
        <translation>Menü</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1300"/>
        <source>Help</source>
        <translation>Yardım</translation>
    </message>
    <message>
        <source>bark</source>
        <translation type="vanished">Havlama</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="352"/>
        <source>  name</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="495"/>
        <source>edit</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sonar</source>
        <translation type="vanished">Sonar</translation>
    </message>
    <message>
        <source>drip</source>
        <translation type="vanished">Damla</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="918"/>
        <source>up to 100 times</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1227"/>
        <source>mute</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1231"/>
        <source>All bells are off</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>360 Seconds to close</source>
        <translation type="vanished">Kapatmak için 360 Saniye</translation>
    </message>
    <message>
        <source>Time out</source>
        <translation type="vanished">Zaman doldu</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2575"/>
        <source>End</source>
        <translation>Son</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2705"/>
        <source>after tomorrow</source>
        <translation>Yarından sonra</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2708"/>
        <source>Tomorrow</source>
        <translation>Yarın</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2883"/>
        <location filename="../clock.cpp" line="3092"/>
        <source>hour</source>
        <translation>Saat</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2886"/>
        <location filename="../clock.cpp" line="3095"/>
        <source>min</source>
        <translation>Dk</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2889"/>
        <source>sec</source>
        <translation>Sn</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1619"/>
        <location filename="../clock.cpp" line="2074"/>
        <location filename="../clock.cpp" line="3124"/>
        <location filename="../clock.cpp" line="3149"/>
        <source>Mon</source>
        <translation>Pzt</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="obsolete">Kapat</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="493"/>
        <source>Delete</source>
        <translation>Sil</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="494"/>
        <source>ClearAll</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="780"/>
        <location filename="../clock.cpp" line="2843"/>
        <source>continue</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Set Up</source>
        <translation type="vanished">ayarlandır</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1301"/>
        <source>About</source>
        <translation>hakkında</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1302"/>
        <source>Close</source>
        <translation>kapat</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1483"/>
        <source>recent alarm</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1621"/>
        <location filename="../clock.cpp" line="2076"/>
        <location filename="../clock.cpp" line="3125"/>
        <location filename="../clock.cpp" line="3150"/>
        <source>Tue</source>
        <translation>Sal</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1623"/>
        <location filename="../clock.cpp" line="2078"/>
        <location filename="../clock.cpp" line="3126"/>
        <location filename="../clock.cpp" line="3151"/>
        <source>Wed</source>
        <translation>Çar</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1625"/>
        <location filename="../clock.cpp" line="2080"/>
        <location filename="../clock.cpp" line="3127"/>
        <location filename="../clock.cpp" line="3152"/>
        <source>Thu</source>
        <translation>Per</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1627"/>
        <location filename="../clock.cpp" line="2082"/>
        <location filename="../clock.cpp" line="3128"/>
        <location filename="../clock.cpp" line="3153"/>
        <source>Fri</source>
        <translation>Cum</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1629"/>
        <location filename="../clock.cpp" line="2084"/>
        <location filename="../clock.cpp" line="3129"/>
        <location filename="../clock.cpp" line="3154"/>
        <source>Sat</source>
        <translation>Cts</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1631"/>
        <location filename="../clock.cpp" line="2086"/>
        <location filename="../clock.cpp" line="3130"/>
        <location filename="../clock.cpp" line="3155"/>
        <source>Sun</source>
        <translation>Paz</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1641"/>
        <location filename="../clock.cpp" line="1642"/>
        <location filename="../clock.cpp" line="2095"/>
        <location filename="../clock.cpp" line="3292"/>
        <source>Every day</source>
        <translation>Her gün</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1712"/>
        <location filename="../clock.cpp" line="3340"/>
        <location filename="../clock.cpp" line="3385"/>
        <location filename="../clock.cpp" line="3389"/>
        <location filename="../clock.cpp" line="3407"/>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1841"/>
        <source>warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1841"/>
        <source>the number of alarms reaches limit!！</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="1841"/>
        <source>yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2097"/>
        <location filename="../clock.cpp" line="2098"/>
        <source>  work</source>
        <translation>  İş</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2097"/>
        <source>  工作日</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2100"/>
        <source>不重复</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2541"/>
        <source>60 Seconds to close</source>
        <translation type="unfinished">Kapatmak için 360 Saniye {60 ?}</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3341"/>
        <location filename="../clock.cpp" line="3392"/>
        <source>five mins late</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3342"/>
        <location filename="../clock.cpp" line="3395"/>
        <source>ten mins late</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3343"/>
        <location filename="../clock.cpp" line="3398"/>
        <source>twenty mins late</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3344"/>
        <location filename="../clock.cpp" line="3401"/>
        <source>thirsty mins late</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3345"/>
        <location filename="../clock.cpp" line="3404"/>
        <source>one hour late</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="3589"/>
        <source>mini window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>glass(default)</source>
        <translation type="vanished">Bardak (varsayılan)</translation>
    </message>
    <message>
        <source>bark(default)</source>
        <translation type="vanished">Havlama (varsayılan)</translation>
    </message>
    <message>
        <source>sonar(default)</source>
        <translation type="vanished">Sonar(varsayılan)</translation>
    </message>
    <message>
        <source>drip(default)</source>
        <translation type="vanished">Damla(varsayılan)</translation>
    </message>
    <message>
        <source>1min</source>
        <translation type="vanished">2 dk</translation>
    </message>
    <message>
        <source>Monday to Friday</source>
        <translation type="vanished">Pazartesiden Cumaya</translation>
    </message>
    <message>
        <source>24 hour system</source>
        <translation type="vanished">24 saat düzeni</translation>
    </message>
    <message>
        <source>Notification</source>
        <translation type="vanished">Bildirim</translation>
    </message>
    <message>
        <source>Alert in 2 minutes</source>
        <translation type="vanished">2 dakika içinde uyar</translation>
    </message>
</context>
<context>
    <name>Natice_alarm</name>
    <message>
        <location filename="../noticeAlarm.ui" line="14"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../noticeAlarm.ui" line="120"/>
        <source>Alarm clock</source>
        <translation>Saat</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.ui" line="227"/>
        <source>11:20 设计例会...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>工作会议</source>
        <translation type="vanished">Toplantı</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.ui" line="334"/>
        <source>Remind later</source>
        <translation>Hatırlat</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="vanished">Kapat</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.cpp" line="60"/>
        <source>Ring prompt</source>
        <translation>Çalma İsteği</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.cpp" line="182"/>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../noticeAlarm.cpp" line="189"/>
        <source>Time out</source>
        <translation type="unfinished">Zaman doldu</translation>
    </message>
    <message>
        <source>glass</source>
        <translation type="vanished">Bardak</translation>
    </message>
    <message>
        <source>bark</source>
        <translation type="vanished">Havlama</translation>
    </message>
    <message>
        <source>sonar</source>
        <translation type="vanished">Sonar</translation>
    </message>
    <message>
        <source>drip</source>
        <translation type="vanished">Damla</translation>
    </message>
    <message>
        <location filename="../noticeAlarm.cpp" line="283"/>
        <source> Seconds to close</source>
        <translation> dakika sonra kapanacak</translation>
    </message>
</context>
<context>
    <name>Notice_Dialog</name>
    <message>
        <source>Ring prompt</source>
        <translation type="vanished">Çalma İsteği</translation>
    </message>
    <message>
        <source>glass</source>
        <translation type="vanished">Bardak</translation>
    </message>
    <message>
        <source>bark</source>
        <translation type="vanished">Havlama</translation>
    </message>
    <message>
        <source>sonar</source>
        <translation type="vanished">Sonar</translation>
    </message>
    <message>
        <source>drip</source>
        <translation type="vanished">Damla</translation>
    </message>
    <message>
        <source>End of countdown time</source>
        <translation type="vanished">dk sonra kapanacak</translation>
    </message>
    <message>
        <source>秒后关闭铃声</source>
        <translation type="vanished">Zili saniyeler içinde kapatın</translation>
    </message>
    <message>
        <source>闹钟:</source>
        <translation type="vanished">Alarm saati:</translation>
    </message>
    <message>
        <source>起床铃</source>
        <translation type="vanished">Uyandırma zili</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../clock.cpp" line="2175"/>
        <source>Hint</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2176"/>
        <source>Are you sure to delete？</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2177"/>
        <source>sure</source>
        <translation type="unfinished">Tamam</translation>
    </message>
    <message>
        <location filename="../clock.cpp" line="2178"/>
        <source>cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
</context>
<context>
    <name>SelectBtnUtil</name>
    <message>
        <location filename="../selectbtnutil.cpp" line="180"/>
        <source>glass</source>
        <translation type="unfinished">Bardak</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="181"/>
        <source>bark</source>
        <translation type="unfinished">Havlama</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="182"/>
        <source>sonar</source>
        <translation type="unfinished">Sonar</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="183"/>
        <source>drip</source>
        <translation type="unfinished">Damla</translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="151"/>
        <source>diy bell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="179"/>
        <source>none</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="269"/>
        <source>select bell</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../selectbtnutil.cpp" line="267"/>
        <source>audio files(*mp3 *wav *ogg)</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>TestWidget</name>
    <message>
        <location filename="../countdownAnimation.cpp" line="110"/>
        <source>TestWidget</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>VerticalScroll_24</name>
    <message>
        <location filename="../verticalScroll24.cpp" line="190"/>
        <source>PM</source>
        <translation>ÖS</translation>
    </message>
    <message>
        <location filename="../verticalScroll24.cpp" line="192"/>
        <source>AM</source>
        <translation>ÖÖ</translation>
    </message>
    <message>
        <location filename="../verticalScroll24.cpp" line="227"/>
        <source>VerticalScroll_24</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>VerticalScroll_60</name>
    <message>
        <location filename="../verticalScroll60.cpp" line="161"/>
        <source>VerticalScroll_60</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>VerticalScroll_99</name>
    <message>
        <location filename="../verticalScroll99.cpp" line="172"/>
        <source>VerticalScroll_99</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>close_or_hide</name>
    <message>
        <location filename="../closeOrHide.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="310"/>
        <source>sure</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="406"/>
        <source>请选择关闭后的状态</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="270"/>
        <source>cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="126"/>
        <source> backstage</source>
        <translation> Arka Çalış</translation>
    </message>
    <message>
        <source>backstage</source>
        <translation type="vanished">Arka Çalış</translation>
    </message>
    <message>
        <location filename="../closeOrHide.ui" line="178"/>
        <source> Exit program </source>
        <translation> Kapat </translation>
    </message>
    <message>
        <location filename="../closeOrHide.cpp" line="41"/>
        <source>Please select the state after closing:</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>delete_msg</name>
    <message>
        <location filename="../deleteMsg.ui" line="14"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../deleteMsg.ui" line="241"/>
        <source>sure</source>
        <translation>Tamam</translation>
    </message>
    <message>
        <location filename="../deleteMsg.ui" line="206"/>
        <source>cancel</source>
        <translation>İptal</translation>
    </message>
    <message>
        <location filename="../deleteMsg.ui" line="85"/>
        <source>are you sure ?</source>
        <translation>Emin misiniz?</translation>
    </message>
</context>
<context>
    <name>item_new</name>
    <message>
        <location filename="../itemNew.cpp" line="81"/>
        <source>Form</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>set_alarm_repeat_Dialog</name>
    <message>
        <location filename="../setAlarmRepeatDialog.cpp" line="40"/>
        <source>Alarm</source>
        <translation>Alarm</translation>
    </message>
    <message>
        <location filename="../setAlarmRepeatDialog.cpp" line="148"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>setuppage</name>
    <message>
        <source>开机启动</source>
        <translation type="vanished">Başlat</translation>
    </message>
    <message>
        <source>Boot up</source>
        <translation type="vanished">Önyükleme</translation>
    </message>
    <message>
        <source>  work</source>
        <translation type="vanished">  İş</translation>
    </message>
    <message>
        <source>  Time</source>
        <translation type="vanished">  Zaman</translation>
    </message>
    <message>
        <source>  Pop-up</source>
        <translation type="vanished">  Açılır</translation>
    </message>
    <message>
        <source>  duration</source>
        <translation type="vanished">  Süre</translation>
    </message>
    <message>
        <source>  ringtone</source>
        <translation type="vanished">  Zil sesi</translation>
    </message>
    <message>
        <source>  Mute</source>
        <translation type="vanished">  Sessiz</translation>
    </message>
    <message>
        <source>work</source>
        <translation type="vanished">İş</translation>
    </message>
    <message>
        <source>Time</source>
        <translation type="vanished">Zaman</translation>
    </message>
    <message>
        <source>Pop-up</source>
        <translation type="vanished">Açılır</translation>
    </message>
    <message>
        <source>duration</source>
        <translation type="vanished">Süre</translation>
    </message>
    <message>
        <source>ringtone</source>
        <translation type="vanished">Zil sesi</translation>
    </message>
    <message>
        <source>Mute</source>
        <translation type="vanished">Sessiz</translation>
    </message>
    <message>
        <source>volume</source>
        <translation type="vanished">Ses</translation>
    </message>
    <message>
        <source>setting</source>
        <translation type="vanished">Ayar</translation>
    </message>
    <message>
        <source>Mon</source>
        <translation type="vanished">Pzt</translation>
    </message>
    <message>
        <source>Tue</source>
        <translation type="vanished">Sal</translation>
    </message>
    <message>
        <source>Wed</source>
        <translation type="vanished">Çar</translation>
    </message>
    <message>
        <source>Thu</source>
        <translation type="vanished">Per</translation>
    </message>
    <message>
        <source>Fri</source>
        <translation type="vanished">Cum</translation>
    </message>
    <message>
        <source>Sat</source>
        <translation type="vanished">Cts</translation>
    </message>
    <message>
        <source>Sun</source>
        <translation type="vanished">Paz</translation>
    </message>
    <message>
        <source>Every day</source>
        <translation type="vanished">Her Gün</translation>
    </message>
    <message>
        <source>Following system</source>
        <translation type="vanished">Takip sistemi</translation>
    </message>
    <message>
        <source>24 hour system</source>
        <translation type="vanished">24 saat düzeni</translation>
    </message>
    <message>
        <source>12 hour system</source>
        <translation type="vanished">12 saat düzeni</translation>
    </message>
    <message>
        <source>Notification</source>
        <translation type="vanished">Bildirim</translation>
    </message>
    <message>
        <source>Full screen</source>
        <translation type="vanished">Tam ekran</translation>
    </message>
    <message>
        <source>Alert in 2 minutes</source>
        <translation type="vanished">2 dakika içinde uyar</translation>
    </message>
    <message>
        <source>Alert in 5 minutes</source>
        <translation type="vanished">5 dakika içinde uyar</translation>
    </message>
    <message>
        <source>Alert in 10 minutes</source>
        <translation type="vanished">10 dakika içinde uyar</translation>
    </message>
    <message>
        <source>Alert in 30 minutes</source>
        <translation type="vanished">30 dakika içinde uyar</translation>
    </message>
    <message>
        <source>Alert in 60 minutes</source>
        <translation type="vanished">60 dakika içinde uyar</translation>
    </message>
    <message>
        <source>glass</source>
        <translation type="vanished">Bardak</translation>
    </message>
    <message>
        <source>bark</source>
        <translation type="vanished">Havlama</translation>
    </message>
    <message>
        <source>sonar</source>
        <translation type="vanished">Sonar</translation>
    </message>
    <message>
        <source>drip</source>
        <translation type="vanished">Damla</translation>
    </message>
</context>
<context>
    <name>stopwatch_item</name>
    <message>
        <location filename="../stopwatchItem.cpp" line="74"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../stopwatchItem.cpp" line="51"/>
        <location filename="../stopwatchItem.cpp" line="114"/>
        <source>max</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../stopwatchItem.cpp" line="122"/>
        <source>min</source>
        <translation type="unfinished">Dk</translation>
    </message>
</context>
<context>
    <name>tinyCountdown</name>
    <message>
        <location filename="../tinycountdown.ui" line="26"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tinycountdown.ui" line="72"/>
        <source>01:29:58</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tinycountdown.cpp" line="259"/>
        <source>close</source>
        <translation type="unfinished">Kapat</translation>
    </message>
    <message>
        <location filename="../tinycountdown.cpp" line="267"/>
        <source>main window</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../tinycountdown.cpp" line="279"/>
        <source>suspend</source>
        <translation type="unfinished">Askıya Al</translation>
    </message>
    <message>
        <location filename="../tinycountdown.cpp" line="298"/>
        <source>finish</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
