<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>BlueToothMain</name>
    <message>
        <source>Show icon on taskbar</source>
        <translation>ᠡᠬᠦᠷᠭᠡ ᠵᠢᠨ ᠪᠠᠭᠠᠷ ᠲᠤ᠌ ᠯᠠᠨᠶᠠ ᠵᠢᠨ ᠵᠢᠷᠤᠭ ᠢ᠋ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
        <extra-contents_path>/bluetooth/Show icon on taskbar</extra-contents_path>
    </message>
    <message>
        <source>Discoverable by nearby Bluetooth devices</source>
        <translation>ᠵᠡᠷᠬᠡᠯᠡᠳᠡᠬᠡ ᠯᠠᠨᠶᠠ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠳ᠋ᠤ᠌ ᠢᠯᠡᠷᠡᠬᠦᠯᠦᠭᠳᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ</translation>
        <extra-contents_path>/bluetooth/Discoverable</extra-contents_path>
    </message>
    <message>
        <source>My Devices</source>
        <translation>ᠮᠢᠨᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation>ᠬᠦᠬᠡ ᠰᠢᠳᠦ</translation>
    </message>
    <message>
        <source>Other Devices</source>
        <translation>ᠪᠤᠰᠤᠳ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ</translation>
        <extra-contents_path>/bluetooth/Other Devices</extra-contents_path>
    </message>
    <message>
        <source>Bluetooth adapter</source>
        <translation>ᠯᠠᠨᠶᠠ ᠳ᠋ᠤ᠌ ᠳᠤᠬᠢᠷᠠᠬᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ</translation>
        <extra-contents_path>/Bluetooth/Bluetooth adapter</extra-contents_path>
    </message>
    <message>
        <source>Turn on</source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ</translation>
        <extra-contents_path>/bluetooth/Turn on Bluetooth</extra-contents_path>
    </message>
    <message>
        <source>All</source>
        <translation>ᠪᠦᠬᠦ</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation>ᠠᠦ᠋ᠳᠢᠤ᠋ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <source>Peripherals</source>
        <translation>ᠳᠠᠷᠤᠪᠴᠢ ᠬᠤᠯᠤᠭᠠᠨᠴᠢᠷ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ</translation>
    </message>
    <message>
        <source>PC</source>
        <translation>ᠺᠤᠮᠫᠢᠦᠲᠸᠷ</translation>
    </message>
    <message>
        <source>Phone</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <source>Other</source>
        <translation>ᠪᠤᠰᠤᠳ</translation>
    </message>
    <message>
        <source>Bluetooth driver abnormal</source>
        <translation>ᠯᠠᠨᠶᠠ ᠵᠢᠨ ᠬᠦᠳᠡᠯᠬᠡᠯᠳᠡ ᠬᠡᠪ ᠤ᠋ᠨ ᠪᠤᠰᠤ</translation>
    </message>
    <message>
        <source>Auto discover Bluetooth audio devices</source>
        <translation>ᠯᠠᠨᠶᠠ ᠠᠦ᠋ᠳᠢᠤ᠋ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠠᠦ᠋ᠲ᠋ᠣ᠋ ᠪᠡᠷ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ</translation>
        <extra-contents_path>/bluetooth/Automatically discover Bluetooth audio devices</extra-contents_path>
    </message>
</context>
<context>
    <name>BlueToothMainWindow</name>
    <message>
        <source>Bluetooth adapter is abnormal !</source>
        <translation type="obsolete">སོ་སྔོན་བཀོད་སྒྲིག་ཡོ་ཆས་རྒྱུན་ལྡན་མིན།</translation>
    </message>
    <message>
        <source>Bluetooth</source>
        <translation type="obsolete">སོ་སྔོན།</translation>
    </message>
    <message>
        <source>Turn on :</source>
        <translation type="obsolete">སྒོ་འབྱེད།</translation>
    </message>
    <message>
        <source>Show icon on taskbar</source>
        <translation type="obsolete">འགན་བྱང་དུ་སོ་སྔོན་རིས་རྟགས་འཆར་བ།</translation>
    </message>
    <message>
        <source>Discoverable by nearby Bluetooth devices</source>
        <translation type="obsolete">ཉེ་འཁོར་གྱི་སོ་སྔོན་སྒྲིག་ཆས་ཀྱི་བཤེར་རུང་བ།</translation>
    </message>
    <message>
        <source>My Devices</source>
        <translation type="obsolete">སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <source>Other Devices</source>
        <translation type="obsolete">སྒྲིག་ཆས་གཞན་དག</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="obsolete">དྲ་བྱང་།</translation>
    </message>
    <message>
        <source>Other</source>
        <translation type="obsolete">གཞན་དག</translation>
    </message>
</context>
<context>
    <name>Bluetooth</name>
    <message>
        <source>Bluetooth</source>
        <translation>ᠯᠠᠨᠶᠠ</translation>
    </message>
</context>
<context>
    <name>BluetoothNameLabel</name>
    <message>
        <source>Tip</source>
        <translation>ᠠᠨᠭᠬᠠᠷᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <source>Double-click to change the device name</source>
        <translation type="vanished">ཟུང་རྡེབ་བྱས་ཏེ་སྒྲིག་ཆས་ཀྱི་མིང་བཅོས་རོགས།</translation>
    </message>
    <message>
        <source>The length of the device name does not exceed %1 characters !</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢᠨ ᠤᠷᠳᠤ%1 ᠦᠰᠦᠭ ᠡᠴᠡ ᠳᠠᠪᠠᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <source>Can now be found as &quot;%1&quot;</source>
        <translation>ᠤᠳᠤ ᠪᠡᠷ ᠢᠯᠡᠷᠡᠬᠦᠯᠵᠤ ᠪᠤᠯᠬᠤ &quot;%1&quot;</translation>
    </message>
    <message>
        <source>Click to change the device name</source>
        <translation>ᠳᠤᠪᠰᠢᠵᠤ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ ᠵᠢ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
</context>
<context>
    <name>DevRemoveDialog</name>
    <message>
        <source>After it is removed, the PIN code must be matched for the next connection.</source>
        <translation>ᠰᠢᠯᠵᠢᠬᠦᠯᠦᠨ ᠤᠰᠠᠳᠬᠠᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ᠂ ᠳᠠᠬᠢᠭᠠᠳ ᠴᠦᠷᠬᠡᠯᠡᠬᠦ ᠳ᠋ᠤ᠌ PIN ᠺᠤᠳ᠋ ᠲᠠᠢ ᠠᠪᠤᠴᠠᠯᠳᠤᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ.</translation>
    </message>
    <message>
        <source>Remove</source>
        <translation>ᠤᠰᠠᠳᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>Are you sure to remove %1 ?</source>
        <translation>%1 ᠵᠢ/ ᠢ᠋ ᠯᠠᠪᠳᠠᠢ ᠤᠰᠠᠳᠬᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <source>Connection failed! Please remove it before connecting.</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠤᠰᠠᠳᠬᠠᠭᠰᠠᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠳᠠᠬᠢᠭᠠᠳ ᠴᠦᠷᠬᠡᠯᠡᠬᠡᠷᠡᠢ.</translation>
    </message>
    <message>
        <source>Bluetooth Connections</source>
        <translation>ᠯᠠᠨᠶᠠ ᠵᠢ ᠬᠤᠯᠪᠤᠪᠠ</translation>
    </message>
</context>
<context>
    <name>DevRenameDialog</name>
    <message>
        <source>Rename</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ ᠢ᠋ ᠵᠠᠰᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Name</source>
        <translation>ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <source>The value contains 1 to 32 characters</source>
        <translation>ᠤᠷᠳᠤ ᠨᠢ ᠡᠷᠬᠡᠪᠰᠢ 1-32 ᠦᠰᠦᠭ ᠪᠠᠢᠬᠤ ᠬᠡᠷᠡᠭᠳᠡᠢ</translation>
    </message>
</context>
<context>
    <name>DeviceInfoItem</name>
    <message>
        <source>Connecting</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Disconnecting</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠵᠢ ᠳᠠᠰᠤᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Connected</source>
        <translation>ᠨᠢᠬᠡᠨᠳᠡ ᠴᠦᠷᠬᠡᠯᠡᠪᠡ</translation>
    </message>
    <message>
        <source>Connect fail</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>Disconnect fail</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠵᠢ ᠳᠠᠰᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>send file</source>
        <translation>ᠹᠠᠢᠯ ᠢᠯᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>remove</source>
        <translation>ᠤᠰᠠᠳᠬᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Not Connected</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <source>disconnect</source>
        <translation>ᠴᠦᠷᠬᠡᠯᠡᠬᠡ᠎ᠶᠢ ᠳᠠᠰᠤᠯᠬᠤ</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source></source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
