<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="kk">
<context>
    <name>BaseDialog</name>
    <message>
        <source>Disk test</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DeviceOperation</name>
    <message>
        <source>unknown</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FDClickWidget</name>
    <message>
        <source>the capacity is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>blank CD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>other user device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>another device</source>
        <translation type="obsolete">其它设备</translation>
    </message>
    <message>
        <source>Eject</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unmounted</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FDFrame</name>
    <message>
        <source>eject</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FormateDialog</name>
    <message>
        <source>Formatted successfully!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Formatting failed, please unplug the U disk and try again!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Rom size:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Filesystem:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disk name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Completely erase(Time is longer, please confirm!)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Format disk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Formatting this volume will erase all data on it. Please back up all retained data before formatting. Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Disk test</source>
        <translation type="obsolete">U盘检测</translation>
    </message>
    <message>
        <source>Disk format</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>usb management tool</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ukui-flash-disk</source>
        <translation type="vanished">U盘管理工具</translation>
    </message>
    <message>
        <source>kindly reminder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>wrong reminder</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please do not pull out the USB flash disk when reading or writing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please do not pull out the CDROM when reading or writing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please do not pull out the SD Card when reading or writing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>There is a problem with this device</source>
        <translation type="obsolete">此设备存在问题</translation>
    </message>
    <message>
        <source>telephone device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Removable storage device removed</source>
        <translation type="vanished">移动存储设备已移除</translation>
    </message>
    <message>
        <source>Please do not pull out the storage device when reading or writing</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Storage device removed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MainWindow</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>ukui flash disk</source>
        <translation type="vanished">U盘管理工具</translation>
    </message>
    <message>
        <source>kylin device daemon</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <source>OK</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QClickWidget</name>
    <message>
        <source>the capacity is empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>blank CD</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>other user device</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>another device</source>
        <translation type="obsolete">其它设备</translation>
    </message>
    <message>
        <source>Unmounted</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>弹出</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>RepairDialogBox</name>
    <message>
        <source>Disk test</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;h4&gt;The system could not recognize the disk contents&lt;/h4&gt;&lt;p&gt;Check that the disk and drive are properly connected, make sure the disk is not a read-only disk, and try again. For more information, search for help on read-only files and how to change read-only files.&lt;/p&gt;</source>
        <translation type="vanished">&lt;h4&gt;系统无法识别U盘内容&lt;/h4&gt;&lt;p&gt;检查磁盘和驱动器是否正确连接，确保磁盘不是只读磁盘，然后重试。有关更多信息，请搜索有关只读文件和如何更改只读文件的帮助。&lt;/p&gt;</translation>
    </message>
    <message>
        <source>Format disk</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Repair</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;h4&gt;The system could not recognize the disk contents&lt;/h4&gt;&lt;p&gt;Check that the disk/drive is properly connected,make sure the disk is not a read-only disk, and try again.For more information, search for help on read-only files andhow to change read-only files.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>&lt;h4&gt;The system could not recognize the disk contents&lt;/h4&gt;&lt;p&gt;Check that the disk/drive &apos;%1&apos; is properly connected,make sure the disk is not a read-only disk, and try again.For more information, search for help on read-only files andhow to change read-only files.&lt;/p&gt;</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RepairProgressBar</name>
    <message>
        <source>&lt;h3&gt;%1&lt;/h3&gt;</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Attempting a disk repair...</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Repair successfully!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>The repair completed. If the USB flash disk is not mounted, please try formatting the device!</source>
        <translation type="vanished">修复失败，如果设备没有成功挂载，请尝试格式化修复！</translation>
    </message>
    <message>
        <source>Disk test</source>
        <translation type="obsolete">U盘检测</translation>
    </message>
    <message>
        <source>Disk repair</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Repair failed. If the USB flash disk is not mounted, please try formatting the device!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ejectInterface</name>
    <message>
        <source>usb has been unplugged safely</source>
        <translation type="vanished">U盘已安全拔出</translation>
    </message>
    <message>
        <source>cdrom has been unplugged safely</source>
        <translation type="vanished">光盘已安全拔出</translation>
    </message>
    <message>
        <source>sd has been unplugged safely</source>
        <translation type="vanished">SD卡已安全拔出</translation>
    </message>
    <message>
        <source>usb is occupying unejectable</source>
        <translation type="vanished">U盘占用无法弹出</translation>
    </message>
    <message>
        <source>Storage device can be safely unplugged</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>gpartedInterface</name>
    <message>
        <source>ok</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>gparted has started,can not eject</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>interactiveDialog</name>
    <message>
        <source>usb is occupying,do you want to eject it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>cdrom is occupying,do you want to eject it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sd is occupying,do you want to eject it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>cancle</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>yes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>cdrom is occupying</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>sd is occupying</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>usb is occupying</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
