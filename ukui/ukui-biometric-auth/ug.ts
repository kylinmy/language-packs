<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>BioAuthWidget</name>
    <message>
        <source>Retry</source>
        <translation type="obsolete">重试</translation>
    </message>
    <message>
        <source>%1 too many unsuccessful attempts,please enter password.</source>
        <translation type="obsolete">%1验证失败达最大次数，请使用密码登录</translation>
    </message>
    <message>
        <source>%1 authentication failure,there are still %2 remaining opportunities</source>
        <translation type="obsolete">%1认证失败，还剩%2次尝试机会</translation>
    </message>
    <message>
        <source>Please use wechat to scan the code</source>
        <translation type="vanished">请使用微信扫码</translation>
    </message>
</context>
<context>
    <name>BioDevices</name>
    <message>
        <source>FingerPrint</source>
        <translation type="obsolete">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="obsolete">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="obsolete">虹膜</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="obsolete">人脸</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="obsolete">声纹</translation>
    </message>
    <message>
        <source>Wechat</source>
        <translation type="vanished">微信</translation>
    </message>
    <message>
        <source>QRCode</source>
        <translation type="vanished">二维码</translation>
    </message>
</context>
<context>
    <name>LoginOptionsWidget</name>
    <message>
        <source>Login Options</source>
        <translation type="vanished">登录选项</translation>
    </message>
    <message>
        <source>Wechat</source>
        <translation type="vanished">微信</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../src/mainwindow.cpp" line="65"/>
        <source>Authentication</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fingerprint authentication failed, you still have %1 verification opportunities</source>
        <translation type="vanished">指纹验证失败，您还有%1次尝试机会</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="26"/>
        <source>Form</source>
        <translation></translation>
    </message>
    <message>
        <source>More</source>
        <translation type="obsolete">更多</translation>
    </message>
    <message>
        <source>Restart</source>
        <translation type="obsolete">重新开始</translation>
    </message>
    <message>
        <source>Password</source>
        <translation type="vanished">密码</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="302"/>
        <location filename="../src/mainwindow.cpp" line="882"/>
        <location filename="../src/mainwindow.cpp" line="948"/>
        <source>Biometric</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="373"/>
        <source>use password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>DeviceType:</source>
        <translation type="obsolete">设备类型：</translation>
    </message>
    <message>
        <source>Back</source>
        <translation type="obsolete">返回</translation>
    </message>
    <message>
        <source>Details</source>
        <translation type="vanished">详细</translation>
    </message>
    <message>
        <source>Action Id:</source>
        <translation type="obsolete">动作:</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation type="vanished">描述：</translation>
    </message>
    <message>
        <source>Polkit.subject-pid:</source>
        <translation type="vanished">Polkit.subject-pid：</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation type="obsolete">重试</translation>
    </message>
    <message>
        <source>Device types:</source>
        <translation type="obsolete">设备类型：</translation>
    </message>
    <message>
        <source>Vendor:</source>
        <translation type="vanished">发行商：</translation>
    </message>
    <message>
        <source>Action:</source>
        <translation type="vanished">动作：</translation>
    </message>
    <message>
        <source>Polkit.caller-pid:</source>
        <translation type="vanished">Polkit.caller-pid：</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="334"/>
        <location filename="../src/mainwindow.cpp" line="884"/>
        <source>Cancel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.ui" line="353"/>
        <location filename="../src/mainwindow.cpp" line="880"/>
        <source>Authenticate</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="886"/>
        <location filename="../src/mainwindow.cpp" line="991"/>
        <source>Use password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Auth</source>
        <translation type="obsolete">授权</translation>
    </message>
    <message>
        <source>Too many unsuccessful attempts,please enter password.</source>
        <translation type="vanished">指纹验证失败达最大次数，请使用密码解锁</translation>
    </message>
    <message>
        <source>%1 authentication failure,there are still %2 remaining opportunities</source>
        <translation type="vanished">%1认证失败，还剩%2次尝试机会</translation>
    </message>
    <message>
        <source>%1 too many unsuccessful attempts,please enter password.</source>
        <translation type="vanished">%1验证失败达最大次数，请使用密码登录</translation>
    </message>
    <message>
        <source>in authentication, please wait...</source>
        <translation type="vanished">认证中，请稍等...</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1026"/>
        <location filename="../src/mainwindow.cpp" line="1093"/>
        <location filename="../src/mainwindow.cpp" line="1094"/>
        <source>Please try again in %1 minutes.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1036"/>
        <location filename="../src/mainwindow.cpp" line="1103"/>
        <location filename="../src/mainwindow.cpp" line="1104"/>
        <source>Please try again in %1 seconds.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1045"/>
        <location filename="../src/mainwindow.cpp" line="1046"/>
        <location filename="../src/mainwindow.cpp" line="1112"/>
        <location filename="../src/mainwindow.cpp" line="1113"/>
        <source>Account locked permanently.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="675"/>
        <location filename="../src/mainwindow.cpp" line="676"/>
        <source>Password cannot be empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Failed to verify %1, please enter password.</source>
        <translation type="vanished">验证%1失败，请输入密码.</translation>
    </message>
    <message>
        <source>Unable to verify %1, please enter password.</source>
        <translation type="vanished">无法验证%1，请输入密码.</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="175"/>
        <source>Failed to verify %1, you still have %2 verification opportunities</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>An application is attempting to perform an action that requires privileges. Authentication is required to perform this action.</source>
        <translation type="vanished">一个程序正试图执行一个需要特权的动作。要求授权以执行该动作。</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="578"/>
        <source>Password: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="261"/>
        <source>Please enter your password or enroll your fingerprint </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="187"/>
        <source>Abnormal network</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="511"/>
        <source>This operation requires the administrator&apos;s authorization. Please enter your password to allow this operation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="580"/>
        <source>_Password: </source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="582"/>
        <source>_Password:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="691"/>
        <source>Authentication failed, please try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="623"/>
        <source>days left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Biometric/code scan authentication failed too many times, please enter the password.</source>
        <translation type="vanished">生物/扫码验证失败达最大次数，请使用密码解锁.</translation>
    </message>
    <message>
        <source>Bioauth/code scan authentication failed, you still have %1 verification opportunities</source>
        <translation type="vanished">生物/扫码验证失败，您还有%1次尝试机会</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="164"/>
        <location filename="../src/mainwindow.cpp" line="1176"/>
        <source>Failed to verify %1, please enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="168"/>
        <location filename="../src/mainwindow.cpp" line="1178"/>
        <source>Unable to verify %1, please enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>NET Exception</source>
        <translation type="vanished">网络异常</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="517"/>
        <source>A program is attempting to perform an action that requires privileges.It requires authorization to perform the action.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="576"/>
        <source>Input Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="627"/>
        <source>hours left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="631"/>
        <source>minutes left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="635"/>
        <source>seconds left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1193"/>
        <source>Verify face recognition or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1198"/>
        <source>Press fingerprint or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1203"/>
        <source>Verify voiceprint or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1208"/>
        <source>Verify finger vein or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1213"/>
        <source>Verify iris or enter password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="1218"/>
        <source>Use the bound wechat scanning code or enter the password to unlock</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Use the bound wechat scanning code or enter the password to log in</source>
        <translation type="vanished">使用绑定的微信扫码或输入密码登录</translation>
    </message>
    <message>
        <location filename="../src/mainwindow.cpp" line="623"/>
        <location filename="../src/mainwindow.cpp" line="627"/>
        <location filename="../src/mainwindow.cpp" line="631"/>
        <location filename="../src/mainwindow.cpp" line="635"/>
        <source>Account locked,</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Authentication failed, please try again</source>
        <translation type="obsolete">认证失败，请重试</translation>
    </message>
</context>
<context>
    <name>PolkitListener</name>
    <message>
        <location filename="../src/PolkitListener.cpp" line="88"/>
        <source>Another client is already authenticating, please try again later.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PolkitListener.cpp" line="257"/>
        <source>Authentication failure, please try again.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../src/PolkitListener.cpp" line="264"/>
        <source>Password input error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account locked %1 minutes due to %2 fail attempts</source>
        <translation type="vanished">账户锁定%1分钟由于%2次错误尝试</translation>
    </message>
    <message>
        <source>Authentication failure,there are still %1 remaining opportunities</source>
        <translation type="vanished">认证失败，还剩余%1次尝试机会</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>FingerPrint</source>
        <translation type="obsolete">指纹</translation>
    </message>
    <message>
        <source>FingerVein</source>
        <translation type="obsolete">指静脉</translation>
    </message>
    <message>
        <source>Iris</source>
        <translation type="obsolete">虹膜</translation>
    </message>
    <message>
        <source>Face</source>
        <translation type="obsolete">人脸</translation>
    </message>
    <message>
        <source>VoicePrint</source>
        <translation type="obsolete">声纹</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">取消</translation>
    </message>
</context>
</TS>
