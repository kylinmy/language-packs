<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>Touchpad</name>
    <message>
        <location filename="../touchpad-settings.cpp" line="37"/>
        <source>Touchpad</source>
        <translation>ᠬᠦᠷᠦᠯᠴᠡᠬᠦ᠌ ᠡᠵᠡᠮᠱᠢᠯ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
</context>
<context>
    <name>TouchpadUI</name>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="63"/>
        <source>Touchpad</source>
        <translation type="unfinished">ᠬᠦᠷᠦᠯᠴᠡᠬᠦ᠌ ᠡᠵᠡᠮᠱᠢᠯ ᠬᠠᠪᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="82"/>
        <source>Disable touchpad when using the mouse</source>
        <translation>ᠮᠠᠦ᠋ᠰ ᠬᠠᠪᠴᠢᠭᠤᠯᠬᠤ ᠦᠶ᠎ᠡ ᠎ᠳᠦ ᠬᠦᠷᠦᠯᠴᠡᠬᠦᠢ ᠡᠵᠡᠮᠱᠢᠯ ᠬᠠᠪᠳᠠᠰᠤ ᠬᠡᠷᠡᠭ᠌ᠯᠡᠬᠦ᠌ ᠎ᠶ᠋ᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="100"/>
        <source>Pointer Speed</source>
        <translation>ᠵᠢᠭᠠᠯᠳᠠ ᠎ᠶ᠋ᠢᠨ ᠬᠤᠷᠳᠤᠴᠠ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="102"/>
        <source>Slow</source>
        <translation>ᠤᠳᠠᠭᠠᠨ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="103"/>
        <source>Fast</source>
        <translation>ᠬᠤᠷᠳᠤᠨ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="127"/>
        <source>Disable touchpad when typing</source>
        <translation>ᠦᠰᠦᠭ ᠱᠢᠪᠡᠬᠦ᠌ ᠦᠶ᠎ᠡ ᠎ᠳᠦ ᠬᠦᠷᠦᠯᠴᠡᠬᠦᠢ ᠡᠵᠡᠮᠱᠢᠯ ᠬᠠᠪᠳᠠᠰᠤ ᠬᠡᠷᠡᠭ᠌ᠯᠡᠬᠦ᠌ ᠎ᠶ᠋ᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="146"/>
        <source>Touch and click on the touchpad</source>
        <translation>ᠬᠦᠷᠦᠯᠴᠡᠬᠦᠢ ᠡᠵᠡᠮᠱᠢᠯ ᠬᠠᠪᠳᠠᠰᠤ ᠎ᠶ᠋ᠢ ᠬᠦᠩᠬᠡᠨ ᠳᠤᠪᠴᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="165"/>
        <source>Scroll bar slides with finger</source>
        <translation>ᠦᠩᠬᠦᠷᠢᠬᠦᠯ ᠬᠤᠷᠤᠭᠤ ᠎ᠶ᠋ᠢ ᠳᠠᠭᠠᠵᠤ ᠭᠤᠯᠭᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="183"/>
        <source>Scrolling area</source>
        <translation>ᠦᠩᠬᠦᠷᠢᠬᠦᠯ ᠎ᠦᠨ ᠬᠦᠷᠢᠶ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="186"/>
        <source>Two-finger scrolling in the middle area</source>
        <translation>ᠳᠤᠮᠳᠠᠬᠢ ᠪᠦᠰᠡ ᠎ᠳᠦ ᠦᠩᠬᠦᠷᠢᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="187"/>
        <source>Edge scrolling</source>
        <translation>ᠵᠠᠭᠠᠭ ᠎ᠢᠶᠠᠷ ᠦᠩᠬᠦᠷᠢᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="188"/>
        <source>Disable scrolling</source>
        <translation>ᠦᠩᠬᠦᠷᠢᠬᠦᠯ ᠎ᠢ ᠴᠠᠭᠠᠵᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="202"/>
        <source>gesture</source>
        <translation>ᠭᠠᠷ ᠎ᠤᠨ ᠲᠤᠬᠢᠶ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="233"/>
        <source>three fingers click</source>
        <translation>ᠭᠤᠷᠪᠠᠨ ᠬᠤᠷᠤᠭᠤ ᠎ᠪᠠᠷ ᠬᠦᠩᠭᠡᠨ ᠳᠤᠪᠴᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="234"/>
        <source>show global search</source>
        <translation type="unfinished">ᠪᠦᠷᠢᠨ ᠬᠠᠢᠯᠲᠠ ᠎ᠶ᠋ᠢ ᠳᠠᠭᠤᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="239"/>
        <source>swipe three fingers down</source>
        <translation>ᠭᠤᠷᠪᠠᠨ ᠬᠤᠷᠤᠭᠤ ᠎ᠪᠠᠷ ᠳᠤᠷᠤᠭ᠍ᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="240"/>
        <source>show desktop</source>
        <translation type="unfinished">ᠱᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠎ᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="245"/>
        <source>swipe three fingers up</source>
        <translation>ᠭᠤᠷᠪᠠᠨ ᠬᠤᠷᠤᠭᠤ ᠎ᠪᠠᠷ ᠳᠡᠭᠡᠭ᠌ᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="246"/>
        <source>open the multitasking view</source>
        <translation type="unfinished">ᠤᠯᠠᠨ ᠡᠬᠦᠷᠬᠡᠳᠦ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ ᠎ᠢ ᠨᠡᠬᠡᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="251"/>
        <source>swipe three fingers horizontally</source>
        <translation>ᠭᠤᠷᠪᠠᠨ ᠬᠤᠷᠤᠭᠤ ᠪᠡᠷ᠎ᠡ ᠵᠡᠬᠦᠨᠰᠢ ᠪᠠᠷᠠᠭᠤᠨᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="252"/>
        <source>switch windows</source>
        <translation type="unfinished">ᠨᠡᠬᠡᠬᠡᠭ᠌ᠰᠡᠨ ᠴᠤᠩᠬᠤᠨ ᠎ᠤ ᠬᠤᠭᠤᠷᠤᠨᠳᠤ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="257"/>
        <source>four fingers click</source>
        <translation>ᠳᠦᠷᠪᠡᠨ ᠬᠤᠷᠤᠭᠤ ᠎ᠪᠠᠷ ᠳᠤᠪᠴᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="258"/>
        <source>show sidebar</source>
        <translation type="unfinished">ᠬᠠᠵᠠᠭᠤ ᠪᠠᠭᠠᠷ ᠎ᠢ ᠳᠠᠭᠤᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="265"/>
        <source>swipe four fingers horizontally</source>
        <translation>ᠳᠦᠷᠪᠡᠨ ᠬᠤᠷᠤᠭᠤ ᠎ᠪᠠᠷ ᠵᠡᠬᠦᠨᠰᠢ ᠪᠠᠷᠠᠭᠤᠨᠰᠢ ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="266"/>
        <source>switch desktop</source>
        <translation>ᠱᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ ᠎ᠤᠨ ᠬᠤᠭᠤᠷᠤᠨᠳᠤ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../touchpadsettingsui.cpp" line="294"/>
        <source>more gesture</source>
        <translation>ᠨᠡᠩ ᠤᠯᠠᠨ ᠭᠠᠷ ᠎ᠤᠨ ᠲᠤᠬᠢᠶ᠎ᠠ</translation>
    </message>
</context>
</TS>
