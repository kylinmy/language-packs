<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>BasePopupTitle</name>
    <message>
        <source>Font Viewer</source>
        <translation>ཡིག་གཟུགས་དོ་དམ་ཆས།</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>BasePreviewArea</name>
    <message>
        <source>Enter Text Content For Preview</source>
        <translation>ཡི་གེའི་ནང་དོན་ནང་འཇུག་བྱས་ནས་སྔོན་ལྟ་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>BaseSearchEdit</name>
    <message>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
</context>
<context>
    <name>FontListView</name>
    <message>
        <source>Add Font</source>
        <translation>ཡིག་གཟུགས་ཁ་སྣོན་བྱེད་པ</translation>
    </message>
    <message>
        <source>Apply Font</source>
        <translation>ཡིག་གཟུགས་བཀོལ་སྤྱོད་བྱེད་པ</translation>
    </message>
    <message>
        <source>Remove Font</source>
        <translation>ཡིག་གཟུགས་མེད་པར་བཟོ་བ</translation>
    </message>
    <message>
        <source>Export Font</source>
        <translation>ཡིག་གཟུགས་ཕྱིར་འདྲེན།</translation>
    </message>
    <message>
        <source>Cancel Collection</source>
        <translation>བསྡུ་ལེན་མེད་པར་བཟོ</translation>
    </message>
    <message>
        <source>Check Font</source>
        <translation>ཡིག་གཟུགས་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <source>Collection</source>
        <translation>ཡིག་གཟུགས་བསྡུ་ཉར།</translation>
    </message>
    <message>
        <source>Add Fonts</source>
        <translation>ཡིག་གཟུགས་ཁ་སྣོན་བྱེད་པ།</translation>
    </message>
    <message>
        <source>font(*.ttf *.fon *.ttc *.afm)</source>
        <translation>ཡིག་གཟུགས་ཡིག་ཆ།(*.ttf *.fon *ttc *.afm)</translation>
    </message>
    <message>
        <source>Export Fonts</source>
        <translation>ཡིག་གཟུགས་ཕྱིར་གཏོང་།</translation>
    </message>
    <message>
        <source>Build the core strength of Chinese operating system</source>
        <translation>ཀྲུང་གོའི་བཀོལ་སྤྱོད་མ་ལག་གི་ལྟེ་བའི་སྟོབས་ཤུགས་འཛུགས་སྐྱོང་བྱ</translation>
    </message>
</context>
<context>
    <name>FunctionWid</name>
    <message>
        <source>All Font</source>
        <translation>ཡིག་གཟུགས་ཡོད་ཚད།</translation>
    </message>
    <message>
        <source>System Font</source>
        <translation>མ་ལག་གི་ཡིག་གཟུགས</translation>
    </message>
    <message>
        <source>User Font</source>
        <translation>སྤྱོད་མཁན་གྱི་ཡིག་གཟུགས</translation>
    </message>
    <message>
        <source>Collection Font</source>
        <translation>འཚོལ་སྡུད་བྱས་པའི་ཡིག་གཟུགས།</translation>
    </message>
</context>
<context>
    <name>MainView</name>
    <message>
        <source>Font Viewer</source>
        <translation>ཡིག་གཟུགས་དོ་དམ་ཆས།</translation>
    </message>
</context>
<context>
    <name>PopupAbout</name>
    <message>
        <source>Version: </source>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ། </translation>
    </message>
    <message>
        <source>Font Viewer is a tool to help users install and organize management; After installation, the font can be applied to self-developed applications, third-party pre installed applications and user self installed applications.</source>
        <translation>ཡིག་གཟུགས་དོ་དམ་ཆས་ནི་སྤྱོད་མཁན་གྱིས་དོ་དམ་སྒྲིག་སྦྱོར་དང་སྒྲིག་འཛུགས་བྱེད་པར་རོགས་རམ་བྱེད་པའི་ལག་ཆ་ཞིག་ཡིན། སྒྲིག་སྦྱོར་བྱས་རྗེས་ཡིག་གཟུགས་དེ་རང་གིས་གསར་སྤེལ་བྱས་པའི་ཉེར་སྤྱོད་གོ་རིམ་དང་། ཕྱོགས་གསུམ་པའི་སྔོན་ལ་སྒྲིག་སྦྱོར་བྱས་པའི་ཉེར་སྤྱོད་གོ་རིམ། སྤྱོད་མཁན་རང་གིས་རང་ལ་སྒྲིག་སྦྱོར་བྱས་པའི་ཉེར་སྤྱོད་གོ་རིམ་བཅས་སུ་སྤྱད་ཆོག</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར། </translation>
    </message>
    <message>
        <source>Font Viewer</source>
        <translation>ཡིག་གཟུགས་དོ་དམ་ཆས།</translation>
    </message>
</context>
<context>
    <name>PopupFontInfo</name>
    <message>
        <source>FontName</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <source>FontSeries</source>
        <translation>རིགས་གྲས།</translation>
    </message>
    <message>
        <source>FontStyle</source>
        <translation>མ་དཔེ།</translation>
    </message>
    <message>
        <source>FontType</source>
        <translation>རིགས་དབྱེ།</translation>
    </message>
    <message>
        <source>FontVersion</source>
        <translation>པར་གཞི།</translation>
    </message>
    <message>
        <source>FontPath</source>
        <translation>གནས་ས།</translation>
    </message>
    <message>
        <source>FontCopyright</source>
        <translation>པར་དབང་།</translation>
    </message>
    <message>
        <source>FontTrademark</source>
        <translation>ཉོ་ཚོང་མཚོན་རྟགས།</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="vanished">继续</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>PopupInstallFail</name>
    <message>
        <source>There is a problem with the font file. Installation failed!</source>
        <translation>ཡིག་གཟུགས་ཡིག་ཆ་ལ་གནད་དོན་ཡོད། སྒྲིག་སྦྱོར་བྱས་ཀྱང་ཕམ་སོང་།</translation>
    </message>
    <message>
        <source>confirm</source>
        <translation>ངོས་འཛིན་བྱས་པ།</translation>
    </message>
    <message>
        <source>Continue</source>
        <translation type="vanished">继续</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
</context>
<context>
    <name>PopupInstallSuccess</name>
    <message>
        <source>already installed </source>
        <translation>སྒྲིག་སྦྱོར་བྱས་ཟིན་པ། </translation>
    </message>
    <message>
        <source> fonts!</source>
        <translation> ཡིག་གཟུགས་རིགས་ཤིག</translation>
    </message>
    <message>
        <source> fonts!  </source>
        <translation type="vanished">款字体！</translation>
    </message>
</context>
<context>
    <name>PopupRemove</name>
    <message>
        <source>Are you sure you want to remove this font?</source>
        <translation>ཁྱོད་ཀྱིས་དངོས་གནས་ཡིག་གཟུགས་འདི་མེད་པར་བཟོ་དགོས་སམ།</translation>
    </message>
    <message>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Add Font</source>
        <translation>ཡིག་གཟུགས་ཁ་སྣོན་བྱེད་པ</translation>
    </message>
    <message>
        <source>Minimize</source>
        <translation>ཆེས་ཆུང་དུ་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Menu</source>
        <translation>འདེམས་པང་།</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <source>Maximize</source>
        <translation>ཚད་གཞི་མཐོ་ཤོས་ཀྱི་སྒོ་ནས</translation>
    </message>
    <message>
        <source>Normal</source>
        <translation>སླར་གསོ།</translation>
    </message>
</context>
</TS>
