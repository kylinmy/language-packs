<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>kdk::KAboutDialog</name>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="164"/>
        <location filename="../src/kaboutdialog.cpp" line="176"/>
        <source>Service &amp; Support: </source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠪᠠ ᠳᠡᠮᠵᠢᠬᠦ ᠪᠦᠯᠬᠦᠮ：</translation>
    </message>
</context>
<context>
    <name>kdk::KInputDialog</name>
    <message>
        <location filename="../src/kinputdialog.cpp" line="206"/>
        <source>Enter a value:</source>
        <translation>ᠲᠤᠭ᠎ᠠ ᠬᠡᠮᠵᠢᠭᠳᠡᠯ ᠢ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ：</translation>
    </message>
</context>
<context>
    <name>kdk::KInputDialogPrivate</name>
    <message>
        <location filename="../src/kinputdialog.cpp" line="212"/>
        <source>ok</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/kinputdialog.cpp" line="214"/>
        <source>cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>kdk::KMenuButton</name>
    <message>
        <location filename="../src/kmenubutton.cpp" line="39"/>
        <source>more</source>
        <translation>ᠨᠡᠩ ᠤᠯᠠᠨ</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="40"/>
        <source>Setting</source>
        <translation>ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="41"/>
        <source>Theme</source>
        <translation>ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="42"/>
        <source>Assist</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="43"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="44"/>
        <source>Quit</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="51"/>
        <source>Auto</source>
        <translation>ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ ᠢ ᠳᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="53"/>
        <source>Light</source>
        <translation>ᠬᠦᠶᠦᠬᠡᠨ ᠦᠩᠭᠡ ᠲᠠᠢ ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="55"/>
        <source>Dark</source>
        <translation>ᠬᠦᠨ ᠦᠩᠭᠡ ᠲᠠᠢ ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ</translation>
    </message>
</context>
<context>
    <name>kdk::KProgressDialog</name>
    <message>
        <location filename="../src/kprogressdialog.cpp" line="47"/>
        <source>cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>kdk::KSearchLineEditPrivate</name>
    <message>
        <location filename="../src/ksearchlineedit.cpp" line="285"/>
        <source>search</source>
        <translation>ᠬᠠᠢᠯᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/ksearchlineedit.cpp" line="374"/>
        <source>Search</source>
        <translation>ᠬᠠᠢᠯᠳᠠ</translation>
    </message>
</context>
<context>
    <name>kdk::KSecurityLevelBar</name>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="38"/>
        <source>Low</source>
        <translation>ᠳᠤᠤᠷ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="41"/>
        <source>Medium</source>
        <translation>ᠳᠤᠮᠳᠠ</translation>
    </message>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="44"/>
        <source>High</source>
        <translation>ᠦᠨᠳᠦᠷ</translation>
    </message>
</context>
<context>
    <name>kdk::KSecurityLevelBarPrivate</name>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="112"/>
        <source>Low</source>
        <translation>ᠳᠤᠤᠷ᠎ᠠ</translation>
    </message>
</context>
<context>
    <name>kdk::KUninstallDialog</name>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="102"/>
        <location filename="../src/kuninstalldialog.cpp" line="194"/>
        <source>uninstall</source>
        <translation>ᠲᠡᠷᠡ ᠳᠠᠷᠤᠢ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="163"/>
        <source>deb name:</source>
        <translation>ᠪᠠᠭᠳᠠᠬᠤ ᠨᠡᠷ᠎ᠡ ：</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="168"/>
        <source>deb version:</source>
        <translation>ᠬᠡᠪᠯᠡᠯ：</translation>
    </message>
</context>
<context>
    <name>kdk::KUninstallDialogPrivate</name>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="284"/>
        <source>deb name:</source>
        <translation>ᠪᠠᠭᠳᠠᠬᠤ ᠨᠡᠷ᠎ᠡ：</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="288"/>
        <source>deb version:</source>
        <translation>ᠬᠡᠪᠯᠡᠯ：</translation>
    </message>
</context>
<context>
    <name>kdk::KWindowButtonBarPrivate</name>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="162"/>
        <source>minimize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="168"/>
        <source>maximize</source>
        <translation>ᠬᠠᠮᠤᠭ ᠤᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="173"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
</TS>
