<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>QObject</name>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Ok</source>
        <translation type="vanished">确定</translation>
    </message>
</context>
<context>
    <name>TestWidget</name>
    <message>
        <source>doing</source>
        <translatorcomment>正在运行...</translatorcomment>
        <translation type="vanished">正在运行...</translation>
    </message>
    <message>
        <source>cancel</source>
        <translatorcomment>取消</translatorcomment>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Version：2020.1.0</source>
        <translation type="vanished">版本：2020.1.0</translation>
    </message>
</context>
<context>
    <name>kdk::KAboutDialog</name>
    <message>
        <location filename="../src/kaboutdialog.cpp" line="159"/>
        <location filename="../src/kaboutdialog.cpp" line="171"/>
        <source>Service &amp; Support: </source>
        <translatorcomment>服务与支持团队：</translatorcomment>
        <translation>服务与支持团队：</translation>
    </message>
</context>
<context>
    <name>kdk::KInputDialog</name>
    <message>
        <location filename="../src/kinputdialog.cpp" line="206"/>
        <source>Enter a value:</source>
        <translation>请输入数值：</translation>
    </message>
</context>
<context>
    <name>kdk::KInputDialogPrivate</name>
    <message>
        <location filename="../src/kinputdialog.cpp" line="212"/>
        <source>ok</source>
        <translation>确认</translation>
    </message>
    <message>
        <location filename="../src/kinputdialog.cpp" line="214"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>kdk::KMenuButton</name>
    <message>
        <location filename="../src/kmenubutton.cpp" line="39"/>
        <source>more</source>
        <translation>更多</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="40"/>
        <source>Setting</source>
        <translatorcomment>设置</translatorcomment>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="41"/>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="42"/>
        <source>Assist</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="43"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="44"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="51"/>
        <source>Auto</source>
        <translation>跟随主题</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="53"/>
        <source>Light</source>
        <translation>浅色主题</translation>
    </message>
    <message>
        <location filename="../src/kmenubutton.cpp" line="55"/>
        <source>Dark</source>
        <translation>深色主题</translation>
    </message>
</context>
<context>
    <name>kdk::KProgressDialog</name>
    <message>
        <location filename="../src/kprogressdialog.cpp" line="47"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>kdk::KSearchLineEdit</name>
    <message>
        <source>Please input searching content</source>
        <translatorcomment>请输入搜索内容</translatorcomment>
        <translation type="vanished">请输入搜索内容</translation>
    </message>
    <message>
        <source>Searching</source>
        <translatorcomment>搜索</translatorcomment>
        <translation type="vanished">搜索</translation>
    </message>
    <message>
        <source>Search</source>
        <translation type="vanished">搜索</translation>
    </message>
</context>
<context>
    <name>kdk::KSearchLineEditPrivate</name>
    <message>
        <location filename="../src/ksearchlineedit.cpp" line="206"/>
        <source>search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../src/ksearchlineedit.cpp" line="279"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
</context>
<context>
    <name>kdk::KSecurityLevelBar</name>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="38"/>
        <source>Low</source>
        <translation>低</translation>
    </message>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="41"/>
        <source>Medium</source>
        <translation>中</translation>
    </message>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="44"/>
        <source>High</source>
        <translation>高</translation>
    </message>
</context>
<context>
    <name>kdk::KSecurityLevelBarPrivate</name>
    <message>
        <location filename="../src/ksecuritylevelbar.cpp" line="110"/>
        <source>Low</source>
        <translation>低</translation>
    </message>
</context>
<context>
    <name>kdk::KTitleBar</name>
    <message>
        <source>minimize</source>
        <translation type="vanished">最小化</translation>
    </message>
    <message>
        <source>maximize</source>
        <translation type="vanished">最大化</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="vanished">关闭</translation>
    </message>
</context>
<context>
    <name>kdk::KUninstallDialog</name>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="102"/>
        <location filename="../src/kuninstalldialog.cpp" line="194"/>
        <source>uninstall</source>
        <translatorcomment>立即卸载</translatorcomment>
        <translation>立即卸载</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="163"/>
        <source>deb name:</source>
        <translation>包名：</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="168"/>
        <source>deb version:</source>
        <translation>版本：</translation>
    </message>
</context>
<context>
    <name>kdk::KUninstallDialogPrivate</name>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="284"/>
        <source>deb name:</source>
        <translation>包名：</translation>
    </message>
    <message>
        <location filename="../src/kuninstalldialog.cpp" line="288"/>
        <source>deb version:</source>
        <translation>版本：</translation>
    </message>
</context>
<context>
    <name>kdk::KWindowButtonBar</name>
    <message>
        <source>minimize</source>
        <translatorcomment>最小化</translatorcomment>
        <translation type="vanished">最小化</translation>
    </message>
    <message>
        <source>maximize</source>
        <translation type="vanished">最大化</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="vanished">关闭</translation>
    </message>
</context>
<context>
    <name>kdk::KWindowButtonBarPrivate</name>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="112"/>
        <source>minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="118"/>
        <source>maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <location filename="../src/kwindowbuttonbar.cpp" line="123"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
</context>
</TS>
