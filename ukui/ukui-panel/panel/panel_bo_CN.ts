<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>UKUIPanel</name>
    <message>
        <location filename="../ukuipanel.cpp" line="837"/>
        <source>Small</source>
        <translation>ཡིག་གཟུགས་ཆུང་བ།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="839"/>
        <source>Medium</source>
        <translation>ཆེ་ཆུང་འབྲིང་རིམ།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="841"/>
        <source>Large</source>
        <translation>ཆེ་བ།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="846"/>
        <source>Adjustment Size</source>
        <translation>ཆེ་ཆུང་སྙོམས་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="882"/>
        <source>Up</source>
        <translation>སྟེང་།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="884"/>
        <source>Bottom</source>
        <translation>ཞབས་ཁུལ།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="886"/>
        <source>Left</source>
        <translation>གཡོན།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="888"/>
        <source>Right</source>
        <translation>གཡས།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="891"/>
        <source>Adjustment Position</source>
        <translation>གནས་ཡུལ་ལེགས་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="917"/>
        <source>Hide Panel</source>
        <translation>ངོས་པང་སྦེད་པ།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1246"/>
        <source>Show Taskview</source>
        <translation>ལས་འགན་མཐོང་སྣེ་འཆར་བའི་གནོན་མཐེབ།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1263"/>
        <source>Show Nightmode</source>
        <translation>མཚན་ལྗོངས་རྣམ་པ་འཆར་བའི་གནོན་མཐེབ།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1269"/>
        <source>Show Desktop</source>
        <translation>ཅོག་ངོས་འཆར་བ།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1276"/>
        <source>Show System Monitor</source>
        <translation>བརྒྱུད་ཁོངས་ལྟ་ཞིབ་ཆས།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1301"/>
        <source>Lock This Panel</source>
        <translation>འགན་བྱང་ལ་ཟྭ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../ukuipanel.cpp" line="1316"/>
        <source>About Kylin</source>
        <translation>དགུ་ཚིགས་ཆིག་ལིན་གྱི་སྐོར།</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="79"/>
        <source>Use alternate configuration file.</source>
        <translation>སྡེབ་སྒྲིག་ཡིག་ཆ་བཀོལ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="80"/>
        <source>Configuration file</source>
        <translation>སྡེབ་སྒྲིག་ཡིག་ཆ་བཀོལ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="85"/>
        <source>ukui-panel set mode </source>
        <translation>ལས་འགན་རེའུ་མིག་སྒྲིག་བཀོད་རྣམ་པ། </translation>
    </message>
    <message>
        <location filename="../ukuipanelapplication.cpp" line="86"/>
        <source>panel set option</source>
        <translation>ངོས་པང་སྒྲིག་བཀོད་གདམ་གསེས།</translation>
    </message>
</context>
</TS>
