<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>UKUITaskBar</name>
    <message>
        <location filename="../ukuitaskbar.cpp" line="1082"/>
        <source>Drop Error</source>
        <translation>ཐིགས་པ་ཆག་པའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../ukuitaskbar.cpp" line="1083"/>
        <source>File/URL &apos;%1&apos; cannot be embedded into QuickLaunch for now</source>
        <translation>window.File/URL &apos;%1&apos;་སྟེང་དུ་མངོན་འདུག་གནས་སྐབས་སུ་QuickLaunchནང་དུ་འཇུག་མི་རུང་།</translation>
    </message>
</context>
<context>
    <name>UKUITaskButton</name>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="581"/>
        <source>Application</source>
        <translation>ཉེར་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="614"/>
        <source>To &amp;Desktop</source>
        <translation>ཅོག་ངོས་སྟེང་ལ།</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="616"/>
        <source>&amp;All Desktops</source>
        <translation>ཅོག་ཙེ་ཡོད་ཚད།</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="623"/>
        <source>Desktop &amp;%1</source>
        <translation>ཅོག་ངོས་ཀྱི་རྣམ་པ།%1</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="630"/>
        <source>&amp;To Current Desktop</source>
        <translation>མིག་སྔའི་ཅོག་ཙེའི་སྟེང་།</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="638"/>
        <source>&amp;Move</source>
        <translation>སྤོ་བ།</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="641"/>
        <source>Resi&amp;ze</source>
        <translation>ཆེ་ཆུང་སྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="648"/>
        <source>Ma&amp;ximize</source>
        <translation>ཆེས་ཆེར་བསྒྱུར།</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="654"/>
        <source>Maximize vertically</source>
        <translation>སྒེའུ་ཁུང་དྲང་འཕྱང་དུ་ཆེས་ཆེར་སྐྱེད།</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="659"/>
        <source>Maximize horizontally</source>
        <translation>སྒེའུ་ཁུང་ཆུ་སྙོམས་སུ་ཆེས་ཆེར་སྐྱེད།</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="665"/>
        <source>&amp;Restore</source>
        <translation>ཕྱིར་ལོག</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="669"/>
        <source>Mi&amp;nimize</source>
        <translation>སྒེའུ་ཁུང་ཆུང་སྒྱུར།</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="674"/>
        <source>Roll down</source>
        <translation>མར་དུ་འདེད།</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="678"/>
        <source>Roll up</source>
        <translation>ཡར་དུ་འདེད།</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="686"/>
        <source>&amp;Layer</source>
        <translation>རིས་རིམ།</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="688"/>
        <source>Always on &amp;top</source>
        <translation>ནམ་ཡང་རྩེ་མོར་གནས་པ།</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="694"/>
        <source>&amp;Normal</source>
        <translation>སོར་བཞག</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="700"/>
        <source>Always on &amp;bottom</source>
        <translation>ཐོག་མཐའ་བར་གསུམ་དུ་ཞབས་ན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="708"/>
        <source>&amp;Close</source>
        <translation>ཁ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="868"/>
        <source>delete from quicklaunch</source>
        <translation>ལས་འགན་སྡེ་ནས་གཏན་འཇགས་འདོར་བ།</translation>
    </message>
</context>
<context>
    <name>UKUITaskGroup</name>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="254"/>
        <source>Group</source>
        <translation>ཚོ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="261"/>
        <source>delete from taskbar</source>
        <translation>ལས་འགན་སྡེ་ནས་གཏན་འཇགས་འདོར་བ།</translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="263"/>
        <source>add to taskbar</source>
        <translation>ལས་འགན་སྡེ་རུ་ཁ་སྣོན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="271"/>
        <source>close</source>
        <translation>ཁ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>UKUITaskWidget</name>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="443"/>
        <source>Widget</source>
        <translation>ལྷུ་ལག་ཆུང་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="446"/>
        <source>close</source>
        <translation>ཁ་རྒྱག</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="447"/>
        <source>restore</source>
        <translation>སླར་གསོ།</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="449"/>
        <source>maximaze</source>
        <translation>ཆེ་སྒྱུར།</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="452"/>
        <source>minimize</source>
        <translation>ཆུང་སྒྱུར།</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="453"/>
        <source>above</source>
        <translation>རྩེ་མོར་བཞག</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="454"/>
        <source>clear</source>
        <translation>རྩེ་མོར་བཞག་པ་མེད་པར་བཟོ་བ།</translation>
    </message>
</context>
</TS>
