<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>UKUITaskBar</name>
    <message>
        <location filename="../ukuitaskbar.cpp" line="1082"/>
        <source>Drop Error</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠳᠠᠰᠤᠷᠬᠤ ᠠᠯᠳᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../ukuitaskbar.cpp" line="1083"/>
        <source>File/URL &apos;%1&apos; cannot be embedded into QuickLaunch for now</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UKUITaskButton</name>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="581"/>
        <source>Application</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠯᠳᠡ</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="614"/>
        <source>To &amp;Desktop</source>
        <translation>ᠱᠢᠷᠡᠬᠡᠨ ᠨᠢᠭᠤᠷ᠎ᠲᠤ ᠬᠦᠷᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="616"/>
        <source>&amp;All Desktops</source>
        <translation>ᠪᠦᠬᠦᠢᠯᠡ ᠱᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="623"/>
        <source>Desktop &amp;%1</source>
        <translation>ᠱᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ%1</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="630"/>
        <source>&amp;To Current Desktop</source>
        <translation>ᠤᠳᠤᠬᠠᠨ᠎ᠤ ᠱᠢᠷᠡᠭᠡᠨ ᠨᠢᠭᠤᠷ᠎ᠲᠤ ᠬᠦᠷᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="638"/>
        <source>&amp;Move</source>
        <translation>ᠱᠢᠯᠵᠢᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="641"/>
        <source>Resi&amp;ze</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ ᠬᠡᠮᠵᠢᠶ᠎ᠡ᠎ᠶᠢ ᠵᠤᠬᠢᠴᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="648"/>
        <source>Ma&amp;ximize</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="654"/>
        <source>Maximize vertically</source>
        <translation>ᠭᠤᠯᠳᠤ ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="659"/>
        <source>Maximize horizontally</source>
        <translation>ᠬᠦᠨᠳᠡᠯᠡᠨ ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="665"/>
        <source>&amp;Restore</source>
        <translation type="unfinished">ᠰᠡᠷᠬᠦᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="669"/>
        <source>Mi&amp;nimize</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="674"/>
        <source>Roll down</source>
        <translation>ᠳᠤᠷᠤᠭᠱᠢ</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="678"/>
        <source>Roll up</source>
        <translation>ᠳᠡᠭᠡᠭᠱᠢ</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="686"/>
        <source>&amp;Layer</source>
        <translation>ᠳᠠᠪᠬᠤᠷᠭ᠎ᠠ᠎ᠶᠢᠨ ᠲᠤᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="688"/>
        <source>Always on &amp;top</source>
        <translation>ᠬᠡᠵᠢᠶᠡᠳᠡ ᠤᠷᠤᠢ᠎ᠳᠤ ᠪᠠᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="694"/>
        <source>&amp;Normal</source>
        <translation>ᠠᠶᠠᠳᠠᠯ</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="700"/>
        <source>Always on &amp;bottom</source>
        <translation>ᠬᠡᠵᠢᠶᠡᠳᠡ ᠢᠷᠤᠭᠠᠷ᠎ᠲᠤ ᠪᠠᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="708"/>
        <source>&amp;Close</source>
        <translation type="unfinished">ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="868"/>
        <source>delete from quicklaunch</source>
        <translation>ᠡᠬᠦᠷᠭᠡ᠎ᠶᠢᠨ ᠬᠡᠷᠡᠭᠰᠡᠬᠡ᠎ᠡᠴᠡ ᠬᠡᠪᠱᠢᠮᠡᠯ᠎ᠢ ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
</context>
<context>
    <name>UKUITaskGroup</name>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="254"/>
        <source>Group</source>
        <translation>ᠪᠦᠯᠦᠭ</translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="261"/>
        <source>delete from taskbar</source>
        <translation>ᠡᠬᠦᠷᠭᠡ᠎ᠶᠢᠨ ᠬᠡᠷᠡᠭᠰᠡᠬᠡ᠎ᠡᠴᠡ ᠬᠡᠪᠱᠢᠮᠡᠯ᠎ᠢ ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="263"/>
        <source>add to taskbar</source>
        <translation>ᠡᠬᠦᠷᠭᠡ᠎ᠶᠢᠨ ᠬᠡᠷᠡᠭᠰᠡᠬᠡ᠎ᠳᠦ ᠨᠡᠮᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="271"/>
        <source>close</source>
        <translation type="unfinished">ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
</context>
<context>
    <name>UKUITaskWidget</name>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="443"/>
        <source>Widget</source>
        <translation>ᠵᠢᠵᠢᠭ ᠲᠤᠨᠤᠭᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="446"/>
        <source>close</source>
        <translation type="unfinished">ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="447"/>
        <source>restore</source>
        <translation type="unfinished">ᠰᠡᠷᠬᠦᠬᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="449"/>
        <source>maximaze</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠶᠡᠬᠡᠴᠢᠯᠡᠯ</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="452"/>
        <source>minimize</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="453"/>
        <source>above</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="454"/>
        <source>clear</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
