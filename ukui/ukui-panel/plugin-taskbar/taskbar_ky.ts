<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ky">
<context>
    <name>UKUITaskBar</name>
    <message>
        <location filename="../ukuitaskbar.cpp" line="1082"/>
        <source>Drop Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskbar.cpp" line="1083"/>
        <source>File/URL &apos;%1&apos; cannot be embedded into QuickLaunch for now</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UKUITaskButton</name>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="581"/>
        <source>Application</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="614"/>
        <source>To &amp;Desktop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="616"/>
        <source>&amp;All Desktops</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="623"/>
        <source>Desktop &amp;%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="630"/>
        <source>&amp;To Current Desktop</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="638"/>
        <source>&amp;Move</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="641"/>
        <source>Resi&amp;ze</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="648"/>
        <source>Ma&amp;ximize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="654"/>
        <source>Maximize vertically</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="659"/>
        <source>Maximize horizontally</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="665"/>
        <source>&amp;Restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="669"/>
        <source>Mi&amp;nimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="674"/>
        <source>Roll down</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="678"/>
        <source>Roll up</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="686"/>
        <source>&amp;Layer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="688"/>
        <source>Always on &amp;top</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="694"/>
        <source>&amp;Normal</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="700"/>
        <source>Always on &amp;bottom</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="708"/>
        <source>&amp;Close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskbutton.cpp" line="868"/>
        <source>delete from quicklaunch</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UKUITaskGroup</name>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="254"/>
        <source>Group</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="261"/>
        <source>delete from taskbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="263"/>
        <source>add to taskbar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskgroup.cpp" line="271"/>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>UKUITaskWidget</name>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="443"/>
        <source>Widget</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="446"/>
        <source>close</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="447"/>
        <source>restore</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="449"/>
        <source>maximaze</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="452"/>
        <source>minimize</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="453"/>
        <source>above</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ukuitaskwidget.cpp" line="454"/>
        <source>clear</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
