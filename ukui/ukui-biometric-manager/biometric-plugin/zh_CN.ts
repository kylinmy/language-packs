<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>BiometricEnrollDialog</name>
    <message>
        <location filename="../biometricenroll.ui" line="26"/>
        <location filename="../ui_biometricenroll.h" line="220"/>
        <source>Dialog</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../biometricenroll.ui" line="340"/>
        <location filename="../ui_biometricenroll.h" line="225"/>
        <source>Continue adding</source>
        <translation>继续录入</translation>
    </message>
    <message>
        <location filename="../biometricenroll.ui" line="359"/>
        <location filename="../ui_biometricenroll.h" line="226"/>
        <source>Finish</source>
        <translation>完成</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="187"/>
        <source>FingerPrint</source>
        <translation>指纹</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="189"/>
        <source>Fingervein</source>
        <translation>指静脉</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="191"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="193"/>
        <source>Face</source>
        <translation>人脸</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="195"/>
        <source>VoicePrint</source>
        <translation>声纹</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="205"/>
        <source>Enroll</source>
        <translation>录入</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="208"/>
        <source>Verify</source>
        <translation>验证</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="211"/>
        <source>Search</source>
        <translation>搜索</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="240"/>
        <source>Permission is required.
Please authenticate yourself to continue</source>
        <translation>需要授权，请先进行认证以继续操作</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="266"/>
        <location filename="../biometricenroll.cpp" line="451"/>
        <source>Enroll successfully</source>
        <translation>录入成功</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="334"/>
        <location filename="../biometricenroll.cpp" line="453"/>
        <source>Verify successfully</source>
        <translation>验证成功</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="337"/>
        <source>Not Match</source>
        <translation>不匹配</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="609"/>
        <source>Keep looking straight at the camera.</source>
        <translation>保持直视镜头</translation>
    </message>
    <message>
        <source>Place your finger on the device button and remove. Repeat</source>
        <translation type="vanished">将手指放在设备按钮上再移开，重复此步骤</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="626"/>
        <source>D-Bus calling error</source>
        <translation>D-Bus获取错误</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="635"/>
        <source>Device is busy</source>
        <translation>设备忙</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="640"/>
        <source>No such device</source>
        <translation>设备不存在</translation>
    </message>
    <message>
        <location filename="../biometricenroll.cpp" line="645"/>
        <source>Permission denied</source>
        <translation>权限不够</translation>
    </message>
</context>
<context>
    <name>BiometricMoreInfoDialog</name>
    <message>
        <location filename="../biometricmoreinfo.ui" line="26"/>
        <location filename="../ui_biometricmoreinfo.h" line="205"/>
        <source>Dialog</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="76"/>
        <location filename="../ui_biometricmoreinfo.h" line="206"/>
        <source>Biometrics </source>
        <translation>生物识别</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="166"/>
        <location filename="../ui_biometricmoreinfo.h" line="208"/>
        <source>Default device </source>
        <translation>默认设备</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="208"/>
        <location filename="../ui_biometricmoreinfo.h" line="209"/>
        <source>Verify Type:</source>
        <translation>验证类型：</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="215"/>
        <location filename="../ui_biometricmoreinfo.h" line="210"/>
        <source>Bus Type:</source>
        <translation>总线类型：</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="222"/>
        <location filename="../ui_biometricmoreinfo.h" line="211"/>
        <source>Device Status:</source>
        <translation>设备状态：</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="243"/>
        <location filename="../ui_biometricmoreinfo.h" line="214"/>
        <source>Storage Type:</source>
        <translation>存储类型：</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.ui" line="250"/>
        <location filename="../ui_biometricmoreinfo.h" line="215"/>
        <source>Identification Type:</source>
        <translation>验证类型：</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="77"/>
        <source>Connected</source>
        <translation>已连接</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="77"/>
        <source>Unconnected</source>
        <translation>未连接</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="126"/>
        <source>FingerPrint</source>
        <translation>指纹</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="128"/>
        <source>Fingervein</source>
        <translation>指静脉</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="130"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="132"/>
        <source>Face</source>
        <translation>人脸</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="134"/>
        <source>VoicePrint</source>
        <translation>声纹</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="143"/>
        <source>Hardware Verification</source>
        <translation>硬件验证</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="145"/>
        <source>Software Verification</source>
        <translation>软件验证</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="147"/>
        <source>Mix Verification</source>
        <translation>混合验证</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="149"/>
        <source>Other Verification</source>
        <translation>其他验证</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="157"/>
        <source>Device Storage</source>
        <translation>设备存储</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="159"/>
        <source>OS Storage</source>
        <translation>系统存储</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="161"/>
        <source>Mix Storage</source>
        <translation>混合存储</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="169"/>
        <source>Serial</source>
        <translation>串口</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="171"/>
        <source>USB</source>
        <translation>USB</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="173"/>
        <source>PCIE</source>
        <translation>PCIE</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="175"/>
        <source>Any</source>
        <translation>任意类型</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="177"/>
        <source>Other</source>
        <translation>其他</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="185"/>
        <source>Hardware Identification</source>
        <translation>硬件识别</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="187"/>
        <source>Software Identification</source>
        <translation>软件识别</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="189"/>
        <source>Mix Identification</source>
        <translation>混合识别</translation>
    </message>
    <message>
        <location filename="../biometricmoreinfo.cpp" line="191"/>
        <source>Other Identification</source>
        <translation>其他识别</translation>
    </message>
</context>
<context>
    <name>Biometrics</name>
    <message>
        <source>Biometrics</source>
        <translation type="vanished">登录选项</translation>
    </message>
    <message>
        <location filename="../biometrics.cpp" line="36"/>
        <source>Login Options</source>
        <translation>登录选项</translation>
    </message>
</context>
<context>
    <name>BiometricsWidget</name>
    <message>
        <source>Biometric password</source>
        <translation type="vanished">登录选项</translation>
    </message>
    <message>
        <source>Account password</source>
        <translation type="vanished">帐户密码</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="139"/>
        <location filename="../ui_biometricswidget.h" line="380"/>
        <source>Change password</source>
        <translation>修改密码</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="334"/>
        <location filename="../ui_biometricswidget.h" line="387"/>
        <source>(Fingerprint, face recognition, etc)</source>
        <translation>(指纹、人脸识别等)</translation>
    </message>
    <message>
        <source>Enable biometrics </source>
        <translation type="vanished">生物特征</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="208"/>
        <location filename="../biometricswidget.cpp" line="878"/>
        <location filename="../biometricswidget.cpp" line="944"/>
        <location filename="../ui_biometricswidget.h" line="382"/>
        <source>(Can be used to log in, unlock the system, and authorize authentication)</source>
        <translation>(可用于登录、解锁系统及授权认证)</translation>
    </message>
    <message>
        <source>Device Type</source>
        <translation type="vanished">设备类型</translation>
    </message>
    <message>
        <source>Device Name</source>
        <translation type="vanished">设备名</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="201"/>
        <location filename="../ui_biometricswidget.h" line="381"/>
        <source>Scan code login</source>
        <translation>扫码登录</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="59"/>
        <location filename="../ui_biometricswidget.h" line="378"/>
        <source>Login options</source>
        <translation>登录选项</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="113"/>
        <location filename="../ui_biometricswidget.h" line="379"/>
        <source>Password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="228"/>
        <location filename="../ui_biometricswidget.h" line="383"/>
        <source>Bound wechat:</source>
        <translation>已绑定的微信:</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="257"/>
        <location filename="../ui_biometricswidget.h" line="385"/>
        <source>Bind</source>
        <translation>绑定</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="327"/>
        <location filename="../ui_biometricswidget.h" line="386"/>
        <source>Biometric</source>
        <translation>生物识别</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="456"/>
        <location filename="../ui_biometricswidget.h" line="389"/>
        <source>Type</source>
        <translation>类型</translation>
    </message>
    <message>
        <location filename="../biometricswidget.ui" line="574"/>
        <location filename="../ui_biometricswidget.h" line="390"/>
        <source>Device</source>
        <translation>设备</translation>
    </message>
    <message>
        <source>Add biometric feature</source>
        <translation type="vanished">添加生物密码</translation>
    </message>
    <message>
        <source>Disable this function</source>
        <translation type="vanished">禁用该功能</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="99"/>
        <source>Advanced Settings</source>
        <translation>高级设置</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="267"/>
        <source>Standard</source>
        <translation>标准用户</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="269"/>
        <source>Admin</source>
        <translation type="unfinished">管理员</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="271"/>
        <source>root</source>
        <translation type="unfinished">Root</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="495"/>
        <source>(default)</source>
        <translation>（默认）</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="502"/>
        <source>Add </source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="506"/>
        <location filename="../biometricswidget.cpp" line="511"/>
        <source>No available device was detected</source>
        <translation>未检测到可用设备</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="929"/>
        <source>Binding WeChat</source>
        <translation>绑定微信</translation>
    </message>
    <message>
        <source>Delete</source>
        <translation type="obsolete">删除</translation>
    </message>
    <message>
        <source>Unbound</source>
        <translation type="vanished">未绑定</translation>
    </message>
    <message>
        <location filename="../biometricswidget.cpp" line="933"/>
        <source>Unbind</source>
        <translation>解除绑定</translation>
    </message>
</context>
<context>
    <name>ChangeFeatureName</name>
    <message>
        <location filename="../changefeaturename.ui" line="26"/>
        <location filename="../ui_changefeaturename.h" line="171"/>
        <source>Change Username</source>
        <translation>修改用户名</translation>
    </message>
    <message>
        <location filename="../changefeaturename.ui" line="65"/>
        <location filename="../ui_changefeaturename.h" line="173"/>
        <source>Change featurename</source>
        <translation>修改特证名</translation>
    </message>
    <message>
        <location filename="../changefeaturename.ui" line="261"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <source>Feature name</source>
        <translation type="vanished">特征名称</translation>
    </message>
    <message>
        <location filename="../changefeaturename.ui" line="242"/>
        <location filename="../ui_changefeaturename.h" line="177"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../ui_changefeaturename.h" line="178"/>
        <source>Save</source>
        <translation>保存</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="23"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="35"/>
        <source>Name already in use, change another one.</source>
        <translation>该用户名已存在，请更改。</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="37"/>
        <source>Duplicate feature name</source>
        <translation>特征名重复</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="39"/>
        <source>Please do not use spaces as the beginning or end of the feature name</source>
        <translation>请不要在名称首或者未输入空格</translation>
    </message>
    <message>
        <source> rename</source>
        <translation type="vanished">重命名</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="70"/>
        <source> Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../changefeaturename.cpp" line="67"/>
        <source> name</source>
        <translation>名称</translation>
    </message>
    <message>
        <source>Rename</source>
        <translation type="vanished">重命名</translation>
    </message>
    <message>
        <source>name</source>
        <translation type="vanished">名称</translation>
    </message>
</context>
<context>
    <name>ChangePwdDialog</name>
    <message>
        <location filename="../changepwddialog.ui" line="130"/>
        <location filename="../ui_changepwddialog.h" line="356"/>
        <source>Change Pwd</source>
        <translation>更改密码</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="317"/>
        <location filename="../ui_changepwddialog.h" line="360"/>
        <source>Pwd type</source>
        <translation>密码类型</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="395"/>
        <location filename="../ui_changepwddialog.h" line="362"/>
        <source>Cur pwd</source>
        <translation>当前密码</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="440"/>
        <location filename="../ui_changepwddialog.h" line="363"/>
        <source>New pwd</source>
        <translation>新密码</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="485"/>
        <location filename="../ui_changepwddialog.h" line="364"/>
        <source>New pwd sure</source>
        <translation>新密码确认</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="605"/>
        <location filename="../ui_changepwddialog.h" line="366"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../changepwddialog.ui" line="627"/>
        <location filename="../ui_changepwddialog.h" line="367"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="64"/>
        <source>Change pwd</source>
        <translation>更改密码</translation>
    </message>
    <message>
        <source>Cur pwd checking!</source>
        <translation type="vanished">当前密码检查!</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="172"/>
        <source>General Pwd</source>
        <translation>通用密码</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="178"/>
        <location filename="../changepwddialog.cpp" line="389"/>
        <source>Current Password</source>
        <translation>当前密码</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="179"/>
        <location filename="../changepwddialog.cpp" line="390"/>
        <location filename="../changepwddialog.cpp" line="398"/>
        <source>New Password</source>
        <translation>新密码</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="180"/>
        <location filename="../changepwddialog.cpp" line="391"/>
        <location filename="../changepwddialog.cpp" line="399"/>
        <source>New Password Identify</source>
        <translation>新密码确认</translation>
    </message>
    <message>
        <source>Authentication failed, input authtok again!</source>
        <translation type="vanished">密码输入错误,重新输入!</translation>
    </message>
    <message>
        <source>Pwd input error, re-enter!</source>
        <translation type="vanished">密码输入错误,重新输入!</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="327"/>
        <source>Contains illegal characters!</source>
        <translation>含有非法字符！</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="329"/>
        <source>Same with old pwd</source>
        <translation>与旧密码相同</translation>
    </message>
    <message>
        <source>Password length needs to more than %1 character!</source>
        <translation type="vanished">密码长度至少大于%1个字符！</translation>
    </message>
    <message>
        <source>Password length needs to less than %1 character!</source>
        <translation type="vanished">密码长度需要小于%1个字符！</translation>
    </message>
    <message>
        <source>Password length needs to more than 5 character!</source>
        <translation type="vanished">密码长度需要大于5个字符！</translation>
    </message>
    <message>
        <location filename="../changepwddialog.cpp" line="245"/>
        <location filename="../changepwddialog.cpp" line="365"/>
        <source>Inconsistency with pwd</source>
        <translation>与新密码不同</translation>
    </message>
</context>
<context>
    <name>ChangeUserPwd</name>
    <message>
        <source>Change Pwd</source>
        <translation type="vanished">修改密码</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="116"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="123"/>
        <source>Change password</source>
        <translation>修改密码</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="136"/>
        <location filename="../changeuserpwd.cpp" line="149"/>
        <location filename="../changeuserpwd.cpp" line="555"/>
        <source>Current Pwd</source>
        <translation>当前密码</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="178"/>
        <location filename="../changeuserpwd.cpp" line="189"/>
        <location filename="../changeuserpwd.cpp" line="556"/>
        <location filename="../changeuserpwd.cpp" line="569"/>
        <source>New Pwd</source>
        <translation>新密码</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="218"/>
        <location filename="../changeuserpwd.cpp" line="222"/>
        <location filename="../changeuserpwd.cpp" line="557"/>
        <location filename="../changeuserpwd.cpp" line="570"/>
        <source>Sure Pwd</source>
        <translation>确认新密码</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="280"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="284"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="450"/>
        <source>Authentication failed, input authtok again!</source>
        <translation>密码输入错误,重新输入!</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="749"/>
        <source>current pwd cannot be empty!</source>
        <translation>当前密码不能为空</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="754"/>
        <source>new pwd cannot be empty!</source>
        <translation>新密码不能为空</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="759"/>
        <source>sure pwd cannot be empty!</source>
        <translation>确认密码不能为空</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="373"/>
        <location filename="../changeuserpwd.cpp" line="638"/>
        <source>Inconsistency with pwd</source>
        <translation>与新密码不同</translation>
    </message>
    <message>
        <location filename="../changeuserpwd.cpp" line="600"/>
        <source>Contains illegal characters!</source>
        <translation>含有非法字符！</translation>
    </message>
</context>
<context>
    <name>DeviceType</name>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="40"/>
        <source>FingerPrint</source>
        <translation>指纹</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="42"/>
        <source>FingerVein</source>
        <translation>指静脉</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="44"/>
        <source>Iris</source>
        <translation>虹膜</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="46"/>
        <source>Face</source>
        <translation>人脸</translation>
    </message>
    <message>
        <location filename="../biometricdeviceinfo.cpp" line="48"/>
        <source>VoicePrint</source>
        <translation>声纹</translation>
    </message>
</context>
<context>
    <name>PasswdCheckUtil</name>
    <message>
        <location filename="../passwdcheckutil.cpp" line="159"/>
        <source>The password is shorter than %1 characters</source>
        <translation>密码少于 %1 个字符</translation>
    </message>
    <message>
        <source>The password contains less than %ld character classes</source>
        <translation type="obsolete">密码包含的字符类型少于 %1 种</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="162"/>
        <source>The password contains less than %1 character classes</source>
        <translation>密码包含的字符类型少于 %1 种</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="165"/>
        <source>The password is the same as the old one</source>
        <translation>与旧密码相同</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="168"/>
        <source>The password contains the user name in some form</source>
        <translation>密码包含了某种形式的用户名</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="171"/>
        <source>The password differs with case changes only</source>
        <translation>密码仅包含大小写变更</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="174"/>
        <source>The password is too similar to the old one</source>
        <translation>密码与原来的太相似</translation>
    </message>
    <message>
        <location filename="../passwdcheckutil.cpp" line="177"/>
        <source>The password is a palindrome</source>
        <translation>密码是一个回文</translation>
    </message>
</context>
<context>
    <name>QRCodeEnrollDialog</name>
    <message>
        <location filename="../qrcodeenroll.ui" line="26"/>
        <location filename="../ui_qrcodeenroll.h" line="139"/>
        <source>Form</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.ui" line="102"/>
        <location filename="../qrcodeenroll.cpp" line="140"/>
        <location filename="../ui_qrcodeenroll.h" line="141"/>
        <source>Bind Wechat Account</source>
        <translation>绑定微信账号</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.ui" line="112"/>
        <location filename="../qrcodeenroll.cpp" line="190"/>
        <location filename="../qrcodeenroll.cpp" line="613"/>
        <location filename="../ui_qrcodeenroll.h" line="142"/>
        <source>Please use wechat scanning code for binding.</source>
        <translation>请使用微信扫码，进行绑定。</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.ui" line="199"/>
        <location filename="../ui_qrcodeenroll.h" line="144"/>
        <source>Confirm</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="143"/>
        <source>Verify Wechat Account</source>
        <translation>验证微信账号</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="146"/>
        <source>Search Wechat Account</source>
        <translation>搜索微信账号</translation>
    </message>
    <message>
        <source>Permission is required.
Please authenticate yourself to continue</source>
        <translation type="vanished">需要授权，请先进行认证以继续操作</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="212"/>
        <source>Bind Successfully</source>
        <translation>绑定成功</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="247"/>
        <location filename="../qrcodeenroll.cpp" line="325"/>
        <source>Verify successfully</source>
        <translation>验证成功</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="254"/>
        <source>Not Match</source>
        <translation>不匹配</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="322"/>
        <source>The wechat account is bound successfully!</source>
        <translation>微信账号绑定成功</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="477"/>
        <source>Abnormal network</source>
        <translation>网络异常</translation>
    </message>
    <message>
        <source>Network anomalies</source>
        <translation type="vanished">网络异常</translation>
    </message>
    <message>
        <location filename="../qrcodeenroll.cpp" line="490"/>
        <location filename="../qrcodeenroll.cpp" line="495"/>
        <location filename="../qrcodeenroll.cpp" line="500"/>
        <source>Binding failure</source>
        <translation>绑定失败</translation>
    </message>
    <message>
        <source>Enroll successfully</source>
        <translation type="vanished">录入成功</translation>
    </message>
    <message>
        <source>D-Bus calling error</source>
        <translation type="obsolete">D-Bus获取错误</translation>
    </message>
    <message>
        <source>Device is busy</source>
        <translation type="vanished">设备忙</translation>
    </message>
    <message>
        <source>No such device</source>
        <translation type="vanished">设备不存在</translation>
    </message>
    <message>
        <source>Permission denied</source>
        <translation type="vanished">权限不够</translation>
    </message>
</context>
</TS>
