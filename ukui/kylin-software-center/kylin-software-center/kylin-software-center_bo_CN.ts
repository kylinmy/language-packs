<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>AdImageWidget</name>
    <message>
        <source>Download</source>
        <translation type="vanished">下载</translation>
    </message>
    <message>
        <source>After installation, you can experience the Android environment after restarting</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་རྗེས་བསྐྱར་དུ་འགོ་ཚུགས་རྗེས་ཨན་ཏུང་གི་ཁོར་ཡུག་ཉམས་ལེན་བྱས་ཆོག།</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པ།</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>AppCardWidget</name>
    <message>
        <source>install</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>New features</source>
        <translation>པར་གཞི་གསར་པའི་ནུས་པ།</translation>
    </message>
    <message>
        <source>Uninstall</source>
        <translation>ལེན་པ།</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པ།</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>AppDetailWidget</name>
    <message>
        <source>Platform introduction</source>
        <translation>སྟེགས་བུའི་ངོ་སྤྲོད།</translation>
    </message>
    <message>
        <source>Technical Features</source>
        <translation>ལག་རྩལ་གྱི་ཁྱད་ཆོས།</translation>
    </message>
    <message>
        <source>Number of Downloads</source>
        <translation>ཕབ་ལེན་བྱས་པའི་གྲངས་འབོར།</translation>
    </message>
    <message>
        <source>times</source>
        <translation>ཐེངས།</translation>
    </message>
    <message>
        <source> scores</source>
        <translation> སྐར་མ་ཞིག</translation>
    </message>
    <message>
        <source>Full Score 5 points</source>
        <translation>སྐར་མ་5ཐོབ་པ།</translation>
    </message>
    <message>
        <source>size</source>
        <translation>ཆེ་ཆུང་།</translation>
    </message>
    <message>
        <source>version</source>
        <translation>པར་གཞི།</translation>
    </message>
    <message>
        <source>update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པ།</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Brief Introduction</source>
        <translation>ངོ་སྤྲོད་མདོར་བསྡུས།</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>དྲ་ཚིགས་ས་གནས།</translation>
    </message>
    <message>
        <source>Update Content</source>
        <translation>ནང་དོན་གསར་སྒྱུར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Other information</source>
        <translation>ཆ་འཕྲིན་གཞན་དག</translation>
    </message>
    <message>
        <source>Package name</source>
        <translation>ཐུམ་སྒྲིལ་གྱི་མིང་།</translation>
    </message>
    <message>
        <source>Payment type</source>
        <translation>རིན་སྤྲོད་རིགས།</translation>
    </message>
    <message>
        <source>Safety inspection</source>
        <translation>བདེ་འཇགས་ཞིབ་བཤེར།</translation>
    </message>
    <message>
        <source>Developer</source>
        <translation>གསར་སྤེལ་ཚོང་པ།</translation>
    </message>
    <message>
        <source>Additional terms</source>
        <translation>ཟུར་སྣོན་ཆ་རྐྱེན།</translation>
    </message>
    <message>
        <source>In software charge</source>
        <translation>མཉེན་ཆས་ཀྱི་རིན་བསྡུའི་ཁྲོད།</translation>
    </message>
    <message>
        <source>free</source>
        <translation>རིན་མི་དགོས་པ།</translation>
    </message>
    <message>
        <source>Software signature</source>
        <translation>མཉེན་ཆས་ཀྱི་མིང་རྟགས།</translation>
    </message>
    <message>
        <source>Signature unit:</source>
        <translation>མིང་རྟགས་འགོད་པའི་ལས་ཁུངས།</translation>
    </message>
    <message>
        <source>Manual recheck</source>
        <translation>ལག་པས་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Intellectual property</source>
        <translation>ཤེས་བྱའི་ཐོན་དངོས་བདག་དབང་ཆོག་འཐུས།</translation>
    </message>
    <message>
        <source>Similar Software Recommendation</source>
        <translation>རིགས་གཅིག་པའི་མཉེན་ཆས་འོས་སྦྱོར།</translation>
    </message>
    <message>
        <source>Score</source>
        <translation>སྐར་གྲངས་ཐོབ་པ།</translation>
    </message>
    <message>
        <source>User comments</source>
        <translation>སྤྱོད་མཁན་གྱི་དཔྱད་གཏམ།</translation>
    </message>
    <message>
        <source>comments</source>
        <translation>དཔྱད་གཏམ་བརྗོད་པ།</translation>
    </message>
    <message>
        <source>Your feedback is the driving force for us to do a good job in domestic system software ecology!</source>
        <translation>ཁྱོད་ཀྱི་ཚུར་ལན་ནི་ང་ཚོས་རྒྱལ་ནང་གི་མ་ལག་མཉེན་ཆས་སྐྱེ་ཁམས་ལེགས་པར་སྒྲུབ་པའི་སྒུལ་ཤུགས་ཡིན།</translation>
    </message>
    <message>
        <source>Enter up to 200 characters</source>
        <translation>ཆེས་མང་ན་ཡི་གེ་200ལ་འཇུག་དགོས།</translation>
    </message>
    <message>
        <source>submit</source>
        <translation>གོང་སྤྲོད།</translation>
    </message>
    <message>
        <source>Please rate before commenting!</source>
        <translation>དཔྱད་གཏམ་མ་སྤེལ་གོང་ལ་སྐྱེད་ཀ་གནང་རོགས།</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>Please install the software before commenting!</source>
        <translation>དཔྱད་གཏམ་མ་སྤེལ་གོང་ལ་མཉེན་ཆས་འདི་སྒྲིག་སྦྱོར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>You have read all the comments</source>
        <translation>དཔྱད་གཏམ་ཚང་མ་ཁྱེད་ཀྱིས་བལྟས་ཟིན།</translation>
    </message>
    <message>
        <source> comments</source>
        <translation> དོན་ཚན།</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་མེད་པ།</translation>
    </message>
    <message>
        <source>W+</source>
        <translation>ཁྲི+</translation>
    </message>
    <message>
        <source>MB</source>
        <translation></translation>
    </message>
    <message>
        <source>KB</source>
        <translation></translation>
    </message>
    <message>
        <source>B</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AppendCommentPopupWindow</name>
    <message>
        <source>close</source>
        <translation>ཁ་རྒྱག</translation>
    </message>
    <message>
        <source>Additional comments</source>
        <translation>ཁ་གསབ་ཀྱི་བསམ་འཆར།</translation>
    </message>
    <message>
        <source>Your feedback is the driving force for us to do a good job in domestic system software ecology!</source>
        <translation>ཁྱོད་ཀྱི་ཚུར་ལན་ནི་ང་ཚོས་རྒྱལ་ནང་གི་མ་ལག་མཉེན་ཆས་སྐྱེ་ཁམས་ལེགས་པར་སྒྲུབ་པའི་སྒུལ་ཤུགས་ཡིན།</translation>
    </message>
    <message>
        <source>Enter up to 200 characters</source>
        <translation>ཆེས་མང་ན་ཡི་གེ་200ལ་འཇུག་དགོས།</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation>འདོར་བ།</translation>
    </message>
    <message>
        <source>submit</source>
        <translation>གོང་སྤྲོད།</translation>
    </message>
</context>
<context>
    <name>CategoryShowWidget</name>
    <message>
        <source>Default ranking</source>
        <translation>ཐོག་མའི་སྒྲིག་རིམ།</translation>
    </message>
    <message>
        <source>Download ranking</source>
        <translation>ཕབ་ལེན་གྱི་ཨང་རིམ།</translation>
    </message>
    <message>
        <source>Sort ranking</source>
        <translation>རིགས་དབྱེའི་ཨང་རིམ།</translation>
    </message>
    <message>
        <source>UpdateTime sorting</source>
        <translation>གསར་སྒྱུར་དུས་ཚོད་ཀྱི་རིགས་འབྱེད་པ།</translation>
    </message>
    <message>
        <source>Comment ranking</source>
        <translation>དཔྱད་གཏམ་གྱི་ཨང་རིམ།</translation>
    </message>
</context>
<context>
    <name>CategoryWidget</name>
    <message>
        <source>Default ranking</source>
        <translation>ཐོག་མའི་སྒྲིག་རིམ།</translation>
    </message>
    <message>
        <source>Download ranking</source>
        <translation>ཕབ་ལེན་གྱི་ཨང་རིམ།</translation>
    </message>
    <message>
        <source>Sort ranking</source>
        <translation>རིགས་དབྱེའི་ཨང་རིམ།</translation>
    </message>
    <message>
        <source>Comment ranking</source>
        <translation>དཔྱད་གཏམ་གྱི་ཨང་རིམ།</translation>
    </message>
    <message>
        <source>UpdateTime sorting</source>
        <translation>གསར་སྒྱུར་དུས་ཚོད་ཀྱི་རིགས་འབྱེད་པ།</translation>
    </message>
    <message>
        <source>The environment is initialized, the interface is temporarily locked, please wait...</source>
        <translation>ཁོར་ཡུག་ཐོག་མར་བཏོན་པ་དང་། འབྲེལ་མཐུད་བྱེད་ས་གནས་སྐབས་སུ་ཟྭ་བརྒྱབ་ནས་སྒུག་རོགས།</translation>
    </message>
    <message>
        <source>retry</source>
        <translation>བསྐྱར་དུ་ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>Comment</name>
    <message>
        <source>just</source>
        <translation>ད་ལྟ།</translation>
    </message>
    <message>
        <source>delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <source>Additional comments</source>
        <translation>ཁ་གསབ་ཀྱི་བསམ་འཆར།</translation>
    </message>
</context>
<context>
    <name>CommentWidget</name>
    <message>
        <source>just</source>
        <translation type="vanished">刚刚</translation>
    </message>
    <message>
        <source>delete</source>
        <translation type="vanished">删除</translation>
    </message>
</context>
<context>
    <name>DownloadRankWidget</name>
    <message>
        <source>Download Ranking</source>
        <translation>ཕབ་ལེན་གྱི་ཨང་རིམ།</translation>
    </message>
    <message>
        <source>View all</source>
        <translation>ཚང་མར་ལྟ་ཞིབ་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>DownloadRankingItem</name>
    <message>
        <source>Uninstall</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་མེད་པ།</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པ།</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>Downloading</name>
    <message>
        <source>Downloading</source>
        <translation>ཕབ་ལེན་བྱེད་པ།</translation>
    </message>
    <message>
        <source>all Pause</source>
        <translation>ཚང་མ་མཚམས་འཇོག་པ།</translation>
    </message>
    <message>
        <source>Open download directory</source>
        <translation>ཁ་ཕྱེ་ནས་ཕབ་ལེན་བྱས་པའི་དཀར་ཆག</translation>
    </message>
    <message>
        <source>Empty downloaded</source>
        <translation>ཕབ་ལེན་བྱས་པའི་སྟོང་ཆ།</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པ།</translation>
    </message>
    <message>
        <source>No download task</source>
        <translation>ཕབ་ལེན་གྱི་ལས་འགན་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>ImproveWorkEfficiencyTemplate</name>
    <message>
        <source>Selected Applications</source>
        <translation>བདམས་ཟིན་པའི་ཉེར་སྤྱོད།</translation>
    </message>
    <message>
        <source>View all</source>
        <translation>ཚང་མར་ལྟ་ཞིབ་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>ImproveWorkEfficiencyTemplateCard</name>
    <message>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པ།</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>KYComboBox</name>
    <message>
        <source>Your Email/Name/Phone</source>
        <translation>ཁྱེད་ཀྱི་གློག་རྡུལ་ཡིག་ཟམ་དང་སྤྱོད་མཁན་གྱི་མིང་། ཁ་པར།</translation>
    </message>
</context>
<context>
    <name>LandingWidget</name>
    <message>
        <source>No download task</source>
        <translation type="vanished">没有找到你要的软件</translation>
    </message>
    <message>
        <source>No software found</source>
        <translation>ཁྱེད་ལ་མཁོ་བའི་མཉེན་ཆས་མ་རྙེད་པ།</translation>
    </message>
    <message>
        <source>go to</source>
        <translation>ལ་སོང་།</translation>
    </message>
    <message>
        <source> Full library </source>
        <translation> དཔེ་མཛོད་ཁང་ཆ་ཚང་། </translation>
    </message>
    <message>
        <source>search and try</source>
        <translation>འཚོལ་ཞིབ་དང་ཚོད་ལྟ་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>MainDialog</name>
    <message>
        <source>Kylin ID</source>
        <translation>ཆི་ལིན ID</translation>
    </message>
    <message>
        <source>Forget?</source>
        <translation>གསང་གྲངས་བརྗེད་པ་རེད་དམ།</translation>
    </message>
    <message>
        <source>Remember it</source>
        <translation>གསང་གྲངས་ཡིད་ལ་འཛིན་དགོས།</translation>
    </message>
    <message>
        <source>Register</source>
        <translation>ཐོ་འགོད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Your password</source>
        <translation>ཁྱེད་ཀྱི་གསང་གྲངས་ནང་འཇུག་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Your code</source>
        <translation>ཁྱེད་ཀྱི་ཚབ་རྟགས་ནང་འཇུག་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Your phone number here</source>
        <translation>ཁྱེད་ཀྱི་འདི་གའི་ཁ་པར་ཨང་གྲངས་ནང་འཇུག་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Pass login</source>
        <translation>གསང་གྲངས་ཐོ་འགོད་བྱས་པ།</translation>
    </message>
    <message>
        <source>Phone login</source>
        <translation>ཁ་པར་གྱི་ཐོ་འགོད།</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>ཐོ་འགོད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>སྐུར་སྐྱེལ།</translation>
    </message>
    <message>
        <source>Please move slider to right place</source>
        <translation>ཁྱེད་ཀྱིས་ཤུད་ཆས་དེ་གཡས་ཕྱོགས་སུ་སྤོར་རོགས།</translation>
    </message>
    <message>
        <source>%1s left</source>
        <translation>%1སྐར་ཆ་ལྷག་ཡོད།</translation>
    </message>
    <message>
        <source>User stop verify Captcha</source>
        <translation>སྤྱོད་མཁན་གྱིས་ར་སྤྲོད་ཨང་གྲངས་མཚམས་བཞག</translation>
    </message>
    <message>
        <source>No response data!</source>
        <translation>ཚུར་སྣང་ལན་ཆ་མེད་པའི་གཞི་གྲངས།</translation>
    </message>
    <message>
        <source>Timeout!</source>
        <translation>ཐོ་འགོད་དུས་ཚོད་ལས་བརྒལ་འདུག་ཡང་བསྐྱར་ཚོད་ལྟ་བྱོས།</translation>
    </message>
    <message>
        <source>Wrong account or password!</source>
        <translation>ནོར་འཁྲུལ་གྱི་རྩིས་ཐོ་དང་ཡང་ན་གསང་གྲངས།</translation>
    </message>
    <message>
        <source>No network!</source>
        <translation>དྲ་རྒྱ་མེད།</translation>
    </message>
    <message>
        <source>Pictrure has expired!</source>
        <translation>འདྲ་པར་དུས་ལས་ཡོལ་འདུག</translation>
    </message>
    <message>
        <source>User deleted!</source>
        <translation>སྤྱོད་མཁན་གྱིས་བསུབ་སོང་།</translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation>བཀོལ་སྤྱོད་བྱས་ཟིན་པའི་ཁ་པར་ཨང་གྲངས་རེད།</translation>
    </message>
    <message>
        <source>Wrong phone number format!</source>
        <translation>ཁ་པར་ཨང་གྲངས་ཀྱི་རྣམ་གཞག་ནོར་སོང་།</translation>
    </message>
    <message>
        <source>Your are reach the limit!</source>
        <translation>ཁ་པར་འདིས་ཉིན་དེར་འཕྲིན་ཐུང་བསྡུ་ལེན་བྱེད་པའི་ཐེངས་གྲངས་ཚད་བཀག་ལ་སླེབས་ཡོད།</translation>
    </message>
    <message>
        <source>Please check your phone number!</source>
        <translation>ཁྱེད་ཀྱི་ཁ་པར་ཨང་གྲངས་ལ་ཞིབ་བཤེར་གནང་རོགས།</translation>
    </message>
    <message>
        <source>Please check your code!</source>
        <translation>ཁྱེད་ཀྱི་ཚབ་རྟགས་ལ་ཞིབ་བཤེར་གནང་རོགས།</translation>
    </message>
    <message>
        <source>Send sms Limited!</source>
        <translation>འཕྲིན་ཐུང་བསྐུར་ན་ཚད་བཀག་ལ་ཐོན་ཡོད།</translation>
    </message>
    <message>
        <source>Pictrure blocked!</source>
        <translation>འདྲ་པར་གྱི་རྣམ་པ་ཆག་སོང་།</translation>
    </message>
    <message>
        <source>Illegal code!</source>
        <translation>ཁྲིམས་དང་མི་མཐུན་པའི་ཚབ་རྟགས།</translation>
    </message>
    <message>
        <source>Phone code is expired!</source>
        <translation>ཁ་པར་གྱི་ཨང་གྲངས་དུས་ལས་ཡོལ་ཟིན།</translation>
    </message>
    <message>
        <source>Failed attemps limit reached!</source>
        <translation>ཕམ་ཁ་བྱུང་བའི་ཚོད་འཛིན་གྱི་ཚད་ལ་སླེབས་ཡོད།</translation>
    </message>
    <message>
        <source>Please wait</source>
        <translation>སྐུ་མཁྱེན་སྒུག་རོགས།…</translation>
    </message>
    <message>
        <source>Parsing data failed!</source>
        <translation>གཞི་གྲངས་ལ་དབྱེ་ཞིབ་བྱས་ཀྱང་ཕམ་ཉེས་བྱུང་བ་རེད།</translation>
    </message>
    <message>
        <source>Server internal error!</source>
        <translation>ཞབས་ཞུའི་ཡོ་བྱད་ནང་ཁུལ་གྱི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>Slider validate error</source>
        <translation>འདྲེད་བརྡར་གྱིས་ནོར་འཁྲུལ་ལ་ཚོད་དཔག་བྱས།</translation>
    </message>
    <message>
        <source>Phone code error!</source>
        <translation>ཁ་པར་ཨང་གྲངས་ནོར་སོང་།</translation>
    </message>
    <message>
        <source>Code can not be empty!</source>
        <translation>ཚབ་རྟགས་ནི་སྟོང་པ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <source>MCode can not be empty!</source>
        <translation>ཨང་ཀི་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <source>Unsupported operation!</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་བྱ་སྤྱོད་ཅིག་རེད།</translation>
    </message>
    <message>
        <source>Unsupported Client Type!</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་ཚོང་འགྲུལ་གྱི་རིགས་དབྱིབས་ཤིག་རེད།</translation>
    </message>
    <message>
        <source>Please check your input!</source>
        <translation>ཁྱེད་ཀྱི་ནང་དོན་ལ་ཞིབ་བཤེར་གནང་རོགས།</translation>
    </message>
    <message>
        <source>Process failed</source>
        <translation>བརྒྱུད་རིམ་ལ་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>software store</source>
        <translation>མཉེན་ཆས་ཚོང་ཁང་།</translation>
    </message>
    <message>
        <source>homePage</source>
        <translation>གཙོ་ངོས།</translation>
    </message>
    <message>
        <source>Office</source>
        <translation type="vanished">办公</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="obsolete">图像</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="vanished">影音</translation>
    </message>
    <message>
        <source>Drive</source>
        <translation>སྐུད་སྐུལ།</translation>
    </message>
    <message>
        <source>Mobile apps</source>
        <translation>སྒུལ་བདེའི་ཉེར་སྤྱོད།</translation>
    </message>
    <message>
        <source>All classification</source>
        <translation>རིགས་དབྱེ་ཡོད་ཚད།</translation>
    </message>
    <message>
        <source>Software management</source>
        <translation>མཉེན་ཆས་དོ་དམ།</translation>
    </message>
    <message>
        <source>Not logged in!</source>
        <translation>ཐོ་འགོད་མ་བྱས་པ།</translation>
    </message>
    <message>
        <source>Log in now</source>
        <translation>ད་ལྟ་ཐོ་འགོད་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>User settings</source>
        <translation>སྤྱོད་མཁན་གྱི་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <source>Log out</source>
        <translation>ཕྱིར་འཐེན་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Sign in</source>
        <translation>ནང་འཛུལ།</translation>
    </message>
    <message>
        <source>NetWork error</source>
        <translation>དྲ་རྒྱ་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>Please check the network or try again later</source>
        <translation>དྲ་རྒྱར་ཞིབ་བཤེར་བྱེད་པའམ་ཡང་ན་རྗེས་སུ་ཡང་བསྐྱར་ཚོད་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation>ཡང་བསྐྱར་ཚོད་ལེན།</translation>
    </message>
    <message>
        <source>Comment failed</source>
        <translation>དཔྱད་གཏམ་ལ་ཕམ་ཉེས་བྱུང་།</translation>
    </message>
    <message>
        <source>Contains sensitive words</source>
        <translation>དེའི་ནང་དུ་ཚོར་བ་སྐྱེན་པའི་ཐ་སྙད་འདུས་ཡོད།</translation>
    </message>
    <message>
        <source>ok</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>Failed to get mobile app list.</source>
        <translation type="vanished">获取移动应用列表失败</translation>
    </message>
    <message>
        <source>The obtained application list is empty.</source>
        <translation type="vanished">获取的应用程序列表为空</translation>
    </message>
    <message>
        <source>Get mobile app list...</source>
        <translation>སྒུལ་བདེའི་ཉེར་སྤྱོད་གོ་རིམ་གྱི་མིང་ཐོ་ཐོབ།</translation>
    </message>
    <message>
        <source>Failed to get mobile app list, please try again later.</source>
        <translation>སྒུལ་བདེའི་ཉེར་སྤྱོད་རེའུ་མིག་མ་ཐོབ་པས་རྗེས་སུ་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པ།</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་མེད་པ།</translation>
    </message>
    <message>
        <source>It is not suitable for the hardware combination of this machine. Please look forward to it.</source>
        <translation>འཕྲུལ་ཆས་འདིའི་མཁྲེགས་ཆས་སྡེབ་སྒྲིག་བྱེད་པར་མི་འཚམ། རེ་སྒུག་ཆེན་པོ་བྱེད་ཀྱི་ཡོད།</translation>
    </message>
    <message>
        <source>The obtained mobile app list is empty.</source>
        <translation>ཐོབ་ཟིན་པའི་སྒུལ་བདེའི་ཉེར་སྤྱོད་རེའུ་མིག་ནི་སྟོང་བ་ཡིན།</translation>
    </message>
    <message>
        <source>Please install mobile operating environment!</source>
        <translation>སྒུལ་བདེའི་བཀོལ་སྤྱོད་ཁོར་ཡུག་སྒྲིག་སྦྱོར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Select all</source>
        <translation>ཡོངས་འདེམས།</translation>
    </message>
    <message>
        <source>709 graphics card optimization, please look forward to!</source>
        <translation>709རི་མོའི་བྱང་བུ་ལེགས་སྒྱུར་བྱེད་པར་རེ་སྒུག་ཆེན་པོ་བྱེད་ཀྱི་ཡོད།</translation>
    </message>
    <message>
        <source>Frequent operation</source>
        <translation>རྒྱུན་དུ་བཀོལ་སྤྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Please try again later</source>
        <translation>ཅུང་ཙམ་འགོར་རྗེས་ཡང་བསྐྱར་ཚོད་ལྟ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Multiple likes or cancellations are not allowed</source>
        <translation>ཐེངས་མང་པོར་དགའ་པོ་བྱེད་པའམ་ཡང་ན་མེད་པར་བཟོས་མི་ཆོག།</translation>
    </message>
    <message>
        <source>Comment like failed</source>
        <translation>དཔྱད་གཏམ་དེ་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <source>Failed to delete user comments</source>
        <translation>སྤྱོད་མཁན་གྱི་བསམ་འཆར་བསུབ་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <source>Update now</source>
        <translation>ད་ལྟ་གསར་སྒྱུར་བྱས།</translation>
    </message>
    <message>
        <source>Deselect all</source>
        <translation>ཚང་མ་ཕྱིར་འབུད་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>No software to update</source>
        <translation>གསར་སྒྱུར་བྱེད་པའི་མཉེན་ཆས་མེད་པ།</translation>
    </message>
    <message>
        <source>Update software</source>
        <translation>གསར་སྒྱུར་མཉེན་ཆས།</translation>
    </message>
    <message>
        <source>No uninstallable software</source>
        <translation>གནས་སྐབས་སུ་བསུབ་ཐབས་མེད་པའི་མཉེན་ཆས།</translation>
    </message>
    <message>
        <source>Uninstall software</source>
        <translation>མཉེན་ཆས་སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Local historical installation</source>
        <translation>ས་གནས་དེ་གའི་ལོ་རྒྱུས་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <source>No historical installation</source>
        <translation>གནས་སྐབས་སྒྲིག་སྦྱོར་ཟིན་ཐོ་མེད།</translation>
    </message>
    <message>
        <source>Historical installation</source>
        <translation>ལོ་རྒྱུས་ཀྱི་སྒྲིག་སྦྱོར།</translation>
    </message>
    <message>
        <source>Cloud history installation</source>
        <translation>སྤྲིན་གྱི་ལོ་རྒྱུས་སྒྲིག་སྦྱོར།</translation>
    </message>
    <message>
        <source>Network problem, please try again later</source>
        <translation>དྲ་རྒྱའི་གནད་དོན།ཕྱིས་སུ་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Uninstall</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་མེད་པ།</translation>
    </message>
    <message>
        <source>all Pause</source>
        <translation>ཚང་མ་མཚམས་འཇོག་ཏུ་བཞག་པ།</translation>
    </message>
    <message>
        <source>all Continue</source>
        <translation>ཚང་མ་མུ་མཐུད་དུ།</translation>
    </message>
    <message>
        <source>Downloading</source>
        <translation>ཕབ་ལེན་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Download path does not have read/write permission</source>
        <translation>ཕབ་ལེན་གྱི་ལམ་ཕྲན་ལ་ཀློག་འདོན་དང་འབྲི་བའི་ཆོག་མཆན་མེད།</translation>
    </message>
    <message>
        <source>Please use a different download path</source>
        <translation>ཁྱེད་ཀྱིས་གཞན་དང་མི་འདྲ་བའི་ཕབ་ལེན་ལམ་བུ་བེད་སྤྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <source>set up path</source>
        <translation>འགྲོ་ལམ་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <source> is installed</source>
        <translation> སྒྲིག་སྦྱོར་བྱས་ཡོད།</translation>
    </message>
    <message>
        <source>Failed to install app!</source>
        <translation>ཉེར་སྤྱོད་གོ་རིམ་སྒྲིག་སྦྱོར་བྱེད་མ་ཐུབ་པ་རེད།</translation>
    </message>
    <message>
        <source>Dependency resolution failed</source>
        <translation>གཞན་རྟེན་རང་བཞིན་གྱི་ཐག་གཅོད་བྱེད་མ་ཐུབ།</translation>
    </message>
    <message>
        <source>Install Error</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>Insufficient space</source>
        <translation>བར་སྟོང་མི་འདང་བ།</translation>
    </message>
    <message>
        <source>Please check the network</source>
        <translation>དྲ་རྒྱར་ཞིབ་བཤེར་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>Successfully installed the new version</source>
        <translation>པར་གཞི་གསར་པ་བདེ་བླག་ངང་སྒྲིག་སྦྱོར་བྱས་པ།</translation>
    </message>
    <message>
        <source>Open the new software store</source>
        <translation>མཉེན་ཆས་ཚོང་ཁང་གསར་པ་སྒོ་འབྱེད་པ།</translation>
    </message>
    <message>
        <source>Install error</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>Please check apt</source>
        <translation type="vanished">请检查apt</translation>
    </message>
    <message>
        <source>Confirm to cancel the upgrade?</source>
        <translation>རིམ་སྤོར་མེད་པར་བཟོ་རྒྱུ་གཏན་འཁེལ་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>Uninstall error</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <source>New version detected</source>
        <translation>ཞིབ་དཔྱད་ཚད་ལེན་བྱས་པའི་པར་གཞི་གསར་བ།</translation>
    </message>
    <message>
        <source>Downloading installation package...</source>
        <translation>ནང་འཇུག་ཁུག་མ་ཕབ་ལེན་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>Confirm to cancel the upgrade</source>
        <translation type="vanished">确定取消升级？</translation>
    </message>
    <message>
        <source>Cancel upgrade</source>
        <translation>རིམ་སྤོར་མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <source>Continue upgrading</source>
        <translation>མུ་མཐུད་དུ་རིམ་པ་སྤར་བ།</translation>
    </message>
    <message>
        <source>It is currently the latest version</source>
        <translation>དེ་ནི་མིག་སྔར་ཆེས་གསར་བའི་པར་གཞི་ཡིན།</translation>
    </message>
    <message>
        <source>All results </source>
        <translation>གྲུབ་འབྲས་ཡོད་ཚད། </translation>
    </message>
    <message>
        <source>Default ranking</source>
        <translation>ཐོག་མའི་སྒྲིག་རིམ།</translation>
    </message>
    <message>
        <source>Download ranking</source>
        <translation>ཕབ་ལེན་གྱི་ཨང་རིམ།</translation>
    </message>
    <message>
        <source>Sort ranking</source>
        <translation>རིགས་དབྱེའི་ཨང་རིམ།</translation>
    </message>
    <message>
        <source>Comment ranking</source>
        <translation>དཔྱད་གཏམ་གྱི་ཨང་རིམ།</translation>
    </message>
    <message>
        <source>UpdateTime sorting</source>
        <translation>གསར་སྒྱུར་དུས་ཚོད་ཀྱི་རིགས་འབྱེད་པ།</translation>
    </message>
    <message>
        <source>There are still tasks in progress. Are you sure you want to exit?</source>
        <translation>ད་དུང་ལས་འགན་སྒྲུབ་བཞིན་ཡོད། ཁྱོད་ཀྱིས་རང་ཉིད་ཕྱིར་འགྲོ་འདོད་པ་གཏན་འཁེལ་བྱེད་ཐུབ་བམ།</translation>
    </message>
    <message>
        <source>Exit now, you will not be able to check more applications.</source>
        <translation>ད་ལྟ་ཕྱིར་འབུད་བྱས་ན་ཁྱོད་ཀྱིས་བཀོལ་སྤྱོད་བྱ་རིམ་མང་པོ་ཞིག་ལ་བརྟག་དཔྱད་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <source>maximize</source>
        <translation>ཆེ་ཤོས་སུ་སྒྱུར་བ།</translation>
    </message>
    <message>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <source>MB</source>
        <translation></translation>
    </message>
    <message>
        <source>KB</source>
        <translation></translation>
    </message>
    <message>
        <source>B</source>
        <translation></translation>
    </message>
    <message>
        <source>apk Security verification failed</source>
        <translation>apkཁུག་མའི་བདེ་འཇགས་ར་སྤྲོད་ཕམ་སོང་།</translation>
    </message>
    <message>
        <source>%1</source>
        <translation></translation>
    </message>
    <message>
        <source>About to update automatically</source>
        <translation>རང་འགུལ་གྱིས་གསར་སྒྱུར་བྱེད་གྲབས་ཡོད།</translation>
    </message>
    <message>
        <source>Error Code</source>
        <translation>ནོར་འཁྲུལ་གྱི་ཚབ་རྟགས།</translation>
    </message>
    <message>
        <source>Environment startup failed</source>
        <translation>ཁོར་ཡུག་གསར་གཏོད་བྱེད་པར་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
    <message>
        <source>The environment is initialized, the interface is temporarily locked, please wait...</source>
        <translation>ཁོར་ཡུག་ཐོག་མར་བཏོན་པ་དང་། འབྲེལ་མཐུད་བྱེད་ས་གནས་སྐབས་སུ་ཟྭ་བརྒྱབ་ནས་སྒུག་རོགས།</translation>
    </message>
</context>
<context>
    <name>NewFeaturesWidget</name>
    <message>
        <source>New Features</source>
        <translation>པར་གཞི་གསར་པའི་ནུས་པ།</translation>
    </message>
    <message>
        <source>Release time:</source>
        <translation>འགྲེམས་སྤེལ་བྱེད་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <source>Installed version:</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པའི་པར་གཞི།</translation>
    </message>
    <message>
        <source>Software size:</source>
        <translation>མཉེན་ཆས་ཀྱི་ཆེ་ཆུང་ནི།</translation>
    </message>
    <message>
        <source>Latest version:</source>
        <translation>ཆེས་གསར་བའི་པར་གཞི་ནི།</translation>
    </message>
</context>
<context>
    <name>NewProductsWidget</name>
    <message>
        <source>The latest %1 software on the shelf today</source>
        <translation>དེ་རིང་%1གསར་སྒྱུར་བྱས་།</translation>
    </message>
    <message>
        <source>View all</source>
        <translation>ཚང་མར་ལྟ་ཞིབ་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>OtherInfomationModule</name>
    <message>
        <source>Privacy policy</source>
        <translation>གསང་དོན་སྲིད་ཇུས།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>kylin-software-center is already running!</source>
        <translation>ཅིན་ལིན་གྱི་མཉེན་ཆས་ལྟེ་གནས་འཁོར་སྐྱོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
</context>
<context>
    <name>RatingWidget</name>
    <message>
        <source>scores</source>
        <translation>གྲུབ་འབྲས།</translation>
    </message>
    <message>
        <source>full Score 5 points</source>
        <translation>སྐར་མ་5ཐོབ་པ།</translation>
    </message>
    <message>
        <source>My score</source>
        <translation>ངའི་སྐར་གྲངས།</translation>
    </message>
    <message>
        <source>Scoring failed</source>
        <translation>སྐར་གྲངས་ཐོབ་པར་ཕམ་ཉེས་བྱུང་།</translation>
    </message>
    <message>
        <source>Please install the app before scoring</source>
        <translation>སྐར་མ་རྒྱག་པའི་སྔོན་ལ་བཀོལ་སྤྱོད་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>No score yet</source>
        <translation>ད་ལྟ་ད་དུང་སྐར་གྲངས་མེད།</translation>
    </message>
</context>
<context>
    <name>RemoterDaemon</name>
    <message>
        <source>Several new software products in the software store have been launched. Please check more details now!</source>
        <translation type="vanished">软件商店推出了几种新的软件产品,请立即查看更多详细信息！</translation>
    </message>
    <message>
        <source>Software Store</source>
        <translation type="vanished">软件商店</translation>
    </message>
    <message>
        <source>New Arrivals</source>
        <translation type="vanished">新品上架</translation>
    </message>
    <message>
        <source>View now</source>
        <translation type="vanished">立即查看</translation>
    </message>
</context>
<context>
    <name>SearchTipWidget</name>
    <message>
        <source>Daily recommendations</source>
        <translation>ཉིན་རེའི་གྲོས་འགོ།</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>ཁག་གཅིག་བརྗེས།</translation>
    </message>
    <message>
        <source>Search all software...</source>
        <translation>མཉེན་ཆས་ཡོད་ཚད་འཚོལ་ཞིབ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Software</source>
        <translation>མཉེན་ཆས།</translation>
    </message>
    <message>
        <source>Mobile Apps</source>
        <translation>སྤོ་འགུལ་ཉེར་སྤྱོད།</translation>
    </message>
    <message>
        <source>Drive</source>
        <translation>སྐུད་སྐུལ།</translation>
    </message>
</context>
<context>
    <name>SelectedApplicationsCard</name>
    <message>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པ།</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>SelectedApplicationsWidget</name>
    <message>
        <source>View all</source>
        <translation>ཚང་མར་ལྟ་ཞིབ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Selected Applications</source>
        <translation>བདམས་ཟིན་པའི་ཉེར་སྤྱོད།</translation>
    </message>
</context>
<context>
    <name>SetWidget</name>
    <message>
        <source>software store</source>
        <translation>མཉེན་ཆས་ཚོང་ཁང་།</translation>
    </message>
    <message>
        <source>Download directory settings</source>
        <translation>ཕབ་ལེན་བྱས་པའི་དཀར་ཆག་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <source>Save the downloaded software in the following directory:</source>
        <translation>གཤམ་གྱི་དཀར་ཆག་ནང་དུ་ཕབ་ལེན་བྱས་པའི་མཉེན་ཆས་ཉར་ཚགས་བྱས་ཡོད་དེ།</translation>
    </message>
    <message>
        <source>Restore default directory</source>
        <translation>ཁས་ལེན་དཀར་ཆག་སླར་གསོ་བྱས།</translation>
    </message>
    <message>
        <source>Installation package cleaning</source>
        <translation>སྒྲིག་སྦྱོར་ཁུག་མ་གཙང་བཤེར།</translation>
    </message>
    <message>
        <source>After installation, the downloaded installation package will be automatically deleted</source>
        <translation>ནང་འཇུག་བྱས་ཚར་རྗེས་རང་འགུལ་གྱིས་ནང་འཇུག་བྱས་པའི་ནང་འཇུག་ཁུག་མ་བསུབ་དགོས།</translation>
    </message>
    <message>
        <source>After installation, keep the downloaded installation package</source>
        <translation>ནང་འཇུག་བྱས་ཚར་རྗེས།ནང་འཇུག་བྱས་པའི་ནང་འཇུག་ཁུག་མ་ཉར་ཚགས་བྱས།</translation>
    </message>
    <message>
        <source>One week after the download, the installation package will be deleted automatically</source>
        <translation>ཕབ་ལེན་བྱས་ནས་གཟའ་འཁོར་གཅིག་འགོར་རྗེས་སྒྲིག་སྦྱོར་བྱས་པའི་ཁུག་མ་རང་འགུལ་གྱིས་བསུབ་རྒྱུ་རེད།</translation>
    </message>
    <message>
        <source>Allow the installed software to generate desktop shortcuts</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པའི་མཉེན་ཆས་ཀྱིས་ཅོག་ངོས་ཀྱི་མྱུར་ལམ་འབྱུང་དུ་འཇུག་དགོས།</translation>
    </message>
    <message>
        <source>Update settings</source>
        <translation>གསར་སྒྱུར་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <source>Automatic software update</source>
        <translation>རང་འགུལ་མཉེན་ཆས་གསར་སྒྱུར།</translation>
    </message>
    <message>
        <source>Automatically download and install software updates</source>
        <translation>རང་འགུལ་གྱིས་ཕབ་ལེན་དང་མཉེན་ཆས་གསར་བསྒྱུར་ནང་འཇུག་བྱས།</translation>
    </message>
    <message>
        <source>When the software is updated, a notification is displayed on the right side of the screen</source>
        <translation type="vanished">软件有更新时，在屏幕右侧显示通知</translation>
    </message>
    <message>
        <source>When the software is updated, you will be notified</source>
        <translation type="vanished">软件有更新时，会以通知形式告诉您</translation>
    </message>
    <message>
        <source>Auto update software store</source>
        <translation>རང་འགུལ་གྱིས་མཉེན་ཆས་ཚོང་ཁང་།</translation>
    </message>
    <message>
        <source>Automatically download the new version of software store and install it after you close software store</source>
        <translation>མཉེན་ཆས་ཚོང་ཁང་གི་པར་གཞི་གསར་པ་རང་འགུལ་གྱིས་ཕབ་ལེན་བྱས་ཏེ་མཉེན་ཆས་ཚོང་ཁང་སྒོ་བརྒྱབ་རྗེས་སྒྲིག་སྦྱོར་བྱས།</translation>
    </message>
    <message>
        <source>Start the software store automatically</source>
        <translation>རང་འགུལ་གྱིས་མཉེན་ཆས་ཚོང་ཁང་ནས་མགོ་བརྩམས།</translation>
    </message>
    <message>
        <source>After startup, the software store will start in the background</source>
        <translation>སྒོ་ཕྱེ་རྗེས།མཉེན་ཆས་ཚོང་ཁང་རྒྱབ་ཕྱོགས་ནས་སྒོ་འབྱེད་རྒྱུ་རེད།</translation>
    </message>
    <message>
        <source>Server address settings</source>
        <translation>ཞབས་ཞུ་ཆས་ཀྱི་ས་གནས་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <source>If there are internal services, you can change the server address to obtain all services of Kirin software store and Kirin ID.</source>
        <translation>གལ་ཏེ་ནང་ཁུལ་གྱི་ཞབས་ཞུ་ཡོད་ན།་ཞབས་ཞུ་ཆས་ཀྱི་ས་གནས་བརྗེ་ཆོག་ལ།ཆི་ལིན་མཉེན་ཆས་ཚོང་ཁང་དང་ཆི་ལིན་ཞབས་ཞུ་ཡོད་ཚད་ཐོབ་ཐུབ།</translation>
    </message>
    <message>
        <source>Software store server address:</source>
        <translation>མཉེན་ཆས་ཚོང་ཁང་གི་ཞབས་ཞུའི་ས་གནས།</translation>
    </message>
    <message>
        <source>Kylin ID server address:</source>
        <translation>ཆི་ལིནID་ཞབས་ཞུ་ཆས་ཀྱི་ས་གནས།</translation>
    </message>
    <message>
        <source>Restore default settings</source>
        <translation>ཐོག་མའི་སྒྲིག་བཀོད་སླར་གསོ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>Download path cannot be empty</source>
        <translation>ཕབ་ལེན་གྱི་ལམ་བུ་སྟོང་པ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <source>ok</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <source>Download path does not exist</source>
        <translation>ཕབ་ལེན་གྱི་ལམ་བུ་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>SoftwareManagement</name>
    <message>
        <source>Update</source>
        <translation>མཉེན་ཆས་གསར་སྒྱུར།</translation>
    </message>
    <message>
        <source>Uninstall</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་མེད་པ།</translation>
    </message>
    <message>
        <source>Historical</source>
        <translation>ལོ་རྒྱུས་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <source>Update software</source>
        <translation>གསར་སྒྱུར་མཉེན་ཆས།</translation>
    </message>
    <message>
        <source>Select all</source>
        <translation>ཡོངས་འདེམས།</translation>
    </message>
    <message>
        <source>Update all</source>
        <translation>ཚང་མ་གསར་སྒྱུར་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Uninstall all</source>
        <translation>ཚང་མ་སྒྲིག་སྦྱོར་བྱེད་དགོས།</translation>
    </message>
    <message>
        <source>Install all</source>
        <translation>ཚང་མ་སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Deselect all</source>
        <translation>ཚང་མ་ཕྱིར་འབུད་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Local historical installation</source>
        <translation>ས་གནས་དེ་གའི་ལོ་རྒྱུས་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <source>Cloud history installation</source>
        <translation>སྤྲིན་གྱི་ལོ་རྒྱུས་སྒྲིག་སྦྱོར།</translation>
    </message>
    <message>
        <source>About to update automatically</source>
        <translation>རང་འགུལ་གྱིས་གསར་སྒྱུར་བྱེད་གྲབས་ཡོད།</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པ།</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>SortButton</name>
    <message>
        <source>Default ranking</source>
        <translation>ཐོག་མའི་སྒྲིག་རིམ།</translation>
    </message>
</context>
<context>
    <name>SortCardWidget</name>
    <message>
        <source>Down</source>
        <translation>ཕབ་ལེན།</translation>
    </message>
    <message>
        <source>Run</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
</context>
<context>
    <name>SpecialModelCard</name>
    <message>
        <source>more</source>
        <translation>དེ་བས་ཀྱང་མང་བ།</translation>
    </message>
</context>
<context>
    <name>SpecialModelWidget</name>
    <message>
        <source>View all</source>
        <translation>ཚང་མར་ལྟ་ཞིབ་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Search software/mobile app/driver</source>
        <translation>མཉེན་ཆས་འཚོལ་ཞིབ།སྤོ་འགུལ་བཀོལ་སྤྱོད།སྐུལ་འདེད།</translation>
    </message>
    <message>
        <source>minimize</source>
        <translation>ཆེས་ཆུང་བསྒྱུར།</translation>
    </message>
    <message>
        <source>maximize</source>
        <translation>ཆེ་ཤོས་སུ་སྒྱུར་བ།</translation>
    </message>
    <message>
        <source>close</source>
        <translation>ཁ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>homePageWidget</name>
    <message>
        <source>The latest %1 software on the shelf today</source>
        <translation>དེ་རིང་%1་གསར་སྒྱུར་བྱས།</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Theme</source>
        <translation type="vanished">主题</translation>
    </message>
    <message>
        <source>Set</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <source>Check for updates</source>
        <translation>ཞིབ་བཤེར་བྱས་ནས་གནས་ཚུལ་གསར་སྒྱུར།</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation>རང་འགུལ་གྱི་རྣམ་པ།</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>མདོག་སྐྱ་བོའི་རྣམ་པ།</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>མདོག་ཟབ་པའི་རྣམ་པ།</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <source>kyliin-software-center</source>
        <translation type="vanished">软件商店</translation>
    </message>
    <message>
        <source>Software store is a graphical software management tool with rich content. It supports the recommendation of new products and popular applications, and provides one-stop software services such as software search, download, installation, update and uninstall.</source>
        <translation>མཉེན་ཆས་ཚོང་ཁང་ནི་ནང་དོན་ཕུན་སུམ་ཚོགས་པའི་རི་མོའི་མཉེན་ཆས་དོ་དམ་ཡོ་བྱད་ཅིག་ཡིན། དེས་ཐོན་རྫས་གསར་པ་དང་དགའ་བསུ་ཐོབ་པའི་ཉེར་སྤྱོད་གོ་རིམ་འོས་སྦྱོར་བྱེད་པར་རྒྱབ་སྐྱོར་བྱས་པ་མ་ཟད། མཉེན་ཆས་འཚོལ་བཤེར་དང་། ཕབ་ལེན། སྒྲིག་སྦྱོར། གསར་སྒྱུར། སྒྲིག་སྦྱོར་བཅས་བྱེད་པ་སོགས་གཅིག་བརྟེན་གྱི་མཉེན་ཆས་ཞབས་ཞུ་མཁོ་འདོན་བྱས་ཡོད།</translation>
    </message>
    <message>
        <source>close</source>
        <translation>ཁ་རྒྱག་པ།</translation>
    </message>
    <message>
        <source>Software store</source>
        <translation>མཉེན་ཆས་ཚོང་ཁང་།</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本:</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation>ཞབས་ཞུ་རྒྱབ་སྐྱོར་རུ་ཁག </translation>
    </message>
    <message>
        <source>kylin-software-center</source>
        <translation type="vanished">软件商店</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>ཕྱིར་འཐེན་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Disclaimers:</source>
        <translation>འགན་འཁྲི་མེད་པར་བཟོ་རྒྱུའི་གསལ་བསྒྲགས།</translation>
    </message>
    <message>
        <source>Kylin Software Store Disclaimers</source>
        <translation>མཉེན་ཆས་ཚོང་ཁང་གི་འགན་མེད་གསལ་བསྒྲགས།</translation>
    </message>
</context>
<context>
    <name>newproductcard</name>
    <message>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པ།</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་མེད་པ།</translation>
    </message>
    <message>
        <source>N</source>
        <translation>གསར་བ།</translation>
    </message>
</context>
<context>
    <name>sideBarWidget</name>
    <message>
        <source>Software Store</source>
        <translation>མཉེན་ཆས་ཚོང་ཁང་།</translation>
    </message>
    <message>
        <source>Sign in</source>
        <translation>ནང་འཛུལ།</translation>
    </message>
</context>
</TS>
