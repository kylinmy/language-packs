<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AdImageWidget</name>
    <message>
        <source>Download</source>
        <translation type="vanished">下载</translation>
    </message>
    <message>
        <source>After installation, you can experience the Android environment after restarting</source>
        <translation>安装完成，重启后即可体验安卓环境</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>已安装</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>安装</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>安装中</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>卸载中</translation>
    </message>
</context>
<context>
    <name>AppCardWidget</name>
    <message>
        <source>install</source>
        <translation>安装</translation>
    </message>
    <message>
        <source>New features</source>
        <translation>新版功能</translation>
    </message>
    <message>
        <source>Uninstall</source>
        <translation>卸载</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>已安装</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>安装</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>安装中</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>卸载中</translation>
    </message>
</context>
<context>
    <name>AppDetailWidget</name>
    <message>
        <source>Platform introduction</source>
        <translation>平台介绍</translation>
    </message>
    <message>
        <source>Technical Features</source>
        <translation>技术特点</translation>
    </message>
    <message>
        <source>Number of Downloads</source>
        <translation>下载数</translation>
    </message>
    <message>
        <source>times</source>
        <translation>次</translation>
    </message>
    <message>
        <source> scores</source>
        <translation>个评分</translation>
    </message>
    <message>
        <source>Full Score 5 points</source>
        <translation>满分5分</translation>
    </message>
    <message>
        <source>size</source>
        <translation>大小</translation>
    </message>
    <message>
        <source>version</source>
        <translation>版本</translation>
    </message>
    <message>
        <source>update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>已安装</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>安装</translation>
    </message>
    <message>
        <source>Brief Introduction</source>
        <translation>简介</translation>
    </message>
    <message>
        <source>Website</source>
        <translation>官网</translation>
    </message>
    <message>
        <source>Update Content</source>
        <translation>更新内容</translation>
    </message>
    <message>
        <source>Other information</source>
        <translation>其他信息</translation>
    </message>
    <message>
        <source>Package name</source>
        <translation>软件包名</translation>
    </message>
    <message>
        <source>Payment type</source>
        <translation>付费类型</translation>
    </message>
    <message>
        <source>Safety inspection</source>
        <translation>安全检测</translation>
    </message>
    <message>
        <source>Developer</source>
        <translation>开发者</translation>
    </message>
    <message>
        <source>Additional terms</source>
        <translation>附加条款</translation>
    </message>
    <message>
        <source>In software charge</source>
        <translation>软件内付费</translation>
    </message>
    <message>
        <source>free</source>
        <translation>免费</translation>
    </message>
    <message>
        <source>Software signature</source>
        <translation>软件签名</translation>
    </message>
    <message>
        <source>Signature unit:</source>
        <translation>签名单位</translation>
    </message>
    <message>
        <source>Manual recheck</source>
        <translation>人工复检</translation>
    </message>
    <message>
        <source>Intellectual property</source>
        <translation>知识产权许可</translation>
    </message>
    <message>
        <source>Similar Software Recommendation</source>
        <translation>同类软件推荐</translation>
    </message>
    <message>
        <source>Score</source>
        <translation>评分</translation>
    </message>
    <message>
        <source>User comments</source>
        <translation>用户评论</translation>
    </message>
    <message>
        <source>comments</source>
        <translation>条</translation>
    </message>
    <message>
        <source>Your feedback is the driving force for us to do a good job in domestic system software ecology!</source>
        <translation>您的反馈，是我们做好国产系统软件生态动力！</translation>
    </message>
    <message>
        <source>Enter up to 200 characters</source>
        <translation>最多输入200个字符</translation>
    </message>
    <message>
        <source>submit</source>
        <translation>提交</translation>
    </message>
    <message>
        <source>Please rate before commenting!</source>
        <translation>评论前请先进行评分！</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>确认</translation>
    </message>
    <message>
        <source>Please install the software before commenting!</source>
        <translation>评论前请先安装应用</translation>
    </message>
    <message>
        <source>You have read all the comments</source>
        <translation>您已看完所有评论</translation>
    </message>
    <message>
        <source> comments</source>
        <translation>条</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>安装中</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>卸载中</translation>
    </message>
    <message>
        <source>W+</source>
        <translation>万+</translation>
    </message>
    <message>
        <source>MB</source>
        <translation></translation>
    </message>
    <message>
        <source>KB</source>
        <translation></translation>
    </message>
    <message>
        <source>B</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>AppendCommentPopupWindow</name>
    <message>
        <source>close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <source>Additional comments</source>
        <translation>追加评论</translation>
    </message>
    <message>
        <source>Your feedback is the driving force for us to do a good job in domestic system software ecology!</source>
        <translation>您的反馈，是我们做好国产系统软件生态动力！</translation>
    </message>
    <message>
        <source>Enter up to 200 characters</source>
        <translation>最多输入200个字符</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>submit</source>
        <translation>提交</translation>
    </message>
</context>
<context>
    <name>CategoryShowWidget</name>
    <message>
        <source>Default ranking</source>
        <translation>默认排行</translation>
    </message>
    <message>
        <source>Download ranking</source>
        <translation>下载排行</translation>
    </message>
    <message>
        <source>Sort ranking</source>
        <translation>评分排行</translation>
    </message>
    <message>
        <source>UpdateTime sorting</source>
        <translation>更新时间</translation>
    </message>
    <message>
        <source>Comment ranking</source>
        <translation>评论排行</translation>
    </message>
</context>
<context>
    <name>CategoryWidget</name>
    <message>
        <source>Default ranking</source>
        <translation>默认排行</translation>
    </message>
    <message>
        <source>Download ranking</source>
        <translation>下载排行</translation>
    </message>
    <message>
        <source>Sort ranking</source>
        <translation>评分排行</translation>
    </message>
    <message>
        <source>Comment ranking</source>
        <translation>评论排行</translation>
    </message>
    <message>
        <source>UpdateTime sorting</source>
        <translation>更新时间</translation>
    </message>
    <message>
        <source>The environment is initialized, the interface is temporarily locked, please wait...</source>
        <translation>环境初始化，暂时锁定界面，请等待...</translation>
    </message>
    <message>
        <source>retry</source>
        <translation>重试</translation>
    </message>
</context>
<context>
    <name>Comment</name>
    <message>
        <source>just</source>
        <translation>刚刚</translation>
    </message>
    <message>
        <source>delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <source>Additional comments</source>
        <translation>追加评论</translation>
    </message>
</context>
<context>
    <name>CommentWidget</name>
    <message>
        <source>just</source>
        <translation type="vanished">刚刚</translation>
    </message>
    <message>
        <source>delete</source>
        <translation type="vanished">删除</translation>
    </message>
</context>
<context>
    <name>DownloadRankWidget</name>
    <message>
        <source>Download Ranking</source>
        <translation>下载排行</translation>
    </message>
    <message>
        <source>View all</source>
        <translation>查看全部</translation>
    </message>
</context>
<context>
    <name>DownloadRankingItem</name>
    <message>
        <source>Uninstall</source>
        <translation>卸载</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>已安装</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>安装</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>安装中</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>卸载中</translation>
    </message>
</context>
<context>
    <name>Downloading</name>
    <message>
        <source>Downloading</source>
        <translation>正在下载</translation>
    </message>
    <message>
        <source>all Pause</source>
        <translation>全部暂停</translation>
    </message>
    <message>
        <source>Open download directory</source>
        <translation>打开下载目录</translation>
    </message>
    <message>
        <source>Empty downloaded</source>
        <translation>清空已下载</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>已安装</translation>
    </message>
    <message>
        <source>No download task</source>
        <translation>暂无下载任务</translation>
    </message>
</context>
<context>
    <name>ImproveWorkEfficiencyTemplate</name>
    <message>
        <source>Selected Applications</source>
        <translation>精选应用</translation>
    </message>
    <message>
        <source>View all</source>
        <translation>查看全部</translation>
    </message>
</context>
<context>
    <name>ImproveWorkEfficiencyTemplateCard</name>
    <message>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>已安装</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>安装</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>安装中</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>卸载中</translation>
    </message>
</context>
<context>
    <name>KYComboBox</name>
    <message>
        <source>Your Email/Name/Phone</source>
        <translation>邮箱/用户名/手机号码</translation>
    </message>
</context>
<context>
    <name>LandingWidget</name>
    <message>
        <source>No download task</source>
        <translation type="vanished">没有找到你要的软件</translation>
    </message>
    <message>
        <source>No software found</source>
        <translation>没有找到你要的软件</translation>
    </message>
    <message>
        <source>go to</source>
        <translation>去</translation>
    </message>
    <message>
        <source> Full library </source>
        <translation>全库</translation>
    </message>
    <message>
        <source>search and try</source>
        <translation>搜索试试</translation>
    </message>
</context>
<context>
    <name>MainDialog</name>
    <message>
        <source>Kylin ID</source>
        <translation>麒麟 ID</translation>
    </message>
    <message>
        <source>Forget?</source>
        <translation>忘记密码？</translation>
    </message>
    <message>
        <source>Remember it</source>
        <translation>记住密码</translation>
    </message>
    <message>
        <source>Register</source>
        <translation>注册</translation>
    </message>
    <message>
        <source>Your password</source>
        <translation>请输入密码</translation>
    </message>
    <message>
        <source>Your code</source>
        <translation>请输入验证码</translation>
    </message>
    <message>
        <source>Your phone number here</source>
        <translation>请输入手机号码</translation>
    </message>
    <message>
        <source>Pass login</source>
        <translation>密码登录</translation>
    </message>
    <message>
        <source>Phone login</source>
        <translation>手机登录</translation>
    </message>
    <message>
        <source>Login</source>
        <translation>登录</translation>
    </message>
    <message>
        <source>Send</source>
        <translation>发送</translation>
    </message>
    <message>
        <source>Please move slider to right place</source>
        <translation>请滑动滑块到合适的位置</translation>
    </message>
    <message>
        <source>%1s left</source>
        <translation>剩%1秒</translation>
    </message>
    <message>
        <source>User stop verify Captcha</source>
        <translation>用户给停止验证验证码</translation>
    </message>
    <message>
        <source>No response data!</source>
        <translation>无响应数据!</translation>
    </message>
    <message>
        <source>Timeout!</source>
        <translation>登录超时，请重试！</translation>
    </message>
    <message>
        <source>Wrong account or password!</source>
        <translation>服务器内部错误!</translation>
    </message>
    <message>
        <source>No network!</source>
        <translation>网络不可达!</translation>
    </message>
    <message>
        <source>Pictrure has expired!</source>
        <translation>图片已经过期！</translation>
    </message>
    <message>
        <source>User deleted!</source>
        <translation>用户已经销户！</translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation>手机号码已经在使用！</translation>
    </message>
    <message>
        <source>Wrong phone number format!</source>
        <translation>手机号码格式错误!</translation>
    </message>
    <message>
        <source>Your are reach the limit!</source>
        <translation>该手机当日接收短信次数达到上限！</translation>
    </message>
    <message>
        <source>Please check your phone number!</source>
        <translation>请检查您的手机号码！</translation>
    </message>
    <message>
        <source>Please check your code!</source>
        <translation>手机号码其他错误！</translation>
    </message>
    <message>
        <source>Send sms Limited!</source>
        <translation>发送短信达到限制！</translation>
    </message>
    <message>
        <source>Pictrure blocked!</source>
        <translation>图片格式损坏!</translation>
    </message>
    <message>
        <source>Illegal code!</source>
        <translation>非法参数！</translation>
    </message>
    <message>
        <source>Phone code is expired!</source>
        <translation>验证码已经失效！</translation>
    </message>
    <message>
        <source>Failed attemps limit reached!</source>
        <translation>尝试次数太多！</translation>
    </message>
    <message>
        <source>Please wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Parsing data failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Server internal error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Slider validate error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Phone code error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Code can not be empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>MCode can not be empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unsupported operation!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Unsupported Client Type!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please check your input!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Process failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>software store</source>
        <translation>软件商店</translation>
    </message>
    <message>
        <source>homePage</source>
        <translation>主页</translation>
    </message>
    <message>
        <source>Office</source>
        <translation type="vanished">办公</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="obsolete">图像</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="vanished">影音</translation>
    </message>
    <message>
        <source>Drive</source>
        <translation>驱动</translation>
    </message>
    <message>
        <source>Mobile apps</source>
        <translation>移动应用</translation>
    </message>
    <message>
        <source>All classification</source>
        <translation>全部分类</translation>
    </message>
    <message>
        <source>Software management</source>
        <translation>软件管理</translation>
    </message>
    <message>
        <source>Not logged in!</source>
        <translation>没有登录!</translation>
    </message>
    <message>
        <source>Log in now</source>
        <translation>立即登录</translation>
    </message>
    <message>
        <source>User settings</source>
        <translation>用户设置</translation>
    </message>
    <message>
        <source>Log out</source>
        <translation>退出登录</translation>
    </message>
    <message>
        <source>Sign in</source>
        <translation>登录</translation>
    </message>
    <message>
        <source>NetWork error</source>
        <translation>网络错误！</translation>
    </message>
    <message>
        <source>Please check the network or try again later</source>
        <translation>请检查网络或稍后重试。</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>Retry</source>
        <translation>重试</translation>
    </message>
    <message>
        <source>Comment failed</source>
        <translation>评论失败</translation>
    </message>
    <message>
        <source>Contains sensitive words</source>
        <translation>包含敏感词汇</translation>
    </message>
    <message>
        <source>ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <source>Failed to get mobile app list.</source>
        <translation type="vanished">获取移动应用列表失败</translation>
    </message>
    <message>
        <source>The obtained application list is empty.</source>
        <translation type="vanished">获取的应用程序列表为空</translation>
    </message>
    <message>
        <source>Get mobile app list...</source>
        <translation>获取移动应用列表...</translation>
    </message>
    <message>
        <source>Failed to get mobile app list, please try again later.</source>
        <translation>获取移动应用列表失败，请稍后再试</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>已安装</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>安装中</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>卸载中</translation>
    </message>
    <message>
        <source>It is not suitable for the hardware combination of this machine. Please look forward to it.</source>
        <translation>暂未适配本机硬件组合，敬请期待</translation>
    </message>
    <message>
        <source>The obtained mobile app list is empty.</source>
        <translation>获取的移动应用列表为空</translation>
    </message>
    <message>
        <source>Please install mobile operating environment!</source>
        <translation>请安装移动运行环境！</translation>
    </message>
    <message>
        <source>Select all</source>
        <translation>全选</translation>
    </message>
    <message>
        <source>709 graphics card optimization, please look forward to!</source>
        <translation>709显卡优化中,敬请期待！</translation>
    </message>
    <message>
        <source>Frequent operation</source>
        <translation>频繁操作</translation>
    </message>
    <message>
        <source>Please try again later</source>
        <translation>请稍后再试</translation>
    </message>
    <message>
        <source>Multiple likes or cancellations are not allowed</source>
        <translation>不允许多次点赞或取消点赞</translation>
    </message>
    <message>
        <source>Comment like failed</source>
        <translation>评论点赞失败</translation>
    </message>
    <message>
        <source>Failed to delete user comments</source>
        <translation>删除用户评论失败</translation>
    </message>
    <message>
        <source>Update now</source>
        <translation>立即更新</translation>
    </message>
    <message>
        <source>Deselect all</source>
        <translation>取消全选</translation>
    </message>
    <message>
        <source>No software to update</source>
        <translation>暂无可更新软件</translation>
    </message>
    <message>
        <source>Update software</source>
        <translation>更新软件</translation>
    </message>
    <message>
        <source>No uninstallable software</source>
        <translation>暂无可卸载软件</translation>
    </message>
    <message>
        <source>Uninstall software</source>
        <translation>卸载软件</translation>
    </message>
    <message>
        <source>Local historical installation</source>
        <translation>本机历史安装</translation>
    </message>
    <message>
        <source>No historical installation</source>
        <translation>暂无安装记录</translation>
    </message>
    <message>
        <source>Historical installation</source>
        <translation>历史安装</translation>
    </message>
    <message>
        <source>Cloud history installation</source>
        <translation>云历史安装</translation>
    </message>
    <message>
        <source>Network problem, please try again later</source>
        <translation>网络问题，请稍后再试</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>安装</translation>
    </message>
    <message>
        <source>Uninstall</source>
        <translation>卸载</translation>
    </message>
    <message>
        <source>all Pause</source>
        <translation>全部暂停</translation>
    </message>
    <message>
        <source>all Continue</source>
        <translation>全部继续</translation>
    </message>
    <message>
        <source>Downloading</source>
        <translation>正在下载</translation>
    </message>
    <message>
        <source>Download path does not have read/write permission</source>
        <translation>下载路径没有读写权限</translation>
    </message>
    <message>
        <source>Please use a different download path</source>
        <translation>请使用其他下载路径</translation>
    </message>
    <message>
        <source>set up path</source>
        <translation>设置路径</translation>
    </message>
    <message>
        <source> is installed</source>
        <translation>已安装</translation>
    </message>
    <message>
        <source>Failed to install app!</source>
        <translation>安装应用失败</translation>
    </message>
    <message>
        <source>Dependency resolution failed</source>
        <translation>依赖解析失败</translation>
    </message>
    <message>
        <source>Install Error</source>
        <translation>安装失败</translation>
    </message>
    <message>
        <source>Insufficient space</source>
        <translation>空间不够</translation>
    </message>
    <message>
        <source>Please check the network</source>
        <translation>请检查网络</translation>
    </message>
    <message>
        <source>Successfully installed the new version</source>
        <translation>成功安装新版本</translation>
    </message>
    <message>
        <source>Open the new software store</source>
        <translation>打开新版软件商店</translation>
    </message>
    <message>
        <source>Install error</source>
        <translation>安装失败</translation>
    </message>
    <message>
        <source>Please check apt</source>
        <translation type="vanished">请检查apt</translation>
    </message>
    <message>
        <source>Confirm to cancel the upgrade?</source>
        <translation>确定取消升级？</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>确认</translation>
    </message>
    <message>
        <source>Uninstall error</source>
        <translation>卸载失败</translation>
    </message>
    <message>
        <source>New version detected</source>
        <translation>检测到新版本</translation>
    </message>
    <message>
        <source>Downloading installation package...</source>
        <translation>正在下载安装包...</translation>
    </message>
    <message>
        <source>Confirm to cancel the upgrade</source>
        <translation type="vanished">确定取消升级？</translation>
    </message>
    <message>
        <source>Cancel upgrade</source>
        <translation>取消升级</translation>
    </message>
    <message>
        <source>Continue upgrading</source>
        <translation>继续升级</translation>
    </message>
    <message>
        <source>It is currently the latest version</source>
        <translation>当前已是最新版本</translation>
    </message>
    <message>
        <source>All results </source>
        <translation>全部结果</translation>
    </message>
    <message>
        <source>Default ranking</source>
        <translation>默认排行</translation>
    </message>
    <message>
        <source>Download ranking</source>
        <translation>下载排行</translation>
    </message>
    <message>
        <source>Sort ranking</source>
        <translation>评分排行</translation>
    </message>
    <message>
        <source>Comment ranking</source>
        <translation>评论排行</translation>
    </message>
    <message>
        <source>UpdateTime sorting</source>
        <translation>更新时间</translation>
    </message>
    <message>
        <source>There are still tasks in progress. Are you sure you want to exit?</source>
        <translation>仍有任务正在进行，确定要退出吗？</translation>
    </message>
    <message>
        <source>Exit now, you will not be able to check more applications.</source>
        <translation>现在退出，您将无法体检到更多应用程序。</translation>
    </message>
    <message>
        <source>maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <source>MainWindow</source>
        <translation></translation>
    </message>
    <message>
        <source>MB</source>
        <translation></translation>
    </message>
    <message>
        <source>KB</source>
        <translation></translation>
    </message>
    <message>
        <source>B</source>
        <translation></translation>
    </message>
    <message>
        <source>apk Security verification failed</source>
        <translation>apk包安全验证失败</translation>
    </message>
    <message>
        <source>%1</source>
        <translation></translation>
    </message>
    <message>
        <source>About to update automatically</source>
        <translation>即将自动更新</translation>
    </message>
    <message>
        <source>Error Code</source>
        <translation>错误码</translation>
    </message>
    <message>
        <source>Environment startup failed</source>
        <translation>环境启动失败</translation>
    </message>
    <message>
        <source>The environment is initialized, the interface is temporarily locked, please wait...</source>
        <translation>环境初始化，暂时锁定界面，请等待...</translation>
    </message>
</context>
<context>
    <name>NewFeaturesWidget</name>
    <message>
        <source>New Features</source>
        <translation>新版功能</translation>
    </message>
    <message>
        <source>Release time:</source>
        <translation>发布时间：</translation>
    </message>
    <message>
        <source>Installed version:</source>
        <translation>已装版本：</translation>
    </message>
    <message>
        <source>Software size:</source>
        <translation>软件大小：</translation>
    </message>
    <message>
        <source>Latest version:</source>
        <translation>最新版本：</translation>
    </message>
</context>
<context>
    <name>NewProductsWidget</name>
    <message>
        <source>The latest %1 software on the shelf today</source>
        <translation>今日更新%1个</translation>
    </message>
    <message>
        <source>View all</source>
        <translation>查看全部</translation>
    </message>
</context>
<context>
    <name>OtherInfomationModule</name>
    <message>
        <source>Privacy policy</source>
        <translation>隐私政策</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>kylin-software-center is already running!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>RatingWidget</name>
    <message>
        <source>scores</source>
        <translation>评分</translation>
    </message>
    <message>
        <source>full Score 5 points</source>
        <translation>满分5分</translation>
    </message>
    <message>
        <source>My score</source>
        <translation>我的评分</translation>
    </message>
    <message>
        <source>Scoring failed</source>
        <translation>评分失败</translation>
    </message>
    <message>
        <source>Please install the app before scoring</source>
        <translation>评分前请先安装应用</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>确认</translation>
    </message>
    <message>
        <source>No score yet</source>
        <translation>暂未评分</translation>
    </message>
</context>
<context>
    <name>RemoterDaemon</name>
    <message>
        <source>Several new software products in the software store have been launched. Please check more details now!</source>
        <translation type="vanished">软件商店推出了几种新的软件产品,请立即查看更多详细信息！</translation>
    </message>
    <message>
        <source>Software Store</source>
        <translation type="vanished">软件商店</translation>
    </message>
    <message>
        <source>New Arrivals</source>
        <translation type="vanished">新品上架</translation>
    </message>
    <message>
        <source>View now</source>
        <translation type="vanished">立即查看</translation>
    </message>
</context>
<context>
    <name>SearchTipWidget</name>
    <message>
        <source>Daily recommendations</source>
        <translation>每日推荐</translation>
    </message>
    <message>
        <source>Change</source>
        <translation>换一批</translation>
    </message>
    <message>
        <source>Search all software...</source>
        <translation>在 全部软件中 搜索...</translation>
    </message>
    <message>
        <source>Software</source>
        <translation>软件</translation>
    </message>
    <message>
        <source>Mobile Apps</source>
        <translation>移动应用</translation>
    </message>
    <message>
        <source>Drive</source>
        <translation>驱动</translation>
    </message>
</context>
<context>
    <name>SelectedApplicationsCard</name>
    <message>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>已安装</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>安装</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>安装中</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>卸载中</translation>
    </message>
</context>
<context>
    <name>SelectedApplicationsWidget</name>
    <message>
        <source>View all</source>
        <translation>查看全部</translation>
    </message>
    <message>
        <source>Selected Applications</source>
        <translation>精选应用</translation>
    </message>
</context>
<context>
    <name>SetWidget</name>
    <message>
        <source>software store</source>
        <translation>软件商店</translation>
    </message>
    <message>
        <source>Download directory settings</source>
        <translation>下载目录设置</translation>
    </message>
    <message>
        <source>Save the downloaded software in the following directory:</source>
        <translation>将下载的软件保存在以下目录：</translation>
    </message>
    <message>
        <source>Restore default directory</source>
        <translation>恢复默认目录</translation>
    </message>
    <message>
        <source>Installation package cleaning</source>
        <translation>安装包清理</translation>
    </message>
    <message>
        <source>After installation, the downloaded installation package will be automatically deleted</source>
        <translation>安装完成后，自动删除下载的安装包</translation>
    </message>
    <message>
        <source>After installation, keep the downloaded installation package</source>
        <translation>安装完成后，保留下载的安装包</translation>
    </message>
    <message>
        <source>One week after the download, the installation package will be deleted automatically</source>
        <translation>下载完成一周后，自动删除安装包</translation>
    </message>
    <message>
        <source>Allow the installed software to generate desktop shortcuts</source>
        <translation>允许安装后的软件生成桌面快捷方式</translation>
    </message>
    <message>
        <source>Update settings</source>
        <translation>更新设置</translation>
    </message>
    <message>
        <source>Automatic software update</source>
        <translation>自动更新软件</translation>
    </message>
    <message>
        <source>Automatically download and install software updates</source>
        <translation>自动下载并安装软件更新</translation>
    </message>
    <message>
        <source>When the software is updated, a notification is displayed on the right side of the screen</source>
        <translation type="vanished">软件有更新时，在屏幕右侧显示通知</translation>
    </message>
    <message>
        <source>When the software is updated, you will be notified</source>
        <translation type="vanished">软件有更新时，会以通知形式告诉您</translation>
    </message>
    <message>
        <source>Auto update software store</source>
        <translation>自动更新软件商店</translation>
    </message>
    <message>
        <source>Automatically download the new version of software store and install it after you close software store</source>
        <translation>自动下载新版软件商店，并在您关闭旧版软件商店后自动安装</translation>
    </message>
    <message>
        <source>Start the software store automatically</source>
        <translation>开机自动启动软件商店</translation>
    </message>
    <message>
        <source>After startup, the software store will start in the background</source>
        <translation>开机后，软件商店将在后台启动</translation>
    </message>
    <message>
        <source>Server address settings</source>
        <translation>服务器地址设置</translation>
    </message>
    <message>
        <source>If there are internal services, you can change the server address to obtain all services of Kirin software store and Kirin ID.</source>
        <translation>如有内部服务，可更改服务器地址，以获得麒麟软件商店和麒麟ID的所有服务。</translation>
    </message>
    <message>
        <source>Software store server address:</source>
        <translation>软件商店服务器地址：</translation>
    </message>
    <message>
        <source>Kylin ID server address:</source>
        <translation>麒麟ID服务器地址：</translation>
    </message>
    <message>
        <source>Restore default settings</source>
        <translation>恢复默认设置</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <source>OK</source>
        <translation>确认</translation>
    </message>
    <message>
        <source>Download path cannot be empty</source>
        <translation>下载路径不能为空</translation>
    </message>
    <message>
        <source>ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <source>Download path does not exist</source>
        <translation>下载路径不存在</translation>
    </message>
</context>
<context>
    <name>SoftwareManagement</name>
    <message>
        <source>Update</source>
        <translation>更新软件</translation>
    </message>
    <message>
        <source>Uninstall</source>
        <translation>卸载软件</translation>
    </message>
    <message>
        <source>Historical</source>
        <translation>历史安装</translation>
    </message>
    <message>
        <source>Update software</source>
        <translation>更新软件</translation>
    </message>
    <message>
        <source>Select all</source>
        <translation>全选</translation>
    </message>
    <message>
        <source>Update all</source>
        <translation>全部更新</translation>
    </message>
    <message>
        <source>Uninstall all</source>
        <translation>一键卸载</translation>
    </message>
    <message>
        <source>Install all</source>
        <translation>一键安装</translation>
    </message>
    <message>
        <source>Deselect all</source>
        <translation>取消全选</translation>
    </message>
    <message>
        <source>Local historical installation</source>
        <translation>本机历史安装</translation>
    </message>
    <message>
        <source>Cloud history installation</source>
        <translation>云历史安装</translation>
    </message>
    <message>
        <source>About to update automatically</source>
        <translation>即将自动更新</translation>
    </message>
    <message>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>已安装</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>安装中</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>卸载中</translation>
    </message>
</context>
<context>
    <name>SortButton</name>
    <message>
        <source>Default ranking</source>
        <translation>默认排行</translation>
    </message>
</context>
<context>
    <name>SortCardWidget</name>
    <message>
        <source>Down</source>
        <translation>下载</translation>
    </message>
    <message>
        <source>Run</source>
        <translation>打开</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
</context>
<context>
    <name>SpecialModelCard</name>
    <message>
        <source>more</source>
        <translation>更多</translation>
    </message>
</context>
<context>
    <name>SpecialModelWidget</name>
    <message>
        <source>View all</source>
        <translation>查看全部</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Search software/mobile app/driver</source>
        <translation>搜索软件/移动应用/驱动</translation>
    </message>
    <message>
        <source>minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <source>maximize</source>
        <translation>最大化</translation>
    </message>
    <message>
        <source>close</source>
        <translation>关闭</translation>
    </message>
</context>
<context>
    <name>homePageWidget</name>
    <message>
        <source>The latest %1 software on the shelf today</source>
        <translation>今日更新%1个</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Theme</source>
        <translation type="vanished">主题</translation>
    </message>
    <message>
        <source>Set</source>
        <translation>设置</translation>
    </message>
    <message>
        <source>Check for updates</source>
        <translation>检查更新</translation>
    </message>
    <message>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <source>Auto</source>
        <translation>自由模式</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>浅色模式</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>深色模式</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">退出</translation>
    </message>
    <message>
        <source>kyliin-software-center</source>
        <translation type="vanished">软件商店</translation>
    </message>
    <message>
        <source>Software store is a graphical software management tool with rich content. It supports the recommendation of new products and popular applications, and provides one-stop software services such as software search, download, installation, update and uninstall.</source>
        <translation>软件商店是一款内容丰富的图形化软件管理工具，支持新品上架、热门应用推荐等功能，提供软件搜索、下载、安装、更新、卸载等一站式软件服务。</translation>
    </message>
    <message>
        <source>close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <source>Software store</source>
        <translation>软件商店</translation>
    </message>
    <message>
        <source>Version: </source>
        <translation type="vanished">版本:</translation>
    </message>
    <message>
        <source>Service &amp; Support: </source>
        <translation>服务与支持:</translation>
    </message>
    <message>
        <source>kylin-software-center</source>
        <translation type="vanished">软件商店</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <source>Disclaimers:</source>
        <translation>免责申明:</translation>
    </message>
    <message>
        <source>Kylin Software Store Disclaimers</source>
        <translation>软件商店免责声明</translation>
    </message>
</context>
<context>
    <name>newproductcard</name>
    <message>
        <source>Open</source>
        <translation>打开</translation>
    </message>
    <message>
        <source>installed</source>
        <translation type="vanished">已安装</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>已安装</translation>
    </message>
    <message>
        <source>Update</source>
        <translation>更新</translation>
    </message>
    <message>
        <source>Install</source>
        <translation>安装</translation>
    </message>
    <message>
        <source>Installing</source>
        <translation>安装中</translation>
    </message>
    <message>
        <source>Uninstalling</source>
        <translation>卸载中</translation>
    </message>
    <message>
        <source>N</source>
        <translation>新</translation>
    </message>
</context>
<context>
    <name>sideBarWidget</name>
    <message>
        <source>Software Store</source>
        <translation>软件商店</translation>
    </message>
    <message>
        <source>Sign in</source>
        <translation>登录</translation>
    </message>
</context>
</TS>
