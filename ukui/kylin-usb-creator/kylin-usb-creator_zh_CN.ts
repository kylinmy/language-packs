<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>MainWindow</name>
    <message>
        <source>Kylin USB Creator</source>
        <translation type="vanished">麒麟U盘启动器</translation>
    </message>
    <message>
        <source>kylin usb creator</source>
        <translation type="vanished">麒麟U盘启动器</translation>
    </message>
    <message>
        <source>minimize</source>
        <translation type="vanished">最小化</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="167"/>
        <source>close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="80"/>
        <location filename="../mainwindow.cpp" line="144"/>
        <location filename="../mainwindow.cpp" line="245"/>
        <source>usb boot maker</source>
        <translation>U盘启动器</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="82"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="98"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="269"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="174"/>
        <source>Retry</source>
        <translation>重试</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="269"/>
        <source>USB driver is in production.Are you sure you want to stop task and exit the program?</source>
        <translation>启动盘正在制作中，是否停止任务并退出程序？</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="333"/>
        <location filename="../mainwindow.cpp" line="335"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="334"/>
        <source>select file</source>
        <translation>选择文件</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="352"/>
        <location filename="../mainwindow.cpp" line="374"/>
        <source>Click or drag to add a mirror file</source>
        <translation>点击或拖拽添加镜像文件</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="388"/>
        <source>Drag and drop the image file here</source>
        <translation>拖拽镜像文件到此</translation>
    </message>
</context>
<context>
    <name>Page1</name>
    <message>
        <location filename="../page1.cpp" line="52"/>
        <location filename="../page1.cpp" line="399"/>
        <source>Click or drag to add a mirror file</source>
        <translation>点击或拖拽添加镜像文件</translation>
    </message>
    <message>
        <location filename="../page1.cpp" line="446"/>
        <location filename="../page1.cpp" line="45"/>
        <source>Drag and drop the image file here</source>
        <translation>拖拽镜像文件到此</translation>
    </message>
    <message>
       // <location filename="../page1.cpp" line="58"/>
       // <source>select file</source>
       // <translation>选择文件</translation>
    </message>
    <message>
        <location filename="../page1.cpp" line="65"/>
        <source>choose iso file</source>
        <translation>选择光盘镜像文件</translation>
    </message>
    <message>
        <location filename="../page1.cpp" line="312"/>
        <location filename="../page1.cpp" line="333"/>
        <location filename="../page1.cpp" line="391"/>
        <source>select USB flash drive</source>
        <translation>选择U盘</translation>
    </message>
    <message>
        <location filename="../page1.cpp" line="386"/>
        <source>MBR signature not detected,continue anyway?</source>
        <translation>未检测到MBR签名，是否继续？</translation>
    </message>
    <message>
        //<location filename="../page1.cpp" line="335"/>
       // <location filename="../page1.cpp" line="393"/>
       // <source>Browse</source>
      //  <translation>浏览</translation>
    </message>
    <message>
        <location filename="../page1.cpp" line="55"/>
        <source>format partition</source>
        <translation>格式化分区</translation>
    </message>
    <message>
        <source>USB drive will be formatted,please backup your files!</source>
        <translation type="vanished">制作启动盘的U盘将被格式化，请先备份好重要文件！</translation>
    </message>
    <message>
        <source>Authorization</source>
        <translation type="vanished">授权</translation>
    </message>
    <message>
        <source>cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Choose iso file</source>
        <translation type="vanished">选择光盘镜像文件</translation>
    </message>
    <message>
        <source>Select USB drive</source>
        <translation type="vanished">选择U盘</translation>
    </message>
    <message>
        <source>Open</source>
        <translation type="vanished">浏览</translation>
    </message>
    <message>
        <location filename="../page1.cpp" line="386"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <source>ISO Invalid,please make sure you choose a vavlid image!</source>
        <translation type="vanished">ISO镜像无效，请选择正确的镜像！</translation>
    </message>
    <message>
        <location filename="../page1.cpp" line="62"/>
        <source>Start</source>
        <translation>开始制作</translation>
    </message>
    <message>
        <source>These operations needs to be verified:</source>
        <translation type="vanished">执行该动作需要root授权，您需要进行验证：</translation>
    </message>
    <message>
        <source>USB drive will be formatted,please backup your files</source>
        <translation type="vanished">制作启动盘的U盘将被格式化，请先备份好重要文件</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">授权</translation>
    </message>
    <message>
        <source>These operations needs to be verified.</source>
        <translation type="vanished">要安装或卸载软件，您需要进行验证。</translation>
    </message>
    <message>
        <source>Request authorization:</source>
        <translation type="vanished">一个程序正试图执行一个需要特权的动作。要求授权以执行该动作：</translation>
    </message>
    <message>
        <source>Password：</source>
        <translation type="vanished">输入密码：</translation>
    </message>
    <message>
        <location filename="../page1.cpp" line="317"/>
        <location filename="../page1.cpp" line="333"/>
        <location filename="../page1.cpp" line="391"/>
        <source>No USB drive available</source>
        <translation>无可用U盘</translation>
    </message>
</context>
<context>
    <name>Page2</name>
    <message>
        <location filename="../page2.cpp" line="103"/>
        <source>In production</source>
        <translation>制作中</translation>
    </message>
    <message>
        <source>Please do not remove the USB driver or power off now.</source>
        <translation type="obsolete">制作时请不要移除U盘或关机。</translation>
    </message>
    <message>
        <location filename="../page2.cpp" line="41"/>
        <location filename="../page2.cpp" line="103"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="../page2.cpp" line="113"/>
        <source>Please do not remove the USB driver or power off now</source>
        <translation>制作时请不要移除U盘或关机</translation>
    </message>
    <message>
        <location filename="../page2.cpp" line="123"/>
        <location filename="../page2.cpp" line="141"/>
        <source>Return</source>
        <translation>返回</translation>
    </message>
    <message>
        <location filename="../page2.cpp" line="124"/>
        <location filename="../page2.cpp" line="104"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="../page2.cpp" line="91"/>
        <location filename="../page2.cpp" line="142"/>
        <source>Retry</source>
        <translation>重试</translation>
    </message>
    <message>
        <location filename="../page2.cpp" line="125"/>
        <location filename="../page2.cpp" line="292"/>
        <source>Finish</source>
        <translation>制作完成</translation>
    </message>
    <message>
        <location filename="../page2.cpp" line="144"/>
        <location filename="../page2.cpp" line="289"/>
        <source>Creation Failed</source>
        <translation>制作失败，请检查后重试</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../../../../../../../usr/include/x86_64-linux-gnu/qt5/QtWidgets/qmessagebox.h" line="320"/>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</translation>
    </message>
    <message>
        <location filename="../../../../../../../usr/include/x86_64-linux-gnu/qt5/QtWidgets/qmessagebox.h" line="322"/>
        <source>Incompatible Qt Library Error</source>
        <translation>Incompatible Qt Library Error</translation>
    </message>
</context>
<context>
    <name>StyleWidget</name>
    <message>
        <source>kylin usb creator</source>
        <translation type="vanished">麒麟U盘启动器</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation type="vanished">主题</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">帮助</translation>
    </message>
    <message>
        <source>About</source>
        <translation type="vanished">关于</translation>
    </message>
    <message>
        <source>Setting</source>
        <translation type="vanished">设置</translation>
    </message>
    <message>
        <source>Quit</source>
        <translation type="vanished">退出</translation>
    </message>
</context>
<context>
    <name>T::QApplication</name>
    <message>
        <location filename="../../../../../../../usr/include/x86_64-linux-gnu/qt5/QtWidgets/qmessagebox.h" line="320"/>
        <source>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</source>
        <translation>Executable &apos;%1&apos; requires Qt %2, found Qt %3.</translation>
    </message>
    <message>
        <location filename="../../../../../../../usr/include/x86_64-linux-gnu/qt5/QtWidgets/qmessagebox.h" line="322"/>
        <source>Incompatible Qt Library Error</source>
        <translation>Incompatible Qt Library Error</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>menu</source>
        <translation type="vanished">菜单</translation>
    </message>
    <message>
        <location filename="../include/menumodule.cpp" line="40"/>
        <source>Menu</source>
        <translation>菜单</translation>
    </message>
    <message>
        <location filename="../include/menumodule.cpp" line="52"/>
        <source>Theme</source>
        <translation>主题</translation>
    </message>
    <message>
        <location filename="../include/menumodule.cpp" line="54"/>
        <location filename="../include/menumodule.cpp" line="133"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="../include/menumodule.cpp" line="56"/>
        <location filename="../include/menumodule.cpp" line="131"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="../include/menumodule.cpp" line="56"/>
        <source>Setting</source>
        <translation>设置</translation>
    </message>
    <message>
        <location filename="../include/menumodule.cpp" line="58"/>
        <location filename="../include/menumodule.cpp" line="129"/>
        <location filename="../include/menumodule.cpp" line="217"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../include/menumodule.cpp" line="65"/>
        <source>Auto</source>
        <translation>自动</translation>
    </message>
    <message>
        <location filename="../include/menumodule.cpp" line="69"/>
        <location filename="../include/menumodule.cpp" line="144"/>
        <source>Light</source>
        <translation>浅色模式</translation>
    </message>
    <message>
        <location filename="../include/menumodule.cpp" line="73"/>
        <location filename="../include/menumodule.cpp" line="149"/>
        <source>Dark</source>
        <translation>深色模式</translation>
    </message>
    <message>
        <location filename="../include/menumodule.cpp" line="87"/>
        <source>checkupdate</source>
        <translation>检查更新</translation>
    </message>
    <message>
        <location filename="../include/menumodule.cpp" line="91"/>
        <location filename="../include/menumodule.cpp" line="186"/>
        <location filename="../include/menumodule.cpp" line="270"/>
        <source>successback</source>
        <translation>建议反馈</translation>
    </message>
    <message>
        <location filename="../include/menumodule.cpp" line="95"/>
        <source>officewebsite</source>
        <translation>官方网站</translation>
    </message>
    <message>
        <location filename="../include/menumodule.cpp" line="224"/>
        <source>submit</source>
        <translation>提交</translation>
    </message>
    <message>
        <location filename="../include/menumodule.cpp" line="225"/>
        <source>Please enter the contents</source>
        <translation>请输入内容</translation>
    </message>
    <message>
        <location filename="../include/menumodule.cpp" line="256"/>
        <source>Maximum 150 words!</source>
        <translation>最多输入150个字!</translation>
    </message>
    <message>
        <location filename="../include/menumodule.cpp" line="227"/>
        <source>Please enter your feedback</source>
        <translation>请输入反馈内容</translation>
    </message>
    <message>
        <location filename="../include/menumodule.cpp" line="220"/>
        <location filename="../include/menumodule.cpp" line="244"/>
        <source>usb boot maker</source>
        <translation>U盘启动器</translation>
    </message>
    <message>
        <location filename="../include/menumodule.cpp" line="357"/>
        <source>USB Boot Maker provides system image making function.The operation process is simple and easy.You can choose ISO image and usb driver,and make boot driver with a few clicks.</source>
        <translation>U盘启动器提供系统盘制作功能，操作流程简单便捷。用户可以自行选择ISO镜像和U盘，一键轻松制作U盘启动盘。</translation>
    </message>
    <message>
        <source>close</source>
        <translation type="vanished">关闭</translation>
    </message>
    <message>
        <source>kylin usb creator</source>
        <translation type="vanished">麒麟U盘启动器</translation>
    </message>
    <message>
        <source>Kylin USB Creator provides system image making function.The operation process is simple and easy.You can choose ISO image and usb driver,and make boot driver with a few clicks.</source>
        <translation type="vanished">麒麟U盘启动器提供系统盘制作功能，操作流程简单便捷。用户可以自行选择ISO镜像和U盘，一键轻松制作U盘启动盘。</translation>
    </message>
    <message>
        <source>Kylin USB Creator provides system image making function.The operation process is simple and easy.You can choose ISO image and usb driver,and make boot driver with a few clicks</source>
        <translation type="vanished">麒麟U盘启动器提供系统盘制作功能。操作流程简单便捷。用户可以自行选择ISO镜像和U盘，一键轻松制作U盘启动器。</translation>
    </message>
    <message>
        <location filename="../include/menumodule.cpp" line="246"/>
        <source>Version: </source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="../include/menumodule.cpp" line="308"/>
        <location filename="../include/menumodule.cpp" line="326"/>
        <source>Service &amp; Support: </source>
        <translation>服务与支持团队：</translation>
    </message>
    <message>
        <source>Support: support@kylinos.cn</source>
        <translation type="vanished">支持：support@kylinos.cn</translation>
    </message>
</context>
<context>
    <name>rootAuthDialog</name>
    <message>
        <source>Input password</source>
        <translation type="vanished">请输入密码</translation>
    </message>
    <message>
        <source>please enter the password</source>
        <translation type="vanished">请输入密码</translation>
    </message>
    <message>
        <source>Wrong password!Try again</source>
        <translation type="vanished">密码错误，请重新尝试</translation>
    </message>
    <message>
        <source>Current user is not in the sudoers file,please change another account or change authority</source>
        <translation type="vanished">当前用户不在sudoer名单中，请修改账户或权限</translation>
    </message>
    <message>
        <source>Current user is not in the sudoers file,please change another account or change authority.</source>
        <translation type="vanished">当前用户不在sudoer名单中，请修改账户或权限。</translation>
    </message>
</context>
</TS>
