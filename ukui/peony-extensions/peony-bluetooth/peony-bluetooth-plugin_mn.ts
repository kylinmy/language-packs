<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>Peony::BluetoothPlugin</name>
    <message>
        <location filename="../bluetoothplugin.cpp" line="104"/>
        <source>Send from bluetooth to...</source>
        <translation>ᠯᠠᠨᠶᠠ ᠪᠡᠷ ᠹᠠᠢᠯ ᠢ᠋ ᠢᠯᠡᠬᠡᠬᠡᠳ...</translation>
    </message>
    <message>
        <location filename="../bluetoothplugin.h" line="53"/>
        <source>Peony-Qt bluetooth Extension</source>
        <translation>ᠯᠠᠨᠶᠠ ᠤᠭᠯᠤᠷᠭ᠎ᠠ ᠲᠤᠨᠤᠭ</translation>
    </message>
    <message>
        <location filename="../bluetoothplugin.h" line="54"/>
        <source>bluetooth Menu Extension.</source>
        <translation>ᠯᠠᠨᠶᠠ ᠲᠤᠪᠶᠤᠭ ᠤ᠋ᠨ ᠦᠷᠭᠡᠳᠭᠡᠯ.</translation>
    </message>
</context>
</TS>
