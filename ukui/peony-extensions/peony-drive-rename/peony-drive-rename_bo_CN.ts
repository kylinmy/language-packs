<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>Peony::DriveRename</name>
    <message>
        <location filename="../drive-rename.cpp" line="165"/>
        <location filename="../drive-rename.cpp" line="177"/>
        <source>Rename</source>
        <translation>མིང་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="177"/>
        <source>Device name:</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་གཤམ་གསལ།</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="181"/>
        <location filename="../drive-rename.cpp" line="189"/>
        <location filename="../drive-rename.cpp" line="195"/>
        <location filename="../drive-rename.cpp" line="208"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="181"/>
        <source>Renaming cannot start with a decimal point, Please re-enter!</source>
        <translation>མིང་བསྒྱུར་བ་ནི་ཆེས་ཆུང་བ་ནས་འགོ་རྩོམ་མི་ཐུབ་པས། ཡང་བསྐྱར་ནང་དུ་ཞུགས་རོགས།</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="189"/>
        <source>The device name exceeds the character limit, rename failed!</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་དེ་ཡི་གེའི་ཚད་ལས་བརྒལ་ནས་མིང་བསྒྱུར་མ་ཐུབ་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="195"/>
        <source>Renaming will unmount the device. Do you want to continue?</source>
        <translation>མིང་བསྒྱུར་ན་སྒྲིག་ཆས་འདི་མེད་པར་བཟོ་སྲིད། ཁྱོད་ཀྱིས་ད་དུང་མུ་མཐུད་དུ་རྒྱུན་འཁྱོངས་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../drive-rename.cpp" line="208"/>
        <source>The device may not support the rename operation, rename failed!</source>
        <translation>སྒྲིག་ཆས་འདིས་མིང་བསྒྱུར་བའི་བཀོལ་སྤྱོད་ལ་རྒྱབ་སྐྱོར་བྱེད་མི་སྲིད། མིང་བསྒྱུར་མ་ཐུབ་པ་རེད།</translation>
    </message>
</context>
<context>
    <name>Peony::DriveRenamePlugin</name>
    <message>
        <location filename="../driverenameplugin.h" line="21"/>
        <source>drive rename</source>
        <translation>སྒྲིག་ཆས་ཀྱི་མིང་བསྒྱུར་བ།</translation>
    </message>
</context>
</TS>
