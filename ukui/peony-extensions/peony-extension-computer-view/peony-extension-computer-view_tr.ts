<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="tr" sourcelanguage="en_GB">
<context>
    <name>ComputerItemDelegate</name>
    <message>
        <location filename="computer-view/computer-item-delegate.cpp" line="155"/>
        <source>You should mount volume first</source>
        <translation>Önce bölümü bağlamalısınız</translation>
    </message>
</context>
<context>
    <name>ComputerNetworkItem</name>
    <message>
        <location filename="computer-view/computer-network-item.cpp" line="45"/>
        <source>Network Neighborhood</source>
        <translation>Ağ Komşuları</translation>
    </message>
</context>
<context>
    <name>ComputerRemoteVolumeItem</name>
    <message>
        <location filename="computer-view/computer-remote-volume-item.cpp" line="62"/>
        <source>Remote</source>
        <translation>Uzak</translation>
    </message>
</context>
<context>
    <name>ComputerUserShareItem</name>
    <message>
        <location filename="computer-view/computer-user-share-item.cpp" line="20"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>ComputerVolumeItem</name>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="54"/>
        <source>Volume</source>
        <translation>Bölüm</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="63"/>
        <location filename="computer-view/computer-volume-item.cpp" line="115"/>
        <source>File System</source>
        <translation>Dosya Sistemi</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="179"/>
        <source>Data</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LoginRemoteFilesystem</name>
    <message>
        <location filename="login-remote-filesystem.ui" line="41"/>
        <source>Connect to Sever</source>
        <translation type="unfinished">Sunucuya bağlan</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="89"/>
        <source>server information</source>
        <translation type="unfinished">Sunucu bilgisi</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="111"/>
        <source>user information</source>
        <translation type="unfinished">Kullanıcı bilgisi</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="141"/>
        <source>tag</source>
        <translation type="unfinished">Etiket</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="196"/>
        <source>user</source>
        <translation type="unfinished">Kullanıcı</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="252"/>
        <source>password</source>
        <translation type="unfinished">Parola</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="308"/>
        <source>protocol</source>
        <translation type="unfinished">Protokol</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="351"/>
        <source>server</source>
        <translation type="unfinished">Sunucu</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="382"/>
        <source>directory</source>
        <translation type="unfinished">Dizin</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="411"/>
        <source>SAMBA</source>
        <translation type="unfinished">SAMBA</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="416"/>
        <source>FTP</source>
        <translation type="unfinished">FTP</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="445"/>
        <source>/</source>
        <translation type="unfinished">/</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="476"/>
        <source>port</source>
        <translation type="unfinished">Port</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="508"/>
        <source>20</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="513"/>
        <source>21</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="518"/>
        <source>137</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="523"/>
        <source>138</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="528"/>
        <source>139</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="533"/>
        <source>445</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="592"/>
        <source>ok</source>
        <translation type="unfinished">Tamam</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="623"/>
        <source>cancel</source>
        <translation type="unfinished">İptal</translation>
    </message>
</context>
<context>
    <name>Peony::ComputerViewContainer</name>
    <message>
        <location filename="computer-view-container.cpp" line="132"/>
        <source>Connect a server</source>
        <translation>Servere bağlan</translation>
    </message>
    <message>
        <source>sftp://, etc...</source>
        <translation type="vanished">sftp://, gibi...</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="150"/>
        <location filename="computer-view-container.cpp" line="155"/>
        <source>Unmount</source>
        <translation>Bağlantıyı kes</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="162"/>
        <source>Eject</source>
        <translation type="unfinished">Çıkar</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="188"/>
        <source>format</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="206"/>
        <location filename="computer-view-container.cpp" line="223"/>
        <source>Property</source>
        <translation>Özellik</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="208"/>
        <source>You have to mount this volume first</source>
        <translation>Önce bu bölümü bağlamalısınız</translation>
    </message>
</context>
<context>
    <name>Peony::DriveRename</name>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="165"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="177"/>
        <source>Rename</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="177"/>
        <source>Device name:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="181"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="189"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="195"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="208"/>
        <source>Warning</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="181"/>
        <source>Renaming cannot start with a decimal point, Please re-enter!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="189"/>
        <source>The device name exceeds the character limit, rename failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="195"/>
        <source>Renaming will unmount the device. Do you want to continue?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="208"/>
        <source>The device may not support the rename operation, rename failed!</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="peony-computer-view-plugin.h" line="42"/>
        <source>Computer View</source>
        <translation>Bilgisayar Görünümü</translation>
    </message>
    <message>
        <location filename="peony-computer-view-plugin.h" line="43"/>
        <source>Show drives, network and personal directories.</source>
        <translation>Sürücüleri, ağı ve kişisel dizinleri göster.</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="674"/>
        <source>One or more programs prevented the unmount operation.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="675"/>
        <location filename="computer-view/computer-volume-item.cpp" line="679"/>
        <location filename="computer-view/computer-volume-item.cpp" line="683"/>
        <source>Unmount failed</source>
        <translation type="unfinished">Bağlantıyı kesme başarısız</translation>
    </message>
    <message>
        <source>Error: %1
Do you want to unmount forcely?</source>
        <translation type="obsolete">Hata: %1
        Zorla ayrılmak ister misin?</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="371"/>
        <source>It need to synchronize before operating the device,place wait!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="646"/>
        <source>The device has been mount successfully!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="667"/>
        <source>Data synchronization is complete,the device has been unmount successfully!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="675"/>
        <location filename="computer-view/computer-volume-item.cpp" line="683"/>
        <source>Error: %1
</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="678"/>
        <source>Unable to unmount it, you may need to close some programs, such as: GParted etc.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="679"/>
        <source>%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="706"/>
        <location filename="computer-view/computer-volume-item.cpp" line="721"/>
        <source>Eject failed</source>
        <translation type="unfinished">Çıkarma başarısız</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">İptal</translation>
    </message>
    <message>
        <source>Eject Anyway</source>
        <translation type="obsolete">Yine de Çıkar</translation>
    </message>
</context>
</TS>
