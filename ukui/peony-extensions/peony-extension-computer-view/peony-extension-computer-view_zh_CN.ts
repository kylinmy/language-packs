<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN" sourcelanguage="en_GB">
<context>
    <name>ComputerItemDelegate</name>
    <message>
        <location filename="computer-view/computer-item-delegate.cpp" line="155"/>
        <source>You should mount volume first</source>
        <translation>需要首先挂载分区</translation>
    </message>
</context>
<context>
    <name>ComputerNetworkItem</name>
    <message>
        <location filename="computer-view/computer-network-item.cpp" line="45"/>
        <source>Network Neighborhood</source>
        <translation>网上邻居</translation>
    </message>
</context>
<context>
    <name>ComputerRemoteVolumeItem</name>
    <message>
        <location filename="computer-view/computer-remote-volume-item.cpp" line="62"/>
        <source>Remote</source>
        <translation>远程目录</translation>
    </message>
</context>
<context>
    <name>ComputerUserShareItem</name>
    <message>
        <source>User Share</source>
        <translation type="vanished">本机共享</translation>
    </message>
    <message>
        <location filename="computer-view/computer-user-share-item.cpp" line="20"/>
        <source>Data</source>
        <translation>数据盘</translation>
    </message>
</context>
<context>
    <name>ComputerVolumeItem</name>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="54"/>
        <source>Volume</source>
        <translation>本地分区</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="63"/>
        <location filename="computer-view/computer-volume-item.cpp" line="115"/>
        <source>File System</source>
        <translation>文件系统</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="179"/>
        <source>Data</source>
        <translation>数据盘</translation>
    </message>
</context>
<context>
    <name>LoginRemoteFilesystem</name>
    <message>
        <location filename="login-remote-filesystem.ui" line="41"/>
        <source>Connect to Sever</source>
        <translation>连接到服务器</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="89"/>
        <source>server information</source>
        <translation>服务器信息</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="111"/>
        <source>user information</source>
        <translation>用户信息</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="141"/>
        <source>tag</source>
        <translation>标签</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="196"/>
        <source>user</source>
        <translation>用户名</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="252"/>
        <source>password</source>
        <translation>密码</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="308"/>
        <source>protocol</source>
        <translation>协议</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="351"/>
        <source>server</source>
        <translation>服务端地址</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="382"/>
        <source>directory</source>
        <translation>目录</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="411"/>
        <source>SAMBA</source>
        <translation></translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="416"/>
        <source>FTP</source>
        <translation>FTP</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="445"/>
        <source>/</source>
        <translation>/</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="476"/>
        <source>port</source>
        <translation>端口</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="508"/>
        <source>20</source>
        <translation>20</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="513"/>
        <source>21</source>
        <translation>21</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="518"/>
        <source>137</source>
        <translation>137</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="523"/>
        <source>138</source>
        <translation>138</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="528"/>
        <source>139</source>
        <translation>139</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="533"/>
        <source>445</source>
        <translation>445</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="592"/>
        <source>ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="login-remote-filesystem.ui" line="623"/>
        <source>cancel</source>
        <translation>取消</translation>
    </message>
</context>
<context>
    <name>Peony::ComputerViewContainer</name>
    <message>
        <location filename="computer-view-container.cpp" line="132"/>
        <source>Connect a server</source>
        <translation>连接远程服务器</translation>
    </message>
    <message>
        <source>sftp://, etc...</source>
        <translation type="vanished">如sftp://...</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>OK</source>
        <translation type="vanished">确定</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="150"/>
        <location filename="computer-view-container.cpp" line="155"/>
        <source>Unmount</source>
        <translation>卸载</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="162"/>
        <source>Eject</source>
        <translation>弹出</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="188"/>
        <source>format</source>
        <translation>格式化</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="206"/>
        <location filename="computer-view-container.cpp" line="223"/>
        <source>Property</source>
        <translation>属性</translation>
    </message>
    <message>
        <location filename="computer-view-container.cpp" line="208"/>
        <source>You have to mount this volume first</source>
        <translation>需要首先挂载分区</translation>
    </message>
</context>
<context>
    <name>Peony::DriveRename</name>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="165"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="177"/>
        <source>Rename</source>
        <translation>重命名</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="177"/>
        <source>Device name:</source>
        <translation>设备名称：</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="181"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="189"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="195"/>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="208"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="181"/>
        <source>Renaming cannot start with a decimal point, Please re-enter!</source>
        <translation>重命名不能以小数点开始，请重新输入!</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="189"/>
        <source>The device name exceeds the character limit, rename failed!</source>
        <translation>设备名称超过了字符限制，重命名失败!</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="195"/>
        <source>Renaming will unmount the device. Do you want to continue?</source>
        <translation>重命名将会卸载设备，是否继续？</translation>
    </message>
    <message>
        <location filename="../peony-drive-rename/drive-rename.cpp" line="208"/>
        <source>The device may not support the rename operation, rename failed!</source>
        <translation>可能设备不支持重命名操作，重命名失败！</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="peony-computer-view-plugin.h" line="42"/>
        <source>Computer View</source>
        <translation>电脑视图</translation>
    </message>
    <message>
        <location filename="peony-computer-view-plugin.h" line="43"/>
        <source>Show drives, network and personal directories.</source>
        <translation>显示设备分区, 网络目录和个人目录.</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="674"/>
        <source>One or more programs prevented the unmount operation.</source>
        <translation>一个或多个程序阻止卸载操作。</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="675"/>
        <location filename="computer-view/computer-volume-item.cpp" line="679"/>
        <location filename="computer-view/computer-volume-item.cpp" line="683"/>
        <source>Unmount failed</source>
        <translation>卸载失败</translation>
    </message>
    <message>
        <source>Error: %1
Do you want to unmount forcely?</source>
        <translation type="vanished">错误：%1
是否强制卸载？</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="371"/>
        <source>It need to synchronize before operating the device,place wait!</source>
        <translation>操作设备前需要同步数据，请稍等！</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="646"/>
        <source>The device has been mount successfully!</source>
        <translation>设备挂载成功！</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="667"/>
        <source>Data synchronization is complete,the device has been unmount successfully!</source>
        <translation>数据同步完成，设备已经成功卸载！</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="675"/>
        <location filename="computer-view/computer-volume-item.cpp" line="683"/>
        <source>Error: %1
</source>
        <translation>错误: %1\n
</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="678"/>
        <source>Unable to unmount it, you may need to close some programs, such as: GParted etc.</source>
        <translation>无法卸载，您可能需要先关闭一些程序，如分区编辑器等。</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="679"/>
        <source>%1</source>
        <translation>%1</translation>
    </message>
    <message>
        <location filename="computer-view/computer-volume-item.cpp" line="706"/>
        <location filename="computer-view/computer-volume-item.cpp" line="721"/>
        <source>Eject failed</source>
        <translation>弹出失败</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">取消</translation>
    </message>
    <message>
        <source>Eject Anyway</source>
        <translation type="vanished">无论如何弹出</translation>
    </message>
</context>
</TS>
