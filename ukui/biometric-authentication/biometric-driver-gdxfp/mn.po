# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: biometric-driver-gdxfp 0.1.0\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-01-18 09:38+0800\n"
"PO-Revision-Date: 2022-11-25 13:20+0000\n"
"Last-Translator: bolor2022 <759085099@qq.com>\n"
"Language-Team: Mongolian <http://weblate.openkylin.top/projects/"
"biometric-authentication-master/biometric-driver-gdxfp/mn/>\n"
"Language: mn\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 4.12.1-dev\n"

#: ../libgdxfp/gdxfp_driver.c:186
#, c-format
msgid "Detecting %s device ...\n"
msgstr "%s ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠶᠢ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂᠂᠂\n"

#: ../libgdxfp/gdxfp_driver.c:290
#, c-format
msgid "There is %s fingerprint device detected\n"
msgstr "%s ᠬᠤᠷᠤᠭᠤᠨ ᠤ ᠤᠷᠤᠮ ᠤᠨ ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠤᠯᠪᠠ\n"

#: ../libgdxfp/gdxfp_driver.c:294
#, c-format
msgid "No %s device detected\n"
msgstr "%s ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠶᠢ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠣᠯᠣᠭᠰᠠᠨ ᠦᠬᠡᠢ\n"

#: ../libgdxfp/gdxfp_driver.c:366
msgid "device close failed"
msgstr "ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠬᠠᠭᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ"

#: ../libgdxfp/gdxfp_driver.c:467 ../libgdxfp/gdxfp_driver.c:475
msgid "uuid file write error\n"
msgstr "uuid ᠹᠠᠢᠯ ᠢ ᠪᠢᠴᠢᠭᠰᠡᠨ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ\n"

#: ../libgdxfp/gdxfp_driver.c:481 ../libgdxfp/gdxfp_driver.c:608
#: ../libgdxfp/gdxfp_driver.c:693
#, c-format
msgid "Please press your fingers"
msgstr "ᠬᠤᠷᠤᠭᠤ ᠪᠡᠨ ᠳᠠᠷᠤᠭᠠᠷᠠᠢ"

#: ../libgdxfp/gdxfp_driver.c:513 ../libgdxfp/gdxfp_driver.c:636
#: ../libgdxfp/gdxfp_driver.c:736
msgid "The device is busy"
msgstr "ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠶᠠᠭᠠᠷᠠᠤ ᠪᠠᠢᠨ᠎ᠠ"

#: ../libgdxfp/gdxfp_driver.c:817 ../libgdxfp/gdxfp_driver.c:1020
msgid "file read error\n"
msgstr "ᠹᠠᠢᠯ ᠢ ᠤᠩᠰᠢᠭᠰᠠᠨ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ\n"

#: ../libgdxfp/gdxfp_driver.c:920
#, c-format
msgid "Device %s[%d] received interrupt request\n"
msgstr "%s %d ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠳᠠᠰᠤᠯᠳᠠᠯ ᠭᠤᠶᠤᠴᠢᠯᠠᠯᠳᠠ ᠵᠢ ᠳᠤᠰᠪᠠ\n"

#: ../libgdxfp/gdxfp_driver.c:1003
msgid "Unable to find feature that require renaming\n"
msgstr "ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠬᠦ ᠱᠠᠭᠠᠷᠳᠠᠯᠭ᠎ᠠ ᠲᠠᠢ ᠣᠨᠴᠠᠯᠢᠭ ᠢ ᠡᠷᠢᠵᠤ ᠣᠯᠣᠭᠰᠠᠨ ᠦᠬᠡᠢ\n"

#: ../libgdxfp/gdxfp_driver.c:1006
#, c-format
msgid "There are %d feature samples to renaming failed, please try again\n"
msgstr ""
"%d ᠣᠨᠴᠠᠯᠢᠭ ᠤᠨ ᠳᠤᠷᠰᠢᠴᠠ ᠠᠪᠬᠤ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠦᠯᠳᠡ ᠢᠯᠠᠭᠳᠠᠪᠠ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ\n"

#: ../libgdxfp/gdxfp_driver.c:1033
msgid "The new name already exists\n"
msgstr "ᠳᠤᠰ ᠰᠢᠨ᠎ᠡ ᠨᠡᠷ᠎ᠡ ᠤᠷᠤᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ\n"

#: ../libgdxfp/gdxfp_driver.c:1058
msgid "Exceed the maximum enroll limit"
msgstr "ᠣᠷᠣᠭᠤᠯᠬᠤ ᠬᠢᠵᠠᠭᠠᠷᠯᠠᠯᠳᠠ ᠠᠴᠠ ᠬᠡᠳᠦᠷᠡᠪᠡ"

#: ../libgdxfp/gdxfp_internal.c:317
#, c-format
msgid "Fingerprint enroll error"
msgstr "ᠬᠤᠷᠤᠭᠤᠨ ᠤ ᠤᠷᠤᠮ ᠢ ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠪᠤᠷᠤᠭᠤᠳᠠᠪᠠ"

#: ../libgdxfp/gdxfp_internal.c:323
#, c-format
msgid "Fingerprint enroll successfully"
msgstr "ᠬᠤᠷᠤᠭᠤᠨ ᠤ ᠤᠷᠤᠮ ᠢ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠪᠠ"

#: ../libgdxfp/gdxfp_internal.c:329
#, c-format
msgid "Fingerprint enroll failed"
msgstr "ᠬᠤᠷᠤᠭᠤᠨ ᠤ ᠤᠷᠤᠮ ᠢ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ"

#: ../libgdxfp/gdxfp_internal.c:335
#, c-format
msgid ""
"Fingerprint collection is successful, please continue to press other parts "
"of the finger"
msgstr ""
"ᠬᠤᠷᠤᠭᠤᠨ ᠤ ᠤᠷᠤᠮ ᠤᠨ ᠳᠤᠷᠰᠢᠴᠠ ᠵᠢ ᠠᠪᠴᠤ ᠴᠢᠳᠠᠪᠠ᠂ᠬᠤᠷᠤᠭᠤᠨ ᠤ ᠪᠡᠨ ᠪᠤᠰᠤᠳ ᠬᠡᠰᠡᠭ ᠢ "
"ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠨ ᠳᠠᠷᠤᠭᠠᠷᠠᠢ"

#: ../libgdxfp/gdxfp_internal.c:340
#, c-format
msgid "The current finger already exists, please enroll it with another finger"
msgstr ""
"ᠤᠳᠤᠬᠢ ᠬᠤᠷᠤᠭᠤᠨ ᠤ ᠤᠷᠤᠮ ᠤᠷᠤᠰᠢᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠪᠤᠰᠤᠳ ᠬᠤᠷᠤᠭᠤᠨ ᠤ ᠤᠷᠤᠮ ᠵᠢᠡᠨ ᠳᠠᠬᠢᠵᠤ "
"ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ"

#: ../libgdxfp/gdxfp_internal.c:345
#, c-format
msgid "The fingerprint image is too bad, Please press your finger again"
msgstr "ᠬᠤᠷᠤᠨ ᠤ ᠤᠷᠤᠮ ᠤᠨ ᠢᠮᠡᠭᠧ ᠵᠢᠷᠤᠭ ᠪᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ᠬᠤᠷᠤᠭᠤ ᠪᠡᠨ ᠳᠠᠬᠢᠵᠤ ᠳᠠᠷᠤᠭᠠᠷᠠᠢ"

#: ../libgdxfp/gdxfp_internal.c:350
#, c-format
msgid "The finger pressing time is too short，Please press your finger again"
msgstr "ᠬᠤᠷᠤᠭᠤ ᠳᠠᠷᠤᠭᠰᠠᠨ ᠴᠠᠭ ᠬᠡᠳᠦ ᠪᠤᠭᠤᠨᠢ᠂ᠬᠤᠷᠤᠭᠤ ᠪᠡᠨ ᠳᠠᠬᠢᠵᠤ ᠳᠠᠷᠤᠭᠠᠷᠠᠢ"

#: ../libgdxfp/gdxfp_internal.c:355
#, c-format
msgid "Duplicate fingerprint template generated"
msgstr "ᠳᠠᠪᠬᠤᠷᠳᠠᠭᠰᠠᠨ ᠬᠤᠷᠤᠭᠤᠨ ᠤ ᠤᠷᠤᠮ ᠤᠨ ᠬᠡᠮ ᠬᠠᠪᠳᠠᠰᠤ ᠵᠢ ᠡᠬᠦᠰᠬᠡᠪᠡ"

#: ../libgdxfp/gdxfp_internal.c:361
#, c-format
msgid "Please press the different positions of your fingers"
msgstr "ᠬᠤᠷᠤᠭᠤ ᠪᠡᠨ ᠠᠳᠠᠯᠢ ᠪᠤᠰᠤ ᠪᠠᠢᠷᠢᠨ ᠳ᠋ᠤ ᠳᠠᠷᠤᠭᠠᠷᠠᠢ"

#: ../libgdxfp/gdxfp_internal.c:388 ../libgdxfp/gdxfp_internal.c:445
#: ../libgdxfp/gdxfp_internal.c:478
#, c-format
msgid "Fingerprint identify successfully"
msgstr "ᠬᠤᠷᠤᠭᠤᠨ ᠤ ᠤᠷᠤᠮ ᠢ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠵᠤ ᠴᠢᠳᠠᠪᠠ"

#: ../libgdxfp/gdxfp_internal.c:394 ../libgdxfp/gdxfp_internal.c:439
#: ../libgdxfp/gdxfp_internal.c:472
#, c-format
msgid "Fingerprint identify failed"
msgstr "ᠬᠤᠷᠤᠭᠤᠨ ᠤ ᠤᠷᠤᠮ ᠢ ᠢᠯᠭᠠᠨ ᠳᠠᠨᠢᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ"

#: ../libgdxfp/gdxfp_internal.c:400
#, c-format
msgid "Device initialization failed"
msgstr "ᠳᠦᠬᠦᠬᠡᠷᠦᠮᠵᠢ ᠵᠢ ᠠᠩᠬᠠᠵᠢᠭᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ"

#: ../libgdxfp/gdxfp_internal.c:406
#, c-format
msgid "The image is not available"
msgstr "ᠬᠤᠷᠤᠭᠤᠨ ᠤ ᠤᠷᠤᠮ ᠤᠨ ᠢᠮᠡᠭ᠌ᠧ ᠵᠢᠷᠤᠭ ᠢ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠳ᠋ᠤ ᠠᠯᠳᠠᠭ᠎ᠠ ᠭᠠᠷᠪᠠ"

#: ../libgdxfp/gdxfp_internal.c:412
#, c-format
msgid "The finger pressing time is too short"
msgstr "ᠬᠤᠷᠤᠭᠤ ᠳᠠᠷᠤᠭᠰᠠᠨ ᠴᠠᠭ ᠬᠡᠳᠦ ᠪᠤᠭᠤᠨᠢ᠂ ᠬᠤᠷᠤᠭᠤ ᠪᠡᠨ ᠳᠠᠬᠢᠵᠤ ᠳᠠᠷᠤᠭᠠᠷᠠᠢ"

#: ../id_tables.h:6
msgid "Goodix Fingerprint Module"
msgstr "ᠬᠤᠧᠠ ᠸᠧᠢ ᠬᠤᠷᠤᠭᠤᠨ ᠤ ᠤᠷᠤᠮ ᠤᠨ ᠬᠡᠪ ᠬᠠᠪᠳᠠᠰᠤ"

#~ msgid "enroll timeout"
#~ msgstr "录入超时"

#~ msgid "Please keep the image clear with your fingers"
#~ msgstr "请保持清洁的手指"

#~ msgid "Cancel the current authentication process"
#~ msgstr "取消当前认证进程"

#~ msgid "get feature list failed"
#~ msgstr "获取指纹特征列表失败"
