<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="aboutdialog.ui" line="13"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <location filename="aboutdialog.cpp" line="40"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="aboutdialog.cpp" line="62"/>
        <source>Backup &amp; Restore</source>
        <translation>备份还原工具</translation>
    </message>
    <message>
        <location filename="aboutdialog.cpp" line="73"/>
        <source>version: </source>
        <translation>版本：</translation>
    </message>
    <message>
        <location filename="aboutdialog.cpp" line="87"/>
        <source>The backup tool is a tool that supports system backup and data backup. When the user data is damaged or the system is attacked, the tool can flexibly restore the status of the backup node. A lot of optimization and innovation have been carried out for domestic hardware and software platforms.</source>
        <translation>备份还原工具是一款支持系统备份还原和数据备份还原的工具，当用户数据损坏或系统遭受攻击时能够通过该工具灵活的还原到备份节点的状态。针对国产软硬件平台开展了大量的优化和创新。</translation>
    </message>
    <message>
        <location filename="aboutdialog.cpp" line="99"/>
        <source>Service &amp; Support: %1</source>
        <translation>服务与支持团队：%1</translation>
    </message>
</context>
<context>
    <name>BackupListWidget</name>
    <message>
        <location filename="component/backuplistwidget.cpp" line="102"/>
        <source>File drag and drop area</source>
        <translation>拖放文件夹识别路径</translation>
    </message>
</context>
<context>
    <name>BackupPointListDialog</name>
    <message>
        <location filename="backuppointlistdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="34"/>
        <source>Backup Name</source>
        <translation>备份名称</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="34"/>
        <source>UUID</source>
        <translation>备份标识</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="34"/>
        <source>Backup Time</source>
        <translation>备份时间</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="34"/>
        <source>Backup Size</source>
        <translation>备份大小</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="34"/>
        <source>Position</source>
        <translation>备份位置</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="87"/>
        <source>No Backup</source>
        <translation>无备份</translation>
    </message>
</context>
<context>
    <name>BackupPositionSelectDialog</name>
    <message>
        <location filename="component/backuppositionselectdialog.cpp" line="8"/>
        <source>Please select a path</source>
        <translation>请选择一个路径</translation>
    </message>
</context>
<context>
    <name>DataBackup</name>
    <message>
        <location filename="module/databackup.cpp" line="72"/>
        <source>Data Backup</source>
        <translation>数据备份</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="93"/>
        <source>Only files in the /home, /root, and /data directories can be backed up</source>
        <translation>仅支持备份/home、/root、/data目录下的文件</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="95"/>
        <source>Only files in the /home, /root, and /data/usershare directories can be backed up</source>
        <translation>仅支持备份/home、/root、/data/usershare目录下的文件</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="106"/>
        <source>Multi-Spot</source>
        <translation>多点还原</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="113"/>
        <source>Security</source>
        <translation>安全可靠</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="120"/>
        <source>Protect Data</source>
        <translation>防止数据丢失</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="127"/>
        <source>Convenient</source>
        <translation>便捷快速</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="133"/>
        <source>Start Backup</source>
        <translation>开始备份</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="153"/>
        <source>Update Backup</source>
        <translation>备份更新</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="190"/>
        <source>Backup Management &gt;&gt;</source>
        <translation>备份管理 &gt;&gt;</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="264"/>
        <source>Please select backup position</source>
        <translation>请选择备份位置</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="412"/>
        <location filename="module/databackup.cpp" line="770"/>
        <source>local default path : </source>
        <translation>本地默认路径：</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="415"/>
        <location filename="module/databackup.cpp" line="773"/>
        <source>removable devices path : </source>
        <translation>移动设备：</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="294"/>
        <location filename="module/databackup.cpp" line="610"/>
        <source>Select backup data</source>
        <translation>选择备份数据</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="326"/>
        <location filename="module/databackup.cpp" line="642"/>
        <source>Add</source>
        <translation>添加</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="345"/>
        <location filename="module/databackup.cpp" line="661"/>
        <source>Select</source>
        <translation>选择</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="506"/>
        <location filename="module/databackup.cpp" line="728"/>
        <source>Please select file to backup</source>
        <translation>请选择备份文件</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="375"/>
        <location filename="module/databackup.cpp" line="690"/>
        <location filename="module/databackup.cpp" line="936"/>
        <location filename="module/databackup.cpp" line="1298"/>
        <source>Back</source>
        <translation>上一步</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="282"/>
        <source>Browse...</source>
        <translation>浏览...</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="317"/>
        <location filename="module/databackup.cpp" line="633"/>
        <source>Clear</source>
        <translation>清空</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="384"/>
        <location filename="module/databackup.cpp" line="699"/>
        <location filename="module/databackup.cpp" line="949"/>
        <location filename="module/databackup.cpp" line="1307"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="425"/>
        <location filename="module/databackup.cpp" line="462"/>
        <location filename="module/databackup.cpp" line="765"/>
        <source>customize path : </source>
        <translation>自定义路径：</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="582"/>
        <source>Default backup location</source>
        <translation>默认备份位置</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="837"/>
        <location filename="module/databackup.cpp" line="1192"/>
        <location filename="module/databackup.cpp" line="1441"/>
        <location filename="module/databackup.cpp" line="1720"/>
        <source>checking</source>
        <translation>环境检测</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="840"/>
        <location filename="module/databackup.cpp" line="1195"/>
        <location filename="module/databackup.cpp" line="1444"/>
        <location filename="module/databackup.cpp" line="1723"/>
        <source>preparing</source>
        <translation>备份准备</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="843"/>
        <location filename="module/databackup.cpp" line="1198"/>
        <location filename="module/databackup.cpp" line="1447"/>
        <location filename="module/databackup.cpp" line="1726"/>
        <source>backuping</source>
        <translation>备份中</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="846"/>
        <location filename="module/databackup.cpp" line="1201"/>
        <location filename="module/databackup.cpp" line="1450"/>
        <location filename="module/databackup.cpp" line="1729"/>
        <source>finished</source>
        <translation>备份完成</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="960"/>
        <source>Recheck</source>
        <translation>重新检测</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="991"/>
        <source>Checking, wait a moment ...</source>
        <translation>正在检测，请稍等...</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="997"/>
        <source>Do not perform other operations during backup to avoid data loss</source>
        <translation>备份过程中不要做其它操作，以防数据丢失</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1000"/>
        <source>Check whether the remaining capacity of the backup partition is sufficient</source>
        <translation>检测备份位置空间是否充足···</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1003"/>
        <source>Check whether the remaining capacity of the removable device is sufficient</source>
        <translation>检测移动设备空间是否充足···</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1022"/>
        <source>Check success</source>
        <translation>检测成功</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1024"/>
        <source>The storage for backup is enough</source>
        <translation>备份空间充足</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1029"/>
        <source>Make sure the computer is plugged in or the battery level is above 60%</source>
        <translation>请确保电脑已连接电源或电量超过60%</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1041"/>
        <source>Check failure</source>
        <translation>检测失败</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1109"/>
        <location filename="module/databackup.cpp" line="1603"/>
        <source>Program lock failed, please retry</source>
        <translation>程序锁定失败，请重试</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1111"/>
        <location filename="module/databackup.cpp" line="1605"/>
        <source>There may be other backups or restores being performed</source>
        <translation>可能有其它备份/还原等任务在执行</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1115"/>
        <location filename="module/databackup.cpp" line="1609"/>
        <source>Unsupported task type</source>
        <translation>不支持的任务类型</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1117"/>
        <location filename="module/databackup.cpp" line="1611"/>
        <source>No processing logic was found in the service</source>
        <translation>没有找到相应的处理逻辑</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1121"/>
        <location filename="module/databackup.cpp" line="1615"/>
        <source>Failed to mount the backup partition</source>
        <translation>备份分区挂载失败</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1123"/>
        <location filename="module/databackup.cpp" line="1617"/>
        <source>Check whether there is a backup partition</source>
        <translation>检查是否有备份分区</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1127"/>
        <source>The filesystem of device is vfat format</source>
        <translation>移动设备的文件系统是vfat格式</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1129"/>
        <source>Please change filesystem format to ext3、ext4 or ntfs</source>
        <translation>请换成ext3、ext4、ntfs等文件系统格式</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1133"/>
        <source>The device is read only</source>
        <translation>移动设备是只读挂载的</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1135"/>
        <source>Please chmod to rw</source>
        <translation>请修改为读写模式</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1139"/>
        <location filename="module/databackup.cpp" line="1621"/>
        <source>The storage for backup is not enough</source>
        <translation>备份空间不足</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1141"/>
        <location filename="module/databackup.cpp" line="1623"/>
        <source>Retry after release space</source>
        <translation>建议释放空间后重试</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1145"/>
        <location filename="module/databackup.cpp" line="1627"/>
        <source>Other backup or restore task is being performed</source>
        <translation>其它备份还原等操作正在执行</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1147"/>
        <location filename="module/databackup.cpp" line="1629"/>
        <source>Please try again later</source>
        <translation>请稍后重试</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1227"/>
        <source>Backup Name</source>
        <translation>备份名称</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1272"/>
        <location filename="module/databackup.cpp" line="1322"/>
        <source>Name already exists</source>
        <translation>名称已存在</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1508"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1531"/>
        <source>Do not use computer in case of data loss</source>
        <translation>请勿使用电脑，以防数据丢失</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1635"/>
        <source>Failed to create the backup point directory</source>
        <translation>创建备份目录失败</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1637"/>
        <source>Please check backup partition permissions</source>
        <translation>请检查备份分区权限</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1641"/>
        <source>The backup had been canceled</source>
        <translation>备份已取消</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1643"/>
        <source>Re-initiate the backup if necessary</source>
        <translation>如需要可重新进行备份</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1676"/>
        <source>An error occurred during backup</source>
        <translation>备份期间发生错误</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1678"/>
        <source>Error messages refer to log file : /var/log/backup.log</source>
        <translation>错误信息请参考日志文件：/var/log/backup.log</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1810"/>
        <source>Home Page</source>
        <translation>返回首页</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1818"/>
        <source>Retry</source>
        <translation>再试一次</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1844"/>
        <source>The backup is successful</source>
        <translation>备份成功</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1859"/>
        <source>The backup is failed</source>
        <translation>备份失败</translation>
    </message>
</context>
<context>
    <name>DataRestore</name>
    <message>
        <location filename="module/datarestore.cpp" line="59"/>
        <source>Data Restore</source>
        <translation>数据还原</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="77"/>
        <source>Backed up first, then can be restored</source>
        <translation>必须先进行数据备份，否则无法进行数据还原操作</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="88"/>
        <source>Fast Recovery</source>
        <translation>快速恢复</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="95"/>
        <source>Security</source>
        <translation>安全可靠</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="102"/>
        <source>Protect Data</source>
        <translation>解决数据丢失</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="109"/>
        <source>Independent</source>
        <translation>自主操作</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="115"/>
        <source>Start Restore</source>
        <translation>开始还原</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="240"/>
        <location filename="module/datarestore.cpp" line="566"/>
        <location filename="module/datarestore.cpp" line="802"/>
        <source>checking</source>
        <translation>环境检测</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="244"/>
        <location filename="module/datarestore.cpp" line="570"/>
        <location filename="module/datarestore.cpp" line="806"/>
        <source>restoring</source>
        <translation>还原中</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="248"/>
        <location filename="module/datarestore.cpp" line="574"/>
        <location filename="module/datarestore.cpp" line="810"/>
        <source>finished</source>
        <translation>还原完成</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="329"/>
        <source>Back</source>
        <translation>上一步</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="337"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="348"/>
        <source>Recheck</source>
        <translation>重新检测</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="385"/>
        <source>Checking, wait a moment ...</source>
        <translation>正在检测，请稍等...</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="391"/>
        <source>Check whether the restore environment meets the requirements</source>
        <translation>检查恢复环境是否符合要求</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="393"/>
        <source>Do not perform other operations during restore to avoid data loss</source>
        <translation>还原期间不要做其它操作，以防数据丢失</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="411"/>
        <source>Check success</source>
        <translation>检测成功</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="413"/>
        <location filename="module/datarestore.cpp" line="614"/>
        <source>Do not use computer in case of data loss</source>
        <translation>请勿使用电脑，以防数据丢失</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="418"/>
        <source>Make sure the computer is plugged in or the battery level is above 60%</source>
        <translation>请确保电脑已连接电源或电量超过60%</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="430"/>
        <source>Check failure</source>
        <translation>检测失败</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="503"/>
        <location filename="module/datarestore.cpp" line="695"/>
        <source>Program lock failed, please retry</source>
        <translation>程序锁定失败，请重试</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="505"/>
        <location filename="module/datarestore.cpp" line="697"/>
        <source>There may be other backups or restores being performed</source>
        <translation>可能有其它备份/还原等任务在执行</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="509"/>
        <location filename="module/datarestore.cpp" line="701"/>
        <source>Unsupported task type</source>
        <translation>不支持的任务类型</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="511"/>
        <location filename="module/datarestore.cpp" line="703"/>
        <source>No processing logic was found in the service</source>
        <translation>没有找到相应的处理逻辑</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="515"/>
        <location filename="module/datarestore.cpp" line="707"/>
        <source>The .user.txt file does not exist</source>
        <translation>.user.txt文件不存在</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="517"/>
        <location filename="module/datarestore.cpp" line="523"/>
        <location filename="module/datarestore.cpp" line="529"/>
        <location filename="module/datarestore.cpp" line="709"/>
        <location filename="module/datarestore.cpp" line="715"/>
        <location filename="module/datarestore.cpp" line="721"/>
        <source>Backup points may be corrupted</source>
        <translation>备份点可能被损坏</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="521"/>
        <location filename="module/datarestore.cpp" line="713"/>
        <source>The .exclude.user.txt file does not exist</source>
        <translation>.exclude.user.txt文件不存在</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="527"/>
        <location filename="module/datarestore.cpp" line="719"/>
        <source>The backup point data directory does not exist</source>
        <translation>备份点数据目录不存在</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="725"/>
        <source>Failed to rsync /boot/efi</source>
        <translation>同步/boot/efi失败</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="727"/>
        <source>Check the mounting mode of the /boot/efi partition</source>
        <translation>请检查/boot/efi分区挂载方式</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="731"/>
        <source>Failed to prepare the restore directory</source>
        <translation>还原目录准备失败</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="733"/>
        <source>Refer to log :/var/log/backup.log for more information</source>
        <translation>更多信息请参考日志/var/log/backup.log</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="766"/>
        <source>An error occurred during restore</source>
        <translation>还原时发生错误</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="768"/>
        <source>Error messages refer to log file : /var/log/backup.log</source>
        <translation>错误信息请参考日志文件：/var/log/backup.log</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="884"/>
        <source>Home Page</source>
        <translation>返回首页</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="892"/>
        <source>Retry</source>
        <translation>再试一次</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="898"/>
        <source>Reboot System</source>
        <translation>重启系统</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="932"/>
        <source>Successfully restoring the data</source>
        <translation>还原成功</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="940"/>
        <source>The system needs to reboot. Otherwise, some tools cannot be used.</source>
        <translation>系统需要重启，否则某些工具可能无法使用</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="953"/>
        <source>Restoring the data failed</source>
        <translation>还原失败</translation>
    </message>
</context>
<context>
    <name>DeleteBackupDialog</name>
    <message>
        <location filename="deletebackupdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>对话框</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="44"/>
        <location filename="deletebackupdialog.cpp" line="45"/>
        <source>Please wait while data is being removed</source>
        <translation>正在删除数据，请稍候</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="65"/>
        <source>Removing backup point...</source>
        <translation>正在删除备份点...</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="82"/>
        <source>Ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="128"/>
        <source>Other backup or restore task is being performed</source>
        <translation>其它备份还原等操作正在执行</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="169"/>
        <source>Program lock failed, please retry</source>
        <translation>程序锁定失败，请重试</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="173"/>
        <source>Unsupported task type</source>
        <translation>不支持的任务类型</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="203"/>
        <source>Deleted backup successfully.</source>
        <translation>删除备份成功。</translation>
    </message>
    <message>
        <location filename="deletebackupdialog.cpp" line="205"/>
        <source>Failed to delete backup.</source>
        <translation>删除备份失败。</translation>
    </message>
</context>
<context>
    <name>FuncTypeConverter</name>
    <message>
        <location filename="functypeconverter.cpp" line="34"/>
        <source>System Backup</source>
        <translation>系统备份</translation>
    </message>
    <message>
        <location filename="functypeconverter.cpp" line="37"/>
        <source>System Recovery</source>
        <translation>系统还原</translation>
    </message>
    <message>
        <location filename="functypeconverter.cpp" line="40"/>
        <source>Data Backup</source>
        <translation>数据备份</translation>
    </message>
    <message>
        <location filename="functypeconverter.cpp" line="43"/>
        <source>Data Recovery</source>
        <translation>数据还原</translation>
    </message>
    <message>
        <location filename="functypeconverter.cpp" line="46"/>
        <source>Log Records</source>
        <translation>操作日志</translation>
    </message>
    <message>
        <location filename="functypeconverter.cpp" line="49"/>
        <source>Ghost Image</source>
        <translation>Ghost镜像</translation>
    </message>
</context>
<context>
    <name>GhostImage</name>
    <message>
        <location filename="module/ghostimage.cpp" line="68"/>
        <source>Ghost Image</source>
        <translation>Ghost镜像</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="89"/>
        <source>A ghost image file can only be created after backup system to local disk</source>
        <translation>必须先进行本地系统备份，否则无法创建镜像文件</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="100"/>
        <source>Simple</source>
        <translation>操作简单</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="107"/>
        <source>Fast</source>
        <translation>创建速度快</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="114"/>
        <source>Security</source>
        <translation>安全可靠</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="121"/>
        <source>Timesaving</source>
        <translation>节省时间</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="127"/>
        <source>Start Ghost</source>
        <translation>创建镜像</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="198"/>
        <source>Please select storage location</source>
        <translation>请选择存储位置</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="227"/>
        <source>local default path : </source>
        <translation>本地默认路径：</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="230"/>
        <source>removable devices path : </source>
        <translation>移动设备：</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="247"/>
        <location filename="module/ghostimage.cpp" line="392"/>
        <source>Back</source>
        <translation>上一步</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="256"/>
        <location filename="module/ghostimage.cpp" line="400"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="303"/>
        <location filename="module/ghostimage.cpp" line="683"/>
        <location filename="module/ghostimage.cpp" line="997"/>
        <source>checking</source>
        <translation>环境检测</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="307"/>
        <location filename="module/ghostimage.cpp" line="687"/>
        <location filename="module/ghostimage.cpp" line="1001"/>
        <source>ghosting</source>
        <translation>创建中</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="311"/>
        <location filename="module/ghostimage.cpp" line="691"/>
        <location filename="module/ghostimage.cpp" line="1005"/>
        <source>finished</source>
        <translation>创建完成</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="411"/>
        <source>Recheck</source>
        <translation>重新检测</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="447"/>
        <source>Checking, wait a moment ...</source>
        <translation>正在检测，请稍等...</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="453"/>
        <source>Check whether the conditions for creating an ghost image are met</source>
        <translation>检测是否具备制作Ghost镜像条件</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="455"/>
        <source>Do not perform other operations during creating an ghost image to avoid data loss</source>
        <translation>制作Ghost镜像期间不要做其它操作，以防数据丢失</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="473"/>
        <source>Check success</source>
        <translation>检测成功</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="475"/>
        <source>The storage space is enough</source>
        <translation>存储空间充足</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="480"/>
        <source>Make sure the computer is plugged in or the battery level is above 60%</source>
        <translation>请确保电脑已连接电源或电量超过60%</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="492"/>
        <source>Check failure</source>
        <translation>检测失败</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="566"/>
        <location filename="module/ghostimage.cpp" line="861"/>
        <source>Program lock failed, please retry</source>
        <translation>程序锁定失败，请重试</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="568"/>
        <location filename="module/ghostimage.cpp" line="863"/>
        <source>There may be other backups or restores being performed</source>
        <translation>可能有其它备份/还原等任务在执行</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="572"/>
        <location filename="module/ghostimage.cpp" line="867"/>
        <source>Unsupported task type</source>
        <translation>不支持的任务类型</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="574"/>
        <location filename="module/ghostimage.cpp" line="869"/>
        <source>No processing logic was found in the service</source>
        <translation>没有找到相应的处理逻辑</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="578"/>
        <location filename="module/ghostimage.cpp" line="873"/>
        <source>Failed to mount the backup partition</source>
        <translation>备份分区挂载失败</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="580"/>
        <location filename="module/ghostimage.cpp" line="875"/>
        <source>Check whether there is a backup partition</source>
        <translation>检查是否有备份分区</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="584"/>
        <location filename="module/ghostimage.cpp" line="879"/>
        <source>The filesystem of device is vfat format</source>
        <translation>移动设备的文件系统是vfat格式</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="586"/>
        <location filename="module/ghostimage.cpp" line="881"/>
        <source>Please change filesystem format to ext3、ext4 or ntfs</source>
        <translation>请换成ext3、ext4、ntfs等文件系统格式</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="590"/>
        <location filename="module/ghostimage.cpp" line="885"/>
        <source>The device is read only</source>
        <translation>移动设备是只读挂载的</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="592"/>
        <location filename="module/ghostimage.cpp" line="887"/>
        <source>Please chmod to rw</source>
        <translation>请修改为读写模式</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="596"/>
        <location filename="module/ghostimage.cpp" line="891"/>
        <source>The storage for ghost is not enough</source>
        <translation>Ghost存储空间不足</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="598"/>
        <location filename="module/ghostimage.cpp" line="604"/>
        <location filename="module/ghostimage.cpp" line="893"/>
        <location filename="module/ghostimage.cpp" line="899"/>
        <source>Retry after release space</source>
        <translation>建议释放空间后重试</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="602"/>
        <location filename="module/ghostimage.cpp" line="897"/>
        <source>There is not enough space for temporary .kyimg file</source>
        <translation>没有足够的空间存放临时.kyimg文件</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="608"/>
        <location filename="module/ghostimage.cpp" line="903"/>
        <source>Other backup or restore task is being performed</source>
        <translation>其它备份还原等操作正在执行</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="610"/>
        <location filename="module/ghostimage.cpp" line="905"/>
        <source>Please try again later</source>
        <translation>请稍后重试</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="615"/>
        <location filename="module/ghostimage.cpp" line="910"/>
        <source>The backup node does not exist</source>
        <translation>相应的备份节点不存在</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="617"/>
        <location filename="module/ghostimage.cpp" line="912"/>
        <source>Check whether the backup point has been deleted</source>
        <translation>请检查备份点是否已经被删除</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="752"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="783"/>
        <source>Do not use computer in case of data loss</source>
        <translation>请勿使用电脑，以防数据丢失</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="916"/>
        <source>The data is being compressed to the local disk, please wait patiently...</source>
        <translation>正压缩数据到本地磁盘，请耐心等待...</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="921"/>
        <source>Transferring image file to mobile device, about to be completed...</source>
        <translation>正在传输image文件到移动设备，即将完成...</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="926"/>
        <source>The image creation had been canceled</source>
        <translation>已取消制作Ghost镜像</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="928"/>
        <source>Re-initiate the image creation if necessary</source>
        <translation>如需要可以重新进行Ghost镜像制作</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="961"/>
        <source>An error occurred during make ghost image</source>
        <translation>制作Ghost镜像时发生错误</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="963"/>
        <source>Error messages refer to log file : /var/log/backup.log</source>
        <translation>错误信息请参考日志文件：/var/log/backup.log</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1079"/>
        <source>Home Page</source>
        <translation>返回首页</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1087"/>
        <source>Retry</source>
        <translation>再试一次</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1121"/>
        <source>Ghost image creation is successful</source>
        <translation>创建成功</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1124"/>
        <source>You can view it in the directory : %1</source>
        <translation>您可以在“%1”目录下查看</translation>
    </message>
    <message>
        <location filename="module/ghostimage.cpp" line="1138"/>
        <source>Ghost image creation is failed</source>
        <translation>创建失败</translation>
    </message>
</context>
<context>
    <name>LeftsiderbarWidget</name>
    <message>
        <location filename="leftsiderbarwidget.cpp" line="50"/>
        <location filename="leftsiderbarwidget.cpp" line="51"/>
        <source>Backup &amp; Restore</source>
        <translation>备份还原工具</translation>
    </message>
</context>
<context>
    <name>MainDialog</name>
    <message>
        <location filename="maindialog.cpp" line="123"/>
        <source>Main menu</source>
        <translation>主菜单</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="124"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="125"/>
        <source>Close</source>
        <translation>关闭</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="166"/>
        <source>Help</source>
        <translation>帮助</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="169"/>
        <source>About</source>
        <translation>关于</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="171"/>
        <source>Exit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="31"/>
        <source>Backup &amp; Restore</source>
        <translation>备份还原工具</translation>
    </message>
</context>
<context>
    <name>ManageBackupPointList</name>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="16"/>
        <source>System Backup Information</source>
        <translation>系统备份信息</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="18"/>
        <source>Data Backup Information</source>
        <translation>数据备份信息</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="30"/>
        <source>You can delete the backup that does not need, refer operation logs for more details</source>
        <translation>您可以删除不需要的备份，更多细节请参考“操作日志”</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="33"/>
        <source>Delete</source>
        <translation>删除</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="153"/>
        <source>backup finished</source>
        <translation>备份完成</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="155"/>
        <source>backup unfinished</source>
        <translation>备份未完成</translation>
    </message>
</context>
<context>
    <name>OperationLog</name>
    <message>
        <location filename="module/operationlog.cpp" line="43"/>
        <source>No operation log</source>
        <translation>无操作日志</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="53"/>
        <source>Backup Name</source>
        <translation>备份名称</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="53"/>
        <source>UUID</source>
        <translation>备份标识</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="53"/>
        <source>Operation</source>
        <translation>操作</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="53"/>
        <source>Operation Time</source>
        <translation>操作时间</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="153"/>
        <source>new system backup</source>
        <translation>新建系统备份</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="157"/>
        <source>udpate system backup</source>
        <translation>增量系统备份</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="161"/>
        <source>new data backup</source>
        <translation>新建数据备份</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="165"/>
        <source>update data backup</source>
        <translation>更新数据备份</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="169"/>
        <source>restore system</source>
        <translation>系统还原</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="173"/>
        <source>restore retaining user data</source>
        <translation>保留用户数据还原</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="177"/>
        <source>restore data</source>
        <translation>数据还原</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="181"/>
        <source>delete backup</source>
        <translation>删除备份</translation>
    </message>
    <message>
        <location filename="module/operationlog.cpp" line="185"/>
        <source>make ghost image</source>
        <translation>制作ghost镜像</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../backup-daemon/parsebackuplist.cpp" line="363"/>
        <source>factory backup</source>
        <translation>出厂备份</translation>
    </message>
    <message>
        <location filename="../common/utils.cpp" line="1042"/>
        <source>Factory Backup</source>
        <translation>出厂备份</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="34"/>
        <source>Backup State</source>
        <translation>备份状态</translation>
    </message>
    <message>
        <location filename="backuppointlistdialog.cpp" line="34"/>
        <source>PrefixPath</source>
        <translation>前缀路径</translation>
    </message>
    <message>
        <location filename="component/backuplistwidget.cpp" line="220"/>
        <location filename="component/backuplistwidget.cpp" line="226"/>
        <location filename="component/backuplistwidget.cpp" line="235"/>
        <location filename="component/backuplistwidget.cpp" line="258"/>
        <location filename="maindialog.cpp" line="286"/>
        <location filename="maindialog.cpp" line="302"/>
        <location filename="maindialog.cpp" line="322"/>
        <location filename="module/databackup.cpp" line="477"/>
        <location filename="module/databackup.cpp" line="495"/>
        <location filename="module/databackup.cpp" line="715"/>
        <location filename="module/datarestore.cpp" line="997"/>
        <location filename="module/managebackuppointlist.cpp" line="49"/>
        <location filename="module/selectrestorepoint.cpp" line="49"/>
        <location filename="module/systembackup.cpp" line="345"/>
        <location filename="module/systemrestore.cpp" line="206"/>
        <source>Warning</source>
        <translation>警告</translation>
    </message>
    <message>
        <location filename="component/backuplistwidget.cpp" line="220"/>
        <source>Path can not include symbols that such as : ``,$(),${},;,&amp;,|,etc.</source>
        <translation>路径中不能包含：``、$()、${}、;、&amp;、|等特殊符号</translation>
    </message>
    <message>
        <location filename="component/backuplistwidget.cpp" line="220"/>
        <source>OK</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="component/backuplistwidget.cpp" line="227"/>
        <source>Path already exists : </source>
        <translation>路径已经存在：</translation>
    </message>
    <message>
        <location filename="component/backuplistwidget.cpp" line="228"/>
        <location filename="component/backuplistwidget.cpp" line="237"/>
        <location filename="component/backuplistwidget.cpp" line="260"/>
        <location filename="main.cpp" line="45"/>
        <location filename="maindialog.cpp" line="288"/>
        <location filename="maindialog.cpp" line="304"/>
        <location filename="maindialog.cpp" line="324"/>
        <location filename="module/databackup.cpp" line="447"/>
        <location filename="module/databackup.cpp" line="479"/>
        <location filename="module/databackup.cpp" line="497"/>
        <location filename="module/databackup.cpp" line="717"/>
        <location filename="module/databackup.cpp" line="1548"/>
        <location filename="module/datarestore.cpp" line="999"/>
        <location filename="module/ghostimage.cpp" line="807"/>
        <location filename="module/managebackuppointlist.cpp" line="49"/>
        <location filename="module/managebackuppointlist.cpp" line="54"/>
        <location filename="module/selectrestorepoint.cpp" line="49"/>
        <location filename="module/systembackup.cpp" line="316"/>
        <location filename="module/systembackup.cpp" line="347"/>
        <location filename="module/systembackup.cpp" line="1106"/>
        <source>Ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="component/backuplistwidget.cpp" line="236"/>
        <source>The file or directory does not exist : </source>
        <translation>文件或目录不存在</translation>
    </message>
    <message>
        <location filename="component/backuplistwidget.cpp" line="259"/>
        <source>Only data that exists in the follow directorys can be selected: %1.
 Path:%2 is not in them.</source>
        <translation>只有后面目录中的数据可以选择：%1。
路径：%2不在其中。</translation>
    </message>
    <message>
        <location filename="main.cpp" line="43"/>
        <location filename="module/databackup.cpp" line="445"/>
        <location filename="module/databackup.cpp" line="1548"/>
        <location filename="module/datarestore.cpp" line="177"/>
        <location filename="module/ghostimage.cpp" line="807"/>
        <location filename="module/managebackuppointlist.cpp" line="54"/>
        <location filename="module/selectrestorepoint.cpp" line="55"/>
        <location filename="module/systembackup.cpp" line="314"/>
        <location filename="module/systembackup.cpp" line="1106"/>
        <source>Information</source>
        <translation>提示</translation>
    </message>
    <message>
        <source>This function can only be used by administrator.</source>
        <translation type="vanished">此功能只能由系统管理员使用。</translation>
    </message>
    <message>
        <location filename="main.cpp" line="44"/>
        <source>Another user had opened kybackup, you can not start it again.</source>
        <translation>其他用户已开启备份还原工具，不能再开启</translation>
    </message>
    <message>
        <location filename="main.cpp" line="98"/>
        <source>kybackup</source>
        <translation>备份还原工具</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="287"/>
        <source>An exception occurred when mounting backup partition.</source>
        <translation>挂载备份分区时发生错误。</translation>
    </message>
    <message>
        <source>Please check if the backup partition exists which can be created when you install the Operating System.</source>
        <translation type="vanished">请检查备份还原分区是否存在，在安装操作系统时必须创建备份还原分区。</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="303"/>
        <source>Failed to mount backup partition.</source>
        <translation>挂载备份分区失败。</translation>
    </message>
    <message>
        <location filename="maindialog.cpp" line="323"/>
        <location filename="module/datarestore.cpp" line="998"/>
        <source>It&apos;s busy, please wait</source>
        <translation>系统正忙，请稍等</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="446"/>
        <location filename="module/systembackup.cpp" line="315"/>
        <source>Are you sure to continue customizing the path?
The custom path backup file is not protected, which may cause the backup file to be lost or damaged</source>
        <translation>确定自定义路径？
自定义路径备份文件不受保护，可能导致备份文件丢失或损坏</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="478"/>
        <location filename="module/systembackup.cpp" line="346"/>
        <source>Please select backup position</source>
        <translation>请选择备份位置</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="496"/>
        <location filename="module/databackup.cpp" line="716"/>
        <source>Please select a backup file or directory</source>
        <translation>请选择一个备份文件或目录</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="1548"/>
        <location filename="module/ghostimage.cpp" line="807"/>
        <location filename="module/systembackup.cpp" line="1106"/>
        <source>Are you sure to cancel the operation？</source>
        <translation>确定取消当前操作？</translation>
    </message>
    <message>
        <location filename="module/databackup.cpp" line="447"/>
        <location filename="module/databackup.cpp" line="1548"/>
        <location filename="module/datarestore.cpp" line="177"/>
        <location filename="module/ghostimage.cpp" line="807"/>
        <location filename="module/managebackuppointlist.cpp" line="54"/>
        <location filename="module/selectrestorepoint.cpp" line="55"/>
        <location filename="module/systembackup.cpp" line="316"/>
        <location filename="module/systembackup.cpp" line="1106"/>
        <location filename="module/systemrestore.cpp" line="208"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="177"/>
        <location filename="module/selectrestorepoint.cpp" line="55"/>
        <location filename="module/systemrestore.cpp" line="208"/>
        <source>Continue</source>
        <translation>继续</translation>
    </message>
    <message>
        <location filename="module/datarestore.cpp" line="177"/>
        <source>Contains the user&apos;s home directory, which need to reboot after restoration. Are you sure to continue?</source>
        <translation>包含用户家目录，还原完成后需要重启系统。是否继续？</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="49"/>
        <location filename="module/selectrestorepoint.cpp" line="49"/>
        <source>Please select one backup to continue.</source>
        <translation>请选择一个备份再继续。</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="54"/>
        <source>Are you sure to delete the backup ?</source>
        <translation>是否确定删除此备份？</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="62"/>
        <location filename="module/managebackuppointlist.cpp" line="143"/>
        <location filename="module/selectrestorepoint.cpp" line="162"/>
        <source>Customize:</source>
        <translation>自定义位置：</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="147"/>
        <location filename="module/selectrestorepoint.cpp" line="169"/>
        <source>Udisk Device:</source>
        <translation>移动设备：</translation>
    </message>
    <message>
        <location filename="module/managebackuppointlist.cpp" line="63"/>
        <location filename="module/managebackuppointlist.cpp" line="149"/>
        <location filename="module/selectrestorepoint.cpp" line="171"/>
        <source>Local Disk:</source>
        <translation>本地磁盘：</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="55"/>
        <source>Do you want to continue?</source>
        <translation>是否继续？</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="167"/>
        <source>Other machine:</source>
        <translation>异机备份：</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="207"/>
        <source>Restore factory settings, your system user data will not be retained. Are you sure to continue?</source>
        <translation>恢复出厂设置，您的系统用户数据都将会消失。是否继续？</translation>
    </message>
</context>
<context>
    <name>SelectRestorePoint</name>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="14"/>
        <source>System Backup Information</source>
        <translation>系统备份信息</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="16"/>
        <source>Data Backup Information</source>
        <translation>数据备份信息</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="29"/>
        <source>Ok</source>
        <translation>确定</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="63"/>
        <source>Other machine:</source>
        <translation>异机备份：</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="65"/>
        <source>Customize:</source>
        <translation>自定义位置：</translation>
    </message>
    <message>
        <location filename="module/selectrestorepoint.cpp" line="67"/>
        <source>Udisk Device:</source>
        <translation>移动设备：</translation>
    </message>
</context>
<context>
    <name>SystemBackup</name>
    <message>
        <location filename="module/systembackup.cpp" line="69"/>
        <source>System Backup</source>
        <translation>系统备份</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="89"/>
        <source>Can be restored when files are damaged or lost</source>
        <translation>系统原始文件受损或丢失时可以进行还原</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="100"/>
        <source>Multi-Spot</source>
        <translation>多还原点</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="107"/>
        <source>Small Size</source>
        <translation>体积小</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="114"/>
        <source>Security</source>
        <translation>安全保障</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="121"/>
        <source>Simple</source>
        <translation>操作简单</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="127"/>
        <source>Start Backup</source>
        <translation>开始备份</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="144"/>
        <source>Backup Management &gt;&gt;</source>
        <translation>备份管理 &gt;&gt;</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="217"/>
        <source>Please select backup position</source>
        <translation>请选择备份位置</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="282"/>
        <source>local default path : </source>
        <translation>本地默认路径：</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="285"/>
        <source>removable devices path : </source>
        <translation>移动设备：</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="247"/>
        <location filename="module/systembackup.cpp" line="487"/>
        <location filename="module/systembackup.cpp" line="833"/>
        <source>Back</source>
        <translation>上一步</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="235"/>
        <source>Browse...</source>
        <translation>浏览...</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="256"/>
        <location filename="module/systembackup.cpp" line="495"/>
        <location filename="module/systembackup.cpp" line="842"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="295"/>
        <location filename="module/systembackup.cpp" line="331"/>
        <source>customize path : </source>
        <translation>自定义路径：</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="389"/>
        <location filename="module/systembackup.cpp" line="744"/>
        <location filename="module/systembackup.cpp" line="982"/>
        <location filename="module/systembackup.cpp" line="1314"/>
        <source>checking</source>
        <translation>环境检测</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="392"/>
        <location filename="module/systembackup.cpp" line="747"/>
        <location filename="module/systembackup.cpp" line="985"/>
        <location filename="module/systembackup.cpp" line="1317"/>
        <source>preparing</source>
        <translation>备份准备</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="395"/>
        <location filename="module/systembackup.cpp" line="750"/>
        <location filename="module/systembackup.cpp" line="988"/>
        <location filename="module/systembackup.cpp" line="1320"/>
        <source>backuping</source>
        <translation>备份中</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="398"/>
        <location filename="module/systembackup.cpp" line="753"/>
        <location filename="module/systembackup.cpp" line="991"/>
        <location filename="module/systembackup.cpp" line="1323"/>
        <source>finished</source>
        <translation>备份完成</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="506"/>
        <source>Recheck</source>
        <translation>重新检测</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="537"/>
        <source>Checking, wait a moment ...</source>
        <translation>正在检测，请稍等...</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="543"/>
        <source>Do not perform other operations during backup to avoid data loss</source>
        <translation>备份期间不要做其它操作，以防数据丢失</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="546"/>
        <source>Check whether the remaining capacity of the backup partition is sufficient</source>
        <translation>检测备份位置空间是否充足···</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="549"/>
        <source>Check whether the remaining capacity of the removable device is sufficient</source>
        <translation>检测移动设备空间是否充足···</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="568"/>
        <source>Check success</source>
        <translation>检测成功</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="570"/>
        <source>The storage for backup is enough</source>
        <translation>备份空间充足</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="575"/>
        <source>Make sure the computer is plugged in or the battery level is above 60%</source>
        <translation>请确保电脑已连接电源或电量超过60%</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="587"/>
        <source>Check failure</source>
        <translation>检测失败</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="661"/>
        <location filename="module/systembackup.cpp" line="1166"/>
        <source>Program lock failed, please retry</source>
        <translation>程序锁定失败，请重试</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="663"/>
        <location filename="module/systembackup.cpp" line="1168"/>
        <source>There may be other backups or restores being performed</source>
        <translation>可能有其它备份/还原等任务在执行</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="667"/>
        <location filename="module/systembackup.cpp" line="1172"/>
        <source>Unsupported task type</source>
        <translation>不支持的任务类型</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="669"/>
        <location filename="module/systembackup.cpp" line="1174"/>
        <source>No processing logic was found in the service</source>
        <translation>没有找到相应的处理逻辑</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="673"/>
        <location filename="module/systembackup.cpp" line="1178"/>
        <source>Failed to mount the backup partition</source>
        <translation>备份分区挂载失败</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="675"/>
        <location filename="module/systembackup.cpp" line="1180"/>
        <source>Check whether there is a backup partition</source>
        <translation>检查是否有备份分区</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="679"/>
        <location filename="module/systembackup.cpp" line="1184"/>
        <source>The filesystem of device is vfat format</source>
        <translation>移动设备的文件系统是vfat格式</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="681"/>
        <location filename="module/systembackup.cpp" line="1186"/>
        <source>Please change filesystem format to ext3、ext4 or ntfs</source>
        <translation>请换成ext3、ext4、ntfs等文件系统格式</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="685"/>
        <location filename="module/systembackup.cpp" line="1190"/>
        <source>The device is read only</source>
        <translation>移动设备是只读挂载的</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="687"/>
        <location filename="module/systembackup.cpp" line="1192"/>
        <source>Please chmod to rw</source>
        <translation>请修改为读写模式</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="691"/>
        <location filename="module/systembackup.cpp" line="1196"/>
        <source>The storage for backup is not enough</source>
        <translation>备份空间不足</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="693"/>
        <location filename="module/systembackup.cpp" line="1198"/>
        <source>Retry after release space</source>
        <translation>建议释放空间后重试</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="697"/>
        <location filename="module/systembackup.cpp" line="1202"/>
        <source>Other backup or restore task is being performed</source>
        <translation>其它备份还原等操作正在执行</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="699"/>
        <location filename="module/systembackup.cpp" line="1204"/>
        <source>Please try again later</source>
        <translation>请稍后重试</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="779"/>
        <source>Backup Name</source>
        <translation>备份名称</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="809"/>
        <location filename="module/systembackup.cpp" line="857"/>
        <source>Name already exists</source>
        <translation>名称已存在</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="922"/>
        <source>factory backup</source>
        <translation>出厂备份</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1063"/>
        <source>Cancel</source>
        <translation>取消</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1087"/>
        <source>Do not use computer in case of data loss</source>
        <translation>请勿使用电脑，以防数据丢失</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1210"/>
        <source>Failed to create the backup point directory</source>
        <translation>创建备份目录失败</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1212"/>
        <source>Please check backup partition permissions</source>
        <translation>请检查备份分区权限</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1216"/>
        <source>The system is being compressed to the local disk, please wait patiently...</source>
        <translation>正压缩系统到本地磁盘，请耐心等待...</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1224"/>
        <source>Transferring image file to mobile device, about to be completed...</source>
        <translation>正在传输image文件到移动设备，即将完成...</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1229"/>
        <source>The backup had been canceled</source>
        <translation>已取消备份</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1231"/>
        <source>Re-initiate the backup if necessary</source>
        <translation>如需要可重新进行备份</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1244"/>
        <location filename="module/systembackup.cpp" line="1270"/>
        <source>An error occurred during backup</source>
        <translation>备份时发生错误</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1246"/>
        <location filename="module/systembackup.cpp" line="1272"/>
        <source>Error messages refer to log file : /var/log/backup.log</source>
        <translation>错误信息请参考日志文件：/var/log/backup.log</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1404"/>
        <source>Home Page</source>
        <translation>返回首页</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1412"/>
        <source>Retry</source>
        <translation>再试一次</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1438"/>
        <source>The backup is successful</source>
        <translation>备份成功</translation>
    </message>
    <message>
        <location filename="module/systembackup.cpp" line="1453"/>
        <source>The backup is failed</source>
        <translation>备份失败</translation>
    </message>
</context>
<context>
    <name>SystemRestore</name>
    <message>
        <location filename="module/systemrestore.cpp" line="62"/>
        <source>System Restore</source>
        <translation>系统还原</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="80"/>
        <source>You can restore the system to its previous state</source>
        <translation>在您遇到问题时可将系统还原到以前的状态</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="91"/>
        <source>Simple</source>
        <translation>操作简单</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="98"/>
        <source>Security</source>
        <translation>安全可靠</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="105"/>
        <source>Repair</source>
        <translation>修复系统损坏</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="112"/>
        <source>Independent</source>
        <translation>自主操作</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="118"/>
        <source>Start Restore</source>
        <translation>开始还原</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="130"/>
        <source>Factory Restore</source>
        <translation>出厂还原</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="134"/>
        <source>Retaining User Data</source>
        <translation>保留用户数据</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="261"/>
        <location filename="module/systemrestore.cpp" line="595"/>
        <location filename="module/systemrestore.cpp" line="832"/>
        <source>checking</source>
        <translation>环境检测</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="265"/>
        <location filename="module/systemrestore.cpp" line="599"/>
        <location filename="module/systemrestore.cpp" line="836"/>
        <source>restoring</source>
        <translation>还原中</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="269"/>
        <location filename="module/systemrestore.cpp" line="603"/>
        <location filename="module/systemrestore.cpp" line="840"/>
        <source>finished</source>
        <translation>还原完成</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="350"/>
        <source>Back</source>
        <translation>上一步</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="358"/>
        <source>Next</source>
        <translation>下一步</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="369"/>
        <source>Recheck</source>
        <translation>重新检测</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="406"/>
        <source>Checking, wait a moment ...</source>
        <translation>正在检测，请稍等...</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="412"/>
        <source>Check whether the restore environment meets the requirements</source>
        <translation>检查恢复环境是否符合要求</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="414"/>
        <source>Do not perform other operations during restore to avoid data loss</source>
        <translation>还原期间不要做其它操作，以防数据丢失</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="432"/>
        <source>Check success</source>
        <translation>检测成功</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="434"/>
        <source>The system will reboot automatically after the restore is successful</source>
        <translation>还原成功后系统将自动重启</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="439"/>
        <source>Make sure the computer is plugged in or the battery level is above 60%</source>
        <translation>请确保电脑已连接电源或电量超过60%</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="451"/>
        <source>Check failure</source>
        <translation>检测失败</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="526"/>
        <location filename="module/systemrestore.cpp" line="725"/>
        <source>Program lock failed, please retry</source>
        <translation>程序锁定失败，请重试</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="528"/>
        <location filename="module/systemrestore.cpp" line="727"/>
        <source>There may be other backups or restores being performed</source>
        <translation>可能有其它备份/还原等任务在执行</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="532"/>
        <location filename="module/systemrestore.cpp" line="731"/>
        <source>Unsupported task type</source>
        <translation>不支持的任务类型</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="534"/>
        <location filename="module/systemrestore.cpp" line="733"/>
        <source>No processing logic was found in the service</source>
        <translation>没有找到相应的处理逻辑</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="538"/>
        <location filename="module/systemrestore.cpp" line="737"/>
        <source>The .user.txt file does not exist</source>
        <translation>.user.txt文件不存在</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="540"/>
        <location filename="module/systemrestore.cpp" line="546"/>
        <location filename="module/systemrestore.cpp" line="552"/>
        <location filename="module/systemrestore.cpp" line="739"/>
        <location filename="module/systemrestore.cpp" line="745"/>
        <location filename="module/systemrestore.cpp" line="751"/>
        <source>Backup points may be corrupted</source>
        <translation>备份点可能被损坏</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="544"/>
        <location filename="module/systemrestore.cpp" line="743"/>
        <source>The .exclude.user.txt file does not exist</source>
        <translation>.exclude.user.txt文件不存在</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="550"/>
        <location filename="module/systemrestore.cpp" line="749"/>
        <source>The backup point data directory does not exist</source>
        <translation>备份点数据目录不存在</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="556"/>
        <location filename="module/systemrestore.cpp" line="755"/>
        <source>Failed to rsync /boot/efi</source>
        <translation>同步/boot/efi失败</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="558"/>
        <location filename="module/systemrestore.cpp" line="757"/>
        <source>Check the mounting mode of the /boot/efi partition</source>
        <translation>请检查/boot/efi分区挂载方式</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="643"/>
        <source>Do not use computer in case of data loss</source>
        <translation>请勿使用电脑，以防数据丢失</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="761"/>
        <source>Failed to prepare the restore directory</source>
        <translation>还原目录准备失败</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="763"/>
        <source>Refer to log :/var/log/backup.log for more information</source>
        <translation>更多信息请参考日志/var/log/backup.log</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="796"/>
        <source>An error occurred during restore</source>
        <translation>还原时发生错误</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="798"/>
        <source>Error messages refer to log file : /var/log/backup.log</source>
        <translation>错误信息请参考日志文件：/var/log/backup.log</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="914"/>
        <source>Home Page</source>
        <translation>返回首页</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="922"/>
        <source>Retry</source>
        <translation>再试一次</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="956"/>
        <source>Successfully restoring the system</source>
        <translation>系统还原成功</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="961"/>
        <source>The system will automatically reboot</source>
        <translation>系统将自动重启</translation>
    </message>
    <message>
        <location filename="module/systemrestore.cpp" line="970"/>
        <source>Restoring the system failed</source>
        <translation>系统还原失败</translation>
    </message>
</context>
<context>
    <name>restore</name>
    <message>
        <location filename="main.cpp" line="106"/>
        <source>system restore</source>
        <translation>系统还原</translation>
    </message>
</context>
</TS>
