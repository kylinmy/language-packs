<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ug">
<context>
    <name>CloudSyncUI::GlobalVariant</name>
    <message>
        <source>Your account:%1</source>
        <translation type="vanished">您的帐户：%1</translation>
    </message>
    <message>
        <source>Auto sync</source>
        <translation type="vanished">自动同步</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="vanished">未连接</translation>
    </message>
    <message>
        <source>Sync your settings</source>
        <translation type="vanished">同步您的设置</translation>
    </message>
    <message>
        <source>Sync failed!</source>
        <translation type="vanished">同步存在失败项！</translation>
    </message>
    <message>
        <source>CloudSync service may crashed!</source>
        <translation type="vanished">云帐户功能可能没有开启！</translation>
    </message>
    <message>
        <source>Please ensure CloudSync installed!</source>
        <translation type="vanished">确保云帐户服务已经安装！</translation>
    </message>
    <message>
        <source>Sync processing!</source>
        <translation type="vanished">同步中！</translation>
    </message>
    <message>
        <source>Kylin Cloud Account</source>
        <translation type="vanished">云帐户</translation>
    </message>
    <message>
        <source>Cloud ID desktop message</source>
        <translation type="vanished">消息</translation>
    </message>
</context>
<context>
    <name>FirstPage</name>
    <message>
        <location filename="../firstpage.cpp" line="20"/>
        <source>Service is not valid for private server</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../firstpage.cpp" line="100"/>
        <source>Sync your settings across the other devices!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../firstpage.cpp" line="110"/>
        <source>Network is not accessible</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../firstpage.cpp" line="111"/>
        <source>Ensure internet accessibility</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../firstpage.cpp" line="134"/>
        <source>Sign In</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FrameItem</name>
    <message>
        <source>Auto-sync</source>
        <translation type="vanished">自动同步</translation>
    </message>
    <message>
        <location filename="../frameitem.cpp" line="77"/>
        <source>Some items sync failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frameitem.cpp" line="78"/>
        <source>Watting for sync!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../frameitem.cpp" line="128"/>
        <source>Auto-Sync</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>FrameList</name>
    <message>
        <location filename="../framelist.cpp" line="22"/>
        <source>Wallpaper</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="25"/>
        <source>ScreenSaver</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="28"/>
        <source>Peony</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="31"/>
        <source>Power</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="34"/>
        <source>Themes</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="37"/>
        <source>Touchpad</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="40"/>
        <source>Quick Launch</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="43"/>
        <source>Panel</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="46"/>
        <source>Mouse</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="49"/>
        <source>Menu</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="52"/>
        <source>Font</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="55"/>
        <source>Keyboard</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="58"/>
        <source>Datetime</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../framelist.cpp" line="61"/>
        <source>Avatar</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>HeaderModel</name>
    <message>
        <location filename="../headermodel.cpp" line="25"/>
        <source>Change Password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../headermodel.cpp" line="26"/>
        <source>Sign Out</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>KYComboBox</name>
    <message>
        <location filename="../../common-ui/kycombobox.cpp" line="48"/>
        <source>Your Email/Name/Phone</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MCodeWidget</name>
    <message>
        <location filename="../mcodewidget.cpp" line="33"/>
        <source>SongTi</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainDialog</name>
    <message>
        <source>Kylin ID Center</source>
        <translation type="vanished">麒麟ID登录中心</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="98"/>
        <source>Forget?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>kylin ID</source>
        <translation type="vanished">麒麟ID</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="97"/>
        <source>Kylin ID</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="103"/>
        <source>Remember it</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="244"/>
        <source>Your password</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your Email/Name/Phone</source>
        <translation type="vanished">邮箱/用户名/手机号码</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="248"/>
        <source>Pass login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="249"/>
        <source>Phone login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="250"/>
        <location filename="../maindialog.cpp" line="632"/>
        <location filename="../maindialog.cpp" line="713"/>
        <location filename="../maindialog.cpp" line="747"/>
        <source>Login</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="251"/>
        <location filename="../maindialog.cpp" line="732"/>
        <source>Send</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="245"/>
        <source>Please wait</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Your Email/Name/Phone here</source>
        <translation type="vanished">请输入邮箱/用户名/手机号码</translation>
    </message>
    <message>
        <source>Your password here</source>
        <translation type="vanished">请输入密码</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="104"/>
        <source>Register</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="246"/>
        <source>Your code</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="247"/>
        <source>Your phone number here</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="278"/>
        <location filename="../maindialog.cpp" line="548"/>
        <source>Please move slider to right place</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="738"/>
        <source>%1s left</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="931"/>
        <source>User stop verify Captcha</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="932"/>
        <source>Parsing data failed!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="933"/>
        <location filename="../maindialog.cpp" line="940"/>
        <source>No response data!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="934"/>
        <source>Timeout!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="935"/>
        <source>Server internal error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="936"/>
        <location filename="../maindialog.cpp" line="944"/>
        <location filename="../maindialog.cpp" line="946"/>
        <source>Phone number error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="938"/>
        <location filename="../maindialog.cpp" line="948"/>
        <source>Pictrure has expired!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="939"/>
        <source>User deleted!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="958"/>
        <source>Please check account status!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation type="vanished">手机号码已经在使用！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="943"/>
        <source>Your are reach the limit!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Please check your phone number!</source>
        <translation type="vanished">手机号码其他错误！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="945"/>
        <source>Please check your code!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="949"/>
        <source>Pictrure blocked!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="950"/>
        <source>Illegal code!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="951"/>
        <source>Phone code is expired!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="952"/>
        <source>Failed attemps limit reached!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="954"/>
        <source>Slider validate error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="955"/>
        <source>Phone code error!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="956"/>
        <source>Code can not be empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="957"/>
        <source>MCode can not be empty!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Account doesn&apos;t exists!</source>
        <translation type="vanished">用户未注册！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="937"/>
        <source>No network!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="941"/>
        <source>Phone number exsists!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="942"/>
        <source>Wrong phone number format!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="947"/>
        <source>Send sms Limited!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Too many attemps,you will lock in 5 min!</source>
        <translation type="vanished">尝试过多，锁定5分钟!</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="953"/>
        <source>Wrong account or password!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Wrong phone code!</source>
        <translation type="vanished">手机验证码错误!</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exist!</source>
        <translation type="vanished">用户未注册！</translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="959"/>
        <source>Unsupported operation!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="960"/>
        <source>Unsupported Client Type!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="961"/>
        <source>Please check your input!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../maindialog.cpp" line="963"/>
        <source>Process failed</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <source>Sync your settings</source>
        <translation type="vanished">同步您的设置</translation>
    </message>
    <message>
        <source>Sync failed!</source>
        <translation type="vanished">同步失败！</translation>
    </message>
    <message>
        <source>CloudSync service may crashed!</source>
        <translation type="vanished">云帐户功能可能没有开启！</translation>
    </message>
    <message>
        <source>Please ensure CloudSync installed!</source>
        <translation type="vanished">确保云帐户服务已经安装！</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="95"/>
        <location filename="../mainwidget.cpp" line="522"/>
        <location filename="../mainwidget.h" line="105"/>
        <source>Disconnected</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="144"/>
        <source>Sync processing!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Waiting for service start...</source>
        <translation type="vanished">等待服务准备完毕...</translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="275"/>
        <source>Auto-sync</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="280"/>
        <source>Cloud Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="339"/>
        <location filename="../mainwidget.cpp" line="589"/>
        <source>Failed to sync!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="357"/>
        <source>We get some trouble when service start</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="386"/>
        <source>Make sure installed cloud sync!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="537"/>
        <source>Waitting for sync complete!</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="567"/>
        <source>Cloud Sync</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../mainwidget.cpp" line="570"/>
        <source>Kylin Account</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Sync!</source>
        <translation type="vanished">同步中！</translation>
    </message>
    <message>
        <source>Kylin Cloud Account</source>
        <translation type="vanished">云帐户</translation>
    </message>
    <message>
        <source>Cloud ID desktop message</source>
        <translation type="vanished">消息</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Your account:%1</source>
        <translation type="vanished">您的帐户：%1</translation>
    </message>
    <message>
        <source>Auto sync</source>
        <translation type="vanished">自动同步</translation>
    </message>
    <message>
        <source>Disconnected</source>
        <translation type="vanished">未连接</translation>
    </message>
    <message>
        <source>Sync your settings</source>
        <translation type="vanished">同步您的设置</translation>
    </message>
    <message>
        <source>Sync failed!</source>
        <translation type="vanished">同步存在失败！</translation>
    </message>
    <message>
        <source>CloudSync service may crashed!</source>
        <translation type="vanished">云帐户功能可能没有开启！</translation>
    </message>
    <message>
        <source>Please ensure CloudSync installed!</source>
        <translation type="vanished">确保云帐户服务已经安装！</translation>
    </message>
    <message>
        <source>Sync processing!</source>
        <translation type="vanished">同步中！</translation>
    </message>
    <message>
        <source>Kylin Cloud Account</source>
        <translation type="vanished">云帐户</translation>
    </message>
    <message>
        <source>Cloud ID desktop message</source>
        <translation type="vanished">消息</translation>
    </message>
</context>
<context>
    <name>networkaccount</name>
    <message>
        <location filename="../networkaccount.cpp" line="30"/>
        <source>Cloud Account</source>
        <translation type="unfinished"></translation>
    </message>
</context>
</TS>
