<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>KYComboBox</name>
    <message>
        <location filename="../../common-ui/kycombobox.cpp" line="48"/>
        <source>Your Email/Name/Phone</source>
        <translation>邮箱/用户名/手机号码</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Kylin ID Center</source>
        <translation type="vanished">麒麟ID登录中心</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="130"/>
        <source>Forget?</source>
        <translation>忘记密码?</translation>
    </message>
    <message>
        <source>kylin ID</source>
        <translation type="vanished">麒麟ID</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="129"/>
        <source>Kylin ID</source>
        <translation>Kylin ID</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="135"/>
        <source>Remember it</source>
        <translation>记住密码</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="305"/>
        <source>Your password</source>
        <translation>请输入密码</translation>
    </message>
    <message>
        <source>Your Email/Name/Phone</source>
        <translation type="vanished">邮箱/用户名/手机号码</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="309"/>
        <source>Pass login</source>
        <translation>密码登录</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="310"/>
        <source>Phone login</source>
        <translation>手机登录</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="311"/>
        <location filename="../mainwindow.cpp" line="683"/>
        <location filename="../mainwindow.cpp" line="770"/>
        <location filename="../mainwindow.cpp" line="838"/>
        <source>Login</source>
        <translation>登录</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="312"/>
        <location filename="../mainwindow.cpp" line="789"/>
        <source>Send</source>
        <translation>发送</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="306"/>
        <source>Please wait</source>
        <translation>正在登录</translation>
    </message>
    <message>
        <source>Your Email/Name/Phone here</source>
        <translation type="vanished">请输入邮箱/用户名/手机号码</translation>
    </message>
    <message>
        <source>Your password here</source>
        <translation type="vanished">请输入密码</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="136"/>
        <source>Register</source>
        <translation>注册</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="307"/>
        <source>Your code</source>
        <translation>请输入验证码</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="308"/>
        <source>Your phone number here</source>
        <translation>请输入手机号码</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="339"/>
        <location filename="../mainwindow.cpp" line="592"/>
        <source>Please move slider to right place</source>
        <translation>请滑动滑块到合适的位置</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="795"/>
        <source>%1s left</source>
        <translation>剩%1秒</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1086"/>
        <source>User stop verify Captcha</source>
        <translation>用户给停止验证验证码</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1087"/>
        <source>Parsing data failed!</source>
        <translation>解析数据失败!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1088"/>
        <location filename="../mainwindow.cpp" line="1095"/>
        <source>No response data!</source>
        <translation>无响应数据!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1089"/>
        <source>Timeout!</source>
        <translation>登录超时，请重试！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1090"/>
        <source>Server internal error!</source>
        <translation>服务器内部错误!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1091"/>
        <location filename="../mainwindow.cpp" line="1099"/>
        <location filename="../mainwindow.cpp" line="1101"/>
        <source>Phone number error!</source>
        <translation>请检查您的手机号码！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1093"/>
        <location filename="../mainwindow.cpp" line="1103"/>
        <source>Pictrure has expired!</source>
        <translation>图片已经过期！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1094"/>
        <source>User deleted!</source>
        <translation>用户已经销户！</translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation type="vanished">手机号码已经在使用！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1098"/>
        <source>Your are reach the limit!</source>
        <translation>该手机当日接收短信次数达到上限！</translation>
    </message>
    <message>
        <source>Please check your phone number!</source>
        <translation type="vanished">请检查您的手机号码！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1100"/>
        <source>Please check your code!</source>
        <translation>手机号码其他错误！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1104"/>
        <source>Pictrure blocked!</source>
        <translation>图片格式损坏!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1105"/>
        <source>Illegal code!</source>
        <translation>非法参数！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1106"/>
        <source>Phone code is expired!</source>
        <translation>验证码已经失效！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1107"/>
        <source>Failed attemps limit reached!</source>
        <translation>尝试次数太多！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1109"/>
        <source>Slider validate error</source>
        <translation>滑动验证码验证失败</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1110"/>
        <source>Phone code error!</source>
        <translation>手机验证码错误！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1111"/>
        <source>Code can not be empty!</source>
        <translation>验证码不能为空！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1112"/>
        <source>MCode can not be empty!</source>
        <translation>验证码不能为空！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1113"/>
        <source>Please check account status!</source>
        <translation>请检查你的账号状态！</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exists!</source>
        <translation type="vanished">用户未注册！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1092"/>
        <source>No network!</source>
        <translation>网络不可达!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1096"/>
        <source>Phone number exsists!</source>
        <translation>手机号码已存在！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1097"/>
        <source>Wrong phone number format!</source>
        <translation>手机号码格式错误!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1102"/>
        <source>Send sms Limited!</source>
        <translation>发送短信达到限制！</translation>
    </message>
    <message>
        <source>Too many attemps,you will lock in 5 min!</source>
        <translation type="vanished">尝试过多，锁定5分钟!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1108"/>
        <source>Wrong account or password!</source>
        <translation>账号或者密码错误!</translation>
    </message>
    <message>
        <source>Wrong phone code!</source>
        <translation type="vanished">手机验证码错误!</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exist!</source>
        <translation type="vanished">用户未注册！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1114"/>
        <source>Unsupported operation!</source>
        <translation>暂不支持该操作!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1115"/>
        <source>Unsupported Client Type!</source>
        <translation>不支持的客户端类型!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1116"/>
        <source>Please check your input!</source>
        <translation>请检查您的输入!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1118"/>
        <source>Process failed</source>
        <translation>发送请求失败</translation>
    </message>
</context>
<context>
    <name>WechatLogin</name>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="114"/>
        <source>Register</source>
        <translation>注册</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="123"/>
        <source>Login</source>
        <translation>登录</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="137"/>
        <location filename="../wechat/wechatlogin.cpp" line="468"/>
        <source>Scan Code for Quick Login</source>
        <translation>微信扫一扫快速登录</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="299"/>
        <location filename="../wechat/wechatlogin.cpp" line="347"/>
        <source>Fail to Get QRcode,Please Retry</source>
        <translation>获得二维码失败，请重试</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="313"/>
        <source>QRcode Invaild,Please Click to Refresh</source>
        <translation>二维码失效，请点击刷新</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="327"/>
        <source>Success</source>
        <translation>扫描成功</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="424"/>
        <source>Not register yet, click to register</source>
        <translation>用户未注册，请先点击注册</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="445"/>
        <source>Click login button after register successful</source>
        <translation>注册后点击登录按钮完成登录</translation>
    </message>
</context>
</TS>
