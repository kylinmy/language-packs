<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>KYComboBox</name>
    <message>
        <location filename="../../common-ui/kycombobox.cpp" line="48"/>
        <source>Your Email/Name/Phone</source>
        <translation>ᠢᠮᠸᠯ/ ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠨᠡᠷ᠎ᠡ/ ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <source>Kylin ID Center</source>
        <translation type="vanished">麒麟ID登录中心</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="130"/>
        <source>Forget?</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠵᠢᠡᠨ ᠮᠠᠷᠳᠠᠭᠰᠠᠨ ᠤᠤ?</translation>
    </message>
    <message>
        <source>kylin ID</source>
        <translation type="vanished">麒麟ID</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="129"/>
        <source>Kylin ID</source>
        <translation>Kylin ID</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="135"/>
        <source>Remember it</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠵᠢᠨᠨ ᠴᠡᠬᠡᠵᠢᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="305"/>
        <source>Your password</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠵᠢᠨᠨ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <source>Your Email/Name/Phone</source>
        <translation type="vanished">邮箱/用户名/手机号码</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="309"/>
        <source>Pass login</source>
        <translation>ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠵᠢᠡᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="310"/>
        <source>Phone login</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤ ᠪᠡᠷ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="311"/>
        <location filename="../mainwindow.cpp" line="683"/>
        <location filename="../mainwindow.cpp" line="770"/>
        <location filename="../mainwindow.cpp" line="838"/>
        <source>Login</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="312"/>
        <location filename="../mainwindow.cpp" line="789"/>
        <source>Send</source>
        <translation>ᠢᠯᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="306"/>
        <source>Please wait</source>
        <translation>ᠶᠠᠭ ᠨᠡᠪᠳᠡᠷᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <source>Your Email/Name/Phone here</source>
        <translation type="vanished">请输入邮箱/用户名/手机号码</translation>
    </message>
    <message>
        <source>Your password here</source>
        <translation type="vanished">请输入密码</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="136"/>
        <source>Register</source>
        <translation>ᠪᠦᠷᠢᠳᠬᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="307"/>
        <source>Your code</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠢ᠋ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="308"/>
        <source>Your phone number here</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤᠤ ᠨᠤᠮᠸᠷ ᠵᠢᠨᠨ ᠤᠷᠤᠭᠤᠯᠤᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="339"/>
        <location filename="../mainwindow.cpp" line="592"/>
        <source>Please move slider to right place</source>
        <translation>ᠭᠤᠯᠭᠤᠬᠤ ᠬᠡᠰᠡᠭ ᠢ᠋ ᠳᠤᠬᠢᠷᠠᠮᠵᠢᠳᠠᠢ ᠪᠠᠢᠷᠢᠨ ᠳ᠋ᠤ᠌ ᠭᠤᠯᠭᠤᠭᠤᠯᠵᠤ ᠠᠪᠠᠴᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="795"/>
        <source>%1s left</source>
        <translation>%1 ᠰᠸᠺᠦᠨ᠋ᠲ ᠦᠯᠡᠳᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1086"/>
        <source>User stop verify Captcha</source>
        <translation>ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠢ᠋ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠵᠢ ᠵᠤᠭᠰᠤᠭᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1087"/>
        <source>Parsing data failed!</source>
        <translation>ᠳ᠋ᠠᠢᠲ᠋ᠠ ᠵᠢᠨ ᠵᠠᠳᠠᠯᠤᠯᠳᠠ ᠢᠯᠠᠭᠳᠠᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1088"/>
        <location filename="../mainwindow.cpp" line="1095"/>
        <source>No response data!</source>
        <translation>ᠬᠠᠷᠢᠭᠤ ᠳᠤᠰᠬᠠᠯ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ ᠳ᠋ᠠᠢᠲ᠋ᠠ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1089"/>
        <source>Timeout!</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠴᠠᠭ ᠬᠡᠳᠦᠷᠡᠪᠡ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1090"/>
        <source>Server internal error!</source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦᠷ ᠤ᠋ᠨ ᠳᠤᠳᠤᠭᠠᠳᠤ ᠵᠢᠨ ᠪᠤᠷᠤᠭᠤ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1091"/>
        <location filename="../mainwindow.cpp" line="1099"/>
        <location filename="../mainwindow.cpp" line="1101"/>
        <source>Phone number error!</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠵᠢᠨᠨ ᠪᠠᠢᠴᠠᠭᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1093"/>
        <location filename="../mainwindow.cpp" line="1103"/>
        <source>Pictrure has expired!</source>
        <translation>ᠵᠢᠷᠤᠭ ᠨᠢᠭᠡᠨᠳᠡ ᠬᠤᠭᠤᠴᠠᠭ᠎ᠠ ᠨᠢ ᠦᠩᠬᠡᠷᠡᠪᠡ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1094"/>
        <source>User deleted!</source>
        <translation>ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠨᠢᠭᠡᠨᠳᠡ ᠳᠠᠩᠰᠠ ᠪᠡᠨ ᠬᠠᠰᠤᠪᠠ!</translation>
    </message>
    <message>
        <source>Phone number already in used!</source>
        <translation type="vanished">手机号码已经在使用！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1098"/>
        <source>Your are reach the limit!</source>
        <translation>ᠳᠤᠰ ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠳᠤᠰ ᠡᠳᠦᠷ ᠲᠤ᠌ ᠬᠤᠷᠢᠶᠠᠵᠤ ᠠᠪᠬᠤ ᠤᠬᠤᠷᠢ ᠮᠡᠳᠡᠬᠡᠨ ᠤ᠋ ᠲᠤᠭ᠎ᠠ ᠨᠢᠭᠡᠨᠳᠡ ᠬᠦᠢᠴᠡᠪᠡ!</translation>
    </message>
    <message>
        <source>Please check your phone number!</source>
        <translation type="vanished">请检查您的手机号码！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1100"/>
        <source>Please check your code!</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠪᠤᠰᠤᠳ ᠪᠤᠷᠤᠭᠤ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1104"/>
        <source>Pictrure blocked!</source>
        <translation>ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ ᠡᠪᠳᠡᠷᠡᠪᠡ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1105"/>
        <source>Illegal code!</source>
        <translation>ᠬᠠᠤᠯᠢ ᠪᠤᠰᠤ ᠫᠠᠷᠠᠮᠧᠲ᠋ᠷ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1106"/>
        <source>Phone code is expired!</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1107"/>
        <source>Failed attemps limit reached!</source>
        <translation>ᠳᠤᠷᠰᠢᠭᠰᠠᠨ ᠤᠳᠠᠭ᠎ᠠ ᠬᠡᠳᠦ ᠤᠯᠠᠨ ᠪᠤᠯᠪᠠ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1109"/>
        <source>Slider validate error</source>
        <translation>ᠭᠤᠯᠭᠤᠭᠤᠯᠬᠤ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠵᠢᠡᠷ ᠪᠠᠳᠤᠯᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1110"/>
        <source>Phone code error!</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠪᠤᠷᠤᠭᠤ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1111"/>
        <source>Code can not be empty!</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1112"/>
        <source>MCode can not be empty!</source>
        <translation>ᠰᠢᠯᠭᠠᠨ ᠪᠠᠳᠤᠯᠠᠬᠤ ᠺᠤᠳ᠋ ᠬᠤᠭᠤᠰᠤᠨ ᠪᠠᠢᠵᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1113"/>
        <source>Please check account status!</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠳᠠᠩᠰᠠᠨ ᠤ᠋ ᠪᠠᠢᠳᠠᠯ ᠵᠢᠨᠨ ᠪᠠᠢᠴᠠᠭᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exists!</source>
        <translation type="vanished">用户未注册！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1092"/>
        <source>No network!</source>
        <translation>ᠰᠦᠯᠵᠢᠶ᠎ᠡ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1096"/>
        <source>Phone number exsists!</source>
        <translation>ᠳᠤᠰ ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠨᠢᠭᠡᠨᠳᠡ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1097"/>
        <source>Wrong phone number format!</source>
        <translation>ᠭᠠᠷ ᠤᠳᠠᠰᠤᠨ ᠤ᠋ ᠨᠤᠮᠸᠷ ᠤ᠋ᠨ ᠵᠠᠭᠪᠤᠷ ᠪᠤᠷᠤᠭᠤ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1102"/>
        <source>Send sms Limited!</source>
        <translation>ᠢᠯᠡᠬᠡᠬᠦ ᠤᠬᠤᠷᠢ ᠮᠡᠳᠡᠭᠡᠨ ᠤ᠋ ᠲᠤᠭ᠎ᠠ ᠬᠦᠢᠴᠡᠪᠡ!</translation>
    </message>
    <message>
        <source>Too many attemps,you will lock in 5 min!</source>
        <translation type="vanished">尝试过多，锁定5分钟!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1108"/>
        <source>Wrong account or password!</source>
        <translation>ᠳᠠᠩᠰᠠ ᠪᠤᠶᠤ ᠨᠢᠭᠤᠴᠠ ᠺᠤᠳ᠋ ᠪᠤᠷᠤᠭᠤ!</translation>
    </message>
    <message>
        <source>Wrong phone code!</source>
        <translation type="vanished">手机验证码错误!</translation>
    </message>
    <message>
        <source>Account doesn&apos;t exist!</source>
        <translation type="vanished">用户未注册！</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1114"/>
        <source>Unsupported operation!</source>
        <translation>ᠳᠤᠰ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ ᠵᠢ ᠳᠦᠷ ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1115"/>
        <source>Unsupported Client Type!</source>
        <translation>ᠳᠡᠮᠵᠢᠬᠦ ᠦᠬᠡᠢ ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠵᠢᠨ ᠦᠵᠦᠬᠦᠷ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1116"/>
        <source>Please check your input!</source>
        <translation>ᠲᠠᠨ ᠤ᠋ ᠤᠷᠤᠭᠤᠯᠤᠭᠰᠠᠨ ᠵᠢᠨᠨ ᠪᠠᠢᠴᠠᠭᠠᠭᠠᠷᠠᠢ!</translation>
    </message>
    <message>
        <location filename="../mainwindow.cpp" line="1118"/>
        <source>Process failed</source>
        <translation>ᠢᠯᠡᠬᠡᠭᠰᠡᠨ ᠭᠤᠶᠤᠴᠢᠯᠠᠯ ᠢᠯᠠᠭᠳᠠᠪᠠ</translation>
    </message>
</context>
<context>
    <name>WechatLogin</name>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="114"/>
        <source>Register</source>
        <translation>ᠪᠦᠷᠢᠳᠬᠡᠬᠦᠯᠬᠦ</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="123"/>
        <source>Login</source>
        <translation>ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="137"/>
        <location filename="../wechat/wechatlogin.cpp" line="468"/>
        <source>Scan Code for Quick Login</source>
        <translation>ᠸᠢᠴᠠᠲ ᠵᠢᠡᠷ ᠰᠢᠷᠪᠢᠵᠤ ᠬᠤᠷᠳᠤᠨ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="299"/>
        <location filename="../wechat/wechatlogin.cpp" line="347"/>
        <source>Fail to Get QRcode,Please Retry</source>
        <translation>ᠬᠤᠶᠠᠷ ᠬᠡᠮᠵᠢᠯᠳᠡᠳᠦ ᠺᠤᠳ᠋ ᠤᠯᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂ ᠳᠠᠬᠢᠵᠤ ᠳᠤᠷᠰᠢᠭᠠᠷᠠᠢ</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="313"/>
        <source>QRcode Invaild,Please Click to Refresh</source>
        <translation>ᠬᠤᠶᠠᠷ ᠬᠡᠮᠵᠢᠯᠳᠡᠳᠦ ᠺᠤᠳ᠋ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ᠂ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠰᠢᠨᠡᠴᠢᠯᠡᠬᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="327"/>
        <source>Success</source>
        <translation>ᠰᠢᠷᠪᠢᠵᠤ ᠴᠢᠳᠠᠪᠠ</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="424"/>
        <source>Not register yet, click to register</source>
        <translation>ᠬᠡᠷᠡᠭᠡᠯᠡᠭᠴᠢ ᠪᠦᠷᠢᠳᠬᠡᠬᠦᠯᠦᠬᠡ ᠦᠬᠡᠢ᠂ ᠤᠷᠢᠳᠠᠪᠠᠷ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠪᠦᠷᠢᠳᠬᠡᠬᠦᠯᠦᠬᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <location filename="../wechat/wechatlogin.cpp" line="445"/>
        <source>Click login button after register successful</source>
        <translation>ᠪᠦᠷᠢᠳᠬᠡᠬᠦᠯᠦᠭᠰᠡᠨ ᠤ᠋ ᠳᠠᠷᠠᠭ᠎ᠠ ᠨᠡᠪᠳᠡᠷᠡᠬᠦ ᠳᠠᠷᠤᠪᠴᠢ ᠵᠢ ᠳᠤᠪᠴᠢᠳᠠᠵᠤ ᠨᠡᠪᠳᠡᠷᠡᠨ᠎ᠡ</translation>
    </message>
</context>
</TS>
