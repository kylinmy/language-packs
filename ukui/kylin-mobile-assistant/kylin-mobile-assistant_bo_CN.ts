<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>BaseDevice</name>
    <message>
        <location filename="../projection/device/basedevice.cpp" line="409"/>
        <source>Control Devices Supported</source>
        <translation>རྒྱབ་སྐྱོར་བྱེད་པའི་ཚོད་འཛིན་སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <location filename="../projection/device/basedevice.cpp" line="413"/>
        <source>Control device not supported</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་ཚོད་འཛིན་སྒྲིག་ཆས།</translation>
    </message>
</context>
<context>
    <name>ConnectedTitleWin</name>
    <message>
        <location filename="../ui/view/connectedtitlewin.cpp" line="28"/>
        <source>Connected</source>
        <translation>འབྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/view/connectedtitlewin.cpp" line="29"/>
        <location filename="../ui/view/connectedtitlewin.cpp" line="102"/>
        <source>Mobile Window</source>
        <translation>ཁ་པར་གྱི་འཆར་ངོས།</translation>
    </message>
    <message>
        <location filename="../ui/view/connectedtitlewin.cpp" line="30"/>
        <location filename="../ui/view/connectedtitlewin.cpp" line="97"/>
        <source>Disconnect</source>
        <translation>འབྲེལ་ཐག་ཆད་པ།</translation>
    </message>
</context>
<context>
    <name>CopyToComputerButtonWidget</name>
    <message>
        <location filename="../ui/storagelist/copytocomputerbuttonwidget.cpp" line="23"/>
        <source>Copy</source>
        <translation>འཕྲུལ་ཆས་འདི་ལ་བསྐྱར་དཔར་བྱས།</translation>
    </message>
    <message>
        <location filename="../ui/storagelist/copytocomputerbuttonwidget.cpp" line="46"/>
        <source>Choose folder</source>
        <translation>ཡིག་སྣོད་འདེམས་པ།</translation>
    </message>
</context>
<context>
    <name>InitConnectWin</name>
    <message>
        <location filename="../ui/initconnectwin/initconnectwin.cpp" line="14"/>
        <source>ScanCode</source>
        <translation>ཞིབ་བཤེར་སྦྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/initconnectwin.cpp" line="15"/>
        <source>USBConnect</source>
        <translation>USBསྦྲེལ་མཐུད།</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/initconnectwin.cpp" line="22"/>
        <source>The function is under development, please look forward to it.</source>
        <translation>The function is under development, please look forward to it.</translation>
    </message>
</context>
<context>
    <name>InterfaceWin</name>
    <message>
        <location filename="../ui/initconnectwin/interface_win.cpp" line="26"/>
        <source>No network detected</source>
        <translation>ཞིབ་དཔྱད་ཚད་ལེན་མ་ཐུབ་པའི་དྲ་རྒྱ།</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/interface_win.cpp" line="74"/>
        <source>Use the mobile app to scan this code</source>
        <translation>ཁ་པར་གྱི་ཁ་ཕྱེ་ནས་appརྩ་གཉིས་ཨང་ཏར་བཤར་འབེབས་བྱས།</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/interface_win.cpp" line="78"/>
        <source>Connect the mobile phone and computer to the same network,open the</source>
        <translation>ཁ་པར་དང་གློག་ཀླད་དྲ་རྒྱ་གཅིག་ལ་སྦྲེལ་ནས་ཁ་ཕྱེ།</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/interface_win.cpp" line="81"/>
        <source>mobile phone app</source>
        <translation>ཁ་པར་གྱི་app</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/interface_win.cpp" line="81"/>
        <source>and scan the QR code.</source>
        <translation>རྩ་གཉིས་རྟགས་རིས་འདི་བཤར་འབེབས་བྱས།</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/interface_win.cpp" line="97"/>
        <source>Scan this code with your mobile browser</source>
        <translation>ཁ་པར་གྱི་བཤར་ཆས་བཀོལ་ནས་ཨང་གྲངས་འདི་བཤར་ཕབ་བྱས།</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/interface_win.cpp" line="102"/>
        <source>Please scan this QR code with your mobile browser to download the app</source>
        <translation>ཁྱོད་ཀྱིས་ཁ་པར་བཤར་ཆས་བཀོལ་ནས་རྩ་གཉིས་ཨང་ཏར་བཤར་ཕབ་བྱས་ནས་appཕབ་ལེན་བྱེད་རོགས།</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../ui/mainwindow.cpp" line="257"/>
        <location filename="../ui/mainwindow.cpp" line="765"/>
        <source>mobile-assistant</source>
        <translation>ཁ་པར་ལག་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="454"/>
        <source>Not currently connected, please connect</source>
        <translation>མིག་སྔར་འབྲེལ་མཐུད་མ་བྱས་ན་འབྲེལ་མཐུད་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="605"/>
        <source>Please install kylin-assistant on the Android terminal!</source>
        <translation>ཁ་པར་གྱི་སྣེ་ནས་ཆི་ལིན་ཁ་པརapp་ལག་རོགས་ཕབ་ལེན་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="619"/>
        <source>Connection error</source>
        <translation>འབྲེལ་མཐུད་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="748"/>
        <source>file download failed</source>
        <translation>ཡིག་ཆ་ཕབ་ལེན་བྱེད་མ་ཐུབ་པ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="755"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="755"/>
        <source>Peony access can be error-prone</source>
        <translation>ཡིག་ཆ་དོ་དམ་ཆས་ཀྱིས་བཅར་འདྲི་བྱས་ན་ནོར་འཁྲུལ་འབྱུང་སྲིད།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="757"/>
        <source>Umount failed</source>
        <translation>བཤིག་འདོན་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="765"/>
        <source>Version:</source>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="767"/>
        <source>Mobile Assistant is an interconnection tool of Android device and Kirin operating system, which supports Android file synchronization, file transfer, screen mirroring and other functions, which is simple and fast to operate</source>
        <translation>ཁ་པར་ལག་རོགས་ནིAndroid་སྒྲིག་ཆས་དང་ཆི་ལིན་བཀོལ་སྤྱོད་མ་ལག་གི་ཕན་ཚུན་སྦྲེལ་བའི་ལག་ཆ་ཞིག་ཡིན་ལ།་ཡིག་ཆ་དང་མཉམ་དུ། ཡིག་ཆ་བརྒྱུད་སྐུར་དང་བརྙན་ཤེལ་སོགས་ཀྱི་བྱེད་ནུས་ལAndroid་རྒྱབ་སྐྱོར་བྱེད་གྱིན་ཡོད་ལ།བཀོལ་སྤྱོད་སྟབས་བདེ་ལ་མགྱོགས་པོ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="845"/>
        <source>QQPicture</source>
        <translation>QQཔར།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="848"/>
        <source>QQVideo</source>
        <translation>QQབརྙན།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="851"/>
        <source>QQMusic</source>
        <translation>QQMགླུ་དབྱངས།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="854"/>
        <source>QQDocument</source>
        <translation>QQཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="857"/>
        <source>WeChatPicture</source>
        <translation>སྐད་འཕྲིན་གྱི་འདྲ་པར།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="860"/>
        <source>WeChatVideo</source>
        <translation>སྐད་འཕྲིན་གྱི་བརྙན།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="863"/>
        <source>WeChatMusic</source>
        <translation>སྐད་འཕྲིན་གྱི་གླུ་དབྱངས།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="866"/>
        <source>WeChatDocument</source>
        <translation>འཕྲིན་ཕྲན་གྱི་ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="869"/>
        <location filename="../ui/mainwindow.cpp" line="1028"/>
        <source>Picture</source>
        <translation>པར་རིས།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="872"/>
        <location filename="../ui/mainwindow.cpp" line="1030"/>
        <source>Video</source>
        <translation>བརྙན་ཕབ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="875"/>
        <location filename="../ui/mainwindow.cpp" line="1032"/>
        <source>Music</source>
        <translation>རོལ་མོ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="878"/>
        <location filename="../ui/mainwindow.cpp" line="1034"/>
        <source>Document</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="881"/>
        <location filename="../ui/mainwindow.cpp" line="1036"/>
        <source>WeChat</source>
        <translation>འཕྲིན་ཕྲན།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="884"/>
        <location filename="../ui/mainwindow.cpp" line="1038"/>
        <source>QQ</source>
        <translation>QQ</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1066"/>
        <location filename="../ui/mainwindow.cpp" line="1077"/>
        <source>Uploaded to</source>
        <translation>གོང་དུ་སྤེལ་བ།</translation>
    </message>
    <message>
        <location filename="../ui/mainwindow.cpp" line="1106"/>
        <source>Downloaded to</source>
        <translation>ཕབ་ལེན་བྱས་ནས།</translation>
    </message>
</context>
<context>
    <name>MobileFileList</name>
    <message>
        <location filename="../ui/view/mobilefilelist.cpp" line="61"/>
        <source>Mobile file list</source>
        <translation>ཁ་པར་གྱི་ཡིག་ཆའི་རེའུ་མིག</translation>
    </message>
    <message>
        <location filename="../ui/view/mobilefilelist.cpp" line="69"/>
        <source>Mobile storage</source>
        <translation>སྤོ་འགུལ་གསོག་ཉར།</translation>
    </message>
    <message>
        <location filename="../ui/view/mobilefilelist.cpp" line="138"/>
        <source>Picture</source>
        <translation>པར་རིས།</translation>
    </message>
    <message>
        <location filename="../ui/view/mobilefilelist.cpp" line="142"/>
        <source>Video</source>
        <translation>བརྙན་ཕབ།</translation>
    </message>
    <message>
        <location filename="../ui/view/mobilefilelist.cpp" line="146"/>
        <source>Music</source>
        <translation>རོལ་མོ།</translation>
    </message>
    <message>
        <location filename="../ui/view/mobilefilelist.cpp" line="150"/>
        <source>Document</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../ui/view/mobilefilelist.cpp" line="154"/>
        <source>WeChat</source>
        <translation>འཕྲིན་ཕྲན།</translation>
    </message>
    <message>
        <location filename="../ui/view/mobilefilelist.cpp" line="158"/>
        <source>QQ</source>
        <translation>QQ</translation>
    </message>
    <message>
        <location filename="../ui/view/mobilefilelist.cpp" line="169"/>
        <source>Mobile</source>
        <translation>ཁ་པར།</translation>
    </message>
    <message>
        <location filename="../ui/view/mobilefilelist.cpp" line="170"/>
        <source>All File</source>
        <translation>ཡིག་ཆ་ཡོད་ཚད།</translation>
    </message>
</context>
<context>
    <name>MobileFileListItem</name>
    <message>
        <location filename="../ui/view/mobilefilelistitem.cpp" line="64"/>
        <source>items</source>
        <translation>རྣམ་གྲངས།</translation>
    </message>
</context>
<context>
    <name>MobileFileListView</name>
    <message>
        <location filename="../ui/classificationlist/mobilefilelistview.cpp" line="82"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
</context>
<context>
    <name>MobileFileSecondWidget</name>
    <message>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="73"/>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="172"/>
        <source>Mobile file list</source>
        <translation>ཁ་པར་གྱི་ཡིག་ཆའི་རེའུ་མིག</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="75"/>
        <source>WeChat</source>
        <translation>འཕྲིན་ཕྲན།</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="77"/>
        <source>QQ</source>
        <translation>QQ</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="85"/>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="203"/>
        <source>List Mode</source>
        <translation>མིང་ཐོའི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="93"/>
        <source>Refresh</source>
        <translation>གསར་བསྒྱུར།</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="196"/>
        <source>Icon Mode</source>
        <translation>མཚོན་རྟགས་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="228"/>
        <source>Picture</source>
        <translation>པར་རིས།</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="233"/>
        <source>Video</source>
        <translation>བརྙན་ཕབ།</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="238"/>
        <source>Music</source>
        <translation>རོལ་མོ།</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilesecondwidget.cpp" line="243"/>
        <source>Document</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
</context>
<context>
    <name>MobileFileWidget</name>
    <message>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="51"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="280"/>
        <source>Icon Mode</source>
        <translation>མཚོན་རྟགས་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="55"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="149"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="286"/>
        <source>List Mode</source>
        <translation>མིང་ཐོའི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="84"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="223"/>
        <source>Mobile file list</source>
        <translation>ཁ་པར་ཡིག་ཆའི་རེའུ་མིག</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="89"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="92"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="95"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="98"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="128"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="132"/>
        <source>QQ</source>
        <translation>QQ</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="89"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="101"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="113"/>
        <source>Picture</source>
        <translation>པར་རིས།</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="92"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="104"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="116"/>
        <source>Video</source>
        <translation>བརྙན་ཕབ།</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="95"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="107"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="119"/>
        <source>Music</source>
        <translation>རོལ་མོ།</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="98"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="110"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="122"/>
        <source>Document</source>
        <translation>ཡིག་ཆ།</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="101"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="104"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="107"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="110"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="125"/>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="132"/>
        <source>WeChat</source>
        <translation>འཕྲིན་ཕྲན།</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/mobilefilewidget.cpp" line="157"/>
        <source>Refresh</source>
        <translation>གསར་བསྒྱུར།</translation>
    </message>
</context>
<context>
    <name>MobileStorageListView</name>
    <message>
        <location filename="../ui/storagelist/mobilestoragelistview.cpp" line="76"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
</context>
<context>
    <name>MobileStorageViewWidget</name>
    <message>
        <location filename="../ui/storagelist/mobilestorageviewwidget.cpp" line="30"/>
        <source>No file</source>
        <translation>ཡིག་ཆ་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>MobileStorageWidget</name>
    <message>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="45"/>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="231"/>
        <source>Icon Mode</source>
        <translation>མཚོན་རྟགས་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="49"/>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="97"/>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="236"/>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="293"/>
        <source>List Mode</source>
        <translation>མིང་ཐོའི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="77"/>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="176"/>
        <source>Mobile file list</source>
        <translation>ཁ་པར་ཡིག་ཆའི་རེའུ་མིག</translation>
    </message>
    <message>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="78"/>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="183"/>
        <source>Mobile storage</source>
        <translation>ཁ་པར་གསོག་ཉར།</translation>
    </message>
    <message>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="105"/>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="301"/>
        <source>Refresh</source>
        <translation>གསར་བསྒྱུར།</translation>
    </message>
    <message>
        <location filename="../ui/storagelist/mobilestoragewidget.cpp" line="280"/>
        <source>Search out %1 Results</source>
        <translation>%1གི་གྲུབ་འབྲས་འཚོལ་ཞིབ་བྱས།</translation>
    </message>
</context>
<context>
    <name>QFtp</name>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="810"/>
        <source>Not connected</source>
        <translation>Not connected</translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="878"/>
        <source>Host %1 not found</source>
        <translation>Host %1 not found</translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="881"/>
        <source>Connection refused to host %1</source>
        <translation>Connection refused to host %1</translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="885"/>
        <source>Connection timed out to host %1</source>
        <translation>Connection timed out to host %1</translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="988"/>
        <source>Connected to host %1</source>
        <translation>Connected to host %1</translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="1205"/>
        <source>Connection refused for data connection</source>
        <translation>Connection refused for data connection</translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="1381"/>
        <source>Unknown error</source>
        <translation>Unknown error</translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="2277"/>
        <source>Connecting to host failed:
%1</source>
        <translation>Connecting to host failed:
%1</translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="2280"/>
        <source>Login failed:
%1</source>
        <translation>Login failed:
%1</translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="2283"/>
        <source>Listing directory failed:
%1</source>
        <translation>Listing directory failed:
%1</translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="2286"/>
        <source>Changing directory failed:
%1</source>
        <translation>Changing directory failed:
%1</translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="2289"/>
        <source>Downloading file failed:
%1</source>
        <translation>Downloading file failed:
%1</translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="2292"/>
        <source>Uploading file failed:
%1</source>
        <translation>Uploading file failed:
%1</translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="2295"/>
        <source>Removing file failed:
%1</source>
        <translation>Removing file failed:
%1</translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="2298"/>
        <source>Creating directory failed:
%1</source>
        <translation>Creating directory failed:
%1</translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="2301"/>
        <source>Removing directory failed:
%1</source>
        <translation>Removing directory failed:
%1</translation>
    </message>
    <message>
        <location filename="../filetransfer/qftp.cpp" line="2328"/>
        <source>Connection closed</source>
        <translation>Connection closed</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../filedb/searchdatabase.cpp" line="55"/>
        <source>Database Error</source>
        <translation>Database Error</translation>
    </message>
    <message>
        <location filename="../main.cpp" line="95"/>
        <source>In order to ensure the normal operation of the program, please install the VLC program first!</source>
        <translation>In order to ensure the normal operation of the program, please install the VLC program first!</translation>
    </message>
</context>
<context>
    <name>RotationChart</name>
    <message>
        <location filename="../ui/initconnectwin/rotationchart.cpp" line="92"/>
        <source>No tutorial</source>
        <translation>ཟུར་ཁྲིད་མི་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>ScrollSettingWidget</name>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="22"/>
        <source>Mouse sensitivity</source>
        <translation>འཁོར་ལོའི་ཚོར་སྐྱེན་ཚད།</translation>
    </message>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="34"/>
        <source>slow</source>
        <translation>དལ་མོ་ཡིན་པ།</translation>
    </message>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="35"/>
        <source>quick</source>
        <translation>མགྱོགས་མྱུར།</translation>
    </message>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="40"/>
        <source>ok</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../projection/uibase/scrollsettingwidget.cpp" line="41"/>
        <source>cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
</context>
<context>
    <name>TimeLineView</name>
    <message>
        <location filename="../ui/classificationlist/timelineview.cpp" line="301"/>
        <source>%1/%2/%3</source>
        <translation>%1ལོ%2ཟླ%3ཚེས</translation>
    </message>
    <message>
        <location filename="../ui/classificationlist/timelineview.cpp" line="678"/>
        <location filename="../ui/classificationlist/timelineview.cpp" line="701"/>
        <source>No file</source>
        <translation>ཡིག་ཆ་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>Titlebar</name>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="42"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="50"/>
        <source>Menu</source>
        <translation>འདེམས་པང་།</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="55"/>
        <source>mobile-assistant</source>
        <translation>ཁ་པར་གྱི་ལས་རོགས་པ།</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="70"/>
        <source>Minimize</source>
        <translation>ཆེས་ཆུང་དུ་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="88"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="141"/>
        <location filename="../ui/view/titlebar.cpp" line="179"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="142"/>
        <location filename="../ui/view/titlebar.cpp" line="181"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../ui/view/titlebar.cpp" line="143"/>
        <location filename="../ui/view/titlebar.cpp" line="183"/>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན་བྱ་རྒྱུ།</translation>
    </message>
</context>
<context>
    <name>TransmissionDialog</name>
    <message>
        <location filename="../ui/view/transmissiondialog.cpp" line="16"/>
        <source>Current progress</source>
        <translation>མིག་སྔའི་འཕེལ་རིམ།</translation>
    </message>
</context>
<context>
    <name>UsbConnWin</name>
    <message>
        <location filename="../ui/initconnectwin/usbconnwin.cpp" line="18"/>
        <source>Complete the connection tutorial</source>
        <translation>འབྲེལ་མཐུད་ཀྱི་ཟུར་ཁྲིད་ལེགས་འགྲུབ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/usbconnwin.cpp" line="19"/>
        <source>Mobile phone model</source>
        <translation>ཁ་པར་གྱི་བཟོ་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/usbconnwin.h" line="30"/>
        <source>vivo</source>
        <translation>vivo</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/usbconnwin.h" line="31"/>
        <source>HUAWEI</source>
        <translation>ཧྭ་ཝེ།</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/usbconnwin.h" line="32"/>
        <source>Xiaomi</source>
        <translation>ཞའོ་སྨི།</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/usbconnwin.h" line="33"/>
        <source>SAMSUNG</source>
        <translation>སྐར་གསུམ་ཀུང་སི།</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/usbconnwin.h" line="34"/>
        <source>OPPO</source>
        <translation>OPPO</translation>
    </message>
    <message>
        <location filename="../ui/initconnectwin/usbconnwin.h" line="35"/>
        <source>Other</source>
        <translation>དེ་མིན།</translation>
    </message>
</context>
<context>
    <name>VideoForm</name>
    <message>
        <location filename="../projection/device/deviceui/videoform.cpp" line="794"/>
        <source>Control device not supported</source>
        <translation>རྒྱབ་སྐྱོར་མི་བྱེད་པའི་ཚོད་འཛིན་སྒྲིག་ཆས་ལ་སྦྲེལ་མཐུད་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>VideoTitle</name>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="40"/>
        <source>mobile-assistant</source>
        <translation>ཁ་པར་ལས་རོགས་པ།</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="119"/>
        <location filename="../projection/uibase/videotitle.cpp" line="163"/>
        <source>Hide Navigation Button</source>
        <translation>ཕྱོགས་སྟོན་ར་བ་སྦས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="120"/>
        <source>Stay on top</source>
        <translation>རྩེ་མོར་བཞག</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="121"/>
        <location filename="../projection/uibase/videotitle.cpp" line="187"/>
        <source>FullScreen</source>
        <translation>བརྙན་ཤེལ་ཆ་ཚང་།</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="122"/>
        <source>Mouse sensitivity</source>
        <translation>འཁོར་ལོའི་ཚོར་སྐྱེན་ཚད།</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="123"/>
        <source>Quit</source>
        <translation>ཕྱིར་འཐེན་བྱ་རྒྱུ།</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="160"/>
        <source>Show Navigation Button</source>
        <translation>ཕྱོགས་སྟོན་གྱི་སྒྲོག་གུ་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="173"/>
        <source>Stay On Top</source>
        <translation>རྩེ་མོར་བཞག</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="177"/>
        <source>Cancel Stay On Top</source>
        <translation>རྩེ་མོ་འཇོག་པ་མེད་པར་བཟོས།</translation>
    </message>
    <message>
        <location filename="../projection/uibase/videotitle.cpp" line="190"/>
        <source>Cancel FullScreen</source>
        <translation>བརྙན་ཤེལ་ཧྲིལ་བོ་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>videoForm</name>
    <message>
        <location filename="../projection/device/deviceui/videoform.ui" line="17"/>
        <source>kylin-mobile-assistant</source>
        <translation>ཁ་པར་ལས་རོགས་པ།</translation>
    </message>
</context>
</TS>
