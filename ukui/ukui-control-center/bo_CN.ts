<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="bo_CN">
<context>
    <name>About</name>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="561"/>
        <source>System Summary</source>
        <translation>མ་ལག་ཕྱོགས་བསྡོམས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="607"/>
        <source>Kernel</source>
        <translation>ནང་སྙིང་།</translation>
        <extra-contents_path>/About/Kernel</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="609"/>
        <source>CPU</source>
        <translation>CPU</translation>
        <extra-contents_path>/About/CPU</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="611"/>
        <source>Memory</source>
        <translation>ནང་སྙིང་།</translation>
        <extra-contents_path>/About/Memory</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="563"/>
        <location filename="../../../plugins/system/about/about.cpp" line="909"/>
        <source>Disk</source>
        <translation>མཁྲེགས་སྡེར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="62"/>
        <source>About and Support</source>
        <translation>འབྲེལ་ཡོད་དང་རྒྱབ་སྐྱོར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="221"/>
        <source>Version Number</source>
        <translation>པར་གཞིའི་ཨང་གྲངས།</translation>
    </message>
    <message>
        <source>InterVersion</source>
        <translation type="vanished">ཕན་ཚུན་བརྗེ་རེས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="240"/>
        <source>Patch Version</source>
        <translation>མ་ལག་གི་ལྷན་པའི་པར་གཞིའི་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="262"/>
        <source>HostName</source>
        <translation>གཙོ་སྐྱོང་བྱེད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="398"/>
        <source>Privacy and agreement</source>
        <translation>སྒེར་གྱི་གསང་དོན་དང་གྲོས་མཐུན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="409"/>
        <source>Send optional diagnostic data</source>
        <translation>བསལ་འདེམས་ཀྱི་བརྟག་དཔྱད་གཞི་གྲངས་སྐུར་སྐྱེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="411"/>
        <source>By sending us diagnostic data, improve the system experience and solve your problems faster</source>
        <translation>ང་ཚོར་ནད་གཞི་བརྟག་དཔྱད་ཀྱི་གཞི་གྲངས་བསྐུར་ནས་མ་ལག་གི་ཉམས་མྱོང་ལེགས་བཅོས་བྱས་ཏེ་ཁྱོད་ཀྱི་གནད་དོན་ཐག་གཅོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="462"/>
        <source>Copyright © 2009-%1 KylinSoft. All rights reserved.</source>
        <translation>པར་དབང་ཡོད་ཚད©་2009-%1 KylinSoft.ཆི་ལིན་མཉེན་ཆས་ཀྱིས་དབང་ཆ་ཚང་མ་སོར་བཞག་བྱས་ཡོད་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="472"/>
        <source>&lt;&lt;Protocol&gt;&gt;</source>
        <translation>འགན་མེད་གྲོས་མཐུན་ཚོད་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="474"/>
        <source>and</source>
        <translation>དང་།།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="475"/>
        <source>&lt;&lt;Privacy&gt;&gt;</source>
        <translation>་སྤྱོད་མཁན་གྱི་གསང་བའི་གྲོས་མཐུན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="562"/>
        <source>Support</source>
        <translation>རྒྱབ་སྐྱོར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="565"/>
        <source>Wechat code scanning obtains HP professional technical support</source>
        <translation>འཕྲིན་ཕྲན་ཨང་གྲངས་ལ་ཞིབ་བཤེར་བྱས་ནས་HPཆམ་ལས་ལག་རྩལ་གྱི་རྒྱབ་སྐྱོར་ཐོབ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="566"/>
        <source>See more about Kylin Tianqi edu platform</source>
        <translation>ཆི་ལིན་ཐེན་ཆི་སློབ་གསོའི་སྟེགས་བུའི་ཆ་འཕྲིན་མང་པོར་ལྟ་ཞིབ་བྱས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="585"/>
        <source>Learn more HP user manual&gt;&gt;</source>
        <translation>སྔར་ལས་ལྷག་པའི་སྒོ་ནས་HPབེད་སྤྱོད་བྱེད་མཁན་གྱི་ལག་དེབ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="593"/>
        <source>See user manual&gt;&gt;</source>
        <translation>སྤྱོད་མཁན་གྱི་ལག་དེབ་ལ་ཞིབ་བཤེར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="605"/>
        <source>Version</source>
        <translation>པར་གཞིའི་མིང་།</translation>
        <extra-contents_path>/About/version</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="613"/>
        <source>Desktop</source>
        <translation>ཅོག་ངོས།</translation>
        <extra-contents_path>/About/Desktop</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="615"/>
        <source>User</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
        <extra-contents_path>/About/User</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="617"/>
        <source>Status</source>
        <translation>མ་ལག་གྱི་གནས་ཚུལ།</translation>
        <extra-contents_path>/About/Status</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="618"/>
        <source>Serial</source>
        <translation>གོ་རིམ་གྱི་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="619"/>
        <source>DateRes</source>
        <translation>ཞབས་ཞུའི་དུས་ཚོད་ཐོན་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="698"/>
        <location filename="../../../plugins/system/about/about.cpp" line="981"/>
        <source>Extend</source>
        <translation>དུས་འགྱངས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="902"/>
        <location filename="../../../plugins/system/about/about.cpp" line="911"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1402"/>
        <source>avaliable</source>
        <translation>འགན་འཁྲི་འཁུར་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="979"/>
        <location filename="../../../plugins/system/about/about.cpp" line="1252"/>
        <source>expired</source>
        <translation>དུས་བཀག་ཐིམ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="1205"/>
        <source>The system needs to be restarted to set the HostName, whether to reboot</source>
        <translation>རྩིས་འཁོར་གྱི་མིང་དག་བཅོས་བྱས་ཟིན།་མ་ལག་ཡང་བསྐྱར་སྒོ་ཕྱེ་ན་ད་གཟོད་རྒྱུན་ལྡན་ལྟར་བཀོལ་སྤྱོད་བྱེད་ཐུབ།མྱུར་དུ་མ་ལག་ཡང་བསྐྱར་སྒོ་འབྱེད་པའི་བསམ་འཆར་བཏོན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="1206"/>
        <source>Reboot Now</source>
        <translation>ད་ལྟ་བསྐྱར་དུ་ལས་ཀ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="1207"/>
        <source>Reboot Later</source>
        <translation>རྗེས་སུ་ཡང་བསྐྱར་ཐེངས་གཅིག་ལ་སྒོ་འབྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="682"/>
        <location filename="../../../plugins/system/about/about.cpp" line="688"/>
        <source>Active</source>
        <translation>གྲུང་སློང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="64"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="678"/>
        <location filename="../../../plugins/system/about/about.cpp" line="686"/>
        <source>Inactivated</source>
        <translation>གྲུང་སློང་མ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="680"/>
        <source>Trial expiration time</source>
        <translation>འདྲི་གཅོད་དུས་ཚོད་ཐིམ་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/about.cpp" line="696"/>
        <source>Activated</source>
        <translation>གྲུང་སློང་བྱས་པ།</translation>
    </message>
</context>
<context>
    <name>AddAutoBoot</name>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="220"/>
        <source>Add autoboot program</source>
        <translation>རང་འགུལ་གྱིས་འཆར་གཞི་ཁ་སྣོན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="224"/>
        <source>Open</source>
        <translation>བཤེར་ཆས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="225"/>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="245"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="226"/>
        <source>Certain</source>
        <translation>གཏན་འཁེལ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="291"/>
        <source>desktop file not allowed add</source>
        <translation>བཀོལ་སྤྱོད་འདི་ཁ་སྣོན་བྱེད་མི་ཆོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="340"/>
        <source>desktop file not exist</source>
        <translation>desktopཡིག་ཆ་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="243"/>
        <source>select autoboot desktop</source>
        <translation>རང་འགུལ་གྱིས་མདུན་ངོས་སུ་བདམས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="166"/>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="221"/>
        <source>Name</source>
        <translation>གོ་རིམ་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="167"/>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="222"/>
        <source>Exec</source>
        <translation>གོ་རིམ་གྱི་ལམ་བུ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="168"/>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="223"/>
        <source>Comment</source>
        <translation>གོ་རིམ་ཞིབ་བརྗོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="236"/>
        <source>Desktop files(*.desktop)</source>
        <translation>ཅོག་ངོས་ཀྱི་ཡིག་ཆ། (*.desktop)</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/addautoboot.cpp" line="244"/>
        <source>Select</source>
        <translation>བདམས་ཐོན་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>AddBtn</name>
    <message>
        <location filename="../../../libukcc/widgets/AddBtn/addbtn.cpp" line="22"/>
        <source>Add</source>
        <translation>ཁ་སྣོན་བརྒྱབ་པ།</translation>
    </message>
</context>
<context>
    <name>AddInputMethodDialog</name>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.ui" line="26"/>
        <source>Select the input method to add</source>
        <translation>ནང་འཇུག་བྱེད་ཐབས་བདམས་ནས་ཁ་སྣོན་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.ui" line="82"/>
        <source>No</source>
        <translation>ཕྱིར་གཏོང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.ui" line="101"/>
        <source>Yes</source>
        <translation>ཁ་སྣོན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="5"/>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="6"/>
        <source>keyboard</source>
        <translation>མཐེབ་གཞོང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="5"/>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="6"/>
        <source>Tibetan</source>
        <translation>བོད་ཡིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="7"/>
        <source>With ASCII numbers</source>
        <translation>ASCIIགྲངས་ཀ་འདུས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addinputmethoddialog.cpp" line="15"/>
        <source>Input Method</source>
        <translation>ནང་འཇུག་བྱེད་ཐབས།</translation>
    </message>
</context>
<context>
    <name>AddLanguageDialog</name>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.ui" line="179"/>
        <source>No</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.ui" line="198"/>
        <source>Yes</source>
        <translation>ཁ་སྣོན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="19"/>
        <source>Add Language</source>
        <translation>ཁ་སྣོན་བྱས་པའི་སྐད་ཆ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="20"/>
        <source>Search</source>
        <translation>ཁྱོད་ཀྱིས་བཙལ་འདོད་པའི་ནང་དོན་ནང་འཇུག་བྱོས།</translation>
    </message>
</context>
<context>
    <name>AptProxyDialog</name>
    <message>
        <location filename="../../../plugins/network/proxy/aptproxydialog.cpp" line="24"/>
        <source>Set Apt Proxy</source>
        <translation>Apt་ལས་ཚབ་བཀོད་སྒྲིག་བྱས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/aptproxydialog.cpp" line="41"/>
        <source>Server Address</source>
        <translation>ཞབས་ཞུ་ཆས་ཀྱི་ས་གནས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/aptproxydialog.cpp" line="59"/>
        <source>Port</source>
        <translation>མཐུད་ཁ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/aptproxydialog.cpp" line="80"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/aptproxydialog.cpp" line="84"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>Area</name>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="26"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="44"/>
        <source>Area</source>
        <translation>སྐད་བརྡའི་ས་ཁོངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="59"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="169"/>
        <source>Language Format</source>
        <translation>སྐད་ཆའི་རྣམ་གཞག</translation>
        <extra-contents_path>/Area/Regional Format</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="156"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="432"/>
        <source>Current Region</source>
        <translation>དུས་ཚོད།དུས་ཚོད།དངུལ་ལོར་གྱི་རྣམ་གཞག་བཅས་མངོན་པའི་ས་ཁོངས།</translation>
        <extra-contents_path>/Area/Current Region</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="264"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="434"/>
        <source>Calendar</source>
        <translation>ལོ་ཐོ།</translation>
        <extra-contents_path>/Area/Calendar</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="350"/>
        <source>First Day Of The Week</source>
        <translation>གཟའ་འཁོར་གཅིག་གི་ཉིན་དང་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="433"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="438"/>
        <source>Date</source>
        <translation>ཟླ་ཚེས།</translation>
        <extra-contents_path>/Area/Date</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="516"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="440"/>
        <source>Time</source>
        <translation>དུས་ཚོད།</translation>
        <extra-contents_path>/Area/Time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="555"/>
        <location filename="../../../plugins/time-language/area/area.cpp" line="171"/>
        <source>System Language</source>
        <translation>མ་ལག་གི་སྐད་ཆ།</translation>
        <extra-contents_path>/Area/system language</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="445"/>
        <source>lunar</source>
        <translation>ལུགས་རྩིས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.ui" line="589"/>
        <source>TextLabel</source>
        <translation>ཡི་གེ་ལ་པེར་གྱིས་བཤད་རྒྱུར</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="174"/>
        <source>Language for system windows,menus and web pages</source>
        <translation>མ་ལག་གི་སྒེའུ་ཁུང་དང་།འདེམས་པང་། དྲ་ངོས་བཅས་ཀྱི་སྐད་ཆ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="291"/>
        <source>US</source>
        <translation>ཨ་རི།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="292"/>
        <source>CN</source>
        <translation>ཀྲུང་གོ</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="301"/>
        <source>monday</source>
        <translation>གཟའ་ཟླ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="302"/>
        <source>sunday</source>
        <translation>གཟའ་ཉི་མ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="436"/>
        <source>First Day Of Week</source>
        <translation>གཟའ་འཁོར་གཅིག་གི་ཉིན་དང་པོ།</translation>
        <extra-contents_path>/Area/First Day Of Week</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="457"/>
        <source>12 Hours</source>
        <translation>ཆུ་ཚོད་12ནང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="458"/>
        <source>24 Hours</source>
        <translation>ཆུ་ཚོད་24རིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="577"/>
        <source>Settings</source>
        <translation>སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="580"/>
        <source>Modify the current region need to logout to take effect, whether to logout?</source>
        <translation>ད་ལྟའི་ས་ཁོངས་ལ་བཟོ་བཅོས་བརྒྱབ་ན་ད་གཟོད་ནུས་པ་ཐོན་ཐུབ། ཐོ་འགོད་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="581"/>
        <source>Logout later</source>
        <translation>རྗེས་སུ་ཐོ་འགོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="582"/>
        <source>Logout now</source>
        <translation>ད་ལྟ་ཐོ་འགོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="584"/>
        <source>Modify the first language need to reboot to take effect, whether to reboot?</source>
        <translation>སྐད་རིགས་དང་པོར་བཟོ་བཅོས་བརྒྱབ་ན་ད་གཟོད་ནུས་པ་ཐོན་ཐུབ། བསྐྱར་དུ་ཐོན་ཐུབ་མིན་ལ་བཟོ་བཅོས་རྒྱག་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="585"/>
        <source>Reboot later</source>
        <translation>ཅུང་ཙམ་ནས་ཡང་བསྐྱར་སྒོ་ཕྱེ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="586"/>
        <source>Reboot now</source>
        <translation>ད་ལྟ་ཡང་བསྐྱར་སྒོ་ཕྱེ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/area.cpp" line="442"/>
        <source>solar calendar</source>
        <translation>སྤྱི་ཟླ།</translation>
    </message>
</context>
<context>
    <name>AutoBoot</name>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="473"/>
        <source>Desktop files(*.desktop)</source>
        <translation>ཅོག་ངོས་ཡིག་ཆ། (*.desktop)</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="481"/>
        <source>select autoboot desktop</source>
        <translation>རང་འགུལ་གྱིས་མདུན་ངོས་སུ་བདམས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="482"/>
        <source>Select</source>
        <translation>བདམས་ཐོན་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="483"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="631"/>
        <source>Add</source>
        <translation>ཁ་སྣོན་བརྒྱབ་པ།</translation>
        <extra-contents_path>/Autoboot/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="638"/>
        <source>Autoboot Settings</source>
        <translation>རང་འགུལ་གྱིས་སྒྲིག་བཀོད་སྒྲིག་བྱེད་པ།</translation>
        <extra-contents_path>/autoboot/Autoboot Settings</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="90"/>
        <source>Auto Boot</source>
        <translation>རང་འགུལ་གྱིས་སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="249"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
</context>
<context>
    <name>Backup</name>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="53"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="43"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="113"/>
        <source>Backup</source>
        <translation>རྗེས་གྲབས་དཔུང་ཁག</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="69"/>
        <source>Back up your files to other drives, and when the original files are lost, damaged, or deleted, 
you can restore them to ensure the integrity of your system.</source>
        <translation>ཡིག་ཆ་དེ་སྒུལ་བྱེད་འཕྲུལ་འཁོར་གཞན་དག་ལ་རྒྱབ་སྐྱོར་བྱས་ནས་སྔར་གྱི་ཡིག་ཆ་བོར་བརླག་ཏུ་སོང་བ་དང་། གཏོར་བརླག་ཐེབས་པའམ་ཡང་ན་བསུབ་པའི་སྐབས་སུ། 
དེ་དག་སླར་གསོ་བྱས་ནས་ཁྱེད་ཚོའི་མ་ལག་གི་ཆ་ཚང་རང་བཞིན་ལ་ཁག་ཐེག་བྱེད་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="113"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="160"/>
        <source>Begin backup</source>
        <translation>རྗེས་གྲབས་ལས་དོན་སྤེལ་འགོ་ཚུགས།</translation>
        <extra-contents_path>/Backup/Begin backup</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="157"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="137"/>
        <source>Restore</source>
        <translation>སླར་གསོ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="173"/>
        <source>View a list of backed-upfiles to backed up files to the system</source>
        <translation>རྒྱབ་སྐྱོར་བྱས་པའི་ཡིག་ཆ་དེ་མ་ལག་ལ་རྒྱབ་སྐྱོར་བྱེད་པའི་མིང་ཐོར་ལྟ་ཞིབ་བྱས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.ui" line="213"/>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="162"/>
        <source>Begin restore</source>
        <translation>སླར་གསོ་བྱེད་འགོ་ཚུགས།</translation>
        <extra-contents_path>/Backup/Begin restore</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="115"/>
        <source>Back up your files to other drives and restore them when the source files are lost, damaged, or deleted to ensure the integrity of the system.</source>
        <translation>ཡིག་ཆ་དེ་སྒུལ་ཤུགས་གཞན་དག་ལ་རྒྱབ་སྐྱོར་བྱས་ནས་འབྱུང་ཁུངས་ཀྱི་ཡིག་ཆ་བོར་བརླག་ཏུ་སོང་བ་དང་། གཏོར་བརླག་ཐེབས་པའམ་ཡང་ན་བསུབ་རྗེས་སླར་གསོ་བྱས་ཏེ་མ་ལག་གི་ཆ་ཚང་རང་བཞིན་ལ་ཁག་ཐེག་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="139"/>
        <source>View the backup list and restore the backup file to the system</source>
        <translation>རྗེས་གྲབས་རེའུ་མིག་ལ་ལྟ་ཞིབ་བྱས་ནས་རྗེས་གྲབས་ཡིག་ཆ་མ་ལག་ནང་དུ་སླར་གསོ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/security-updates/backup/backup.cpp" line="158"/>
        <source>Backup and Restore</source>
        <translation>རྗེས་གྲབས་དང་སླར་གསོ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/backup_intel/backup.ui" line="147"/>
        <source>All data stored on the computer will be permanently erased,and the system will revert to 
                                its original factory state when this operation is completed.</source>
        <translation>རྩིས་འཁོར་ནང་ཉར་ཚགས་བྱས་པའི་གཞི་གྲངས་ཚང་མ་དུས་གཏན་དུ་རྩིས་མེད་དུ་གཏོང་རྒྱུ་དང་། མ་ལག་དེ་སླར་ཡང་རྩིས་མེད་དུ་གཏོང་རྒྱུ། 
                                གཉེར་སྐྱོང་འདི་ལེགས་འགྲུབ་བྱུང་བའི་སྐབས་སུ་དེའི་སྔར་གྱི་བཟོ་གྲྭའི་རྣམ་པ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/backup_intel/backup.ui" line="216"/>
        <location filename="../../../plugins/system/backup_intel/backup.cpp" line="76"/>
        <source>Clear and restore</source>
        <translation>ནང་དོན་དང་སྒྲིག་བཀོད་ཚང་མ་མེད་པར་བཟོས།</translation>
        <extra-contents_path>/Backup/Clear and restore</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/backup_intel/backup.cpp" line="42"/>
        <source>System Recovery</source>
        <translation>མ་ལག་སླར་གསོ།</translation>
    </message>
</context>
<context>
    <name>BrightnessFrame</name>
    <message>
        <location filename="../../../plugins/system/display/brightnessFrame.cpp" line="37"/>
        <source>Failed to get the brightness information of this monitor</source>
        <translation>ལྟ་ཞིབ་ཚད་ལེན་འཕྲུལ་ཆས་འདིའི་གསལ་ཚད་ཀྱི་ཆ་འཕྲིན་ཐོབ་མ་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>ChangeFaceIntelDialog</name>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changefaceinteldialog.ui" line="88"/>
        <location filename="../../../plugins/account/userinfo_intel/changefaceinteldialog.cpp" line="44"/>
        <source>Change User Face</source>
        <translation>སྤྱོད་མཁན་གྱི་ངོ་གདོང་བསྒྱུར་བཅོས།</translation>
        <extra-contents_path>/UserinfoIntel/Change User Face</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changefaceinteldialog.ui" line="280"/>
        <source>History</source>
        <translation>མཚན་ཉིད་རང་འཇོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changefaceinteldialog.ui" line="388"/>
        <source>System</source>
        <translation>མ་ལག་རང་ཁྱེར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changefaceinteldialog.ui" line="476"/>
        <location filename="../../../plugins/account/userinfo_intel/changefaceinteldialog.cpp" line="360"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changefaceinteldialog.ui" line="511"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changefaceinteldialog.cpp" line="355"/>
        <source>select custom face file</source>
        <translation>རང་ཉིད་ཀྱི་མགོ་པར་ཡིག་ཆ་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changefaceinteldialog.cpp" line="356"/>
        <source>Select</source>
        <translation>བདམས་ཐོན་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changefaceinteldialog.cpp" line="357"/>
        <source>Position: </source>
        <translation>གོ་གནས་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changefaceinteldialog.cpp" line="358"/>
        <source>FileName: </source>
        <translation>ཡིག་ཆའི་མིང་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changefaceinteldialog.cpp" line="359"/>
        <source>FileType: </source>
        <translation>ཡིག་ཆའི་རིགས་དབྱིབས་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changefaceinteldialog.cpp" line="374"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changefaceinteldialog.cpp" line="374"/>
        <source>The avatar is larger than 2M, please choose again</source>
        <translation>པར་དབང་དེ་2Mལས་ཆེ་བ་དང་། ཡང་བསྐྱར་གདམ་ག་གནང་རོགས།</translation>
    </message>
</context>
<context>
    <name>ChangeGroupIntelDialog</name>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changegroupinteldialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changegroupinteldialog.ui" line="119"/>
        <source>User Group Settings</source>
        <translation>སྤྱོད་མཁན་ཚོགས་པའི་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changegroupinteldialog.ui" line="149"/>
        <source>User groups available in the system</source>
        <translation>མ་ལག་ཁྲོད་དུ་སྤྱོད་ཆོག་པའི་སྤྱོད་མཁན་ཚོགས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changegroupinteldialog.cpp" line="119"/>
        <source>Add user group</source>
        <translation>སྤྱོད་མཁན་གྱི་ཚོ་ཆུང་ཁ་སྣོན།</translation>
    </message>
</context>
<context>
    <name>ChangePhoneIntelDialog</name>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.ui" line="77"/>
        <source>changephone</source>
        <translation>ཁ་པར་ཨང་གྲངས་བརྗེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.ui" line="190"/>
        <source>Please input old phone num</source>
        <translation>ཁ་པར་རྙིང་བ་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.ui" line="242"/>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.ui" line="429"/>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.ui" line="452"/>
        <source>TextLabel</source>
        <translation>ཡི་གེ་ལ་པེར་གྱིས་བཤད་རྒྱུར</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.ui" line="366"/>
        <source>GetVerifyCode</source>
        <translation>ར་སྤྲོད་ཨང་གྲངས་བླངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.ui" line="586"/>
        <source>submit</source>
        <translation>གོང་འབུལ་ཞུས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="53"/>
        <source>Change Phone</source>
        <translation>ཁ་པར་ཨང་གྲངས་བརྗེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="92"/>
        <source>Phone number</source>
        <translation>ཁ་པར་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="93"/>
        <source>SMS verification code</source>
        <translation>འཕྲིན་ཐུང་ཞིབ་བཤེར་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="125"/>
        <source>Please input old phone number</source>
        <translation>ཁ་པར་ཨང་གྲངས་རྙིང་བ་ནང་འཇུག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="126"/>
        <source>Next</source>
        <translation>གོམ་སྟབས་རྗེས་མར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="129"/>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="252"/>
        <source>Please enter new mobile number</source>
        <translation>ཁ་པར་ཨང་གྲངས་གསར་པར་འཇུག་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="130"/>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="253"/>
        <source>Submit</source>
        <translation>གོང་འབུལ་ཞུས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="146"/>
        <source>changed success</source>
        <translation>འགྱུར་ལྡོག་བྱུང་བའི་ལེགས་འགྲུབ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="147"/>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="323"/>
        <source>You have successfully modified your phone</source>
        <translation>ཁྱེད་ཀྱིས་ལག་ཁྱེར་ཁ་པར་ལ་བཟོ་བཅོས་ལེགས་འགྲུབ་བྱུང་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="198"/>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="222"/>
        <source>Recapture</source>
        <translation>ཕྱིར་འཕྲོག་ལེན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="212"/>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="272"/>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="307"/>
        <source>Network connection failure, please check</source>
        <translation>དྲ་སྦྲེལ་ལ་སྐྱོན་ཤོར་བས་ཞིབ་བཤེར་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="232"/>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="257"/>
        <source>GetCode</source>
        <translation>ར་སྤྲོད་ཨང་ཀི་ཐོབ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="265"/>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="297"/>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="326"/>
        <source>Phone is lock,try again in an hour</source>
        <translation>ཁ་པར་ལ་ཟྭ་བརྒྱབ་ནས་དུས་ཚོད་གཅིག་འགོར་རྗེས་ཡང་བསྐྱར་ཚོད་ལྟ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="268"/>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="300"/>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="329"/>
        <source>Phone code is wrong</source>
        <translation>ར་སྤྲོད་ཨང་གྲངས་ནོར་སོང་། ཡང་བསྐྱར་ཚོད་ལྟ་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="275"/>
        <source>Current login expired,using wechat code!</source>
        <translation>ད་ལྟའི་ཐོ་འགོད་དུས་ཚོད་ཐིམ་ནས་འཕྲིན་ཕྲན་གྱི་ཚབ་རྟགས་བཀོལ་སྤྱོད་བྱས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="278"/>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="310"/>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="336"/>
        <source>Unknown error, please try again later</source>
        <translation>ནོར་འཁྲུལ་མི་ཤེས་པས་ཕྱིས་སུ་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་བྱེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="285"/>
        <source>Phone can not same</source>
        <translation>ཁ་པར་གཅིག་འདྲ་ཡོང་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="294"/>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="322"/>
        <source>finished</source>
        <translation>ལེགས་གྲུབ་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="304"/>
        <location filename="../../../plugins/account/userinfo_intel/changephoneinteldialog.cpp" line="333"/>
        <source>Phone number already in used!</source>
        <translation>བཀོལ་སྤྱོད་བྱས་ཟིན་པའི་ཁ་པར་ཨང་གྲངས་རེད།</translation>
    </message>
</context>
<context>
    <name>ChangePinIntelDialog</name>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changepininteldialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changepininteldialog.ui" line="74"/>
        <source>Change Password</source>
        <translation>གསང་གྲངས་བརྗེ་བ།</translation>
    </message>
</context>
<context>
    <name>ChangePwdIntelDialog</name>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changepwdinteldialog.ui" line="119"/>
        <source>Change Pwd</source>
        <translation>གསང་གྲངས་བརྗེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changepwdinteldialog.ui" line="603"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changepwdinteldialog.ui" line="643"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changepwdinteldialog.cpp" line="186"/>
        <source>General Pwd</source>
        <translation>སྤྱི་སྤྱོད་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changepwdinteldialog.cpp" line="198"/>
        <source>Old Password</source>
        <translation>མིག་སྔའི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changepwdinteldialog.cpp" line="199"/>
        <location filename="../../../plugins/account/userinfo_intel/changepwdinteldialog.cpp" line="575"/>
        <source>New Password</source>
        <translation>གསང་གྲངས་གསར་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changepwdinteldialog.cpp" line="200"/>
        <location filename="../../../plugins/account/userinfo_intel/changepwdinteldialog.cpp" line="576"/>
        <source>New Password Identify</source>
        <translation>གསང་གྲངས་གསར་པ་གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changepwdinteldialog.cpp" line="332"/>
        <location filename="../../../plugins/account/userinfo_intel/changepwdinteldialog.cpp" line="341"/>
        <source>Please set different pwd!</source>
        <translation>གསང་གྲངས་གསར་བ་དེ་མིག་སྔའི་གསང་གྲངས་དང་འདྲ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changepwdinteldialog.cpp" line="349"/>
        <location filename="../../../plugins/account/userinfo_intel/changepwdinteldialog.cpp" line="560"/>
        <source>Inconsistency with pwd</source>
        <translation>གསང་གྲངས་གསར་བ་དང་གཅིག་མཐུན་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changepwdinteldialog.cpp" line="414"/>
        <source>Old pwd is wrong!</source>
        <translation>མིག་སྔའི་གསང་གྲངས་ནོར་འཁྲུལ་རེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changepwdinteldialog.cpp" line="416"/>
        <source>New pwd is too similar with old pwd!</source>
        <translation>གསང་གྲངས་གསར་བ་དང་མིག་སྔའི་གསང་གྲངས་འདྲ་མཚུངས་རེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changepwdinteldialog.cpp" line="421"/>
        <source>Check old pwd failed because of unknown reason!</source>
        <translation>རྒྱུ་མཚན་མི་ཤེས་པའི་རྐྱེན་གྱིས་ཞིབ་བཤེར་རྙིང་བ་ཕམ་སོང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changepwdinteldialog.cpp" line="537"/>
        <source>Password length needs to more than %1 character!</source>
        <translation>གསང་གྲངས་ཀྱི་རིང་ཚད་ནི་%1ལས་བརྒལ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changepwdinteldialog.cpp" line="539"/>
        <source>Password length needs to less than %1 character!</source>
        <translation>གསང་གྲངས་ཀྱི་རིང་ཚད་ནི་%1མན་གྱི་ཡི་གེ་ཞིག་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changepwdinteldialog.cpp" line="547"/>
        <source>Password cannot be made up entirely by Numbers!</source>
        <translation>གསང་གྲངས་ནི་ཨང་ཀིས་ཡོངས་སུ་ཁ་གསབ་བྱེད་མི་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>ChangeTypeIntelDialog</name>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changetypeinteldialog.ui" line="108"/>
        <source>Change Account Type</source>
        <translation>རྩིས་ཐོའི་རིགས་དབྱིབས་བསྒྱུར་བཅོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changetypeinteldialog.ui" line="409"/>
        <source>standard user</source>
        <translation>ཚད་ལྡན་སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changetypeinteldialog.ui" line="422"/>
        <source>Standard users can use most software, but cannot install software and change system settings</source>
        <translation>ཚད་ལྡན་སྤྱོད་མཁན་གྱིས་མཉེན་ཆས་མང་ཆེ་བ་བཀོལ་སྤྱོད་བྱས་ཆོག་མོད། འོན་ཀྱང་མཉེན་ཆས་སྒྲིག་སྦྱོར་དང་མ་ལག་གི་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changetypeinteldialog.ui" line="543"/>
        <source>administrator</source>
        <translation>སྒྲིག་བཀོད་དོ་དམ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changetypeinteldialog.ui" line="556"/>
        <source>Administrators can make any changes they need</source>
        <translation>དོ་དམ་པའི་ཐོ་ཁོངས་ཀྱིས་མ་ལག་གང་རུང་གི་བཀོད་སྒྲིག་བསྒྱུར་ཆོག་མཉེན་ཆས་སྒྲིག་སྦྱོར་དང་རིམ་འཕར་མཉེན་ཆས་ཀྱང་འདུས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changetypeinteldialog.ui" line="579"/>
        <source>Make sure that there is at least one administrator on the computer</source>
        <translation>གསལ་བརྡ།རྩིས་འཁོར་འདིའི་སྟེང་དུ་མ་མཐའ་ཡང་སྤྱོད་མཁན་གཅིག་ལ་དོ་དམ་པའི་དབང་ཆ་ཡོད་པ་ཁག་ཐེག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changetypeinteldialog.ui" line="619"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changetypeinteldialog.ui" line="654"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>ChangeUserLogo</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="111"/>
        <source>User logo</source>
        <translation>སྤྱོད་མཁན་གྱི་མཚོན་རྟགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="136"/>
        <source>System Logos</source>
        <translation>མ་ལག་གི་མཚོན་རྟགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="145"/>
        <source>Select Local Logo</source>
        <translation>ས་གནས་དེ་གའི་མཚོན་རྟགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="154"/>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="294"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="156"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="289"/>
        <source>select custom face file</source>
        <translation>རང་ཉིད་ཀྱི་མགོ་པར་ཡིག་ཆ་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="290"/>
        <source>Select</source>
        <translation>བདམས་ཐོན་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="291"/>
        <source>Position: </source>
        <translation>གོ་གནས་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="292"/>
        <source>FileName: </source>
        <translation>ཡིག་ཆའི་མིང་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="293"/>
        <source>FileType: </source>
        <translation>ཡིག་ཆའི་རིགས་དབྱིབས་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="309"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserlogo.cpp" line="310"/>
        <source>The avatar is larger than 1M, please choose again</source>
        <translation>པར་དབང་དེ་1Mལས་ཆེ་བ་དང་། ཡང་བསྐྱར་གདམ་ག་གནང་རོགས།</translation>
    </message>
</context>
<context>
    <name>ChangeUserNickname</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="31"/>
        <source>Set Nickname</source>
        <translation>མཚང་མིང་གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="54"/>
        <source>UserName</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="71"/>
        <source>NickName</source>
        <translation>མིང་འདོགས་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="79"/>
        <source>nickName already in use.</source>
        <translation>བཀོལ་སྤྱོད་བྱེད་བཞིན་པའི་མཚང་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="115"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="118"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusernickname.cpp" line="154"/>
        <source>The length must be 1~%1 characters!</source>
        <translation>རིང་ཚད་ནི་ངེས་པ1~%1  ཡི་ཡི་གེ་ཡིན་དགོས།</translation>
    </message>
</context>
<context>
    <name>ChangeUserPwd</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="97"/>
        <source>Change password</source>
        <translation>གསང་གྲངས་བརྗེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="102"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="115"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="518"/>
        <source>Current Pwd</source>
        <translation>མིག་སྔའི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="144"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="155"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="519"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="527"/>
        <source>New Pwd</source>
        <translation>གསང་གྲངས་གསར་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="184"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="188"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="520"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="528"/>
        <source>Sure Pwd</source>
        <translation>གསང་གྲངས་གཏན་འཁེལ་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="246"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="250"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="407"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="315"/>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="591"/>
        <source>Inconsistency with pwd</source>
        <translation>གསང་གྲངས་གསར་བ་དང་གཅིག་མཐུན་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="390"/>
        <source>Pwd Changed Succes</source>
        <translation>གསང་གྲངས་བཟོ་བཅོས་རྒྱབ་པ་ལེགས་འགྲུབ་བྱུང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="397"/>
        <source>Authentication failed, input authtok again!</source>
        <translation>བདེན་དཔང་ར་སྤྲོད་བྱེད་མ་ཐུབ་པར་ཡང་བསྐྱར་ནང་འཇུག་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="553"/>
        <source>Contains illegal characters!</source>
        <translation>དེའི་ནང་དུ་ཁྲིམས་འགལ་གྱི་ཡིག་རྟགས་འདུས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="664"/>
        <source>current pwd cannot be empty!</source>
        <translation>ད་ལྟའི་གསང་གྲངས་ནི་སྟོང་པ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="669"/>
        <source>new pwd cannot be empty!</source>
        <translation>གསང་གྲངས་གསར་བ་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeuserpwd.cpp" line="674"/>
        <source>sure pwd cannot be empty!</source>
        <translation>གསང་གྲངས་གཏན་འཁེལ་བྱེད་པ་ནི་སྟོང་བ་ཡིན་མི་སྲིད་པ</translation>
    </message>
</context>
<context>
    <name>ChangeUserType</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="25"/>
        <source>UserType</source>
        <translation>སྤྱོད་མཁན་གྱི་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="70"/>
        <source>Select account type (Ensure have admin on system):</source>
        <translation>རྩིས་ཐོའི་རིགས་བདམས་ནས་(མ་ལག་ལ་སྲིད་འཛིན་དོ་དམ་ཡོད་པར་ཁག་ཐེག་བྱེད་པ།）</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="80"/>
        <source>administrator</source>
        <translation>སྒྲིག་བཀོད་དོ་དམ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="82"/>
        <source>standard user</source>
        <translation>ཚད་ལྡན་སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="84"/>
        <source>change system settings, install and upgrade software.</source>
        <translation>དོ་དམ་པའི་ཐོ་ཁོངས་ཀྱིས་མ་ལག་གི་བཀོད་སྒྲིག་གང་རུང་བསྒྱུར་ཆོག་ནང་འཇུག་དང་རིམ་འཕར་མཉེན་ཆས་ཀྱང་འདུས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="86"/>
        <source>use most software, cannot change system settings.</source>
        <translation>ཚད་ལྡན་སྤྱོད་མཁན་གྱིས་མཉེན་ཆས་མང་ཆེ་བ་བཀོལ་ཆོག་འོན་ཀྱང་མ་ལག་བཀོད་སྒྲིག་ལ་བཟོ་བཅོས་བྱེད་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="139"/>
        <source>Note: Effective After Logout!!!</source>
        <translation>མཉམ་འཇོག་བྱོས།སྤྱོད་མཁན་གྱི་རིགས་ཐོ་སུབ་རྗེས་ནུས་པ་ཐོན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="150"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusertype.cpp" line="153"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
</context>
<context>
    <name>ChangeValidIntelDialog</name>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.ui" line="180"/>
        <source>Password Validity Setting</source>
        <translation>གསང་གྲངས་གོ་ཆོད་པའི་དུས་ཡུན་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.ui" line="312"/>
        <source>Current passwd validity:</source>
        <translation>མིག་སྔའི་འགག་སྒོ་ལས་བརྒལ་བའི་གོ་ཆོད་པའི་དུས་ཡུན་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.ui" line="394"/>
        <source>Adjust date to:</source>
        <translation>ལེགས་སྒྲིག་བྱས་པའི་དུས་ཚོད་ནི།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.ui" line="493"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.ui" line="500"/>
        <source>Certain</source>
        <translation>གཏན་འཁེལ་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>ChangtimeDialog</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="161"/>
        <source>time</source>
        <translation>དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="162"/>
        <source>year</source>
        <translation>ལོ་གཅིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="163"/>
        <source>month</source>
        <translation>ཟླ་བ་གཅིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="164"/>
        <source>day</source>
        <translation>ཨང་གྲངས།</translation>
    </message>
</context>
<context>
    <name>ColorDialog</name>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="32"/>
        <source>Dialog</source>
        <translation>དུས་ཚོད་བཟོ་བཅོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="86"/>
        <source>选择自定义颜色</source>
        <translation>རང་ཉིད་ཀྱི་ཁ་དོག་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="236"/>
        <source>HEX</source>
        <translation>HEX</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="250"/>
        <source>RGB</source>
        <translation>མི་དམངས་ཤོག་སྒོར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="411"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.ui" line="430"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/colordialog.cpp" line="95"/>
        <source>Custom color</source>
        <translation>རང་ཉིད་ཀྱི་ཁ་དོག</translation>
    </message>
</context>
<context>
    <name>CreateGroupDialog</name>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.ui" line="26"/>
        <source>Add New Group</source>
        <translation>ཚོགས་པ་གསར་པ་ཁ་སྣོན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="52"/>
        <source>Name</source>
        <translation>ཚོགས་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="63"/>
        <source>Id</source>
        <translation>IDཚོ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="76"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="74"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/creategroupdialog.cpp" line="48"/>
        <source>Add user group</source>
        <translation>སྤྱོད་མཁན་གྱི་ཚོ་ཆུང་ཁ་སྣོན།</translation>
    </message>
</context>
<context>
    <name>CreateGroupIntelDialog</name>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/creategroupinteldialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/creategroupinteldialog.ui" line="115"/>
        <source>Add New Group</source>
        <translation>ཚོགས་པ་གསར་པ་ཁ་སྣོན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/creategroupinteldialog.ui" line="144"/>
        <source>Group Name</source>
        <translation>ཚོ་ཆུང་གི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/creategroupinteldialog.ui" line="182"/>
        <source>Group Id</source>
        <translation>ཚོགས་པའི་ཐོབ་ཐང་ལག་ཁྱེར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/creategroupinteldialog.ui" line="234"/>
        <source>Group Members</source>
        <translation>ཚོ་ཆུང་གི་ཁོངས་མི།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/creategroupinteldialog.ui" line="344"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/creategroupinteldialog.ui" line="363"/>
        <source>Certain</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
</context>
<context>
    <name>CreateUserIntelDialog</name>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.ui" line="117"/>
        <source>Add New Account</source>
        <translation>སྤྱོད་མཁན་གསར་པ་ཁ་སྣོན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.ui" line="458"/>
        <source>Account Type</source>
        <translation>སྤྱོད་མཁན་གྱི་རིགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.ui" line="550"/>
        <source>standard user</source>
        <translation>ཚད་ལྡན་སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.ui" line="563"/>
        <source>Standard users can use most software, but cannot install the software and 
change system settings</source>
        <translation>ཚད་ལྡན་གྱི་ཐོ་ཁོངས་སུ་མཉེན་ཆས་མང་ཆེ་བ་བཀོལ་ཆོག་འོན་ཀྱང་མཉེན་ཆས་སྒྲིག་སྦྱོར་དང་མ་ལག་སྒྲིག་སྦྱོར་བསྒྱུར་མི་ཆོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.ui" line="666"/>
        <source>administrator</source>
        <translation>སྤྱོད་མཁན་དོ་དམ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.ui" line="679"/>
        <source>Administrators can make any changes they need</source>
        <translation>དོ་དམ་པའི་ཐོ་ཁོངས་ཀྱིས་མ་ལག་གང་རུང་གི་བཀོད་སྒྲིག་བསྒྱུར་ཆོག་མཉེན་ཆས་སྒྲིག་སྦྱོར་དང་རིམ་འཕར་མཉེན་ཆས་ཀྱང་འདུས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.ui" line="728"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.ui" line="760"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.cpp" line="150"/>
        <source>UserName</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.cpp" line="151"/>
        <source>Password</source>
        <translation>གསང་གྲངས་ནང་འཇུག</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.cpp" line="152"/>
        <source>Password Identify</source>
        <translation>གསང་གྲངས་གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.cpp" line="307"/>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.cpp" line="430"/>
        <source>Inconsistency with pwd</source>
        <translation>གསང་གྲངས་དང་གཅིག་མཐུན་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.cpp" line="417"/>
        <source>Password length needs to more than %1 character!</source>
        <translation>གསང་གྲངས་ཀྱི་རིང་ཚད་ནི་%1ལས་བརྒལ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.cpp" line="419"/>
        <source>Password length needs to less than %1 character!</source>
        <translation>གསང་གྲངས་ཀྱི་རིང་ཚད་ནི་%1མན་གྱི་ཡི་གེ་ཞིག་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.cpp" line="526"/>
        <source>The user name cannot be empty</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་སྟོང་པ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.cpp" line="528"/>
        <source>The first character must be lowercase letters!</source>
        <translation>ཡི་གེ་དང་པོ་ནི་ངེས་པར་དུ་ཡི་གེ་དམའ་རུ་གཏོང་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.cpp" line="531"/>
        <source>User name can not contain capital letters!</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་ལ་ཡི་གེ་ཆེན་པོ་ཡོད་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.cpp" line="545"/>
        <source>The user name is already in use, please use a different one.</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་བཀོལ་སྤྱོད་བྱེད་བཞིན་ཡོད་པས་གཞན་ཞིག་བཀོལ་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.cpp" line="550"/>
        <source>User name length need to less than %1 letters!</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་གི་རིང་ཚད་ནི་%1ལས་ཉུང་བའི་འཕྲིན་ཡིག་ཅིག་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.cpp" line="552"/>
        <source>The user name can only be composed of letters, numbers and underline!</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་ནི་འཕྲིན་ཡིག་དང་། ཨང་གྲངས། གསལ་བཤད་བཅས་ཁོ་ནར་བརྟེན་ནས་གྲུབ་པ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/createuserinteldialog.cpp" line="557"/>
        <source>The username is configured, please change the username</source>
        <translation>Username བཀོད་སྒྲིག་བྱས་ཟིན་པས་སྤྱོད་མཁན་གྱི་མིང་བསྒྱུར་རོགས།</translation>
    </message>
</context>
<context>
    <name>CreateUserNew</name>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="48"/>
        <source>CreateUserNew</source>
        <translation>གསར་སྐྲུན་བྱས་པའི་གསར་གཏོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="54"/>
        <source>UserName</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="64"/>
        <source>NickName</source>
        <translation>མིང་འདོགས་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="70"/>
        <source>HostName</source>
        <translation>གཙོ་སྐྱོང་བྱེད་མཁན་གྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="79"/>
        <source>Pwd</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="87"/>
        <source>SurePwd</source>
        <translation>གཏན་འཁེལ་བྱས་པའི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="95"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="98"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="101"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="104"/>
        <source>Required</source>
        <translation>ངེས་པར་དུ་འབྲི་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="108"/>
        <source>verification</source>
        <translation>ཞིབ་བཤེར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="181"/>
        <source>Select Type</source>
        <translation>རྩིས་ཐེམ་རིགས་དབྱིབས་བདམས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="190"/>
        <source>Administrator</source>
        <translation>དོ་དམ་མི་སྣ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="193"/>
        <source>Users can make any changes they need</source>
        <translation>དོ་དམ་པས་མ་ལག་གང་རུང་གི་བཀོད་སྒྲིག་བསྒྱུར་ཆོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="195"/>
        <source>Standard User</source>
        <translation>ཚད་ལྡན་སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="198"/>
        <source>Users cannot change system settings</source>
        <translation>ཚད་ལྡན་སྤྱོད་མཁན་གྱིས་མ་ལག་བཀོད་སྒྲིག་བཟོ་བཅོས་བྱེད་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="275"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="278"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="354"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="591"/>
        <source>Inconsistency with pwd</source>
        <translation>གསང་གྲངས་དང་གཅིག་མཐུན་མིན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="503"/>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="659"/>
        <source>The nick name cannot be empty</source>
        <translation>མིང་འདོགས་པའི་མིང་ནི་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="505"/>
        <source>nickName already in use.</source>
        <translation>བཀོལ་སྤྱོད་བྱེད་བཞིན་པའི་མཚང་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="508"/>
        <source>nickName length must less than %1 letters!</source>
        <translation>སྤྱོད་མཁན་གྱི་གཅེས་མིང་གི་རིང་ཚད་ངེས་པར་དུ་%1ལས་ཆུང་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="532"/>
        <source>Username&apos;s folder exists, change another one</source>
        <translation>སྤྱོད་མཁན་གྱི་ཡིག་སྣོད་གནས་པ་དང་། གཞན་ཞིག་ལ་འགྱུར་ལྡོག་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="536"/>
        <source>Name corresponds to group already exists.</source>
        <translation>མིང་དང་ཚོགས་པ་གཉིས་ལ་བབ་མཚུངས་ཀྱི་མིང་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="561"/>
        <source>Contains illegal characters!</source>
        <translation>དེའི་ནང་དུ་ཁྲིམས་འགལ་གྱི་ཡིག་རྟགས་འདུས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="654"/>
        <source>Username&apos;s length must be between 1 and %1 characters!</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་གི་རིང་ཚད་ངེས་པར་དུ་%1་ནས་1་བར་དུ་གནས་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="664"/>
        <source>new pwd cannot be empty!</source>
        <translation>གསང་གྲངས་གསར་བ་སྟོང་བ་ཡིན་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/createusernew.cpp" line="669"/>
        <source>sure pwd cannot be empty!</source>
        <translation>གསང་གྲངས་ནི་སྟོང་བ་ཡིན་མི་སྲིད་པ་ཐག་གིས་ཆོད།</translation>
    </message>
</context>
<context>
    <name>CustomGlobalTheme</name>
    <message>
        <location filename="../../../plugins/personalized/theme/globaltheme/customglobaltheme.cpp" line="34"/>
        <source>custom</source>
        <translation>མཚན་ཉིད་རང་འཇོག</translation>
    </message>
</context>
<context>
    <name>CustomLineEdit</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/customlineedit.cpp" line="28"/>
        <source>New Shortcut...</source>
        <translation>མགྱོགས་མཐེབ་གསར་བ།</translation>
    </message>
</context>
<context>
    <name>DateTime</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="26"/>
        <source>DateTime</source>
        <translation>དུས་ཚོད་ཀྱི་ཟླ་ཚེས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="65"/>
        <source>current date</source>
        <translation>ད་ལྟའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="321"/>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="265"/>
        <source>Change timezone</source>
        <translation>དུས་ཚོད་བསྒྱུར་བཅོས་བྱ་དགོས།</translation>
        <extra-contents_path>/Date/Change time zone</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="444"/>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="620"/>
        <source>TextLabel</source>
        <translation>ཡི་གེ་ལ་པེར་གྱིས་བཤད་རྒྱུར</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="481"/>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="510"/>
        <source>RadioButton</source>
        <translation>ཀུན་ཁྱབ་རླུང་འཕྲིན་ལས་ཁུངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="712"/>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="744"/>
        <source>:</source>
        <translation>:</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.ui" line="952"/>
        <source>titleLabel</source>
        <translation>ཁ་བྱང་ལ་པེ་ཨར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="91"/>
        <source>Date</source>
        <translation>དུས་ཚོད་དང་ལོ་ཟླ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="169"/>
        <source>Current Date</source>
        <translation>ད་ལྟའི་དུས་ཚོད།</translation>
        <extra-contents_path>/Date/Current Date</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="172"/>
        <source>Other Timezone</source>
        <translation>ཁུལ་གཞན་གྱི་དུས་ཚོད་གཞན་དག</translation>
        <extra-contents_path>/Date/Other Timezone</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="186"/>
        <source>24-hour clock</source>
        <translation>ཆུ་ཚོད་24རིང་།</translation>
        <extra-contents_path>/Date/24-hour clock</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="188"/>
        <source>Set Time</source>
        <translation>དུས་ཚོད་སྒྲིག་བཀོད།</translation>
        <extra-contents_path>/Date/Set Time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="216"/>
        <source>Set Date Manually</source>
        <translation>ལག་པས་དུས་ཚོད་གཏན་འཁེལ་བྱ་དགོས།</translation>
        <extra-contents_path>/Date/Set Date Manually</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="276"/>
        <source>Sync Time</source>
        <translation>རང་འགུལ་གྱི་དུས་ཚོད་གཅིག་མཐུན་ཡོང་བ།</translation>
        <extra-contents_path>/Date/Sync Time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="278"/>
        <source>Manual Time</source>
        <translation>ལག་འགུལ་སྒྲིག་བཀོད་དུས་ཚོད།</translation>
        <extra-contents_path>/Date/Manual Time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="447"/>
        <source>Sync Server</source>
        <translation>དུས་མཉམ་ཞབས་ཞུའི་ཡོ་བྱད།</translation>
        <extra-contents_path>/Date/Sync Server</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="449"/>
        <source>Default</source>
        <translation>སོར་བཞག་མ་ལག</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="451"/>
        <source>Customize</source>
        <translation>མཚན་ཉིད་རང་འཇོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="461"/>
        <source>Server Address</source>
        <translation>ཞབས་ཞུ་ཆས་ས་གནས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="466"/>
        <source>Required</source>
        <translation>ངེས་པར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="467"/>
        <source>Save</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="626"/>
        <source>change time</source>
        <translation>དུས་ཚོད་བསྒྱུར་བཅོས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="636"/>
        <source>Add Timezone</source>
        <translation>དུས་ཚོད་ཀྱི་ཁྱབ་ཁུལ་ཁ་སྣོན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="638"/>
        <source>Change Timezone</source>
        <translation>དུས་ཚོད་བསྒྱུར་བཅོས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="872"/>
        <source>  </source>
        <translation> </translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="873"/>
        <location filename="../../../plugins/time-language/datetime/datetime.cpp" line="882"/>
        <source>Sync failed</source>
        <translation>དུས་མཉམ་དུ་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>DefaultApp</name>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="41"/>
        <source>Default App</source>
        <translation>སོར་བཞག་ཉེར་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="65"/>
        <source>No program available</source>
        <translation>བཀོལ་སྤྱོད་མེད་པའི་གོ་རིམ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="66"/>
        <source>Choose default app</source>
        <translation>ཁས་ལེན་བཀོལ་སྤྱོད་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="249"/>
        <source>Reset default apps to system recommended apps</source>
        <translation>ཁས་ལེན་བཀོལ་སྤྱོད་བསྐྱར་སྒྲིག་མ་ལག་འོས་སྦྱོར་བཀོལ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="250"/>
        <source>Reset</source>
        <translation>བསྐྱར་དུ་བཀོད་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="335"/>
        <source>Browser</source>
        <translation>བཤེར་ཆས།</translation>
        <extra-contents_path>/Defaultapp/Browser</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="337"/>
        <source>Mail</source>
        <translation>སྦྲག་རྫས།</translation>
        <extra-contents_path>/Defaultapp/Mail</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="339"/>
        <source>Image Viewer</source>
        <translation>པར་རིས་པང་ལྟ་ཆས།</translation>
        <extra-contents_path>/Defaultapp/Image Viewer</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="341"/>
        <source>Audio Player</source>
        <translation>སྒྲ་ཕབ་འཕྲུལ་ཆས།</translation>
        <extra-contents_path>/Defaultapp/Audio Player</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="343"/>
        <source>Video Player</source>
        <translation>བརྙན་ཕབ་འཕྲུལ་འཁོར།</translation>
        <extra-contents_path>/Defaultapp/Video Player</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="345"/>
        <source>Text Editor</source>
        <translation>ཡིག་ཚགས་རྩོམ་སྒྲིག་ཆས།</translation>
        <extra-contents_path>/Defaultapp/Text Editor</extra-contents_path>
    </message>
</context>
<context>
    <name>DefaultAppWindow</name>
    <message>
        <location filename="../../../plugins/application/defaultapp/defaultapp.cpp" line="333"/>
        <source>Select Default Application</source>
        <translation>སོར་བཞག་ཉེར་སྤྱོད།</translation>
    </message>
</context>
<context>
    <name>DefineGroupItemIntel</name>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/definegroupitemintel.cpp" line="53"/>
        <source>Edit</source>
        <translation>རྩོམ་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/definegroupitemintel.cpp" line="62"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
</context>
<context>
    <name>DefineShortcutItem</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/defineshortcutitem.cpp" line="58"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
</context>
<context>
    <name>DelGroupIntelDialog</name>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/delgroupinteldialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/delgroupinteldialog.ui" line="38"/>
        <source>TextLabel</source>
        <translation>ཡི་གེ་ལ་པེར་གྱིས་བཤད་རྒྱུར</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/delgroupinteldialog.ui" line="82"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/delgroupinteldialog.ui" line="104"/>
        <source>RemoveFile</source>
        <translation>ཡིག་ཆ་མེད་པར་བཟོ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/delgroupinteldialog.ui" line="145"/>
        <source>Remind</source>
        <translation>དྲན་སྐུལ།</translation>
    </message>
</context>
<context>
    <name>DelUserIntelDialog</name>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/deluserinteldialog.ui" line="90"/>
        <source>   Delete</source>
        <translation>   སྤྱོད་མཁན་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/deluserinteldialog.ui" line="216"/>
        <source>Define</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/deluserinteldialog.ui" line="241"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/deluserinteldialog.cpp" line="55"/>
        <source>Delete the user, belonging to the user&apos;s desktop documents, favorites, music, pictures and video folder will be deleted!</source>
        <translation>སྤྱོད་མཁན་བསུབ་པ་དང་།སྤྱོད་མཁན་ཁོངས་གཏོགས་ཀྱི་ཅོག་ངོས་དང་།ཡིག་ཚགས།་ཉར་ཚགས་ཁུག་མ།་རོལ་མོ།པར་རིས་དང་བརྙན་འཕྲིན་ཡིག་ཁུག་ནང་གི་ནང་དོན་ཚང་མ་བསུབ་དགོས།</translation>
    </message>
</context>
<context>
    <name>DeleteUserExists</name>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="60"/>
        <source>Delete user &apos;</source>
        <translation>བེད་སྤྱོད་བྱེད་མཁན་བསུབ་པ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="61"/>
        <source>&apos;? And:</source>
        <translation>&apos;? ད་དུང་འདི་ལྟ་སྟེ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="86"/>
        <source>Keep desktop, files, favorites, music of the user</source>
        <translation>སྤྱོད་མཁན་དེའི་འོག་ཏུ་གཏོགས་པའི་ཅོག་ངོས།ཡིག་ཆ།ཉར་ཚགས་ཁུག་མ།རོལ་དབྱངས་སོགས་ཀྱི་གཞི་གྲངས་ཉར་ཚགས་བྱས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="88"/>
        <source>Delete whole data belong user</source>
        <translation>སྤྱོད་མཁན་དེའི་གཞི་གྲངས་ཚང་མ་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="120"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/deleteuserexists.cpp" line="122"/>
        <source>Confirm</source>
        <translation>བསུབ་པ།</translation>
    </message>
</context>
<context>
    <name>DigitalAuthIntelDialog</name>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/digitalauthinteldialog.cpp" line="52"/>
        <location filename="../../../plugins/account/userinfo_intel/digitalauthinteldialog.cpp" line="287"/>
        <location filename="../../../plugins/account/userinfo_intel/digitalauthinteldialog.cpp" line="312"/>
        <source>Enter Old Password</source>
        <translation>གསང་གྲངས་རྙིང་པར་འཇུག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/digitalauthinteldialog.cpp" line="76"/>
        <source>Forget Password?</source>
        <translation>གསང་གྲངས་བརྗེད་སོང་ངམ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/digitalauthinteldialog.cpp" line="97"/>
        <location filename="../../../plugins/account/userinfo_intel/digitalauthinteldialog.cpp" line="152"/>
        <location filename="../../../plugins/account/userinfo_intel/digitalauthinteldialog.cpp" line="223"/>
        <location filename="../../../plugins/account/userinfo_intel/digitalauthinteldialog.cpp" line="253"/>
        <location filename="../../../plugins/account/userinfo_intel/digitalauthinteldialog.cpp" line="263"/>
        <source>Input New Password</source>
        <translation>གསང་གྲངས་གསར་པ་ནང་འཇུག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/digitalauthinteldialog.cpp" line="142"/>
        <source>Input Password</source>
        <translation>ནང་འཇུག་གི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/digitalauthinteldialog.cpp" line="230"/>
        <source>The password input is error</source>
        <translation>གསང་གྲངས་ནང་འཇུག་བྱེད་པ་ནི་ནོར་འཁྲུལ་ཡིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/digitalauthinteldialog.cpp" line="244"/>
        <source>Confirm New Password</source>
        <translation>གསང་གྲངས་གསར་པ་གཏན་འཁེལ་བྱེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/digitalauthinteldialog.cpp" line="250"/>
        <source>The password input is inconsistent</source>
        <translation>ཐེངས་གཉིས་ལ་གསང་གྲངས་ནང་འཇུག་བྱས་པ་གཅིག་མཐུན་མིན་པ།ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/digitalauthinteldialog.cpp" line="260"/>
        <source>New password can not be consistent of old password</source>
        <translation>གསང་གྲངས་གསར་པ་ནི་གསང་གྲངས་རྙིང་བ་དང་གཅིག་མཐུན་ཡོང་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/digitalauthinteldialog.cpp" line="284"/>
        <location filename="../../../plugins/account/userinfo_intel/digitalauthinteldialog.cpp" line="309"/>
        <source>Password Change Failed</source>
        <translation>གསང་གྲངས་བསྒྱུར་བཅོས་བྱས་ནས་ཕམ་ཉེས་བྱུང་བ།</translation>
    </message>
</context>
<context>
    <name>DigitalPhoneIntelDialog</name>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/digitalphoneinteldialog.cpp" line="52"/>
        <source>Please Enter Edu OS Password</source>
        <translation>Edu OS གསང་གྲངས་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/digitalphoneinteldialog.cpp" line="163"/>
        <source>The password input is error</source>
        <translation>གསང་གྲངས་ནང་འཇུག་བྱེད་པ་ནི་ནོར་འཁྲུལ་ཡིན།</translation>
    </message>
</context>
<context>
    <name>DisplayPerformanceDialog</name>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="26"/>
        <location filename="../../../plugins/system/display_hw/displayperformancedialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>དུས་ཚོད་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="214"/>
        <location filename="../../../plugins/system/display_hw/displayperformancedialog.ui" line="214"/>
        <source>Display Advanced Settings</source>
        <translation>སྔོན་ཐོན་གྱི་སྒྲིག་བཀོད་མངོན་པར་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="297"/>
        <location filename="../../../plugins/system/display_hw/displayperformancedialog.ui" line="297"/>
        <source>Performance</source>
        <translation>འཁྲབ་སྟོན་གྱི་ནུས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="376"/>
        <location filename="../../../plugins/system/display_hw/displayperformancedialog.ui" line="376"/>
        <source>Applicable to machine with discrete graphics, which can accelerate the rendering of 3D graphics.</source>
        <translation>ཁེར་རྐྱང་གི་བྱང་བུ་ཡོད་པའི་འཕྲུལ་ཆས་ལ་འཚམ་པ་དང་།བྱང་བུའི་ནུས་པ་འདོན་སྤེལ་གང་ལེགས་བྱེད་ཐུབ་པ་དང་།3Dརིས་དབྱིབས་འབྲི་བ་ཇེ་མགྱོགས་སུ་གཏོང་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="392"/>
        <location filename="../../../plugins/system/display_hw/displayperformancedialog.ui" line="392"/>
        <source>(Note: not support connect graphical with xmanager on windows.)</source>
        <translation>མཉམ་འཇོག་བྱོས།རྣམ་པ་འདིས་སྟེང་གི་Xmanagerསོགས་ཀྱི་ལག་ཆ་བཀོལ་ནས་རིས་དབྱིབས་ལས་མངོན་པརWindows་རྒྱབ་སྐྱོར་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="462"/>
        <location filename="../../../plugins/system/display_hw/displayperformancedialog.ui" line="462"/>
        <source>Compatible</source>
        <translation>ཕན་ཚུན་མཐུན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="538"/>
        <location filename="../../../plugins/system/display_hw/displayperformancedialog.ui" line="538"/>
        <source>Applicable to machine with integrated graphics,  there is no 3D graphics acceleration. </source>
        <translation>BMCཕྱོགས་བསྡུས་རི་མོ་ཡོད་པའི་འཕྲུལ་འཁོར་ལ་སྤྱད་འཐུས་པ་ལས་3Dརི་མོའི་མྱུར་ཚད་ཇེ་མགྱོགས་སུ་གཏོང་མི་ཐུབ། </translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="554"/>
        <location filename="../../../plugins/system/display_hw/displayperformancedialog.ui" line="554"/>
        <source>(Note: need connect graphical with xmanager on windows, use this option.)</source>
        <translation>མཉམ་འཇོག་བྱོས།Windows་སྟེང་Xmanager་སོགས་ཀྱི་ལག་ཆ་བཀོལ་ནས་རིས་དབྱིབས་སྦྲེལ་བའི་འཕྲུལ་ཆས་ཀྱིས་ཚན་པ་འདི་འདེམས་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="624"/>
        <location filename="../../../plugins/system/display_hw/displayperformancedialog.ui" line="624"/>
        <source>Automatic</source>
        <translation>རང་འགུལ་གྱིས་ལྟ་ཞིབ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="700"/>
        <location filename="../../../plugins/system/display_hw/displayperformancedialog.ui" line="700"/>
        <source>Auto select according to environment, delay the login time (about 0.5 sec).</source>
        <translation>ཁོར་ཡུག་ལ་གཞིགས་ནས་རང་འགུལ་གྱིས་གདམ་གསེས་བྱས་ཏེ་ཐོ་འགོད་ཀྱི་དུས་ཚོད་ཕྱིར་འགྱངས་བྱ་དགོས། (ཧ་ལམ་སྐར་མ་0.5ཙམ་འགོར་གྱི་ཡོད། )</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="721"/>
        <location filename="../../../plugins/system/display_hw/displayperformancedialog.ui" line="721"/>
        <source>Threshold:</source>
        <translation>མཚམས་ཐང་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="744"/>
        <location filename="../../../plugins/system/display_hw/displayperformancedialog.ui" line="744"/>
        <source>Apply</source>
        <translation>ཉེར་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="757"/>
        <location filename="../../../plugins/system/display_hw/displayperformancedialog.ui" line="757"/>
        <source>Reset</source>
        <translation>བསྐྱར་དུ་བཀོད་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/displayperformancedialog.ui" line="772"/>
        <location filename="../../../plugins/system/display_hw/displayperformancedialog.ui" line="772"/>
        <source>(Note: select this option to use 3D graphics acceleration and xmanager.)</source>
        <translation>མཉམ་འཇོག་བྱོས།3Dརིས་དབྱིབས་བཀོལ་ནས་ཇེ་མགྱོགས་སུ་གཏོང་བ་མ་ཟད།Xmanager་བཀོལ་ནས་རིས་དབྱིབས་སྦྲེལ་སྐབས་རྣམ་གྲངས་འདི་འདེམས་པ།</translation>
    </message>
</context>
<context>
    <name>DisplaySet</name>
    <message>
        <location filename="../../../plugins/system/display/display.cpp" line="35"/>
        <source>Screen</source>
        <translation>བརྙན་ཤེལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.cpp" line="37"/>
        <location filename="../../../plugins/system/display_hw/display_hw.cpp" line="34"/>
        <source>Display</source>
        <translation>འཆར་ཆས།</translation>
    </message>
</context>
<context>
    <name>DisplayWindow</name>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="14"/>
        <location filename="../../../plugins/system/display_hw/display_hw.ui" line="14"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="32"/>
        <location filename="../../../plugins/system/display_hw/display_hw.ui" line="32"/>
        <source>Display</source>
        <translation>འཆར་ཆས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="171"/>
        <location filename="../../../plugins/system/display_hw/display_hw.ui" line="139"/>
        <source>monitor</source>
        <translation>འཆར་ཆས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="418"/>
        <location filename="../../../plugins/system/display_hw/display_hw.ui" line="299"/>
        <source>open monitor</source>
        <translation>འཆར་ཆས་སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display_hw/display_hw.ui" line="353"/>
        <source>Advanced</source>
        <translation>མཐོ་རིམ་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display_hw/display_hw.ui" line="395"/>
        <source>Mirror Display</source>
        <translation>ཤེལ་བརྙན་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="217"/>
        <location filename="../../../plugins/system/display_hw/display_hw.ui" line="185"/>
        <source>as main</source>
        <translation>གཙོ་བོར་འཛིན་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="326"/>
        <location filename="../../../plugins/system/display_hw/display_hw.ui" line="238"/>
        <source>screen zoom</source>
        <translation>བརྙན་ཤེལ་ཆེ་རུ་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="619"/>
        <location filename="../../../plugins/system/display_hw/display_hw.ui" line="537"/>
        <source>follow the sunrise and sunset(17:55-05:04)</source>
        <translation>ཉི་མ་ཤར་བ་དང་ཉི་མ་ནུབ་པའི་རྗེས་སུ་འབྲངས་པ། (17:55-05:04)</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="683"/>
        <location filename="../../../plugins/system/display_hw/display_hw.ui" line="601"/>
        <source>custom time</source>
        <translation>མཚན་ཉིད་རང་འཇོག་གྱི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="747"/>
        <location filename="../../../plugins/system/display_hw/display_hw.ui" line="665"/>
        <source>opening time</source>
        <translation>སྒོ་འབྱེད་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="830"/>
        <location filename="../../../plugins/system/display_hw/display_hw.ui" line="748"/>
        <source>closing time</source>
        <translation>སྒོ་རྒྱག་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="925"/>
        <location filename="../../../plugins/system/display_hw/display_hw.ui" line="843"/>
        <source>color temperature</source>
        <translation>ཁ་དོག་གི་དྲོད་ཚད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="932"/>
        <location filename="../../../plugins/system/display_hw/display_hw.ui" line="850"/>
        <source>warm</source>
        <translation>དྲོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/display.ui" line="955"/>
        <location filename="../../../plugins/system/display_hw/display_hw.ui" line="873"/>
        <source>cold</source>
        <translation>གྲང་ངར་ཆེ་བ།</translation>
    </message>
</context>
<context>
    <name>Fonts</name>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="50"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="44"/>
        <source>Fonts</source>
        <translation>ཡིག་གཟུགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="146"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="117"/>
        <source>Font size</source>
        <translation>ཡིག་གཟུགས་ཆེ་ཆུང་།</translation>
        <extra-contents_path>/Fonts/Font size</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="264"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="119"/>
        <source>Fonts select</source>
        <translation>ཡིག་གཟུགས་གདམ་གསེས་བྱེད་པ།</translation>
        <extra-contents_path>/Fonts/Fonts select</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="370"/>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="121"/>
        <source>Mono font</source>
        <translation>ཡིག་གཟུགས་མཚུངས་པ།</translation>
        <extra-contents_path>/Fonts/Mono font</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.ui" line="421"/>
        <source>Reset to default</source>
        <translation>ཁས་ལེན་སླར་གསོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="134"/>
        <source>Small</source>
        <translation>ཆུང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/fonts/fonts.cpp" line="138"/>
        <source>Large</source>
        <translation>ཆེ་བ།</translation>
    </message>
</context>
<context>
    <name>HostNameDialog</name>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="14"/>
        <source>Set HostName</source>
        <translation>གློག་ཀླད་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="43"/>
        <source>HostName</source>
        <translation>གློག་ཀླད་ཀྱི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="56"/>
        <source>Must be 1-64 characters long</source>
        <translation>ངེས་པར་དུ་ཡི་གེ་1-64ཡི་རིང་ཚད་ཡིན་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="70"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/hostnamedialog.cpp" line="74"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>InputPwdDialog</name>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="29"/>
        <source>Set</source>
        <translation>མ་ལག་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="46"/>
        <source>Set Password</source>
        <translation>གསང་གྲངས་གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="62"/>
        <source>Must be 1-8 characters long</source>
        <translation>ངེས་པར་དུ་ཡི་གེ་1-8ཀྱི་རིང་ཚད་ཡིན་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="79"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/inputpwddialog.cpp" line="83"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>KbdLayoutManager</name>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.ui" line="68"/>
        <source>C</source>
        <translation>རྒྱལ་ཁབ་ལྟར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.ui" line="144"/>
        <source>L</source>
        <translation>སྐད་རིགས་ལྟར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.ui" line="222"/>
        <source>Variant</source>
        <translation>འགྱུར་ལྡོག་ཆེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.ui" line="270"/>
        <source>Add</source>
        <translation>ཁ་སྣོན་བརྒྱབ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.cpp" line="60"/>
        <source>Add Layout</source>
        <translation>བཀོད་པ་ཁ་སྣོན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/kbdlayoutmanager.cpp" line="236"/>
        <source>Del</source>
        <translation>བསུབ་པ།</translation>
    </message>
</context>
<context>
    <name>KeyValueConverter</name>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="46"/>
        <source>System</source>
        <translation>མ་ལག</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="49"/>
        <source>Devices</source>
        <translation>སྒྲིག་ཆས།</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="55"/>
        <source>Personalized</source>
        <translation>རང་གཤིས་ཅན།</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="52"/>
        <source>Network</source>
        <translation>དྲ་རྒྱ།</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="58"/>
        <source>Account</source>
        <translation>རྩིས་ཐོ།</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="61"/>
        <source>Datetime</source>
        <translation>དུས་ཚོད་ཀྱི་སྐད་ཆ།</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="64"/>
        <source>Update</source>
        <translation>གསར་སྒྱུར།</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="67"/>
        <source>Security</source>
        <translation>བདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="70"/>
        <source>Application</source>
        <translation>ཉེར་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../utils/keyvalueconverter.cpp" line="73"/>
        <source>Investigation</source>
        <translation>འཚོལ་བཤེར།</translation>
    </message>
</context>
<context>
    <name>KeyboardControl</name>
    <message>
        <source>Keyboard</source>
        <translation type="vanished">མཐེབ་གཞོང་།</translation>
    </message>
</context>
<context>
    <name>KeyboardMain</name>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="25"/>
        <source>Keyboard</source>
        <translation>མཐེབ་གཞོང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="101"/>
        <source>Key board settings</source>
        <translation>མཐེབ་གཞུང་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="113"/>
        <source>Input settings</source>
        <translation>ནང་འཇུག་གི་སྒྲིག་བཀོད།</translation>
        <extra-contents_path>/Keyboard/Input settings</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="152"/>
        <source>Key repeat</source>
        <translation>ལྡེ་མིག་བསྐྱར་ཟློས་བྱེད་པ།</translation>
        <extra-contents_path>/Keyboard/Key repeat</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="175"/>
        <source>Delay</source>
        <translation>འགོར་འགྱངས་བྱས་པ།</translation>
        <extra-contents_path>/Keyboard/Delay</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="178"/>
        <source>Short</source>
        <translation>ཐུང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="181"/>
        <source>Long</source>
        <translation>རིང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="210"/>
        <source>Speed</source>
        <translation>མྱུར་ཚད།</translation>
        <extra-contents_path>/Keyboard/Speed</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="213"/>
        <source>Slow</source>
        <translation>དལ་མོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="216"/>
        <source>Fast</source>
        <translation>མགྱོགས་མྱུར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="244"/>
        <source>Input test</source>
        <translation>ནང་འཇུག་ཚོད་ལྟ།</translation>
        <extra-contents_path>/Keyboard/Input test</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/keyboardmain.cpp" line="265"/>
        <source>Key tips</source>
        <translation>མཐེབ་མནོན་གསལ་བརྡ།</translation>
        <extra-contents_path>/Keyboard/Key tips</extra-contents_path>
    </message>
</context>
<context>
    <name>LanguageFrame</name>
    <message>
        <location filename="../../../plugins/time-language/area/languageframe.cpp" line="77"/>
        <source>Input Settings</source>
        <translation>ནང་འཇུག་གི་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/languageframe.cpp" line="78"/>
        <source>Delete</source>
        <translation>འབུད་པ།</translation>
    </message>
</context>
<context>
    <name>LayoutManager</name>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="26"/>
        <source>Dialog</source>
        <translation>དུས་ཚོད་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="121"/>
        <source>Manager Keyboard Layout</source>
        <translation>སྤྱི་གཉེར་བའི་མཐེབ་གཞོང་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="234"/>
        <source>Language</source>
        <translation>སྐད་ཆ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="250"/>
        <source>Country</source>
        <translation>རྒྱལ་ཁབ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="293"/>
        <source>Variant</source>
        <translation>འགྱུར་ལྡོག་ཆེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="351"/>
        <source>Layout installed</source>
        <translation>སྒྲིག་སྦྱོར་བྱས་པའི་བཀོད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="399"/>
        <source>Preview</source>
        <translation>སྔོན་བརྡ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="431"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/keyboard/layoutmanager.ui" line="450"/>
        <source>Install</source>
        <translation>སྒྲིག་སྦྱོར་བྱེད་པ།</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../../mainwindow.cpp" line="178"/>
        <source>Normal</source>
        <translation>སླར་གསོ།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="181"/>
        <location filename="../../mainwindow.cpp" line="471"/>
        <source>Maximize</source>
        <translation>ཆེས་ཆེར་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="398"/>
        <location filename="../../mainwindow.cpp" line="449"/>
        <location filename="../../mainwindow.cpp" line="596"/>
        <location filename="../../mainwindow.cpp" line="1068"/>
        <source>Settings</source>
        <translation>སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="429"/>
        <source>Search</source>
        <translation>འཚོལ་ཞིབ།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="469"/>
        <source>Main menu</source>
        <translation>འདེམས་པང་གཙོ་བོ།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="470"/>
        <source>Minimize</source>
        <translation>ཆེས་ཆུང་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="472"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="584"/>
        <source>Help</source>
        <translation>རོགས་རམ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="586"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="588"/>
        <source>Exit</source>
        <translation>ཕྱིར་འཐེན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="753"/>
        <source>Specified</source>
        <translation>གཏན་འབེབས་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="1206"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../../mainwindow.cpp" line="1206"/>
        <source>This function has been controlled</source>
        <translation>འགན་ནུས་འདི་ཚོད་འཛིན་བྱས་ཟིན།</translation>
    </message>
</context>
<context>
    <name>MessageBox</name>
    <message>
        <location filename="../../../plugins/system/backup_intel/messagebox.ui" line="14"/>
        <source>Form</source>
        <translation>རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/backup_intel/messagebox.ui" line="97"/>
        <source>Attention</source>
        <translation>དོ་སྣང་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/backup_intel/messagebox.ui" line="138"/>
        <source>It takes effect after logging off</source>
        <translation>ཐོ་ཁོངས་ནས་སུབ་རྗེས་ནུས་པ་ཐོན་ཆོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/backup_intel/messagebox.ui" line="209"/>
        <source>Logout Now</source>
        <translation>ད་ལྟ་ཐོ་འགོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/backup_intel/messagebox.ui" line="228"/>
        <location filename="../../../plugins/system/backup_intel/messagebox.cpp" line="30"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/backup_intel/messagebox.cpp" line="29"/>
        <source>Reboot Now</source>
        <translation>ད་ལྟ་བསྐྱར་དུ་ལས་ཀ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/backup_intel/messagebox.cpp" line="31"/>
        <source>This cleanup and restore need to be done after the system restarts, whether to restart and restore immediately?</source>
        <translation>ཐེངས་འདིའི་གཙང་བཤེར་དང་སླར་གསོ་བྱེད་པར་མ་ལག་སླར་གསོ་བྱས་རྗེས་འཕྲལ་མར་སླར་གསོ་བྱེད་དགོས་སམ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/backup_intel/messagebox.cpp" line="34"/>
        <source>System Backup Tips</source>
        <translation>མ་ལག་གི་རྗེས་གྲབས་མན་ངག</translation>
    </message>
</context>
<context>
    <name>MessageBoxDialog</name>
    <message>
        <location filename="../../../plugins/system/backup_intel/messageboxdialog.ui" line="14"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/backup_intel/messageboxdialog.ui" line="68"/>
        <source>Message</source>
        <translation>ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/backup_intel/messageboxdialog.ui" line="152"/>
        <source>You do not have administrator rights!</source>
        <translation>ཁྱེད་ཚོར་དོ་དམ་པའི་དབང་ཆ་མེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/backup_intel/messageboxdialog.ui" line="162"/>
        <source> Factory Settings cannot be restored!</source>
        <translation> བཟོ་གྲྭའི་སྒྲིག་བཀོད་སླར་གསོ་བྱེད་མི་ཐུབ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/backup_intel/messageboxdialog.ui" line="247"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
</context>
<context>
    <name>MessageBoxPower</name>
    <message>
        <location filename="../../../plugins/system/backup_intel/messageboxpower.cpp" line="53"/>
        <source>System Recovery</source>
        <translation>མ་ལག་སླར་གསོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/backup_intel/messageboxpower.cpp" line="62"/>
        <source>The battery is low,please connect the power</source>
        <translation>གློག་སྨན་དམའ་བས་གློག་ཁུངས་སྦྲེལ་མཐུད་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/backup_intel/messageboxpower.cpp" line="64"/>
        <source>Keep the power connection, or the power is more than 25%.</source>
        <translation>གློག་ཤུགས་སྦྲེལ་མཐུད་རྒྱུན་འཁྱོངས་བྱེད་པའམ་ཡང་ན་གློག་ཤུགས་25%ཡན་ཟིན་པ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/backup_intel/messageboxpower.cpp" line="68"/>
        <source>Remind in 30 minutes</source>
        <translation>སྐར་མ་30ཡི་ནང་དུ་དྲན་སྐུལ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/backup_intel/messageboxpower.cpp" line="80"/>
        <source>Got it</source>
        <translation>ཤེས་སོང་།</translation>
    </message>
</context>
<context>
    <name>MessageBoxPowerIntel</name>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/messageboxpowerintel.cpp" line="48"/>
        <source>Nothing has been entered, re-enter</source>
        <translation>ནང་དུ་ཅི་ཡང་མ་འཛུལ་བར་ཡང་བསྐྱར་ནང་འདྲེན་བྱས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/messageboxpowerintel.cpp" line="59"/>
        <source>Remind in 30 minutes</source>
        <translation>སྐར་མ་30ཡི་ནང་དུ་དྲན་སྐུལ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/messageboxpowerintel.cpp" line="71"/>
        <source>Got it</source>
        <translation>ཤེས་སོང་།</translation>
    </message>
</context>
<context>
    <name>MouseControl</name>
    <message>
        <source>Mouse</source>
        <translation type="vanished">བྱི་བ།</translation>
    </message>
</context>
<context>
    <name>MouseUI</name>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="25"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="172"/>
        <source>Mouse</source>
        <translation>ཙིག་ལྡེབ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="180"/>
        <source>Pointer</source>
        <translation>ཕྱོགས་སྟོན་འཁོར་ལོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="188"/>
        <source>Cursor</source>
        <translation>འོད་རྟགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="251"/>
        <source>Dominant hand</source>
        <translation>ཙིག་རྟགས་གཙོ་བོའི་མཐེབ་གནོན།</translation>
        <extra-contents_path>/Mouse/Dominant hand</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="254"/>
        <source>Left hand</source>
        <translation>གཡས་མཐེབ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="255"/>
        <source>Right hand</source>
        <translation>གཡོན་མཐེབ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="282"/>
        <source>Scroll direction</source>
        <translation>འགྲིལ་བའི་ཁ་ཕྱོགས།</translation>
        <extra-contents_path>/Mouse/Scroll direction</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="285"/>
        <source>Forward</source>
        <translation>མདུན་དུ་སྐྱོད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="286"/>
        <source>Reverse</source>
        <translation>ལྡོག་ཕྱོགས་སུ་འགྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="314"/>
        <source>Wheel speed</source>
        <translation>འཁོར་ལོའི་མྱུར་ཚད།</translation>
        <extra-contents_path>/Mouse/Wheel speed</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="317"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="390"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="529"/>
        <source>Slow</source>
        <translation>དལ་མོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="327"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="398"/>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="537"/>
        <source>Fast</source>
        <translation>མགྱོགས་མྱུར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="350"/>
        <source>Double-click interval time</source>
        <translation>ལྡབ་འགྱུར་གྱིས་བར་མཚམས་ཀྱི་དུས་ཚོད།</translation>
        <extra-contents_path>/Mouse/Double-click interval time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="353"/>
        <source>Short</source>
        <translation>ཐུང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="361"/>
        <source>Long</source>
        <translation>རིང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="386"/>
        <source>Pointer speed</source>
        <translation>ཕྱོགས་སྟོན་འཁོར་ལོ་མགྱོགས་ཚད།</translation>
        <extra-contents_path>/Mouse/Pointer speed</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="422"/>
        <source>Mouse acceleration</source>
        <translation>ཙིག་རྟགས་མགྱོགས་ཚད།</translation>
        <extra-contents_path>/Mouse/Mouse acceleration</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="444"/>
        <source>Show pointer position when pressing ctrl</source>
        <translation>ctrl མནན་དུས་ཕྱོགས་སྟོན་གྱི་གནས་བབ་མངོན་པར་བྱེད་དགོས།</translation>
        <extra-contents_path>/Mouse/Show pointer position when pressing ctrl</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="467"/>
        <source>Pointer size</source>
        <translation>ཕྱོགས་སྟོན་འཁོར་ལོ་ཆེ་ཆུང་།</translation>
        <extra-contents_path>/Mouse/Pointer size</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="470"/>
        <source>Small(recommend)</source>
        <translation>ཆུང་བ་(འོས་སྦྱོར)།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="471"/>
        <source>Medium</source>
        <translation>འབྲིང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="472"/>
        <source>Large</source>
        <translation>ཆེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="503"/>
        <source>Blinking cursor in text area</source>
        <translation>ཡི་གེའི་ཁྱབ་ཁོངས་སུ་འོད་རྟགས་འཚེར་བ།</translation>
        <extra-contents_path>/Mouse/Blinking cursor in text area</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="526"/>
        <source>Cursor speed</source>
        <translation>འོད་རྟགས་ཀྱི་མྱུར་ཚད།</translation>
        <extra-contents_path>/Mouse/Cursor speed</extra-contents_path>
    </message>
</context>
<context>
    <name>MyLabel</name>
    <message>
        <location filename="../../../plugins/devices/mouse/mouseui.cpp" line="109"/>
        <source>double-click to test</source>
        <translation>གཉིས་རྡེབ་ཚོད་ལྟ།</translation>
    </message>
</context>
<context>
    <name>Notice</name>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="171"/>
        <source>NotFaze Mode</source>
        <translation>ཟིང་མེད་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="173"/>
        <source>(Notification banners, prompts will be hidden, and notification sounds will be muted)</source>
        <translation>(བརྡ་ཐོ་གཏོང་བའི་འཕྲེད་འགེལ་སྦྱར་ཡིག་དང་། བརྡ་གཏོང་ཡི་གེ་སྦས་སྐུང་བྱས་ནས་བརྡ་ཁྱབ་ཀྱི་སྒྲ་གྲགས་ཡོང་། )</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="247"/>
        <source>Automatically turn on</source>
        <translation>རང་འགུལ་གྱིས་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="272"/>
        <source>to</source>
        <translation>དེ་ལྟར་བྱས་ན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="297"/>
        <source>Automatically turn on when multiple screens are connected</source>
        <translation>བརྙན་ཤེལ་མང་པོ་སྦྲེལ་མཐུད་བྱེད་སྐབས་རང་འགུལ་གྱིས་ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="302"/>
        <source>Automatically open in full screen mode</source>
        <translation>བརྙན་ཤེལ་ཧྲིལ་པོའི་རྣམ་པའི་ཐོག་ནས་རང་འགུལ་གྱིས་སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="307"/>
        <source>Allow automatic alarm reminders in Do Not Disturb mode</source>
        <translation>རང་འགུལ་གྱིས་ཉེན་བརྡ་གཏོང་བའི་དྲན་སྐུལ་བྱེད་སྟངས་ལ་སུན་པོ་བཟོ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="337"/>
        <source>Notice Settings</source>
        <translation>བརྡ་ཐོའི་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="339"/>
        <source>Get notifications from the app</source>
        <translation>ཉེར་སྤྱོད་གོ་རིམ་ཁྲོད་ནས་བརྡ་ཐོ་གཏོང་དགོས།</translation>
        <extra-contents_path>/Notice/Get notifications from the app</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/notice.cpp" line="55"/>
        <source>Notice</source>
        <translation>བརྡ་ཐོ།</translation>
    </message>
</context>
<context>
    <name>NoticeMenu</name>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="41"/>
        <source>Beep sound when notified</source>
        <translation>བརྡ་ཐོ་གཏོང་སྐབས་སྐད་ཅོར་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="47"/>
        <source>Show message  on screenlock</source>
        <translation>བརྙན་ཤེལ་སྟེང་ནས་ཆ་འཕྲིན་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="53"/>
        <source>Show noticfication  on screenlock</source>
        <translation>བརྙན་ཤེལ་སྟེང་ནས་དོ་སྣང་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="57"/>
        <source>Notification Style</source>
        <translation>བརྡ་ཐོ་གཏོང་སྟངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="65"/>
        <source>Banner: Appears in the upper right corner of the screen, and disappears automatically</source>
        <translation>འཕྲེད་འགེལ་ཡི་གེ། བརྙན་ཤེལ་གྱི་གཡས་ཟུར་དུ་མངོན་པ་མ་ཟད། རང་འགུལ་གྱིས་མེད་པར་གྱུར་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="70"/>
        <source>Tip:It will be kept on the screen until it is closed</source>
        <translation>གསལ་བརྡ།འཆར་ངོས་སུ་ཉར་ཚགས་བྱས་ནས་སྒོ་རྒྱག་རག་བར་དུ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/notice/noticemenu.cpp" line="75"/>
        <source>None:Notifications will not be displayed on the screen, but will go to the notification center</source>
        <translation>མེད།བརྡ་ཐོ་འཆར་ངོས་སུ་འཆར་མི་སྲིད།འོན་ཀྱང་བརྡ་ཐོ་ལྟེ་གནས་སུ་འགྲོ་སྲིད།</translation>
    </message>
</context>
<context>
    <name>NumbersButtonIntel</name>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/numbersbuttonintel.cpp" line="47"/>
        <source>clean</source>
        <translation>གཙང་ཕྱག</translation>
    </message>
</context>
<context>
    <name>OutputConfig</name>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="97"/>
        <source>resolution</source>
        <translation>འབྱེད་ཕྱོད།</translation>
        <extra-contents_path>/Display/resolution</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="133"/>
        <location filename="../../../plugins/system/display_hw/outputconfig.cpp" line="123"/>
        <source>orientation</source>
        <translation>ཁ་ཕྱོགས།</translation>
        <extra-contents_path>/Display/orientation</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="151"/>
        <location filename="../../../plugins/system/display_hw/outputconfig.cpp" line="140"/>
        <source>arrow-up</source>
        <translation>མི་འཁོར་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="152"/>
        <location filename="../../../plugins/system/display_hw/outputconfig.cpp" line="141"/>
        <source>90° arrow-right</source>
        <translation>90°མདའ་གཡས་ཕྱོགས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="154"/>
        <location filename="../../../plugins/system/display_hw/outputconfig.cpp" line="143"/>
        <source>arrow-down</source>
        <translation>གོང་འོག་གོ་ལྡོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="170"/>
        <location filename="../../../plugins/system/display_hw/outputconfig.cpp" line="155"/>
        <source>frequency</source>
        <translation>གསར་འདོན་ཕྱོད།</translation>
        <extra-contents_path>/Display/frequency</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="153"/>
        <location filename="../../../plugins/system/display_hw/outputconfig.cpp" line="142"/>
        <source>90° arrow-left</source>
        <translation>90°མདའ་གཡོན་ཕྱོགས་སུ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="414"/>
        <source>auto</source>
        <translation>རང་འགུལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="217"/>
        <source>screen zoom</source>
        <translation>བརྙན་ཤེལ་ཆེ་རུ་གཏོང་བ།</translation>
        <extra-contents_path>/Display/screen zoom</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/outputconfig.cpp" line="596"/>
        <location filename="../../../plugins/system/display_hw/outputconfig.cpp" line="282"/>
        <location filename="../../../plugins/system/display_hw/outputconfig.cpp" line="289"/>
        <location filename="../../../plugins/system/display_hw/outputconfig.cpp" line="398"/>
        <source>%1 Hz</source>
        <translation>%1 Hz</translation>
    </message>
</context>
<context>
    <name>PhoneAuthIntelDialog</name>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="42"/>
        <source>Wechat Auth</source>
        <translation>སྐད་འཕྲིན་ར་སྤྲོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="44"/>
        <source>Phone Auth</source>
        <translation>ཁ་པར་ཨང་གྲངས་ཀྱིས་ར་སྤྲོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="71"/>
        <source>Phone number</source>
        <translation>ཁ་པར་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="75"/>
        <source>SMS verification code</source>
        <translation>བརྡ་འཕྲིན་གྱིས་ཞིབ་བཤེར་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="105"/>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="333"/>
        <source>GetCode</source>
        <translation>ར་སྤྲོད་ཨང་ཀི་ཐོབ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="115"/>
        <source>Return</source>
        <translation>ཕྱིར་སློག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="116"/>
        <source>Commit</source>
        <translation>འབུལ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="200"/>
        <source>confirm</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="226"/>
        <source>commit</source>
        <translation>འབུལ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="262"/>
        <source>Mobile number acquisition failed</source>
        <translation>ཁ་པར་ཨང་གྲངས་ལ་ཕམ་ཁ་བྱུང་བས་ལག་པ་འགུལ་ནས་ནང་འཇུག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="294"/>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="325"/>
        <source>Recapture</source>
        <translation>ཕྱིར་འཕྲོག་ལེན་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="317"/>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="395"/>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="565"/>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="683"/>
        <source>Network connection failure, please check</source>
        <translation>དྲ་སྦྲེལ་ལ་སྐྱོན་ཤོར་བས་ཞིབ་བཤེར་གནང་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="382"/>
        <source>Phone is lock,try again in an hour</source>
        <translation>ཁ་པར་ལ་ཟྭ་བརྒྱབ་ནས་དུས་ཚོད་གཅིག་འགོར་རྗེས་ཡང་བསྐྱར་ཚོད་ལྟ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="388"/>
        <source>Phone code is wrong</source>
        <translation>ཁ་པར་ཨང་གྲངས་ནོར་སོང་།ཡང་བསྐྱར་ཚོད་ལྟ་བྱེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="401"/>
        <source>Current login expired,using wechat code!</source>
        <translation>ཐོ་འགོད་ཆ་འཕྲིན་དུས་ལས་ཡོལ་འདུག་ཡང་བསྐྱར་སྐད་འཕྲིན་ཨང་གྲངས་བཀོལ་ནས་ཐོ་འགོད་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="407"/>
        <source>Unknown error, please try again later</source>
        <translation>ནོར་འཁྲུལ་མི་ཤེས་པས་ཕྱིས་སུ་ཡང་བསྐྱར་ཚོད་ལྟ་ཞིག་བྱེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/phoneauthinteldialog.cpp" line="664"/>
        <source>Please use the correct wechat scan code</source>
        <translation>ཡང་དག་པའི་འཕྲིན་ཕྲན་ཞིབ་བཤེར་གྱི་ཚབ་རྟགས་བཀོལ་རོགས།</translation>
    </message>
</context>
<context>
    <name>Power</name>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="59"/>
        <source>Power</source>
        <translation>གློག་ཁུངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="509"/>
        <location filename="../../../plugins/system/power/power.cpp" line="510"/>
        <source>Require password when sleep</source>
        <translation>གཉིད་རྗེས་སད་པར་གསང་གྲངས་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="659"/>
        <location filename="../../../plugins/system/power/power.cpp" line="669"/>
        <source>never</source>
        <translation>གཏན་ནས་བྱེད་མི་དཔེ།</translation>
    </message>
    <message>
        <source>Require password when sleep/hibernation</source>
        <translation type="vanished">གཉིད་ཉལ་སྐབས་གསང་གྲངས་དགོས་པའི་བླང་བྱ་བཏོན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="513"/>
        <location filename="../../../plugins/system/power/power.cpp" line="514"/>
        <source>Password required when waking up the screen</source>
        <translation>བརྙན་ཤེལ་གཉིད་ལས་སད་སྐབས་མཁོ་བའི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="517"/>
        <source>Press the power button</source>
        <translation>གློག་ཁུངས་མཐེབ་གནོན་མནན་དུས་ལག་བསྟར་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="521"/>
        <location filename="../../../plugins/system/power/power.cpp" line="522"/>
        <source>Time to close display</source>
        <translation>དུས་ཚོད་འདིའི་རྗེས་ནས་འཆར་ཆས་ཀྱི་སྒོ་རྒྱག་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="525"/>
        <location filename="../../../plugins/system/power/power.cpp" line="526"/>
        <source>Time to sleep</source>
        <translation>དུས་ཚོད་འདིའི་རྗེས་སུ་མ་ལག་གཉིད་དུ་ཡུར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="529"/>
        <location filename="../../../plugins/system/power/power.cpp" line="530"/>
        <source>Notebook cover</source>
        <translation>ལག་འཁྱེར་གློག་ཀླད་ཀྱི་ཁ་རྒྱག་སྐབས་ལག་བསྟར་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="533"/>
        <location filename="../../../plugins/system/power/power.cpp" line="534"/>
        <location filename="../../../plugins/system/power/power.cpp" line="538"/>
        <source>Using power</source>
        <translation>དབང་ཆ་བེད་སྤྱོད་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="537"/>
        <source>Using battery</source>
        <translation>གློག་སྨན་བཀོལ་སྤྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="541"/>
        <location filename="../../../plugins/system/power/power.cpp" line="542"/>
        <source> Time to darken</source>
        <translation> དུས་ཚོད་འདིའི་རྗེས་ནས་འཆར་ངོས་ཀྱི་གསལ་ཚད་ཇེ་དམའ་རུ་གཏོང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="545"/>
        <location filename="../../../plugins/system/power/power.cpp" line="546"/>
        <source>Battery level is lower than</source>
        <translation>གློག་གཡིས་ཀྱི་ཆུ་ཚད་ལས་དམའ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="549"/>
        <source>Run</source>
        <translation>འཁོར་སྐྱོད་བྱེད་བཞིན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="551"/>
        <location filename="../../../plugins/system/power/power.cpp" line="552"/>
        <source>Low battery notification</source>
        <translation>གློག་གཡིས་ཀྱི་བརྡ་ཐོ་དམའ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="555"/>
        <source>Automatically run saving mode when low battery</source>
        <translation>གློག་ཚད་དམའ་བའི་སྐབས་སུ་རང་འགུལ་གྱིས་ནུས་ཁུངས་གྲོན་ཆུང་གི་རྣམ་པ་ཕྱེས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="556"/>
        <source>Automatically run saving mode when the low battery</source>
        <translation>གློག་ཚད་དམའ་བའི་སྐབས་སུ་རང་འགུལ་གྱིས་ནུས་ཁུངས་གྲོན་ཆུང་གི་རྣམ་པ་ཕྱེས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="559"/>
        <location filename="../../../plugins/system/power/power.cpp" line="560"/>
        <source>Automatically run saving mode when using battery</source>
        <translation>གློག་རྫས་བཀོལ་བའི་སྐབས་སུ་རང་འགུལ་གྱིས་ནུས་ཁུངས་གྲོན་ཆུང་གི་རྣམ་པ་ཕྱེས་པ་རེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="563"/>
        <location filename="../../../plugins/system/power/power.cpp" line="564"/>
        <source>Display remaining charging time and usage time</source>
        <translation>དེ་བྱིངས་ཀྱི་གློག་གསོག་དུས་ཚོད་དང་བཀོལ་སྤྱོད་ཀྱི་དུས་ཚོད་མངོན་པར་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="629"/>
        <source>General</source>
        <translation>སྤྱི་སྤྱོད།</translation>
        <extra-contents_path>/Power/General</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="631"/>
        <source>Select Powerplan</source>
        <translation>གློག་ཁུངས་འཆར་གཞི།</translation>
        <extra-contents_path>/Power/Select Powerplan</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="633"/>
        <source>Battery saving plan</source>
        <translation>གློག་གཡིས་གྲོན་ཆུང་གི་འཆར་གཞི།</translation>
        <extra-contents_path>/Power/Battery saving plan</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="639"/>
        <location filename="../../../plugins/system/power/power.cpp" line="690"/>
        <source>nothing</source>
        <translation>བེད་སྤྱོད་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="639"/>
        <location filename="../../../plugins/system/power/power.cpp" line="690"/>
        <source>blank</source>
        <translation>འཆར་ངོས་སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="639"/>
        <location filename="../../../plugins/system/power/power.cpp" line="650"/>
        <location filename="../../../plugins/system/power/power.cpp" line="690"/>
        <source>suspend</source>
        <translation>གཉིད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="645"/>
        <location filename="../../../plugins/system/power/power.cpp" line="650"/>
        <location filename="../../../plugins/system/power/power.cpp" line="696"/>
        <source>hibernate</source>
        <translation>ངལ་གསོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="650"/>
        <source>interactive</source>
        <translation>འདྲི་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="659"/>
        <source>5min</source>
        <translation>སྐར་མ5</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="659"/>
        <location filename="../../../plugins/system/power/power.cpp" line="669"/>
        <source>10min</source>
        <translation>སྐར་མ10</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="659"/>
        <location filename="../../../plugins/system/power/power.cpp" line="669"/>
        <source>15min</source>
        <translation>སྐར་མ15</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="659"/>
        <location filename="../../../plugins/system/power/power.cpp" line="669"/>
        <source>30min</source>
        <translation>སྐར་མ30</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="659"/>
        <location filename="../../../plugins/system/power/power.cpp" line="669"/>
        <source>1h</source>
        <translation>དུས་ཚོད་1</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="659"/>
        <location filename="../../../plugins/system/power/power.cpp" line="669"/>
        <source>2h</source>
        <translation>དུས་ཚོད2</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="669"/>
        <source>3h</source>
        <translation>དུས་ཚོད3</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="679"/>
        <location filename="../../../plugins/system/power/power.cpp" line="684"/>
        <source>Balance Model</source>
        <translation>དོ་མཉམ་གྱི་དཔེ་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="679"/>
        <location filename="../../../plugins/system/power/power.cpp" line="684"/>
        <source>Save Model</source>
        <translation>གསོག་འཇོག་བྱེད་སྟངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="679"/>
        <location filename="../../../plugins/system/power/power.cpp" line="684"/>
        <source>Performance Model</source>
        <translation>གྲུབ་འབྲས་ཀྱི་དཔེ་དབྱིབས།</translation>
    </message>
    <message>
        <source>1min</source>
        <translation type="vanished">1min</translation>
    </message>
    <message>
        <source>20min</source>
        <translation type="vanished">20min</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/power/power.cpp" line="639"/>
        <location filename="../../../plugins/system/power/power.cpp" line="650"/>
        <location filename="../../../plugins/system/power/power.cpp" line="690"/>
        <source>shutdown</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
</context>
<context>
    <name>Printer</name>
    <message>
        <location filename="../../../plugins/devices/printer/printer.cpp" line="43"/>
        <source>Printer</source>
        <translation>དཔར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/printer/printer.cpp" line="131"/>
        <source>Printers</source>
        <translation>པར་འདེབས་འཕྲུལ་འཁོར།</translation>
    </message>
    <message>
        <source>Printers And Scanners</source>
        <translation type="vanished">པར་འདེབས་འཕྲུལ་འཁོར་དང་བཤེར་ཆས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/printer/printer.cpp" line="181"/>
        <source>Add</source>
        <translation>ཁ་སྣོན་བརྒྱབ་པ།</translation>
        <extra-contents_path>/Printer/Add</extra-contents_path>
    </message>
</context>
<context>
    <name>PrivacyDialog</name>
    <message>
        <location filename="../../../plugins/system/about/privacydialog.cpp" line="11"/>
        <source>Set</source>
        <translation>མ་ལག་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/privacydialog.cpp" line="26"/>
        <source>End User License Agreement and Privacy Policy Statement of Kylin</source>
        <translation>དབྱིན་ཧུ་ཆི་ལིན་མཇུག་མཐར་སྤྱོད་མཁན་གྱིས་གྲོས་མཐུན་དང་གསང་བའི་སྲིད་ཇུས་གསལ་བསྒྲགས་བཀོལ་སྤྱོད་བྱས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/privacydialog.cpp" line="31"/>
        <source>Dear users of Kylin operating system and relevant products, 
         This agreement describes your rights, obligations and prerequisites for your use of this product. Please read the clauses of the Agreement and the supplementary license (hereinafter collectively referred to as “the Agreement”) and the privacy policy statement for Kylin operating system (hereinafter referred to as “the Statement”).
        “This product” in the Agreement and the Statement refers to “Kylin operating system software product” developed, produced and released by Kylinsoft Co., Ltd. and used for handling the office work or building the information infrastructure for enterprises and governments. “We” refers to Kylinsoft Co., Ltd. “You” refers to the users who pay the license fee and use the Kylin operating system and relevant products.

End User License Agreement of Kylin 
Release date of the version: July 30, 2021
Effective date of the version: July 30, 2021
        The Agreement shall include the following content:
        I.     User license 
        II.    Java technology limitations
        III.   Cookies and other technologies
        IV.    Intellectual property clause
        V.     Open source code
        VI.   The third-party software/services
        VII.  Escape clause
        VIII. Integrity and severability of the Agreement
        IX.    Applicable law and dispute settlement

        I.      User license
        According to the number of users who have paid for this product and the types of computer hardware, we shall grant the non-exclusive and non-transferable license to you, and shall only allow the licensed unit and the employees signing the labor contracts with the unit to use the attached software (hereinafter referred to as “the Software”) and documents as well as any error correction provided by Kylinsoft.
        1.     User license for educational institutions
        In the case of observing the clauses and conditions of the Agreement, if you are an educational institution, your institution shall be allowed to use the attached unmodified binary format software and only for internal use. “For internal use” here refers to that the licensed unit and the employees signing the labor contracts with the unit as well as the students enrolled by your institution can use this product.
        2.     Use of the font software
        Font software refers to the software pre-installed in the product and generating font styles. You cannot separate the font software from the Software and cannot modify the font software in an attempt to add any function that such font software, as a part of this product, does not have when it is delivered to you, or you cannot embed the font software in the files provided as a commercial product for any fee or other remuneration, or cannot use it in equipment where this product is not installed. If you use the font software for other commercial purposes such as external publicity, please contact and negotiate with the font copyright manufacture to obtain the permissions for your relevant acts.

        II.    Java technology limitations
        You cannot change the “Java Platform Interface” (referred to as “JPI”, that is, the classes in the “java” package or any sub-package of the “java” package), whether by creating additional classes in JPI or by other means to add or change the classes in JPI. If you create an additional class as well as one or multiple relevant APIs, and they (i) expand the functions of Java platform; And (ii) may be used by the third-party software developers to develop additional software that may call the above additional APIs, you must immediately publish the accurate description of such APIs widely for free use by all developers. You cannot create or authorize other licensees to create additional classes, interfaces or sub-packages marked as “java”, “javax” and “sun” in any way, or similar agreements specified by Sun in any naming agreements. See the appropriate version of the Java Runtime Environment Binary Code License (located at http://jdk.java.net at present) to understand the availability of runtime code jointly distributed with Java mini programs and applications.

        III.   Cookies and other technologies
        In order to help us better understand and serve the users, our website, online services and applications may use the “Cookie” technology. Such Cookies are used to store the network traffic entering and exiting the system and the traffic generated due to detection errors, so they must be set. We shall understand how you interact with our website and online services by using such Cookies.
        If you want to disable the Cookie and use the Firefox browser, you may set it in Privacy and Security Center of Firefox. If your use other browsers, please consult the specific schemes from the relevant suppliers.
        In accordance with Article 76, paragraph 5 of the Network Security Law of the People&apos;s Republic of China, personal information refers to all kinds of information recorded in electronic or other ways, which can identify the natural persons’ personal identity separately or combined with other information, including but not limited to the natural person’s name, date of birth, identity certificate number, personal biological identification information, address and telephone number, etc. If Cookies contain the above information, or the combined information of non-personal information and other personal information collected through Cookie, for the purpose of this privacy policy, we shall regard the combined information as personal privacy information, and shall provide the corresponding security protection measures for your personal information by referring to Kylin Privacy Policy Statement.

        IV.   Intellectual property clause
        1.    Trademarks and Logos
        This product shall be protected by the copyright law, trademark law and other laws and international intellectual property conventions. Title to the product and all associated intellectual property rights are retained by us or its licensors. No right, title or interest in any trademark, service mark, logo or trade name of us or its licensors is granted under the Agreement. Any use of Kylinsoft marked by you shall be in favor of Kylinsoft, and without our consent, you shall not arbitrarily use any trademark or sign of Kylinsoft.
        2.    Duplication, modification and distribution
        If the Agreement remains valid for all duplicates, you may and must duplicate, modify and distribute software observing GNU GPL-GNU General Public License agreement among the Kylin operating system software products in accordance with GNU GPL-GNU General Public License, and must duplicate, modify and distribute other Kylin operating system software products not observing GNU GPL-GNU General Public License agreement in accordance with relevant laws and other license agreements, but no derivative release version based on the Kylin operating system software products can use any of our trademarks or any other signs without our written consent.
        Special notes: Such duplication, modification and distribution shall not include any software, to which GNU GPL-GNU General Public License does not apply, in this product, such as the software store, input method software, font library software and third-party applications contained by the Kylin operating system software products. You shall not duplicate, modify (including decompilation or reverse engineering) or distribute the above software unless prohibited by applicable laws.

        V.    Open source code
        For any open source codes contained in this product, any clause of the Agreement shall not limit, constrain or otherwise influence any of your corresponding rights or obligations under any applicable open source code license or all kinds of conditions you shall observe.

        VI.  The third-party software/services
        The third-party software/services referred to in the Agreement refer to relevant software/services developed by other organizations or individuals other than the Kylin operating system manufacturer. This product may contain or be bundled with the third-party software/services to which the separate license agreements are attached. When you use any third-party software/services with separate license agreements, you shall be bound by such separate license agreements.
        We shall not have any right to control the third-party software/services in these products and shall not expressly or implicitly ensure or guarantee the legality, accuracy, effectiveness or security of the acts of their providers or users.

        VII. Escape clause
        1.    Limited warranty
        We guarantee to you that within ninety (90) days from the date when you purchase or obtain this product in other legal ways (subject to the date of the sales contract), the storage medium (if any) of this product shall not be involved in any defects in materials or technology when it is normally used. All compensation available to you and our entire liability under this limited warranty will be for us to choose to replace this product media or refund the fee paid for this product.
        2.   Disclaimer
        In addition to the above limited warranty, the Software is provided “as is” without any express or implied condition statement and warranty, including any implied warranty of merchantability, suitability for a particular purpose or non-infringement, except that this disclaimer is deemed to be legally invalid.
        3.   Limitation of responsibility
        To the extent permitted by law, under any circumstances, no matter what theory of liability is adopted, no matter how it is caused, for any loss of income, profit or data caused by or related to the use or inability to use the Software, or for special indirect consequential incidental or punitive damages, neither we nor its licensors shall be liable (even if we have been informed of the possibility of such damages). According to the Agreement, in any case, whether in contract tort (including negligence) or otherwise, our liability to you will not exceed the amount you pay for the Software. The above limitations will apply even if the above warranty fails of its essential purpose.

        VIII.Integrity and severability of the Agreement
        1.    The integrity of the Agreement
        The Agreement is an entire agreement on the product use concluded by us with you. It shall replace all oral or written contact information, suggestions, representations and guarantees inconsistent with the Agreement previous or in the same period. During the period of the Agreement, in case of any conflict clauses or additional clauses in the relevant quotations, orders or receipts or in other correspondences regarding the content of the Agreement between the parties, the Agreement shall prevail. No modification of the Agreement will be binding, unless in writing and signed by an authorized representative of each party.
        2.   Severability of the Agreement
        If any provision of the Agreement is deemed to be unenforceable, the deletion of the corresponding provision will still be effective, unless the deletion will hinder the realization of the fundamental purpose of the parties (in which case, the Agreement will be terminated immediately).

        IX.  Applicable law and dispute settlement
        1.   Application of governing laws
        Any dispute settlement (including but not limited to litigation and arbitration) related to the Agreement shall be governed by the laws of the People’s Republic of China. The legal rules of any other countries and regions shall not apply.
        2.  Termination
        If the Software becomes or, in the opinion of either party, may become the subject of any claim for intellectual property infringement, either party may terminate the Agreement immediately.
        The Agreement is effective until termination. You may terminate the Agreement at any time, but you must destroy all originals and duplicates of the Software. The Agreement will terminate immediately without notice from us if you fail to comply with any provision of the Agreement. At the time of termination, you must destroy all originals and duplicates of such software, and shall be legally liable for not observing the Agreement.
        The Agreement shall be in both Chinese and English, and in case of ambiguity between any content above, the Chinese version shall prevail.

        Privacy Policy Statement of Kylin Operating System/n        Release date of the version: July 30, 2021
        Effective date of the version: July 30, 2021

        We attach great importance to personal information and privacy protection. In order to guarantee the legal, reasonable and appropriate collection, storage and use of your personal privacy information and the transmission and storage in the safe and controllable circumstances, we hereby formulate this Statement. We shall provide your personal information with corresponding security protection measures according to the legal requirements and mature security standards in the industry.

        The Statement shall include the following content:
        I.   Collection and use your personal information
        II.  How to store and protect your personal information
        III. How to manage your personal information
        IV.  Privacy of the third-party software/services
        V.   Minors’ use of the products
        VI.  How to update this Statement
        VII. How to contact us

        I.     How to collect and use your personal information
        1.    The collection of personal information
        We shall collect the relevant information when you use this product mainly to provide you with higher-quality products, more usability and better services. Part of information collected shall be provided by you directly, and other information shall be collected by us through your interaction with the product as well as your use and experience of the product. We shall not actively collect and deal with your personal information unless we have obtained your express consent according to the applicable legal stipulations.
        1)   The licensing mechanism for this product allows you to apply for the formal license of the product in accordance with the contract and relevant agreements after you send a machine code to the commercial personnel of Kylinsoft, and the machine code is generated through encryption and conversion according to the information of the computer used by you, such as network card, firmware and motherboard. This machine code shall not directly contain the specific information of the equipment, such as network card, firmware and motherboard, of the computer used by you.
        2)   Server of the software store of this product shall connect it according to the CPU type information and IP address of the computer used by you; at the same time, we shall collect the relevant information of your use of the software store of this product, including but not limited to the time of opening the software store, interaction between the pages, search content and downloaded content. The relevant information collected is generally recorded in the log of server system of software store, and the specific storage position may change due to different service scenarios.
        3)   Upgrading and updating of this product shall be connected according to the IP address of the computer used by you, so that you can upgrade and update the system;
        4)   Your personal information, such as E-mail address, telephone number and name, shall be collected due to business contacts and technical services.
        5)   The biological characteristic management tool support system components of this product shall use the biological characteristics for authentication, including fingerprint, finger vein, iris and voiceprint. The biological characteristic information input by you shall be stored in the local computer, and for such part of information, we shall only receive the verification results but shall not collect or upload it. If you do not need to use the biological characteristics for the system authentication, you may disable this function in the biological characteristic management tool.
        6)   This product shall provide the recording function. When you use the recording function of this product, we shall only store the audio content when you use the recording in the local computer but shall not collect or upload the content.
        7)   The service and support functions of this product shall collect the information provided by you for us, such as log, E-mail, telephone and name, so as to make it convenient to provide the technical services, and we shall properly keep your personal information.
        8)   In the upgrading process of this product, if we need to collect additional personal information of yours, we shall timely update this part of content.
        2.   Use of personal information
        We shall strictly observe the stipulations of laws and regulations and agreements with you to use the information collected for the following purposes. In case of exceeding the scope of following purposes, we shall explain to you again and obtain your consent.
        1)   The needs such as product licensing mechanism, use of software store, system updating and maintenance, biological identification and online services shall be involved;
        2)   We shall utilize the relevant information to assist in promoting the product security, reliability and sustainable service;
        3)   We shall directly utilize the information collected (such as the E-mail address and telephone provided by you) to communicate with you directly, for example, business contact, technical support or follow-up service visit;
        4)   We shall utilize the data collected to improve the current usability of the product, promote the product’s user experience (such as the personalized recommendation of software store) and repair the product defects, etc.;
        5)   We shall use the user behavior data collected for data analysis. For example, we shall use the information collected to analyze and form the urban thermodynamic chart or industrial insight report excluding any personal information. We may make the information excluding identity identification content upon the statistics and processing public and share it with our partners, to understand how the users use our services or make the public understand the overall use trend of our services;
        6)   We may use your relevant information and provide you with the advertising more related to you on relevant websites and in applications andother channels;
        7)   In order to follow the relevant requirements of relevant laws and regulations, departmental regulations and rules and governmental instructions.
        3.   Information sharing and provision
        We shall not share or transfer your personal information to any third party, except for the following circumstances:
        1)   After obtaining your clear consent, we shall share your personal information with the third parities;
        2)   In order to achieve the purpose of external processing, we may share your personal information with the related companies or other third-party partners (the third-party service providers, contractors, agents and application developers). We shall protect your information security by means like encryption and anonymization;
        3)   We shall not publicly disclose the personal information collected. If we must disclose it publicly, we shall notify you of the purpose of such public disclosure, type of information disclosed and the sensitive information that may be involved, and obtain your consent;
        4)   With the continuous development of our business, we may carry out the transactions, such as merger, acquisition and asset transfer, and we shall notify you of the relevant circumstances, and continue to protect or require the new controller to continue to protect your personal information according to laws and regulations and the standards no lower than that required by this Statement;
        5)   If we use your personal information beyond the purpose claimed at the time of collection and the directly or reasonably associated scope, we shall notify you again and obtain your consent before using your personal information.
        4.   Exceptions with authorized consent
        1)   It is directly related to national security, national defense security and other national interests; 
        2)   It is directly related to public safety, public health and public knowledge and other major public interests; 
        3)   It is directly related to crime investigation, prosecution, judgment and execution of judgment; 
        4)   It aims to safeguard the life, property and other major legal rights and interests of you or others but it is impossible to obtain your own consent; 
        5)   The personal information collected is disclosed to the public by yourself; 
        6)   Personal information collected from legally publicly disclosed information, such as legal news reports, government information disclosure and other channels; 
        7)   It is necessary to sign and perform of the contract according to your requirement; 
        8)   It is necessary to maintain the safe and stable operation of the provided products or services, including finding and handling any fault of products or services;
        9)   It is necessary to carry out statistical or academic research for public interest, and when the results of academic research or description are provided, the personal information contained in the results is de-identified;
        10) Other circumstances specified in the laws and regulations.

        II.   How to store and protect personal information
        1.   Information storage place
        We shall store the personal information collected and generated in China within the territory of China in accordance with laws and regulations.
        2.   Information storage duration 
        Generally speaking, we shall retain your personal information for the time necessary to achieve the purpose or for the shortest term stipulated by laws and regulations. Information recorded in the log shall be kept for a specified period and be automatically deleted according to the configuration.
        When operation of our product or services stops, we shall notify you in the forms such as notification and announcement, delete your personal information or conduct anonymization within a reasonable period and immediately stop the activities collecting the personal information.
        3.   How to protect the information
        We shall strive to provide guarantee for the users’ information security, to prevent the loss, improper use, unauthorized access or disclosure of the information.
        We shall use the security protection measures within the reasonable security level to protect the information security. For example, we shall protect your system account and password by means like encryption.
        We shall establish the special management systems, processes and organizations to protect the information security. For example, we shall strictly restrict the scope of personnel who access to the information, and require them to observe the confidentiality obligation.
        4.   Emergency response plan
        In case of security incidents, such as personal information disclosure, we shall start the emergency response plan according to law, to prevent the security incidents from spreading, and shall notify you of the situation of the security incidents, the possible influence of the incidents on you and the remedial measures we will take, in the form of pushing the notifications and announcements. We will also report the disposition of the personal information security events according to the laws, regulations and regulatory requirements.

        III. How to manage your personal information
        If you worry about the personal information disclosure caused by using this product, you may consider suspending or not using the relevant functions involving the personal information, such as the formal license of the product, application store, system updating and upgrading and biological identification, according to the personal and business needs. 
        Please pay attention to the personal privacy protection at the time of using the third-party software/services in this product.

        IV.  Privacy of the third-party software/services

        The third-party software/services referred to in the Agreement refer to relevant software/services developed by other organizations or individuals other than the Kylin operating system manufacturer.
        When you install or use the third-party software/services in this product, the privacy protection and legal responsibility of the third-party software/services shall be independently borne by the third-party software/services. Please carefully read and examine the privacy statement or clauses corresponding to the third-party software/services, and pay attention to the personal privacy protection.

        V.   Minors’ use of the products
        If you are a minor, you shall obtain your guardian’s consent on your use of this product and the relevant service clauses. Except for the information required by the product, we shall not deliberately require the minors to provide more data. With the guardians’ consent or authorization, the accounts created by the minors shall be deemed to be the same as any other accounts. We have formulated special information processing rules to protect the personal information security of minors using this product. The guardians shall also take the appropriate preventive measures to protect the minors and supervise their use of this product.

        VI.  How to update this Statement
        We may update this Statement at any time, and shall display the updated statement to you through the product installation process or the company’s website at the time of updating. After such updates take effect, if you use such services or any software permitted according to such clauses, you shall be deemed to agree on the new clauses. If you disagree on the new clauses, then you must stop using this product, and please close the accountcreated by you in this product; if you are a guardian, please help your minor child to close the account created by him/her in this product.

        VII. How to contact us
        If you have any question, or any complaints or opinions on this Statement, you may seek advice through our customer service hotline 400-089-1870, or the official website (www.kylinos.cn), or “service and support” application in this product. You may also contact us by E-mail (market@kylinos.cn). 
        We shall timely and properly deal with them. Generally, a reply will be made within 15 working days.
        The Statement shall take effect from the date of updating. The Statement shall be in Chinese and English at the same time and in case of any ambiguity of any clause above, the Chinese version shall prevail.
        Last date of updating: November 1, 2021

Address:
        Building 3, Xin’an Entrepreneurship Plaza, Tanggu Marine Science and Technology Park, Binhai High-tech Zone, Tianjin (300450)
        Silver Valley Tower, No. 9, North Forth Ring West Road, Haidian District, Beijing (100190)
        Building T3, Fuxing World Financial Center, No. 303, Section 1 of Furong Middle Road, Kaifu District, Changsha City (410000)
        Digital Entertainment Building, No. 1028, Panyu Road, Xuhui District, Shanghai (200030)
Tel.:
        Tianjin (022) 58955650      Beijing (010) 51659955
        Changsha (0731) 88280170        Shanghai (021) 51098866
Fax:
        Tianjin (022) 58955651      Beijing (010) 62800607
        Changsha (0731) 88280166        Shanghai (021) 51062866

        Company website: www.kylinos.cn
        E-mail: support@kylinos.cn</source>
        <translation>ཅིན་ལིན་གྱི་བཀོལ་སྤྱོད་མ་ལག་དང་འབྲེལ་ཡོད་ཐོན་རྫས་ཀྱི་སྙིང་ཉེ་བའི་སྤྱོད་མཁན། 
         གྲོས་མཐུན་འདིས་ཁྱེད་ཚོས་ཐོན་རྫས་འདི་བཀོལ་སྤྱོད་བྱེད་པའི་ཁེ་དབང་དང་། འོས་འགན། སྔོན་འགྲོའི་ཆ་རྐྱེན་བཅས་གསལ་བཤད་བྱས་ཡོད། ཁྱེད་ཀྱིས་གྲོས་མཐུན་གྱི་དོན་ཚན་དང་ཁ་གསབ་ཆོག་འཐུས་ལག་ཁྱེར་(གཤམ་དུ་ཐུན་མོང་དུ་&quot;གྲོས་མཐུན་&quot;ཞེས་འབོད་པ་)དང་ཁེ་ལིན་གྱི་བཀོལ་སྤྱོད་མ་ལག་གི་གསང་བའི་སྲིད་ཇུས་གསལ་བསྒྲགས་(གཤམ་དུ་&quot;གསལ་བསྒྲགས་&quot;ཞེས་འབོད་རྒྱུ་)ཀློག་རོགས།
        《ཆོད་ཡིག་》དང་《གསལ་བསྒྲགས་》ནང་གི་&quot;ཐོན་རྫས་འདི་&quot;ཞེས་པ་ནི་ཁེ་ལིན་སའོ་ཧྥུ་མ་རྐང་ཚད་ཡོད་ཀུང་སིས་གསར་སྤེལ་དང་། ཐོན་སྐྱེད། ཁྱབ་བསྒྲགས་བཅས་བྱས་པའི་&quot;ཁེ་ལིན་གྱི་བཀོལ་སྤྱོད་མ་ལག་གི་མཉེན་ཆས་ཐོན་རྫས་&quot;ལ་ཟེར། &quot;ང་ཚོ་&quot;ཞེས་པ་ནི་ཁེ་ལིན་སའོ་ཧྥུ་མ་རྐང་ཚད་ཡོད་ཀུང་སིའི་&quot;ཁྱོད་&quot;ཞེས་པ་ནི་ཁེ་ལིན་གྱི་བཀོལ་སྤྱོད་མ་ལག་དང་འབྲེལ་ཡོད་ཐོན་རྫས་བེད་སྤྱོད་བྱེད་མཁན་ལ་ཟེར།

ཅིན་ལིན་གྱི་སྤྱོད་མཁན་གྱི་ཆོག་འཐུས་གྲོས་མཐུན་མཇུག་བསྒྲིལ 
པར་གཞི་ཁྱབ་བསྒྲགས་བྱས་པའི་ཚེས་གྲངས། 2021ལོའི་ཟླ་7ཚེས་30ཉིན།
པར་གཞི་འདི་ལག་བསྟར་བྱེད་པའི་དུས་ཚོད། 2021ལོའི་ཟླ་7ཚེས་30ཉིན།
        གྲོས་མཐུན་ནང་གཤམ་གསལ་གྱི་ནང་དོན་ཚུད་དགོས་པ་སྟེ།
        I.     སྤྱོད་མཁན་གྱི་ལག་ཁྱེར། 
        II.Javaལག་རྩལ་གྱི་ཚད་བཀག
        གསུམ། བག་ལེབ་ཀོར་མོ་སོགས་ཀྱི་ལག་རྩལ།
        IV.ཤེས་བྱའི་ཐོན་དངོས་བདག་དབང་གི་དོན་ཚན།
        V.     སྒོ་འབྱེད་འབྱུང་ཁུངས་ཀྱི་ཚབ་རྟགས
        དྲུག་པ། ཕྱོགས་གསུམ་པའི་མཉེན་ཆས་དང་ཞབས་ཞུ།
        བདུན། བྲོས་བྱོལ་དུ་སོང་བའི་དོན་ཚན།
        བརྒྱད། གྲོས་མཐུན་གྱི་ཁ་དན་ཚིག་གནས་རང་བཞིན་དང་ཚབས་ཆེའི་རང་བཞིན།
        IX.བཅའ་ཁྲིམས་སྤྱད་འཐུས་པ་དང་རྩོད་གཞི་ཐག་གཅོད་བྱ་དགོས།

I.      སྤྱོད་མཁན་གྱི་ལག་ཁྱེར།
        ཐོན་རྫས་འདི་དང་རྩིས་འཁོར་གྱི་མཁྲེགས་ཆས་རིགས་ལ་རིན་དོད་སྤྲད་ཟིན་པའི་སྤྱོད་མཁན་གྱི་མི་གྲངས་དང་རྩིས་འཁོར་གྱི་མཁྲེགས་ཆས་རིགས་ལ་གཞིགས་ནས་ང་ཚོས་ཁྱོད་ལ་ཕྱིར་འབུད་དང་སྤོ་སྒྱུར་བྱས་མི་ཆོག་པའི་ལག་ཁྱེར་སྤྲོད་དགོས་
        1.སློབ་གསོའི་ལས་ཁུངས་ཀྱི་སྤྱོད་མཁན་གྱི་ལག་ཁྱེར།
        གྲོས་མཐུན་གྱི་དོན་ཚན་དང་ཆ་རྐྱེན་ལ་བརྩི་སྲུང་བྱེད་སྐབས་གལ་ཏེ་ཁྱོད་ནི་སློབ་གསོའི་ལས་ཁུངས་ཡིན་ན། ཁྱོད་ཀྱི་ལས་ཁུངས་ཀྱིས་ཟུར་བཀོད་བྱས་མེད་པའི་རྒྱུ་གཉིས་རྣམ་གཞག་གི་མཉེན་ཆས་སྤྱད་དེ་ནང་ཁུལ་དུ་སྤྱོད་དུ་འཇུག་དགོས། འདིར་བཤད་པའི་&quot;ནང་ཁུལ་དུ་སྤྱོད་རྒྱུ་&quot;ཞེས་པ་ནི་ཆོག་མཆན་ཐོབ་པའི་སྡེ་ཚན་དང་སྡེ་ཚན་དེ་གའི་ངལ་རྩོལ་གན་རྒྱ་འཇོག་མཁན་ལས་བཟོ་པ་དང་དེ་བཞིན་ཁྱེད་ཚོའི་ལས་ཁུངས་ཀྱིས་བསྡུ་ལེན་བྱས་པའི་སློབ་མས་ཐོན་རྫས་འདི་བེད་སྤྱོད་བྱས་ཆོག་པར་ཟེར
        2.ཡིག་གཟུགས་མཉེན་ཆས་བཀོལ་སྤྱོད་བྱེད་པ།
        ཡིག་གཟུགས་མཉེན་ཆས་ཞེས་པ་ནི་ཐོན་རྫས་ནང་སྔོན་ཚུད་ནས་སྒྲིག་སྦྱོར་བྱས་པའི་མཉེན་ཆས་དང་ཡིག་གཟུགས་ཀྱི་རྣམ་པ་ཐོན་སྐྱེད་བྱེད་པར་ཟེར། ཁྱེད་ཚོས་ཡིག་གཟུགས་མཉེན་ཆས་དང་མཉེན་ཆས་ལོགས་སུ་འབྱེད་མི་ཐུབ་པ་དང་། ཡིག་གཟུགས་མཉེན་ཆས་བཟོ་བཅོས་རྒྱག་མི་ཐུབ་པས། ཡིག་གཟུགས་མཉེན་ཆས་དེ་རིགས་ཐོན་རྫས་འདིའི་ཆ་ཤས་ཤིག་ཡིན་པའི་ཆ་ནས་ཁྱེད་ཚོར་སྤྲད་པའི་དུས་སུ་མེད་པའམ་ཡང་ན་རིན་དོད།  ཡང་ན་ཐོན་རྫས་དེ་རིགས་སྒྲིག་སྦྱོར་བྱས་མེད་པའི་སྒྲིག་ཆས་ནང་བེད་སྤྱོད་བྱེད་མི་རུང་། གལ་ཏེ་ཡིག་གཟུགས་མཉེན་ཆས་སྤྱད་དེ་ཕྱི་ཕྱོགས་ལ་དྲིལ་བསྒྲགས་བྱེད་པ་སོགས་ཚོང་ལས་ཀྱི་ཆེད་དུ་ཡིན་ན། ཡིག་གཟུགས་པར་དབང་བཟོ་མཁན་དང་འབྲེལ་གཏུག་དང་གྲོས་མོལ་བྱས་ནས་འབྲེལ་ཡོད་བྱ་སྤྱོད་ཀྱི་ཆོག་མཆན་ཐོབ་རོགས།

II.Javaལག་རྩལ་གྱི་ཚད་བཀག
        JPIནང་དུ་ཁ་སྣོན་བྱས་པའི་འཛིན་གྲྭ་གསར་སྐྲུན་བྱེད་པའམ་ཡང་ན་བྱེད་ཐབས་གཞན་དག་སྤྱད་དེ་JPIནང་གི་འཛིན་གྲྭ་ཁ་སྣོན་དང་བསྒྱུར་བཅོས་བྱེད་པ་གང་ཡིན་རུང་&quot;Java Platform Interface&quot;(&quot;JPI&quot;ཞེས་འབོད་པ་)བསྒྱུར་མི་རུང་། གལ་ཏེ་ཁྱོད་ཀྱིས་ཟུར་སྣོན་རིགས་ཤིག་གསར་སྐྲུན་བྱས་པ་མ་ཟད། ད་དུང་འབྲེལ་ཡོད་ཀྱི་APIsགཉིས་གསར་སྐྲུན་བྱས་ན། དེ་དག་(i)Javaསྟེགས་བུའི་ནུས་པ་རྒྱ་བསྐྱེད་པ་དང་། (གཉིས། )ཕྱོགས་གསུམ་པའི་མཉེན་ཆས་གསར་སྤེལ་བྱེད་མཁན་གྱིས་གོང་གསལ་གྱི་ཟུར་སྣོན་APIsཞེས་འབོད་སྲིད་པའི་ཟུར་སྣོན་མཉེན་ཆས་གསར་སྤེལ་བྱས་ཆོག་པས། ཁྱེད་ཚོས་ངེས་པར་དུ་འཕྲལ་མར་གསར་སྤེལ་ཚོང་པ་ཚང་མས་ཡོངས་ཁྱབ་ཏུ་APIsདེ་རིགས་རྒྱ་ཁྱབ་ཏུ་ཁྱབ། ཁྱེད་ཚོས་ཆོག་འཐུས་ལག་ཁྱེར་གཞན་དག་གསར་སྐྲུན་དང་དབང་ཆ་བསྐུར་ནས་བྱེད་སྟངས་གང་རུང་གི་ཐོག་ནས་&quot;java&quot;དང་། &quot;javax&quot;། &quot;sun&quot;བཅས་སུ་རྟགས་བརྒྱབ་པའི་འཛིན་གྲྭ་དང་། འབྲེལ་མཐུད། ཡན་ལག་ཁུག་མ་བཅས་གསར་སྐྲུན་བྱེད་མི་ཐུབ་པའམ་ཡང་ན་དབང་ཆ། Java Runtime Environment Binary Code License(མིག་སྔར་http://jdk.java.net་གནས་ཡོད་)ཡི་འོས་འཚམ་གྱི་པར་གཞི་ལ་བལྟས་ནས་Java mini programདང་ཉེར་སྤྱོད་གོ་རིམ་དང་མཉམ་འབྲེལ་གྱིས་འགྲེམ་སྤེལ་བྱས་པའི་འཁོར་སྐྱོད་དུས་ཚོད་ཀྱི་ཚབ་རྟགས་འདོན་སྤྲོད་བྱེད་ཐུབ་མིན་ལ་རྒྱུས་ལོན་བྱེད་དགོས།

གསུམ། བག་ལེབ་ཀོར་མོ་སོགས་ཀྱི་ལག་རྩལ།
        ང་ཚོས་སྔར་ལས་ལྷག་པའི་སྒོ་ནས་སྤྱོད་མཁན་ལ་རྒྱུས་ལོན་དང་ཞབས་འདེགས་ཞུ་བར་རོགས་རམ་བྱེད་ཆེད། ང་ཚོའི་དྲ་ཚིགས་དང་། དྲ་ཐོག་ཞབས་ཞུ། ཉེར་སྤྱོད་གོ་རིམ་བཅས་ཀྱིས་&quot;ཀ་ར་གོ་རེ་&quot;ལག་རྩལ ཀ་ར་གོ་རེ་འདི་རིགས་ནི་མ་ལག་ནང་དུ་འགྲོ་འོང་བྱེད་པའི་དྲ་རྒྱའི་འགྲིམ་འགྲུལ་དང་ཞིབ་དཔྱད་ཚད་ལེན་གྱི་ནོར་འཁྲུལ་ལས་བྱུང་བའི་འགྲིམ་འགྲུལ་གསོག་ཉར་བྱེད་པར་བཀོལ་བ་ཡིན་པས། ངེས་པར་དུ་གཏན་འཁེལ་བྱ་དགོས། ང་ཚོས་ཁྱོད་ཀྱིས་ཀ་ར་གོ་རེ་འདི་རིགས་བཀོལ་ནས་ང་ཚོའི་དྲ་ཚིགས་དང་དྲ་ཐོག་ཞབས་ཞུ་དང་འབྲེལ་འདྲིས་ཇི་ལྟར་བྱ་རྒྱུར་གོ
        གལ་ཏེ་ཁྱོད་ཀྱིས་ཀ་ར་གོ་རེ་མེད་པར་བཟོས་ནས་Firefox browserབཀོལ་སྤྱོད་བྱེད་འདོད་ན། ཁྱོད་ཀྱིས་དེ་མེ་གསོད་མེ་འགོག་གི་གསང་དོན་དང་བདེ་འཇགས་ལྟེ་གནས་སུ་བཞག་ཆོག གལ་ཏེ་ཁྱོད་ཀྱིས་བལྟ་ཆས་གཞན་པ་བཀོལ་སྤྱོད་བྱས་ན། འབྲེལ་ཡོད་མཁོ་འདོན་ཚོང་པའི་བྱེ་བྲག་གི་ཇུས་གཞིར་འདྲི་རྩད་བྱེད་རོགས།
        《ཀྲུང་ཧྭ་མི་དམངས་སྤྱི་མཐུན་རྒྱལ་ཁབ་ཀྱི་དྲ་རྒྱའི་བདེ་འཇགས་བཅའ་ཁྲིམས་》ཀྱི་དོན་ཚན་དོན་དྲུག་པའི་ནང་གསེས་དོན་ཚན་ལྔ་པའི་གཏན་འབེབས་གཞིར་བཟུང་མི་སྒེར་གྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/privacydialog.cpp" line="298"/>
        <source>Kylinsoft Co., Ltd.</source>
        <translation>ཆི་ལིན་མཉེན་ཆས་ཚད་ཡོད་ཀུང་སི།</translation>
    </message>
</context>
<context>
    <name>Proxy</name>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="433"/>
        <source>Auto url</source>
        <translation>URLབཀོད་སྒྲིག</translation>
        <extra-contents_path>/Proxy/Auto url</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="435"/>
        <source>Http Proxy</source>
        <translation>HTTPཚབ་པ།</translation>
        <extra-contents_path>/Proxy/Http Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="442"/>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="443"/>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="444"/>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="445"/>
        <source>Port</source>
        <translation>མཐུད་ཁ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="177"/>
        <source>Start using</source>
        <translation>བཀོལ་སྤྱོད་བྱེད་འགོ་ཚུགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="191"/>
        <source>Proxy mode</source>
        <translation>ཚབ་བྱེད་དཔེ་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="196"/>
        <source>Auto</source>
        <translation>རང་འགུལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="200"/>
        <source>Manual</source>
        <translation>ལག་འགུལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="431"/>
        <source>System Proxy</source>
        <translation>མ་ལག་གི་ཚབ་བྱེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="437"/>
        <source>Https Proxy</source>
        <translation>Https ཚབ་བྱེད།</translation>
        <extra-contents_path>/Proxy/Https Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="439"/>
        <source>Ftp Proxy</source>
        <translation>Ftp ཚབ་བྱེད།</translation>
        <extra-contents_path>/Proxy/Ftp Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="441"/>
        <source>Socks Proxy</source>
        <translation>SOCKSཚབ་བྱེད།</translation>
        <extra-contents_path>/Proxy/Socks Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="446"/>
        <source>List of ignored hosts. more than one entry, please separate with english semicolon(;)</source>
        <translation>སྣང་མེད་དུ་བཞག་པའི་བདག་པོའི་མིང་ཐོ། འཇུག་སྒོ་གཅིག་ལས་བརྒལ་ན་དབྱིན་ཡིག་གི་ཕྱེད་ཀ་དང་ཁ་གྱེས་རོགས། (;)</translation>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="449"/>
        <source>Apt Proxy</source>
        <translation>Apt ཚབ་བྱེད།</translation>
        <extra-contents_path>/Proxy/Apt Proxy</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="450"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="451"/>
        <source>Server Address : </source>
        <translation>ཞབས་ཞུའི་ཡོ་བྱད་ཀྱི་གནས་ཡུལ </translation>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="452"/>
        <source>Port : </source>
        <translation>མཐུད་ཁ། </translation>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="453"/>
        <source>Edit</source>
        <translation>རྩོམ་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="587"/>
        <source>The apt proxy  has been turned off and needs to be restarted to take effect</source>
        <translation>aptལས་ཚབ་བྱེད་ནུས་སྒོ་བརྒྱབ་ཟིན་པས་ཡང་བསྐྱར་སྒོ་ཕྱེ་རྗེས་ནུས་པ་ཐོན་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="588"/>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="808"/>
        <source>Reboot Later</source>
        <translation>རྗེས་སུ་ཡང་བསྐྱར་སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="589"/>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="809"/>
        <source>Reboot Now</source>
        <translation>ད་ལྟ་བསྐྱར་དུ་ལས་ཀ་བྱེད་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="807"/>
        <source>The system needs to be restarted to set the Apt proxy, whether to reboot</source>
        <translation>མ་ལག་འདི་བསྐྱར་དུ་འགོ་ཚུགས་ནས་Aptཡི་ཚབ་བྱེད་འཕྲུལ་ཆས་གཏན་འཁེལ་བྱེད་དགོས་པ་དང་། བསྐྱར་དུ་འགོ་འཛུགས་དགོས་མིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/network/proxy/proxy.cpp" line="49"/>
        <source>Proxy</source>
        <translation>ཚབ་བྱེད།</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.cpp" line="173"/>
        <source>Unknown</source>
        <translation>ཤེས་མེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.cpp" line="206"/>
        <source>Year</source>
        <translation>ལོ་གཅིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.cpp" line="224"/>
        <source>Jan</source>
        <translation>ཟླ་དང་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.cpp" line="225"/>
        <source>Feb</source>
        <translation>ཟླ་2པར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.cpp" line="226"/>
        <source>Mar</source>
        <translation>ཟླ་གསུམ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.cpp" line="227"/>
        <source>Apr</source>
        <translation>ཟླ་བ་4</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.cpp" line="229"/>
        <source>Jun</source>
        <translation>ཟླ་བ་དྲུག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.cpp" line="230"/>
        <source>Jul</source>
        <translation>ཟླ་བ་བདུན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.cpp" line="231"/>
        <source>Aug</source>
        <translation>ཟླ་8</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.cpp" line="232"/>
        <source>Sep</source>
        <translation>ཟླ་བ་དགུ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.cpp" line="233"/>
        <source>Oct</source>
        <translation>ཟླ་10པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.cpp" line="234"/>
        <source>Nov</source>
        <translation>ཟླ་བ་བཅུ་གཅིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.cpp" line="235"/>
        <source>Dec</source>
        <translation>ཟླ་བ་བཅུ་གཉིས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.cpp" line="256"/>
        <source>Day</source>
        <translation>ཉིན་མོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="71"/>
        <source>User Info</source>
        <translation>སྤྱོད་མཁན་གྱི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.cpp" line="167"/>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.cpp" line="204"/>
        <source>Never</source>
        <translation>གཏན་ནས་བྱེད་མི།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="33"/>
        <location filename="../../../plugins/account/userinfo_intel/changevalidinteldialog.cpp" line="228"/>
        <source>May</source>
        <translation>ཟླ་བ་ལྔ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="33"/>
        <source>January</source>
        <translation>ཟླ་དང་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="33"/>
        <source>February</source>
        <translation>ཟླ་2པར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="33"/>
        <source>March</source>
        <translation>ཟླ་བ་གསུམ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="33"/>
        <source>April</source>
        <translation>ཟླ་4པར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="33"/>
        <source>June</source>
        <translation>ཟླ་6པར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="34"/>
        <source>July</source>
        <translation>ཟླ་7པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="34"/>
        <source>August</source>
        <translation>ཟླ་བརྒྱད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="34"/>
        <source>September</source>
        <translation>ཟླ་9པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="34"/>
        <source>October</source>
        <translation>ཟླ་བ་བཅུ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="34"/>
        <source>Novermber</source>
        <translation>ཟླ་བ་བཅུ་གཅིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.cpp" line="34"/>
        <source>December</source>
        <translation>ཟླ་བ་བཅུ་གཉིས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1262"/>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="320"/>
        <source>min length %1
</source>
        <translation>རིང་ཐུང་གི་ཚད་ནི་1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1272"/>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="330"/>
        <source>min digit num %1
</source>
        <translation>min digit num %1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1281"/>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="339"/>
        <source>min upper num %1
</source>
        <translation>min སྟེང་གི་num%1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1290"/>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="348"/>
        <source>min lower num %1
</source>
        <translation>min དམའ་རིམ་num %1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1299"/>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="357"/>
        <source>min other num %1
</source>
        <translation>གཏེར་ཁ་གཞན་དག%1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1309"/>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="367"/>
        <source>min char class %1
</source>
        <translation>གཏེར་ཁ་སྔོག་འདོན་འཛིན་གྲྭ་%1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1318"/>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="376"/>
        <source>max repeat %1
</source>
        <translation>ཆེས་ཆེ་བའི་བསྐྱར་ཟློས་ཀྱི་ཚད་གཞི་ནི་1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1327"/>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="385"/>
        <source>max class repeat %1
</source>
        <translation>ཆེས་ཆེ་བའི་འཛིན་གྲྭ་བསྐྱར་ཟློས་བྱས་ན་%1
</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1336"/>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="394"/>
        <source>max sequence %1
</source>
        <translation>ཚད་གཞི་མཐོ་ཤོས་ཀྱི་གོ་རིམ་བརྒྱ་ཆ་1
</translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="84"/>
        <source>ukui-control-center is already running!</source>
        <translation>ཚོད་འཛིན་ངོས་ལེབ་འཁོར་སྐྱོད་བྱས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="95"/>
        <source>ukui-control-center is disabled！</source>
        <translation>ཝུའུ་ཁི་ལན་གྱི་ཚོད་འཛིན་ལྟེ་གནས་ནི་དབང་པོ་སྐྱོན་ཅན་ཡིན།</translation>
    </message>
    <message>
        <location filename="../../main.cpp" line="108"/>
        <source>ukui-control-center</source>
        <translation>སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="8"/>
        <source>简体中文</source>
        <translation>བསྡུས་གཟུགས་རྒྱ་ཡིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="9"/>
        <source>English</source>
        <translation>དབྱིན་སྐད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/area/addlanguagedialog.cpp" line="10"/>
        <source>བོད་ཡིག</source>
        <translation>བོད་ཡིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="185"/>
        <source>Customize Shortcut</source>
        <translation>རང་ཉིད་ཀྱི་མཚན་ཉིད་མགྱོགས་མཐེབ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="447"/>
        <source>Edit Shortcut</source>
        <translation>མགྱོགས་ལམ་རྩོམ་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/application/autoboot/autoboot.cpp" line="789"/>
        <source>Programs are not allowed to be added.</source>
        <translation>གོ་རིམ་འདི་ཁ་སྣོན་བྱེད་མི་ཆོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="659"/>
        <source>xxx客户端</source>
        <translation>xxx མཁོ་མཁན་ཕྱོགས།</translation>
    </message>
</context>
<context>
    <name>ResolutionSlider</name>
    <message>
        <location filename="../../../plugins/system/display_hw/resolutionslider.cpp" line="111"/>
        <source>No available resolutions</source>
        <translation>འོས་འཚམ་གྱི་དབྱེ་འབྱེད་ཚད་མེད།</translation>
    </message>
</context>
<context>
    <name>Screenlock</name>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="26"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="48"/>
        <source>Screenlock</source>
        <translation>བརྙན་ཤེལ་གྱི་སྒོ་བརྒྱབ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="80"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="134"/>
        <source>Screenlock Interface</source>
        <translation>བརྙན་ཤེལ་གྱི་འབྲེལ་མཐུད།</translation>
        <extra-contents_path>/Screenlock/Screenlock Interface</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="205"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="183"/>
        <source>Show message on lock screen</source>
        <translation>ཟྭ་ངོས་སུ་ཆ་འཕྲིན་མངོན་པར་བྱས་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="262"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="136"/>
        <source>Show picture of screenlock on screenlogin</source>
        <translation>བརྙན་ཤེལ་སྟེང་གི་བརྙན་ཤེལ་གྱི་པར་རིས་འགྲེམས་སྟོན་བྱས།</translation>
        <extra-contents_path>/Screenlock/Show picture of screenlock on screenlogin</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="475"/>
        <source>Related Settings</source>
        <translation>འབྲེལ་ལྡན་སྒྲིག་འགོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="531"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="592"/>
        <source>Monitor Off</source>
        <translation>སྒོ་རྒྱག་འཆར་ཆས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="550"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="605"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="594"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="595"/>
        <source>Set</source>
        <translation>སྒྲིག་བཀོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="586"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="593"/>
        <source>Screensaver</source>
        <translation>བརྙན་ཡོལ་སྲུང་སྐྱོབ།</translation>
    </message>
    <message>
        <source>Lock screen when screensaver boot</source>
        <translation type="vanished">བརྙན་ཤེལ་གྱི་ལྷམ་ཡུ་རིང་གི་དུས་སུ་བརྙན་ཤེལ་ལ་ཟྭ་རྒྱག</translation>
        <extra-contents_path>/Screenlock/Lock screen when screensaver boot</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="332"/>
        <source>Lock screen delay</source>
        <translation>བརྙན་ཤེལ་གྱི་དུས་ཚོད་འགོར་འགྱངས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="416"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="138"/>
        <source>Browse</source>
        <translation>ལྟ་ཀློག་བྱེད་པ།</translation>
        <extra-contents_path>/Screenlock/Browse</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="423"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="140"/>
        <source>Online Picture</source>
        <translation>དྲ་ཐོག་པར་རིས།</translation>
        <extra-contents_path>/Screenlock/Online Picture</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.ui" line="455"/>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="150"/>
        <source>Reset To Default</source>
        <translation>དང་ཐོག་གི་པར་སླར་གསོ་བྱེད་པ།</translation>
        <extra-contents_path>/Screenlock/Reset To Default</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="167"/>
        <source>1min</source>
        <translation>སྐར་མ1</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="167"/>
        <source>5min</source>
        <translation>སྐར་མ5</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="167"/>
        <source>10min</source>
        <translation>སྐར་མ10</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="167"/>
        <source>30min</source>
        <translation>སྐར་མ30</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="167"/>
        <source>45min</source>
        <translation>སྐར་མ45</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="168"/>
        <source>1hour</source>
        <translation>དུས་ཚོད་1</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="168"/>
        <source>2hour</source>
        <translation>དུས་ཚོད་2</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="168"/>
        <source>3hour</source>
        <translation>དུས་ཚོད3</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="168"/>
        <source>Never</source>
        <translation>གཏན་ནས་བྱེད་མི་དཔེ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="457"/>
        <source>Wallpaper files(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp)</source>
        <translation>པར་རིས་ཀྱི་ཡིག་ཆ། (*.jpg *.jpeg *.bmp *.dib *.png *.jfif *jpe *.gif *.tif *.tiff *wdp)</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="499"/>
        <source>select custom wallpaper file</source>
        <translation>རང་ཉིད་ཀྱི་གྱང་ཤོག་ཡིག་ཆ་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="500"/>
        <source>Select</source>
        <translation>བདམས་ཐོན་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="501"/>
        <source>Position: </source>
        <translation>གོ་གནས་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="502"/>
        <source>FileName: </source>
        <translation>ཡིག་ཆའི་མིང་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="503"/>
        <source>FileType: </source>
        <translation>ཡིག་ཆའི་རིགས་དབྱིབས་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screenlock/screenlock.cpp" line="504"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
</context>
<context>
    <name>Screensaver</name>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.ui" line="59"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="99"/>
        <source>Screensaver</source>
        <translation>བརྙན་ཤེལ་གྱི་བརྙན་ཤེལ་འཕྲུལ་ཆས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.ui" line="201"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="200"/>
        <source>Idle time</source>
        <translation>དུས་ཚོད་འདིའི་རྗེས་སུ་ཡོལ་སྒྲོམ་ཁ་ཕྱེ།</translation>
        <extra-contents_path>/Screensaver/Idle time</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.ui" line="475"/>
        <source>Lock screen when activating screensaver</source>
        <translation>བརྙན་ཤེལ་ལ་སྐུལ་སློང་བྱེད་སྐབས་བརྙན་ཤེལ་ལ་ཟྭ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.ui" line="297"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="198"/>
        <source>Screensaver program</source>
        <translation>འཆར་ངོས་སྲུང་སྐྱོབ་ཀྱི་གོ་རིམ།</translation>
        <extra-contents_path>/Screensaver/Screensaver program</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="186"/>
        <source>View</source>
        <translation>ལྟ་ཚུལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="227"/>
        <source>UKUI</source>
        <translation>UKUI</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="228"/>
        <source>Blank_Only</source>
        <translation>འཆར་ངོས་ནག་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="239"/>
        <source>Customize</source>
        <translation>མཚན་ཉིད་རང་འཇོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="247"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="775"/>
        <source>5min</source>
        <translation>སྐར་མ5</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="247"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="776"/>
        <source>10min</source>
        <translation>སྐར་མ10</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="247"/>
        <source>15min</source>
        <translation>སྐར་མ15</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="247"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="777"/>
        <source>30min</source>
        <translation>སྐར་མ30</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="247"/>
        <source>1hour</source>
        <translation>དུས་ཚོད་1</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="248"/>
        <source>Never</source>
        <translation>གཏན་ནས་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="677"/>
        <source>Screensaver source</source>
        <translation>བརྙན་ཤེལ་གྱི་འབྱུང་ཁུངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="683"/>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="733"/>
        <source>Select</source>
        <translation>བདམས་ཐོན་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="691"/>
        <source>Wallpaper files(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp *.svg)</source>
        <translation>གྱང་ཤོག་ཡིག་ཆ། (*.jpg *.jpeg *.bmp *.dib *.png *.jfif *jpe *.gif *.tif *.tiff *.svg)</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="732"/>
        <source>select custom screensaver dir</source>
        <translation>རང་གིས་གཏན་འཁེལ་བྱས་པའི་འཆར་ངོས་སྲུང་སྐྱོབ་ཀྱི་ལམ་བུ་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="734"/>
        <source>Position: </source>
        <translation>གོ་གནས་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="735"/>
        <source>FileName: </source>
        <translation>ཡིག་ཆའི་མིང་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="736"/>
        <source>FileType: </source>
        <translation>ཡིག་ཆའི་རིགས་དབྱིབས་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="737"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="770"/>
        <source>Switching time</source>
        <translation>བརྗེ་རེས་བྱེད་པའི་དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="774"/>
        <source>1min</source>
        <translation>སྐར་མ1</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="821"/>
        <source>Ordinal</source>
        <translation>གོ་རིམ་ལྟར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="822"/>
        <source>Random</source>
        <translation>སྐབས་བསྟུན་རང་བཞིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="830"/>
        <source>Random switching</source>
        <translation>སྐབས་བསྟུན་གྱིས་བརྗེ་རེས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="873"/>
        <source>Text(up to 30 characters):</source>
        <translation>ཡི་གེ(ཆེས་མང་ན་ཡི་གེ་30ཡོད་པ་གཤམ་གསལ། )</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="907"/>
        <source>Show rest time</source>
        <translation>ངལ་གསོའི་དུས་ཚོད་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="916"/>
        <source>Lock screen when screensaver boot</source>
        <translation>བརྙན་ཤེལ་གྱི་ལྷམ་ཡུ་རིང་གི་དུས་སུ་བརྙན་ཤེལ་ལ་ཟྭ་རྒྱག</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="947"/>
        <source>Text position</source>
        <translation>ཡིག་ཆའི་གནས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="955"/>
        <source>Centered</source>
        <translation>ལྟེ་བར་འཛིན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/screensaver/screensaver.cpp" line="956"/>
        <source>Randow(Bubble text)</source>
        <translation>སྐབས་བསྟུན(ལྦུ་བའི་ཡིག་ཆ།)</translation>
    </message>
</context>
<context>
    <name>SearchWidget</name>
    <message>
        <location filename="../../searchwidget.cpp" line="175"/>
        <location filename="../../searchwidget.cpp" line="179"/>
        <location filename="../../searchwidget.cpp" line="180"/>
        <location filename="../../searchwidget.cpp" line="185"/>
        <source>No search results</source>
        <translation>འཚོལ་ཞིབ་བྱས་འབྲས་མེད་པ།</translation>
    </message>
</context>
<context>
    <name>ShareMain</name>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="240"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="240"/>
        <source>please select an output</source>
        <translation>ཁྱེད་ཀྱིས་ཐོན་རྫས་ཤིག་འདེམས་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="302"/>
        <source>Input Password</source>
        <translation>ནང་འཇུག་གི་གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="303"/>
        <source>Password</source>
        <translation>གསང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="395"/>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="410"/>
        <source>Password length must be less than or equal to 8</source>
        <translation>གསང་བའི་རིང་ཚད་ངེས་པར་དུ་8ལས་ཉུང་བའམ་ཡང་ན་8དང་མཚུངས་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="402"/>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="457"/>
        <source>Password can not be blank</source>
        <translation>གསང་གྲངས་སྟོང་བ་ཡིན་མི་སྲིད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="419"/>
        <source>Share</source>
        <translation>མཉམ་སྤྱོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="473"/>
        <source>Output</source>
        <translation>ཕྱིར་འདོན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="485"/>
        <source>Input</source>
        <translation>ནང་འཇུག</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="495"/>
        <source>Point</source>
        <translation>ཙིག་རྟགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="497"/>
        <source>Keyboard</source>
        <translation>མཐེབ་གཞོང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="499"/>
        <source>Clipboard</source>
        <translation>གཏུབ་པང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="525"/>
        <source>Close</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="527"/>
        <source>ViewOnly</source>
        <translation>ལྟ་ཚུལ་འཛིན་སྟངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="535"/>
        <source>Client Setting</source>
        <translation>མཁོ་མཁན་ཕྱོགས་བཀོད་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="546"/>
        <source>Client Number</source>
        <translation>མཁོ་མཁན་ཕྱོགས་ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="560"/>
        <source>Client IP：</source>
        <translation>མཁོ་མཁན་ཕྱོགས་ཀྱིIP་ས་གནས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="691"/>
        <source>退出程序</source>
        <translation>གོ་རིམ་ཕྱིར་འདོན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="691"/>
        <source>确认退出程序！</source>
        <translation>ཕྱིར་འབུད་ཀྱི་གོ་རིམ་གཏན་འཁེལ་བྱོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="59"/>
        <source>Remote Desktop</source>
        <translation>རྒྱང་རིང་གི་ཅོག་ཙེ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="69"/>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="435"/>
        <source>Allow others to view your desktop</source>
        <translation>མི་གཞན་པས་རྒྱང་རིང་ནས་ཁྱེད་ཀྱི་ཅོག་ངོས་སྦྲེལ་ཆོག</translation>
        <extra-contents_path>/Vino/Allow others to view your desktop</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="85"/>
        <source>Allow connection to control screen</source>
        <translation>མི་གཞན་པས་རྒྱང་རིང་ནས་ཁྱོད་ཀྱི་ཅོག་ངོས་སྦྲེལ་ཆོག་པ་མ་ཟད་ཁྱོད་ཀྱི་འཆར་ངོས་ཚོད་འཛིན་བྱེད་ཆོག</translation>
        <extra-contents_path>/Vino/Allow connection to control screen</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="446"/>
        <source>Security</source>
        <translation>བདེ་འཇགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="101"/>
        <source>You must confirm every visit for this machine</source>
        <translation>ཁྱེད་ཚོས་ངེས་པར་དུ་འཕྲུལ་ཆས་འདིའི་འཚམས་འདྲི་ཚང་མ་གཏན་འཁེལ་བྱེད།</translation>
        <extra-contents_path>/Vino/You must confirm every visit for this machine</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/vino/sharemain.cpp" line="117"/>
        <location filename="../../../plugins/system/vino_hw/sharemain.cpp" line="455"/>
        <source>Require user to enter this password: </source>
        <translation>སྤྱོད་མཁན་གྱིས་གསང་གྲངས་འདིའི་ནང་དུ་འཇུག་དགོས་པའི་བླང་བྱ་ བཏོན། </translation>
        <extra-contents_path>/Vino/Require user to enter this password:</extra-contents_path>
    </message>
</context>
<context>
    <name>Shortcut</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.ui" line="50"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="162"/>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="176"/>
        <source>System Shortcut</source>
        <translation>མ་ལག་གི་མྱུར་ལམ།</translation>
        <extra-contents_path>/Shortcut/System Shortcut</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.ui" line="103"/>
        <source>Custom Shortcut</source>
        <translation>རང་ཉིད་ཀྱི་མཚན་ཉིད་མགྱོགས་མཐེབ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="78"/>
        <source>Shortcut</source>
        <translation>མགྱོགས་མཐེབ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="159"/>
        <source>Add</source>
        <translation>ཁ་སྣོན་བརྒྱབ་པ།</translation>
        <extra-contents_path>/Shortcut/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="164"/>
        <source>Customize Shortcut</source>
        <translation>མགྱོགས་ལམ་གཏན་འཁེལ་བྱ་དགོས།</translation>
        <extra-contents_path>/Shortcut/Customize Shortcut</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="430"/>
        <source>Edit</source>
        <translation>རྩོམ་སྒྲིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="431"/>
        <source>Delete</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/shortcut.cpp" line="711"/>
        <source> or </source>
        <translation> ཡང་ན་དེ་ལྟར་ </translation>
    </message>
</context>
<context>
    <name>StatusDialog</name>
    <message>
        <location filename="../../../plugins/system/about/statusdialog.cpp" line="10"/>
        <source>About</source>
        <translation>འབྲེལ་ཡོད་ཀྱི་སྐོར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/statusdialog.cpp" line="59"/>
        <source>Activation Code</source>
        <translation>གྲུང་སློང་གི་ཚབ་རྟགས།</translation>
    </message>
</context>
<context>
    <name>Theme</name>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="212"/>
        <source>Window Theme</source>
        <translation>སྒེའུ་ཁུང་གི་བརྗོད་བྱ་གཙོ་བོ།</translation>
        <extra-contents_path>/Theme/Window Theme</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="214"/>
        <source>Icon theme</source>
        <translation>མཚོན་རྟགས་ཀྱི་བརྗོད་བྱ་གཙོ་བོ།</translation>
        <extra-contents_path>/Theme/Icon theme</extra-contents_path>
    </message>
    <message>
        <source>Control theme</source>
        <translation type="vanished">ཚོད་འཛིན་གྱི་བརྗོད་བྱ་</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="217"/>
        <source>Cursor theme</source>
        <translation>འོད་རྟགས།</translation>
        <extra-contents_path>/Theme/Cursor theme</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.ui" line="134"/>
        <source>Effect setting</source>
        <translation>ཕན་འབྲས་ཀྱི་སྒྲིག་གཞི།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.ui" line="249"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="220"/>
        <source>Performance mode</source>
        <translation>གྲུབ་འབྲས་ཐོབ་སྟངས།</translation>
        <extra-contents_path>/Theme/Performance mode</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.ui" line="352"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="222"/>
        <source>Transparency</source>
        <translation>ཕྱི་གསལ་ནང་གསལ།</translation>
        <extra-contents_path>/Theme/Transparency</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.ui" line="473"/>
        <source>Reset to default</source>
        <translation>ཁས་ལེན་སླར་གསོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="110"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="528"/>
        <source>Theme</source>
        <translation>བརྗོད་བྱ་གཙོ་བོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="186"/>
        <source>Default</source>
        <translation>སོར་བཞག</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="182"/>
        <source>Light</source>
        <translation>མདོག་སྐྱ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="184"/>
        <source>Dark</source>
        <translation>མདོག་ནག</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="186"/>
        <source>Auto</source>
        <translation>རང་འགུལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="432"/>
        <source>Corlor</source>
        <translation>ནན་བཤད་མདོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="600"/>
        <source>Other</source>
        <translation>འབྲེལ་ཡོད་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="605"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="618"/>
        <source>Set</source>
        <translation>སྒྲིག་བཀོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="610"/>
        <source>Wallpaper</source>
        <translation>ཅོག་ངོས་གྱང་ཤོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="623"/>
        <source>Beep</source>
        <translation>གསལ་སྟོན་སྒྲ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="944"/>
        <source>Blue-Crystal</source>
        <translation>འདན་སྔོན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="946"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="993"/>
        <source>Light-Seeking</source>
        <translation>འོད་ཟེར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="948"/>
        <source>DMZ-Black</source>
        <translation>DMZ-ནག་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="950"/>
        <source>DMZ-White</source>
        <translation>དཀར་པོ་དཔར་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="952"/>
        <source>Dark-Sense</source>
        <translation>མུན་ནག་གི་ཚོར་སྣང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="989"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="993"/>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="999"/>
        <source>basic</source>
        <translation>གཞི་རྩའི་ཆ་ནས</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="991"/>
        <source>Classic</source>
        <translation>མཛད་སྒོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="991"/>
        <source>classic</source>
        <translation>མཛད་སྒོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="995"/>
        <source>HeYin</source>
        <translation>དཔར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="997"/>
        <source>hp</source>
        <translation>ཧུའེ་ཕུའུ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="999"/>
        <source>ukui</source>
        <translation>འོད་འཚོལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1001"/>
        <source>daybreakBlue</source>
        <translation>སྔོན་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1003"/>
        <source>jamPurple</source>
        <translation>ངུར་སྨིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1005"/>
        <source>magenta</source>
        <translation>མེ་དམར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1007"/>
        <source>sunRed</source>
        <translation>དམར་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1009"/>
        <source>sunsetOrange</source>
        <translation>ལི་མདོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1011"/>
        <source>dustGold</source>
        <translation>སེར་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/theme/theme.cpp" line="1013"/>
        <source>polarGreen</source>
        <translation>ལྗང་ཁུ།</translation>
    </message>
    <message>
        <source>default</source>
        <translation type="vanished">ཁ་ཆད་དང་འགལ་</translation>
    </message>
</context>
<context>
    <name>TimeBtn</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="81"/>
        <source>Tomorrow</source>
        <translation>སང་ཉིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="83"/>
        <source>Yesterday</source>
        <translation>ཁ་སང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="85"/>
        <source>Today</source>
        <translation>དེ་རིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="103"/>
        <source>%1 hours earlier than local</source>
        <translation>ས་གནས་དེ་གའི་ཆུ་ཚོད་%1ལས་སྔ་བ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/timeBtn.cpp" line="105"/>
        <source>%1 hours later than local</source>
        <translation>ས་གནས་དེ་གའི་ཆུ་ཚོད་%1ལས་ཆུ་ཚོད་འཕྱི་བ།</translation>
    </message>
</context>
<context>
    <name>TimeZoneChooser</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="33"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="34"/>
        <source>Confirm</source>
        <translation>གཏན་འཁེལ་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="37"/>
        <source>Change Timezone</source>
        <translation>དུས་ཚོད་བསྒྱུར་བཅོས་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="67"/>
        <source>Search Timezone</source>
        <translation>དུས་ཚོད་འཚོལ་ཞིབ་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/worldMap/timezonechooser.cpp" line="93"/>
        <source>To select a time zone, please click where near you on the map and select a city from the nearest city</source>
        <translation>དུས་ཚོད་ཀྱི་ས་ཁོངས་འདེམས་དགོས་ན། ས་བཀྲའི་སྟེང་གི་ཉེ་འདབས་ཀྱི་ས་ཆ་གང་དུ་སོང་ནས་ཆེས་ཉེ་བའི་གྲོང་ཁྱེར་ནས་གྲོང་ཁྱེར་ཞིག་འདེམས་རོགས།</translation>
    </message>
</context>
<context>
    <name>Touchpad</name>
    <message>
        <source>Touchpad</source>
        <translation type="vanished">ལག་ཐོགས་ཁ་པར།</translation>
    </message>
</context>
<context>
    <name>TouchpadUI</name>
    <message>
        <source>Touchpad Setting</source>
        <translation type="vanished">ལག་ཐོགས་ཁ་པར་གྱི་སྒྲིག་བཀོད</translation>
    </message>
    <message>
        <source>Disable touchpad when using the mouse</source>
        <translation type="vanished">བྱི་བ་བཀོལ་སྤྱོད་བྱེད་སྐབས་ལག་ཐོགས་ཁ་པར་གྱི་ཁ་པར་ལ་</translation>
        <extra-contents_path>/Touchpad/Disable touchpad when using the mouse</extra-contents_path>
    </message>
    <message>
        <source>Pointer Speed</source>
        <translation type="vanished">ཕྱོགས་སྟོན་འཁོར་ལོ་མགྱོགས་ཚད།</translation>
        <extra-contents_path>/Touchpad/Pointer Speed</extra-contents_path>
    </message>
    <message>
        <source>Slow</source>
        <translation type="vanished">དལ་མོ།དལ་མོ།</translation>
    </message>
    <message>
        <source>Fast</source>
        <translation type="vanished">མགྱོགས་མྱུར།</translation>
    </message>
    <message>
        <source>Disable touchpad when typing</source>
        <translation type="vanished">ཡི་གེ་ཡི་གེ་རྒྱག་སྐབས་ལག་ཐོགས་ཁ་པར་གྱི་ལག་ཐོགས་ཁ་པར</translation>
        <extra-contents_path>/Touchpad/Disable touchpad when typing</extra-contents_path>
    </message>
    <message>
        <source>Touch and click on the touchpad</source>
        <translation type="vanished">ལག་ཐོགས་ཁ་པར་ལ་རེག་པ་དང་འབྲེལ་མཐུད་བྱེད་པ།</translation>
        <extra-contents_path>/Touchpad/Touch and click on the touchpad</extra-contents_path>
    </message>
    <message>
        <source>Scroll bar slides with finger</source>
        <translation type="vanished">མཛུབ་མོས་འདྲེད་བརྡར་ཤོར་བ།</translation>
        <extra-contents_path>/Touchpad/Scroll bar slides with finger</extra-contents_path>
    </message>
    <message>
        <source>Scrolling area</source>
        <translation type="vanished">ཤོག་ལྷེ་རྒྱག་པའི་རྒྱ་ཁྱོན།</translation>
        <extra-contents_path>/Touchpad/Scrolling area</extra-contents_path>
    </message>
    <message>
        <source>Two-finger scrolling in the middle area</source>
        <translation type="vanished">དཀྱིལ་ཁུལ་དུ་ལག་པ་གཉིས་ཀྱིས་ཕར་འགྲོ་ཚུར་འོང་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <source>Edge scrolling</source>
        <translation type="vanished">མཐའ་འཁོར་དུ་ཕར་འགྲོ་ཚུར་འོང་བྱེད་པ།</translation>
    </message>
    <message>
        <source>Disable scrolling</source>
        <translation type="vanished">དབང་པོ་སྐྱོན་ཅན་དུ་འགྱུར་བར་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>TrialDialog</name>
    <message>
        <location filename="../../../plugins/system/about/trialdialog.cpp" line="12"/>
        <source>Set</source>
        <translation>མ་ལག་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/trialdialog.cpp" line="37"/>
        <source>Yinhe Kylin OS(Trail Version) Disclaimer</source>
        <translation>དབྱིན་ཧུ་ཆི་ལིན་བཀོལ་སྤྱོད་མ་ལག(ཚོད་ལྟའི་པར་གཞི་)་འགན་མེད་གསལ་བསྒྲགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/trialdialog.cpp" line="46"/>
        <source>Dear customer:
       Thank you for trying Yinhe Kylin OS(trail version)! This version is free for users who only try out, no commercial purpose is permitted. The trail period lasts one year and it starts from the ex-warehouse time of the OS. No after-sales service is provided during the trail stage. If any security problems occurred when user put important files or do any commercial usage in system, all consequences are taken by users. Kylin software Co., Ltd. take no legal risk in trail version.
       During trail stage,if you want any technology surpport or activate the system, please buy“Yinhe Kylin Operating System”official version or authorization by contacting 400-089-1870.</source>
        <translation>གུས་པར་འོས་པའི་སྐུ་མགྲོན་ལགས།
          ཁམ་བཟང་།སྐབས་བསྟུན་སྒྲིག་སྦྱོར་བྱས་པའི་&quot;་དགུ་ཚིགས་ཆི་ལིན་བཀོལ་སྤྱོད་མ་ལག་(་ཚོད་སྤྱོད་པར་གཞི་)་&quot;་ཞེས་པ་ནི་པར་གཞི་དེ་དང་དོ་མཉམ་པའི་ལས་རིགས་མཁོ་མཁན་གྱི་རིན་མེད་ཚོད་སྤྱོད་པར་གཞི་ལ་དམིགས་ནས་འཕྲུལ་ཆས་ཧྲིལ་པོའི་ཚོད་སྤྱོད་དང་།ཚོད་ལྟ།དཔྱད་དཔོག་བཅས་ལ་སྤྱོད་པ་ལས་ཚོང་ལས་ཀྱི་སྤྱོད་སྒོ་གཞན་གང་ལའང་སྤྱོད་མི་རུང་།ཚོད་ལྟའི་པར་གཞི་འདི་ནི་མཉེན་ཆས་མཛོད་ཁང་ནས་དུས་ཚོད་བརྩི་བ་དང་།ཚོད་ལྟའི་དུས་ཚོད་ནི་ལོ་གཅིག་ཡིན།ཚོད་སྤྱོད་བྱེད་རིང་འབྲེལ་ཡོད་པར་གཞི་ངོ་མའི་མཉེན་ཆས་བཙོངས་རྗེས་ཀྱི་ཞབས་ཞུ་མཁོ་འདོན་མི་བྱེད་པ་དང་།གལ་ཏེ་མཁོ་མཁན་གྱིས་ཚོད་སྤྱོད་པར་གཞིའི་ཐོག་རང་འགུལ་གྱིས་ཡིག་ཆ་གལ་ཆེན་ཉར་ཚགས་དང་རང་དགར་ཚོང་ལས་ཀྱི་སྤྱོད་སྒོ་སྤེལ་ན།དེ་ལས་བྱུང་བའི་བདེ་འཇགས་ཀྱི་གནད་དོན་དང་མཇུག་འབྲས་ཚང་མ་སྤྱོད་མཁན་རང་ཉིད་ཀྱིས་འགན་འཁུར་དགོས་པ་དང་།
           ཆི་ལིན་མཉེན་ཆས་ཚད་ཡོད་ཀུང་སིས་བཅའ་ཁྲིམས་ཀྱི་ཉེན་ཁ་གང་ཡང་འཁུར་མི་དགོས།ཚོད་ལྟའི་བཀོལ་སྤྱོད་བྱེད་པའི་བརྒྱུད་རིམ་ཁྲོད།གལ་ཏེ་སྐུལ་སློང་ངམ་ཡང་ན་ཆེད་ལས་ཀྱི་ལག་རྩལ་ཞབས་ཞུའི་རྒྱབ་སྐྱོར་ཐོབ་པའི་རེ་བ་ཡོད་ན་།་ཁྱེད་ཀྱིས་&quot;་དགུ་ཚིགས་ཆི་ལིན་བཀོལ་སྤྱོད་མ་ལག་&quot;་དངོས་གཞིའི་པར་གཞིའམ་དབང་བཅོལ་ཉོ་རོགས།འབྲེལ་གཏུག་བྱེད་སྟངས་གཤམ་གསལ་ལྟར་།4000-089-1870</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/about/trialdialog.cpp" line="60"/>
        <source>Kylin software Co., Ltd.</source>
        <translation>ཆི་ལིན་མཉེན་ཆས་ཚད་ཡོད་ཀུང་སི།</translation>
    </message>
</context>
<context>
    <name>UkccAbout</name>
    <message>
        <location filename="../../ukccabout.cpp" line="33"/>
        <location filename="../../ukccabout.cpp" line="59"/>
        <source>Settings</source>
        <translation>སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../ukccabout.cpp" line="64"/>
        <source>Version: </source>
        <translation>པར་གཞི་འདི་ལྟ་སྟེ། </translation>
    </message>
    <message>
        <location filename="../../ukccabout.cpp" line="74"/>
        <source>Service and Support:</source>
        <translation>ཞབས་ཞུ་དང་རྒྱབ་སྐྱོར་བྱ་རྒྱུ་སྟེ།</translation>
    </message>
</context>
<context>
    <name>UnifiedOutputConfig</name>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="93"/>
        <location filename="../../../plugins/system/display_hw/unifiedoutputconfig.cpp" line="81"/>
        <source>resolution</source>
        <translation>འབྱེད་ཕྱོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="129"/>
        <location filename="../../../plugins/system/display_hw/unifiedoutputconfig.cpp" line="112"/>
        <source>orientation</source>
        <translation>ཁ་ཕྱོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="134"/>
        <location filename="../../../plugins/system/display_hw/unifiedoutputconfig.cpp" line="117"/>
        <source>arrow-up</source>
        <translation>འཁོར་སྐྱོད་མི་བྱེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="135"/>
        <location filename="../../../plugins/system/display_hw/unifiedoutputconfig.cpp" line="118"/>
        <source>90° arrow-right</source>
        <translation>90°མདའ་གཡས་ཕྱོགས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="137"/>
        <location filename="../../../plugins/system/display_hw/unifiedoutputconfig.cpp" line="120"/>
        <source>arrow-down</source>
        <translation>གོང་འོག་གོ་ལྡོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="136"/>
        <location filename="../../../plugins/system/display_hw/unifiedoutputconfig.cpp" line="119"/>
        <source>90° arrow-left</source>
        <translation>90°མདའ་གཡོན་ཕྱོགས་སུ་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="169"/>
        <location filename="../../../plugins/system/display_hw/unifiedoutputconfig.cpp" line="149"/>
        <source>frequency</source>
        <translation>གསར་འདོན་ཕྱོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="225"/>
        <source>screen zoom</source>
        <translation>བརྙན་ཤེལ་ཆེ་རུ་གཏོང་བ།</translation>
        <extra-contents_path>/Display/screen zoom</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/unifiedoutputconfig.cpp" line="395"/>
        <location filename="../../../plugins/system/display_hw/unifiedoutputconfig.cpp" line="154"/>
        <location filename="../../../plugins/system/display_hw/unifiedoutputconfig.cpp" line="343"/>
        <source>auto</source>
        <translation>རང་འགུལ།</translation>
    </message>
</context>
<context>
    <name>UserInfo</name>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="41"/>
        <source>Current User</source>
        <translation>མིག་སྔའི་སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="309"/>
        <source>Password</source>
        <translation>གསང་གྲངས་བཟོ་བཅོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="331"/>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="189"/>
        <source>Type</source>
        <translation>རྩིས་ཐེམ་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="353"/>
        <source>Group</source>
        <translation>སྤྱོད་མཁན་ཚོ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="500"/>
        <source>Automatic login at boot</source>
        <translation>རང་འགུལ་གྱིས་ཐོ་འགོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="422"/>
        <source>Login no passwd</source>
        <translation>རིན་མེད་ཐོ་འགོད།</translation>
    </message>
    <message>
        <source>enable autoLogin</source>
        <translation type="vanished">རང་འགུལ་གྱིས་རང་འགུལ་གྱིས་རང་འགུལ</translation>
        <extra-contents_path>/Userinfo/enable autoLogin</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.ui" line="568"/>
        <source>Other Users</source>
        <translation>སྤྱོད་མཁན་གཞན་དག</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1124"/>
        <source>root</source>
        <translation>རྩ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="149"/>
        <source>CurrentUser</source>
        <translation>མིག་སྔར་གྱི་སྤྱོད་མཁན།</translation>
        <extra-contents_path>/Userinfo/CurrentUser</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="155"/>
        <source>OthersUser</source>
        <translation>སྤྱོད་མཁན་གཞན་དག</translation>
        <extra-contents_path>/Userinfo/OthersUser</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="184"/>
        <source>Passwd</source>
        <translation>གསང་གྲངས་བཟོ་བཅོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="194"/>
        <source>Groups</source>
        <translation>སྤྱོད་མཁན་ཚོ་ཆུང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="151"/>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="249"/>
        <source>LoginWithoutPwd</source>
        <translation>གསང་གྲངས་མེད་པའི་ཐོ་འགོད།</translation>
        <extra-contents_path>/Userinfo/LoginWithoutPwd</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="153"/>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="270"/>
        <source>AutoLoginOnBoot</source>
        <translation>སྒོ་ཕྱེ་ནས་རང་འགུལ་གྱིས་ཐོ་འགོད་བྱས།</translation>
        <extra-contents_path>/Userinfo/AutoLoginOnBoot</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="157"/>
        <source>Add</source>
        <translation>ཁ་སྣོན་བརྒྱབ་པ།</translation>
        <extra-contents_path>/Userinfo/Add</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="606"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="606"/>
        <source>The user is logged in, please delete the user after logging out</source>
        <translation>སྤྱོད་མཁན་གྱིས་ཐོ་འགོད་བྱས་ཚར་རྗེས་སྤྱོད་མཁན་བསུབ་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="997"/>
        <source>The account type of “%1” has been modified, will take effect after logout, whether to logout?</source>
        <translation>&quot;%1&quot;ཡི་རྩིས་ཐོའི་རིགས་ལ་བཟོ་བཅོས་བརྒྱབ་ཟིན་པས་ཐོ་འགོད་བྱས་རྗེས་ནུས་པ་འཐོན་རྒྱུ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="998"/>
        <source>logout later</source>
        <translation>རྗེས་སུ་ཐོ་འགོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="999"/>
        <source>logout now</source>
        <translation>ད་ལྟ་ཐོ་འགོད་བྱེད་བཞིན་ཡོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1092"/>
        <source>Hint</source>
        <translation>གསལ་འདེབས་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1093"/>
        <source>The system only allows one user to log in automatically.After it is turned on, the automatic login of other users will be turned off.Is it turned on?</source>
        <translation>མ་ལག་འདིས་སྤྱོད་མཁན་གཅིག་གིས་རང་འགུལ་གྱིས་ཐོ་འགོད་བྱེད་དུ་འཇུག་པ་རེད། ཁ་ཕྱེ་རྗེས་སྤྱོད་མཁན་གཞན་དག་གི་རང་འགུལ་ཐོ་འགོད་ཀྱི་སྒོ་རྒྱག་རྒྱུ་རེད། ཁ་ཕྱེ་བ་ཡིན་ནམ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1096"/>
        <source>Trun on</source>
        <translation>ཁ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1097"/>
        <source>Close on</source>
        <translation>སྒོ་རྒྱག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1120"/>
        <source>Standard</source>
        <translation>སྤྱོད་མཁན་གྱི་ཚད་གཞི།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/userinfo.cpp" line="1122"/>
        <source>Admin</source>
        <translation>དོ་དམ་པ།</translation>
    </message>
</context>
<context>
    <name>UserInfoIntel</name>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.ui" line="83"/>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="145"/>
        <source>Current User</source>
        <translation>མིག་སྔའི་སྤྱོད་མཁན།</translation>
        <extra-contents_path>/UserinfoIntel/Current User</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.ui" line="314"/>
        <source>Change phone</source>
        <translation>ཁ་པར་བརྗེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.ui" line="359"/>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="156"/>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="1145"/>
        <source>Change pwd</source>
        <translation>གསང་གྲངས་བརྗེ་བ།</translation>
        <extra-contents_path>/UserinfoIntel/Change pwd</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.ui" line="410"/>
        <source>User group</source>
        <translation>སྤྱོད་མཁན་ཚོགས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.ui" line="436"/>
        <source>Del user</source>
        <translation>སྤྱོད་མཁན་བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.ui" line="523"/>
        <source>system reboot</source>
        <translation>མ་ལག་བསྐྱར་དུ་འཁོར་སྐྱོད་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.ui" line="578"/>
        <source>Unclosed apps start after a restart</source>
        <translation>ཁྱབ་བསྒྲགས་བྱས་མེད་པའི་ཉེར་སྤྱོད་གོ་རིམ་བསྐྱར་དུ་འགོ་ཚུགས་རྗེས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.ui" line="647"/>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="148"/>
        <source>Other Users</source>
        <translation>སྤྱོད་མཁན་གཞན་དག</translation>
        <extra-contents_path>/UserinfoIntel/Other Users</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="60"/>
        <source>User Info Intel</source>
        <translation>རྩིས་ཐོའི་ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="151"/>
        <source>Change Tel</source>
        <translation>གློག་འཕྲིན་བརྗེ་བ།</translation>
        <extra-contents_path>/UserinfoIntel/Change Tel</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="175"/>
        <source>Delete user</source>
        <translation>སྤྱོད་མཁན་བསུབ་པ།</translation>
        <extra-contents_path>/UserinfoIntel/Delete user</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="178"/>
        <source>Change user name</source>
        <translation>སྤྱོད་མཁན་གྱི་མིང་བསྒྱུར་དགོས།</translation>
        <extra-contents_path>/UserinfoIntel/Change user name</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="191"/>
        <source>standard user</source>
        <translation>ཚད་ལྡན་སྤྱོད་མཁན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="193"/>
        <source>administrator</source>
        <translation>སྤྱོད་མཁན་དོ་དམ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="202"/>
        <source>root</source>
        <translation>རྩ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="432"/>
        <source>Add new user</source>
        <translation>སྤྱོད་མཁན་གསར་པ་ཁ་སྣོན་བྱེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="751"/>
        <source>set pwd</source>
        <translation>གསང་གྲངས་སྒྲིག་བཀོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo_intel/userinfo_intel.cpp" line="765"/>
        <source>Change</source>
        <translation>བསྒྱུར་བཅོས་བྱ་དགོས།</translation>
    </message>
</context>
<context>
    <name>UtilsForUserinfo</name>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="32"/>
        <source>Passwd</source>
        <translation>གསང་གྲངས་བཟོ་བཅོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="36"/>
        <source>Type</source>
        <translation>རྩིས་ཐོ་རིགས་དབྱིབས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="40"/>
        <source>Del</source>
        <translation>བསུབ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="210"/>
        <source>Standard</source>
        <translation>ཚད་གཞི།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/utilsforuserinfo.cpp" line="212"/>
        <source>Admin</source>
        <translation>སྲིད་འཛིན་དོ་དམ།</translation>
    </message>
</context>
<context>
    <name>Vino</name>
    <message>
        <location filename="../../../plugins/system/vino/vino.cpp" line="27"/>
        <location filename="../../../plugins/system/vino_hw/vino_hw.cpp" line="25"/>
        <source>Vino</source>
        <translation>རྒྱང་རིང་གི་ཅོག་ངོས།</translation>
    </message>
</context>
<context>
    <name>Vpn</name>
    <message>
        <location filename="../../../plugins/network/vpn/vpn.cpp" line="31"/>
        <source>Vpn</source>
        <translation>Vpn</translation>
    </message>
    <message>
        <location filename="../../../plugins/network/vpn/vpn.cpp" line="88"/>
        <source>Add a vpn connection</source>
        <translation>vpnསྦྲེལ་མཐུད་ཁ་སྣོན་བྱས་པ།</translation>
        <extra-contents_path>/Vpn/Add a vpn connection</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/network/vpn/vpn.ui" line="53"/>
        <source>VPN</source>
        <translation>Vpn</translation>
    </message>
</context>
<context>
    <name>Wallpaper</name>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="103"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="149"/>
        <source>Desktop Background</source>
        <translation>ཅོག་ཙེའི་རྒྱབ་ལྗོངས།</translation>
        <extra-contents_path>/Wallpaper/Desktop Background</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="398"/>
        <source>Mode</source>
        <translation>བྱེད་སྟངས་མངོན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="537"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="151"/>
        <source>Browse</source>
        <translation>ལྟ་ཀློག་བྱེད་པ།</translation>
        <extra-contents_path>/Wallpaper/Browse</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="544"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="153"/>
        <source>Online Picture</source>
        <translation>དྲ་ཐོག་པར་རིས།</translation>
        <extra-contents_path>/Wallpaper/Online Picture</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="576"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="163"/>
        <source>Reset To Default</source>
        <translation>ཐོག་མའི་སླར་གསོ་།</translation>
        <extra-contents_path>/Wallpaper/Reset To Default</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="540"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="583"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.ui" line="331"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="57"/>
        <source>Background</source>
        <translation>རྒྱབ་ལྗོངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="178"/>
        <source>picture</source>
        <translation>པར་རིས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="178"/>
        <source>color</source>
        <translation>ཁ་དོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="194"/>
        <source>wallpaper</source>
        <translation>གྱང་ཤོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="194"/>
        <source>centered</source>
        <translation>ལྟེ་བར་བཟུང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="194"/>
        <source>scaled</source>
        <translation>གཞི་ཁྱོན་ལྡན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="194"/>
        <source>stretched</source>
        <translation>བརྐྱངས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="194"/>
        <source>zoom</source>
        <translation>རང་འཚམ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="194"/>
        <source>spanned</source>
        <translation>ཁྱབ་ཁོངས་ལས་བརྒལ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="495"/>
        <source>Wallpaper files(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *.jpe *.gif *.tif *.tiff *.wdp)</source>
        <translation>པར་རིས་ཡིག་ཆ།(*.jpg *.jpeg *.bmp *.dib *.png *.jfif *jpe *.gif *.tif *.tiff *wdp)</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="535"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="578"/>
        <source>select custom wallpaper file</source>
        <translation>རང་ཉིད་ཀྱི་གྱང་ཤོག་ཡིག་ཆ་འདེམས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="536"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="579"/>
        <source>Select</source>
        <translation>བདམས་ཐོན་བྱུང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="537"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="580"/>
        <source>Position: </source>
        <translation>གོ་གནས་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="538"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="581"/>
        <source>FileName: </source>
        <translation>ཡིག་ཆའི་མིང་ནི། </translation>
    </message>
    <message>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="539"/>
        <location filename="../../../plugins/personalized/wallpaper/wallpaper.cpp" line="582"/>
        <source>FileType: </source>
        <translation>ཡིག་ཆའི་རིགས་དབྱིབས་ནི། </translation>
    </message>
</context>
<context>
    <name>Widget</name>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="806"/>
        <source>night mode</source>
        <translation>མཚན་མོའི་རྣམ་པ།</translation>
        <extra-contents_path>/Display/night mode</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="546"/>
        <source>screen zoom</source>
        <translation>བརྙན་ཤེལ་ཆེ་རུ་གཏོང་བ།</translation>
        <extra-contents_path>/display/screen zoom</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="557"/>
        <source>Some applications need to be logouted to take effect</source>
        <translation>གོ་རིམ་ཁ་ཤས་མེད་པར་བཟོས་ནས་ནུས་པ་ཐོན་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="637"/>
        <source>Mirror Display</source>
        <translation>ཤེལ་བརྙན་རྣམ་པ།</translation>
        <extra-contents_path>/display/unify output</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="684"/>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be saved after 29 seconds&lt;/font&gt;</source>
        <translation>བརྙན་ཤེལ་གྱི་%1ལ་བཟོ་བཅོས་བརྒྱབ་ཟིན་པས་ཉར་ཚགས་བྱེད་དགོས་སམ། &lt;br/&gt; &lt;font style= &apos;color:#626c6e&apos;&gt;དུས་ཚོད་སྐར་ཆ་29ཡི་རྗེས་སུ་སྒྲིག་བཀོད་ཉར་ཚགས་བྱེད་རྒྱུ་རེད། &lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="1546"/>
        <source>Warnning</source>
        <translation>ཉེན་བརྡ་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="556"/>
        <source>Information</source>
        <translation>ཆ་འཕྲིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="259"/>
        <source>Night Mode</source>
        <translation>མཚན་མོའི་རྣམ་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="280"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="296"/>
        <source>Time</source>
        <translation>དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="313"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2126"/>
        <source>Custom Time</source>
        <translation>མཚན་ཉིད་རང་འཇོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="324"/>
        <source>to</source>
        <translation>དེ་ལྟར་བྱས་ན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="351"/>
        <source>Color Temperature</source>
        <translation>འཆར་ངོས་མདོག་དྲོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="354"/>
        <source>Warmer</source>
        <translation>སྔར་ལས་དྲོ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="357"/>
        <source>Colder</source>
        <translation>གྲང་ངར་ཆེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="666"/>
        <source>Multi-screen</source>
        <translation>བརྙན་ཤེལ་མང་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="671"/>
        <source>First Screen</source>
        <translation>བརྙན་ཤེལ་དང་པོ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="672"/>
        <source>Vice Screen</source>
        <translation>བརྙན་ཤེལ་གཞོན་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="674"/>
        <source>Extend Screen</source>
        <translation>བརྙན་ཤེལ་རིང་དུ་གཏོང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="675"/>
        <source>Clone Screen</source>
        <translation>བརྙན་ཤེལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="725"/>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="543"/>
        <source>monitor</source>
        <translation>འཆར་ཆས།</translation>
        <extra-contents_path>/display/monitor</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="814"/>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="649"/>
        <source>Theme follow night mode</source>
        <translation>བརྗོད་བྱ་གཙོ་བོ་མཚན་མོའི་རྣམ་པའི་འགྱུར་ལྡོག་དང་བསྟུན་ནས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="823"/>
        <source>Auto Brightness</source>
        <translation>རང་འགུལ་གྱིས་གསལ་ཚད་སྙོམས་སྒྲིག་བྱས།</translation>
        <extra-contents_path>/Display/Auto Brightness</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="827"/>
        <source>Adjust screen brightness by ambient</source>
        <translation>མཐའ་འཁོར་གྱི་ཁོར་ཡུག་གི་གསལ་ཚད་ལ་བརྟེན་ནས་རང་འགུལ་གྱིས་འཆར་ངོས་ཀྱི་གསལ་ཚད་སྙོམས་སྒྲིག་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="835"/>
        <source>Dynamic light</source>
        <translation>འགུལ་རྣམ་གྱི་འོད་སྣང་།</translation>
        <extra-contents_path>/Display/Dynamic light</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="837"/>
        <source>Optimize display content to extend battery life</source>
        <translation>ནང་དོན་ཇེ་ལེགས་སུ་གཏོང་བ་བརྒྱུད་ནས་གློག་རྫས་ཀྱི་ཚེ་ཐག་ཇེ་རིང་དུ་གཏོང་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="901"/>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="674"/>
        <source>resolution</source>
        <translation>འབྱེད་ཕྱོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="904"/>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="677"/>
        <source>orientation</source>
        <translation>ཁ་ཕྱོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="907"/>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="680"/>
        <source>frequency</source>
        <translation>གསར་འདོན་ཕྱོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="910"/>
        <source>scale</source>
        <translation>བསྡུས་གཏོང་ཕྱོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="913"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="1117"/>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="683"/>
        <source>Hint</source>
        <translation>གསལ་འདེབས་བྱེད་པ།</translation>
    </message>
    <message>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be saved after 14 seconds&lt;/font&gt;</source>
        <translation type="vanished">བརྙན་ཤེལ་གྱི་%1ལ་བཟོ་བཅོས་བརྒྱབ་ཟིན་པས་ཉར་ཚགས་བྱེད་དགོས་སམ། &lt;br/&gt; &lt;font style= &apos;color:#626c6e&apos;&gt;དུས་ཚོད་སྐར་ཆ་14ཡི་རྗེས་སུ་སྒྲིག་བཀོད་ཉར་ཚགས་བྱེད་རྒྱུ་རེད། &lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="917"/>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="687"/>
        <source>Save</source>
        <translation>ཉར་ཚགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="918"/>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="688"/>
        <source>Not Save</source>
        <translation>ཉར་ཚགས་མི་བྱེད་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="696"/>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be saved after %2 seconds&lt;/font&gt;</source>
        <translation>བརྙན་ཤེལ་གྱི་%1ལ་བཟོ་བཅོས་བརྒྱབ་ཟིན་པས་ཉར་ཚགས་བྱེད་དགོས་སམ། &lt;br/&gt; &lt;font style= &apos;color:#626c6e&apos;&gt;སྒྲིག་བཀོད་དེ་%2秒&lt;/font&gt;ཡི་རྗེས་སུ་ཉར་ཚགས་བྱེད་རྒྱུ་རེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1118"/>
        <source>The zoom function needs to log out to take effect</source>
        <translation>ཆེ་རུ་གཏོང་བའི་ནུས་པ་ཐོ་འགོད་བྱས་ནས་ནུས་པ་ཐོན་པར་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1119"/>
        <source>Log out now</source>
        <translation>ད་ལྟ་ཕྱིར་འཐེན་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1120"/>
        <source>Later</source>
        <translation>རྗེས་སུ་ཕྱིར་འཐེན་བྱ་དགོས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1470"/>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="1537"/>
        <source>Open time should be earlier than close time!</source>
        <translation>སྒོ་འབྱེད་པའི་དུས་ཚོད་ནི་ཉེ་བའི་དུས་ཚོད་ལས་སྔ་བ་རེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2126"/>
        <source>All Day</source>
        <translation>ཉིན་གང་བོར།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2126"/>
        <source>Follow the sunrise and sunset</source>
        <translation>ཉི་མ་ཤར་བ་དང་ཉི་མ་ནུབ་པའི་རྗེས་སུ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="2403"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="2417"/>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="2192"/>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="2199"/>
        <source>Brightness</source>
        <translation>གསལ་ཚད།</translation>
        <extra-contents_path>/Display/Brightness</extra-contents_path>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1553"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="1892"/>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="1839"/>
        <source>please insure at least one output!</source>
        <translation>མ་མཐའ་ཡང་འཆར་ངོས་གཅིག་གི་སྒོ་འབྱེད་པར་ཁག་ཐེག་བྱེད་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="914"/>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be restore after 14 seconds&lt;/font&gt;</source>
        <translation>བརྙན་ཤེལ་གྱི་%1ལ་བཟོ་བཅོས་བརྒྱབ་ཟིན་པས་ཉར་ཚགས་བྱེད་དགོས་སམ། &lt;br/&gt; &lt;font style= &apos;color:#626c6e&apos;&gt;དུས་ཚོད་སྐར་ཆ་14ཡི་རྗེས་སུ་སྒྲིག་བཀོད་ཉར་ཚགས་བྱེད་རྒྱུ་རེད། &lt;/font&gt;</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="927"/>
        <source>The screen %1 has been modified, whether to save it ? &lt;br/&gt;&lt;font style= &apos;color:#626c6e&apos;&gt;the settings will be restore after %2 seconds&lt;/font&gt;</source>
        <translation>བརྙན་ཤེལ་གྱི་%1ལ་བཟོ་བཅོས་བརྒྱབ་ཟིན་པས་ཉར་ཚགས་བྱེད་དགོས་སམ། &lt;br/&gt; &lt;font style= &apos;color:#626c6e&apos;&gt;སྒྲིག་བཀོད་དེ་%2秒&lt;/font&gt;ཡི་རྗེས་སུ་ཉར་ཚགས་བྱེད་རྒྱུ་རེད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1469"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="1553"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="1560"/>
        <location filename="../../../plugins/system/display/widget.cpp" line="1892"/>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="1536"/>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="1839"/>
        <source>Warning</source>
        <translation>ཐ་ཚིག་སྒྲོག་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/system/display/widget.cpp" line="1561"/>
        <location filename="../../../plugins/system/display_hw/widget.cpp" line="1547"/>
        <source>Sorry, your configuration could not be applied.
Common reasons are that the overall screen size is too big, or you enabled more displays than supported by your GPU.</source>
        <translation>དགོངས་པ་མ་ཚོམ།བཀོད་སྒྲིག་བཀོལ་སྤྱོད་བྱེད་མི་ཐུབ།</translation>
    </message>
</context>
<context>
    <name>addShortcutDialog</name>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="26"/>
        <source>Dialog</source>
        <translation>གླེང་མོལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="88"/>
        <source>Exec</source>
        <translation>གོ་རིམ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="174"/>
        <source>Name</source>
        <translation>མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="222"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="293"/>
        <source>TextLabel</source>
        <translation>ཡི་གེ་ལ་པེར་གྱིས་བཤད་རྒྱུར</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="248"/>
        <source>Key</source>
        <translation>མཐེབ་སྣོན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="361"/>
        <source>Save</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="126"/>
        <source>Open</source>
        <translation>སྒོ་ཕྱེ་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.ui" line="342"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="254"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="73"/>
        <source>Add Shortcut</source>
        <translation>མགྱོགས་ལམ་ཁ་སྣོན་བྱས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="86"/>
        <source>Please enter a shortcut</source>
        <translation>མྱུར་བགྲོད་གཞུང་ལམ་ནང་དུ་འཛུལ་རོགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="212"/>
        <source>Desktop files(*.desktop)</source>
        <translation>ཅོག་ངོས་ཡིག་ཆ། (*.desktop)</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="253"/>
        <source>select desktop</source>
        <translation>ཅོག་ངོས་བདམས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="290"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="311"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="322"/>
        <source>Invalid application</source>
        <translation>བཀོལ་སྤྱོད་འདི་བཀོལ་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="292"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="307"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="318"/>
        <source>Shortcut conflict</source>
        <translation>མགྱོགས་མཐེབ་སྡེབ་སྒྲིག་འདི་བཟུང་སྤྱོད་བྱས་ཟིན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="294"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="309"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="320"/>
        <source>Invalid shortcut</source>
        <translation>མགྱོགས་མཐེབ་སྡེབ་སྒྲིག་འདི་བཀོལ་མི་ཆོག</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="297"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="304"/>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="325"/>
        <source>Name repetition</source>
        <translation>མགྱོགས་མཐེབ་འདིའི་མིང་བསྐྱར་ཟློས་བྱས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="331"/>
        <source>Unknown error</source>
        <translation>ཤེས་མེད་པའི་ནོར་འཁྲུལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="507"/>
        <source>Shortcut cannot be empty</source>
        <translation>མགྱོགས་མཐེབ་སྟོང་པར་འཇོག་མི་རུང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/devices/shortcut/addshortcutdialog.cpp" line="511"/>
        <source>Name cannot be empty</source>
        <translation>མིང་སྟོང་པ་ཡིན་མི་རུང་།</translation>
    </message>
</context>
<context>
    <name>changeUserGroup</name>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="33"/>
        <source>user group</source>
        <translation>སྤྱོད་མཁན་ཚོགས་པ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="122"/>
        <source>Group:</source>
        <translation>སྤྱོད་མཁན་ཚོ་ཆུང་ནི།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="135"/>
        <source>GID:</source>
        <translation>སྤྱོད་མཁན་ID:</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="149"/>
        <source>GNum:</source>
        <translation>ཚོགས་པའི་ཁོངས་མི།:</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="193"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="615"/>
        <source>Cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="196"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="616"/>
        <source>Confirm</source>
        <translation>ཉར་ཚགས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="575"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="583"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="647"/>
        <source>Tips</source>
        <translation>གསལ་འདེབས་བྱེད་ཐབས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="575"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="647"/>
        <source>Invalid Id!</source>
        <translation>ནུས་མེད་ཚོ་ཆུང་།ID!</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="578"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="586"/>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="650"/>
        <source>OK</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="583"/>
        <source>Invalid Group Name!</source>
        <translation>གོ་མི་ཆོད་པའི་ཚོགས་པའི་མིང་།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="613"/>
        <source>Whether delete the group: “%1” ?</source>
        <translation>ཚོ་ཆུང་དེ་བསུབ་ཡོད་མེད་ལ་མ་བལྟོས་པར་&quot;%1&quot;ཡིན་ནམ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/account/userinfo/changeusergroup.cpp" line="614"/>
        <source>which will make some file components in the file system invalid!</source>
        <translation>འདིས་ཡིག་ཆའི་མ་ལག་ཁྲོད་ཀྱི་IDཡིག་ཆ་ཁ་ཤས་སྒྲིག་འཛུགས་ལ་ནུས་པ་མེད་པར་བྱས།</translation>
    </message>
</context>
<context>
    <name>changtimedialog</name>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="32"/>
        <source>Dialog</source>
        <translation>དུས་ཚོད་བསྒྱུར་བ།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="115"/>
        <source>current date</source>
        <translation>ད་ལྟའི་དུས་ཚོད་ཀྱི་ཟླ་ཚེས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="200"/>
        <source>time</source>
        <translation>དུས་ཚོད།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="321"/>
        <source>year</source>
        <translation>ལོ་གཅིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="398"/>
        <source>month</source>
        <translation>ཟླ་བ་གཅིག</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="472"/>
        <source>day</source>
        <translation>ཨང་གྲངས།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="574"/>
        <source>cancel</source>
        <translation>ཕྱིར་འཐེན།</translation>
    </message>
    <message>
        <location filename="../../../plugins/time-language/datetime/changtime.ui" line="593"/>
        <source>confirm</source>
        <translation>གཏན་འཁེལ།</translation>
    </message>
</context>
</TS>
