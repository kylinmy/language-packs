<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>Core</name>
    <message>
        <location filename="../src/controller/core/core.cpp" line="653"/>
        <source>Add</source>
        <translation>ᠵᠢᠷᠤᠭ ᠨᠡᠮᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>Information</name>
    <message>
        <source>Information</source>
        <translation type="vanished">信息</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="20"/>
        <source>Info</source>
        <translation>ᠮᠠᠳᠡᠭᠡ</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="25"/>
        <source>Name</source>
        <translation>ᠨᠡᠷᠡᠢᠳᠦᠯ</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="32"/>
        <source>Type</source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="38"/>
        <source>Capacity</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="44"/>
        <source>Size</source>
        <translation>ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="50"/>
        <source>Color</source>
        <translation>ᠦᠩᠭᠡ ᠵᠢᠨ ᠤᠷᠤᠨ ᠵᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="56"/>
        <source>Created</source>
        <translation>ᠪᠠᠢᠭᠤᠯᠤᠭᠰᠠᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <location filename="../src/view/information.cpp" line="62"/>
        <source>Modified</source>
        <translation>ᠵᠠᠰᠠᠭᠰᠠᠨ ᠴᠠᠭ</translation>
    </message>
    <message>
        <source>Format</source>
        <translation type="vanished">格式</translation>
    </message>
    <message>
        <source>Storage size</source>
        <translation type="vanished">大小</translation>
    </message>
    <message>
        <source>Pixel Size</source>
        <translation type="vanished">尺寸</translation>
    </message>
    <message>
        <source>Color Space</source>
        <translation type="vanished">颜色空间</translation>
    </message>
    <message>
        <source>Create Time</source>
        <translation type="vanished">创建时间</translation>
    </message>
    <message>
        <source>Revise Time</source>
        <translation type="vanished">修改时间</translation>
    </message>
</context>
<context>
    <name>KyView</name>
    <message>
        <source>Kylin Photo Viewer</source>
        <translation type="vanished">麒麟看图</translation>
    </message>
    <message>
        <location filename="../src/view/kyview.cpp" line="30"/>
        <location filename="../src/view/kyview.cpp" line="1341"/>
        <source>Pictures</source>
        <translation>ᠵᠢᠷᠤᠭ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/view/kyview.cpp" line="690"/>
        <location filename="../src/view/kyview.cpp" line="838"/>
        <location filename="../src/view/kyview.cpp" line="995"/>
        <source>full srceen</source>
        <translation>ᠪᠦᠳᠦᠨ ᠳᠡᠯᠭᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../src/view/kyview.cpp" line="1342"/>
        <source>Version:</source>
        <translation>ᠬᠡᠪᠯᠡᠯ:</translation>
    </message>
    <message>
        <location filename="../src/view/kyview.cpp" line="1343"/>
        <source>A system picture tool that can quickly open common formats. It provides zoom,flip and other processing simplely.</source>
        <translation>ᠵᠢᠷᠤᠭ ᠦᠵᠡᠬᠦ ᠪᠤᠯ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠵᠢᠷᠤᠭ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ ᠪᠠᠭᠠᠵᠢ᠂ ᠳᠦᠷᠭᠡᠨ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠦᠷᠬᠦᠯᠵᠢ ᠬᠠᠷᠠᠭᠳᠠᠳᠠᠭ ᠵᠠᠭᠪᠤᠷ ᠤ᠋ᠨ ᠵᠢᠷᠤᠭ ᠢ᠋ ᠤᠯᠵᠤ ᠦᠵᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ᠃ ᠡᠬᠦᠨ ᠤ᠋ ᠵᠡᠷᠭᠡᠴᠡᠭᠡ ᠠᠪᠴᠢᠭᠤᠯᠬᠤ᠂ ᠡᠷᠬᠢᠬᠦᠯᠬᠦ ᠵᠡᠷᠭᠡ ᠳᠦᠬᠦᠮ ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠯᠳᠡ ᠢ᠋ ᠬᠠᠩᠭᠠᠨ᠎ᠠ᠂ ᠠᠵᠢᠯᠯᠠᠭᠤᠯᠬᠤ ᠳ᠋ᠤ᠌ ᠳᠦᠬᠦᠮ ᠪᠠᠢᠳᠠᠭ.</translation>
    </message>
    <message>
        <location filename="../src/view/kyview.cpp" line="633"/>
        <location filename="../src/view/kyview.cpp" line="694"/>
        <location filename="../src/view/kyview.cpp" line="835"/>
        <location filename="../src/view/kyview.cpp" line="1001"/>
        <source>recovery</source>
        <translation>ᠰᠡᠷᠬᠦᠬᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>OCRResultWidget</name>
    <message>
        <location filename="../src/view/ocrresultwidget.cpp" line="14"/>
        <source>OCR recognition...</source>
        <translation>ᠶᠠᠭ ᠳᠤᠳᠤᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../src/view/ocrresultwidget.cpp" line="22"/>
        <source>No text dected</source>
        <translation>ᠲᠸᠺᠰᠲ ᠢ᠋ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠤᠯᠤᠭᠰᠠᠨ ᠦᠬᠡᠢ</translation>
    </message>
</context>
<context>
    <name>OpenImage</name>
    <message>
        <location filename="../src/view/openimage.cpp" line="20"/>
        <source>Load picture</source>
        <translation>ᠵᠢᠷᠤᠭ ᠠᠴᠢᠶᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/view/openimage.cpp" line="91"/>
        <source>Open Image</source>
        <translation>ᠵᠢᠷᠤᠭ ᠨᠡᠬᠡᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/view/openimage.cpp" line="92"/>
        <source>Image Files(</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠳᠦᠷᠦᠯ ᠵᠦᠢᠯ(</translation>
    </message>
    <message>
        <source>打开图片</source>
        <translation type="vanished">Open Image</translation>
    </message>
    <message>
        <source>文件类型(</source>
        <translation type="vanished">Image Files(</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../src/main.cpp" line="68"/>
        <source>Pictures</source>
        <translation>ᠵᠢᠷᠤᠭ ᠦᠵᠡᠬᠦ</translation>
    </message>
</context>
<context>
    <name>ShowImageWidget</name>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="40"/>
        <source>Copy</source>
        <translation>ᠺᠤᠫᠢᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="41"/>
        <source>Rename</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠨᠡᠷᠡᠯᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="42"/>
        <source>Set Desktop Wallpaper</source>
        <translation>ᠰᠢᠷᠡᠬᠡᠨ ᠨᠢᠭᠤᠷ ᠤ᠋ᠨ ᠬᠠᠨᠠᠨ ᠵᠢᠷᠤᠭ ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="43"/>
        <source>Set Lock Wallpaper</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ ᠬᠠᠨᠠᠨ ᠵᠢᠷᠤᠭ ᠪᠤᠯᠭᠠᠵᠤ ᠳᠤᠬᠢᠷᠠᠭᠤᠯᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="44"/>
        <source>Print</source>
        <translation>ᠫᠷᠢᠨᠲ᠋ᠧᠷᠯᠠᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="45"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="46"/>
        <source>Show in File</source>
        <translation>ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠬᠠᠪᠳᠠᠰᠤᠨ ᠳ᠋ᠤ᠌ ᠢᠯᠡᠷᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="47"/>
        <source>Save as</source>
        <translation>ᠦᠭᠡᠷ᠎ᠡ ᠭᠠᠵᠠᠷ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="48"/>
        <source>Markup</source>
        <translation>ᠳᠡᠮᠳᠡᠭ ᠳᠠᠯᠪᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="63"/>
        <source>Next</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="67"/>
        <source>Previous</source>
        <translation>ᠳᠡᠭᠡᠷᠡᠬᠢ ᠬᠠᠭᠤᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="98"/>
        <source>Save</source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="99"/>
        <location filename="../src/view/showimagewidget.cpp" line="116"/>
        <source>Cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="118"/>
        <source>Export</source>
        <translation>txt ᠭᠠᠷᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="438"/>
        <source>OCR recognition...</source>
        <translation>ᠶᠠᠭ ᠳᠤᠳᠤᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ...</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="504"/>
        <location filename="../src/view/showimagewidget.cpp" line="531"/>
        <location filename="../src/view/showimagewidget.cpp" line="538"/>
        <location filename="../src/view/showimagewidget.cpp" line="545"/>
        <location filename="../src/view/showimagewidget.cpp" line="965"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="504"/>
        <source>save file failed!</source>
        <translation>ᠹᠠᠢᠯ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ!</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="531"/>
        <source>save fail.name cannot begin with &quot;.&quot; </source>
        <translation>ᠬᠠᠳᠠᠭᠠᠯᠠᠵᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ᠂&quot;.&quot; ᠵᠢᠡᠷ/ ᠪᠡᠷ ᠨᠡᠷᠡᠢᠳᠴᠤ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ </translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="965"/>
        <source>是否保存对此图片的更改？</source>
        <translation>ᠳᠤᠰ ᠵᠢᠷᠤᠭ ᠢ᠋ ᠦᠬᠡᠷᠡᠴᠢᠯᠡᠭᠰᠡᠨ ᠢ᠋ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <source>save fail.name begins with &quot;.&quot; </source>
        <translation type="vanished">保存失败。</translation>
    </message>
    <message>
        <location filename="../src/view/showimagewidget.cpp" line="538"/>
        <location filename="../src/view/showimagewidget.cpp" line="545"/>
        <source>the file name is illegal</source>
        <translation>ᠬᠠᠤᠯᠢ ᠪᠤᠰᠤ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
</context>
<context>
    <name>TitleBar</name>
    <message>
        <source>Kylin Photo Viewer</source>
        <translation type="vanished">麒麟看图</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="154"/>
        <source>Pictures</source>
        <translation>ᠵᠢᠷᠤᠭ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="41"/>
        <source>minimize</source>
        <translation>ᠬᠠᠮᠤᠭ᠎ᠤᠨ ᠪᠠᠭᠠᠴᠢᠯᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="51"/>
        <source>full screen</source>
        <translation>ᠪᠦᠳᠦᠨ ᠳᠡᠯᠭᠡᠴᠡ</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="58"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="150"/>
        <source>Return</source>
        <translation>ᠪᠤᠴᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="298"/>
        <location filename="../src/view/titlebar.cpp" line="304"/>
        <location filename="../src/view/titlebar.cpp" line="330"/>
        <location filename="../src/view/titlebar.cpp" line="333"/>
        <location filename="../src/view/titlebar.cpp" line="336"/>
        <location filename="../src/view/titlebar.cpp" line="340"/>
        <source>Warning</source>
        <translation>ᠰᠡᠷᠡᠮᠵᠢᠯᠡᠬᠦᠯᠦᠯ</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="299"/>
        <source>This file will be hidden(the file whose name begins with &quot;.&quot; will be the hidden property file.)</source>
        <translation>ᠳᠤᠰ ᠹᠠᠢᠯ ᠤᠳᠠᠬᠤ ᠦᠬᠡᠢ ᠨᠢᠭᠤᠴᠠᠯᠠᠭᠳᠠᠨ᠎ᠠ( ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ&quot;.&quot; ᠵᠢᠡᠷ/ ᠪᠡᠷ ᠡᠬᠢᠯᠡᠭᠰᠡᠨ ᠨᠢ ᠤᠳᠠᠬᠤ ᠦᠬᠡᠢ ᠨᠢᠭᠤᠴᠠᠯᠠᠭᠳᠠᠭᠰᠠᠨ ᠬᠠᠷᠢᠶᠠᠳᠤ ᠹᠠᠢᠯ ᠪᠤᠯᠤᠨ᠎ᠠ.)</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="304"/>
        <source>the file name is illegal</source>
        <translation>ᠬᠠᠤᠯᠢ ᠪᠤᠰᠤ ᠹᠠᠢᠯ ᠤ᠋ᠨ ᠨᠡᠷ᠎ᠡ</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="330"/>
        <source>File does not exist (or has been deleted)!</source>
        <translation>ᠹᠠᠢᠯ ᠪᠠᠢᠬᠤ ᠦᠬᠡᠢ(ᠨᠢᠭᠡᠨᠳᠡ ᠬᠠᠰᠤᠭᠳᠠᠪᠠ)!</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="333"/>
        <source>This name has been occupied, please choose another！</source>
        <translation>ᠳᠤᠰ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠨᠢᠭᠡᠨᠳᠡ ᠬᠡᠷᠡᠭᠯᠡᠭᠳᠡᠪᠡ᠂ ᠪᠤᠰᠤᠳ ᠨᠡᠷᠡᠢᠳᠦᠯ ᠰᠤᠩᠭᠤᠭᠠᠷᠠᠢ！</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="337"/>
        <source>This is a read-only file, please modify the permissions before operation！</source>
        <translation>ᠡᠨᠡ ᠨᠢ ᠵᠦᠪᠬᠡᠨ ᠤᠩᠰᠢᠬᠤ ᠹᠠᠢᠯ᠂ ᠡᠷᠭᠡ ᠵᠢ ᠵᠠᠰᠠᠭᠰᠠᠨ ᠤᠤ ᠳᠠᠷᠠᠭ᠎ᠠ ᠠᠵᠢᠯᠯᠠᠭᠠᠷᠠᠢ！</translation>
    </message>
    <message>
        <location filename="../src/view/titlebar.cpp" line="340"/>
        <source>Other error, rename failed！</source>
        <translation>ᠪᠤᠰᠤᠳ ᠪᠤᠷᠤᠭᠤ᠂ ᠳᠠᠬᠢᠨ ᠨᠡᠷᠡᠢᠳᠴᠤ ᠴᠢᠳᠠᠭᠰᠠᠨ ᠦᠬᠡᠢ！</translation>
    </message>
    <message>
        <source>full srceen</source>
        <translation type="vanished">最大化</translation>
    </message>
    <message>
        <source>recovery</source>
        <translation type="vanished">还原</translation>
    </message>
</context>
<context>
    <name>ToolBar</name>
    <message>
        <location filename="../src/view/toolbar.cpp" line="21"/>
        <source>Zoom out</source>
        <translation>ᠪᠠᠭᠠᠰᠬᠠᠭᠰᠠᠨ ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="32"/>
        <source>View scale</source>
        <translation>ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠬᠠᠷᠢᠴᠠᠯ</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="40"/>
        <source>Zoom in</source>
        <translation>ᠬᠠᠷᠠᠭᠠᠨ ᠵᠢᠷᠤᠭ ᠢ᠋ ᠶᠡᠬᠡᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="49"/>
        <source>Life size</source>
        <translation>ᠤᠭ ᠤ᠋ᠨ ᠶᠡᠬᠡ ᠪᠠᠭ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="56"/>
        <source>Window widget</source>
        <translation>ᠵᠤᠬᠢᠴᠠᠬᠤ ᠴᠤᠩᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="89"/>
        <source>OCR</source>
        <translation>OCR</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="900"/>
        <source>Rorate left</source>
        <translation>ᠵᠡᠬᠦᠨᠰᠢ ᠡᠷᠬᠢᠯᠳᠦᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="912"/>
        <source>Rorate right</source>
        <translation>ᠪᠠᠷᠠᠭᠤᠨᠰᠢ ᠡᠷᠬᠢᠯᠳᠦᠬᠦ</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="68"/>
        <source>Flip horizontally</source>
        <translation>ᠬᠡᠪᠳᠡᠭᠡ ᠳᠤᠯᠢᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="74"/>
        <source>Flip vertically</source>
        <translation>ᠪᠤᠰᠤᠭ᠎ᠠ ᠳᠤᠯᠢᠳᠠᠰᠤ</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="81"/>
        <source>Crop</source>
        <translation>ᠡᠰᠬᠡᠬᠦ</translation>
    </message>
    <message>
        <source>Tailoring</source>
        <translation type="vanished">裁剪</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="113"/>
        <source>Sidebar</source>
        <translation>ᠬᠠᠵᠠᠭᠤ ᠲᠠᠯ᠎ᠠ ᠵᠢᠨ ᠪᠠᠭᠠᠷ</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="119"/>
        <source>Get info</source>
        <translation>ᠰᠤᠷᠠᠭ ᠵᠠᠩᠬᠢ</translation>
    </message>
    <message>
        <source>Original size</source>
        <translation type="vanished">原始尺寸</translation>
    </message>
    <message>
        <source>Adaptive widget</source>
        <translation type="vanished">图片适应窗口</translation>
    </message>
    <message>
        <source>Rorate</source>
        <translation type="vanished">旋转</translation>
    </message>
    <message>
        <source>Horizontal mirror</source>
        <translation type="vanished">水平镜像</translation>
    </message>
    <message>
        <source>Vertical mirror</source>
        <translation type="vanished">垂直镜像</translation>
    </message>
    <message>
        <source>Thumbnail</source>
        <translation type="vanished">侧栏</translation>
    </message>
    <message>
        <source>Information</source>
        <translation type="vanished">信息</translation>
    </message>
    <message>
        <location filename="../src/view/toolbar.cpp" line="126"/>
        <source>Delete</source>
        <translation>ᠬᠠᠰᠤᠬᠤ</translation>
    </message>
</context>
<context>
    <name>menuModule</name>
    <message>
        <source>Service &amp; Support Team: </source>
        <translation type="vanished">服务与支持团队： </translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="30"/>
        <location filename="../src/view/menumodule.cpp" line="220"/>
        <source>Version: </source>
        <translation>ᠬᠡᠪᠯᠡᠯ᠄ </translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="31"/>
        <source>A system picture tool that can quickly open common formats. It provides zoom,flip and other processing simplely.</source>
        <translation>ᠵᠢᠷᠤᠭ ᠦᠵᠡᠬᠦ ᠪᠤᠯ ᠰᠢᠰᠲ᠋ᠧᠮ ᠤ᠋ᠨ ᠵᠢᠷᠤᠭ ᠪᠠᠢᠴᠠᠭᠠᠵᠤ ᠦᠵᠡᠬᠦ ᠪᠠᠭᠠᠵᠢ᠂ ᠳᠦᠷᠭᠡᠨ ᠨᠡᠬᠡᠬᠡᠵᠤ ᠦᠷᠬᠦᠯᠵᠢ ᠬᠠᠷᠠᠭᠳᠠᠳᠠᠭ ᠵᠠᠭᠪᠤᠷ ᠤ᠋ᠨ ᠵᠢᠷᠤᠭ ᠢ᠋ ᠤᠯᠵᠤ ᠦᠵᠡᠵᠤ ᠪᠤᠯᠤᠨ᠎ᠠ᠃ ᠡᠬᠦᠨ ᠤ᠋ ᠵᠡᠷᠭᠡᠴᠡᠭᠡ ᠠᠪᠴᠢᠭᠤᠯᠬᠤ᠂ ᠡᠷᠬᠢᠬᠦᠯᠬᠦ ᠵᠡᠷᠭᠡ ᠳᠦᠬᠦᠮ ᠵᠢᠷᠤᠭ ᠤ᠋ᠨ ᠰᠢᠢᠳᠪᠦᠷᠢᠯᠡᠯᠳᠡ ᠢ᠋ ᠬᠠᠩᠭᠠᠨ᠎ᠠ᠂ ᠠᠵᠢᠯᠯᠠᠭᠤᠯᠬᠤ ᠳ᠋ᠤ᠌ ᠳᠦᠬᠦᠮ ᠪᠠᠢᠳᠠᠭ.</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="36"/>
        <source>menu</source>
        <translation>ᠲᠤᠪᠶᠤᠭ</translation>
    </message>
    <message>
        <source>Open..</source>
        <translation type="vanished">打开..</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="57"/>
        <source>Theme</source>
        <translation>ᠭᠤᠤᠯ ᠰᠡᠳᠦᠪ</translation>
    </message>
    <message>
        <source>Open...</source>
        <translation type="vanished">打开...</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="26"/>
        <location filename="../src/view/menumodule.cpp" line="267"/>
        <location filename="../src/view/menumodule.cpp" line="276"/>
        <source>Service &amp; Support: </source>
        <translation>ᠦᠢᠯᠡᠴᠢᠯᠡᠬᠦ ᠪᠤᠯᠤᠨ ᠳᠡᠮᠵᠢᠬᠦ ᠪᠦᠯᠬᠦᠮ: </translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="54"/>
        <location filename="../src/view/menumodule.cpp" line="105"/>
        <source>Open</source>
        <translation>ᠨᠡᠭᠡᠭᠡᠭᠦ</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="60"/>
        <location filename="../src/view/menumodule.cpp" line="103"/>
        <source>Help</source>
        <translation>ᠳᠤᠰᠠᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="63"/>
        <location filename="../src/view/menumodule.cpp" line="101"/>
        <source>About</source>
        <translation>ᠲᠤᠬᠠᠢ</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="66"/>
        <location filename="../src/view/menumodule.cpp" line="99"/>
        <source>Quit</source>
        <translation>ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="171"/>
        <location filename="../src/view/menumodule.cpp" line="189"/>
        <source>Pictures</source>
        <translation>ᠵᠢᠷᠤᠭ ᠦᠵᠡᠬᠦ</translation>
    </message>
    <message>
        <source>kylin photo view</source>
        <translation type="vanished">麒麟看图</translation>
    </message>
    <message>
        <location filename="../src/view/menumodule.cpp" line="178"/>
        <source>close</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <source>Kylin Photo View</source>
        <translation type="vanished">麒麟看图</translation>
    </message>
</context>
</TS>
