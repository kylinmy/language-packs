<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_Hant">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="../UI/aboutdialog.cpp" line="26"/>
        <source>Service &amp; Support: </source>
        <translation>服務與支援： </translation>
    </message>
    <message>
        <location filename="../UI/aboutdialog.cpp" line="30"/>
        <location filename="../UI/aboutdialog.cpp" line="97"/>
        <source>Version: </source>
        <translation>版本： </translation>
    </message>
    <message>
        <location filename="../UI/aboutdialog.cpp" line="58"/>
        <location filename="../UI/aboutdialog.cpp" line="72"/>
        <source>Calendar</source>
        <translation>日曆</translation>
    </message>
    <message>
        <location filename="../UI/aboutdialog.cpp" line="63"/>
        <source>close</source>
        <translation>關閉</translation>
    </message>
</context>
<context>
    <name>CSchceduleDlg</name>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="140"/>
        <location filename="../UI/schedulewidget.cpp" line="148"/>
        <source>New Event</source>
        <translation>新建活動</translation>
    </message>
    <message>
        <source>Edit Event</source>
        <translation type="vanished">编辑日程</translation>
    </message>
    <message>
        <source>New Schedule</source>
        <translation type="vanished">新建日程</translation>
    </message>
    <message>
        <source>Type:</source>
        <translation type="vanished">类型:</translation>
    </message>
    <message>
        <source>Work</source>
        <translation type="vanished">工作</translation>
    </message>
    <message>
        <source>Life</source>
        <translation type="vanished">生活</translation>
    </message>
    <message>
        <source>Other</source>
        <translation type="vanished">其他</translation>
    </message>
    <message>
        <source>Description:</source>
        <translation type="vanished">描述:</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="158"/>
        <source>All Day:</source>
        <translation>整天：</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="169"/>
        <source>Starts:</source>
        <translation>開始：</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="200"/>
        <source>Ends:</source>
        <translation>結束：</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="237"/>
        <source>Theme:</source>
        <translation>主題：</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="247"/>
        <source>Remind Me:</source>
        <translation>提醒我：</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="258"/>
        <source>Repeat:</source>
        <translation>重複：</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="275"/>
        <source>Frequency:</source>
        <translation>頻率：</translation>
    </message>
    <message>
        <source>Never</source>
        <translation type="vanished">从不</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="271"/>
        <source>Daily</source>
        <translation>日常</translation>
    </message>
    <message>
        <source>Weekdays</source>
        <translation type="vanished">周末</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="272"/>
        <source>Weekly</source>
        <translation>週刊</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="273"/>
        <source>Monthly</source>
        <translation>每月</translation>
    </message>
    <message>
        <location filename="../UI/schedulewidget.cpp" line="274"/>
        <source>Yearly</source>
        <translation>每年</translation>
    </message>
    <message>
        <source>End Repeat:</source>
        <translation type="vanished">最后重复:</translation>
    </message>
    <message>
        <source>After</source>
        <translation type="vanished">之后</translation>
    </message>
</context>
<context>
    <name>DayItem</name>
    <message>
        <location filename="../UI/dayitem.cpp" line="36"/>
        <source>解析json文件错误！</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>DayView</name>
    <message>
        <location filename="../UI/dayview.cpp" line="101"/>
        <source>Monday</source>
        <translation>星期一</translation>
    </message>
    <message>
        <location filename="../UI/dayview.cpp" line="103"/>
        <source>Tuesday</source>
        <translation>星期二</translation>
    </message>
    <message>
        <location filename="../UI/dayview.cpp" line="105"/>
        <source>Wednesday</source>
        <translation>星期三</translation>
    </message>
    <message>
        <location filename="../UI/dayview.cpp" line="107"/>
        <source>Thursday</source>
        <translation>星期四</translation>
    </message>
    <message>
        <location filename="../UI/dayview.cpp" line="109"/>
        <source>Friday</source>
        <translation>星期五</translation>
    </message>
    <message>
        <location filename="../UI/dayview.cpp" line="111"/>
        <source>Saturday</source>
        <translation>星期六</translation>
    </message>
    <message>
        <location filename="../UI/dayview.cpp" line="113"/>
        <source>Sunday</source>
        <translation>星期日</translation>
    </message>
</context>
<context>
    <name>HeaderWidget</name>
    <message>
        <location filename="../UI/headerwidget.cpp" line="10"/>
        <source>腊月</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../UI/headerwidget.cpp" line="16"/>
        <source>AllDay</source>
        <translation>全天</translation>
    </message>
    <message>
        <location filename="../UI/headerwidget.cpp" line="22"/>
        <source>00</source>
        <translation></translation>
    </message>
</context>
<context>
    <name>LeftHeaderWidget</name>
    <message>
        <source>AllDay</source>
        <translation type="obsolete">全天</translation>
    </message>
</context>
<context>
    <name>LeftWidget</name>
    <message>
        <location filename="../UI/leftwidget.cpp" line="22"/>
        <location filename="../UI/leftwidget.cpp" line="82"/>
        <source>Create Schedule</source>
        <translation>創建計劃</translation>
    </message>
    <message>
        <location filename="../UI/leftwidget.cpp" line="37"/>
        <source>Yi Ji</source>
        <translation>姬毅</translation>
    </message>
    <message>
        <location filename="../UI/leftwidget.cpp" line="50"/>
        <source>Prev</source>
        <translation>昨日</translation>
    </message>
    <message>
        <location filename="../UI/leftwidget.cpp" line="53"/>
        <source>Next</source>
        <translation>下一個</translation>
    </message>
    <message>
        <source>Mon</source>
        <translation type="obsolete">周一</translation>
    </message>
    <message>
        <source>Tue</source>
        <translation type="obsolete">周二</translation>
    </message>
    <message>
        <source>Wed</source>
        <translation type="obsolete">周三</translation>
    </message>
    <message>
        <source>Thu</source>
        <translation type="obsolete">周四</translation>
    </message>
    <message>
        <source>Fri</source>
        <translation type="obsolete">周五</translation>
    </message>
    <message>
        <source>Sat</source>
        <translation type="obsolete">周六</translation>
    </message>
    <message>
        <source>Sun</source>
        <translation type="obsolete">周日</translation>
    </message>
</context>
<context>
    <name>MainWidget</name>
    <message>
        <location filename="../UI/mainwidget.cpp" line="81"/>
        <source>Calendar</source>
        <translation>日曆</translation>
    </message>
</context>
<context>
    <name>MonthItemWidget</name>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="63"/>
        <source>Jan</source>
        <translation>1月</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="65"/>
        <source>Feb</source>
        <translation>2月</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="67"/>
        <source>Mar</source>
        <translation>三月</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="69"/>
        <source>Apr</source>
        <translation>四月</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="71"/>
        <source>May</source>
        <translation>五月</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="73"/>
        <source>Jun</source>
        <translation>六月</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="75"/>
        <source>Jul</source>
        <translation>七月</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="77"/>
        <source>Aug</source>
        <translation>八月</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="79"/>
        <source>Sep</source>
        <translation>九月</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="81"/>
        <source>Oct</source>
        <translation>10月</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="83"/>
        <source>Nov</source>
        <translation>11 月</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="85"/>
        <source>Dec</source>
        <translation>12 月</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="95"/>
        <source>M</source>
        <translation>M</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="95"/>
        <source>Tu</source>
        <translation>圖</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="95"/>
        <source>Th</source>
        <translation>千</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="95"/>
        <source>Sa</source>
        <translation>薩</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="95"/>
        <source>Su</source>
        <translation>蘇</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="95"/>
        <source>W</source>
        <translation>W</translation>
    </message>
    <message>
        <location filename="../UI/monthitemwidget.cpp" line="95"/>
        <source>F</source>
        <translation>F</translation>
    </message>
</context>
<context>
    <name>MonthView</name>
    <message>
        <location filename="../UI/monthview.cpp" line="154"/>
        <source>Mon</source>
        <translation>星期一</translation>
    </message>
    <message>
        <location filename="../UI/monthview.cpp" line="154"/>
        <source>Tue</source>
        <translation>星期二</translation>
    </message>
    <message>
        <location filename="../UI/monthview.cpp" line="154"/>
        <source>Wed</source>
        <translation>結婚</translation>
    </message>
    <message>
        <location filename="../UI/monthview.cpp" line="154"/>
        <source>Thu</source>
        <translation>星期四</translation>
    </message>
    <message>
        <location filename="../UI/monthview.cpp" line="154"/>
        <source>Fri</source>
        <translation>週五</translation>
    </message>
    <message>
        <location filename="../UI/monthview.cpp" line="154"/>
        <source>Sat</source>
        <translation>坐</translation>
    </message>
    <message>
        <location filename="../UI/monthview.cpp" line="154"/>
        <source>Sun</source>
        <translation>太陽</translation>
    </message>
</context>
<context>
    <name>RightWidget</name>
    <message>
        <location filename="../UI/rightwidget.cpp" line="22"/>
        <location filename="../UI/rightwidget.cpp" line="156"/>
        <source>Day</source>
        <translation>日</translation>
    </message>
    <message>
        <location filename="../UI/rightwidget.cpp" line="23"/>
        <source>Week</source>
        <translation>周</translation>
    </message>
    <message>
        <location filename="../UI/rightwidget.cpp" line="27"/>
        <location filename="../UI/rightwidget.cpp" line="148"/>
        <location filename="../UI/rightwidget.cpp" line="152"/>
        <location filename="../UI/rightwidget.cpp" line="156"/>
        <source>Month</source>
        <translation>月</translation>
    </message>
    <message>
        <location filename="../UI/rightwidget.cpp" line="28"/>
        <location filename="../UI/rightwidget.cpp" line="143"/>
        <location filename="../UI/rightwidget.cpp" line="147"/>
        <location filename="../UI/rightwidget.cpp" line="151"/>
        <location filename="../UI/rightwidget.cpp" line="155"/>
        <location filename="../UI/rightwidget.cpp" line="223"/>
        <location filename="../UI/rightwidget.cpp" line="226"/>
        <source>Year</source>
        <translation>年</translation>
    </message>
    <message>
        <location filename="../UI/rightwidget.cpp" line="29"/>
        <source>Today</source>
        <translation>今天</translation>
    </message>
</context>
<context>
    <name>ScheduleMark</name>
    <message>
        <location filename="../UI/schedulemark.cpp" line="75"/>
        <source>Edit</source>
        <translation>編輯</translation>
    </message>
    <message>
        <location filename="../UI/schedulemark.cpp" line="76"/>
        <source>Delete</source>
        <translation>刪除</translation>
    </message>
</context>
<context>
    <name>TitleWidget</name>
    <message>
        <location filename="../UI/titlewidget.cpp" line="21"/>
        <location filename="../UI/titlewidget.cpp" line="51"/>
        <source>About</source>
        <translation>大約</translation>
    </message>
    <message>
        <location filename="../UI/titlewidget.cpp" line="22"/>
        <location filename="../UI/titlewidget.cpp" line="54"/>
        <source>Help</source>
        <translation>説明</translation>
    </message>
    <message>
        <location filename="../UI/titlewidget.cpp" line="23"/>
        <location filename="../UI/titlewidget.cpp" line="57"/>
        <source>Quit</source>
        <translation>退出</translation>
    </message>
    <message>
        <location filename="../UI/titlewidget.cpp" line="88"/>
        <source>Calendar</source>
        <translation>日曆</translation>
    </message>
    <message>
        <location filename="../UI/titlewidget.cpp" line="91"/>
        <source>Menu</source>
        <translation>功能表</translation>
    </message>
    <message>
        <location filename="../UI/titlewidget.cpp" line="100"/>
        <source>Minimize</source>
        <translation>最小化</translation>
    </message>
    <message>
        <location filename="../UI/titlewidget.cpp" line="107"/>
        <source>Max</source>
        <translation>麥克斯</translation>
    </message>
    <message>
        <location filename="../UI/titlewidget.cpp" line="117"/>
        <source>Close</source>
        <translation>關閉</translation>
    </message>
</context>
<context>
    <name>WeekView</name>
    <message>
        <source>AllDay</source>
        <translation type="vanished">全天</translation>
    </message>
    <message>
        <location filename="../UI/weekview.cpp" line="130"/>
        <source> </source>
        <translation></translation>
    </message>
    <message>
        <location filename="../UI/weekview.cpp" line="130"/>
        <source>Mon</source>
        <translation>星期一</translation>
    </message>
    <message>
        <location filename="../UI/weekview.cpp" line="130"/>
        <source>Tue</source>
        <translation>星期二</translation>
    </message>
    <message>
        <location filename="../UI/weekview.cpp" line="130"/>
        <source>Wed</source>
        <translation>結婚</translation>
    </message>
    <message>
        <location filename="../UI/weekview.cpp" line="130"/>
        <source>Thu</source>
        <translation>星期四</translation>
    </message>
    <message>
        <location filename="../UI/weekview.cpp" line="130"/>
        <source>Fri</source>
        <translation>週五</translation>
    </message>
    <message>
        <location filename="../UI/weekview.cpp" line="130"/>
        <source>Sat</source>
        <translation>坐</translation>
    </message>
    <message>
        <location filename="../UI/weekview.cpp" line="130"/>
        <source>Sun</source>
        <translation>太陽</translation>
    </message>
</context>
</TS>
