<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="mn">
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>Form</source>
        <translation>ᠬᠡᠯᠪᠡᠷᠢ ᠮᠠᠶᠢᠭ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>Switch User</source>
        <translation>ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶᠢ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>Hibernate</source>
        <translation>ᠢᠴᠡᠬᠡᠯᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>Suspend</source>
        <translation>ᠤᠨᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>Logout</source>
        <translation>ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>Reboot</source>
        <translation>ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>Shut Down</source>
        <translation>ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>Lock Screen</source>
        <translation>ᠳᠡᠯᠭᠡᠴᠡ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>cancel</source>
        <translation>ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>confirm</source>
        <translation>ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.ui"/>
        <source>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;html&gt;&lt;head/&gt;&lt;body&gt;&lt;p&gt;&lt;br/&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
</context>
<context>
    <name>QApplication</name>
    <message>
        <location filename="../tools/main.cpp" line="283"/>
        <source>UKUI session tools, show the shutdown dialog without any arguments.</source>
        <translation>UKUI ᠶᠠᠷᠢᠯᠴᠠᠭᠠᠨ᠎ᠤ ᠪᠠᠭᠠᠵᠢ ᠂ ᠶᠠᠮᠠᠷ ᠴᠤ᠌ ᠫᠠᠷᠠᠮᠸᠲᠷ ᠦᠬᠡᠢ ᠬᠠᠭᠠᠭᠰᠠᠨ ᠶᠠᠷᠢᠯᠴᠠᠭᠠᠨ᠎ᠤ ᠴᠤᠩᠬᠤ᠎ᠶᠢ ᠢᠯᠡᠷᠡᠬᠦᠯᠦᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="291"/>
        <source>Switch the user of this computer.</source>
        <translation>ᠡᠨᠡ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ᠎ᠤᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶᠢ ᠰᠤᠯᠢᠬᠤ᠄</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="294"/>
        <source>Hibernate this computer.</source>
        <translation>ᠡᠨᠡ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ᠎ᠢ ᠢᠴᠡᠬᠡᠯᠡᠬᠦᠯᠦᠬᠡᠷᠡᠢ</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="297"/>
        <source>Suspend this computer.</source>
        <translation>ᠡᠨᠡ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ᠎ᠢ ᠳᠦᠷ ᠵᠤᠭᠰᠤᠭᠠᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="300"/>
        <source>Logout this computer.</source>
        <translation>ᠡᠨᠡ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ᠎ᠢ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="303"/>
        <source>Restart this computer.</source>
        <translation>ᠡᠨᠡ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ᠎ᠢ ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠨ᠎ᠡ᠃</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="306"/>
        <source>Shutdown this computer.</source>
        <translation>ᠡᠨᠡ ᠺᠤᠮᠫᠢᠦᠲᠸᠷ᠎ᠢ ᠬᠠᠭᠠᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <source>system-monitor</source>
        <translation type="vanished">系统监视器</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="391"/>
        <source>Switch User</source>
        <translation type="unfinished">ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ᠎ᠶᠢ ᠰᠤᠯᠢᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="392"/>
        <source>Hibernate</source>
        <translation type="unfinished">ᠢᠴᠡᠬᠡᠯᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="393"/>
        <source>Suspend</source>
        <translation type="unfinished">ᠤᠨᠳᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="394"/>
        <source>Logout</source>
        <translation type="unfinished">ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="395"/>
        <source>Reboot</source>
        <translation type="unfinished">ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="396"/>
        <source>Shut Down</source>
        <translation type="unfinished">ᠬᠠᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="397"/>
        <source>Lock Screen</source>
        <translation type="unfinished">ᠳᠡᠯᠭᠡᠴᠡ ᠤᠨᠢᠰᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="423"/>
        <source>Multiple users are logged in at the same time.Are you sure you want to close this system?</source>
        <translation>ᠬᠡᠳᠦ ᠬᠡᠳᠦᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠠᠳᠠᠯᠢ ᠴᠠᠭ᠎ᠲᠤ ᠨᠡᠪᠳᠡᠷᠡᠵᠤ ᠪᠠᠢᠬᠤ ᠪᠠᠢᠳᠠᠯ ᠲᠠᠢ ᠂ ᠲᠠ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠡᠴᠡ ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="435"/>
        <source>cancel</source>
        <translation type="unfinished">ᠦᠬᠡᠢᠰᠭᠡᠬᠦ᠌</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="436"/>
        <source>confirm</source>
        <translation type="unfinished">ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="503"/>
        <source>(user),ukui-control-center is performing a system update or package installation.</source>
        <translation>（ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ） ᠂ ᠱᠢᠰᠲ᠋ᠧᠮ ᠶᠠᠭ ᠰᠠᠢᠬᠠᠨ ᠳᠡᠰ ᠳᠡᠪᠱᠢᠬᠦ᠌ ᠪᠤᠶᠤᠰᠤᠹᠲ᠎ᠤᠨ ᠪᠠᠭᠯᠠᠭ᠎ᠠ᠎ᠶᠢ ᠤᠭᠰᠠᠷᠠᠬᠤ/ᠪᠠᠭᠤᠯᠭᠠᠬᠤ ᠬᠠᠮᠢᠶᠠᠳᠠᠢ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ᠎ᠶᠢ ᠬᠦᠢᠴᠡᠳᠭᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="505"/>
        <source>(user),yhkylin-backup-tools is performing a system backup or restore.</source>
        <translation>（ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ） ᠂ᠬᠤᠪᠢᠯᠪᠤᠷᠢ᠎ᠶᠢ ᠬᠡᠪ᠎ᠲᠦ᠍ ᠨᠢ ᠤᠷᠤᠭᠤᠯᠬᠤ ᠪᠠᠭᠠᠵᠢ ᠶᠠᠭ ᠰᠠᠢᠬᠠᠨᠬᠠᠭᠤᠯᠤᠨ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠪᠤᠶᠤ ᠬᠡᠪ᠎ᠲᠦ᠍ ᠨᠢ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠃</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="507"/>
        <source>For system security,Reboot、Shutdown、Logout and Hibernate are temporarily unavailable.</source>
        <translation>ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠠᠶᠤᠯᠬᠦᠢ᠎ᠶᠢᠨ ᠳᠦᠯᠦᠬᠡ ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ᠌ ᠂ ᠬᠠᠭᠠᠬᠤ ᠂ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ ᠪᠤᠯᠤᠨ ᠢᠴᠡᠬᠡᠯᠡᠬᠦ᠌ ᠴᠢᠳᠠᠪᠬᠢ᠎ᠶᠢ ᠲᠦᠷ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ᠍ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠃</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="509"/>
        <source>For system security,Reboot、Shutdown and Hibernate are temporarily unavailable.</source>
        <translation>ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠠᠶᠤᠯᠬᠦᠢ᠎ᠶᠢᠨ ᠳᠦᠯᠦᠬᠡ ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦᠯᠬᠦ᠌᠂ᠬᠠᠭᠠᠬᠤ ᠪᠤᠯᠤᠨ ᠢᠴᠡᠬᠡᠯᠡᠬᠦ᠌ ᠴᠢᠳᠠᠪᠬᠢ᠎ᠶᠢ ᠲᠦᠷ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ᠍ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠃</translation>
    </message>
    <message>
        <location filename="../ukui-session/main.cpp" line="397"/>
        <source>UKUI Session Manager</source>
        <translation>ᠶᠠᠷᠢᠯᠴᠠᠭᠠᠨ᠎ᠤ ᠬᠠᠮᠢᠶᠠᠷᠤᠭᠴᠢ</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source> is block system</source>
        <translation type="vanished">阻止系统</translation>
    </message>
    <message>
        <source> into sleep for reason </source>
        <translation type="vanished">休眠，因为</translation>
    </message>
    <message>
        <source> is block system </source>
        <translation type="vanished">阻止系统</translation>
    </message>
    <message>
        <source>into sleep for reason </source>
        <translation type="vanished">睡眠，因为</translation>
    </message>
    <message>
        <source>Are you sure</source>
        <translation type="vanished">你确定</translation>
    </message>
    <message>
        <source> you want to get system into sleep?</source>
        <translation type="vanished">要让系统进入休眠吗?</translation>
    </message>
    <message>
        <source>Are you sure you want to get system into sleep?</source>
        <translation type="vanished">你确定要让系统进入睡眠吗?</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="61"/>
        <source>cancel</source>
        <translation type="unfinished">ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="62"/>
        <source>confirm</source>
        <translation type="unfinished">ᠪᠠᠳᠤᠯᠠᠬᠤ</translation>
    </message>
    <message>
        <source> is block system into reboot for reason </source>
        <translation type="vanished">阻止系统重启，因为</translation>
    </message>
    <message>
        <source> is block system into shutdown for reason </source>
        <translation type="vanished">阻止系统关机，因为</translation>
    </message>
    <message>
        <source>Are you sure you want to reboot?</source>
        <translation type="vanished">你确定要重启系统吗？</translation>
    </message>
    <message>
        <source>Are you sure you want to shutdown?</source>
        <translation type="vanished">你确定要退出系统吗？</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="60"/>
        <source>Multiple users are logged in at the same time.Are you sure you want to close this system?</source>
        <translation type="unfinished">ᠬᠡᠳᠦ ᠬᠡᠳᠦᠨ ᠬᠡᠷᠡᠭᠯᠡᠭᠴᠢ ᠠᠳᠠᠯᠢ ᠴᠠᠭ᠎ᠲᠤ ᠨᠡᠪᠳᠡᠷᠡᠵᠤ ᠪᠠᠢᠬᠤ ᠪᠠᠢᠳᠠᠯ ᠲᠠᠢ ᠂ ᠲᠠ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠡᠴᠡ ᠪᠤᠴᠠᠵᠤ ᠭᠠᠷᠬᠤ ᠤᠤ?</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="82"/>
        <source>System update or package installation in progress,this function is temporarily unavailable.</source>
        <translation>ᠶᠠᠭ ᠰᠠᠢᠬᠠᠨ ᠳᠡᠰ ᠳᠡᠪᠱᠢᠬᠦ᠌ ᠪᠤᠶᠤ ᠤᠭᠰᠠᠷᠠᠬᠤ/ ᠪᠠᠭᠤᠯᠭᠠᠬᠤ （ ᠬᠠᠮᠢᠶᠠᠳᠠᠢ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ）᠎ᠶᠢ ᠬᠦᠢᠴᠡᠳᠭᠡᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠡᠨᠡ ᠴᠢᠳᠠᠪᠬᠢ᠎ᠶᠢ ᠲᠦᠷ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ᠍ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ᠃</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="83"/>
        <source>System backup or restore in progress,this function is temporarily unavailable.</source>
        <translation>ᠶᠠᠭ ᠰᠠᠢᠬᠠᠨ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠢ ᠬᠠᠭᠤᠯᠵᠤ ᠬᠠᠳᠠᠭᠠᠯᠠᠬᠤ ᠪᠤᠶᠤ ᠬᠡᠪ᠎ᠲᠦ᠍ ᠨᠢ ᠤᠷᠤᠭᠤᠯᠵᠤ ᠪᠠᠢᠨ᠎ᠠ᠂ ᠡᠨᠡ ᠴᠢᠳᠠᠪᠬᠢ᠎ᠶᠢ ᠲᠦᠷ ᠬᠡᠷᠡᠭᠯᠡᠵᠦ᠍ ᠪᠤᠯᠬᠤ ᠦᠬᠡᠢ ᠃</translation>
    </message>
    <message>
        <location filename="../tools/main.cpp" line="95"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1381"/>
        <source>The following program is running to prevent the system from hibernate!</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠫᠠᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠢᠴᠡᠬᠡᠯᠡᠬᠦ᠌᠎ᠶᠢ ᠬᠤᠷᠢᠭᠯᠠᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1384"/>
        <source>The following program is running to prevent the system from suspend!</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠫᠠᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠤᠨᠳᠠᠬᠤ᠎ᠶᠢ ᠬᠤᠷᠢᠭᠯᠠᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1387"/>
        <source>The following program is running to prevent the system from logout!</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠫᠠᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠢ ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ᠎ᠶᠢ ᠬᠤᠷᠢᠭᠯᠠᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1390"/>
        <source>The following program is running to prevent the system from reboot!</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠫᠠᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠳᠠᠬᠢᠵᠤ ᠡᠬᠢᠯᠡᠬᠦ᠌᠎ᠶᠢ ᠬᠤᠷᠢᠭᠯᠠᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1393"/>
        <source>The following program is running to prevent the system from shutting down!</source>
        <translation>ᠳᠠᠷᠠᠭᠠᠬᠢ ᠫᠠᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠱᠢᠰᠲ᠋ᠧᠮ᠎ᠦᠨ ᠬᠠᠭᠠᠬᠤ᠎ᠶᠢ ᠬᠤᠷᠢᠭᠯᠠᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1454"/>
        <source>Still Hibernate</source>
        <translation>ᠢᠴᠡᠭᠡᠯᠡᠭᠰᠡᠬᠡᠷ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1456"/>
        <source>Still Suspend</source>
        <translation>ᠤᠨᠳᠠᠭᠰᠠᠭᠠᠷ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1460"/>
        <source>Still Reboot</source>
        <translation>ᠡᠬᠢᠯᠡᠬᠦᠯᠦᠭᠰᠡᠬᠡᠷ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1462"/>
        <source>Still Shutdown</source>
        <translation>ᠬᠠᠭᠠᠭᠰᠠᠭᠠᠷ ᠪᠠᠢᠨ᠎ᠠ</translation>
    </message>
    <message>
        <location filename="../tools/mainwindow.cpp" line="1478"/>
        <source>Cancel</source>
        <translation type="unfinished">ᠬᠦᠴᠦᠨ ᠦᠬᠡᠢ ᠪᠤᠯᠭᠠᠬᠤ</translation>
    </message>
    <message>
        <location filename="../tools/powerprovider.cpp" line="61"/>
        <source>some applications are running and they don&apos;t want you to do this.</source>
        <translation>ᠵᠠᠷᠢᠮ ᠫᠠᠷᠦᠭᠷᠡᠮ ᠶᠠᠭ ᠠᠵᠢᠯᠯᠠᠵᠤ ᠪᠠᠢᠨ᠎ᠠ ᠂ ᠳᠡᠭᠡᠬᠦ᠌ ᠮᠦᠷᠳᠡᠭᠡᠨ ᠡᠨᠡ ᠨᠢ ᠲᠠᠨ᠎ᠤ ᠲᠤᠰ ᠠᠵᠢᠯᠯᠠᠬᠤᠢ᠎ᠶᠢ ᠦᠷᠬᠦᠯᠵᠢᠯᠡᠨ ᠬᠦᠢᠴᠡᠳᠭᠡᠬᠦ᠌᠎ᠶᠢ ᠬᠦᠰᠡᠬᠦ᠌ ᠦᠬᠡᠢ ᠪᠠᠢᠨ᠎ᠠ ᠃</translation>
    </message>
    <message>
        <location filename="../tools/powerprovider.cpp" line="63"/>
        <source>Still to do!</source>
        <translation>ᠬᠦᠢᠴᠡᠳᠭᠡᠭᠰᠡᠬᠡᠷ ᠪᠠᠢᠨ᠎ᠠ!</translation>
    </message>
    <message>
        <location filename="../tools/powerprovider.cpp" line="64"/>
        <source>give up</source>
        <translation>ᠳᠡᠪᠴᠢᠬᠦ᠌</translation>
    </message>
</context>
</TS>
